SimbaNicApp.directive('simbaUserLock', function(settings) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
        
        $.backstretch([
            settings.imgPath + "/bg/1.jpg",
            settings.imgPath + "/bg/2.jpg",
            settings.imgPath + "/bg/3.jpg",
            settings.imgPath + "/bg/4.jpg"
            ], {
            fade: 1000,
            duration: 8000
        });

        }
    };
});