(function() {
  var addCurrency;

  $(document).ready(function() {
    return $('input:checkbox#chk_all_remove').change(function() {
      return $('input:checkbox[id*="properties_config_id"]').prop('checked', $(this).prop("checked"));
    });
  });

  $(document).ready(function() {
    $('input:checkbox[id^="chk_all_reserve"][id*="_rental"]').change(function() {
      var cat, first_figure, last_figure, last_sum, name, other_last_sum, other_selected, other_sum, selected, subject_sq_ft, sum, total_sum, user_price;
      name = $(this).attr('id');
      if (name === 'chk_all_reserve_rental') {
        $('input:checkbox[id*="property_rental_id"]').prop('checked', $(this).prop("checked"));
      } else {
        $('input:checkbox[id*="property_other_rental_id"]').prop('checked', $(this).prop("checked"));
      }
      selected = [];
      other_selected = [];
      cat = $('input[id*="property_rental_category"]').val();
      if ($(this).attr('checked')) {
        if (name === 'chk_all_reserve_rental') {
          $('table#ref_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        } else {
          $('table#ref_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        }
        sum = 0;
        $.each(selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          sum += xval;
        });
        last_sum = 0;
        if (isNaN(sum)) {
          last_sum = 0;
        } else {
          last_sum = sum.toFixed(2);
        }
        other_sum = 0;
        $.each(other_selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          other_sum += xval;
        });
        other_last_sum = 0;
        if (isNaN(other_sum)) {
          other_last_sum = 0;
        } else {
          other_last_sum = other_sum.toFixed(2);
        }
        first_figure = (!isNaN(last_sum) ? last_sum : 0);
        last_figure = (!isNaN(other_last_sum) ? other_last_sum : 0);
        if (isNaN((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length))) {
          total_sum = 0;
        } else {
          total_sum = ((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length)).toFixed(2);
        }
        if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val("");
          } else {
            $('input[id*="comparables_user_avm_rental"]').val("");
          }
        } else {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val(total_sum);
          } else {
            $('input[id*="comparables_user_avm_rental"]').val(total_sum);
          }
        }
        user_price = 0;
        if (cat === 'sale') {
          if (last_sum > 1) {
            subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
            user_price = (parseFloat(subject_sq_ft) * total_sum).toFixed(0);
          } else {
            user_price = 0;
          }
        } else {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (total_sum / parseFloat(subject_sq_ft)).toFixed(2);
        }
        if (cat === 'sale') {
          $('input[id*="comparables_user_avm_sale"]').val(user_price);
        } else {
          $('input[id*="comparables_user_sale_price_psf_rental"]').val(user_price);
        }
      } else {
        if (name === 'chk_all_reserve_rental') {
          $('table#ref_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        } else {
          $('table#ref_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_rental tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        }
        sum = 0;
        $.each(selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          sum += xval;
        });
        last_sum = 0;
        if (isNaN(sum / selected.length)) {
          last_sum = 0;
        } else {
          last_sum = sum.toFixed(2);
        }
        other_sum = 0;
        $.each(other_selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          other_sum += xval;
        });
        other_last_sum = 0;
        if (isNaN(other_sum)) {
          other_last_sum = 0;
        } else {
          other_last_sum = other_sum.toFixed(2);
        }
        first_figure = (!isNaN(last_sum) ? last_sum : 0);
        last_figure = (!isNaN(other_last_sum) ? other_last_sum : 0);
        if (isNaN((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length))) {
          total_sum = 0;
        } else {
          total_sum = ((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length)).toFixed(2);
        }
        if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val("");
          } else {
            $('input[id*="comparables_user_avm_rental"]').val("");
          }
        } else {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val(total_sum);
          } else {
            $('input[id*="comparables_user_avm_rental"]').val(total_sum);
          }
        }
        user_price = 0;
        if (cat === 'sale') {
          if (last_sum > 1) {
            subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
            user_price = (parseFloat(subject_sq_ft) * total_sum).toFixed(0);
          } else {
            user_price = 0;
          }
        } else {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (total_sum / parseFloat(subject_sq_ft)).toFixed(2);
        }
        if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
          if (cat === 'sale') {
            $('input[id*="comparables_user_avm_sale"]').val("");
          } else {
            $('input[id*="comparables_user_sale_price_psf_rental"]').val("");
          }
        } else {
          if (cat === 'sale') {
            $('input[id*="comparables_user_avm_sale"]').val(user_price);
          } else {
            $('input[id*="comparables_user_sale_price_psf_rental"]').val(user_price);
          }
        }
      }
    });
    return $('input:checkbox[id^="chk_all_reserve"][id*="_sale"]').change(function() {
      var cat, first_figure, last_figure, last_sum, name, other_last_sum, other_selected, other_sum, selected, subject_sq_ft, sum, total_sum, user_price;
      name = $(this).attr('id');
      if (name === 'chk_all_reserve_sale') {
        $('input:checkbox[id*="property_sale_id"]').prop('checked', $(this).prop("checked"));
      } else {
        $('input:checkbox[id*="property_other_sale_id"]').prop('checked', $(this).prop("checked"));
      }
      selected = [];
      other_selected = [];
      cat = $('input[id*="property_sale_category"]').val();
      if ($(this).attr('checked')) {
        if (name === 'chk_all_reserve_sale') {
          $('table#ref_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        } else {
          $('table#ref_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        }
        sum = 0;
        $.each(selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          sum += xval;
        });
        last_sum = 0;
        if (isNaN(sum)) {
          last_sum = 0;
        } else {
          last_sum = sum.toFixed(2);
        }
        other_sum = 0;
        $.each(other_selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          other_sum += xval;
        });
        other_last_sum = 0;
        if (isNaN(other_sum)) {
          other_last_sum = 0;
        } else {
          other_last_sum = other_sum.toFixed(2);
        }
        first_figure = (!isNaN(last_sum) ? last_sum : 0);
        last_figure = (!isNaN(other_last_sum) ? other_last_sum : 0);
        if (isNaN((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length))) {
          total_sum = 0;
        } else {
          total_sum = ((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length)).toFixed(2);
        }
        if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val("");
          } else {
            $('input[id*="comparables_user_avm_rental"]').val("");
          }
        } else {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val(total_sum);
          } else {
            $('input[id*="comparables_user_avm_rental"]').val(total_sum);
          }
        }
        user_price = 0;
        if (cat === 'sale') {
          if (last_sum > 1) {
            subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
            user_price = (parseFloat(subject_sq_ft) * total_sum).toFixed(0);
          } else {
            user_price = 0;
          }
        } else {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (total_sum / parseFloat(subject_sq_ft)).toFixed(2);
        }
        if (cat === 'sale') {
          $('input[id*="comparables_user_avm_sale"]').val(user_price);
        } else {
          $('input[id*="comparables_user_sale_price_psf_rental"]').val(user_price);
        }
      } else {
        if (name === 'chk_all_reserve_sale') {
          $('table#ref_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        } else {
          $('table#ref_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              selected.push($(this).attr('data-sale-price-psf'));
            } else {
              selected.push($(this).attr('data-sale-price'));
            }
          });
          $('table#ref_other_sale tbody input:checked').each(function() {
            var id;
            id = $(this).attr('id');
            if (cat === 'sale') {
              other_selected.push($(this).attr('data-sale-price-psf'));
            } else {
              other_selected.push($(this).attr('data-sale-price'));
            }
          });
        }
        sum = 0;
        $.each(selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          sum += xval;
        });
        last_sum = 0;
        if (isNaN(sum / selected.length)) {
          last_sum = 0;
        } else {
          last_sum = sum.toFixed(2);
        }
        other_sum = 0;
        $.each(other_selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          other_sum += xval;
        });
        other_last_sum = 0;
        if (isNaN(other_sum)) {
          other_last_sum = 0;
        } else {
          other_last_sum = other_sum.toFixed(2);
        }
        first_figure = (!isNaN(last_sum) ? last_sum : 0);
        last_figure = (!isNaN(other_last_sum) ? other_last_sum : 0);
        if (isNaN((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length))) {
          total_sum = 0;
        } else {
          total_sum = ((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length)).toFixed(2);
        }
        if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val("");
          } else {
            $('input[id*="comparables_user_avm_rental"]').val("");
          }
        } else {
          if (cat === 'sale') {
            $('input[id*="comparables_user_sale_price_psf_sale"]').val(total_sum);
          } else {
            $('input[id*="comparables_user_avm_rental"]').val(total_sum);
          }
        }
        user_price = 0;
        if (cat === 'sale') {
          if (last_sum > 1) {
            subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
            user_price = (parseFloat(subject_sq_ft) * total_sum).toFixed(0);
          } else {
            user_price = 0;
          }
        } else {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (total_sum / parseFloat(subject_sq_ft)).toFixed(2);
        }
        if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
          if (cat === 'sale') {
            $('input[id*="comparables_user_avm_sale"]').val("");
          } else {
            $('input[id*="comparables_user_sale_price_psf_rental"]').val("");
          }
        } else {
          if (cat === 'sale') {
            $('input[id*="comparables_user_avm_sale"]').val(user_price);
          } else {
            $('input[id*="comparables_user_sale_price_psf_rental"]').val(user_price);
          }
        }
      }
    });
  });

  $(document).ready(function() {
    $('input[id^="property_"][id*="_rental_id"]').click(function() {
      var cat, first_figure, last_figure, last_sum, other_last_sum, other_selected, other_sum, selected, subject_sq_ft, sum, total_sum, user_price;
      cat = $('input[id*="property_rental_category"]').val();
      selected = [];
      $('table#ref_rental tbody input:checked').each(function() {
        var id;
        id = $(this).attr('id');
        if (cat === 'sale') {
          selected.push($(this).attr('data-sale-price-psf'));
        } else {
          selected.push($(this).attr('data-sale-price'));
        }
      });
      sum = 0;
      $.each(selected, function(index, value) {
        var xval;
        xval = parseFloat(value);
        sum += xval;
      });
      last_sum = 0;
      if (isNaN(sum)) {
        last_sum = 0;
      } else {
        last_sum = sum.toFixed(2);
      }
      other_selected = [];
      $('table#ref_other_rental tbody input:checked').each(function() {
        var id;
        id = $(this).attr('id');
        if (cat === 'sale') {
          other_selected.push($(this).attr('data-sale-price-psf'));
        } else {
          other_selected.push($(this).attr('data-sale-price'));
        }
      });
      other_sum = 0;
      other_sum = 0;
      $.each(other_selected, function(index, value) {
        var xval;
        xval = parseFloat(value);
        other_sum += xval;
      });
      other_last_sum = 0;
      if (isNaN(other_sum)) {
        other_last_sum = 0;
      } else {
        other_last_sum = other_sum.toFixed(2);
      }
      first_figure = (!isNaN(last_sum) ? last_sum : 0);
      last_figure = (!isNaN(other_last_sum) ? other_last_sum : 0);
      if (isNaN((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length))) {
        total_sum = 0;
      } else {
        total_sum = ((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length)).toFixed(2);
      }
      if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
        if (cat === 'sale') {
          $('input[id*="comparables_user_sale_price_psf_sale"]').val("");
        } else {
          $('input[id*="comparables_user_avm_rental"]').val("");
        }
      } else {
        if (cat === 'sale') {
          $('input[id*="comparables_user_sale_price_psf_sale"]').val(total_sum);
        } else {
          $('input[id*="comparables_user_avm_rental"]').val(total_sum);
        }
      }
      user_price = 0;
      if (cat === 'sale') {
        if (total_sum > 1) {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (parseFloat(subject_sq_ft) * total_sum).toFixed(0);
        } else {
          user_price = "";
        }
      } else {
        if (total_sum > 0) {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (total_sum / parseFloat(subject_sq_ft)).toFixed(2);
        } else {
          user_price = "";
        }
      }
      if (cat === 'sale') {
        $('input[id*="comparables_user_avm_sale"]').val(user_price);
      } else {
        $('input[id*="comparables_user_sale_price_psf_rental"]').val(user_price);
      }
      if ($('table#ref_rental tbody input:checked').length !== $('table#ref_rental tbody input').length) {
        $('input:checkbox#chk_all_reserve_rental').attr('checked', false);
      }
      if ($('table#ref_other_rental tbody input:checked').length !== $('table#ref_other_rental tbody input').length) {
        $('input:checkbox#chk_all_reserve_other_rental').attr('checked', false);
      }
      if (($('table#ref_rental tbody input').length > 0) && ($('table#ref_rental tbody input:checked').length === $('table#ref_rental tbody input').length)) {
        $('input:checkbox#chk_all_reserve_rental').attr('checked', true);
      }
      if (($('table#ref_other_rental tbody input').length > 0) && ($('table#ref_other_rental tbody input:checked').length === $('table#ref_other_rental tbody input').length)) {
        return $('input:checkbox#chk_all_reserve_other_rental').attr('checked', true);
      }
    });
    return $('input[id^="property_"][id*="_sale_id"]').click(function() {
      var cat, first_figure, last_figure, last_sum, other_last_sum, other_selected, other_sum, selected, subject_sq_ft, sum, total_sum, user_price;
      cat = $('input[id*="property_sale_category"]').val();
      selected = [];
      $('table#ref_sale tbody input:checked').each(function() {
        var id;
        id = $(this).attr('id');
        if (cat === 'sale') {
          selected.push($(this).attr('data-sale-price-psf'));
        } else {
          selected.push($(this).attr('data-sale-price'));
        }
      });
      sum = 0;
      $.each(selected, function(index, value) {
        var xval;
        xval = parseFloat(value);
        sum += xval;
      });
      last_sum = 0;
      if (isNaN(sum)) {
        last_sum = 0;
      } else {
        last_sum = sum.toFixed(2);
      }
      other_selected = [];
      $('table#ref_other_sale tbody input:checked').each(function() {
        var id;
        id = $(this).attr('id');
        if (cat === 'sale') {
          other_selected.push($(this).attr('data-sale-price-psf'));
        } else {
          other_selected.push($(this).attr('data-sale-price'));
        }
      });
      other_sum = 0;
      $.each(other_selected, function(index, value) {
        var xval;
        xval = parseFloat(value);
        other_sum += xval;
      });
      other_last_sum = 0;
      if (isNaN(other_sum)) {
        other_last_sum = 0;
      } else {
        other_last_sum = other_sum.toFixed(2);
      }
      first_figure = (!isNaN(last_sum) ? last_sum : 0);
      last_figure = (!isNaN(other_last_sum) ? other_last_sum : 0);
      if (isNaN((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length))) {
        total_sum = 0;
      } else {
        total_sum = ((parseFloat(first_figure) + parseFloat(last_figure)) / (selected.length + other_selected.length)).toFixed(2);
      }
      if (isNaN(total_sum) || ((selected.length + other_selected.length) <= 0)) {
        if (cat === 'sale') {
          $('input[id*="comparables_user_sale_price_psf_sale"]').val("");
        } else {
          $('input[id*="comparables_user_avm_rental"]').val("");
        }
      } else {
        if (cat === 'sale') {
          $('input[id*="comparables_user_sale_price_psf_sale"]').val(total_sum);
        } else {
          $('input[id*="comparables_user_avm_rental"]').val(total_sum);
        }
      }
      user_price = 0;
      if (cat === 'sale') {
        if (total_sum > 1) {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (parseFloat(subject_sq_ft) * total_sum).toFixed(0);
        } else {
          user_price = "";
        }
      } else {
        if (total_sum > 0) {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (total_sum / parseFloat(subject_sq_ft)).toFixed(2);
        } else {
          user_price = "";
        }
      }
      if (cat === 'sale') {
        $('input[id*="comparables_user_avm_sale"]').val(user_price);
      } else {
        $('input[id*="comparables_user_sale_price_psf_rental"]').val(user_price);
      }
      if ($('table#ref_sale tbody input:checked').length !== $('table#ref_sale tbody input').length) {
        $('input:checkbox#chk_all_reserve_sale').attr('checked', false);
      }
      if (($('table#ref_sale tbody input').length > 0) && ($('table#ref_sale tbody input:checked').length === $('table#ref_sale tbody input').length)) {
        $('input:checkbox#chk_all_reserve_sale').attr('checked', true);
      }
      if ($('table#ref_other_sale tbody input:checked').length !== $('table#ref_other_sale tbody input').length) {
        $('input:checkbox#chk_all_reserve_other_sale').attr('checked', false);
      }
      if (($('table#ref_other_sale tbody input').length > 0) && ($('table#ref_other_sale tbody input:checked').length === $('table#ref_other_sale tbody input').length)) {
        return $('input:checkbox#chk_all_reserve_other_sale').attr('checked', true);
      }
    });
  });

  addCurrency = function(x) {
    var retVal;
    retVal = x ? parseFloat(x.replace(/[,$]/g, '')) : 0;
    return '$' + retVal.toString().replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  };

}).call(this);
