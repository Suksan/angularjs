(function() {
  var addCurrency;

  $(document).ready(function() {
    var hash;
    hash = window.location.hash.trim();
    if ((!hash) || (hash && hash.indexOf("rental") > -1)) {
      $("#sale_tab > li > a:first").addClass("josh");
    }
    if ((!hash) || (hash && hash.indexOf("sale") > -1)) {
      $("#rental_tab > li > a:first").addClass("josh");
    }
    $("#sale_tab > li > a").bind("click", function() {
      $("#sale_tab > li > a").not(this).each(function() {
        $(this).removeClass("josh");
      });
    });
    $("#rental_tab > li > a").bind("click", function() {
      $("#rental_tab > li > a").not(this).each(function() {
        $(this).removeClass("josh");
      });
    });
  });

  $(document).ready(function() {
    return $('input:checkbox#chk_all_remove').change(function() {
      return $('input:checkbox[id*="properties_config_id"]').prop('checked', $(this).prop("checked"));
    });
  });

  $(document).ready(function() {
    return $('input:checkbox#chk_all_reserve').change(function() {
      var last_sum, selected, subject_sq_ft, sum, user_price;
      $('input:checkbox[id*="property_config_id"]').prop('checked', $(this).prop("checked"));
      if ($('input:checkbox#chk_all_reserve').attr('checked')) {
        selected = [];
        $('table#ref tbody input').each(function() {
          var id;
          id = $(this).attr('id');
          selected.push($(this).attr('data-sale-price-psf'));
        });
        sum = 0;
        $.each(selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          sum += xval;
        });
        last_sum = 0;
        if (isNaN(sum / selected.length)) {
          last_sum = 0;
        } else {
          last_sum = (sum / selected.length).toFixed(2);
        }
        if (isNaN(sum / selected.length)) {
          $('input[id^="property_proforma_comparables_user_sale_price_psf"]').val("");
        } else {
          $('input[id^="property_proforma_comparables_user_sale_price_psf"]').val(last_sum);
        }
        user_price = 0;
        if (last_sum > 1) {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (parseFloat(subject_sq_ft) * last_sum).toFixed(0);
        } else {
          user_price = 0;
        }
        $('input[id^="property_proforma_comparables_user_avm"]').val(user_price);
      } else {
        selected = [];
        $('table#ref tbody input').each(function() {
          var id;
          id = $(this).attr('id');
          selected.push($(this).attr('data-sale-price-psf'));
        });
        sum = 0;
        $.each(selected, function(index, value) {
          var xval;
          xval = parseFloat(value);
          sum += xval;
        });
        last_sum = 0;
        if (isNaN(sum / selected.length)) {
          last_sum = 0;
        } else {
          last_sum = (sum / selected.length).toFixed(2);
        }
        $('input[id^="property_proforma_comparables_user_sale_price_psf"]').val("");
        user_price = 0;
        if (last_sum > 1) {
          subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
          user_price = (parseFloat(subject_sq_ft) * last_sum).toFixed(2);
        } else {
          user_price = 0;
        }
        $('input[id^="property_proforma_comparables_user_avm"]').val("");
      }
    });
  });

  $(document).ready(function() {
    return $('input[id^="property_config_id"]').click(function() {
      var last_sum, selected, subject_sq_ft, sum, user_price;
      selected = [];
      $('table#ref tbody input:checked').each(function() {
        var id;
        id = $(this).attr('id');
        selected.push($(this).attr('data-sale-price-psf'));
      });
      sum = 0;
      $.each(selected, function(index, value) {
        var xval;
        xval = parseFloat(value);
        sum += xval;
      });
      last_sum = 0;
      if (isNaN(sum / selected.length)) {
        last_sum = 0;
      } else {
        last_sum = (sum / selected.length).toFixed(2);
      }
      if (isNaN(sum / selected.length)) {
        $('input[id^="property_proforma_comparables_user_sale_price_psf"]').val("");
      } else {
        $('input[id^="property_proforma_comparables_user_sale_price_psf"]').val(last_sum);
      }
      user_price = 0;
      if (last_sum > 1) {
        subject_sq_ft = $('label[for="square_feet"]').parent().next().text();
        user_price = (parseFloat(subject_sq_ft) * last_sum).toFixed(0);
      } else {
        user_price = "";
      }
      $('input[id^="property_proforma_comparables_user_avm"]').val(user_price);
      if ($('table#ref tbody input:checked').length !== $('table#ref tbody input').length) {
        return $('input:checkbox#chk_all_reserve').attr('checked', false);
      }
    });
  });

  addCurrency = function(x) {
    var retVal;
    retVal = x ? parseFloat(x.replace(/[,$]/g, '')) : 0;
    return '$' + retVal.toString().replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  };

}).call(this);
