(function() {
  var $geoerrors, $geoinfo, $geomap, bounds, html_content_for, icons, map, marker, p, position, properties, _i, _len;

  $geomap = $('#geomap');

  if ($geomap.length === 0) {
    return;
  }

  $geoinfo = $('#geoinfo');

  properties = $geomap.data('properties');

  icons = $geomap.data('icons');

  map = new google.maps.Map($geomap[0], {
    zoom: 4,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  $geoerrors = $('#geoerrors');

  html_content_for = function(p) {
    var content, k, v, _ref;
    content = "<h5><img src=\"" + icons[p.index] + "\" style=\"height: 30px !important\" /> <a href=\"" + p.url + "\" target=\"_blank\">" + p.address + "</a></h5>";
    content += "<table>";
    _ref = p.data;
    for (k in _ref) {
      v = _ref[k];
      content += "<tr><td><b>" + k + "</b></td><td>" + v + "</td></tr>";
    }
    content += "</table>";
    return content;
  };

  bounds = new google.maps.LatLngBounds();

  for (_i = 0, _len = properties.length; _i < _len; _i++) {
    p = properties[_i];
    if (p.geocoded) {
      position = new google.maps.LatLng(p.latitude, p.longitude);
      bounds.extend(position);
      marker = new google.maps.Marker({
        position: position,
        map: map,
        title: p.address,
        html: html_content_for(p),
        icon: icons[p.index]
      });
      google.maps.event.addListener(marker, "mouseover", (function(marker) {
        return function() {
          return $geoinfo.html(marker.html);
        };
      })(marker));
    } else {
      marker = $("<img src=\"" + icons[p.index] + "\" style=\"height: 30px !important; margin-left: 10px; cursor: pointer;\" />");
      marker.data('html', html_content_for(p));
      marker.on('mouseover', function() {
        return $geoinfo.html($(this).data('html'));
      });
      $geoerrors.append(marker);
    }
  }

  map.fitBounds(bounds);

}).call(this);
