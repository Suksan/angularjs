SimbaNicApp.factory('SimbaNicAPI', function ($http, $window, AuthService, settings) {
    var simbaFactory = {};

    // Save Record
    simbaFactory.save = function (module_name, name_value_list) {
        
        var param = {
            session: AuthService.getSessionId(),
            module: module_name,
            name_value_list: name_value_list
        }

        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "set_entry",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'service/v4_1/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    }

    simbaFactory.saveDocument = function (document_id, file, filename, revision) {
        
        var param = {
            session: AuthService.getSessionId(),
        }
        param['note'] = {
            'id': document_id,
            'file': file,
            'filename': filename,
            'revision': revision
        };

        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "set_document_revision",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'service/v4_1/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    }

    simbaFactory.saveDocumentRevisions = function (document_id, revisions_array) {
        
        var param = {
            session: AuthService.getSessionId(),
            document_id: document_id,
            revisions_array: revisions_array
        }

        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "simba_save_document_revisions",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'custom/service/v4_1_custom/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    }

    simbaFactory.delete = function (module_name, guid) {

        var param = {
            session: AuthService.getSessionId(),
            module_name: module_name,
            name_value_list: [
                [
                    {
                        "name": "id",
                        "value": guid,
                    },
                    {
                        "name": "deleted",
                        "value": "1"
                    }
                ],
            ],
        }
        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "set_entries",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'service/v4_1/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    };

    // Get DetailView
    simbaFactory.getEntry = function (module_name, guid, select_fields, link_name_to_fields_array) {

        if (typeof(select_fields)==='undefined') select_fields = [];
        if (typeof(link_name_to_fields_array)==='undefined') link_name_to_fields_array = [];

        var param = {
            session: AuthService.getSessionId(),
            module_name: module_name,
            ids: [guid],
            select_fields: select_fields,
            link_name_to_fields_array: link_name_to_fields_array
        }
        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "get_simba_entries",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'custom/service/v4_1_custom/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    };

    // Get ListView
    simbaFactory.getEntryList = function (module_name, query, order_by, offset, select_fields, link_name_to_fields_array, max_results, deleted, favorites) {

        if (typeof(query)==='undefined') query = '';
        if (typeof(order_by)==='undefined') order_by = '';
        if (typeof(offset)==='undefined') offset = 0;
        if (typeof(select_fields)==='undefined') select_fields = [];
        if (typeof(link_name_to_fields_array)==='undefined') link_name_to_fields_array = [];
        if (typeof(max_results)==='undefined') max_results = 20;
        if (typeof(deleted)==='undefined') deleted = 0;
        if (typeof(favorites)==='undefined') favorites = false;

        var param = {
            session: AuthService.getSessionId(),
            module_name: module_name,
            query: query,
            order_by: order_by,
            offset: offset,
            select_fields: select_fields,
            link_name_to_fields_array: link_name_to_fields_array,
            max_results: max_results,
            deleted: deleted,
            favorites: favorites
        }
        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "get_simba_entry_list",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'custom/service/v4_1_custom/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    };

    // get relationship list view
    simbaFactory.getRelationshipList = function (module_name, module_id, link_field_name, deleted, related_fields, related_module_link_name_to_fields_array, related_module_query, order_by, offset, limit) {

        if (typeof(deleted)==='undefined') deleted = 0;
        if (typeof(related_fields)==='undefined') related_fields = [];
        if (typeof(related_module_link_name_to_fields_array)==='undefined') related_module_link_name_to_fields_array = [];
        if (typeof(related_module_query)==='undefined') related_module_query = '';
        if (typeof(order_by)==='undefined') order_by = '';
        if (typeof(offset)==='undefined') offset = 0;
        if (typeof(limit)==='undefined') limit = -1;

        var param = {
            session: AuthService.getSessionId(),
            module_name: module_name,
            module_id: module_id,
            link_field_name: link_field_name,
            deleted: deleted,
            related_fields: related_fields,
            related_module_link_name_to_fields_array: related_module_link_name_to_fields_array,
            related_module_query: related_module_query,
            order_by: order_by,
            offset: offset,
            limit: limit,
        }
        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "get_simba_relationships",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'custom/service/v4_1_custom/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    };

    simbaFactory.getEntriesCount = function (module_name, query, deleted) {

        if (typeof(query)==='undefined') query = '';
        if (typeof(deleted)==='undefined') deleted = 0;

        var param = {
            session: AuthService.getSessionId(),
            module_name: module_name,
            query: query,
            deleted: deleted,
        }
        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "get_entries_count",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'service/v4_1/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    };

    // Set Relationship
    simbaFactory.setRelationship = function (module_name, module_id, link_field_name, related_ids, name_value_list, deleted) {

        if (typeof(link_field_name)==='undefined') link_field_name = '';
        if (typeof(related_ids)==='undefined') related_ids = [];
        if (typeof(name_value_list)==='undefined') name_value_list = [];
        if (typeof(deleted)==='undefined') deleted = 0;

        var param = {
            session: AuthService.getSessionId(),
            module_name: module_name,
            module_id: module_id,
            link_field_name: link_field_name,
            related_ids: related_ids,
            name_value_list: name_value_list,
            delete: deleted
        }
        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "set_relationship",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + 'service/v4_1/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    };

    simbaFactory.search = function (search_string, modules, offset, max_results, assigned_user_id, select_fields, unified_search_only, favorites, search_field) {

        if (typeof(offset)==='undefined') offset = 0;
        if (typeof(max_results)==='undefined') max_results = 10;
        if (typeof(assigned_user_id)==='undefined') assigned_user_id = '';
        if (typeof(select_fields)==='undefined') select_fields = [];
        if (typeof(unified_search_only)==='undefined') unified_search_only = false;
        if (typeof(favorites)==='undefined') favorites = false;
        if (typeof(search_field)==='undefined') search_field = [];
        
        var param = {
            session: AuthService.getSessionId(),
            search_string: search_string,
            modules: modules,
            offset: offset,
            max_results: max_results,
            assigned_user_id: assigned_user_id,
            select_fields: select_fields,
            unified_search_only: unified_search_only,
            favorites: favorites,
            search_field: search_field,
        }

        var restJson = JSON.stringify(param);

        var restData = jQuery.param({
            method : "simba_search_by_module",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : restJson 
        });

        var request = $http({
            url: settings.apiURL + '/custom/service/v4_1_custom/rest.php',
            method: 'POST',
            dataType: 'JSON',
            data: restData,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
            },
        });

        return request;
    }

    return simbaFactory;
});