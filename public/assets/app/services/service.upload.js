SimbaNicApp.factory('SimbaNicUploadService', function ($http, $window, AuthService, Upload, settings) {
    
    var simbaFactory = {};

    // Upload a file
    simbaFactory.upload = function (file, module, guid, field_name, save_for_fieldname) {

        if (typeof(save_for_fieldname)==='undefined') save_for_fieldname = 1;
        
        var param = {
            session: AuthService.getSessionId(),
            module: module,
            id: guid,
            field_name: field_name,
            save_for_fieldname: save_for_fieldname,
        };

        var restJson = JSON.stringify(param);

        var uploadData = {
            method : "simba_file_upload",
            rest_data: restJson,
            input_type : "JSON",
            response_type : "JSON",
        };
        uploadData[field_name] = file;

        var request = Upload.upload({
            url: settings.apiURL + 'custom/service/v4_1_custom/rest.php',
            data: uploadData,
        });

        return request;
    }

    return simbaFactory;
});