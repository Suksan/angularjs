var SimbaNicApp = angular.module("SimbaNicApp", [
    "ui.router", 
    "ui.bootstrap",
    "oc.lazyLoad",  
    "ngSanitize",
    'ngStorage',
    'ngIdle',
    'ngMap',
    'nl2br',
    'angucomplete-alt',
    'ngFileUpload',
    'validation', 'validation.rule',
    'countUpModule',
    'ngBootbox',
    'scrollToFixed',
    'ui.calendar',
    'ui.select',
    'mwl.confirm',
    'ui.slimscroll',
    'feeds',
    'nya.bootstrap.select',
    'angularFileUpload',
    'ngFlash',
    'googlechart',
	'frapontillo.bootstrap-switch',
	'xeditable'

]);

SimbaNicApp.config(['$ocLazyLoadProvider', 'IdleProvider', 'KeepaliveProvider', '$validationProvider', function($ocLazyLoadProvider, IdleProvider, KeepaliveProvider, $validationProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
    IdleProvider.idle(60*60);
    IdleProvider.timeout(5);
    KeepaliveProvider.interval(10);
    $validationProvider.setErrorHTML(function (msg) {
        return  "<label class=\"control-label has-error\">" + msg + "</label>";
    });
    $validationProvider.setSuccessHTML(function (msg) {
       return  "";
    });
}]);



//AngularJS v1.3.x workaround for old style controller declarition in HTML
SimbaNicApp.config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);


/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
*********************************************/

/* Setup global settings */
SimbaNicApp.factory('settings', ['$rootScope', '$location', function($rootScope, $location) {
    // supported languages
    // alert('yes');
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        siteURL: $location.host() == '45.35.251.155' ? 'http://45.35.251.155:81/' : 'http://localhost:3000/',
        crmURL: $location.host() == '45.35.251.155' ? 'http://45.35.251.155:81/' : 'http://localhost:3000/',
        apiURL: $location.host() == '45.35.251.155' ? 'http://45.35.251.155:81/' : 'http://localhost:3000/',
        assetsPath: 'assets',
        globalPath: 'assets/global',
        layoutPath: 'assets/layouts/layout4',
        imgPath: 'assets/img',
        pluginsPath: 'assets/plugins',
        documentsPath: 'assets/documents',
        googleApiKey: 'AIzaSyDb5zWoch9FOF1CxVuZ3J2nEtOfesMv2PI',
        googleGeocodeApiURL: 'https://maps.googleapis.com/maps/api/geocode/json',
        financeLink: 'http://www.b2rfinance.com/apply-now/',
        subpanelMaxEntries: 5,
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
SimbaNicApp.controller('AppController', ['$scope', '$rootScope', '$window', function($scope, $rootScope, $window) {

   

    $scope.$on('Keepalive', function(e, countdown) {
        
    });

    $rootScope.salutation = ["Mr.", "Ms.", "Mrs.", "Dr.", "Prof."];

    $rootScope.simbaDataTable = {};
    $rootScope.simbaDataTable.language = {
        "aria": {
            "sortAscending": ": activate to sort column ascending",
            "sortDescending": ": activate to sort column descending"
        },
        "emptyTable": "Nothing here yet",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "",
        "infoFiltered": "(filtered1 from _MAX_ total entries)",
        "lengthMenu": "_MENU_ entries",
        "search": "",
        "zeroRecords": "No matching records found",
        "loadingRecords": "<i class='fa fa-spin fa-refresh'></i>",
        "loadingInfo": "Loading...",
        "processing": "<i class='fa fa-spin fa-refresh'></i>",

    };

    $rootScope.simbaDataTable.ListLengthMenu = [
        [10, 20, 30, 50],
        [10, 20, 30, 50]
    ];

    $rootScope.simbaDataTable.subpanelLengthMenu = [
        [5, 10, 20],
        [5, 10, 20]
    ];

    // $rootScope.simbaDataTable.subpanelPageLength = 10;

    $rootScope.simbaDataTable.ListPageLength = 10;
    $rootScope.simbaDataTable.subpanelPageLength = 5;

    $rootScope.avatarImagePath = 'assets/img/avatar.png';

    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
SimbaNicApp.controller('HeaderController', ['$scope','$http','$window', function($scope,$http,$window) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
    
   
}]);

/* Setup Layout Part - Sidebar */
SimbaNicApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Sidebar */
SimbaNicApp.controller('PageHeadController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {        
        //Demo.init(); // init theme panel
    });
}]);


/* Setup Layout Part - Footer */
SimbaNicApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
SimbaNicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/Opportunities");  
    
    
    $stateProvider

        // Dashboard
        .state('dashboard', {
            url: "/dashboard.html",
            templateUrl: "assets/app/components/dashboard/dashboard.html",            
            data: { pageTitle: 'Admin Dashboard', pageHead: 'dashboard' },
            controller: "dashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/bootstrap-confirmation/bootstrap-confirmation.js',
                            'assets/app/components/dashboard/dashboardDirective.js',
                            'assets/app/components/dashboard/dashboardController.js',
                        ]
                    });
                }],                
            }
        })

        .state('offerActive', {
            url: "/Offer",
            templateUrl: "assets/app/components/closings/listView.html",            
            data: {pageTitle: 'Offer', closingView: 'active', status: 'offer'},
            controller: "ClosingsListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/closings/function.js',
                            'assets/app/components/closings/listViewDirective.js',
                            'assets/app/components/closings/listViewController.js',
                        ]
                    });
                }],                
            }
        })

        .state('offerLost', {
            url: "/Offer/Lost",
            templateUrl: "assets/app/components/closings/listView.html",            
            data: {pageTitle: 'Offer', closingView: 'active', status: 'lost'},
            controller: "ClosingsListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/closings/function.js',
                            'assets/app/components/closings/listViewDirective.js',
                            'assets/app/components/closings/listViewController.js',
                        ]
                    });
                }],                
            }
        })

        .state('closings', {
            url: "/Closings",
            templateUrl: "assets/app/components/closings/listView.html",            
            data: {pageTitle: 'Closings', closingView: 'active', status: 'won'},
            controller: "ClosingsListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/closings/function.js',
                            'assets/app/components/closings/listViewDirective.js',
                            'assets/app/components/closings/listViewController.js',
                        ]
                    });
                }],                
            }
        })

        .state('closingsPurchased', {
            url: "/Closings/Purchased",
            templateUrl: "assets/app/components/closings/listView.html",            
            data: {pageTitle: 'Closings', closingView: 'purchased', status: 'purchase'},
            controller: "ClosingsListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/closings/function.js',
                            'assets/app/components/closings/listViewDirective.js',
                            'assets/app/components/closings/listViewController.js',
                        ]
                    });
                }],                
            }
        })

        .state('closingsCancelled', {
            url: "/Closings/Cancelled",
            templateUrl: "assets/app/components/closings/listView.html",            
            data: {pageTitle: 'Closings', closingView: 'lost', status: 'cancelled'},
            controller: "ClosingsListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/closings/function.js',
                            'assets/app/components/closings/listViewDirective.js',
                            'assets/app/components/closings/listViewController.js',
                        ]
                    });
                }],               
            }
        })

        // Closings
        .state('ClosingDetail', {
            url: "/Closings/:guid",
            templateUrl: "assets/app/components/closings/detailView.html",            
            data: {pageTitle: 'Closing'},
            controller: "ClosingsDetailViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css', 
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/ui-select/select.min.css',
                            'assets/plugins/angular/ui-select/select2.css',

                            'assets/plugins/bootstrap-confirmation/bootstrap-confirmation.js',
                            'assets/plugins/gmaps/gmaps.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/closings/detailViewDirective.js',
                            'assets/app/components/closings/detailViewController.js',
                        ]
                    });
                }],                
            }
        })

        .state('location', {
            url: "/Location",
            templateUrl: "assets/app/components/location/location.html",            
            data: {pageTitle: 'Location'},
            controller: "LocationController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/location/locationController.js',
                            'assets/app/components/location/locationDirective.js',
                        ]
                    });
                }],                
            }
        })

       .state('rooms', {
    url: "/Rooms",
    templateUrl: "assets/app/components/rooms/rooms.html",            
    data: {pageTitle: 'Rooms'},
    controller: "RoomsController",
    resolve: {
        deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name: 'SimbaNicApp',
                insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                files: [
                    'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                    'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                    'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                    'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                    'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                    'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                    'assets/plugins/datatables/datatables.min.css', 
                    'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                    'assets/plugins/angular/slick/slick.css',
                    'assets/plugins/angular/slick/slick-theme.css',
                    'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                    'assets/plugins/angular/angular-chart/angular-chart.min.css',
                    'assets/plugins/counterup/jquery.waypoints.min.js',
                    'assets/plugins/counterup/jquery.counterup.min.js',
                    'assets/plugins/angular/slick/slick.min.js',
                    'assets/plugins/gmaps/gmaps.min.js',
                    'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                    'assets/scripts/dataTable.js',
                    
                    'assets/app/components/opportunities/filter.js',
                    'assets/app/components/rooms/roomsController.js',
                    'assets/app/components/rooms/roomsDirective.js',
                ]
            });
        }],                
    }
})

        .state('trucks', {
            url: "/Trucks",
            templateUrl: "assets/app/components/trucks/trucks.html",
            data: {pageTitle: 'Trucks'},
            controller: "trucksController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/trucks/trucksController.js',
                            'assets/app/components/trucks/trucksDirective.js',
                        ]
                    });
                }],                
            }
        })

        .state('activity', {
            url: "/Activity",
            templateUrl: "assets/app/components/activity/activity.html",
            data: {pageTitle: 'Activity Type'},
            controller: "ActivityController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/activity/activityController.js',
                            'assets/app/components/activity/activityDirective.js',
                        ]
                    });
                }],                
            }
        })

         .state('routePlanning', {
            url: "/RoutePlanning",
            templateUrl: "assets/app/components/routePlanning/routePlanning.html",
            data: {pageTitle: 'Route & Planning'},
            controller: "RoutePlanningController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/routePlanning/routePlanningController.js',
                            'assets/app/components/routePlanning/routePlanningDirective.js',
                        ]
                    });
                }],                
            }
        })

         .state('binEnquiry', {
            url: "/BinEnquiry",
            templateUrl: "assets/app/components/binEnquiry/binEnquiry.html",
            data: {pageTitle: 'Bin Enquiry by Location'},
            controller: "BinEnquiryController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/binEnquiry/binEnquiryController.js',
                            'assets/app/components/binEnquiry/binEnquiryDirective.js',
                        ]
                    });
                }],                
            }
        })

        .state('routes', {
            url: "/Routes",
            templateUrl: "assets/app/components/routes/routes.html",
            data: {pageTitle: 'Routes'},
            controller: "RoutesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/routes/routesController.js',
                            'assets/app/components/routes/routesDirective.js',
                        ]
                    });
                }],                
            }
        })

        .state('drivers', {
            url: "/Drivers",
            templateUrl: "assets/app/components/drivers/drivers.html",
            data: {pageTitle: 'Drivers'},
            controller: "DriversController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/drivers/driversController.js',
                            'assets/app/components/drivers/driversDirective.js',
                        ]
                    });
                }],                
            }
        })

         .state('users', {
            url: "/Users",
            templateUrl: "assets/app/components/users/users.html",
            data: {pageTitle: 'Users'},
            controller: "UsersController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',                           
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/users/filter.js',
                            'assets/app/components/users/usersController.js',
                            'assets/app/components/users/usersDirective.js',
                        ]
                    });
                }],                
            }
        })

         .state('jobTracking', {
            url: "/JobTracking",
            templateUrl: "assets/app/components/jobTracking/jobTracking.html",
            data: {pageTitle: 'Job Tracking'},
            controller: "jobTrackingController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/jobTracking/filter.js',
                            'assets/app/components/jobTracking/jobTrackingController.js',
                            'assets/app/components/jobTracking/jobTrackingDirective.js',
                        ]
                    });
                }],                
            }
        })

         .state('history', {
            url: "/History/:id",
            templateUrl: "assets/app/components/history/history.html",
            data: {pageTitle: 'Historical Movement'},
            controller: "historyController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',                           
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/history/filter.js',
                            'assets/app/components/history/historyController.js',
                            'assets/app/components/history/historyDirective.js',
                        ]
                    });
                }],                
            }
        })

         .state('listIssue', {
            url: "/ListIssue",
            templateUrl: "assets/app/components/listIssue/listIssue.html",
            data: {pageTitle: 'List of Issues'},
            controller: "listIssueController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',                           
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/listIssue/filter.js',
                            'assets/app/components/listIssue/listIssueController.js',
                            'assets/app/components/listIssue/listIssueDirective.js',
                        ]
                    });
                }],                
            }
        })



        .state('tasksDetail', {
            url: "/Tasks/:guid",
            templateUrl: "assets/app/components/tasks/detailView.html",
            data: {pageTitle: 'Tasks'},
            controller: "TaskDetailViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/angular/angucomplete-alt/angucomplete-alt.css',
                            'assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/angular/ui-select/select.min.css',

                            'assets/plugins/bootstrap-confirmation/bootstrap-confirmation.js',

                            'assets/app/components/tasks/detailViewController.js',
                            'assets/app/components/tasks/detailViewDirective.js',
                        ]
                    });
                }],                
            }
        })

        .state('contacts', {
            abstract: true,
            url: "/Contacts",
            templateUrl: "assets/app/components/contacts/listView.html",
            data: {pageTitle: 'Contacts'},
            controller: "ContactListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/contacts/listViewController.js',
                            'assets/app/components/contacts/listViewDirective.js',
                        ]
                    });
                }],               
            }
        })

        .state('contacts.list', {
            url: "",
        })

        .state('contacts.detail', {
            url: "/:guid",
            //templateUrl: "app/components/contacts/detailView.html",
            data: {pageTitle: 'Contacts'},
            //controller: "ContactDetailViewController",
            views: {
                '': {
                    templateUrl: 'assets/app/components/contacts/detailView.html',
                    controller: 'ContactDetailViewController',
                },
            },
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/ui-select/select.min.css',
                            'assets/plugins/angular/ui-select/select2.css',

                            'assets/plugins/bootstrap-confirmation/bootstrap-confirmation.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/contacts/detailViewController.js',
                            'assets/app/components/contacts/detailViewDirective.js',
                        ]
                    });
                }],                
            }
        })

        .state('opportunityActive', {
            url: "/Opportunities",
            templateUrl: "assets/app/components/opportunities/listView.html",            
            data: {pageTitle: 'Bin'},
            controller: "opportunityListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            '/assets/app/components/opportunities/listViewDirective.js',
                            '/assets/app/components/opportunities/listViewController.js',
                        ]
                    });
                }],               
            }
        })

        .state('opportunitySaved', {
            url: "/Opportunities/Saved",
            templateUrl: "assets/app/components/opportunities/listView.html",            
            data: {pageTitle: 'Opportunity'},
            controller: "opportunityListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/opportunities/listViewDirective.js',
                            'assets/app/components/opportunities/listViewController.js',
                        ]
                    });
                }],                
            }
        })

        .state('opportunityAdd', {
            url: "/Opportunities/Edit",
            templateUrl: "assets/app/components/opportunities/editView.html",            
            data: {pageTitle: 'Add Properties'},
            controller: "opportunityEditViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                            'assets/plugins/bootstrap-select/css/bootstrap-select.min.css',

                            'assets/plugins/bootstrap-select/js/bootstrap-select.min.js',

                            'assets/app/components/opportunities/editViewDirective.js',
                            'assets/app/components/opportunities/editViewController.js',
                        ]
                    });
                }],               
            }
        })

        // Opportunity
        .state('OpportunityDetail', {
            url: "/Opportunities/:guid",
            templateUrl: "assets/app/components/opportunities/detailView.html",            
            data: {pageTitle: 'Opportunity'},
            controller: "OpportunityDetailViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        	'assets/plugins/angular/nya-bootstrap-select/nya-bs-select.min.css',
                        	'assets/plugins/angular/angular-xeditable/xeditable.min.css',
                            'assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            'assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/plugins/angular/slick/slick.css',
                            'assets/plugins/angular/slick/slick-theme.css',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.css',
                            'assets/plugins/angular/angular-chart/angular-chart.min.css',
                            'assets/plugins/counterup/jquery.waypoints.min.js',
                            'assets/plugins/counterup/jquery.counterup.min.js',
                            'assets/plugins/angular/slick/slick.min.js',
                            'assets/plugins/gmaps/gmaps.min.js',
                            'assets/plugins/angular/slick/lightbox/slick-lightbox.min.js',

                            'assets/scripts/dataTable.js',
                            
                            'assets/app/components/opportunities/filter.js',
                            'assets/app/components/opportunities/detailViewDirective.js',
                            'assets/app/components/opportunities/detailViewController.js',
                            
                        ]
                    });
                }],               
            }
        })

        // Calendar
        .state('Calendar', {
            url: "/Calendar",
            templateUrl: "assets/app/components/calendar/calendar.html",            
            data: {pageTitle: 'Calendar'},
            controller: "CalendarController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/angular/ui-calendar/fullcalendar.min.css',
                            'assets/plugins/angular/angular-clndr/angular-clndr.css',
                            
                            'assets/app/components/calendar/calendarDirective.js',
                            'assets/app/components/calendar/calendarController.js',
                        ]
                    });
                }],
                
            }
        })

        .state('reports', {
            url: "/Reports",
            templateUrl: "assets/app/components/reports/listView.html",
            data: {pageTitle: 'Reports'},
            controller: "ReportsListViewController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/plugins/datatables/datatables.min.css', 
                            'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            'assets/scripts/dataTable.js',
                            'assets/app/components/reports/listViewController.js',
                            'assets/app/components/reports/listViewDirective.js',
                        ]
                    });
                }],               
            }
        })

        // AngularJS plugins
        .state('login', {
            url: "/login.html",
            templateUrl: "assets/app/components/login/index.html",
            data: {pageTitle: 'Login'},
            controller: "loginController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'assets/css/login.css',
                            
                            //'public/assets/plugins/jquery-validation/js/jquery.validate.min.js',
                            //'public/assets/plugins/jquery-validation/js/additional-methods.min.js',
                            'assets/scripts/login.js',
                            'assets/app/components/login/loginController.js',
                        ]
                    });
                }]
            }
        })

        .state('lock', {
            url: "/lock.html",
            templateUrl: "assets/app/components/login/lock.html",            
            data: {pageTitle: 'User Lock'},
            controller: "userLockController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({

                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/css/lock.css', 

                            'assets/plugins/backstretch/jquery.backstretch.min.js',
                            'assets/scripts/lock.js',
                            
                            'assets/app/components/login/userLockController.js',
                        ]
                    });
                }]
            }
        })

        .state('changeNewPassword', {
            url: "/changeNewPassword.html",
            templateUrl: "assets/app/components/login/changeNewPassword.html",            
            data: {pageTitle: 'Change New Password'},
            controller: "changeNewPasswordController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({

                        name: 'SimbaNicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/css/login.css',
                            
                            'assets/scripts/login.js',
                            
                            'assets/app/components/login/changeNewPasswordController.js',
                        ]
                    });
                }]
            }
        })

        

}]);

/* Init global settings and run the app */
SimbaNicApp.run([ "Idle", "$rootScope", "settings", "$state", function(Idle, $rootScope, settings, $state) {
    Idle.watch();
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
}]);

SimbaNicApp.factory('AuthResolver', function($q, $rootScope, $window, AuthService, Auth) {
    return {
        resolve : function() {

            Auth.checkAuthentication()
            .success(function(response){
                if(response)
                {
                    console.log("Valid Session");
                    return response;
                }
                else
                {
                    AuthService.setCurrentUser('');
                    AuthService.setStatus(false);
                    $rootScope.loginBean.loggedIn = AuthService.getStatus();
                    $rootScope.loginBean.currentUser = AuthService.getCurrentUser();
                    $rootScope.loginBean.simbaTimeOut = false;
                    $window.location.href = '#/login.html';
                    $window.location.reload();
                }
            }).error(function(error){
                AuthService.setCurrentUser('');
                AuthService.setStatus(false);
                $rootScope.loginBean.loggedIn = AuthService.getStatus();
                $rootScope.loginBean.currentUser = AuthService.getCurrentUser();
                $rootScope.loginBean.simbaTimeOut = false;
                $window.location.href = '#/login.html';
                $window.location.reload();
            });

            var deferred = $q.defer();
            var unwatch = $rootScope.$watch('loginBean.currentUser', function(
                    currentUser) {

                if (angular.isDefined($rootScope.loginBean.currentUser)) {
                    if ($rootScope.loginBean.currentUser) {
                        if ($rootScope.loginBean.simbaTimeOut) {
                            $window.location.href = '#/lock.html';    
                        }
                        deferred.resolve($rootScope.loginBean.currentUser);
                    } else {
                        deferred.reject();
                        if ($rootScope.loginBean.simbaTimeOut) {
                            $window.location.href = '#/lock.html';    
                        }
                        else {
                            $window.location.href = '#/login.html';
                        }
                        
                    }
                    unwatch();
                }
            });
            return deferred.promise;
        }
    };
})

angular.module('SimbaNicApp').run(function(confirmationPopoverDefaults) {
    confirmationPopoverDefaults.cancelButtonType = 'danger';
    confirmationPopoverDefaults.confirmText = "<i class='glyphicon glyphicon-ok'></i> Yes";
    confirmationPopoverDefaults.cancelText = "<i class='glyphicon glyphicon-remove'></i> No";
});