SimbaNicApp.factory('CgseqService', function($http, settings, $location) {
	// alert('jam maew');
	var promise = null;
	var books = [];
	return {
       getInfo: function(guid){
            if (promise) 
						{
							return promise;
						}
						else
						{
				// alert('guid:' + guid);
							return $http.get( settings.siteURL + 'client/markets/10/properties/' + guid + '.json');
			    // $http({
			        // url: 'http://localhost:3000/client/markets/10/properties/6419326.json' ,
			        // method: 'GET',
			        // dataType: 'JSON',
			    // }).success(function(response){
			        // alert(JSON.stringify(response));
			        // return response;
			    // }).error(function(error){
			    	// alert('error');
			        // return error;
			    // });
		    }
     	},
     	getRoom: function(){
     				// alert('yay');
            if (promise) 
						{
							return promise;
						}
						else
						{
				// alert('guid:' + guid);
							// return $http.get( settings.siteURL + 'client/markets/10/properties/' + guid + '.json');
							return $http.get('http://localhost:3000/assets/app/components/rooms/rooms.json');
			    // $http({
			        // url: 'http://localhost:3000/client/markets/10/properties/6419326.json' ,
			        // method: 'GET',
			        // dataType: 'JSON',
			    // }).success(function(response){
			        // alert(JSON.stringify(response));
			        // return response;
			    // }).error(function(error){
			    	// alert('error');
			        // return error;
			    // });
		    }
     	},
     	getRentalMapInfo: function(guid){
            if (promise) 
						{
							return promise;
						}
						else
						{
				// alert('guid:' + guid);
							// return $http.get( settings.apiURL + 'client/markets/10/properties/' + guid + '.json');
							return $http.get(settings.siteURL + "client/markets/10/properties/" +  guid +  "/show_rental_comps.json");
			    // $http({
			        // url: 'http://localhost:3000/client/markets/10/properties/6419326.json' ,
			        // method: 'GET',
			        // dataType: 'JSON',
			    // }).success(function(response){
			        // alert(JSON.stringify(response));
			        // return response;
			    // }).error(function(error){
			    	// alert('error');
			        // return error;
			    // });
		    }
     	},
     	getSaleMapInfo: function(guid){
            if (promise) 
						{
							return promise;
						}
						else
						{
				// alert('guid:' + guid);
							// return $http.get( settings.apiURL + 'client/markets/10/properties/' + guid + '.json');
							return $http.get(settings.siteURL + "client/markets/10/properties/" +  guid +  "/show_sale_comps.json");
			    // $http({
			        // url: 'http://localhost:3000/client/markets/10/properties/6419326.json' ,
			        // method: 'GET',
			        // dataType: 'JSON',
			    // }).success(function(response){
			        // alert(JSON.stringify(response));
			        // return response;
			    // }).error(function(error){
			    	// alert('error');
			        // return error;
			    // });
		    }
     	},
     	saveBooks: function(guid,$params) {
          // alert($params);
	      return $http({
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	        url: settings.siteURL + 'client/markets/10/properties/' + guid + '.json' ,
	        method: "PUT",
	        data: $params,
	      })
	        .success(function(addData) {
	          books = addData;
	          
	        });
	    }   
    }
    
    
	
})