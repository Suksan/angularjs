angular.module('SimbaNicApp').controller('ClosingsDetailViewController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, closingsFunctions, Auth, AuthService, SimbaNicAPI, $ngBootbox, $injector, $filter, SimbaNicUploadService, Upload, $window) {

    $ngBootbox.hideAll();

    $scope.guid = $stateParams.guid;

    $scope.visitForm = {};
    $scope.closingInfo = {};
    $scope.closingInfo.contacts_opportunities = [];
    $scope.availableContacts = [];
    //$scope.contacts_opportunities = [];
    //$scope.db_opportunities_contacts = [];
    
    jQuery( ".simba_page_sidebar li" ).removeClass("active");
    jQuery( ".simba_page_sidebar li" ).removeClass("open");

    $rootScope.simbaLoadingShow();

    $scope.getEntry = function() {

        SimbaNicAPI.getEntry('Opportunities', $stateParams.guid)
            .success(function(response) {

                if(typeof(response.data) === 'undefined')
                {
                    $scope.getEntry();
                }
                else
                {
                    $rootScope.simbaLoadingHide();

                    $scope.closingInfo = response.data[0];
                    
                    var status = response.data[0].opportunity_status;

                    $scope.offerClass = "bg-grey";
                    $scope.wonClass = "bg-grey";
                    $scope.inspectClass = "bg-grey";
                    $scope.closingClass = "bg-grey";
                    $scope.purchaseClass = "bg-grey";
                    
                    if(status == 'offer_make_offer' || status == 'offer_out_for_signature' || status == 'offer_signed_ready_to_submit' || status == 'offer_sent_to_seller' || status == 'offer_lost_deal')
                    {
                        $scope.offerClass = "done";
                    }
                    else if(status == 'closing_contingencies')
                    {
                        $scope.offerClass = "done";
                        $scope.wonClass = "done";
                    }
                    else if(status == 'closing_inspection')
                    {
                        $scope.offerClass = "done";
                        $scope.wonClass = "done";
                        $scope.inspectClass = "done";
                    }
                    else if(status == 'closing_post_constingencies')
                    {
                        $scope.offerClass = "done";
                        $scope.wonClass = "done";
                        $scope.inspectClass = "done";
                        $scope.closingClass = "done";
                    }
                    else if(status == 'closed_purchased')
                    {
                        $scope.offerClass = "done";
                        $scope.wonClass = "done";
                        $scope.inspectClass = "done";
                        $scope.closingClass = "done";
                        $scope.purchaseClass = "done";
                    }
                    else if(status == 'closing_cancel_contract')
                    {
                        $scope.offerClass = "done";
                        $scope.wonClass = "done";
                    }
                    
                    if(status == 'closing_cancel_contract')
                    {
                        $scope.wonText = "Cancelled";
                    }
                    else
                    {
                        $scope.wonText = "Won";
                    }

                    var status_name = closingsFunctions.getClosingsStatus(status);
                    $rootScope.statusName = status_name;

                    console.log(status_name);

                    if(status_name == "Offer" || status_name == "Lost")
                    {
                        $rootScope.breadcrumbText = "Offer";
                        jQuery('.closings_menu').removeClass('active open');
                        jQuery('.offer_menu').addClass('active open');
                    }
                    else
                    {
                        $rootScope.breadcrumbText = "Closings";
                        jQuery('.offer_menu').removeClass('active open');
                        jQuery('.closings_menu').addClass('active open');
                    }

                    $state.current.data.pageTitle =  status_name + " | " + response.data[0].property_address_c;
                }
                
        });
    }

    $scope.getEntry();

    $scope.openCalendar = function(e, date) {
        $scope.open[date] = true;
    };

    

    $scope.closingsVisit = function() {
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/closings/detailView/visit.html?timestamp=' + new Date().getTime(),
            size: 'large',
            closeButton: false,
            className: 'simbanic_closings_visit_dialog',
            scope: $scope
        });
    }

    $scope.closingsFinance = function() {
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/closings/detailView/finance.html?timestamp=' + new Date().getTime(),
            size: 'large',
            closeButton: false,
            className: 'simbanic_closings_finance_dialog',
            scope: $scope
        });
    }

    $scope.uploadFiles = function(files, errFiles) {

        $scope.documentFiles = files;
        $scope.documentErrFiles = errFiles;

        angular.forEach(files, function(file) {

            var current_date = $filter('date')(new Date(),'yyyy-MM-dd');

            $rootScope.simbaLoadingShow();

            var name_value_list = [
                {
                    "name": "document_name",
                    "value": file.name
                },
                {
                    "name": "active_date",
                    "value": current_date,
                },
                {
                    "name": "status_id",
                    "value": "Pending",
                },
                {
                    "name": "revision",
                    "value": 1
                },
                {
                    "name": "assigned_user_id",
                    "value": AuthService.getUserId()
                },
            ];

            SimbaNicAPI.save('Documents', name_value_list)
                .success(function(response) {

                    var document_id = response.id;

                    var revisions_data= {
                        change_log: 'Document Created',
                        created_by: AuthService.getUserId(),
                        filename: file.name,
                        file_mime_type: file.type,
                        file_ext: file.name.substr(file.name.lastIndexOf('.')+1),
                        revision: 1,
                        document_id: document_id
                    }

                    SimbaNicAPI.saveDocumentRevisions(document_id, revisions_data)
                        .success(function(response) {
                            console.log(response);
                            SimbaNicUploadService.upload(file, 'DocumentRevisions', response.id, 'filename', 0).then(function (response) {

                                $rootScope.simbaLoadingHide();

                                jQuery("#subpanel_documents").dataTable()._fnAjaxUpdate();

                                $timeout(function () {
                                    
                                });
                            }, function (response) {

                            }, function (evt) {
                                file.progress = Math.min(100, parseInt(100.0 * 
                                                         evt.loaded / evt.total));
                            });
                        });

                    SimbaNicAPI.setRelationship('Opportunities', $stateParams.guid, 'documents', [document_id])
                        .success(function(response) {

                        });


                        /*SimbaNicAPI.saveDocument(document_id, JSON.stringify(url), file.name, 1)
                            .success(function(response) {
                                $rootScope.simbaLoadingHide();
                                jQuery("#subpanel_documents").dataTable()._fnAjaxUpdate();
                        });*/

                }).error(function(error) {
                    
                });

        });
    }

    $scope.setOpportunitiesContacts = function(opportunities_contacts) {
        if(opportunities_contacts)
        {
            if(opportunities_contacts.length > 0)
            {
                angular.forEach(opportunities_contacts, function(value, key){
                    $scope.db_opportunities_contacts.push(value.id);
                });
                console.log($scope.db_opportunities_contacts);
            }
        }
    }

    $scope.saveContactOpportunities = function() {

        var contact_ids = [];

        if($scope.closingInfo.contacts_opportunities)
        {
            if($scope.closingInfo.contacts_opportunities.length > 0)
            {
                $rootScope.simbaLoadingShow();

                angular.forEach($scope.closingInfo.contacts_opportunities, function(value, key) {
                    contact_ids.push(value.id);
                });

                SimbaNicAPI.setRelationship('Opportunities', $stateParams.guid, 'contacts', contact_ids)
                    .success(function(response) {
                        
                        $scope.$$childHead.reloadContact();
                        $scope.closingInfo.contacts_opportunities = [];
                        $scope.loadContacts("");
                        $rootScope.simbaLoadingHide();
                });
            }
        }
        console.log(contact_ids);
    }

    $scope.loadContacts = function (search_string) {

        var query = "(contacts.id NOT IN (SELECT contact_id from opportunities_contacts WHERE opportunity_id = '" + $stateParams.guid +"')) AND (contacts.first_name LIKE '%" + search_string + "%' OR contacts.last_name LIKE '%" + search_string + "%')";

        SimbaNicAPI.getEntryList("Contacts", query, 'first_name ASC', 0, ["id", "first_name", "last_name", "company_name_c", "photo", "email1"])
            .success(function(response) {

                if(typeof(response.data) === 'undefined')
                {
                    $scope.loadContacts();
                }
                else
                {
                    $scope.availableContacts = response.data;
                    $rootScope.simbaLoadingHide();
                }
        });
    }

    $scope.tagTransform = function (newTag) {

        var item = {
            first_name: newTag,
            name: newTag + ' - Create a New Contact',
            create: true,
            last_name: "",
            company_name_c: "",
            email1: ""
        };

        return item;
        
    };

    $scope.onContactSelected = function (selectedItem) {
        
        if(selectedItem.create)
        {
            $window.location.href = '#/Contacts/0?create=true&guid=' + $stateParams.guid + '&name=' + selectedItem.first_name;
            return false;
        }
    }

    /*$scope.loadRelationshipList = function () {
        SimbaNicAPI.getRelationshipList("Opportunities", $stateParams.guid, "contacts", 0, ["id", "first_name", "last_name", "company_name_c", "photo", "email1"])
        .success(function(response) {
            if(typeof(response.data) === 'undefined')
            {
                $scope.loadRelationshipList();
            }
            else
            {
                $scope.contacts_opportunities = response.data;
                $scope.setOpportunitiesContacts($scope.contacts_opportunities);
            }
            
        });
    }*/

    $scope.funcAsync = function (search_string) {
        
        $rootScope.simbaLoadingShow();
        $scope.loadContacts(search_string);
        
    }

    //$scope.loadRelationshipList();
    

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});

SimbaNicApp.controller('TabController', ['$scope', function($scope) {
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
}]);

SimbaNicApp.controller('closingsVisitController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, SimbaNicAPI, $filter, $injector, $ngBootbox) {

    $scope.visitForm = {};

    var $validationProvider = $injector.get('$validation');
            
    angular.extend($validationProvider, {
        validCallback: function (element){
            $(element).parents('.form-group:first').removeClass('has-error');
        },
        invalidCallback: function (element) {
            $(element).parents('.form-group:first').addClass('has-error');
        }
    });

    $scope.save = function() {

        if(!$validationProvider.checkValid)
        {
            return false;
        }
        
        var earliest_time = $scope.convertAmPmTo24Hour($scope.visitForm.earliest_time);
        var latest_time = $scope.convertAmPmTo24Hour($scope.visitForm.latest_time);

        if($scope.visitForm.visit_type == 'personal_visit')
        {
            if(latest_time < earliest_time)
            {
                $scope.earliestTimeError = true;
                return false;
            }
            else
            {
                $scope.earliestTimeError = false;
            }    
        }

        var visit_date = $filter('date')($scope.visitForm.visit_date_value, 'yyyy-MM-dd');
        earliest_time = visit_date + " " + earliest_time;
        latest_time = visit_date + " " + latest_time;

        var name_value_list = [
                {
                    "name": "name",
                    "value": $scope.$parent.closingInfo.property_address_c
                },
                {
                    "name": "visit_date_c",
                    "value": visit_date
                },
                {
                    "name": "visit_type_c",
                    "value": $scope.visitForm.visit_type
                },
                {
                    "name": "earliest_time_c",
                    "value": earliest_time
                },
                {
                    "name": "latest_time_c",
                    "value": latest_time
                },
                {
                    "name": "parent_type",
                    "value": "Opportunities"
                },
                {
                    "name": "parent_id",
                    "value": $stateParams.guid
                },
                {
                    "name": "description",
                    "value": $scope.visitForm.comments
                },
        ];

        $rootScope.simbaLoadingShow();

        SimbaNicAPI.save('Tasks', name_value_list)
            .success(function(response) {
                $rootScope.simbaLoadingHide();
                $scope.$parent.reloadTask();
                $ngBootbox.hideAll();
            });

    }

    $scope.latestTimeChange = function() {

        if($scope.visitForm.latest_time)
        {
            var earliest_time = $scope.convertAmPmTo24Hour($scope.visitForm.earliest_time);
            var latest_time = $scope.convertAmPmTo24Hour($scope.visitForm.latest_time);
            if(latest_time < earliest_time)
            {
                $scope.earliestTimeError = true;
                return false;
            }
            else
            {
                $scope.earliestTimeError = false;
            }
        }
    };

    $scope.convertAmPmTo24Hour = function(time_value) {

        var time = time_value;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if(AMPM == "PM" && hours<12) hours = hours+12;
        if(AMPM == "AM" && hours==12) hours = hours-12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if(hours<10) sHours = "0" + sHours;
        if(minutes<10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }

});
