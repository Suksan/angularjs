angular.module('SimbaNicApp').controller('ClosingsListViewController', function($rootScope, $scope, $http, $timeout, settings, $state, AuthService, NgMap, Auth, $localStorage, $window, SimbaNicAPI, $ngBootbox) {

    $scope.listView = function() {

        jQuery('.page-head').show();
        $scope.showListView = true;
        $scope.showMapView = false;
        $scope.renderClosingsMap = false;
    }

    $scope.mapView = function() {
     
        jQuery('.page-head').hide();
        $scope.showMapView = true;
        $scope.showListView = false;
        $scope.renderClosingsMap = true;
        $scope.templateMapViewUrl = "assets/app/components/closings/listView/mapView.html?timeStamp="+ new Date().getTime();
        $scope.templateMapViewMapUrl = "assets/app/components/closings/listView/map.html?timeStamp="+ new Date().getTime();
    }

    $scope.listView();

    var query = "";
    var closingView = $state.current.data.closingView ? $state.current.data.closingView : 'active';
    var status = $state.current.data.status ? $state.current.data.status : 'offer';
    var parentMenuActive = "";

    if(status == "offer")
    {
        query = "opportunity_status_c = 'offer_make_offer' OR opportunity_status_c = 'offer_out_for_signature' OR opportunity_status_c = 'offer_signed_ready_to_submit' OR opportunity_status_c = 'offer_sent_to_seller'";
        parentMenuActive = "Offers";
    }
    else if(status == "lost")
    {
        query = "opportunity_status_c = 'offer_lost_deal'";
        parentMenuActive = "Offers";
    }
    else if(status == "won")
    {
        query = "opportunity_status_c = 'closing_contingencies' OR opportunity_status_c = 'closing_inspection' OR opportunity_status_c = 'closing_post_constingencies'";
        parentMenuActive = "Closings";
    }
    else if(status == "purchase")
    {
        query = "opportunity_status_c = 'closed_purchased'";
        parentMenuActive = "Closings";
    }
    else if(status == "cancelled")
    {
        query = "opportunity_status_c = 'closing_cancel_contract'";
        parentMenuActive = "Closings";
    }
    else
    {
        query = "";
    }
    $scope.parentMenuActive = parentMenuActive;
    $scope.listQuery = query;

	$scope.hidePropertyDetail = function(e, property) {
		$scope.map.hideInfoWindow('simbaPopupInfoWindow');
	};

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    $scope.getDatetime = function() {
        return new Date().getTime();
    };

});

angular.module('SimbaNicApp').controller('mapViewController', function($rootScope, $scope, $window, NgMap, SimbaNicAPI, settings, $http) {

    $scope.map = {};
    $scope.closingsPins = [];

    var bounds = new google.maps.LatLngBounds();
    var geocoder = new google.maps.Geocoder();
    $scope.deviceWidth = $window.innerWidth;

    $scope.refreshMap = function() {

        NgMap.getMap({ id:'closingsMapView' }).then(function(map) {

            map.setCenter(bounds.getCenter());

            var listener = google.maps.event.addListener(map, "bounds_changed", function() {
                if (map.getZoom() > 16) {
                    map.setZoom(15);
                }
                google.maps.event.removeListener(listener); 
            });

            map.fitBounds(bounds);
            $scope.map = map;
            google.maps.event.trigger(map, 'resize');
        });

        var element = angular.element('[scroll-to-fixed]');
        element.trigger('update');
    }

    $scope.initClosingMapData = function(){

        $scope.closingMapInfo = {
            center: [bounds.getCenter().lat(), bounds.getCenter().lng()],
            zoom: 11,
            pins: $scope.closingsPins,
            height: $window.innerHeight - angular.element('[simba-page-container]').offset().top - 150 + "px",
        };
    }

    $scope.mapHeight = $window.innerHeight - angular.element('[simba-page-container]').offset().top - 150 + "px";

    $scope.refreshMapBound = function() {

        var count = 0;
        var pinsLength = $scope.closingsPins.length;

        angular.forEach($scope.closingsPins, function(value, key) {

            var address = value.property_address_c + ', ' + value.property_address_city_c + ', ' + value.property_address_state_c + ' ' + value.property_address_postalcode_c + ', USA';

            $rootScope.simbaLoadingShow();

            $http({
                url: settings.googleGeocodeApiURL + '?key='+ settings.googleApiKey + '&address=' + address,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
                },
            }).success(function(response) {

                $rootScope.simbaLoadingHide();
                
                if (response.status == "OK") {

                    var latlng = new google.maps.LatLng(response.results[0].geometry.location.lat, response.results[0].geometry.location.lng);
                    bounds.extend(latlng);

                    $scope.closingsPins[key]['position'] = [response.results[0].geometry.location.lat, response.results[0].geometry.location.lng];
                }
                count = count + 1;
                if(pinsLength == count)
                {
                    $scope.refreshMap();
                    $scope.initClosingMapData();
                }
            });

        });
    }

    $scope.scrollToFixedOptions = {
        marginTop: $('.page-header').outerHeight(true) + 10,
        bottom: -10
    };

    /*SimbaNicAPI.getEntryList('Opportunities', $scope.$parent.listQuery, '', 0, ["id", "property_address_c", "property_address_city_c", "property_address_state_c", "property_address_postalcode_c", "list_price_c", "bed_c", "bath_c"])

    .success(function(response) {

        var center = "Texas, 77096, USA";

        $scope.closingsPins = [];

        if(response.data)
        {
            $scope.closingsPins = response.data;
            $scope.refreshMapBound();
            //$scope.refreshMap();
        }
    });*/

    $scope.loadClosingsPins = function() {

    }

    $scope.showPropertyDetail = function(e, property, scroll_Top) {

        $scope.simbaProperty = property;

        NgMap.getMap({ id:'closingsMapView' }).then(function(map) {

            $scope.map = map;
            $scope.map.showInfoWindow ('simbaInfoWindow', property.id);
            if($scope.map.markers)
            {
                angular.forEach($scope.map.markers, function(value, key) {
                    $scope.map.markers[key].setIcon("");
                });
                $scope.map.markers[property.id].setIcon(settings.siteURL + "/public/assets/img/green_marker.png");
            }
        });

        jQuery('.map_bg_active').removeClass('bg-green');
        jQuery('.' + property.id + ' > .map_bg_active').addClass('bg-green');

        if(scroll_Top)
        {
            $window.scrollTo(0, $("." + property.id).offset().top - 200);
        }
    };

});


angular.module('SimbaNicApp').controller('mapPopupViewController', function($rootScope, $scope, $window, NgMap, SimbaNicAPI, settings, $location) {

    var popupBounds = new google.maps.LatLngBounds();

    $scope.popupDialog = true;

    $scope.popupMapHeight = $window.innerHeight - 60;

    $scope.refreshPopupMap = function() {

        NgMap.getMap({ id: 'closingMapPopup' }).then(function(map) {

            $rootScope.simbaLoadingShow();

            map.setCenter(popupBounds.getCenter());

            var listener = google.maps.event.addListener(map, "bounds_changed", function() {

                if (map.getZoom() > 16) {
                    map.setZoom(15);
                }
                google.maps.event.removeListener(listener); 

            });

            map.fitBounds(popupBounds);
            $scope.map = map;
            google.maps.event.trigger(map, 'resize');
            map.fitBounds(popupBounds);
            $rootScope.simbaLoadingHide();
        });

    }



    $scope.initClosingMapPopupData = function(){

        $scope.closingsMapPopup = {
            center: [popupBounds.getCenter().lat(), popupBounds.getCenter().lng()],
            zoom: 11,
            pins: $scope.$parent.closingsPins,
            height: $window.innerHeight - 100,
        };
    }

    $scope.refreshPopupMapBound = function() {

        var count = 0;

        var pinsLength = $scope.$parent.closingsPins.length;

        angular.forEach($scope.$parent.closingsPins, function(value, key){

            var latlng = new google.maps.LatLng(value.position[0], value.position[1]);
            popupBounds.extend(latlng);
            count = count + 1;
            if(pinsLength == count)
            {
                $scope.refreshPopupMap();
                $scope.initClosingMapPopupData();
            }
        });
    }

    $scope.refreshPopupMapBound();

    $scope.showPopupPropertyDetail = function(e, property) {

        NgMap.getMap({ id:'closingMapPopup' }).then(function(map) {

            $scope.map = map;

            $scope.map.showInfoWindow('simbaPopupInfoWindow', property.id);

            if($scope.map.markers)
            {
                angular.forEach($scope.map.markers, function(value, key) {
                    $scope.map.markers[key].setIcon("");
                });

                $scope.map.markers[property.id].setIcon(settings.siteURL + "/public/assets/img/green_marker.png");
            }

        });

        $scope.simbaPopupProperty = property;
        jQuery('.map_bg_active').removeClass('bg-green');
        jQuery('.' + property.id + ' > .map_bg_active').addClass('bg-green');
    };

    angular.element($window).bind('resize', function() {

        //$scope.refreshPopupMap();

    });

});
