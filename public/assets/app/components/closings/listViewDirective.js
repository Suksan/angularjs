SimbaNicApp.directive('ngListviewClosings', function(settings, AuthService, $rootScope, $state, $filter, $rootScope, closingsFunctions) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            if(scope.parentMenuActive)
            {
                if(scope.parentMenuActive == "Offers")
                {
                    jQuery('.closings_menu').removeClass('active open');
                }
                if(scope.parentMenuActive == "Closings")
                {
                    jQuery('.offer_menu').removeClass('active open');
                }
            }

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: false,

                "order": [
                    [0, 'asc']
                ],

                /*"columnDefs": [{
                    'orderable': false,
                    'targets': [3]
                }],*/

                "processing": true,

                "serverSide": true,

                "ajax": {
                        "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "method": "POST",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Opportunities',
                                query: scope.listQuery,
                                order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                                offset: d.start,
                                select_fields: ["id","opportunity_status_c","list_price_c","offer_price_c","property_address_c","property_address_city_c","property_address_state_c","offer_date_c","close_date_c" ],
                                link_name_to_fields_array: [],
                                max_results: d.length,
                                deleted: '0',
                                Favorites: false,
                            })
                        },
                    },
                
                "lengthMenu": $rootScope.simbaDataTable.ListLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.ListPageLength,

                "searching": false,

                /*"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",*/

                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "width": "20%",
                        "render" : function (data,type,row){
                            return "<a href='#/Closings/" + data.id + "'>"+ data.property_address_c +"</a>";
                        },
                        "name": "property_address_c" 
                    },
                    { 
                        "data": "property_address_city_c",
                        "width": "10%",
                        "name": "property_address_city_c"
                    },
                    { 
                        "data": "property_address_state_c",
                        "width": "10%",
                        "name": "property_address_state_c"
                    },
                    { 
                        "data": null,
                        "width": "15%",
                        "render" : function (data, type, row){
                            return closingsFunctions.getClosingsStatus(data.opportunity_status_c);
                        },
                        "name": "opportunity_status_c"
                    },
                    { 
                        "data": null,
                        "width": "10%",
                        "render" : function (data,type,row){
                            return $filter('currency')(data.list_price_c, '$', 0);
                        },
                        "name": "list_price_c"
                    },
                    {
                        "data": null,
                        "width": "10%",
                        "render" : function (data, type, row) {
                            return $filter('currency')(data.offer_price_c, '$', 0);
                        },
                        "name" : "offer_price_c"
                    },
                    { 
                        "data": "offer_date_c",
                        "width": "13%",
                        "name": "offer_date_c"
                    },
                    { 
                        "data": "close_date_c",
                        "width": "12%",
                        "name": "close_date_c", 
                    },
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width", "200px");
                    $(nRow).children('td:eq(1)').css("min-width", "150px");
                    $(nRow).children('td:eq(2)').css("min-width", "150px");
                    $(nRow).children('td:eq(3)').css("min-width", "150px");
                    $(nRow).children('td:eq(4)').css("min-width", "150px");
                    $(nRow).children('td:eq(5)').css("min-width", "150px");
                    $(nRow).children('td:eq(6)').css("min-width", "150px");
                    $(nRow).children('td:eq(6)').css("min-width", "150px");
                    
                    return nRow;
                },
                "fnDrawCallback":function() {

                    jQuery('.listview_closings').parent('.dataTables_scrollBody').addClass('overflow_x_scroll');

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  
                    {
                        $('#listview_closings_wrapper .dataTables_paginate').css("display", "block"); 
                    }
                    else
                    {
                        $('#listview_closings_wrapper .dataTables_paginate').css("display", "none");
                    }
                }
            });
        }
    };
});

SimbaNicApp.directive('ngMaplistviewClosings', function(settings, $stateParams, AuthService, $filter, $rootScope, $window, closingsFunctions, $ngBootbox) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            scope.sort_by = "offer_date_c DESC";
            scope.deviceWidth = $window.innerWidth;
            
            if(!scope.listQuery)
            {
                return false;
            }
            var language = $rootScope.simbaDataTable.language;
            language.info = "_START_ to _END_ of _TOTAL_";

            var oTable = elem.dataTable({
            
                "language": language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "processing": true,
                "serverSide": true,
                "ordering": false,
                "searching": false,

                ajax: {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "data": function ( d ) {
                        d.method = "get_entry_list";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = JSON.stringify({
                            session: AuthService.getSessionId(),
                            module_name: 'Opportunities',
                            query: scope.listQuery,
                            order_by: scope.sort_by,
                            offset: d.start,
                            select_fields: ["id","opportunity_status_c","description","list_price_c","offer_price_c","property_address_c","property_address_city_c","property_address_state_c","bed_c","bath_c","sq_ft_c","offer_date_c","close_date_c" ],
                            link_name_to_fields_array: [],
                            max_results: d.length,
                            deleted: '0',
                            Favorites: false,
                        });
                    },
                },
                
                "lengthMenu": $rootScope.simbaDataTable.ListLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.ListPageLength,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data, type, row) {

                            var opportunity_status_c =  closingsFunctions.getClosingsStatus(data.opportunity_status_c);

                            if(!data.description)
                            {
                                data.description = "";
                            }

                            var popupMapDisplay = "";


                            if(scope.deviceWidth < 768)
                            {
                                popupMapDisplay = '<span class="icon-map closing_mapview_responsive_icon" aria-hidden="true"></span>';
                            }

                        return '<div class="row margin-bottom-10 '+ data.id +'">\
                                    <div class="col-md-12 map_bg_active">\
                                        <div class="simba_list_mapview" style="background: url(\'\\' + settings.imgPath + '/property1.jpg\')">\
                                                <div class="simba_inline_mapview">\
                                                    <div class="simba_mapview_status text-right">\
                                                        <span class="pull-left">Status: '+ opportunity_status_c +'</span>\
                                                        <span class="icon-map closing_mapview_responsive_icon visible-xs" aria-hidden="true" data-property-id="'+ data.id +'"></span>\
                                                    </div>\
                                                    <div class="simba_mapview_price">\
                                                        <div class="row">\
                                                            <div class="col-md-6 col-sm-6 col-xs-6">\
                                                                List: ' + $filter('currency')(data.list_price_c, '$', 0) + '\
                                                            </div>\
                                                            <div class="col-md-6 col-sm-6 col-xs-6">\
                                                                Offer: ' + $filter('currency')(data.offer_price_c, '$', 0) + '\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="simba_mapview_info">\
                                                        <div class="row">\
                                                            <div style="white-space: normal;" class="col-md-6 col-sm-6 col-xs-6">\
                                                            <a href="#/Closings/'+ data.id +'">\
                                                            '+ 
                                                            data.property_address_c + ' ' +
                                                            data.property_address_city_c + ', ' +
                                                            data.property_address_state_c +
                                                            '</a>\
                                                            </div>\
                                                            <div class="col-md-1 col-sm-1 col-xs-1 simba_mapview_bed text-center">\
                                                                <div> ' 
                                                                + data.bed_c + 
                                                                '</div>\
                                                                <div>Bd</div>\
                                                            </div>\
                                                            <div class="col-md-2 col-sm-2 col-xs-2 text-center">\
                                                                <div> ' 
                                                                + data.bath_c + 
                                                                '</div>\
                                                                <div>Ba</div>\
                                                            </div>\
                                                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">\
                                                                <div>'+ data.sq_ft_c +'</div>\
                                                                <div>Sq. Ft.</div>\
                                                            </div>\
                                                        </div>\
                                                    <div class="row simba_mapview_date_info">\
                                                        <div class="col-md-6 col-sm-6 col-xs-6">\
                                                            <div>\
                                                                Offer: ' + data.offer_date_c + '\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-6 col-sm-6 col-xs-6">\
                                                            <div class="text-right">\
                                                                Close: ' + data.close_date_c + '\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>';
                        },
                        "width": "100%",
                    },
                ],
                "fnDrawCallback":function() {

                    if(this.fnGetData().length > 0)
                    {
                        scope.$parent.closingsPins = this.fnGetData();
                        scope.refreshMapBound();
                    }

                    jQuery('.map_list_paginate').html(jQuery('#maplistview_closings_info').text());
                    jQuery('#maplistview_closings_info').hide();

                    jQuery('.closing_mapview_responsive_icon').on('click', function(e) {

                        var property_id = jQuery(this).attr('data-property-id');

                        if(scope.closingsPins)
                        {
                            angular.forEach(scope.closingsPins, function (value, key) {
                                if(value.id == property_id)
                                {
                                    scope.activePopupProperty = value;
                                }
                            });
                        }

                        scope.activePopupPropertyId = property_id;
                        
                        $ngBootbox.customDialog({
                            templateUrl: 'assets/app/components/closings/listView/popup.html?id='+ property_id +'&timestamp=' + scope.getDatetime(),
                            size: 'large',
                            closeButton: false,
                            backdrop: true,
                            className: 'simbanic_closings_map_popup',
                            scope: scope,
                        });
                        
                    });

                    
                    jQuery('#maplistview_closings tbody td').hover(function(e) {

                        if(scope.deviceWidth >= 768)
                        {
                            var showMarker = false;
                            var sData, sPos;
                            
                            if(oTable.fnGetPosition(this))
                            {
                                sPos = oTable.fnGetPosition(this);
                            }
                            else
                            {
                                return false;
                            }
                            if(oTable.fnGetData(sPos[0]))
                            {
                                sData = oTable.fnGetData( sPos[0] );
                                showMarker = true;
                            }
                            else
                            {
                                return false;
                            }
                            if(showMarker)
                            {
                                scope.showPropertyDetail(e, sData);
                            }
                        }
                    });
                    

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  
                    {
                        $('#maplistview_closings_wrapper .dataTables_paginate').css("display", "block");
                        $('#maplistview_closings_wrapper .dataTables_length').css("display", "block");
                        $('#maplistview_closings_wrapper .dataTables_filter').css("display", "block");                    
                    } 
                    else 
                    {
                        $('#maplistview_closings_wrapper .dataTables_paginate').css("display", "none");
                        $('#maplistview_closings_wrapper .dataTables_length').css("display", "none");
                        $('#maplistview_closings_wrapper .dataTables_filter').css("display", "none");
                    }
                }
            });

           

            scope.listSortBy = function() {
                elem.dataTable()._fnAjaxUpdate();
            };

            jQuery('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        }
    };
});