SimbaNicApp.directive('ngGmap', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            var map = new GMaps({
                div: '#gmap_basic',
                lat: -12.043333,
                lng: -77.028333
            });
        }
    };
});

SimbaNicApp.directive('visitDatePicker', function($timeout, $parse){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            parsed = $parse(attrs.datepicker);

            elem.datepicker({
                orientation: "right",
                autoclose: true,
                startDate: new Date(),
                onSelect: function(dateText, inst) {
                    scope.$apply(function(){
                        //parsed.assign(scope, dateText);
                    });
                }
            });
            elem.datepicker().on('changeDate', function(e) {
                //alert(elem.datepicker('getDate'));
                scope.visitForm.visit_date_value = elem.datepicker('getDate');
                
                /*scope.$apply(function() {
                    elem.val(elem.val());
                    scope.visitForm.visit_date_value = elem.datepicker('getDate');
                });*/

            });

            scope.visitDatePicker = function() {
                elem.triggerHandler('focus');
            };

            

        }
    }
})

SimbaNicApp.directive('earliestTimePicker', function($timeout, $filter){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.timepicker({
                autoclose: true,
                showSeconds: false,
                minuteStep: 30,
                defaultTime: false,
                //defaultTime: $filter('date')(new Date(new Date().getTime() + 30*60000), 'hh:mm a')

            });

            elem.timepicker().on('show.timepicker', function(e) {
                if (typeof(scope.visitForm.earliest_time) === 'undefined')
                {
                    elem.timepicker('setTime', '12:00 PM');    
                }
            });

            scope.earliestTimePicker = function() {
                $timeout(function() {
                    elem.triggerHandler('click');
                }, 100);
            };
        }
    }
})

SimbaNicApp.directive('latestTimePicker', function($timeout, $filter){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.timepicker({
                autoclose: true,
                showSeconds: false,
                minuteStep: 30,
                defaultTime: false,
                //defaultTime: $filter('date')(new Date(new Date().getTime() + 60*60000), 'hh:mm a')
            });

            elem.timepicker().on('show.timepicker', function(e) {
                if (typeof(scope.visitForm.latest_time) === 'undefined')
                {
                    elem.timepicker('setTime', '01:00 PM');    
                }
            });

            scope.latestTimePicker = function() {
                $timeout(function() {
                    elem.triggerHandler('click');
                }, 100);
            };

        }
    }
})

SimbaNicApp.directive('ngSubpanelDocuments', function(settings, $stateParams, AuthService, $rootScope, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: false,

                "order": [
                    [2, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "method": "POST",
                    "data": function ( d ) {
                        d.method = "get_simba_relationships";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = JSON.stringify({
                            session: AuthService.getSessionId(),
                            module_name: 'Opportunities',
                            module_id: $stateParams.guid,
                            link_field_name: 'documents',
                            related_module_query: '',
                            related_fields: ["id", "name", "filename", "template_type", "date_entered", "status_id", "active_date", "category_id", "created_by_name", "assigned_user_id"],
                            related_module_link_name_to_fields_array: [],
                            deleted: '0',
                            order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                            offset: d.start,
                            limit: -1,
                        });
                    },
                },
                
                "lengthMenu": $rootScope.simbaDataTable.subpanelLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.subpanelPageLength,

                "pagingType": "simple",

                /*"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",*/
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                columnDefs: [ {
                    className: 'control',
                    orderable: false,
                    targets:   0
                },{
                    orderable: false,
                    targets:   1
                },{
                    orderable: false,
                    targets:   3
                } ],

                "columns": [
                    { 
                        "data": null,
                        "width": "20%",
                        "render" : function (data,type,row){
                            if(data.assigned_user_id == AuthService.getUserId())
                            {
                                return "<button class='btn btn-outline red-mint btn-small document_delete_btn' \
                                        data-toggle='confirmation' \
                                        data-id = '" + data.id + "'\
                                        >Remove</button>";    
                            }
                            else
                            {
                                return "";
                            }
                            
                        },
                    },
                    { 
                        "data": null,
                        "width": "30%",
                        "render" : function (data,type,row){
                            return "<a href='"+ settings.crmURL +"/index.php?entryPoint=download&id="+ data.id +"&type=Documents' target='_blank'>"+ data.name +"</a>";
                        },
                        "name": "name"
                    },
                    /*{ "data": "filename" },
                    { "data": "template_type" },
                    { "data": "date_entered" },
                    { "data": "status_id" },*/
                    { 
                        "data": "active_date",
                        "width": "25%",
                        "name": "active_date"
                    },
                    { 
                        "data": "created_by_name",
                        "width": "25%",
                        "name": "created_by_name",
                    },
                ],
                "fnDrawCallback":function(){

                    $('[data-toggle="confirmation"]').confirmation({
                        btnOkClass: 'btn-success',
                        btnCancelClass: 'btn-danger',
                        singleton: true,
                        popout: true,
                        container: 'body'
                    });

                    $('[data-toggle="confirmation"]').on('confirmed.bs.confirmation', function () {
                        $rootScope.simbaLoadingShow();

                        SimbaNicAPI.setRelationship('Opportunities', $stateParams.guid, 'documents', [jQuery(this).attr("data-id")], [], 1)
                            .success(function(response) {

                                $rootScope.simbaLoadingHide();
                                scope.reloadDocument();

                            });
                    });

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#subpanel_documents_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#subpanel_documents_wrapper .dataTables_length').css("display", "block");
                        $('#subpanel_documents_wrapper .dataTables_filter').css("display", "block");                       
                    } 
                    else {
                        $('#subpanel_documents_wrapper .dataTables_paginate').css("display", "none");
                        $('#subpanel_documents_wrapper .dataTables_length').css("display", "none");
                        $('#subpanel_documents_wrapper .dataTables_filter').css("display", "none");
                    }
                }
                
            });

            scope.reloadDocument = function() {
                elem.dataTable()._fnAjaxUpdate();
            };
        }
    };
});

SimbaNicApp.directive('ngSubpanelContacts', function(settings, $stateParams, AuthService, $rootScope, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: false,

                "order": [
                    [1, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "method": "POST",
                    "data": function ( d ) {
                        d.method = "get_simba_relationships";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = JSON.stringify({
                            session: AuthService.getSessionId(),
                            module_name: 'Opportunities',
                            module_id: $stateParams.guid,
                            link_field_name: 'contacts',
                            related_module_query: "",
                            related_fields: ["id", "first_name", "last_name", "name", "company_name_c", "email1", "phone_work", "phone_mobile", "title", "primary_address_street",  "description", "assigned_user_id"],
                            related_module_link_name_to_fields_array: [],
                            deleted: '0',
                            order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                            offset: d.start,
                            limit: d.length,
                        });
                    },
                },
                
                "lengthMenu": $rootScope.simbaDataTable.subpanelLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.subpanelPageLength,

                "pagingType": "simple",

                //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                columnDefs: [ {
                    className: 'control',
                    orderable: false,
                    targets:   0
                },{
                    'orderable': false,
                    'targets': [4]
                } ],

                "columns": [
                    { 
                        "data": null,
                        "width": "20%",
                        "render" : function (data,type,row){

                            return "<button class='btn btn-outline red-mint btn-small contact_remove_btn' \
                                    data-toggle='confirmation' \
                                    data-id = '" + data.id + "'\
                                    >Remove</button>";
                        },
                    },
                    {
                        "data": null,
                        "width": "30%",
                        "name": "first_name",
                        "render" : function (data,type,row){
                            return "<a href='#/Contacts/" + data.id + "' target='_blank'>"+ data.first_name + ' ' + data.last_name +"</a>";
                        },
                    },
                    { 
                        "data": "company_name_c",
                        "name": "company_name_c",
                        "width": "20%", 
                    },
                    { 
                        "data": "phone_work",
                        "name": "phone_work",
                        "width": "20%", 
                    },
                    { 
                        "data": "email1",
                        "name": "email1",
                        "width": "30%",
                    },
                    { 
                        "data": "phone_mobile",
                        "name": "phone_mobile" 
                    },
                    { 
                        "data": "title",
                        "name": "title"  
                    },
                    { 
                        "data": "description" ,
                        "name": "description"  
                    },
                ],
                "fnDrawCallback":function(){

                    $('[data-toggle="confirmation"]').confirmation({
                        btnOkClass: 'btn-success',
                        btnCancelClass: 'btn-danger',
                        singleton: true,
                        popout: true,
                        container: 'body'
                    });

                    $('[data-toggle="confirmation"]').on('confirmed.bs.confirmation', function () {
                        $rootScope.simbaLoadingShow();

                        SimbaNicAPI.setRelationship('Opportunities', $stateParams.guid, 'contacts', [jQuery(this).attr("data-id")], [], 1)
                            .success(function(response) {

                                console.log(response);

                                $rootScope.simbaLoadingHide();
                                scope.reloadContact();

                            });
                    });

                    if(!jQuery('#subpanel_contacts_filter input[type="search"]').val())
                    {
                        if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                            $('#subpanel_contacts_wrapper .dataTables_paginate').css("display", "block"); 
                            $('#subpanel_contacts_wrapper .dataTables_length').css("display", "block");
                            $('#subpanel_contacts_wrapper .dataTables_filter').css("display", "none");                       
                         } else {
                            $('#subpanel_contacts_wrapper .dataTables_paginate').css("display", "none");
                            $('#subpanel_contacts_wrapper .dataTables_length').css("display", "none");
                            $('#subpanel_contacts_wrapper .dataTables_filter').css("display", "none");
                        }
                    }

                   
                }

            });

            scope.reloadContact = function() {
                elem.dataTable()._fnAjaxUpdate();
            };
        }
    };
});

SimbaNicApp.directive('ngSubpanelHistory', function(settings, $stateParams, AuthService, $rootScope) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            var restHistoryData = {
                session: AuthService.getSessionId(),
                module_name: 'Opportunities',
                id: $stateParams.guid,
            }
            var restHistoryDataJson = JSON.stringify(restHistoryData);

            elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [0, 'asc']
                ],

                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "data": function ( d ) {
                        d.method = "get_simba_activities";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = restHistoryDataJson
                    },
                },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],
                
                "pageLength": 5,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            if(!data.description)
                            {
                                data.description = "";
                            }
                            return '<div class="general-item-list">\
                                            <div class="item">\
                                                <div class="item-head">\
                                                    <div class="item-details">\
                                                        <img class="item-pic rounded" src="public/assets/img/avatar9.jpg">\
                                                        <a href="#" class="item-name primary-link"> ' 
                                                        + data.name +
                                                        '</a>\
                                                        <span class="item-label">3 hrs ago</span>\
                                                    </div>\
                                                    <span class="item-status">\
                                                        <span class="badge badge-empty badge-success"></span> ' + data.status + '</span>\
                                                </div>\
                                                <div class="item-body"> '
                                                    + data.description + '\
                                                </div>\
                                            </div>';
                        },
                        "width": "100%",
                    },
                ],
                "fnDrawCallback":function() {
                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                             $('#subpanel_history_wrapper .dataTables_paginate').css("display", "block"); 
                             $('#subpanel_history_wrapper .dataTables_length').css("display", "block");
                             $('#subpanel_history_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                             $('#subpanel_history_wrapper .dataTables_paginate').css("display", "none");
                             $('#subpanel_history_wrapper .dataTables_length').css("display", "none");
                             $('#subpanel_history_wrapper .dataTables_filter').css("display", "none");
                     }
                }

            });
        }
    };
});

SimbaNicApp.directive('ngSubpanelTasks', function(settings, $stateParams, AuthService, $rootScope, $filter, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var table = elem;

            var oTable = table.dataTable({

                "language": $rootScope.simbaDataTable.language,

                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                }],

                "order": [
                    [2, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "method": "POST",
                    "data": function ( d ) {
                        d.method = "get_simba_relationships";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = JSON.stringify({
                            session: AuthService.getSessionId(),
                            module_name: 'Opportunities',
                            module_id: $stateParams.guid,
                            link_field_name: 'tasks',
                            related_module_query: "tasks.status != 'Completed' AND tasks.assigned_user_id != 0",
                            related_fields: ["id","name","status","date_due","assigned_user_name"],
                            related_module_link_name_to_fields_array: [],
                            deleted: '0',
                            order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                            offset: d.start,
                            limit: d.length,
                        });
                    },
                },

                "lengthMenu": $rootScope.simbaDataTable.subpanelLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.subpanelPageLength,

                "pagingType": "simple",
                
                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data, type, row){
                            return '<div class="md-checkbox" >\
                                    <input type="checkbox" id="'+ data.id +'" class="md-check task_incomplete" >\
                                    <label for="'+ data.id +'">\
                                        <span></span>\
                                        <span class="check" ></span>\
                                        <span class="box"></span> </label>\
                                </div>';
                        },
                        "width": "3%"
                    },
                    { 
                        "data": null,
                        "name": "name",
                        "width": "40%",
                        "render" : function (data, type, row) {
                            return "<a href='#/Tasks/"+ data.id +"?return_action=Closings&return_id="+$stateParams.guid+"'>"+ data.name +"</a>";
                        },
                    },
                    { 
                        "data": null,
                        "name": "date_due",
                        "render" : function (data, type, row) {
                            
                            if(data.date_due)
                            {
                                var date = new Date(data.date_due.replace(/-/g,"/"));
                                return $filter('date')(date, 'MM/dd/yyyy hh:mm a');
                            }
                            else
                            {
                                return '';
                            }

                            
                        },
                        "width": "27%"
                    },
                    { 
                        "data": "assigned_user_name",
                        "name": "assigned_user_name",
                        "width": "30%" 
                    },

                ],
                "fnDrawCallback":function(){

                    jQuery(".task_incomplete").change(function() {

                        if(jQuery(this).is(':checked'))
                        {
                            //if(confirm("Are you sure you want to Confirm this task?"))
                            //{
                                $rootScope.simbaLoadingShow();

                                var name_value_list = [
                                    {
                                        "name": "id",
                                        "value": jQuery(this).attr("id"),
                                    },
                                    {
                                        "name": "status",
                                        "value": "Completed"
                                    }
                                ];

                                SimbaNicAPI.save('Tasks', name_value_list)
                                    .success(function(response) {
                                        scope.reloadTask();
                                        $rootScope.simbaLoadingHide();
                                })
                            //}
                        }

                    });

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#subpanel_tasks_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#subpanel_tasks_wrapper .dataTables_length').css("display", "block");
                        $('#subpanel_tasks_wrapper .dataTables_filter').css("display", "block");                       
                    } else {
                        $('#subpanel_tasks_wrapper .dataTables_paginate').css("display", "none");
                        $('#subpanel_tasks_wrapper .dataTables_length').css("display", "none");
                        $('#subpanel_tasks_wrapper .dataTables_filter').css("display", "none");
                    }
                }
                
            });

            scope.reloadTask = function() {
                elem.dataTable()._fnAjaxUpdate();
            };

            jQuery('#subpanel_tasks_wrapper .row:nth-child(2) div:nth-child(2)').html("<a class='pull-right' href='#/Tasks'>See All Tasks <i class='fa fa-arrow-circle-o-right'></i></a>");
        }
    };
});
