angular.module('SimbaNicApp').controller('TaskDetailViewController', function($rootScope, $scope, $http, $timeout, Auth, AuthService, $stateParams, settings, $filter, $location, Upload, SimbaNicUploadService, SimbaNicAPI, $window) {

    $scope.editView = false;
    $scope.createView = false;
    
    if($location.search().edit)
    {
        $scope.editView = true;
    }

    if($location.search().create)
    {
        $scope.editView = true;
        $scope.createView = true;
    }

    $scope.backURL = false;

    if($location.search().return_action)
    {
        if($location.search().return_id)
        {
            $scope.backURL = true;
            $scope.return_action = '#/' + $location.search().return_action + '/' + $location.search().return_id;
        }
        else
        {
            $scope.return_action = '#/' + $location.search().return_action;    
        }
        
    }
    else
    {
        $scope.return_action = '#/Tasks';
    }

    $scope.task = {};

    if($location.search().address)
    {
        $scope.property_address_c = $location.search().address;
        $scope.selectedPropertyAddress = { property_address_c: $location.search().address, id: $location.search().guid };
    }
    if($location.search().guid)
    {
        $scope.task.parent_id = $location.search().guid;
    }

    $scope.guid = $stateParams.guid;

    

    $scope.getEntry = function() {

        if($stateParams.guid != 0)
        {
            $rootScope.simbaLoadingShow();

            SimbaNicAPI.getEntry('Tasks', $stateParams.guid)
                .success(function(response) {

                    if(typeof(response.data) === 'undefined')
                    {
                        $scope.getEntry();
                    }
                    else
                    {
                        $scope.task = response.data[0];

                        if(response.data[0].date_due)
                        {
                            var date = new Date(response.data[0].date_due.replace(/-/g,"/"));
                            $scope.task.date_due = $filter('date')(date, 'yyyy/MM/dd');
                            $scope.task.date_due_time = $filter('date')(date, 'hh:mm a');
                            /*if($scope.task.date_due_time == "12:00 AM")
                            {
                                $scope.task.date_due_time = "";
                            }*/
                        }
                        
                        if(response.data[0].property_address)
                        {
                            $scope.task.property_address_c = response.data[0].property_address;
                            $scope.property_address_c = $scope.task.property_address_c;
                            $scope.selectedPropertyAddress = { property_address_c: $scope.property_address_c };
                        }
                        
                        $rootScope.simbaLoadingHide();
                    }
                });
        }
    }

    $scope.getEntry();

    $scope.formatDate = function(date) {
        var dateOut = new Date(date);
        return dateOut;
    };

    $scope.remoteUrlRequestFn = function(str) 
    {
        return {
            method : "simba_search_by_module",
            input_type : "JSON",
            response_type : "JSON",
            rest_data : {
                session: AuthService.getSessionId(),
                search_string: str,
                modules: ["Opportunities"],
                offset: 0,
                max_results: 5,
                assigned_user_id: '',
                select_fields: ["id", "property_address_c"],
                unified_search_only: false,
                favorites: false,
                search_field: ["property_address_c"],
            },
        }; 
    }

    $scope.createUploadFiles = function(files, errFiles) {

        $scope.noteFiles = files;
        console.log(files);

        angular.forEach(files, function(file) {

            Upload.dataUrl(file).then(function(urls){
            });

        });

        $scope.$watch('noteFiles', function() {
            if ($scope.noteFiles != null) {
            }
        });

    }

    $scope.removeNoteItem = function(index) {
        $scope.noteFiles.splice(index, 1);
    }

    $scope.uploadFiles = function(files, errFiles) {

        if(!$scope.task.name)
        {
            return false;
        }

        $scope.files = files;
        $scope.errFiles = errFiles;

        angular.forEach(files, function(file) {

            $rootScope.simbaLoadingShow();

            $scope.upload(file, $stateParams.guid, true);

        });
    }

    $scope.upload = function (file, parent_id, reload) {

        $rootScope.simbaLoadingShow();

        var name_value_list = [
            {
                "name": "name",
                "value": $scope.property_address_c ? $scope.property_address_c : $scope.task.name
            },
            {
                "name": "file_mime_type",
                "value": file.type
            },
            {
                "name": "filename",
                "value": file.name
            },
            {
                "name": "parent_type",
                "value": "Tasks"
            },
            {
                "name": "parent_id",
                "value": parent_id
            },
        ];

        SimbaNicAPI.save('Notes', name_value_list)
            .success(function(response) {
                
                SimbaNicUploadService.upload(file, 'Notes', response.id, 'filename', 0).then(function (response) {

                        $rootScope.simbaLoadingHide();
                        if(reload)
                        {
                            if($location.search().guid)
                            {
                                $window.location.href = '#/Closings/' + $location.search().guid;
                                return false;
                            }
                            else if($scope.createView)
                            {
                                $window.location.href = '#/Tasks/' + $scope.task.id;
                                return false;
                            }
                            else
                            {
                                $scope.reloadNotes();
                            }
                        }

                }, function (response) {

                }, function (evt) {
                    
                });

        }).error(function(error) {
            
        });
    };

    $scope.convertAmPmTo24Hour = function(time_value) {

        var time = time_value;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if(AMPM == "PM" && hours<12) hours = hours+12;
        if(AMPM == "AM" && hours==12) hours = hours-12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if(hours<10) sHours = "0" + sHours;
        if(minutes<10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }

    $scope.dueDateTimePicker = function() {
        jQuery('.timepicker').triggerHandler('click');
    };

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});