SimbaNicApp.directive('ngListviewTasks', function(settings, AuthService, $rootScope, $state, $filter, $http, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            scope.filter1 = 'incomplete';
            scope.filter2 = 'my_tasks';

            scope.filterQuery = "";

            scope.getFilterQuery = function() {

                var query = "";

                if(scope.filter1 == "incomplete")
                {
                    if(query)
                    {
                        query = query + " AND tasks.status != 'Completed' ";
                    }
                    else
                    {
                        query = " tasks.status != 'Completed' ";
                    }
                }
                else if(scope.filter1 == "complete")
                {
                    if(query)
                    {
                        query = query + " AND tasks.status = 'Completed' ";
                    }
                    else
                    {
                        query = " tasks.status = 'Completed' ";
                    }
                }
                else if(scope.filter1 == "all_status")
                {
                    if(query)
                    {
                        query = query + " AND tasks.status != '' ";
                    }
                    else
                    {
                        query = " tasks.status != '' ";
                    }
                }

                if(scope.filter2 == "all_user_tasks")
                {
                    if(query)
                    {
                        query = query + " AND tasks.assigned_user_id != 0 ";
                    }
                    else
                    {
                        query = " tasks.assigned_user_id != 0 ";
                    }
                }
                else if(scope.filter2 == "my_tasks")
                {
                    if(query)
                    {
                        query = query + " AND tasks.assigned_user_id = '" + AuthService.getUserId() + "' ";
                    }
                    else
                    {
                        query = " tasks.assigned_user_id = '" + AuthService.getUserId() + "'";
                    }
                }
                
                scope.filterQuery = query;
                console.log(scope.filterQuery);
            };

            scope.getFilterQuery();

            elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },
                
                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                },
                {
                    'orderable': false,
                    'targets': [6]
                },
                {
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [3, 'asc']
                ],
                "processing": true,
                "serverSide": true,
                "ajax": {
                        "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Tasks',
                                query: scope.filterQuery + " AND tasks.name LIKE '%" + d.search['value'] + "%' ",
                                order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                                offset: d.start,
                                select_fields: ["id", "name", "status", "date_due", "assigned_user_name", "parent_id", "property_address_c"],
                                link_name_to_fields_array: [],
                                max_results: d.length,
                                deleted: '0',
                                Favorites: false,
                            });
                        },
                    },
                
                "lengthMenu": [
                    [10, 15, 20],
                    [10, 15, 20]
                ],
                
                "pageLength": 10,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "width": "5%",
                        "render" : function (data, type, row) {
                            if(data.status)
                            {
                                if(data.status != 'Completed')
                                {
                                    return '<div class="md-checkbox" >\
                                        <input type="checkbox" id="'+ data.id +'" class="md-check task_incomplete" >\
                                        <label for="'+ data.id +'">\
                                            <span></span>\
                                            <span class="check" ></span>\
                                            <span class="box"></span> </label>\
                                    </div>';
                                    /*return "<div class='checker'><span class=''><input type='checkbox' class='checkboxes task_incomplete' ng-click='taskChecked()' id='" + data.id + "' value='" + data.id + "' /></span></div>";*/
                                }
                                else
                                {
                                    return "";
                                }
                            }
                            else
                            {
                                return "";
                            }
                        },
                    },
                    { 
                        "data": null,
                        "name": 'name',
                        "width": "25%",
                        "render" : function (data,type,row){
                            return "<a href='#/Tasks/" + data.id + "'>"+ data.name +"</a>";
                        }, 
                    },
                    { 
                        "data": null,
                        "name": 'status',
                        "width": "10%",
                        "render" : function (data, type, row) {
                            if(data.status)
                            {
                                if(data.status != 'Completed')
                                {
                                    return "Incomplete";
                                }
                                else
                                {
                                    return "Complete";
                                }
                            }
                            else
                            {
                                return "";
                            }
                        },
                    },
                    {
                        "data": null,
                        "name": 'date_due',
                        "width": "10%",
                        "render" : function (data, type, row) {
                            var date = new Date(data.date_due.replace(/-/g,"/"));
                            return $filter('date')(date, 'MM/dd/yyyy hh:mm a');
                        },
                    },
                    {
                        "data": "assigned_user_name",
                        "name": 'assigned_user_name',
                        "width": "15%" 
                    },
                    { 
                        "data": null,
                        "width": "20%",
                        "name": "property_address_c",
                        "render" : function (data,type,row){
                            return "<a href='#/Closings/" + data.parent_id + "'>"+ data.property_address_c +"</a>";
                        }, 
                    },
                    {
                        "data": null,
                        "width": "15%",
                        "render" : function (data, type, row) {
                            return "<div class='btn-group btn-group-circle'><a href='#/Tasks/" + data.id + "?edit=true' class='btn btn-outline green-sharp btn-small task_edit_btn'>Edit</a><button class='btn btn-outline red-mint btn-small task_delete_btn' \
                                        data-toggle='confirmation' \
                                        data-id = '" + data.id + "'\
                                        >Delete</button></div>";
                        },
                    },
                ],
                "fnDrawCallback":function() {

                    $('[data-toggle="confirmation"]').confirmation({
                        btnOkClass: 'btn-success',
                        btnCancelClass: 'btn-danger',
                        singleton: true,
                        popout: true,
                        container: 'body'
                    });

                    $('[data-toggle="confirmation"]').on('confirmed.bs.confirmation', function () {

                        $rootScope.simbaLoadingShow();

                        SimbaNicAPI.delete('Tasks', jQuery(this).attr("data-id"))
                            .success(function(response) {
                                scope.listFilterBy();
                                $rootScope.simbaLoadingHide();
                        })

                    });

                    jQuery(".task_incomplete").change(function() {

                        if(jQuery(this).is(':checked'))
                        {
                            //if(confirm("Are you sure you want to Confirm this task?"))
                            //{
                                $rootScope.simbaLoadingShow();

                                var name_value_list = [
                                    {
                                        "name": "id",
                                        "value": jQuery(this).attr("id"),
                                    },
                                    {
                                        "name": "status",
                                        "value": "Completed"
                                    }
                                ];

                                SimbaNicAPI.save('Tasks', name_value_list)
                                    .success(function(response) {
                                        scope.listFilterBy();
                                        $rootScope.simbaLoadingHide();
                                })
                            //}
                        }

                    });

                    if(!jQuery('#listview_tasks_filter input[type="search"]').val())
                    {
                        if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                            $('#listview_tasks_wrapper .dataTables_paginate').css("display", "block"); 
                            $('#listview_tasks_wrapper .dataTables_length').css("display", "block");
                            $('#listview_tasks_wrapper .dataTables_filter').css("display", "block");                       
                        } else {
                            $('#listview_tasks_wrapper .dataTables_paginate').css("display", "none");
                            $('#listview_tasks_wrapper .dataTables_length').css("display", "none");
                            $('#listview_tasks_wrapper .dataTables_filter').css("display", "none");
                        }
                    }
                    
                }
            });

            elem.find('.group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).prop("checked", true);
                        $(this).parents('tr').addClass("active");
                    } else {
                        $(this).prop("checked", false);
                        $(this).parents('tr').removeClass("active");
                    }
                });
                jQuery.uniform.update(set);
            });

            elem.on('change', 'tbody tr .checkboxes', function () {
                $(this).parents('tr').toggleClass("active");
            });
            


            scope.listFilterBy = function(status) {
                scope.getFilterQuery();
                elem.dataTable()._fnAjaxUpdate();
            };

        }
    };
});