SimbaNicApp.directive('ngListviewTasks', function(settings, AuthService, $rootScope, $state, $filter, $http) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            scope.filter_by = "tasks.status != 'Completed'";

            elem.dataTable({
            
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "Nothing here yet",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "_MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },
                "columnDefs": [{  // set default column settings
                    'orderable': false,
                    'targets': [0]
                }, {
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [1, 'asc']
                ],

                "ajax": {
                        "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Tasks',
                                query: scope.filter_by,
                                order_by: "",
                                offset: "0",
                                select_fields: [],
                                link_name_to_fields_array: [],
                                max_results: -1,
                                deleted: '0',
                                Favorites: false,
                            });
                        },
                    },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],
                
                "pageLength": 5,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "width": "5%",
                        "render" : function (data, type, row) {
                            if(data.status)
                            {
                                if(data.status != 'Completed')
                                {
                                    return "<div class='checker'><span class=''><input type='checkbox' class='checkboxes task_incomplete' ng-click='taskChecked()' id='" + data.id + "' value='" + data.id + "' /></span></div> ";
                                }
                                else
                                {
                                    return "";
                                }
                            }
                            else
                            {
                                return "";
                            }
                        },
                    },
                    { 
                        "data": null,
                        "width": "20%",
                        "render" : function (data,type,row){
                            return "<a href='#/Task/" + data.id + "'>"+ data.name +"</a>";
                        }, 
                    },
                    { 
                        "data": "status",
                        "width": "15%"
                    },
                    {
                        "data": null,
                        "width": "10%",
                        "render" : function (data, type, row) {
                            var date = new Date(data.date_due.replace(/-/g,"/"));
                            return $filter('date')(date, 'MM/dd/yyyy');
                        },
                    },
                    { 
                        "data": "assigned_user_name",
                        "width": "15%" 
                    },
                    { 
                        "data": null,
                        "width": "15%",
                        "render" : function (data, type, row){
                            return "";
                        }, 
                    },
                    {
                        "data": null,
                        "width": "20%",
                        "render" : function (data, type, row) {
                            return "<button href='' class='btn btn-outline green-sharp btn-small border-radius-left-top-bottom task_edit_btn' \
                                         \
                                        >Edit</button><button class='btn btn-outline red-mint btn-small border-radius-right-top-bottom task_delete_btn' \
                                        data-toggle='confirmation' \
                                        data-id = '" + data.id + "'\
                                        >Delete</button>";
                        }, 
                    },
                    
                ],
                "fnDrawCallback":function() {

                    $('[data-toggle="confirmation"]').confirmation({
                        btnOkClass: 'btn-success',
                        btnCancelClass: 'btn-danger',
                        singleton: true,
                        popout: true
                    });

                    $('[data-toggle="confirmation"]').on('confirmed.bs.confirmation', function () {

                        var param = {
                            session: AuthService.getSessionId(),
                            module_name: 'Tasks',
                            name_value_list: [
                                [
                                    {
                                        "name": "id",
                                        "value": jQuery(this).attr("data-id"),
                                    },
                                    {
                                        "name": "deleted",
                                        "value": "1"
                                    }
                                ],
                            ],
                        }
                        var restJson = JSON.stringify(param);

                        var restData = jQuery.param({
                            method : "set_entries",
                            input_type : "JSON",
                            response_type : "JSON",
                            rest_data : restJson 
                        });

                        $http({
                            url: settings.apiURL + 'service/v4_1/rest.php',
                            method: 'POST',
                            dataType: 'JSON',
                            data: restData,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
                            },
                        }).success(function(response){
                            scope.listFilterBy();
                        }).error(function(error) {
                            scope.error = error;
                        });

                    });

                    $('[data-toggle="confirmation"]').on('canceled.bs.confirmation', function () {
                        alert('No');
                    });

                    jQuery(".task_incomplete").change(function() {

                        if(jQuery(this).is(':checked'))
                        {
                            if(confirm("Are you sure you want to Confirm this task?"))
                            {
                                var param = {
                                    session: AuthService.getSessionId(),
                                    module_name: 'Tasks',
                                    name_value_list: [
                                        [
                                            {
                                                "name": "id",
                                                "value": jQuery(this).attr("id"),
                                            },
                                            {
                                                "name": "status",
                                                "value": "Completed"
                                            }
                                        ],
                                    ],
                                }
                                var restJson = JSON.stringify(param);

                                var restData = jQuery.param({
                                    method : "set_entries",
                                    input_type : "JSON",
                                    response_type : "JSON",
                                    rest_data : restJson 
                                });

                                $http({
                                    url: settings.apiURL + 'service/v4_1/rest.php',
                                    method: 'POST',
                                    dataType: 'JSON',
                                    data: restData,
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
                                    },
                                }).success(function(response){
                                    scope.listFilterBy();
                                }).error(function(error) {
                                    scope.error = error;
                                });
                            }
                        }

                    });
                        


                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                             $('#listview_tasks_wrapper .dataTables_paginate').css("display", "block"); 
                             $('#listview_tasks_wrapper .dataTables_length').css("display", "block");
                             $('#listview_tasks_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                             $('#listview_tasks_wrapper .dataTables_paginate').css("display", "none");
                             $('#listview_tasks_wrapper .dataTables_length').css("display", "none");
                             $('#listview_tasks_wrapper .dataTables_filter').css("display", "none");
                     }
                }
            });

            elem.find('.group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).prop("checked", true);
                        $(this).parents('tr').addClass("active");
                    } else {
                        $(this).prop("checked", false);
                        $(this).parents('tr').removeClass("active");
                    }
                });
                jQuery.uniform.update(set);
            });

            elem.on('change', 'tbody tr .checkboxes', function () {
                $(this).parents('tr').toggleClass("active");
            });

            jQuery('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });

            scope.listFilterBy = function(){
                elem.dataTable()._fnAjaxUpdate();
            };

            scope.taskChecked = function(){
                alert("Hi");
                return false;
                if(confirm("Are you sure you want to Confirm this task?"))
                {
                    alert("Yes");
                }
                else
                {
                    alert("No");
                }
                return false;
                var param = {
                    session: AuthService.getSessionId(),
                    module_name: 'Tasks',
                    name_value_list: [
                        [
                            {
                                "name": "id",
                                "value": "2020308f-52de-3c0b-b50e-56cd8d8ed055"
                            },
                            {
                                "name": "status",
                                "value": "Completed"
                            }
                        ],
                    ],
                }
                var restJson = JSON.stringify(param);

                var restData = jQuery.param({
                    method : "set_entries",
                    input_type : "JSON",
                    response_type : "JSON",
                    rest_data : restJson 
                });

                $http({
                    url: settings.apiURL + 'service/v4_1/rest.php',
                    method: 'POST',
                    dataType: 'JSON',
                    data: restData,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
                    },
                }).success(function(response){
                    scope.listFilterBy();
                }).error(function(error) {
                    scope.error = error;
                });

            }

        }
    };
});