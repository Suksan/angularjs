angular.module('SimbaNicApp').controller('DriversController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
	// $scope.simbaLoading = true;

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.driverViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/drivers/detailview/driverView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_driver_dialog',
            scope: $scope
        });
    }
    $scope.driverAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/drivers/driverAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_driver_dialog',
            scope: $scope
        });
    }
});
SimbaNicApp.controller('driverViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getDriver(id) {
         // alert("get location");
         CgseqService.getDriver(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load truck data: ' + error.message;
            });
    }
    $scope.editDriver = function (db) {
        // alert('yes');
       
        $params = $.param({
            "id": db.id,
            "first_name": db.first_name,
            "last_name": db.last_name,
            "nickname": db.nickname,
            "driver_licence_number": db.driver_licence_number,
            "national_id": db.national_id,
            "phone_number": db.phone_number

        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveDriver(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getDriver($scope.selected_id);


});
SimbaNicApp.controller('driverAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initDriverAdd(id) {
          // alert("get driver" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            first_name : "",
            last_name : "",
            nickname : "",
            driver_licence_number : "",
            national_id : "",
            phone_number : ""

        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editDriver = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.driver_obj = {id: record.id, first_name: record.first_name,last_name: record.last_name, nickname: record.nickname,driver_licence_number: record.driver_licence_number, national_id: record.national_id, phone_number: record.phone_number};

        $params = $.param({
            "id": $scope.driver_obj.id,
            "first_name": $scope.driver_obj.first_name,
            "last_name": $scope.driver_obj.last_name,
            "nickname": $scope.driver_obj.nickname,
            "driver_licence_number": $scope.driver_obj.driver_licence_number,
            "national_id": $scope.driver_obj.national_id,
            "phone_number": $scope.driver_obj.phone_number

        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddDriver($scope.driver_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initDriverAdd($scope.selected_id);


});