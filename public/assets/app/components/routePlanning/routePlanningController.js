angular.module('SimbaNicApp').controller('RoutePlanningController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

     $scope.routePlanningViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/routePlanning/detailview/routePlanningView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_routePlanning_dialog',
            scope: $scope
        });
    }

});
SimbaNicApp.controller('routePlanningViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getRoutePlanning(id) {
         // alert("get location");
         CgseqService.getRoutePlanning(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load truck data: ' + error.message;
            });
    }
    
  
      
    
    
    getRoutePlanning($scope.selected_id);


});