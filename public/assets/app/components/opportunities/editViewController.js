angular.module('SimbaNicApp').controller('opportunityEditViewController', function($rootScope, $scope, $http, $timeout, settings, $window, FileUploader, Flash, $injector, StateServices) {

	$scope.opportunityEditForm = {};
	$scope.opportunityEditForm.folder_name = "";
    $scope.opportunityEditForm.state = "";
	$scope.simbaAllUpload = 0;

	var uploader = $scope.uploader = new FileUploader();

	$scope.uploadLocation = function(){
        $scope.opportunityEditForm.folder_name = "";
	}

    uploader.onSuccessItem = function(item, response, status, headers) {

    	//alert(uploader.getNotUploadedItems().length);

    	$scope.simbaAllUpload = $scope.simbaAllUpload + 1;
        
        if($scope.simbaAllUpload == uploader.queue.length)
        {
        	//var message = 'Thank you for uploading files, a message will be sent when processing is complete.';
            var message = 'Your properties have been uploaded';
        	Flash.create('success', message, 0, {class: 'simba-flash'}, true);
        	$timeout(function() {
		    	$window.location.href = '#/Opportunities/Edit';
        		$window.location.reload();
		    }, 5000);
        }
    };

    $scope.stateList = StateServices.get();

    var $validationProvider = $injector.get('$validation');
            
    angular.extend($validationProvider, {
        validCallback: function (element){
            $(element).parents('.form-group:first').removeClass('has-error');
        },
        invalidCallback: function (element) {
            $(element).parents('.form-group:first').addClass('has-error');
        }
    });

    $scope.saveCloud = function() {
    	uploader.uploadAll();
    }

    $scope.singleSave = function() {
        var message = 'Your properties have been uploaded';
        Flash.create('success', message, 0, {class: 'simba-flash'}, true);
        $timeout(function() {
            $window.location.href = '#/Opportunities/Edit';
            $window.location.reload();
        }, 5000);
    }

    /*uploader.removeFromQueue = function(){

    }*/

    /*uploader.onCompleteAll = function() {
  		var message = '<strong>Well done!</strong> You successfully read this important alert message.';
        var id = Flash.create('success', message, 0, {class: 'simba-flash', id: 'custom-id'}, true);
    };*/

})