angular.module('SimbaNicApp').controller('userLockController', ['$rootScope', '$http', '$scope', 'settings', 'AuthService', '$window', '$state', '$ngBootbox', function($rootScope, $http, $scope, settings, AuthService, $window, $state, $ngBootbox) {

	$ngBootbox.hideAll();

	AuthService.setSimbaTimeOut(true);

	$scope.lockForm = {};
	$scope.lockForm.password = "";

	if(!AuthService.getCurrentUser())
	{
		$window.location.href = '#/login.html';
		$window.location.reload();
		return false;
	}

	$scope.simbaLock = function() {
	
    	var params = {
    		user_auth:{
		        user_name: $rootScope.loginBean.currentUser.name_value_list.user_name.value,
		        password: $scope.lockForm.password,
		        encryption: 'PLAIN'
		    },
		    application: 'RestGreenlet'
		}
		var json = JSON.stringify(params);

	    var loginParam = jQuery.param({
	    	method : "login",
		    input_type : "JSON",
		    response_type : "JSON",
		    rest_data : json	
	    });

		$http({
		    url: settings.apiURL + 'service/v4_1/rest.php',
		    method: 'POST',
		    dataType: 'JSON',
		    data: loginParam,
		    headers: {
		        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
		    },
		}).success(function(response) {
			if(response.id)
			{
				AuthService.setStatus(true);
				AuthService.setSimbaTimeOut(false);
				AuthService.setCurrentUser(response);
				AuthService.setSessionId(response.id);

				$rootScope.loginBean = {};
			    $rootScope.loginBean.loggedIn = AuthService.getStatus();
			    $rootScope.loginBean.currentUser = AuthService
			            .getCurrentUser();
	            $rootScope.loginBean.simbaTimeOut = AuthService
			            .setSimbaTimeOut();;

				$window.location.href = '#/dashboard.html';
				$window.location.reload();
			}
			else
			{
				alert('Please Enter Correct Password');
			}
		}).error(function(error){
		    
		});
	}

}]);