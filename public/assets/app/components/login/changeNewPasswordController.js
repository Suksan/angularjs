angular.module('SimbaNicApp').controller('changeNewPasswordController', ['$rootScope', '$http', '$scope', 'settings', 'AuthService', '$window', '$state', '$location', function($rootScope, $http, $scope, settings, AuthService, $window, $state, $location) {

	AuthService.setStatus(false);
	AuthService.setSimbaTimeOut(false);

	$rootScope.loginBean = {};
    $rootScope.loginBean.loggedIn = AuthService.getStatus();
    $rootScope.loginBean.simbaTimeOut = AuthService.getSimbaTimeOut();

	$scope.changeNewPasswordForm = {};
	$scope.changeNewPasswordForm.userName = "";
	$scope.changeNewPasswordForm.newPassword = "";
	$scope.changeNewPasswordForm.guid = $location.search().guid;

	$scope.simbaChangeNewPassword = function() {

	    var param = jQuery.param({
	    	method : "change_new_password",
		    input_type : "JSON",
		    response_type : "JSON",
		    guid : $scope.changeNewPasswordForm.guid,
		    user_name : $scope.changeNewPasswordForm.userName,
		    new_password : $scope.changeNewPasswordForm.newPassword,
	    });

		$http({
		    url: settings.apiURL + 'service/v4_1/rest.php',
		    method: 'POST',
		    dataType: 'JSON',
		    data: param,
		    headers: {
		        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
		    },
		}).success(function(response) {
			if(response.success)
			{
				$window.location.href = '#/login.html';
				$state.reload();
			}
			else
			{
				alert(response.message);
			}
		}).error(function(error){
		    
		});
	}

}]);