angular.module('SimbaNicApp').controller('UsersController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
	// $scope.simbaLoading = true;

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.userViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/users/detailview/userView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_user_dialog',
            scope: $scope
        });
    }
    $scope.userAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/users/userAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_user_dialog',
            scope: $scope
        });
    }
});
SimbaNicApp.controller('userViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getUser(id) {
         // alert("get location");
         CgseqService.getUser(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load truck data: ' + error.message;
            });
    }
    $scope.editUser = function (db) {
        // alert('yes');
       
        $params = $.param({
            "id": db.id,
            "first_name": db.first_name,
            "last_name": db.last_name,
            "nickname": db.nickname,
            "department": db.department,
            "employee_id": db.employee_id,
            "role": db.role,
            "login_name": db.login_name,
            "password": db.password,
            "email": db.email,
            "phone_number": db.phone_number

        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveUser(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getUser($scope.selected_id);


});
SimbaNicApp.controller('userAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initUserAdd(id) {
          // alert("get user" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            first_name : "",
            last_name : "",
            nickname : "",
            department : "",
            employee_id : "",
            role : "",
            login_name : "",
            password : "",
            email : "",
            phone_number : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editUser = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.user_obj = {id: record.id, first_name: record.first_name, last_name: record.last_name, nickname: record.nickname, department: record.department, employee_id: record.employee_id, role: record.role, login_name: record.login_name, password: record.password, email: record.email, phone_number: record.phone_number};

        $params = $.param({
            "id": $scope.user_obj.id,
            "first_name": $scope.user_obj.first_name,
            "last_name": $scope.user_obj.last_name,
            "nickname": $scope.user_obj.nickname,
            "department": $scope.user_obj.department,
            "employee_id": $scope.user_obj.employee_id,
            "role": $scope.user_obj.role,
            "login_name": $scope.user_obj.login_name,
            "password": $scope.user_obj.password,
            "email": $scope.user_obj.email,
            "phone_number": $scope.user_obj.phone_number
        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddUser($scope.user_obj.id, $params).success(function(response) {
            alert('Record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initUserAdd($scope.selected_id);


});