SimbaNicApp.filter('ordinal', function() {

	// alert('jam maew');
	return function(number) {

    // Ensure that the passed in data is a number
	    if(isNaN(number) || number < 1) {
	
	      // If the data is not a number or is less than one (thus not having a cardinal value) return it unmodified.
	      return number;
	
	    } else {
	
	      // If the data we are applying the filter to is a number, perform the actions to check it's ordinal suffix and apply it.
	
	      var lastDigit = number % 10;
	
	      if(lastDigit === 1) {
	        return number + 'st'
	      } else if(lastDigit === 2) {
	        return number + 'nd'
	      } else if (lastDigit === 3) {
	        return number + 'rd'
	      } else if (lastDigit > 3) {
	        return number + 'th'
	      }
	
	    }
  	}
});

SimbaNicApp.filter('subdivisionFilter', function() {
	// alert('jam maew');
	return function(subdivision,section_number) {

    // Ensure that the passed in data is a number
       if((section_number) && (section_number !== '-')) {
        return subdivision + ' ' + section_number
       } else {
	    return subdivision
	   }
  	}
});

SimbaNicApp.filter('garageFilter', function() {
	// alert('jam maew');
	return function(garage_attach,garage_capacity) {
		// alert(garage_attach);
    // Ensure that the passed in data is a number
		if ((garage_capacity) && (garage_capacity !== '-')) {
			
			if (garage_attach === 'Yes') {
				return "Attached" + ', ' + garage_capacity + ' spaces'
			} else if (garage_attach === 'No') {
				return "Detached" + ', ' + garage_capacity + ' spaces'
			} else {
				return  garage_capacity + ' spaces'
			}
		} else {
			if (garage_attach === 'Yes') {
				return "Attached" 
			} else if (garage_attach === 'No') {
				return "Detached" 
			} else {
				return  '-'

			}
	   	}
  	}
});

SimbaNicApp.filter('domFilter', function() {
	// alert('jam maew');
	return function(dom) {
		// alert(garage_attach);
    // Ensure that the passed in data is a number
		if (angular.isUndefined(dom) || dom === null  || dom === '-') {
			
			return dom
		} else {
			if (dom <= 1) {
				return dom + ' ' + 'day'
			} else {
				return dom + ' ' + 'days'
			}
	   	}
  	}
});


SimbaNicApp.filter('rangeFilter', function() {
	// alert('jam maew');
	return function(min,max) {
		// alert(garage_attach);
    // Ensure that the passed in data is a number
		var str_min = '';
		var str_max = '';

		if(min && (min !== '-')) {
			str_min = min
		} else {
			str_min = 'n/a'
		}

		if(max && (max !== '-')) {
			str_max = max
		} else {
			str_max = 'n/a'
		}

		if (min || max) {
			return str_min + ' - ' + str_max
		} else {
			return '-'
		}

  }
});


SimbaNicApp.filter('doubleRangeFilter', function() {
	// alert('jam maew');
	return function(min,max,subMin,subMax) {
		// alert(garage_attach);
    // Ensure that the passed in data is a number
		var strMin = '';
		var strMax = '';
		var strSubMin = '';
		var strSubMax = '';
		var strSub = '';

		if(min && (min !== '-')) {
			strMin = min
		} else {
			strMin = 'n/a'
		}

		if(max && (max !== '-')) {
			strMax = max
		} else {
			strMax = 'n/a'
		}

		if(subMin && (subMin !== '-')) {
			strSubMin = subMin
		} else {
			strSubMin = 'n/a'
		}

		if(subMax && (subMax !== '-')) {
			strSubMax = subMax
		} else {
			strSubMax = 'n/a'
		}

		if ((strSubMin  || strSubMax ) && ((strSubMin !== 'n/a') && (strSubMax !== 'n/a')) ) {
			strSub =  strSubMin + ' - ' + strSubMax
		} else {
			strSub = '-'
		}

		if ((strMin  || strMax )  &&  ((strMin !== 'n/a') && (strMax !== 'n/a')) ) {
			// alert(min);
			// alert(max);
			if (strSub && strSub !== '-') {
				return strMin + ' - ' + strMax + ' (' + strSub + ')'
		  } else {
		  	return strMin + ' - ' + strMax 
		  }
		} else {
			if (strSub && strSub !== '-') {
				return '-' + ' (' + strSub + ')'
			} else {
				return '-' 
			}
		}

  }
});