SimbaNicApp.directive('ngListviewReports', function(settings, AuthService, $rootScope, $state, $filter, $http, $stateParams, $location, $window) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {


            var grid = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [1, 'asc']
                ],
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "http://localhost/theme/metronic/theme/admin_4_angularjs/demo/table_ajax.php",
                },
                "bStateSave": true,
                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"]
                ],
                
                "pageLength": 10,

                "dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                
                "fnDrawCallback":function() {
                    
                    
                }

            });

            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
                var action = $(".table-group-action-input", grid.getTableWrapper());
                if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.val());
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                } else if (action.val() == "") {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                } else if (grid.getSelectedRowsCount() === 0) {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            });

            grid.setAjaxParam("customActionType", "group_action");
            grid.getDataTable().ajax.reload();
            grid.clearAjaxParams();
        }
    };
});