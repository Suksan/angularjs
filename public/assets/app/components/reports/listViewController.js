angular.module('SimbaNicApp').controller('ReportsListViewController', function($rootScope, $scope, $http, $timeout, Auth, AuthService) {

    $scope.simbaLoading = true;
	
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});