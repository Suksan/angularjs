	   SimbaNicApp.directive('ngSimbaTrucks', function(settings, $rootScope, $state, $filter, $rootScope) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            
            
            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: false,

                scrollX: true,

                autoWidth: false,

             

                "order": [
                    [0, 'asc']
                ],

                "processing": true,

                "serverSide": true,
                "ajax": {
                        // "url": "client/markets/10/properties/search_opportunities",
                        // "url": "assets/app/components/trucks/trucks.json",
                        "url": "http://localhost:8080/api/trucks",
                        
                    },
                
                "lengthMenu": [
                    [20, 30, 40, 50],
                    [20, 30, 40, 50]
                ],
                    "lengthMenu": $rootScope.simbaDataTable.ListLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.ListPageLength,
                
                "searching": false,
                
                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [     
       //            { 
                      //      "data": null,
                      //      "width": "4%",
                      //      "render" : function (data,type,row){
                      //       return "<a href='#/Opportunities/" + data.id + "'>"+ data.street_address +"</a>";
                      //   },
                      // },   
                        {"data":  "id"              , "width": "1.8%"},
                        {"data":  "licence_number"  , "width": "1.8%"},
                        {"data":  "make"            , "width": "1.8%"},
                        {"data":  "model"           , "width": "1.8%"},
                        {"data":  "colour"          , "width": "1.8%"},
                        {"data":  "type"            , "width": "1.8%"},
                        {"data":  "year"            , "width": "1.8%"},
                        {"data":  "mileage"         , "width": "1.8%"}

                    
                    // { 
                     // "data": "golf_course",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "gated_community",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "rental_output_cap_rate",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "comparables_avm_sale",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "comparables_cdom_sale",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "comparables_cdom_rental",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "rental_income_monthly_gross_rent",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "rental_output_max_purchase_amount",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "sale_sold_count",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "rental_sold_count",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "latitude",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "category",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "longitude",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "state",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "lot_square_feet",
                     // "width": "1.8%" 
                    // }, 
                    // { 
                     // "data": "listing_date",
                     // "width": "1.8%" 
                    // },           
                     // { 
                        // "data": null,
                        // "width": "4%",
                        // "render" : function (data,type,row){
                            // return "<a href='#/Opportunities/" + data.id + "'>"+ data.street_address +"</a>";
                        // }, 
                    // },
                    // { 
                        // "data": "city",
                        // "width": "6%"
                    // },
                    // { 
                        // "data": "state",
                        // "width": "6%" 
                    // },
                    // { 
                        // "data": null,
                        // "width": "6%",
                        // "render" : function (data,type,row){
                            // return $filter('currency')(data.list_price, '$', 0);
                        // }, 
                    // },
                    // {
                        // "data": null,
                        // "width": "6%",
                        // "render" : function (data, type, row) {
                            // return $filter('currency')(data.offer_price, '$', 0);
                        // }, 
                    // },
                    // { 
                        // "data": "offer_date",
                        // "width": "4%" 
                    // },
                    // { 
                        // "data": "closed_date",
                        // "width": "4%" 
                    // },
                    // { 
                        // "data": "mls_number",
                        // "width": "3%" 
                    // },
                    // { 
                        // "data": "tag",
                        // "width": "4%" 
                    // },
                    // { 
                        // "data": "comparables_avm_sale",
                        // "width": "4%" 
                    // },
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width",  "200px");
                    $(nRow).children('td:eq(1)').css("min-width",  "150px");
                    $(nRow).children('td:eq(2)').css("min-width",  "150px");
                    $(nRow).children('td:eq(3)').css("min-width",  "150px");
                    $(nRow).children('td:eq(4)').css("min-width",  "150px");
                    $(nRow).children('td:eq(5)').css("min-width",  "150px");
                    $(nRow).children('td:eq(6)').css("min-width",  "150px");
                    $(nRow).children('td:eq(7)').css("min-width",  "150px");
                    $(nRow).children('td:eq(8)').css("min-width",  "150px");
                    $(nRow).children('td:eq(9)').css("min-width",  "150px");
                    $(nRow).children('td:eq(10)').css("min-width", "150px");
                    $(nRow).children('td:eq(11)').css("min-width", "150px");
                    $(nRow).children('td:eq(12)').css("min-width", "150px");
                    $(nRow).children('td:eq(13)').css("min-width", "150px");
                    $(nRow).children('td:eq(14)').css("min-width", "150px");
                    $(nRow).children('td:eq(15)').css("min-width", "150px");
                    $(nRow).children('td:eq(16)').css("min-width", "150px");
                    $(nRow).children('td:eq(17)').css("min-width", "150px");
                    $(nRow).children('td:eq(18)').css("min-width", "150px");
                    $(nRow).children('td:eq(19)').css("min-width", "150px");
                    $(nRow).children('td:eq(20)').css("min-width", "150px");
                    $(nRow).children('td:eq(21)').css("min-width", "150px");
                    $(nRow).children('td:eq(22)').css("min-width", "150px");
                    $(nRow).children('td:eq(23)').css("min-width", "150px");
                    $(nRow).children('td:eq(24)').css("min-width", "150px");
                    $(nRow).children('td:eq(25)').css("min-width", "150px");
                    $(nRow).children('td:eq(26)').css("min-width", "150px");
                    $(nRow).children('td:eq(27)').css("min-width", "150px");
                    $(nRow).children('td:eq(28)').css("min-width", "150px");
                    $(nRow).children('td:eq(29)').css("min-width", "150px");
                    $(nRow).children('td:eq(30)').css("min-width", "150px");
                    $(nRow).children('td:eq(31)').css("min-width", "150px");
                    $(nRow).children('td:eq(32)').css("min-width", "150px");
                    $(nRow).children('td:eq(33)').css("min-width", "150px");
                    $(nRow).children('td:eq(34)').css("min-width", "150px");
                    $(nRow).children('td:eq(35)').css("min-width", "150px");
                    $(nRow).children('td:eq(36)').css("min-width", "150px");
                    $(nRow).children('td:eq(37)').css("min-width", "150px");
                    $(nRow).children('td:eq(38)').css("min-width", "150px");


                    
                    return nRow;
                },
                "fnDrawCallback":function() {

                    jQuery('.listview_opportunities').parent('.dataTables_scrollBody').addClass('overflow_x_scroll');

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  
                    {
                        $('#listview_opportunities_wrapper .dataTables_paginate').css("display", "block"); 
                    }
                    else
                    {
                        $('#listview_opportunities_wrapper .dataTables_paginate').css("display", "none");
                    }
                }
            });
        }
    };
});

SimbaNicApp.directive('ngMaplistviewOpportunities', function(settings, $stateParams, $filter, $rootScope, $window, $ngBootbox) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            scope.deviceWidth = $window.innerWidth;
            
            var language = $rootScope.simbaDataTable.language;
            language.info = "_START_ to _END_ of _TOTAL_";

            var oTable = elem.dataTable({
            
                "language": language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "processing": true,
                "serverSide": true,
                "ordering": false,

                ajax: {
                    "url": "/client/markets/10/properties/search_opportunities",
                },
                
                "lengthMenu": $rootScope.simbaDataTable.ListLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.ListPageLength,

                "searching": false,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data, type, row) {

                            if(!data.description)
                            {
                                data.description = "";
                            }

                        return '<div class="row margin-bottom-10 '+ data.id +'">\
                                    <div class="col-md-12 map_bg_active">\
                                        <div class="simba_list_mapview" style="background: url(\'' + settings.imgPath + '/property1.jpg\')">\
                                                <div class="simba_inline_mapview">\
                                                    <div class="simba_mapview_status text-right">\
                                                        <span class="pull-left">Status: </span>\
                                                    </div>\
                                                    <div class="simba_mapview_price">\
                                                        <div class="row">\
                                                            <div class="col-md-6 col-sm-6 col-xs-6">\
                                                                List: ' + $filter('currency')(data.list_price, '$', 0) + '\
                                                            </div>\
                                                            <div class="col-md-6 col-sm-6 col-xs-6">\
                                                                Offer: ' + $filter('currency')(data.offer_price, '$', 0) + '\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    <div class="simba_mapview_info">\
                                                        <div class="row">\
                                                            <div style="white-space: normal;" class="col-md-6 col-sm-6 col-xs-6">\
                                                            <a href="#/Opportunities/'+ data.id +'">\
                                                            '+ 
                                                            data.street_address + ' ' +
                                                            data.city + ', ' +
                                                            data.state +
                                                            '</a>\
                                                            </div>\
                                                            <div class="col-md-1 col-sm-1 col-xs-1 simba_mapview_bed text-center">\
                                                                <div> ' 
                                                                + data.beds + 
                                                                '</div>\
                                                                <div>Bd</div>\
                                                            </div>\
                                                            <div class="col-md-2 col-sm-2 col-xs-2 text-center">\
                                                                <div> ' 
                                                                + data.bathrooms + 
                                                                '</div>\
                                                                <div>Ba</div>\
                                                            </div>\
                                                            <div class="col-md-3 col-sm-3 col-xs-3 text-center">\
                                                                <div>'+ data.square_feet +'</div>\
                                                                <div>Sq. Ft.</div>\
                                                            </div>\
                                                        </div>\
                                                    <div class="row simba_mapview_date_info">\
                                                        <div class="col-md-6 col-sm-6 col-xs-6">\
                                                            <div>\
                                                                Offer: ' + data.offer_date + '\
                                                            </div>\
                                                        </div>\
                                                        <div class="col-md-6 col-sm-6 col-xs-6">\
                                                            <div class="text-right">\
                                                                Close: ' + data.closed_date + '\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>';
                        },
                        "width": "100%",
                    },
                ],
                "fnDrawCallback":function() {

                    jQuery('.map_list_paginate').html(jQuery('#maplistview_opportunities_info').text());
                    jQuery('#maplistview_opportunities_info').hide();

                    jQuery('.closing_mapview_responsive_icon').on('click', function(e) {

                        var property_id = jQuery(this).attr('data-property-id');

                        if(scope.closingsPins)
                        {
                            angular.forEach(scope.closingsPins, function (value, key) {
                                if(value.id == property_id)
                                {
                                    scope.activePopupProperty = value;
                                }
                            });
                        }

                        scope.activePopupPropertyId = property_id;
                        
                        /*$ngBootbox.customDialog({
                            templateUrl: 'app/components/closings/listView/popup.html?id='+ property_id +'&timestamp=' + scope.getDatetime(),
                            size: 'large',
                            closeButton: false,
                            backdrop: true,
                            className: 'simbanic_closings_map_popup',
                            scope: scope,
                        });*/
                        
                    });

                    
                    jQuery('#maplistview_opportunities tbody td').hover(function(e) {

                        if(scope.deviceWidth >= 768)
                        {
                            var showMarker = false;
                            var sData, sPos;
                            
                            if(oTable.fnGetPosition(this))
                            {
                                sPos = oTable.fnGetPosition(this);
                            }
                            else
                            {
                                return false;
                            }
                            if(oTable.fnGetData(sPos[0]))
                            {
                                sData = oTable.fnGetData( sPos[0] );
                                showMarker = true;
                            }
                            else
                            {
                                return false;
                            }
                            if(showMarker)
                            {
                                scope.showPropertyDetail(e, sData);
                            }
                        }
                    });

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  
                    {
                        $('#maplistview_opportunities_wrapper .dataTables_paginate').css("display", "block");
                        $('#maplistview_opportunities_wrapper .dataTables_length').css("display", "none");
                        $('#maplistview_opportunities_wrapper .dataTables_filter').css("display", "none");
                    } 
                    else 
                    {
                        $('#maplistview_opportunities_wrapper .dataTables_paginate').css("display", "none");
                        $('#maplistview_opportunities_wrapper .dataTables_length').css("display", "none");
                        $('#maplistview_opportunities_wrapper .dataTables_filter').css("display", "none");
                    }
                }
            });

            scope.listSortBy = function() {
                elem.dataTable()._fnAjaxUpdate();
            };

            jQuery('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        }
    };
});





