angular.module('SimbaNicApp').controller('ContactDetailViewController', function($rootScope, $scope, $http, $timeout, Auth, AuthService, $stateParams, settings, $filter, $location, StateServices, SimbaNicAPI, $window, $anchorScroll) {

    $("tr").removeClass('simba_row_active');
    jQuery("div[data-id='"+ $stateParams.guid +"']").parent('td').parent('tr').addClass("simba_row_active");

    $scope.deviceWidth = $window.innerWidth;

    angular.element($window).bind('resize', function(){
        $scope.$apply(function(){
            $scope.deviceWidth = $window.innerWidth;    
        })
    });

    $scope.editView = false;
    $scope.isCreate = false;

    $scope.contact = {};

    $scope.$watch('editView', function() {
        $scope.$parent.editView = $scope.editView;
    });
    $scope.$watch('isCreate', function() {
        $scope.$parent.isCreate = $scope.isCreate;
    });
    $scope.$watch('isSaveBtnDisabled', function() {
        $scope.$parent.isSaveBtnDisabled = $scope.isSaveBtnDisabled;
    });
    $scope.$watch('isCancelBtnDisabled', function() {
        $scope.$parent.isCancelBtnDisabled = $scope.isCancelBtnDisabled;
    });

    if($location.search().edit)
    {
        $scope.editView = true;
    }

    $scope.gotoListView = function() {
        $location.hash('contact_list_view');
        $anchorScroll();
    }

    $scope.stateList = StateServices.get();

    $scope.getEntry = function() {
        if($stateParams.guid != 0)
        {
            $rootScope.simbaLoadingShow();
            SimbaNicAPI.getEntry('Contacts', $stateParams.guid)
                .success(function(response) {

                    if(typeof(response.data) === 'undefined')
                    {
                        $scope.getEntry();
                    }
                    else
                    {
                        $scope.contact = response.data[0];
                        $scope.getContactProfileURL();
                        $rootScope.simbaLoadingHide();
                        if($scope.deviceWidth <= 768)
                        {
                            $location.hash('contact_detail_view');
                            $anchorScroll();
                        }
                    }

                });
        }
    }

    $scope.getEntry();
	
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        //App.initAjax();
    });

    $scope.$on('create', function(e) {  
        $scope.create();
    });

    $scope.$on('edit', function(e) {  
        $scope.edit();
    });

    $scope.$on('save', function(e) {
        $scope.save();
    });

    $scope.$on('cancel', function(e) {  
        $scope.cancel();
    });

    $scope.getContactProfileURL = function() {
        $scope.contact.photoURL = $rootScope.avatarImagePath;

        if($scope.contact.photo)
        {
            $scope.contact.photoURL = settings.crmURL + "index.php?entryPoint=download&id="+ $scope.contact.id +"_photo&type=Contacts&timestamp=" + new Date().getTime();
        }
    }

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});
