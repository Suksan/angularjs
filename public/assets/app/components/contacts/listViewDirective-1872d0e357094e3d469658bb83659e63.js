SimbaNicApp.directive('ngListviewContacts', function(settings, AuthService, $rootScope, $state, $filter, $http, $stateParams, $location, $window) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [0, 'asc']
                ],
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Contacts',
                                query: "contacts.first_name LIKE '%" + d.search['value'] + "%' OR contacts.last_name LIKE '%" + d.search['value'] + "%'",
                                order_by: "first_name ASC",
                                offset: d.start,
                                select_fields: ['id', 'first_name', 'last_name', 'photo'],
                                link_name_to_fields_array: [],
                                max_results: d.length,
                                deleted: '0',
                                Favorites: false,
                            });
                        },
                },
                
                "lengthMenu": [
                    [20, 50, 100],
                    [20, 50, 100]
                ],
                
                "scrollY":        "400px",
                "scrollCollapse": true,
                //"scroller":       true,
                "scrollInfinite": true,
                "deferRender":    true,
                "pagingType": "simple",
                "pageLength": 20,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            if($location.path() == "/Contacts")
                            {
                                //$window.location.href = '#/Contacts/'+data.id;
                            }
                            var photo = $rootScope.avatarImagePath;
                            if(data.photo)
                            {
                                photo = settings.crmURL + 'index.php?entryPoint=download&id='+ data.id +'_photo&type=Contacts';
                            }
                            return '<div class="general-item-list" data-id="'+ data.id +'" ui-sref-active="active">\
                                            <div class="item">\
                                                <div class="item-head">\
                                                    <div class="item-details">\
                                                        <img class="item-pic rounded simba_contact_profile" src="'+ photo + '" style="width: 35px;">\
                                                        <a href="#/Contacts/'+ data.id +'" class="item-name primary-link simba_contact_name"> ' 
                                                        + data.first_name + ' ' + data.last_name
                                                        '</a>\
                                                    </div>\
                                                </div>\
                                            </div>';
                        },
                        "width": "100%",
                    },
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    
                    /*if($stateParams.guid)
                    {
                        if($stateParams.guid == aData.id)
                        {
                            $(nRow).addClass("simba_row_active");
                        }
                    }
                    return nRow;*/
                    if(!$stateParams.guid)
                    {
                        if(iDisplayIndex == 0)
                        {
                            $(nRow).addClass("simba_row_active");
                            $window.location.href = '#/Contacts/' + aData.id;
                            return false;
                        }
                    }
                },
                /*"fnPageChange('next')": function () {
                    alert("Hiii");
                    
                },*/
                "fnInitComplete":function() {
                    
                    /*$('.dataTables_scrollBody').on('scroll', function() {
                        var position, scroll_height, table_settings;
                        table_settings = elem.fnSettings();
                        if(!table_settings.bDrawing && (table_settings._iDisplayStart + table_settings._iDisplayLength) < table_settings.fnRecordsDisplay() && $(window).scrollTop() !== 0) {

                            position = $('#listview_contacts tbody td:last').offset().top;
                            scroll_height = $(window).scrollTop() + $(window).height();
                            if(scroll_height > position) 
                            {
                                elem.fnPageChange('next');
                            }
                        };
                    })*/
                },
                "fnDrawCallback":function() {

                    jQuery('#listview_contacts_length').parent().remove();
                    jQuery('#listview_contacts_filter').parent().css('width', '100%');
                    jQuery('#listview_contacts_filter label input').attr('placeholder', 'Search All Contacts');

                    var position = jQuery('#listview_contacts tbody td:last').offset().top;
                    console.log(position);
                    $(document).on( 'scroll', elem, function(){
                        
                    });
                    
                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#listview_contacts_wrapper .dataTables_paginate').css("display", "block");
                        $('#listview_contacts_wrapper .dataTables_length').css("display", "none");
                        $('#listview_contacts_wrapper .dataTables_filter').css("display", "block");
                     } else {
                        $('#listview_contacts_wrapper .dataTables_paginate').css("display", "none");
                        $('#listview_contacts_wrapper .dataTables_length').css("display", "none");
                        //$('#listview_contacts_wrapper .dataTables_filter').css("display", "none");
                     }
                }

            });
            /*$(document).on( 'scroll', elem, function(){
                var position, scroll_height, table_settings;
                table_settings = elem.fnSettings();
                console.log(table_settings.bDrawing);
                console.log('_iDisplayStart' + table_settings._iDisplayStart);
                console.log('_iDisplayLength' + table_settings._iDisplayLength);
                console.log('fnRecordsDisplay' + table_settings.fnRecordsDisplay());
                position = jQuery('#listview_contacts tbody td:last').offset().top;
                console.log('position' + position);

                if(!table_settings.bDrawing && (table_settings._iDisplayStart + table_settings._iDisplayLength) < table_settings.fnRecordsDisplay()) {
                    
                    position = $('#listview_contacts tbody td:last').offset().top;
                    scroll_height = elem.scrollTop() + $(window).height();
                    if(scroll_height > position) {
                        elem.fnPageChange('next');
                    }
                };
            });*/
        }
    };
});
