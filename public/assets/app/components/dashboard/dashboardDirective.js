SimbaNicApp.directive('ngNewOpportunities', function(settings, AuthService, $rootScope, $state, $http, $timeout) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                "columnDefs": [{
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [1, 'desc']
                ],

                "responsive": true,

                // Put Web API HERE.

                "ajax": {
                    "url": settings.siteURL + "assets/app/components/dashboard/newopportunities.json",
                    "method": "POST"
                },

                fixedHeader: {
                    header: true,
                    footer: false
                },
                
                scrollY: 300,

                "paging":   false,
                "info":     false,
                "searching": false,
                
                //use this variable when ajax url caLL
                "columns": [
                    { 
                        "data": null,
                        "width": "70%",
                        "render" : function (data,type,row){
                            return "<a href='#/Opportunities/18092c24-cbd9-9506-744e-56d6c9e94c7e'>"+ data.address +"</a>";
                        },
                    },
                    {
                        "data": "gross_yield",
                        "width": "30%"
                    }
                ],

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "fnDrawCallback": function (oSettings) {

                    if(jQuery('#new_opportunities_wrapper .dataTables_scrollBody > table').height() > 299)
                    {
                        jQuery('#new_opportunities_wrapper .dataTables_scrollBody').slimScroll({
                           height: '300px'
                        });
                    }
                    
                }
            });

            /*$timeout((function() {
                elem.slimScroll({
                    height: '250px'
                });
            }), 100);*/

            
        
        }
    };
});

SimbaNicApp.directive('ngClosingsDashlet', function(settings, AuthService, $rootScope, $state, $filter, $rootScope, closingsFunctions) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [1, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                        "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "method": "POST",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Opportunities',
                                query: "((opportunity_status_c = 'closed_purchased' OR opportunity_status_c = 'closing_cancel_contract') AND (close_date_c >= (CURDATE() - INTERVAL 15 DAY)) ) OR (opportunity_status_c = 'closing_contingencies' OR opportunity_status_c = 'closing_inspection' OR opportunity_status_c = 'closing_post_constingencies')",
                                order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                                offset: 0,
                                select_fields: ["id", "opportunity_status_c", "property_address_c", "property_address_city_c", "property_address_state_c", "property_address_postalcode_c", "close_date_c"],
                                link_name_to_fields_array: [],
                                max_results: 20,
                                deleted: '0',
                                Favorites: false,
                            })
                        },
                    },

                "fixedHeader": {
                    header: true,
                    footer: false
                },
                
                "scrollY": 300,

                "paging":   false,
                "info":     false,
                "searching": false,
                

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "name": "property_address_c",
                        "width": "70%",
                        "render" : function (data,type,row){

                            var class_name = "";
                            var status_name = closingsFunctions.getClosingsStatus(data.opportunity_status_c);

                            if(status_name == "Won" || status_name == "Inspect" || status_name == "Closing" || status_name == "Offer")
                            {
                                status_name = 'Active';
                                class_name = 'label-success';
                            }
                            else if(status_name == "Purchased")
                            {
                                class_name = 'label-warning';
                            }
                            else if(status_name == "Cancelled")
                            {
                                class_name = 'label-danger';
                            }
                            var address = data.property_address_c;
                            //address += data.property_address_c ? ', ' + data.property_address_city_c : '';
                            //address += data.property_address_city_c ? ', ' + data.property_address_state_c : '';
                            //address += data.property_address_state_c ? ', ' + data.property_address_postalcode_c : '';
                            var status = "<span class='label label-sm "+ class_name +"' >" + status_name +"</span>";

                            return "<a href='#/Closings/" + data.id + "'>"+ address +"</a> " + status;
                        },
                    },
                    { 
                        "data": "close_date_c",
                        "width": "30%",
                        "name": "close_date_c",
                    },
                ],
                "fnDrawCallback": function (oSettings) {

                    if(jQuery('#closings_dashlet_wrapper .dataTables_scrollBody > table').height() > 299)
                    {
                        jQuery('#closings_dashlet_wrapper .dataTables_scrollBody').slimScroll({
                           height: '300px'
                        });
                    }
                }
                
            });
        }
    };
});

SimbaNicApp.directive('ngOffersDashlet', function(settings, AuthService, $rootScope, $state, $filter, $rootScope, closingsFunctions) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [1, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                        "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "method": "POST",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Opportunities',
                                query: "((opportunity_status_c = 'offer_lost_deal') AND (close_date_c >= (CURDATE() - INTERVAL 15 DAY)) OR (opportunity_status_c = 'offer_make_offer' OR opportunity_status_c = 'offer_out_for_signature' OR opportunity_status_c = 'offer_signed_ready_to_submit' OR opportunity_status_c = 'offer_sent_to_seller'))",
                                order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                                offset: 0,
                                select_fields: ["id", "opportunity_status_c", "property_address_c", "property_address_city_c", "property_address_state_c", "property_address_postalcode_c", "close_date_c"],
                                link_name_to_fields_array: [],
                                max_results: 20,
                                deleted: '0',
                                Favorites: false,
                            })
                        },
                    },

                "fixedHeader": {
                    header: true,
                    footer: false
                },
                
                "scrollY": 300,

                "paging":   false,
                "info":     false,
                "searching": false,
                

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "name": "property_address_c",
                        "width": "70%",
                        "render" : function (data,type,row){

                            var class_name = "";
                            var status_name = closingsFunctions.getClosingsStatus(data.opportunity_status_c);

                            if(status_name == "Won" || status_name == "Inspect" || status_name == "Closing" || status_name == "Offer")
                            {
                                status_name = 'Active';
                                class_name = 'label-success';
                            }
                            else if(status_name == "Lost")
                            {
                                class_name = 'label-danger';
                            }
                            var address = data.property_address_c;
                            //address += data.property_address_c ? ', ' + data.property_address_city_c : '';
                            //address += data.property_address_city_c ? ', ' + data.property_address_state_c : '';
                            //address += data.property_address_state_c ? ', ' + data.property_address_postalcode_c : '';
                            var status = "<span class='label label-sm "+ class_name +"' >" + status_name +"</span>";

                            return "<a href='#/Closings/" + data.id + "'>"+ address +"</a> " + status;
                        },
                    },
                    { 
                        "data": "close_date_c",
                        "width": "30%",
                        "name": "close_date_c",
                    },
                ],
                "fnDrawCallback": function (oSettings) {
                    
                    if(jQuery('#offers_dashlet_wrapper .dataTables_scrollBody > table').height() > 299)
                    {
                        jQuery('#offers_dashlet_wrapper .dataTables_scrollBody').slimScroll({
                           height: '300px'
                        });
                    }
                }
            });
        }
    };
});

SimbaNicApp.directive('ngTasksDashlet', function(settings, AuthService, $rootScope, $state, $filter, $rootScope, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [1, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                        "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                        "method": "POST",
                        "data": function ( d ) {
                            d.method = "get_entry_list";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = JSON.stringify({
                                session: AuthService.getSessionId(),
                                module_name: 'Tasks',
                                query: "tasks.status != 'Completed' AND tasks.assigned_user_id = '" + AuthService.getUserId() + "'",
                                order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                                offset: 0,
                                select_fields: ["id", "name","date_due", "status", "property_address_c"],
                                link_name_to_fields_array: [],
                                max_results: 20,
                                deleted: '0',
                                Favorites: false,
                            })
                        },
                    },
                
                "scrollY": 300,

                "paging":   false,
                "info":     false,
                "searching": false,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "name": "name",
                        "width": "70%",
                        "render" : function (data,type,row){

                            if(data.status)
                            {
                                if(data.status != 'Completed')
                                {
                                    var address = data.property_address_c ? ': ' + data.property_address_c : '';

                                    return '<div class="md-checkbox" style="display: inline-block;">\
                                        <input type="checkbox" id="task_'+ data.id +'" data-id="'+ data.id +'" class="md-check task_incomplete" data-task-toggle="confirmation">\
                                        <label for="task_'+ data.id +'">\
                                            <span></span>\
                                            <span class="check" ></span>\
                                            <span class="box"></span> </label>\
                                    </div><a href="#/Tasks/'+ data.id +'">' + data.name + address + '</a>';
                                }
                                else
                                {
                                    return "";
                                }
                            }
                            else
                            {
                                return "";
                            }
                        },
                    },
                    {
                        "data": null,
                        "name": 'date_due',
                        "width": "30%",
                        "render" : function (data, type, row) {
                            var date = new Date(data.date_due.replace(/-/g,"/"));
                            return $filter('date')(date, 'MM/dd/yyyy');
                        },
                    },
                ],
                "fnDrawCallback": function (oSettings) {
                    
                    if(jQuery('#tasks_dashlet_wrapper .dataTables_scrollBody > table').height() > 299)
                    {
                        jQuery('#tasks_dashlet_wrapper .dataTables_scrollBody').slimScroll({
                           height: '300px'
                        });
                    }

                    $('[data-task-toggle="confirmation"]').confirmation({
                        btnOkClass: 'btn-success',
                        btnCancelClass: 'btn-danger',
                        singleton: true,
                        popout: true,
                        container: 'body',
                        title: 'Complete: Are you sure?'
                    });

                    $('[data-task-toggle="confirmation"]').on('confirmed.bs.confirmation', function () {

                        $rootScope.simbaLoadingShow();

                        var name_value_list = [
                            {
                                "name": "id",
                                "value": jQuery(this).attr("data-id"),
                            },
                            {
                                "name": "status",
                                "value": "Completed"
                            }
                        ];

                        SimbaNicAPI.save('Tasks', name_value_list)
                            .success(function(response) {
                                scope.reloadDashletTask();
                                $rootScope.simbaLoadingHide();
                        })

                    });

                    /*jQuery(".task_incomplete").change(function() {

                        if(jQuery(this).is(':checked'))
                        {
                            jQuery(this).attr('checked')
                            if(confirm("Are you sure you want to Confirm this task?"))
                            {
                                $rootScope.simbaLoadingShow();

                                var name_value_list = [
                                    {
                                        "name": "id",
                                        "value": jQuery(this).attr("data-id"),
                                    },
                                    {
                                        "name": "status",
                                        "value": "Completed"
                                    }
                                ];

                                SimbaNicAPI.save('Tasks', name_value_list)
                                    .success(function(response) {
                                        scope.reloadDashletTask();
                                        $rootScope.simbaLoadingHide();
                                })
                            }
                        }

                    });*/
                }
            });

            scope.reloadDashletTask = function() {
                elem.dataTable()._fnAjaxUpdate();
            };
        }
    };
});

SimbaNicApp.directive('ngReportsDashlet', function(settings, AuthService, $rootScope, $state, $http, $timeout) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                "columnDefs": [{
                    "searchable": false,
                    "targets": [0]
                }],

                "responsive": true,

                // Put Web API HERE.

                "ajax": {
                    "url": settings.siteURL + "assets/app/components/dashboard/reports.json",
                    "method": "POST"
                },

                fixedHeader: {
                    header: true,
                    footer: false
                },
                
                scrollY: 160,

                "paging":   false,
                "info":     false,
                "searching": false,
                "ordering": false,
                
                //use this variable when ajax url caLL
                "columns": [
                    { 
                        "data": null,
                        "width": "70%",
                        "render" : function (data,type,row){
                            return "<a href='#/'>"+ data.name +"</a>";
                        },
                    },
                    {
                        "data": "last_run",
                        "width": "30%"
                    }
                ],

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "fnDrawCallback": function (oSettings) {

                    if(jQuery('#reports_dashlet_wrapper .dataTables_scrollBody > table').height() > 299)
                    {
                        jQuery('#reports_dashlet_wrapper .dataTables_scrollBody').slimScroll({
                           height: '160px'
                        });
                    }
                    
                }
            });
        }
    };
});

SimbaNicApp.directive('ngReports1Chart', function(settings, AuthService, $rootScope, $state, $http, $timeout) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            
            elem.sparkline([8, 9, 10, 11, 10, 10, 12, 10, 10, 11, 9, 12, 11, 10, 9, 11, 13, 13, 12], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#35aa47',
                negBarColor: '#e02222'
            });
        }
    };
});

SimbaNicApp.directive('ngReports2Chart', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.sparkline([9, 11, 12, 13, 12, 13, 10, 14, 13, 11, 11, 12, 11, 11, 10, 12, 11, 10], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '55',
                barColor: '#ffb848',
                negBarColor: '#e02222'
            });

            jQuery('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check',
            });

        }
    };
});
