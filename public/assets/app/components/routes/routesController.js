angular.module('SimbaNicApp').controller('RoutesController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
	// $scope.simbaLoading = true;

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.routeViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/routes/detailview/routeView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_route_dialog',
            scope: $scope
        });
    }
    $scope.routeAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/routes/routeAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_route_dialog',
            scope: $scope
        });
    }

});
SimbaNicApp.controller('routeViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getRoute(id) {
         // alert("get location");
         CgseqService.getRoute(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load truck data: ' + error.message;
            });
    }
    $scope.editRoute = function (db) {
        // alert('yes');
       
        $params = $.param({
            "id": db.id,
            "name": db.name,
            "point_1": db.point_1,
            "point_2": db.point_2,
            "vihicle_type": db.vihicle_type,
            "manage_by": db.manage_by

        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveRoute(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getRoute($scope.selected_id);


});
SimbaNicApp.controller('routeAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initRouteAdd(id) {
          // alert("get route" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            name : "",
            point_1 : "",
            point_2 : "",
            vihicle_type : "",
            manage_by : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editRoute = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.route_obj = {id: record.id, name: record.name, point_1: record.point_1, point_2: record.point_2, vihicle_type: record.vihicle_type, manage_by: record.manage_by};

        $params = $.param({
            "id": $scope.route_obj.id,
            "name": $scope.route_obj.name,
            "point_1": $scope.route_obj.point_1,
            "point_2": $scope.route_obj.point_2,
            "vihicle_type": $scope.route_obj.vihicle_type,
            "manage_by": $scope.route_obj.manage_by
        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddRoute($scope.route_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initRouteAdd($scope.selected_id);


});