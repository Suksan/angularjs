require 'CSV'
require 'rubygems'
require 'active_record'
require 'mysql2'
require 'spreadsheet'

File.delete("file.xls") if File.exist?("file.xls")

Spreadsheet.client_encoding = 'UTF-8'

book = Spreadsheet::Workbook.new
sheet1 = book.create_worksheet :name => "staffs"


ActiveRecord::Base.establish_connection(
  adapter:  'mysql2', # or 'postgresql' or 'sqlite3'
  host:     'localhost',
  database: 'greenlight_staging',
  username: 'root',
  password: 'simpson'
)

class Property < ActiveRecord::Base
  #blah, blah, blah
  
end

class Proforma < ActiveRecord::Base
  #blah, blah, blah
end

class PropertyDetail < ActiveRecord::Base
  #blah, blah, blah
end

pid = 2418949
p = Property.find(pid)
pd = PropertyDetail.find_by_id(p.detail_id)
pf = Proforma.where(["property_id = ? and user_id is null",pid])[0]

i = 0
  
sheet1[i,0] = p.id
sheet1[i,1] = p.status
sheet1[i,2] = p.rep
sheet1[i,3] = p.dom
sheet1[i,4] = pf.rental_output_gross_yield
sheet1[i,5] = ""
sheet1[i,6] = ""
sheet1[i,7] = p.zillow_rental_estimate
sheet1[i,8] = p.list_price
sheet1[i,9] = pf.comparables_avm_sale
sheet1[i,10] = p.zillow_sale_estimate
sheet1[i,11] = pf.rental_output_renovation_expense
sheet1[i,12] = p.street_address
sheet1[i,13] = p.city
sheet1[i,14] = p.county
sheet1[i,15] = p.zip_code
sheet1[i,16] = p.subdivision
sheet1[i,17] = p.mls_area_key
sheet1[i,18] = p.beds
sheet1[i,19] = p.bathrooms
sheet1[i,20] = p.square_feet
sheet1[i,21] = p.lot_square_feet
sheet1[i,22] = p.year_built
sheet1[i,23] = p.cdom
sheet1[i,24] = p.avg_school_rating
sheet1[i,25] = p.category
sheet1[i,26] = p.mls_number
sheet1[i,27] = ""
sheet1[i,28] = ""
sheet1[i,29] = ""
sheet1[i,30] = ""
sheet1[i,31] = p.tax_amount
sheet1[i,32] = pd.annual_maintenance_fee
sheet1[i,33] = p.pool_private
sheet1[i,34] = p.appreciation_rate
sheet1[i,35] = p.elementary_school
sheet1[i,36] = p.elementary_school_rating
sheet1[i,37] = p.middle_school
sheet1[i,38] = p.middle_school_rating
sheet1[i,39] = p.high_school
sheet1[i,40] = p.high_school_rating
sheet1[i,41] = ""
sheet1[i,42] = p.garage_capacity
sheet1[i,43] = p.state
sheet1[i,44] = p.school_district_name
sheet1[i,45] = p.zillow_id
sheet1[i,46] = p.tax_rate
sheet1[i,47] = pd.public_remarks
sheet1[i,48] = ""
sheet1[i,49] = ""
sheet1[i,50] = ""
sheet1[i,51] = ""
sheet1[i,52] = pf.comparables_avm_rental
sheet1[i,53] = ""
sheet1[i,54] = p.tax_id
sheet1[i,55] = ""
sheet1[i,56] = ""


book.write 'file.xls'  