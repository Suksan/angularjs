module ApplicationHelper

  def bootstrap_alert_class(type)
    case type
      when :error then 'alert-error'
      when :notice then 'alert-info'
      when :success then 'alert-success'
      when :timedout then nil
      else type.to_s
    end
  end

  def breadcrumb(text, url=nil, link_options={})
    @breadcrumbs ||= []
    @breadcrumbs << text
    nav_item(text, url, link_options)
  end

  def tab_item(text, url)
    nav_item(text, url, 'data-toggle' => 'tab')
  end

  def nav_item(text, url=nil, link_options={})
    puts "request.url #{url}"
    # binding.pry
    content_tag(:li, class: [(:disabled if url.nil?), (:active if ((url && request.url == url))    )].compact.join(' ')) do
      content = url ? link_to(text, url, link_options) : link_to(text, '#')
      divider = link_options.delete(:divider)
      divider ? content + content_tag(:span, divider, class: :divider) : content
    end
  end

  def simple_form_for(record, options={}, &block)
    options[:direction] ||= :horizontal
    options[:builder] ||= SimpleForm::FormBuilder
    options[:html] ||= {}
    unless options[:html].key?(:novalidate)
      options[:html][:novalidate] = !SimpleForm.browser_validations
    end
    options[:html][:class] = [SimpleForm.form_class, simple_form_css_class(record, options), "form-#{options.delete(:direction)}"].compact.join(" ")
    options[:html]['data-observe'] = true if options.delete(:observe)
    options[:html]['data-autosubmit'] = true if options.delete(:autosubmit)
    
    with_simple_form_field_error_proc do
      form_for(record, options, &block)
    end
  end

  def breadcrumbs_for(action, model, &block)
    breadcrumbs = @breadcrumbs.map {|b| b.is_a?(Proc) ? b[action, model] : b}.compact
    unless breadcrumbs.empty?
      content_tag(:ul, class: :breadcrumb) do
        result = breadcrumbs.map.with_index{ |b, i|
          nav_item(b.delete(:text), b.delete(:url), b.merge(divider: i < breadcrumbs.length-1 ? '/' : nil))
        }.reduce(:<<).concat(block_given? ? capture(&block) : nil).html_safe
      end
    end
  end

  def link_to_previous(text=t(:cancel))
    link_to text, :back, class: 'btn btn-link'
  end

  def debug(variable, force=false)
    super(variable) if Rails.env.development? || force
  end

  def page_info(collection)
    Delayed::Worker.logger.debug "collection: #{collection}"
    content_tag(:div, class: :page_info) do
      content_tag(:div, page_entries_info(collection), class: 'count pull-left') +
      will_paginate(collection, class: 'pagination pull-right')
    end
  end
  
  def page_info_comps(collection)
    Delayed::Worker.logger.debug "collection: #{collection}"
    content_tag(:div, class: :page_info) do
      content_tag(:div, page_entries_info(collection), class: 'count pull-left') +
      will_paginate(collection, :params => {:anchor => "data"}, class: 'pagination pull-right')
    end
  end
 
  def sortable(key, label, options={})
    current = key.to_sym == sort_by
    puts "#() sortable #{key}"
    if key == "rep"
      key = "users.firstname"
      current = key.to_sym == sort_by
    elsif key == "tag"
      key = "property_tags.name"
      current = key.to_sym == sort_by
    # elsif key == "school_district"
      # key = "school_districts.school_district_name"
      # current = key.to_sym == sort_by
    end
    output = link_to(params.merge(options).merge(sort_by: key, sort_asc: (!sort_asc if current)), {class: [:sort, (:dropup if current && sort_asc)].compact.join(' ')}) do
      output = label
      output << content_tag(:b, nil, class: 'caret') if current
      output.html_safe
    end
  end
  
  def non_sortable(key, label, options={})
    current = key.to_sym == sort_by
    puts "#() sortable #{key}"
    if key == "rep"
      key = "users.firstname"
      current = key.to_sym == sort_by
    elsif key == "tag"
      key = "property_tags.name"
      current = key.to_sym == sort_by
    # elsif key == "school_district"
      # key = "school_districts.school_disctrict_name"
      # current = key.to_sym == sort_by
    end
    # output = link_to(params.merge(options).merge(sort_by: key, sort_asc: (!sort_asc if current)), {class: [:sort, (:dropup if current && sort_asc)].compact.join(' ')}) do
      output = label
      output << content_tag(:b, nil, class: 'caret') if current
      output.html_safe
    # end
  end
  
  def flatten_hash(hash = params, ancestor_names = [])
    flat_hash = {}
    hash.each do |k, v|
      names = Array.new(ancestor_names)
      names << k
      if v.is_a?(Hash)
        flat_hash.merge!(flatten_hash(v, names))
      else
        key = flat_hash_key(names)
        key += "[]" if v.is_a?(Array)
        flat_hash[key] = v
      end
    end

    flat_hash
  end

  def flat_hash_key(names)
    names = Array.new(names)
    name = names.shift.to_s.dup 
    names.each do |n|
      name << "[#{n}]"
    end
    name
  end

  def hash_as_hidden_fields(hash = params)
    hidden_fields = []
    flatten_hash(hash).each do |name, value|
      value = [value] if !value.is_a?(Array)
      value.each do |v|
        hidden_fields << hidden_field_tag(name, v.to_s, :id => nil)          
      end
    end

    hidden_fields.join("\n")
  end
  
  def icon_link_to(path, opts = {}, link_opts = {})
    classes = []
    [:icon, :blank].each do |klass|
      if k = opts.delete(klass)
        classes << "#{klass}-#{k}"
      end
    end
    classes << "enlarge" if opts.delete(:enlarge)
    opts[:class] ||= ""
    opts[:class] << " " << classes.join(" ")
    link_to content_tag(:i, "", opts), path, link_opts
  end
  
  def page_select(options, v = 20)
    puts "~~ helper per_page #{v}"
    v = 20 if v.nil?
    a = [[20,20],[50,50],[100,100]]
     content_tag(:table , style: 'margin-bottom: 0px; border-top:0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Display", style: "width: 65px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            general_options =  {style: "width: 120px"}
            general_options.merge!(options)
            select_tag "per_page", options_for_select(a,selected: v),general_options
          end
        end
      end
    end
  end

  def saved_page_select(options, v = 20)
    puts "~~ helper per_page #{v}"
    v = 20 if v.nil?
    a = [[20,20],[50,50],[100,100]]
     content_tag(:table , style: 'margin-bottom: 0px; border-top:0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Display", style: "width: 65px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            general_options =  {style: "width: 120px"}
            general_options.merge!(options)
            select_tag "saved_per_page", options_for_select(a,selected: v),general_options
          end
        end
      end
    end
  end

  def data_page_select(options, v = 0)
    puts "~~ helper per_page #{v}"
    v = 0 if v.nil?
    a = [["all",0],[20,20],[50,50],[100,100]]
     content_tag(:table , style: 'margin-bottom: 0px; border-top:0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Display", style: "width: 65px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            general_options =  {style: "width: 120px"}
            general_options.merge!(options)
            select_tag "data_per_page", options_for_select(a,selected: v),general_options
          end
        end
      end
    end
  end
  
  def client_sale_data_page_select(options, v = 0)
    puts "~~ helper per_page #{v}"
    v = 0 if v.nil?
    a = [["all",0],[20,20],[50,50],[100,100]]
     content_tag(:table , style: 'margin-bottom: 0px; border-top:0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Display", style: "width: 65px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            general_options =  {style: "width: 120px"}
            general_options.merge!(options)
            select_tag "client_sale_data_per_page", options_for_select(a,selected: v),general_options
          end
        end
      end
    end
  end
  
  def client_rental_data_page_select(options, v = 0)
    puts "~~ helper per_page #{v}"
    v = 0 if v.nil?
    a = [["all",0],[20,20],[50,50],[100,100]]
     content_tag(:table , style: 'margin-bottom: 0px; border-top:0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Display", style: "width: 65px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            general_options =  {style: "width: 120px"}
            general_options.merge!(options)
            select_tag "client_rental_data_per_page", options_for_select(a,selected: v),general_options
          end
        end
      end
    end
  end
  
  def t(key, options = {})
  # try to translate as usual
    begin
      I18n.t(key, options.merge(raise: true))
    rescue I18n::MissingTranslationData => e
      key.to_s.titleize
    end
  end

end