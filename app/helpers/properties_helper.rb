module PropertiesHelper

  def image_for(property)
    property.send(:fetch_geo_coordinates,current_user.id)
    
    options = { style: 'width: 300px; height: 140px' }
    options[:src] = "http://maps.googleapis.com/maps/api/streetview?size=300x140&location=#{property.latitude},%20#{property.longitude}&sensor=false" if property.geocoded?
    content_tag(:img, nil, options)
    
  end
  
   def property_input(c, field)
    c.label(field, label: Property.label(field, :short)) +
    c.input(field, label: false, wrapper_html: {class: 'clearfix'}, input_html: {class: 'input-medium'})
  end
  
  def prop_token_input(c,j,a)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Rep", class: "user optional control-label", for: "property_rep", style: "width: 40px; text-align: left;")  
          end +
          content_tag(:td, class: 'align-right item', style: 'width: 190px') do
            
            if a
              c.text_field("rep",class: "user optional input-small", "data-pre" => j, style: "width: 140px; display: none;",admin: true )
            else
              c.text_field("rep",class: "user optional input-small", "data-pre" => j, style: "width: 140px; display: none;" )
            end
           
          end
        end
      end
    end
  end
  
  def prop_tokeninput(c,a,v)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Rep", class: "user optional control-label", for: "property_tag", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "rep", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , :include_blank => true
            else
              select_tag "rep", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end
   
  end

   def prop_mul_rep(c,a,v,d,n=nil)
    Delayed::Worker.logger.debug "!!d #{d}"
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Client", class: "user optional control-label", for: "property_tag", style: "width: 60px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n , multiple: true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , multiple: true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n 
              select_tag "rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", app_namespace: n , multiple: true
            else
              select_tag "rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", multiple: true

            end
          end
        end
      end
    end
   
  end
  
  def client_prop_mul_rep(c,a,v,d,n=nil)
    Delayed::Worker.logger.debug "!!d #{d}"
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Client", class: "user optional control-label", for: "property_tag", style: "width: 60px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "client_rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n , multiple: true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "client_rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , multiple: true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n 
              select_tag "client_rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", app_namespace: n , multiple: true
            else
              select_tag "client_rep", options_for_select(a,selected: v,disabled: d), style: "width: 140px", multiple: true

            end
          end
        end
      end
    end
   
  end
  
   
  def prop_select(c,a,v,n=nil)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Label", class: "user optional control-label", for: "property_tag", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "tag", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "tag", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n 
              select_tag "tag", options_for_select(a,selected: v), style: "width: 120px", app_namespace: n , :include_blank => true
            else
              select_tag "tag", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end    
  end
  
  def client_prop_select(c,a,v,n=nil)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Label", class: "user optional control-label", for: "property_tag", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "client_tag", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "client_tag", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n 
              select_tag "client_tag", options_for_select(a,selected: v), style: "width: 120px", app_namespace: n , :include_blank => true
            else
              select_tag "client_tag", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end    
  end
  
  def prop_note(c,vs)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px; margin-top: 4px; ') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left fix_left title') do
            content_tag(:label, "Notes", class: "user optional control-label", style: "width: 100px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-right   item') do
           
            if vs && vs.count > 0
              # if vs.count == 1
                # content_tag("div", vs.first, class: "txt_notes") 
                # c.text_field(vs.first,class: "txt_notes")
                text_field_tag "client_notes",vs.first,class: "txt_notes"
              # else
                # content_tag("div", class: "txt_notes") do
                  # content_tag(:ul, :class => 'a class') do
                    # vs.each_with_index do |item|
                      # concat(content_tag(:li, item))
                    # end
                  # end
                # end
              # end
            else
              # content_tag("div", "-" , class: "txt_notes_blank")
              text_field_tag "client_notes","",class: "txt_notes"
            end
            # else
            # text_field_tag "notes",v,class: "txt_notes"
            # content_tag("div", v, class: "txt_notes")
              
            # end
          end
        end
      end
    end     
  end
  
  def prop_public_remarks(c,v)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px; margin-top: 4px; ') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left fix_left title') do
            content_tag(:label, "Public Remarks", class: "user optional control-label", style: "width: 200px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-left   item') do
           
            
            content_tag("div", v, class: "txt_remarks") 
            
            # else
            # text_field_tag "notes",v,class: "txt_notes"
            # content_tag("div", v, class: "txt_notes")
              
            # end
          end
        end
      end
    end     
  end
  
  def prop_equipment(c,v)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px; margin-top: 4px; ') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left fix_left title') do
            content_tag(:label, "Equipments", class: "user optional control-label", style: "width: 200px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-left   item') do
           
            
            content_tag("div", v, class: "txt_remarks") 
            
            # else
            # text_field_tag "notes",v,class: "txt_notes"
            # content_tag("div", v, class: "txt_notes")
              
            # end
          end
        end
      end
    end     
  end
  
  def prop_realtor_remarks(c,v)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px; margin-top: 4px; ') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left fix_left title') do
            content_tag(:label, "Realtor Remarks", class: "user optional control-label", style: "width: 200px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-left   item') do
           
            
            content_tag("div", v, class: "txt_remarks") 
            
            # else
            # text_field_tag "notes",v,class: "txt_notes"
            # content_tag("div", v, class: "txt_notes")
              
            # end
          end
        end
      end
    end     
  end
  
  
  
  
  def near_commercial_select(c,a,v,n=nil)
   
     content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px;margin-top: 5px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-right title',style: "width: 120px") do
            content_tag(:label, "Commercial", class: "user optional control-label", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "near_commercial", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "near_commercial", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "near_commercial", options_for_select(a,selected: v), style: "width: 120px", app_namespace: n, :include_blank => true
            else
              select_tag "near_commercial", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end
    
  end

  def client_near_commercial_select(c,a,v,n=nil)
   
     content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px;margin-top: 5px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-right title',style: "width: 120px") do
            content_tag(:label, "Commercial", class: "user optional control-label", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"]
              select_tag "client_near_commercial", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "client_near_commercial", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "client_near_commercial", options_for_select(a,selected: v), style: "width: 120px", app_namespace: n, :include_blank => true
            else
              select_tag "client_near_commercial", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end
    
  end
  
  def prop_user_update(c,v)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Last Update", class: "user optional control-label", for: "property_tag", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            content_tag(:span, v ? v.strftime("%m/%d/%Y %H:%M"): '-', id: "last_user_update", style:"padding-top: 5px; width: 120px; text-align: right; height: 25px; display: block;")
          end
        end
      end
    end
    # content_tag(:div, class: "control-group user optional")  do
      # content_tag(:label, "Last User Update", class: "user optional control-label", for: "property_tag", style: "width: 100px; text-align: left;")  +
       # content_tag(:div, class: "controls", style: "margin-left: 120px;") do
         # content_tag(:div, v ? v.strftime("%m/%d/%Y %H:%M"): nil, id: "last_user_update", style:"padding-top: 5px; width: 150px; text-align: right")
       # end
    # end
  end
  
  def flood_select(c,a,v,n=nil)
   
     content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px;margin-top: 5px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title',style: "width: 164px") do
            content_tag(:label, "Flood", class: "user optional control-label", style: "width: 60px; text-align: left;")
          end +
          content_tag(:td, class: 'align-left item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"] && n 
              select_tag "flood", options_for_select(a,selected: v), style: "width: 140px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , app_namespace: n, :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "flood", options_for_select(a,selected: v), style: "width: 140px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"] , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "flood", options_for_select(a,selected: v), style: "width: 140px", app_namespace: n, :include_blank => true
            else
              select_tag "flood", options_for_select(a,selected: v), style: "width: 140px", :include_blank => true
            end
          end
        end
      end
     end
      
    
  end
  
  def flood_select2(c,a,v,n=nil)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Flood", class: "user optional control-label", style: "width: 100px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"] && n 
              select_tag "flood", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n.to_s , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "flood", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "flood", options_for_select(a,selected: v), style: "width: 120px", app_namespace: n.to_s , :include_blank => true
            else
              select_tag "flood", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end     
  end
  
  def client_flood_select2(c,a,v,n=nil)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Flood", class: "user optional control-label", style: "width: 100px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-right item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"] && n 
              select_tag "client_flood", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n.to_s , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "client_flood", options_for_select(a,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "client_flood", options_for_select(a,selected: v), style: "width: 120px", app_namespace: n.to_s , :include_blank => true
            else
              select_tag "client_flood", options_for_select(a,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      end
    end     
  end
  
  
  def client_access_select2(c,a,v,n=nil)
    # content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      # content_tag(:tbody) do
        content_tag(:tr,style: "height: 26.7778px") do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Type:", class: "user optional control-label", style: "width: 70px; text-align: left; ")
          end +          
          content_tag(:td, class: 'align-left item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"] && n 
              select_tag "client_access", options_for_select(a,selected: v), style: "width: 160px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n.to_s , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "client_access", options_for_select(a,selected: v), style: "width: 160px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "client_access", options_for_select(a,selected: v), style: "width: 160px", app_namespace: n.to_s , :include_blank => true
            else
              select_tag "client_access", options_for_select(a,selected: v), style: "width: 160px", :include_blank => true
            end
          end
        end
      # end
    # end     
  end
  
  def client_scheduled_date(c,v,n=nil)
    # content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      # content_tag(:tbody) do
        content_tag(:tr,style: "height: 26.7778px") do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Date:", class: "user optional control-label", style: "width: 70px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-left item') do
            content_tag(:div, :class => 'input-prepend', :style => "width: 160px") do
              
              content_tag(:span, content_tag(:i, nil, class: 'icon-th'), :class => 'add-on').concat(text_field_tag 'scheduled_date', v, class: 'date optional date input-small', style: "width: 131.8px")
            end
          end
        end
      # end
    # end     
  end
  
  def client_username(c,v,n=nil)
    # content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      # content_tag(:tbody) do
        content_tag(:tr,style: "height: 26.7778px") do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "", class: "user optional control-label", style: "width: 70px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-left item') do
            
              
            hidden_field_tag 'username', v
            
          end
        end
      # end
    # end     
  end
  
  def client_scheduled_time(c,v,n=nil)
    # content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      # content_tag(:tbody) do
      
        hour = Array.new
        for h in 7..22 do
          for m in ['00',  '30'] do
            if (h == 22) && (m == '30')
              break
            end
            if h <= 12
              hour << ["%02d" % h + ":" + m + " am" , "%02d" % h + ":" + m + " am"]
            else
              mh = h % 12
              hour << ["%02d" % mh + ":" + m + " pm" , "%02d" % mh + ":" + m + " pm"]
            end
          end
        end
        
        a = []
        a = [["8:00 am","8:00 am"],["9:00 am","9:00 am"]]
        content_tag(:tr,style: "height: 26.7778px") do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "Time:", class: "user optional control-label", style: "width: 70px; text-align: left;")
          end +          
          content_tag(:td, class: 'align-left item') do
            if ENV["RAILS_RELATIVE_URL_ROOT"] && n 
              select_tag "scheduled_time", options_for_select(hour,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], app_namespace: n.to_s , :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"] && n.blank?
              select_tag "scheduled_time", options_for_select(hour,selected: v), style: "width: 120px", app_root: ENV["RAILS_RELATIVE_URL_ROOT"], :include_blank => true
            elsif  ENV["RAILS_RELATIVE_URL_ROOT"].blank? && n  
              select_tag "scheduled_time", options_for_select(hour,selected: v), style: "width: 120px", app_namespace: n.to_s , :include_blank => true
            else
              select_tag "scheduled_time", options_for_select(hour,selected: v), style: "width: 120px", :include_blank => true
            end
          end
        end
      # end
    # end     
  end
  
  def options_for_time_select
      hour = Array.new
      for h in 8..21 do
          for m in ['00', '15', '30', '45'] do
              hour.push [h.to_s + "h" + m.to_s, "%02d" % h + ":" + m + ":00"]
          end
      end
      hour
  end
  
  def prop_county(c,v)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      content_tag(:tbody) do
        content_tag(:tr) do
          content_tag(:td, class: 'align-left title') do
            content_tag(:label, "County", class: "user optional control-label", style: "width: 100px; text-align: left;")
          end +
          content_tag(:td, class: 'align-right item') do
            content_tag(:span, v ? v: '-', id: "county", style:"padding-top: 5px; width: 120px; text-align: right; height: 25px; display: block;")
          end
        end
      end
    end
    # content_tag(:div, class: "control-group user optional")  do
      # content_tag(:label, "Last User Update", class: "user optional control-label", for: "property_tag", style: "width: 100px; text-align: left;")  +
       # content_tag(:div, class: "controls", style: "margin-left: 120px;") do
         # content_tag(:div, v ? v.strftime("%m/%d/%Y %H:%M"): nil, id: "last_user_update", style:"padding-top: 5px; width: 150px; text-align: right")
       # end
    # end
  end

end
