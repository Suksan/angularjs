module SearchesHelper

  def search_text_input(c, field)
    c.label(field, label: Property.label(field, :short)) +
    c.input(field, label: false, as: :text, placeholder: t("#{field}_placeholder".to_sym, default: t(:text_placeholder)), wrapper: false, input_html: {class: 'input-medium'})
  end

  def search_input(c, field)
    c.label(field, label: Property.label(field, :short)) +
    c.input(field, label: false, wrapper_html: {class: 'clearfix'}, input_html: {class: 'input-medium'})
  end

  def multiple_search_input(c, field, autocomplete=false)
    as = Property.attr_config(field, :as)
    options = { model: Property, wrapper: false }
    options[:input_html] = {class: autocomplete ? 'autocomplete input-large' : 'input-large'}
    unless as.to_sym == :enum
      puts "%%%%# #{@stats}"
      collection = @stats["#{field}_distinct"]
      puts "$$5% #{collection}"
      options.merge!(as: :select, collection: collection || {}) 
      options.merge!(label_method: proc{|k, v| [k, v].compact.join(' - ') }, value_method: proc{|k, v| k}) if collection.is_a?(Hash)
    end
    
    content_tag(:a, content_tag(:i, nil, class: 'pull-right icon-remove'), href: '#', data: {toggle: 'clear'}) +
    c.multiple_input(field, options)
  end
  
  def multiple_search_nullable_neg_input(c, field, search_param, autocomplete=false, multiple=false)
    desc = nil
    is_not = false
    Delayed::Worker.logger.debug "field: #{field}"
    Delayed::Worker.logger.debug "stat: #{@stats["#{field}_distinct"]}"
    as = Property.attr_config(field, :as)
    options = { model: Property, wrapper: false,label: false }
    options[:input_html] = {class: autocomplete ? 'autocomplete input-large' : 'input-large'}
    Delayed::Worker.logger.debug "%%%%# as #{field}"
    Delayed::Worker.logger.debug "%%!%# search param #{search_param}"
    case field
      when :flood 
        desc = "Flood"
        options[:input_html] = options[:input_html].merge({size: "5"})
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
         
         
         if is_not
          
          collection << ["Empty", "-1"]
          collection = collection.concat([["High","-high"],["Moderate-low","-moderate-low"],["Undetermined","-undetermined"]])
        else
          
          collection << ["Empty", "0"]
          collection = collection.concat([["High","high"],["Moderate-low","moderate-low"],["Undetermined","undetermined"]])
        end
        
        # collection << ["Empty","0"]
        # collection = collection.concat([["High","high"],["Moderate-low","moderate-low"],["Undetermined","undetermined"]])
        
      when :near_commercial
        desc = "Commercial"
         options[:input_html] = options[:input_html].merge({size: "4"})
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
        
         if is_not
          collection << ["Empty", "-1"]
          collection = collection.concat([["Yes","-a"],["No","-b"]])
        else
          collection << ["Empty", "0"]
          collection = collection.concat([["Yes","a"],["No","b"]])
        end
        
        # options[:input_html] = options[:input_html].merge({size: "4"})
        # collection = []
        # collection << ["Empty","-999"]
        # collection =collection.concat([["Yes","a"],["No","b"]])
      
      when :mls_area_key
        desc = "Mls Area Key"
        options[:input_html] = options[:input_html].merge({size: "4"})
        # collection = []
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
        
        # collection =collection.concat(@market.properties.distinct(:mls_area_key, :mls_area_name) )
        if is_not
          collection << ["Empty", "-999"]
          collection = collection.concat(@stats["#{field}_distinct"].map do |k,v| ["#{k} - #{v}","-#{k}"]  end)
        else
          collection << ["Empty", "0"]
          collection = collection.concat(@stats["#{field}_distinct"].map do |k,v| ["#{k} - #{v}","#{k}"]  end)
        end
      
      when :geo_market_area
        desc = "Geo Market Area"
        options[:input_html] = options[:input_html].merge({size: "4"})
        # collection = []
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
        
        # collection =collection.concat(@market.properties.distinct(:mls_area_key, :mls_area_name) )
        if is_not
          collection << ["Empty", "-999"]
          collection = collection.concat(@stats["#{field}_distinct"].map do |k|  ["#{k}","-#{k}"] end)
        else
          collection << ["Empty", "0"]
          collection = collection.concat(@stats["#{field}_distinct"].map do |k|  ["#{k}","#{k}"] end)
        end
          
    end
    options.merge!(as: :select, collection: collection || {}) 
    options.merge!(label_method: proc{|k, v| [k, v].compact.join(' - ') }, value_method: proc{|k, v| k}) if collection.is_a?(Hash)
    
    content_tag(:label, "#{desc} " , style: "width: 145px; text-align: left; display: inline-block;") +
    
    content_tag(:input, " Not",id: "#{field}_inverse",type: :checkbox,checked: is_not ? true : false) +
    content_tag(:a, content_tag(:i, nil, class: 'pull-right icon-remove'), href: '#', data: {toggle: 'clear'}) +
    content_tag(:label, "", nil) +
    (multiple ? c.multiple_input(field, options) : c.single_input(field, options))
  end
  
  def multiple_search_nullable_input(c, field, search_param, autocomplete=false, multiple=false)
    desc = nil
    as = Property.attr_config(field, :as)
    options = { model: Property, wrapper: false,label: false }
    options[:input_html] = {class: autocomplete ? 'autocomplete input-large' : 'input-large'}
    Delayed::Worker.logger.debug "%%%%# as #{field}"
    Delayed::Worker.logger.debug "%%!%# search param #{search_param}"
    case field
      when :flood 
        desc = "Flood"
        options[:input_html] = options[:input_html].merge({size: "5"})
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
        
         if is_not
          collection << ["Empty", "-1"]
          collection = collection.concat([["High","-high"],["Moderate-low","-moderate-low"],["Undetermined","-undetermined"]])
        else
          collection << ["Empty", "0"]
          collection = collection.concat([["High","high"],["Moderate-low","moderate-low"],["Undetermined","undetermined"]])
        end
        
        # collection << ["Empty","0"]
        # collection = collection.concat([["High","high"],["Moderate-low","moderate-low"],["Undetermined","undetermined"]])
        
      when :near_commercial
        desc = "Commercial"
         options[:input_html] = options[:input_html].merge({size: "4"})
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
        
         if is_not
          collection << ["Empty", "-1"]
          collection = collection.concat([["Yes","-a"],["No","-b"]])
        else
          collection << ["Empty", "0"]
          collection = collection.concat([["Yes","a"],["No","b"]])
        end
        
        # options[:input_html] = options[:input_html].merge({size: "4"})
        # collection = []
        # collection << ["Empty","-999"]
        # collection =collection.concat([["Yes","a"],["No","b"]])
      
      when :mls_area_key
        desc = "Mls Area Key"
        options[:input_html] = options[:input_html].merge({size: "4"})
        # collection = []
        collection = []
        
        is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
        
        # collection =collection.concat(@market.properties.distinct(:mls_area_key, :mls_area_name) )
        if is_not
          collection << ["Empty", "-999"]
          collection = collection.concat(@market.properties.distinct_order_unsigned(:mls_area_key, :mls_area_name).map do |k,v| ["#{k} - #{v}","-#{k}"]  end)
        else
          collection << ["Empty", "0"]
          collection = collection.concat(@market.properties.distinct_order_unsigned(:mls_area_key, :mls_area_name).map do |k,v| ["#{k} - #{v}","#{k}"]  end)
        end
          
    end
    options.merge!(as: :select, collection: collection || {}) 
    options.merge!(label_method: proc{|k, v| [k, v].compact.join(' - ') }, value_method: proc{|k, v| k}) if collection.is_a?(Hash)
     # content_tag(:input, " Not",id: "#{field}_inverse",type: :checkbox,checked: is_not ? true : false) +
    content_tag(:label, desc, style: "width: 165px; text-align: left;") +
    content_tag(:a, content_tag(:i, nil, class: 'pull-right icon-remove'), href: '#', data: {toggle: 'clear'}) +
    content_tag(:label, "", nil) +
    (multiple ? c.multiple_input(field, options) : c.single_input(field, options))
  end
  
  def multiple_search_lookup_input(c, field, search_param, autocomplete=false)
    desc = nil
    is_not = nil
    as = Property.attr_config(field, :as)
    options = { model: Property, wrapper: false }
    options[:input_html] = {class: autocomplete ? 'autocomplete input-medium' : 'input-medium'}
    is_not = search_param && search_param.length > 0 && search_param.reject(&:blank?).try(:first).try(:match,/^-([^\s]+)/)
    # unless as.to_sym == :enum
      # puts "%%%%# as #{as.to_sym}"
      case field
        when :rep 
          desc = "Client"
          collection = []
          
          if is_not
            collection << ["Empty","-999"]
            collection = collection.concat(User.order("firstname , lastname").all.map{|s| [ [(s.firstname.nil? ? '':s.firstname),(s.lastname.nil? ? '': s.lastname)].join(' ').gsub(/^\s\s*/, '').gsub(/\s\s*$/, ''), "-#{s.id.to_s}"]})
          else
            collection << ["Empty","0"]
            collection = collection.concat(User.order("firstname , lastname").all.map{|s| [ [(s.firstname.nil? ? '':s.firstname),(s.lastname.nil? ? '': s.lastname)].join(' ').gsub(/^\s\s*/, '').gsub(/\s\s*$/, ''), s.id.to_s]})
          end
        when :tag
          desc = "Label"
          collection = []
          
          
          if is_not
            collection << ["Empty","-999"]
            collection = collection.concat(@market.property_tags.map{|s| [s.name, "-#{s.id.to_s}"]})
          else
            collection << ["Empty","0"]
            collection = collection.concat(@market.property_tags.map{|s| [s.name, s.id.to_s]})
          end
      end
      Delayed::Worker.logger.debug "$$5% #{collection}"
      options.merge!(label: nil, as: :select, collection: collection || {}) 
      options.merge!(label_method: proc{|k, v| [k, v].compact.join(' - ') }, value_method: proc{|k, v| k}) if collection.is_a?(Hash)
    # end
    
    
    html_option = {id: "search_config_attributes_#{field}", name: "search[config_attributes][#{field}][]",  multiple: "multiple", style: "margin-left: 3px;margin-top: 2px", class: "select autocomplete"}
    
    
     
    content_tag(:label, "#{desc} ", style: "width: 145px; text-align: left; margin-left: 3px; margin-top: 5px; display: inline-block") +
    content_tag(:input, " Not",id: "#{field}_inverse",type: :checkbox,checked: is_not ? true : false) +
    content_tag(:a, content_tag(:i, nil, class: 'pull-right icon-remove'), href: '#', data: {toggle: 'clear'}) +
    content_tag(:label, "", nil) + 
    content_tag(:div) do
      content_tag(:input, "",name: "search[config_attributes][#{field}][]",type: :hidden,value: "") + 
      content_tag(:select,options_for_select(collection, search_param && search_param.length > 0 ? search_param : nil),html_option)
    end
      # select_tag("search_config_attributes_rep", options_for_select(collection), style: "width: 120px", :include_blank => true)
    
    # c.special_input(field, options)
  end
  
  # def multiple_search_lookup_input(c, field, autocomplete=false)
    # as = Property.attr_config(field, :as)
    # options = { model: Property, wrapper: false }
    # options[:input_html] = {class: autocomplete ? 'autocomplete input-medium' : 'input-medium'}
    # unless as.to_sym == :enum
      # puts "%%%%# as #{as.to_sym}"
      # case as.to_sym
        # when :user 
#           
          # collection = []
          # collection << ["Empty","0"]
          # collection = collection.concat(User.order("firstname , lastname").all.map{|s| [ [(s.firstname.nil? ? '':s.firstname),(s.lastname.nil? ? '': s.lastname)].join(' ').gsub(/^\s\s*/, '').gsub(/\s\s*$/, ''), s.id.to_s]})
#           
        # when :tag
          # collection = []
          # collection << ["Empty","0"]
          # collection =collection.concat(@market.property_tags.map{|s| [s.name, s.id.to_s]})
#         
      # end
      # Delayed::Worker.logger.debug "$$5% #{collection}"
      # options.merge!(as: :select, collection: collection || {}) 
      # options.merge!(label_method: proc{|k, v| [k, v].compact.join(' - ') }, value_method: proc{|k, v| k}) if collection.is_a?(Hash)
    # end
#     
    # content_tag(:a, content_tag(:i, nil, class: 'pull-right icon-remove'), href: '#', data: {toggle: 'clear'}) +
    # c.multiple_input(field, options)
  # end

  # def search_boolean_input(c, field)
    # c.input field, label: Property.label(field, :short), as: :radio_buttons, item_wrapper_class: 'inline', collection: [['Yes', 1], ['No', 0], ['Any', '']], wrapper_class: 'clearfix'
  # end
  
  def search_boolean_input(c, field, options = {})
    # Delayed::Worker.logger.debug "options #{options}"
    if options.is_a?(Hash) && options.length > 0
      content_tag(:div,"Back on Market") +
      c.input(field, label: false, as: :radio_buttons, item_wrapper_class: 'inline', collection: [['Yes', 1], ['No', '']], wrapper_class: 'clearfix height32')
         
    else
      c.input(field, label: Property.label(field, :short), as: :radio_buttons, item_wrapper_class: 'inline', collection: [['Yes', 1], ['No', 0], ['Any', '']], wrapper_class: 'clearfix')
    end
  end
  
  def search_boolean_checkbox(c, field, options = {})
    # Delayed::Worker.logger.debug "options #{options}"
    if options.is_a?(Hash) && options.length > 0
       
      c.input(field, label: false, as: :boolean, item_wrapper_class: 'inline', wrapper_class: ' height32 inlaid') +
      content_tag(:div,"Back on Market",style: "float: left; margin-top: 5px") +
      content_tag(:div,"", style: 'clear: both')
         
    else
      c.input(field, label: Property.label(field, :short), as: :radio_buttons, item_wrapper_class: 'inline', collection: [['Yes', 1], ['No', 0], ['Any', '']], wrapper_class: 'clearfix')
    end
  end
  
  def search_boolean_price_reduction(c, field, options = {})
    # Delayed::Worker.logger.debug "options #{options}"
    if options.is_a?(Hash) && options.length > 0
      
      c.input(field, label: false, as: :boolean, item_wrapper_class: 'inline', wrapper_class: ' height32 inlaid') +
      content_tag(:div,"Price Reduction",style: "float: left; margin-top: 5px") +
      content_tag(:div,"", style: 'clear: both')
      
      # c.input(field, label: false, as: :boolean, item_wrapper_class: 'inline', wrapper_class: ' height32 inlaid width100') +
      # content_tag(:div,"Price Reduction",style: "float: left; margin-top: 5px; margin-left: 5px") +
      # content_tag(:div,"", style: 'clear: both')
      
         
    else
      c.input(field, label: Property.label(field, :short), as: :boolean, item_wrapper_class: 'inline', wrapper_class: 'clearfix')
    end
  end
  
  def search_slider(c, field, options={})
    as = Property.attr_config(field, :as).try(:to_sym)
    step = options[:step]
    step ||= case as
      when :percent then 0.01
      when :money then 1000
      else 1
    end
    options[:dimension] ||= case as
      when :money then '&nbsp;$'
      when :percent then '&nbsp;%'
    end
    min = @stats["#{field}_min"]
    max = @stats["#{field}_max"]
    if max && limit = options[:limit]
      options[:heterogeneity] ||= ["75/#{limit}"] if limit < max
    end
    c.slider field, min, max, step, options
  end

  def search_min_max(c, field)
    as = Property.attr_config(field, :as)
    c.label(field, label: Property.label(field, :short)) +
    content_tag(:div, class: 'clearfix') do
      c.input(field, label: false, wrapper: false, input_html: { placeholder: Property.format(field, @stats["#{field}_min"]), multiple: true, class: "small input-mini align-right pull-left #{as}", value: c.object.send(field).try(:first) }) +
      c.input(field, label: false, wrapper: false, input_html: { placeholder: Property.format(field, @stats["#{field}_max"]), multiple: true, class: "small input-mini align-right pull-left #{as}", value: c.object.send(field).try(:last) })
    end
  end

end