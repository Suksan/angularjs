class Gamls < Provider
  
  COUNTYLIST = "APPLING,ATKINSON,BACON,BAKER,BALDWIN,BANKS,BARROW,BARTOW,BENHILL,BERRIEN,BIBB,BLECKLEY,BRANTLEY,BROOKS,BRYAN,BULLOCH,BURKE,BUTTS,CALHOUN,CAMDEN,CANDLER,CARROLL,CATOOSA,CHARLTON,CHATHAM,CHATTAHOOCH,CHATTOOGA,CHEROKEE,CLARKE,CLAY,CLAYTON,CLINCH,COBB,COFFEE,COLQUITT,COLUMBIA,COOK,COWETA,CRAWFORD,CRISP,DADE,DAWSON,DECATUR,DEKALB,DODGE,DOOLY,DOUGHERTY,DOUGLAS,EARLY,ECHOLS,EFFINGHAM,ELBERT,EMANUEL,EVANS,FANNIN,FAYETTE,FLOYD,FORSYTH,FRANKLIN,FULTON,GILMER,GLASCOCK,GLYNN,GORDON,GRADY,GREENE,GWINNETT,HABERSHA,HALL,HANCOCK,HARALSON,HARRIS,HART,HEARD,HENRY,HOUSTON,IRWIN,JACKSON,JASPER,JEFFDAVIS,JEFFERSON,JENKINS,JOHNSON,JONES,LAMAR,LANIER,LAURENS,LEE,LIBERTY,LINCOLN,LONG,LOWNDES,LUMPKIN,MACON,MADISON,MARION,MCDUFFIE,MCINTOSH,MERIWETHER,MILLER,MITCHELL,MONROE,MONTGOMERY,MORGAN,MURRAY,MUSCOGEE,NEWTON,OCONEE,OGLETHORPE,ALABAMAOTH,AKOTH,AZOTH,AROTH,CAOTH,COOTH,CTOTH,DEOTH,DCOTH,FLORIDAOTH,HIOTH,IDOTH,ILOTH,INOTH,IAOTH,KSOTH,KYOTH,LAOTH,MEOTH,MDOTH,MAOTH,MIOTH,MNOTH,MSOTH,MOOTH,MTOTH,NEOTH,NVOTH,NHOTH,NJOTH,NMOTH,NYOTH,NCAROLINOTH,NDOTH,OHOTH,OKOTH,OROTH,PAOTH,RIOTH,SCAROLINOTH,SDOTH,TENNESSEOTH,TXOTH,UTOTH,VTOTH,VAOTH,WAOTH,WVOTH,WIOTH,WYOTH,PAULDING,PEACH,PICKENS,PIERCE,PIKE,POLK,PULASKI,PUTNAM,QUITMAN,RABUN,RANDOLPH,RICHMOND,ROCKDALE,SCHLEY,SCREVEN,SEMINOLE,SPALDING,STEPHENS,STEWART,SUMTER,TALBOT,TALIAFERRO,TATTNALL,TAYLOR,TELFAIR,TERRELL,THOMAS,TIFT,TOOMBS,TOWNS,TREUTLEN,TROUP,TURNER,TWIGGS,UNION,UPSON,WALKER,WALTON,WARE,WARREN,WASHINGTON,WAYNE,WEBSTER,WHEELER,WHITE,WHITFIELD,WILCOX,WILKES,WILKINSON,WORTH"
  CITYLIST = "ABBEVI,ACWORT,ADAIRS,ADEL,ADRIAN,AILEY,ALAMO,ALAPAH,ALBANY,ALDORA,ALLENH,ALLENT,ALMA,ALPHAR,ALSTON,ALTO,AMBROS,AMERIC,ANDERS,APPLIN,ARABI,ARAGON,ARCADE,ARGYLE,ARLING,ARMUCH,ARNOLD,ASHBUR,ATHENS,ATLANT,ATTAPU,AUBURN,AUGUST,AUSTEL,AVALON,AVERA,AVONDA,AXSON,BACONT,BAINBR,BALDWN,BALLGR,BARNES,BARNEY,BARTOW,BARWIC,BAXLEY,BELLVI,BELVED,BERKEL,BERLIN,BETHLE,BETWEN,BIBBCT,BIGCAN,BISHOP,BLCKSH,BLACKS,BLAINE,BLAIRS,BLAKEL,BLOOMI,BLUE,BLUFFT,BLYTHE,BOGART,BOLING,BONAIR,BONANZ,BONEVI,BOSTON,BOSTWI,BOWDON,BOWJUN,BOWERS,BOWMAN,BOXSPR,BRASEL,BRASWE,BREMEN,BRINSO,BRISTO,BRONWO,BROOKF,BROOKH,BROOKL,BROOKS,BROXTO,BRUNSW,BUCHAN,BUCKED,BUENAV,BUFORD,BUTLER,BYROMV,BYRON,CADWEL,CAIRO,CALHOU,CALVAR,CAMAK,CAMILL,CANDLE,CANDLM,CANON,CANTON,CARL,CARLTO,CARNES,CARROL,CARTEC,CARTER,CASSVI,CATAUL,CAVESP,CECIL,CEDARS,CEDART,CENTER,CENTRA,CHAMBL,CHATAH,CHATTS,CHATTA,CHAUNC,CHERRY,CHESTE,CHESTN,CHICKA,CHULA,CISCO,CLARKD,CLRKSV,CLARKS,CLAXTO,CLAYTN,CLERMO,CLEVEL,CLIMAX,CLINCH,CLOUD,CLYO,COBB,COBBTO,COCHRA,COHUTT,COLBER,COLEMA,COLLEG,COLLIN,COLQUI,COLUMB,COMER,COMMER,CONCOR,CONLEY,CONYER,COOLID,COOSA,CORDEL,CORINT,CORNEL,COTTON,COUNTR,COVING,CRABAP,CRANDA,CRAWFO,CRAWFV,CRESCE,CULLOD,CUMMIN,CUSSET,CUTHBE,DACULA,DAHLON,DAISY,DALLAS,DALTON,DAMASC,DANIEL,DANVIL,DARIEN,DASHER,DAVISB,DAWSN,DAWSON,DESOTO,DEARIN,DECATR,DEENWO,DEEPST,DEMORE,DENTON,DEWYRO,DEXTER,DILARD,DIXIE,DOCKJU,DOERUN,DONALS,DOOLIN,DORAVI,DOUGLS,DOUGLA,DOVER,DRUIDH,DRYBRA,DUPONT,DUBLIN,DUDLEY,DULUTH,DUNWDY,EASTBO,EASTDU,EASTEL,EASTGR,EASTNE,EASTPT,EASTON,EASTMA,EVILLE,EATON,EBENEZ,EDEN,EDGEHI,EDISON,ELBERT,ELKO,ELLABE,ELLAVI,ELLENT,ELLENW,ELLERS,ELLIJAY,EMERSN,ENIGMA,EPHESU,EPWORT,ESOMHI,ETON,EUHARL,EVANS,EXPERI,FAIROA,FAIRPL,FAIRBN,FAIRMT,FAIRVI,FARGO,FARMIN,FAYETT,FELTON,FITZGE,FLEMIG,FLEMIN,FLINTS,FLOVIL,FLOWER,FOLKST,FOREST,FORSYT,FRTBEN,FORTGA,FRTGOR,FRTOGL,FRTSTE,FRTVAL,FORTSO,FOWLST,FRANKN,FRANKL,FREEHM,FUNSTO,GAINSC,GAINES,GAINSV,GARDEN,GARFIE,GAY,GENEVA,GEORGE,GIBSON,GILLSV,GIRARD,GLENN,GLENNV,GLENWO,GOOD,GORDON,GOUGH,GRACEW,GRAHAM,GRNTV,GRAVES,GRAY,GRAYSN,GRAYSV,GREENS,GREENV,GRESHA,GRIFFI,GROVET,GUMBRA,GUMLOG,GUYTON,HADDOC,HAGAN,HAHIRA,HAMILT,HAMPTO,HANNAH,HAPEVI,HARALS,HARDWI,HARLEM,HARRIS,HARTSF,HARTWE,HAWKIN,HAZLEH,HELEN,HELENA,HEPHZI,HIAWAS,HIGGST,HIGHSH,HILLSB,HILLTO,HINESV,HIRAM,HOBOKE,HOGANS,HOLLYS,HOMELA,HOMER,HOMERV,HORTEN,HOSCTN,HOWARD,HULL,IDEAL,ILA,INDIAN,INMAN,IRONCI,IRONDA,IRWINT,IRWINV,ISLEOF,IVEY,JACKSO,JCKSNV,JAKIN,JASPER,JEFFER,JEFFRV,JEKYLL,JENKIN,JERSEY,JESUP,JEWELL,JOHCRK,JONESE,JULIET,JUNCTI,KATHLE,KENNES,KEYSVI,KINGSB,KINGSL,KINGST,KITE,KNOXVI,LAFAYE,LAGRAN,LAKECI,LAKEPA,SPIVEY,LAKELA,LAKEMT,LAKEVI,LAKEVW,LAVONI,LAWREN,LEARY,LEBANO,LEESBU,LENOX,LESLIE,LEXING,LILBUR,LILLY,LINCOL,LINDAL,LINWOO,LITHIA,LITHON,LIZELL,LOCUST,LOGANV,LONEOA,LOOKOU,LOUISV,LOUVAL,LOVEJO,LUDOWI,LULA,LUMBER,LUMPKI,LUTHER,LYERLY,LYONS,MABLET,MACON,MADISN,MANASS,MANCHE,MANOR,MANSFI,MARBLE,MARIET,MARSHA,MARTIN,MARTNZ,MATT,MATTHE,MAUK,MAXEYS,MAYSVI,MCCAYS,MCDONO,MCINTY,MCRAE,MEANSV,MEIGS,MELDRI,MENLO,MERIDI,MERSHO,MESENA,METTER,MIDLAN,MIDVIL,MIDWAY,MDWYHR,MILAN,MILLED,MILLEN,MILLWO,MILNER,MILTON,MINERA,MITCHE,MOLENA,MONROE,MONTEZ,MONTGO,MONTIC,MONTRO,MOODYA,MORELA,MRGAN,MORGAN,MORRIS,MORROW,MORVEN,MOULTR,MOUNTA,MOUNTB,MOUNTV,MOUNTZ,MNTCTY,MNTPRK,MURRAY,MUSELL,MYSTIC,NAHUNT,NASHVI,NAYLOR,NELSON,NEWBOR,NEWHOP,NEWING,NEWNAN,NEWTON,NICHLL,NICHOL,NORCRO,NORMAN,NORRIS,NRTATL,NRTDEC,NRTDRU,NHIGHS,NORTHM,NORWOO,NUNEZ,OAKPAR,OAKFIE,OAKHIL,OAKMAN,OAKWOD,OCHLOC,OCILLA,OCONEE,ODUM,OFFERM,OGLETH,OLIVER,OMAHA,OMEGA,ORCHAR,OTHREA,OTHRNO,OTHRSO,OTHRST,OTHRWE,OTHRAL,OTHRFL,OTHRNC,OTHRSC,OTHRTN,OXFORD,PALMET,PANTHE,PARROT,PATTER,PAVO,PAYNE,PEACHT,PEACHC,PEARSO,PELHAM,PEMBRO,PENDER,PERKIN,PERRY,PHILLI,PINELK,PINEMT,PINEMV,PINEHU,PINEVI,PITTS,PLAINF,PLAINS,PLAINV,POOLER,PORTWE,PORTAL,PORTER,POULAN,POWDER,PRESTO,PULASK,PUTNEY,QUITMA,RABUN,RANGER,RAOUL,RAYCIT,RAYLE,REBECC,REDOAK,REDAN,REEDCR,REGIST,REIDSV,REMERT,RENTZ,RESACA,RESTHA,REX,REYNOL,RHINE,RICEBO,RICHLA,RICHMO,RIDDLE,RINCON,RINGGO,RISING,RIVERD,RIVERS,ROBERT,ROBINS,ROCHEL,ROCKSP,ROCKLE,ROCKMA,ROCKY,ROCKYF,ROME,ROOPVI,ROSSVI,ROSWEL,ROYSTN,RUPERT,RUSSEL,RUTLED,RYDAL,SAINTG,SALECI,SANDHI,SANDER,SANDYS,SANTAC,SAPELO,SARDIS,SARGEN,SASSER,SAUTEE,SAVANN,SCOTLA,SCOTTS,SCREVE,SEAISL,SENOA,SEVILL,SHADYD,SHANNO,SHARON,SHRPS,SHELLM,SHILOH,SILOAM,SILCRK,SKIDAW,SKYVAL,SMARR,SMITHV,SMOKER,SMYRNA,SNELLV,SOCIAL,SOPERT,SOUTHA,SPARKS,SPARTA,SPRING,ST.MAR,ST.SIM,STAPLE,STATEN,STATES,STATHA,STEPHE,STILES,STILLM,STOCKB,STOCKT,STONEM,SUCHES,SUGARH,SUGARV,SMRTWN,SMMRVL,SUMNER,SUNNYS,SURREN,SUWANE,SWAINS,SYCAMO,SYLVAN,SYLVES,TALBOT,TALKIN,TALLAP,TALLUL,TALMO,TARRYT,TATE,TAYLOR,TEMPLE,TENNGA,TENNIL,THEROC,THMSTN,THMSVL,THOMSO,THUNDE,TIFTON,TIGER,TIGNAL,TOCCOA,TOCFAL,TOOMSB,TOWNSE,TRENTO,TRION,TUCKER,TUNNEL,TURIN,TURNER,TWINCI,TYTY,TYBEEI,TYRONE,UNADIL,UNION,UNIONP,UNIONV,UPATOI,UVALDA,VALDOS,VARNEL,VERNON,VIDALI,VIENNA,VILLA,VINING,WACO,WADLEY,WALESK,WALNUT,WALTHO,WARESB,WARMSP,WARNER,WARREN,WARTHE,WARWIC,WASHIN,WATKIN,WAVRLY,WAVERL,WAYCRO,WAYNES,WAYNBO,WESTAU,WESTGR,WESTPO,WESTON,WESTSI,WHIGHA,WHITE,WHOAK,WHITEP,WHITEM,WHITES,WILDWO,WILEY,WILLAC,WILLIA,WILMIN,WINDER,WINSTN,WINTER,WOODBI,WOODBU,WOODLA,WOODST,WOODVI,WOOLSE,WRAY,WRENS,WRIGHT,YATESV,YORKVI,YOUNGH,ZEBULO"
  CON = {
     "A" => "Aluminum/Vinyl",
      "O" => "Brick 3 Sided",
      "B" => "Brick 4 Sided",
      "P" => "Brick Front",
      "C" => "Brick/Frame",
      "L" => "Concrete Siding",
      "D" => "Log",
      "K" => "Other (See Remarks)",
      "E" => "Press Board Siding",
      "F" => "Rough-Sawn Siding",
      "Q" => "Steel Frame",
      "G" => "Stone",
      "H" => "Stone/Frame",
      "M" => "Stucco EIFS",
      "N" => "Stucco Real",
      "I" => "Stucco Unspecified",
      "R" => "Tabby",
      "J" => "Wood Siding"
    
  }
  
  CON_CM = {
      "A" => "Block",
      "B" => "Brick",
      "C" => "Frame",
      "H" => "Front/Other",
      "F" => "Other (See Remarks)",
      "G" => "Precast Concrete",
      "J" => "Precast Stone",
      "I" => "Steel & Glass",
      "D" => "Steel/Metal",
      "K" => "Stucco EIFS",
      "L" => "Stucco Real",
      "E" => "Stucco Unspecified"
  }
  
  EQP = {
    "A" => "Alarm - Burglar",
    "K" => "Alarm - Carbon Monoxide",
    "B" => "Alarm - Smoke/Fire",
    "C" => "Central Vacuum",
    "D" => "Dryer",
    "E" => "Electric Air Filter",
    "J" => "Fire Sprinkler",
    "L" => "Generator",
    "F" => "Intercom/Radio",
    "G" => "Satellite Dish",
    "H" => "Washer",
    "I" => "Water Softener"
  }
  
  EQP_KITCHEN = {
    "N" => "Convection Oven",
    "A" => "Cooktop - Separate",
    "B" => "Dishwasher",
    "K" => "Double Oven",
    "C" => "Garbage Disposal",
    "D" => "Icemaker Line",
    "E" => "Indoor Grill",
    "F" => "Microwave - Built In",
    "M" => "None",
    "L" => "Other (See Remarks)",
    "G" => "Oven - Wall",
    "H" => "Range/Oven",
    "I" => "Refrigerator",
    "J" => "Trash Compactor"
  }
  
  INT_ENUM = {
    "A" => "Attic Expandable",
    "B" => "Bookcases",
    "D" => "Cable In Street",
    "C" => "Cable TV Connections",
    "E" => "Ceilings - Trey",
    "F" => "Ceilings - Vaulted",
    "G" => "Ceilings 9 Ft Plus",
    "W" => "Double Vanity",
    "U" => "Elevator",
    "1" => "Exposed Beams",
    "I" => "Foyer - 2 Story",
    "H" => "Foyer - Entrance",
    "X" => "Garden Tub",
    "K" => "Gas Logs",
    "R" => "Handicap Access",
    "J" => "Hardwood Floors",
    "S" => "Other (See Remarks)",
    "L" => "Pulldown Attic Stairs",
    "O" => "Rear Stairs",
    "V" => "Recently Renovated",
    "T" => "Sauna",
    "Z" => "Separate Shower",
    "M" => "Skylight",
    "2" => "Tile Bath",
    "3" => "Tile Floors",
    "Y" => "Walk-in Closet",
    "N" => "Wall-to-wall Carpet",
    "P" => "Wet Bar",
    "Q" => "Whirlpool Bath"
  }

  ROOMS_ENUM = {
    "A" => "Bonus Room",
    "B" => "Den",
    "D" => "Dining Rm/Living Rm Combo",
    "C" => "Dining Room - L Shaped",
    "T" => "Dining Room Seats 12+",
    "E" => "DR - Separate",
    "P" => "Exercise Room",
    "F" => "Family Room",
    "G" => "Great Room",
    "H" => "In-law Suite or Apartment",
    "I" => "Library/Office",
    "Q" => "Loft",
    "J" => "LR Separate J J",
    "K" => "Master On Main Level",
    "O" => "Other (See Remarks)",
    "L" => "Rec Room",
    "R" => "Roommate Plan",
    "M" => "Solarium/Sun Room",
    "S" => "Split Bedroom Plan",
    "N" => "Stubbed Bath",
    "U" => "Theater/Media Room",
    "V" => "Wine Cellar"
  }

  LOOKUP_TYPE_ENUMS = {
    :basement_desc => {
      "A" => "Bath Finished",
      "B" => "Bath Stubbed",
      "C" => "Block",
      "N" => "Boat Door",
      "D" => "Concrete",
      "E" => "Crawlspace",
      "F" => "Daylight",
      "G" => "Earthen",
      "H" => "Entrance - Inside",
      "I" => "Entrance - Outside",
      "J" => "Finished Rooms",
      "K" => "Full",
      "L" => "Partial",
      "O" => "Pier",
      "M" => "Slab/None"
    },
    
    # :building_stories => {
      # "A" => "1 Story",
      # "B" => "1.5 Stories",
      # "C" => "2 Stories",
      # "E" => "Multi - Level",
      # "D" => "Over 2 Stories",
      # "F" => "Split Foyer",
      # "G" => "Split Level"
    # },

    :cooling_system_type => {
      "A" => "Ceiling Fan",
      "B" => "Central",
      "I" => "Common",
      "C" => "Heat Pump",
      "G" => "No Cooling",
      "F" => "Other (See Remarks)",
      "D" => "Whole House Fan",
      "H" => "Window Units",
      "E" => "Zoned/Dual"
     },
     :disclosures => {
       "I" => "Agent Owned",
        "K" => "Agent Related to Seller",
        "O" => "As Is",
        "B" => "Bank Owned",
        "E" => "Corporate Relocation",
        "D" => "Estate Owned",
        "G" => "Fixer Upper",
        "A" => "Foreclosure",
        "N" => "Government Owned",
        "J" => "Historic",
        "C" => "Investor Owned",
        "P" => "No Disclosure",
        "M" => "Pre-Foreclosure",
        "H" => "Recently Renovated",
        "F" => "Rental",
        "L" => "Short Sale"
     },
     :exterior_description => {
        "A" => "Balcony",
        "O" => "Barn",
        "B" => "Deck/Patio",
        "V" => "Dog Run/Pen",
        "C" => "Fenced Yard",
        "D" => "Garden Area",
        "E" => "Gas Grill",
        "P" => "Gazebo",
        "F" => "Green House",
        "G" => "Hot Tub",
        "N" => "Other (See Remarks)",
        "H" => "Out Building",
        "X" => "Outdoor Kitchen",
        "Q" => "Pool House",
        "U" => "Pool Screened/Enclosed",
        "I" => "Pool-above Ground",
        "J" => "Pool-in Ground",
        "S" => "Porch",
        "K" => "Screen Porch",
        "L" => "Sprinkler System",
        "R" => "Stables",
        "M" => "Tennis Court",
        "W" => "Veranda",
        "Y" => "Water Feature",
        "T" => "Workshop"

     },

     :legal_description => {
      "L" => "City View",
      "A" => "Corner",
      "B" => "Cul De Sac",
      "C" => "Golf Course",
      "D" => "Greenbelt",
      "E" => "Level Lot",
      "M" => "Mountain View",
      "H" => "None",
      "I" => "Open Land",
      "J" => "Private Backyard",
      "F" => "Sloping",
      "K" => "Valley View",
      "G" => "Wooded"
     },


     :lot_description => {
      "L" => "City View",
      "A" => "Corner",
      "B" => "Cul De Sac",
      "C" => "Golf Course",
      "D" => "Greenbelt",
      "E" => "Level Lot",
      "M" => "Mountain View",
      "H" => "None",
      "I" => "Open Land",
      "J" => "Private Backyard",
      "F" => "Sloping",
      "K" => "Valley View",
      "G" => "Wooded"
     },



     :roof_type => {
      "A" => "Composition",
      "G" => "Concrete",
      "J" => "Copper",
      "E" => "Metal/Steel",
      "H" => "Other (See Remarks)",
      "D" => "Slate",
      "F" => "Tar/Gravel",
      "C" => "Tile",
      "I" => "Tin",
      "B" => "Wood Shingle"
     },

     :showing_instructions => {
      "N" => "24 Hour Access",
      "M" => "9 AM to 10 PM",
      "A" => "Appt Agent",
      "B" => "Appt Owner",
      "C" => "Appt Tenant",
      "D" => "Call Agent Only",
      "E" => "Caution Alarm",
      "F" => "Caution Pet",
      "G" => "Courtesy Call - Leave Msg",
      "P" => "Day Sleeper",
      "H" => "Key In Office",
      "I" => "Lockbox GAMLS Compatible",
      "Q" => "Lockbox Non-GAMLS Compat",
      "J" => "See Remarks",
      "O" => "Special Hours",
      "L" => "Under Construction",
      "K" => "Vacant"
     },

     

     :style_of_building => {
        "Q" => "A-frame",
        "V" => "Adirondack",
        "A" => "Bungalow/Cottage",
        "T" => "Cabin",
        "B" => "Cape Cod",
        "U" => "Cluster",
        "C" => "Colonial",
        "D" => "Contemporary",
        "E" => "Country/Rustic",
        "R" => "Craftsman",
        "F" => "European",
        "G" => "French Provincial",
        "Y" => "Garden/Patio Home",
        "W" => "High Rise",
        "S" => "Loft",
        "H" => "Mediterranean",
        "X" => "Mid Rise",
        "O" => "Mobile Home",
        "P" => "Modular Home",
        "L" => "Other (See Remarks)",
        "M" => "Ranch",
        "I" => "Traditional",
        "J" => "Tudor",
        "K" => "Victorian"
     }


  }

  

  DATA_ENUMS = {
    category: hash_from_csv('categories'),
    # lot_use: hash_from_csv('lot_uses'),
    rental_class: hash_from_csv('rental_classes'),
    # rental_type: hash_from_csv('rental_types'),
    # single_family_type: hash_from_csv('single_family_types'),
    status: hash_from_csv('statuses'),
    # state: hash_from_csv('states'),
    # hoa_bill_freq: hash_from_csv('hoa_bill_freqs')
    mls_area_name: hash_from_csv('mls_areas'),
    mls_elementary_school: hash_from_csv('mls_elementary_schools'),
    mls_middle_school: hash_from_csv('mls_middle_schools'),
    mls_high_school: hash_from_csv('mls_high_schools')
  }
  DATA_MAPPING = {
    access_to_complex: 'ComplexAcc',
    acreage: 'TOT_ACRES',
    bathrooms: proc { |d| [ d['BAF'], d['BAH'] ].compact.join(" ") },
    bathrooms_full: 'BAF',
    bathrooms_half: 'BAH',
    beds: 'BR',
    building_name: 'BLDG',
    building_stories: proc { |d| 
      field = d['STO'].to_s.split(",").try(:first) 
      if field   
        case field
        when "A"
          "1"
        when "B"
          "1.5"
        when "C"
          "2"
        else
          ""
        end
      end
    },
    category: 'TYP',
    cdom: 'MT',
    city: 'CITY',
    closed_date: proc { |d| if (d['TYP'].to_s == 'RR') && (d['STAT'].in?(['RL','S'])) then to_date_sql(d['OMD']) else  to_date_sql(d['CLD'])  end },
    county: 'CNTY',
    disclosures: 'ONCN',
    dom: proc {|d| d['MT']||0 },
    exemption: 'ZONE',
    expiration_date: proc { |d| to_date_sql(d['XD']) },
    financing_available: 'FIN',
    floor_type: 'FLR',
    garage_capacity: 'GAR',
    heating_system: 'HeatingTyp',
    interior_description: proc{ |d|
      [
        d['INT'].to_s.split(",").map{ |u| INT_ENUM[u.to_s] }.reject(&:blank?).join(', '),
        d['RMS'].to_s.split(",").map{ |u| ROOMS_ENUM[u.to_s] }.reject(&:blank?).join(', ')
      ].reject(&:blank?).join(', ')
    },
    key_map_page: 'DeedPage',
    land_size: 'ACDM',
    latitude:  proc { |d| d['LATITUDE'] ? d['LATITUDE'].to_d : nil },
    legal_description: 'LTDS',
    list_price: proc { |d| d['LP']||d['RR'] },
    list_price_psf: 'PricePerSQ',
    listing_agent_email: 'ID7D86A7AE',
    listing_agent_id: 'IDE2E1D9A0',
    listing_agent_name: 'LAName',
    listing_broker_id: 'IDD96B35B1',
    listing_broker_name: 'IDE2E1D9A0',
    listing_agent_phone: 'IDD2E0CBF1',
    listing_date: proc { |d| to_date_sql(d['LD']) },
    longitude:  proc { |d|  d['LONGITUDE'] ? d['LONGITUDE'].to_d : nil },
    lot: proc { |d| d['LL']||d['LOT'] },
    # lot_description: 'LTDS',
    lot_square_feet: proc { |d| ( d['LTSZ'] && ( /^[\d]+(\.[\d]+){0,1}$/ === d['LTSZ'] ) ) ? d['LTSZ']  : ( !d['TOT_ACRES'] ?  '0' : ( d['TOT_ACRES'].to_d * 43560 ).to_i.to_s ) },
    key_map_book: 'DeedBook',
    key_map_page: 'DeedPage',
    mls_area_key: 'AR',
    mls_area_name: 'AR',
    mls_number: proc { |d|   d['LN'].to_s.gsub(/^0/, '')   },
    new_construction: 'NCO',
    number_of_photos: 'PhotoCount',
    office_phone: 'IDFF81B788',
    original_list_price: 'OLP',
    owner_name: 'OWN',
    public_remarks: 'PUBLIC_REMARKS',
    realtor_remarks: 'PRIVATE_REMARKS',
    rental_class: 'DSCR',
    roof_type:  'ROOF',
    # school_district: proc { |d| [ d["GSD"],
                                  # d["JHD"],
                                  # d["HSD"] ].compact.join(" ") },
    showing_instructions: 'SHO',
    siding_type:  proc{ |d|
      [
        d['CON_CM'].to_s.split(",").map{ |u| CON_CM[u.to_s] }.reject(&:blank?).join(', '),
        d['CON'].to_s.split(",").map{ |u| CON[u.to_s] }.reject(&:blank?).join(', ')
      ].reject(&:blank?).join(', ')
    }, 
    
    # proc { |d| d['CON_CM']||d['CON'] },
    sold_price: 'SP',
    sold_lease_price: 'RP',
    square_feet: 'SQFT_TOT',
    status: 'STAT',
    stories: 'STO',
    street_name: 'STR',
    street_number: 'STN',
    style_of_building: 'STY',
    state: 'STATE',
    subdivision: 'SUBD',
    tax_amount: 'TAX',
    tax_id: 'TID',
    unit_number: 'UnitDescri',
    year_built: 'YR',
    selling_agent_id: 'SALicense',
    mls_record_modified: proc {|d| to_date_sql(d['RECMODDATE']) },
    mls_record_creation_date: proc {|d| to_date_sql(d['RECMODDATE']) },
    equipment:  proc{ |d|
      [
        d['EQP'].to_s.split(",").map{ |u| EQP[u.to_s] }.reject(&:blank?).join(', '),
        d['EQP_Kitchen'].to_s.split(",").map{ |u| EQP_KITCHEN[u.to_s] }.reject(&:blank?).join(', ')
      ].reject(&:blank?).join(', ')
    }, 
    exterior_description: 'EXT',
    # pool_area: proc{ |d|
      # if [ 'U', 'I', 'J' ].include?(d['EXT']) then 'Y'
      # else 'N' end
    # },
    pool_area: proc {|d| if d['EXT'].to_s.split(",").select { |e| e.in?(['U','I','J']) }.length > 0  then "Y" else "N" end },
    # pool_private: proc{ |d|
      # if [ 'U', 'I', 'J' ].include?(d['EXT']) then 'Y'
      # else 'N' end
    # },
    pool_private: proc {|d| if d['EXT'].to_s.split(",").select { |e| e.in?(['U','I','J']) }.length > 0  then "Y" else "N" end },
    fireplace_number: 'FP',
    maintenance_fee: "FEE_AMT",
    annual_maintenance_fee: "FEE_AMT",

    #   payment = nil;
    #   freq = nil;
    #   if (d['ASM'] && d['FREQ']) then
    #     payment = d['ASM'].to_d ;
    #     freq =  d['FREQ']
    #   end;
    #   if freq == 'A' then (payment*1).to_s
    #   elsif freq == 'M' then (payment*12).to_s
    #   elsif freq == 'N' then (payment*1).to_s
    #   elsif freq == 'Q' then (payment*4).to_s
    #   elsif freq == 'V' then (payment*1).to_s
    #   else nil
    #   end
    # },
    zip_4: 'ZIP4',
    zip_code: 'ZIP',
    # back_on_market: proc { |d| to_date_sql(d['RTMD']) },
    basement_desc: 'BSMT',
    owner_phone: 'OWNP',
    road_surface: 'RDFR',
    # sale_price: 'SP',
    selling_agent_email: 'IDA3F6F824',
    selling_agent_name: 'LAName',
    selling_agent_phone: 'IDF9511C67',
    termination_date: proc { |d| to_date_sql(d['OMD']) },
    tax_year: 'TXYR',
    township: 'DIST',
    waterfront: 'Waterfront',
    withdrawn_date: proc { |d| to_date_sql(d['OMD']) },
    cooling_system_type: 'CoolingType',
    listing_agent_fax: 'IDD2E0CBF1',
    road_surface: 'RDFR',
    hirise_fees: 'CAM',
    multi_owner_operating: 'GEX',
    muti_gross_operating_income: 'GIN',
    heating_system: 'HeatingTyp',
    buyer_agent_compensation: 'ComDollar',
    lot_back_dimensions: 'LTDM',
    pending_date: proc { |d| to_date_sql(d['SD']) },
    mls_elementary_school: 'ES',
    mls_middle_school: 'MS',
    mls_high_school: 'HS',
    mls_elementary_school_number: 'ES',
    mls_middle_school_number: 'MS',
    mls_high_school_number: 'HS',
    sub_category: proc { |d| "1" }

  }

  LOOKUP_TYPE_ENUMS2 = {
     :basement_desc => {
        "BATH" => "Bath",
        "BTHST" => "Bath/Stubbed",
        "BTDOR" => "Boat Door",
        "CRAWL" => "Crawl Space",
        "DAYLT" => "Daylight",
        "DRVWY" => "Driveway Access",
        "EXTEN" => "Exterior Entry",
        "FINSH" => "Finished",
        "FULL" => "Full",
        "INTER" => "Interior Entry",
        "PARTL" => "Partial",
        "SLABN" => "Slab/None",
        "UNFIN" => "Unfinished"
     },
     :city => {
        "ABBEVI" => "Abbeville",
        "ACWORT" => "Acworth",
        "ADAIRS" => "Adairsville",
        "ADEL" => "Adel",
        "ADRIAN" => "Adrian",
        "AILEY" => "Ailey",
        "ALAMO" => "Alamo",
        "ALAPAH" => "Alapaha",
        "ALBANY" => "Albany",
        "ALDORA" => "Aldora",
        "ALLENH" => "Allenhurst",
        "ALLENT" => "Allentown",
        "ALMA" => "Alma",
        "ALPHAR" => "Alpharetta",
        "ALSTON" => "Alston",
        "ALTO" => "Alto",
        "AMBROS" => "Ambrose",
        "AMERIC" => "Americus",
        "ANDERS" => "Andersonville",
        "APPLIN" => "Appling",
        "ARABI" => "Arabi",
        "ARAGON" => "Aragon",
        "ARCADE" => "Arcade",
        "ARGYLE" => "Argyle",
        "ARLING" => "Arlington",
        "ARMUCH" => "Armuchee",
        "ARNOLD" => "Arnoldsville",
        "ASHBUR" => "Ashburn",
        "ATHENS" => "Athens",
        "ATLANT" => "Atlanta",
        "ATTAPU" => "Attapulgus",
        "AUBURN" => "Auburn",
        "AUGUST" => "Augusta",
        "AUSTEL" => "Austell",
        "AVALON" => "Avalon",
        "AVERA" => "Avera",
        "AVONDA" => "Avondale Estates",
        "AXSON" => "Axson",
        "BACONT" => "Baconton",
        "BAINBR" => "Bainbridge",
        "BALDWN" => "Baldwin",
        "BALLGR" => "Ball Ground",
        "BARNES" => "Barnesville",
        "BARNEY" => "Barney",
        "BARTOW" => "Bartow",
        "BARWIC" => "Barwick",
        "BAXLEY" => "Baxley",
        "BELLVI" => "Bellville",
        "BELVED" => "Belvedere Park",
        "BERKEL" => "Berkeley Lake",
        "BERLIN" => "Berlin",
        "BETHLE" => "Bethlehem",
        "BETWEN" => "Between",
        "BIBBCT" => "Bibb City",
        "BIGCAN" => "Big Canoe",
        "BISHOP" => "Bishop",
        "BLCKSH" => "Blackshear",
        "BLACKS" => "Blacksville",
        "BLAINE" => "Blaine",
        "BLAIRS" => "Blairsville",
        "BLAKEL" => "Blakely",
        "BLOOMI" => "Bloomingdale",
        "BLUE" => "Blue Ridge",
        "BLUFFT" => "Bluffton",
        "BLYTHE" => "Blythe",
        "BOGART" => "Bogart",
        "BOLING" => "Bolingbroke",
        "BONAIR" => "Bonaire",
        "BONANZ" => "Bonanza",
        "BONEVI" => "Boneville",
        "BOSTON" => "Boston",
        "BOSTWI" => "Bostwick",
        "BOWDON" => "Bowdon",
        "BOWJUN" => "Bowdon Junction",
        "BOWERS" => "Bowersville",
        "BOWMAN" => "Bowman",
        "BOXSPR" => "Box Springs",
        "BRASEL" => "Braselton",
        "BRASWE" => "Braswell",
        "BREMEN" => "Bremen",
        "BRINSO" => "Brinson",
        "BRISTO" => "Bristol",
        "BRONWO" => "Bronwood",
        "BROOKF" => "Brookfield",
        "BROOKH" => "Brookhaven",
        "BROOKL" => "Brooklet",
        "BROOKS" => "Brooks",
        "BROXTO" => "Broxton",
        "BRUNSW" => "Brunswick",
        "BUCHAN" => "Buchanan",
        "BUCKED" => "Buckhead",
        "BUENAV" => "Buena Vista",
        "BUFORD" => "Buford",
        "BUTLER" => "Butler",
        "BYROMV" => "Byromville",
        "BYRON" => "Byron",
        "CADWEL" => "Cadwell",
        "CAIRO" => "Cairo",
        "CALHOU" => "Calhoun",
        "CALVAR" => "Calvary",
        "CAMAK" => "Camak",
        "CAMILL" => "Camilla",
        "CANDLE" => "Candler",
        "CANDLM" => "Candler-Mcafee",
        "CANON" => "Canon",
        "CANTON" => "Canton",
        "CARL" => "Carl",
        "CARLTO" => "Carlton",
        "CARNES" => "Carnesville",
        "CARROL" => "Carrollton",
        "CARTEC" => "Cartecay",
        "CARTER" => "Cartersville",
        "CASSVI" => "Cassville",
        "CATAUL" => "Cataula",
        "CAVESP" => "Cave Spring",
        "CECIL" => "Cecil",
        "CEDARS" => "Cedar Springs",
        "CEDART" => "Cedartown",
        "CENTER" => "Centerville",
        "CENTRA" => "Centralhatchee",
        "CHAMBL" => "Chamblee",
        "CHATAH" => "Chattahoochee Hills",
        "CHATTS" => "Chatsworth",
        "CHATTA" => "Chattanooga Valley",
        "CHAUNC" => "Chauncey",
        "CHERRY" => "Cherrylog",
        "CHESTE" => "Chester",
        "CHESTN" => "Chestnut Mountain",
        "CHICKA" => "Chickamauga",
        "CHULA" => "Chula",
        "CISCO" => "Cisco",
        "CLARKD" => "Clarkdale",
        "CLRKSV" => "Clarkesville",
        "CLARKS" => "Clarkston",
        "CLAXTO" => "Claxton",
        "CLAYTN" => "Clayton",
        "CLERMO" => "Clermont",
        "CLEVEL" => "Cleveland",
        "CLIMAX" => "Climax",
        "CLINCH" => "Clinchfield",
        "CLOUD" => "Cloudland",
        "CLYO" => "Clyo",
        "COBB" => "Cobb",
        "COBBTO" => "Cobbtown",
        "COCHRA" => "Cochran",
        "COHUTT" => "Cohutta",
        "COLBER" => "Colbert",
        "COLEMA" => "Coleman",
        "COLLEG" => "College Park",
        "COLLIN" => "Collins",
        "COLQUI" => "Colquitt",
        "COLUMB" => "Columbus",
        "COMER" => "Comer",
        "COMMER" => "Commerce",
        "CONCOR" => "Concord",
        "CONLEY" => "Conley",
        "CONYER" => "Conyers",
        "COOLID" => "Coolidge",
        "COOSA" => "Coosa",
        "CORDEL" => "Cordele",
        "CORINT" => "Corinth",
        "CORNEL" => "Cornelia",
        "COTTON" => "Cotton",
        "COUNTR" => "Country Club Estates",
        "COVING" => "Covington",
        "CRABAP" => "Crabapple",
        "CRANDA" => "Crandall",
        "CRAWFO" => "Crawford",
        "CRAWFV" => "Crawfordville",
        "CRESCE" => "Crescent",
        "CULLOD" => "Culloden",
        "CUMMIN" => "Cumming",
        "CUSSET" => "Cusseta",
        "CUTHBE" => "Cuthbert",
        "DACULA" => "Dacula",
        "DAHLON" => "Dahlonega",
        "DAISY" => "Daisy",
        "DALLAS" => "Dallas",
        "DALTON" => "Dalton",
        "DAMASC" => "Damascus",
        "DANIEL" => "Danielsville",
        "DANVIL" => "Danville",
        "DARIEN" => "Darien",
        "DASHER" => "Dasher",
        "DAVISB" => "Davisboro",
        "DAWSN" => "Dawson",
        "DAWSON" => "Dawsonville",
        "DESOTO" => "De Soto",
        "DEARIN" => "Dearing",
        "DECATR" => "Decatur",
        "DEENWO" => "Deenwood",
        "DEEPST" => "Deepstep",
        "DEMORE" => "Demorest",
        "DENTON" => "Denton",
        "DEWYRO" => "Dewy Rose",
        "DEXTER" => "Dexter",
        "DILARD" => "Dillard",
        "DIXIE" => "Dixie",
        "DOCKJU" => "Dock Junction",
        "DOERUN" => "Doerun",
        "DONALS" => "Donalsonville",
        "DOOLIN" => "Dooling",
        "DORAVI" => "Doraville",
        "DOUGLS" => "Douglas",
        "DOUGLA" => "Douglasville",
        "DOVER" => "Dover",
        "DRUIDH" => "Druid Hills",
        "DRYBRA" => "Dry Branch",
        "DUPONT" => "Du Pont",
        "DUBLIN" => "Dublin",
        "DUDLEY" => "Dudley",
        "DULUTH" => "Duluth",
        "DUNWDY" => "Dunwoody",
        "EASTBO" => "East Boundary",
        "EASTDU" => "East Dublin",
        "EASTEL" => "East Ellijay",
        "EASTGR" => "East Griffin",
        "EASTNE" => "East Newnan",
        "EASTPT" => "East Point",
        "EASTON" => "Eastanollee",
        "EASTMA" => "Eastman",
        "EVILLE" => "Eastville",
        "EATON" => "Eatonton",
        "EBENEZ" => "Ebenezer",
        "EDEN" => "Eden",
        "EDGEHI" => "Edge Hill",
        "EDISON" => "Edison",
        "ELBERT" => "Elberton",
        "ELKO" => "Elko",
        "ELLABE" => "Ellabell",
        "ELLAVI" => "Ellaville",
        "ELLENT" => "Ellenton",
        "ELLENW" => "Ellenwood",
        "ELLERS" => "Ellerslie",
        "ELLIJAY" => "Ellijay",
        "EMERSN" => "Emerson",
        "ENIGMA" => "Enigma",
        "EPHESU" => "Ephesus",
        "EPWORT" => "Epworth",
        "ESOMHI" => "Esom Hill",
        "ETON" => "Eton",
        "EUHARL" => "Euharlee",
        "EVANS" => "Evans",
        "EXPERI" => "Experiment",
        "FAIROA" => "Fair Oaks",
        "FAIRPL" => "Fair Play",
        "FAIRBN" => "Fairburn",
        "FAIRMT" => "Fairmount",
        "FAIRVI" => "Fairview",
        "FARGO" => "Fargo",
        "FARMIN" => "Farmington",
        "FAYETT" => "Fayetteville",
        "FELTON" => "Felton",
        "FITZGE" => "Fitzgerald",
        "FLEMIG" => "Fleming",
        "FLEMIN" => "Flemington",
        "FLINTS" => "Flintstone",
        "FLOVIL" => "Flovilla",
        "FLOWER" => "Flowery Branch",
        "FOLKST" => "Folkston",
        "FOREST" => "Forest Park",
        "FORSYT" => "Forsyth",
        "FRTBEN" => "Fort Benning South",
        "FORTGA" => "Fort Gaines",
        "FRTGOR" => "Fort Gordon",
        "FRTOGL" => "Fort Oglethorpe",
        "FRTSTE" => "Fort Stewart",
        "FRTVAL" => "Fort Valley",
        "FORTSO" => "Fortson",
        "FOWLST" => "Fowlstown",
        "FRANKN" => "Franklin",
        "FRANKL" => "Franklin Springs",
        "FREEHM" => "Freehome",
        "FUNSTO" => "Funston",
        "GAINSC" => "Gaines School",
        "GAINES" => "Gainesville",
        "GAINSV" => "Gainesville Mills",
        "GARDEN" => "Garden City",
        "GARFIE" => "Garfield",
        "GAY" => "Gay",
        "GENEVA" => "Geneva",
        "GEORGE" => "Georgetown",
        "GIBSON" => "Gibson",
        "GILLSV" => "Gillsville",
        "GIRARD" => "Girard",
        "GLENN" => "Glenn",
        "GLENNV" => "Glennville",
        "GLENWO" => "Glenwood",
        "GOOD" => "Good Hope",
        "GORDON" => "Gordon",
        "GOUGH" => "Gough",
        "GRACEW" => "Gracewood",
        "GRAHAM" => "Graham",
        "GRNTV" => "Grantville",
        "GRAVES" => "Graves",
        "GRAY" => "Gray",
        "GRAYSN" => "Grayson",
        "GRAYSV" => "Graysville",
        "GREENS" => "Greensboro",
        "GREENV" => "Greenville",
        "GRESHA" => "Gresham Park",
        "GRIFFI" => "Griffin",
        "GROVET" => "Grovetown",
        "GUMBRA" => "Gumbranch",
        "GUMLOG" => "Gumlog",
        "GUYTON" => "Guyton",
        "HADDOC" => "Haddock",
        "HAGAN" => "Hagan",
        "HAHIRA" => "Hahira",
        "HAMILT" => "Hamilton",
        "HAMPTO" => "Hampton",
        "HANNAH" => "Hannah",
        "HAPEVI" => "Hapeville",
        "HARALS" => "Haralson",
        "HARDWI" => "Hardwick",
        "HARLEM" => "Harlem",
        "HARRIS" => "Harrison",
        "HARTSF" => "Hartsfield",
        "HARTWE" => "Hartwell",
        "HAWKIN" => "Hawkinsville",
        "HAZLEH" => "Hazlehurst",
        "HELEN" => "Helen",
        "HELENA" => "Helena",
        "HEPHZI" => "Hephzibah",
        "HIAWAS" => "Hiawassee",
        "HIGGST" => "Higgston",
        "HIGHSH" => "High Shoals",
        "HILLSB" => "Hillsboro",
        "HILLTO" => "Hilltonia",
        "HINESV" => "Hinesville",
        "HIRAM" => "Hiram",
        "HOBOKE" => "Hoboken",
        "HOGANS" => "Hogansville",
        "HOLLYS" => "Holly Springs",
        "HOMELA" => "Homeland",
        "HOMER" => "Homer",
        "HOMERV" => "Homerville",
        "HORTEN" => "Hortense",
        "HOSCTN" => "Hoschton",
        "HOWARD" => "Howard",
        "HULL" => "Hull",
        "IDEAL" => "Ideal",
        "ILA" => "Ila",
        "INDIAN" => "Indian Springs",
        "INMAN" => "Inman",
        "IRONCI" => "Iron City",
        "IRONDA" => "Irondale",
        "IRWINT" => "Irwinton",
        "IRWINV" => "Irwinville",
        "ISLEOF" => "Isle Of Hope-Dutch Island",
        "IVEY" => "Ivey",
        "JACKSO" => "Jackson",
        "JCKSNV" => "Jacksonville",
        "JAKIN" => "Jakin",
        "JASPER" => "Jasper",
        "JEFFER" => "Jefferson",
        "JEFFRV" => "Jeffersonville",
        "JEKYLL" => "Jekyll Island",
        "JENKIN" => "Jenkinsburg",
        "JERSEY" => "Jersey",
        "JESUP" => "Jesup",
        "JEWELL" => "Jewell",
        "JOHCRK" => "Johns Creek",
        "JONESE" => "Jonesboro",
        "JULIET" => "Juliette",
        "JUNCTI" => "Junction City",
        "KATHLE" => "Kathleen",
        "KENNES" => "Kennesaw",
        "KEYSVI" => "Keysville",
        "KINGSB" => "Kings Bay Base",
        "KINGSL" => "Kingsland",
        "KINGST" => "Kingston",
        "KITE" => "Kite",
        "KNOXVI" => "Knoxville",
        "LAFAYE" => "La Fayette",
        "LAGRAN" => "Lagrange",
        "LAKECI" => "Lake City",
        "LAKEPA" => "Lake Park",
        "SPIVEY" => "Lake Spivey",
        "LAKELA" => "Lakeland",
        "LAKEMT" => "Lakemont",
        "LAKEVI" => "Lakeview Estates",
        "LAKEVW" => "Lakeview",
        "LAVONI" => "Lavonia",
        "LAWREN" => "Lawrenceville",
        "LEARY" => "Leary",
        "LEBANO" => "Lebanon",
        "LEESBU" => "Leesburg",
        "LENOX" => "Lenox",
        "LESLIE" => "Leslie",
        "LEXING" => "Lexington",
        "LILBUR" => "Lilburn",
        "LILLY" => "Lilly",
        "LINCOL" => "Lincolnton",
        "LINDAL" => "Lindale",
        "LINWOO" => "Linwood",
        "LITHIA" => "Lithia Springs",
        "LITHON" => "Lithonia",
        "LIZELL" => "Lizella",
        "LOCUST" => "Locust Grove",
        "LOGANV" => "Loganville",
        "LONEOA" => "Lone Oak",
        "LOOKOU" => "Lookout Mountain",
        "LOUISV" => "Louisville",
        "LOUVAL" => "Louvale",
        "LOVEJO" => "Lovejoy",
        "LUDOWI" => "Ludowici",
        "LULA" => "Lula",
        "LUMBER" => "Lumber City",
        "LUMPKI" => "Lumpkin",
        "LUTHER" => "Luthersville",
        "LYERLY" => "Lyerly",
        "LYONS" => "Lyons",
        "MABLET" => "Mableton",
        "MACON" => "Macon",
        "MADISN" => "Madison",
        "MANASS" => "Manassas",
        "MANCHE" => "Manchester",
        "MANOR" => "Manor",
        "MANSFI" => "Mansfield",
        "MARBLE" => "Marblehill",
        "MARIET" => "Marietta",
        "MARSHA" => "Marshallville",
        "MARTIN" => "Martin",
        "MARTNZ" => "Martinez",
        "MATT" => "Matt",
        "MATTHE" => "Matthews",
        "MAUK" => "Mauk",
        "MAXEYS" => "Maxeys",
        "MAYSVI" => "Maysville",
        "MCCAYS" => "Mccaysville",
        "MCDONO" => "Mcdonough",
        "MCINTY" => "Mcintyre",
        "MCRAE" => "Mcrae",
        "MEANSV" => "Meansville",
        "MEIGS" => "Meigs",
        "MELDRI" => "Meldrim",
        "MENLO" => "Menlo",
        "MERIDI" => "Meridian",
        "MERSHO" => "Mershon",
        "MESENA" => "Mesena",
        "METTER" => "Metter",
        "MIDLAN" => "Midland",
        "MIDVIL" => "Midville",
        "MIDWAY" => "Midway",
        "MDWYHR" => "Midway-Hardwick",
        "MILAN" => "Milan",
        "MILLED" => "Milledgeville",
        "MILLEN" => "Millen",
        "MILLWO" => "Millwood",
        "MILNER" => "Milner",
        "MILTON" => "Milton",
        "MINERA" => "Mineral Bluff",
        "MITCHE" => "Mitchell",
        "MOLENA" => "Molena",
        "MONROE" => "Monroe",
        "MONTEZ" => "Montezuma",
        "MONTGO" => "Montgomery",
        "MONTIC" => "Monticello",
        "MONTRO" => "Montrose",
        "MOODYA" => "Moody Afb",
        "MORELA" => "Moreland",
        "MRGAN" => "Morgan",
        "MORGAN" => "Morganton",
        "MORRIS" => "Morris",
        "MORROW" => "Morrow",
        "MORVEN" => "Morven",
        "MOULTR" => "Moultrie",
        "MOUNTA" => "Mount Airy",
        "MOUNTB" => "Mount Berry",
        "MOUNTV" => "Mount Vernon",
        "MOUNTZ" => "Mount Zion",
        "MNTCTY" => "Mountain City",
        "MNTPRK" => "Mountain Park",
        "MURRAY" => "Murrayville",
        "MUSELL" => "Musella",
        "MYSTIC" => "Mystic",
        "NAHUNT" => "Nahunta",
        "NASHVI" => "Nashville",
        "NAYLOR" => "Naylor",
        "NELSON" => "Nelson",
        "NEWBOR" => "Newborn",
        "NEWHOP" => "Newhope",
        "NEWING" => "Newington",
        "NEWNAN" => "Newnan",
        "NEWTON" => "Newton",
        "NICHLL" => "Nicholls",
        "NICHOL" => "Nicholson",
        "NORCRO" => "Norcross",
        "NORMAN" => "Norman Park",
        "NORRIS" => "Norristown",
        "NRTATL" => "North Atlanta",
        "NRTDEC" => "North Decatur",
        "NRTDRU" => "North Druid Hills",
        "NHIGHS" => "North High Shoals",
        "NORTHM" => "North Metro",
        "NORWOO" => "Norwood",
        "NUNEZ" => "Nunez",
        "OAKPAR" => "Oak Park",
        "OAKFIE" => "Oakfield",
        "OAKHIL" => "Oakhill",
        "OAKMAN" => "Oakman",
        "OAKWOD" => "Oakwood",
        "OCHLOC" => "Ochlocknee",
        "OCILLA" => "Ocilla",
        "OCONEE" => "Oconee",
        "ODUM" => "Odum",
        "OFFERM" => "Offerman",
        "OGLETH" => "Oglethorpe",
        "OLIVER" => "Oliver",
        "OMAHA" => "Omaha",
        "OMEGA" => "Omega",
        "ORCHAR" => "Orchard Hill",
        "OTHREA" => "Other East",
        "OTHRNO" => "Other North",
        "OTHRSO" => "Other South",
        "OTHRST" => "Other States",
        "OTHRWE" => "Other West",
        "OTHRAL" => "Other-Alabama",
        "OTHRFL" => "Other-Florida",
        "OTHRNC" => "Other-North Carolina",
        "OTHRSC" => "Other-South Carolina",
        "OTHRTN" => "Other-Tennessee",
        "OXFORD" => "Oxford",
        "PALMET" => "Palmetto",
        "PANTHE" => "Panthersville",
        "PARROT" => "Parrott",
        "PATTER" => "Patterson",
        "PAVO" => "Pavo",
        "PAYNE" => "Payne",
        "PEACHT" => "Peachtree City",
        "PEACHC" => "Peachtree Corners",
        "PEARSO" => "Pearson",
        "PELHAM" => "Pelham",
        "PEMBRO" => "Pembroke",
        "PENDER" => "Pendergrass",
        "PERKIN" => "Perkins",
        "PERRY" => "Perry",
        "PHILLI" => "Phillipsburg",
        "PINELK" => "Pine Lake",
        "PINEMT" => "Pine Mountain",
        "PINEMV" => "Pine Mountain Valley",
        "PINEHU" => "Pinehurst",
        "PINEVI" => "Pineview",
        "PITTS" => "Pitts",
        "PLAINF" => "Plainfield",
        "PLAINS" => "Plains",
        "PLAINV" => "Plainville",
        "POOLER" => "Pooler",
        "PORTWE" => "Port Wentworth",
        "PORTAL" => "Portal",
        "PORTER" => "Porterdale",
        "POULAN" => "Poulan",
        "POWDER" => "Powder Springs",
        "PRESTO" => "Preston",
        "PULASK" => "Pulaski",
        "PUTNEY" => "Putney",
        "QUITMA" => "Quitman",
        "RABUN" => "Rabun Gap",
        "RANGER" => "Ranger",
        "RAOUL" => "Raoul",
        "RAYCIT" => "Ray City",
        "RAYLE" => "Rayle",
        "REBECC" => "Rebecca",
        "REDOAK" => "Red Oak",
        "REDAN" => "Redan",
        "REEDCR" => "Reed Creek",
        "REGIST" => "Register",
        "REIDSV" => "Reidsville",
        "REMERT" => "Remerton",
        "RENTZ" => "Rentz",
        "RESACA" => "Resaca",
        "RESTHA" => "Rest Haven",
        "REX" => "Rex",
        "REYNOL" => "Reynolds",
        "RHINE" => "Rhine",
        "RICEBO" => "Riceboro",
        "RICHLA" => "Richland",
        "RICHMO" => "Richmond Hill",
        "RIDDLE" => "Riddleville",
        "RINCON" => "Rincon",
        "RINGGO" => "Ringgold",
        "RISING" => "Rising Fawn",
        "RIVERD" => "Riverdale",
        "RIVERS" => "Riverside",
        "ROBERT" => "Roberta",
        "ROBINS" => "Robins Afb",
        "ROCHEL" => "Rochelle",
        "ROCKSP" => "Rock Spring",
        "ROCKLE" => "Rockledge",
        "ROCKMA" => "Rockmart",
        "ROCKY" => "Rocky Face",
        "ROCKYF" => "Rocky Ford",
        "ROME" => "Rome",
        "ROOPVI" => "Roopville",
        "ROSSVI" => "Rossville",
        "ROSWEL" => "Roswell",
        "ROYSTN" => "Royston",
        "RUPERT" => "Rupert",
        "RUSSEL" => "Russell",
        "RUTLED" => "Rutledge",
        "RYDAL" => "Rydal",
        "SAINTG" => "Saint George",
        "SALECI" => "Sale City",
        "SANDHI" => "Sand Hill",
        "SANDER" => "Sandersville",
        "SANDYS" => "Sandy Springs",
        "SANTAC" => "Santa Claus",
        "SAPELO" => "Sapelo Island",
        "SARDIS" => "Sardis",
        "SARGEN" => "Sargent",
        "SASSER" => "Sasser",
        "SAUTEE" => "Sautee",
        "SAVANN" => "Savannah",
        "SCOTLA" => "Scotland",
        "SCOTTS" => "Scottdale",
        "SCREVE" => "Screven",
        "SEAISL" => "Sea Island",
        "SENOA" => "Senoia",
        "SEVILL" => "Seville",
        "SHADYD" => "Shady Dale",
        "SHANNO" => "Shannon",
        "SHARON" => "Sharon",
        "SHRPS" => "Sharpsburg",
        "SHELLM" => "Shellman",
        "SHILOH" => "Shiloh",
        "SILOAM" => "Siloam",
        "SILCRK" => "Silver Creek",
        "SKIDAW" => "Skidaway Island",
        "SKYVAL" => "Sky Valley",
        "SMARR" => "Smarr",
        "SMITHV" => "Smithville",
        "SMOKER" => "Smoke Rise",
        "SMYRNA" => "Smyrna",
        "SNELLV" => "Snellville",
        "SOCIAL" => "Social Circle",
        "SOPERT" => "Soperton",
        "SOUTHA" => "South Augusta",
        "SPARKS" => "Sparks",
        "SPARTA" => "Sparta",
        "SPRING" => "Springfield",
        "ST.MAR" => "St. Marys",
        "ST.SIM" => "St. Simons",
        "STAPLE" => "Stapleton",
        "STATEN" => "Statenville",
        "STATES" => "Statesboro",
        "STATHA" => "Statham",
        "STEPHE" => "Stephens",
        "STILES" => "Stilesboro",
        "STILLM" => "Stillmore",
        "STOCKB" => "Stockbridge",
        "STOCKT" => "Stockton",
        "STONEM" => "Stone Mountain",
        "SUCHES" => "Suches",
        "SUGARH" => "Sugar Hill",
        "SUGARV" => "Sugar Valley",
        "SMRTWN" => "Summertown",
        "SMMRVL" => "Summerville",
        "SUMNER" => "Sumner",
        "SUNNYS" => "Sunnyside",
        "SURREN" => "Surrency",
        "SUWANE" => "Suwanee",
        "SWAINS" => "Swainsboro",
        "SYCAMO" => "Sycamore",
        "SYLVAN" => "Sylvania",
        "SYLVES" => "Sylvester",
        "TALBOT" => "Talbotton",
        "TALKIN" => "Talking Rock",
        "TALLAP" => "Tallapoosa",
        "TALLUL" => "Tallulah Falls",
        "TALMO" => "Talmo",
        "TARRYT" => "Tarrytown",
        "TATE" => "Tate",
        "TAYLOR" => "Taylorsville",
        "TEMPLE" => "Temple",
        "TENNGA" => "Tennga",
        "TENNIL" => "Tennille",
        "THEROC" => "The Rock",
        "THMSTN" => "Thomaston",
        "THMSVL" => "Thomasville",
        "THOMSO" => "Thomson",
        "THUNDE" => "Thunderbolt",
        "TIFTON" => "Tifton",
        "TIGER" => "Tiger",
        "TIGNAL" => "Tignall",
        "TOCCOA" => "Toccoa",
        "TOCFAL" => "Toccoa Falls",
        "TOOMSB" => "Toomsboro",
        "TOWNSE" => "Townsend",
        "TRENTO" => "Trenton",
        "TRION" => "Trion",
        "TUCKER" => "Tucker",
        "TUNNEL" => "Tunnel Hill",
        "TURIN" => "Turin",
        "TURNER" => "Turnerville",
        "TWINCI" => "Twin City",
        "TYTY" => "Ty Ty",
        "TYBEEI" => "Tybee Island",
        "TYRONE" => "Tyrone",
        "UNADIL" => "Unadilla",
        "UNION" => "Union City",
        "UNIONP" => "Union Point",
        "UNIONV" => "Unionville",
        "UPATOI" => "Upatoi",
        "UVALDA" => "Uvalda",
        "VALDOS" => "Valdosta",
        "VARNEL" => "Varnell",
        "VERNON" => "Vernonburg",
        "VIDALI" => "Vidalia",
        "VIENNA" => "Vienna",
        "VILLA" => "Villa Rica",
        "VINING" => "Vinings",
        "WACO" => "Waco",
        "WADLEY" => "Wadley",
        "WALESK" => "Waleska",
        "WALNUT" => "Walnut Grove",
        "WALTHO" => "Walthourville",
        "WARESB" => "Waresboro",
        "WARMSP" => "Warm Springs",
        "WARNER" => "Warner Robins",
        "WARREN" => "Warrenton",
        "WARTHE" => "Warthen",
        "WARWIC" => "Warwick",
        "WASHIN" => "Washington",
        "WATKIN" => "Watkinsville",
        "WAVRLY" => "Waverly",
        "WAVERL" => "Waverly Hall",
        "WAYCRO" => "Waycross",
        "WAYNES" => "Waynesboro",
        "WAYNBO" => "Waynesville",
        "WESTAU" => "West Augusta",
        "WESTGR" => "West Green",
        "WESTPO" => "West Point",
        "WESTON" => "Weston",
        "WESTSI" => "Westside",
        "WHIGHA" => "Whigham",
        "WHITE" => "White",
        "WHOAK" => "White Oak",
        "WHITEP" => "White Plains",
        "WHITEM" => "Whitemarsh Island",
        "WHITES" => "Whitesburg",
        "WILDWO" => "Wildwood",
        "WILEY" => "Wiley",
        "WILLAC" => "Willacoochee",
        "WILLIA" => "Williamson",
        "WILMIN" => "Wilmington Island",
        "WINDER" => "Winder",
        "WINSTN" => "Winston",
        "WINTER" => "Winterville",
        "WOODBI" => "Woodbine",
        "WOODBU" => "Woodbury",
        "WOODLA" => "Woodland",
        "WOODST" => "Woodstock",
        "WOODVI" => "Woodville",
        "WOOLSE" => "Woolsey",
        "WRAY" => "Wray",
        "WRENS" => "Wrens",
        "WRIGHT" => "Wrightsville",
        "YATESV" => "Yatesville",
        "YORKVI" => "Yorkville",
        "YOUNGH" => "Young Harris",
        "ZEBULO" => "Zebulon"
     },
     
     :cooling_system_type => {
        "CLFAN" => "Ceiling Fans",
        "CNELE" => "Central Electric",
        "CNGAS" => "Central Gas",
        "HSFAN" => "Fan",
        "HPUMP" => "Heat Pump",
        "HUMID" => "Humidity Control",
        "SOLAR" => "Solar A/C",
        "WNDOW" => "Window Units",
        "NONE" => "None",
        "ZONED" => "Zoned",
        "OTHER" => "Other"
     },
     :county => {
        "APPLING" => "Appling",
        "ATKINSON" => "Atkinson",
        "BACON" => "Bacon",
        "BAKER" => "Baker",
        "BALDWIN" => "Baldwin",
        "BANKS" => "Banks",
        "BARROW" => "Barrow",
        "BARTOW" => "Bartow",
        "BENHILL" => "Ben Hill",
        "BERRIEN" => "Berrien",
        "BIBB" => "Bibb",
        "BLECKLEY" => "Bleckley",
        "BRANTLEY" => "Brantley",
        "BROOKS" => "Brooks",
        "BRYAN" => "Bryan",
        "BULLOCH" => "Bulloch",
        "BURKE" => "Burke",
        "BUTTS" => "Butts",
        "CALHOUN" => "Calhoun",
        "CAMDEN" => "Camden",
        "CANDLER" => "Candler",
        "CARROLL" => "Carroll",
        "CATOOSA" => "Catoosa",
        "CHARLTON" => "Charlton",
        "CHATHAM" => "Chatham",
        "CHATTAHOOCH" => "Chattahoochee",
        "CHATTOOGA" => "Chattooga",
        "CHEROKEE" => "Cherokee",
        "CLARKE" => "Clarke",
        "CLAY" => "Clay",
        "CLAYTON" => "Clayton",
        "CLINCH" => "Clinch",
        "COBB" => "Cobb",
        "COFFEE" => "Coffee",
        "COLQUITT" => "Colquitt",
        "COLUMBIA" => "Columbia",
        "COOK" => "Cook",
        "COWETA" => "Coweta",
        "CRAWFORD" => "Crawford",
        "CRISP" => "Crisp",
        "DADE" => "Dade",
        "DAWSON" => "Dawson",
        "DECATUR" => "Decatur",
        "DEKALB" => "Dekalb",
        "DODGE" => "Dodge",
        "DOOLY" => "Dooly",
        "DOUGHERTY" => "Dougherty",
        "DOUGLAS" => "Douglas",
        "EARLY" => "Early",
        "ECHOLS" => "Echols",
        "EFFINGHAM" => "Effingham",
        "ELBERT" => "Elbert",
        "EMANUEL" => "Emanuel",
        "EVANS" => "Evans",
        "FANNIN" => "Fannin",
        "FAYETTE" => "Fayette",
        "FLOYD" => "Floyd",
        "FORSYTH" => "Forsyth",
        "FRANKLIN" => "Franklin",
        "FULTON" => "Fulton",
        "GILMER" => "Gilmer",
        "GLASCOCK" => "Glascock",
        "GLYNN" => "Glynn",
        "GORDON" => "Gordon",
        "GRADY" => "Grady",
        "GREENE" => "Greene",
        "GWINNETT" => "Gwinnett",
        "HABERSHA" => "Habersham",
        "HALL" => "Hall",
        "HANCOCK" => "Hancock",
        "HARALSON" => "Haralson",
        "HARRIS" => "Harris",
        "HART" => "Hart",
        "HEARD" => "Heard",
        "HENRY" => "Henry",
        "HOUSTON" => "Houston",
        "IRWIN" => "Irwin",
        "JACKSON" => "Jackson",
        "JASPER" => "Jasper",
        "JEFFDAVIS" => "Jeff Davis",
        "JEFFERSON" => "Jefferson",
        "JENKINS" => "Jenkins",
        "JOHNSON" => "Johnson",
        "JONES" => "Jones",
        "LAMAR" => "Lamar",
        "LANIER" => "Lanier",
        "LAURENS" => "Laurens",
        "LEE" => "Lee",
        "LIBERTY" => "Liberty",
        "LINCOLN" => "Lincoln",
        "LONG" => "Long",
        "LOWNDES" => "Lowndes",
        "LUMPKIN" => "Lumpkin",
        "MACON" => "Macon",
        "MADISON" => "Madison",
        "MARION" => "Marion",
        "MCDUFFIE" => "Mcduffie",
        "MCINTOSH" => "Mcintosh",
        "MERIWETHER" => "Meriwether",
        "MILLER" => "Miller",
        "MITCHELL" => "Mitchell",
        "MONROE" => "Monroe",
        "MONTGOMERY" => "Montgomery",
        "MORGAN" => "Morgan",
        "MURRAY" => "Murray",
        "MUSCOGEE" => "Muscogee",
        "NEWTON" => "Newton",
        "OCONEE" => "Oconee",
        "OGLETHORPE" => "Oglethorpe",
        "ALABAMAOTH" => "Other-Alabama",
        "AKOTH" => "Other-Alaska",
        "AZOTH" => "Other-Arizona",
        "AROTH" => "Other-Arkansas",
        "CAOTH" => "Other-California",
        "COOTH" => "Other-Colorado",
        "CTOTH" => "Other-Connecticut",
        "DEOTH" => "Other-Delaware",
        "DCOTH" => "Other-District Of Columbia",
        "FLORIDAOTH" => "Other-Florida",
        "HIOTH" => "Other-Hawaii",
        "IDOTH" => "Other-Idaho",
        "ILOTH" => "Other-Illinois",
        "INOTH" => "Other-Indiana",
        "IAOTH" => "Other-Iowa",
        "KSOTH" => "Other-Kansas",
        "KYOTH" => "Other-Kentucky",
        "LAOTH" => "Other-Louisiana",
        "MEOTH" => "Other-Maine",
        "MDOTH" => "Other-Maryland",
        "MAOTH" => "Other-Massachusetts",
        "MIOTH" => "Other-Michigan",
        "MNOTH" => "Other-Minnesota",
        "MSOTH" => "Other-Mississippi",
        "MOOTH" => "Other-Missouri",
        "MTOTH" => "Other-Montana",
        "NEOTH" => "Other-Nebraska",
        "NVOTH" => "Other-Nevada",
        "NHOTH" => "Other-New Hampshire",
        "NJOTH" => "Other-New Jersey",
        "NMOTH" => "Other-New Mexico",
        "NYOTH" => "Other-New York",
        "NCAROLINOTH" => "Other-North Carolina",
        "NDOTH" => "Other-North Dakota",
        "OHOTH" => "Other-Ohio",
        "OKOTH" => "Other-Oklahoma",
        "OROTH" => "Other-Oregon",
        "PAOTH" => "Other-Pennsylvania",
        "RIOTH" => "Other-Rhode Island",
        "SCAROLINOTH" => "Other-South Carolina",
        "SDOTH" => "Other-South Dakota",
        "TENNESSEOTH" => "Other-Tennessee",
        "TXOTH" => "Other-Texas",
        "UTOTH" => "Other-Utah",
        "VTOTH" => "Other-Vermont",
        "VAOTH" => "Other-Virginia",
        "WAOTH" => "Other-Washington",
        "WVOTH" => "Other-West Virginia",
        "WIOTH" => "Other-Wisconsin",
        "WYOTH" => "Other-Wyoming",
        "PAULDING" => "Paulding",
        "PEACH" => "Peach",
        "PICKENS" => "Pickens",
        "PIERCE" => "Pierce",
        "PIKE" => "Pike",
        "POLK" => "Polk",
        "PULASKI" => "Pulaski",
        "PUTNAM" => "Putnam",
        "QUITMAN" => "Quitman",
        "RABUN" => "Rabun",
        "RANDOLPH" => "Randolph",
        "RICHMOND" => "Richmond",
        "ROCKDALE" => "Rockdale",
        "SCHLEY" => "Schley",
        "SCREVEN" => "Screven",
        "SEMINOLE" => "Seminole",
        "SPALDING" => "Spalding",
        "STEPHENS" => "Stephens",
        "STEWART" => "Stewart",
        "SUMTER" => "Sumter",
        "TALBOT" => "Talbot",
        "TALIAFERRO" => "Taliaferro",
        "TATTNALL" => "Tattnall",
        "TAYLOR" => "Taylor",
        "TELFAIR" => "Telfair",
        "TERRELL" => "Terrell",
        "THOMAS" => "Thomas",
        "TIFT" => "Tift",
        "TOOMBS" => "Toombs",
        "TOWNS" => "Towns",
        "TREUTLEN" => "Treutlen",
        "TROUP" => "Troup",
        "TURNER" => "Turner",
        "TWIGGS" => "Twiggs",
        "UNION" => "Union",
        "UPSON" => "Upson",
        "WALKER" => "Walker",
        "WALTON" => "Walton",
        "WARE" => "Ware",
        "WARREN" => "Warren",
        "WASHINGTON" => "Washington",
        "WAYNE" => "Wayne",
        "WEBSTER" => "Webster",
        "WHEELER" => "Wheeler",
        "WHITE" => "White",
        "WHITFIELD" => "Whitfield",
        "WILCOX" => "Wilcox",
        "WILKES" => "Wilkes",
        "WILKINSON" => "Wilkinson",
        "WORTH" => "Worth"
     },
     :disclosures => {
        "RTCOM" => "Active Adult Community",
        "RLATE" => "Agent Related to Seller",
        "CERTP" => "Cert. Prof. Home Bldr",
        "CRPTE" => "Corporate Owner",
        "ESTAT" => "Estate Owned",
        "GOVMT" => "Government Owned",
        "HISTR" => "Historical",
        "FIXUP" => "Fixer Upper",
        "FRECL" => "Foreclosure",
        "INVST" => "Investor Owned",
        "LSPUR" => "Lease Purchase",
        "LNDROWN" => "Lender Owned",
        "HUDLS" => "HUD Listing (see remarks)",
        "RENOV" => "Recently Renovated",
        "NDFS" => "No disclosures from Seller",
        "TRNSF" => "Owner Transferred",
        "EXCHN" => "Owner Will Consider Exchange",
        "OWNRAGT" => "Owner/Agent",
        "POTSS" => "Potential Short Sale",
        "NONE" => "None",
        "SSPAP" => "Short Sale Pre-Approved",
        "SLDAIS" => "Sold As/Is",
        "OTHER" => "Other"
     },
     :equipment => {
        "AIRFL" => "Elec Air Filter",
        "CMPCT" => "Trash Compactor",
        "CNTVC" => "Central Vacuum",
        "DBLOV" => "Double Ovens",
        "DISHW" => "Dishwasher",
        "DRYER" => "Dryer",
        "DSPSL" => "Garbage Disposal",
        "ELERN" => "Elec Ovn/Rng/Ctop",
        "ENRGSTAPP" => "Energy Star Appliances",
        "FRIDGE" => "Refrigerator",
        "GASRN" => "Gas Ovn/Rng/Ctop",
        "GASWH" => "Gas Water Heater",
        "INGRL" => "Indoor Grill",
        "INTCM" => "Intercom/Radio",
        "MICRO" => "Microwave",
        "OTHER" => "Other",
        "REFIN" => "Refrig Included",
        "SLFCL" => "Self-Clean Oven",
        "SMOKE" => "Smoke/Fire Alarm",
        "SSLSE" => "Sec System Leased",
        "SSOWN" => "Sec System Owned",
        "TNKLSWTRHTR" => "Tankless Water Heater",
        "WASHER" => "Washer"
     },
     :exterior_description => {
        "12STEP" => "1-2 Step Entry",
        "BARNS" => "Barn(s)",
        "CARIGE" => "Carriage House",
        "DECK" => "Deck",
        "FENCE" => "Fenced Yard",
        "FRPOR" => "Front Porch",
        "GRDEN" => "Garden Area",
        "GASGR" => "Gas Grill",
        "GUEST" => "Guest House",
        "GAZBO" => "Gazebo",
        "HOTUB" => "Hot Tub/Spa",
        "INVFN" => "Invisible Fence",
        "IRRGT" => "Irrigation System",
        "OUTBL" => "Out-Buildings",
        "OUTFRE" => "Outdoor Fireplace",
        "PATIO" => "Patio",
        "POOLH" => "Pool House",
        "LNDSC" => "Prof Landscaping",
        "PLSCR" => "Pool Screened/Enclosed",
        "SATLT" => "Satellite Dish",
        "STBLE" => "Stables",
        "WRPOR" => "Wrap-Around Porch",
        "OTHER" => "Other"
      },
      :heating_system => {
        "BASBD" => "Baseboard",
        "BOILE" => "Boiler",
        "ELECT" => "Electric",
        "FORCE" => "Forced Air",
        "GAS" => "Gas",
        "GRVTY" => "Gravity/Floor Htg",
        "HPUMP" => "Heat Pump",
        "OIL" => "Oil",
        "OTHER" => "Other",
        "PROPN" => "Propane",
        "SOLAR" => "Solar",
        "STEAM" => "Steam/Hot Water",
        "ZONED" => "Zoned"
     },
     :interior_description => {
        "9CEIL" => "9 ft + Ceil Main",
        "9CLUP" => "9 ft+ Ceil Upper",
        "9CLLW" => "9 ft + Ceil Lower",
        "10CLM" => "10 ft+ Ceil Main",
        "10CUP" => "10 ft + Ceil Upper",
        "10CLW" => "10 ft + Ceil Lower",
        "2STFY" => "2-Story Foyer",
        "BOOKS" => "Bookcases",
        "CATHD" => "Cathedral Ceiling",
        "DISAT" => "Disp Attic Stairs",
        "DBLVN" => "Double Vnty Other",
        "DSLCN" => "High Speed Internet Available",
        "ELEVT" => "Elevator",
        "ENTFY" => "Entrance Foyer",
        "EXBMS" => "Exposed Beams",
        "SPRNK" => "Fire Sprinklers",
        "HRDWD" => "Hardwood Floors",
        "HHCLS" => "His & Her Closets",
        "PRMAT" => "Perm Attic Stairs",
        "PINFL" => "Pine Floor",
        "REARS" => "Rear Stairs",
        "SAUNA" => "Sauna",
        "SKYLT" => "Skylights",
        "TREYC" => "Trey Ceilings",
        "WICLO" => "Walk-In Closet(s)",
        "WWCAR" => "Wall/Wall Carpet",
        "WETBR" => "Wet Bar",
        "OTHER" => "Other"
     },
     :hoa_membership => {
        "OPTNL" => "Optional",
        "REQRD" => "Mandatory",
        "SWMTN" => "Optional",
        "NONE" => "None"
     },
     :lot_description => {
        "BORDR" => "Borders US/State Park",
        "CTYVW" => "City View",
        "CORNR" => "Corner",
        "CREEK" => "Creek",
        "CULDE" => "Cul-De-Sac",
        "DPWTRAC" => "Deepwater Access",
        "DPWTDOC" => "Deepwater at Dock",
        "FLDPL" => "Flood Plain",
        "GOLFC" => "Golf Course",
        "GLFVW" => "Golf View",
        "LAGOON" => "Lagoon",
        "LKEFT" => "Lake Frontage",
        "LKEVW" => "Lake View",
        "LEVEL" => "Level",
        "LVLDR" => "Level Driveway",
        "MARSH" => "Marsh",
        "MTNFT" => "Mountain Frontage",
        "MTNVW" => "Mountain View",
        "OCNFT" => "Ocean Frontage",
        "OCNVW" => "Ocean View",
        "PASTR" => "Pasture",
        "PRVBK" => "Private Backyard",
        "POND" => "Pond",
        "RVRFT" => "River Frontage",
        "RVRVW" => "River View",
        "PLTEN" => "Rm-Pool/Tennis",
        "SLOPE" => "Sloped",
        "STEEP" => "Steep",
        "TIDAL" => "Tidal",
        "WOODD" => "Wooded"
      },
     :mls_area_name => {
        "13" => "Fulton North",
        "14" => "Fulton North",
        "21" => "Atlanta North",
        "22" => "Atlanta North",
        "23" => "Atlanta North",
        "24" => "Atlanta North",
        "31" => "Fulton South",
        "32" => "Fulton South",
        "33" => "Fulton South",
        "41" => "Dekalb-East",
        "42" => "Dekalb-East",
        "43" => "Dekalb-East",
        "51" => "Dekalb-West",
        "52" => "Dekalb-West",
        "53" => "Dekalb-West",
        "61" => "Gwinnett County",
        "62" => "Gwinnett County",
        "63" => "Gwinnett County",
        "64" => "Gwinnett County",
        "65" => "Gwinnett County",
        "66" => "Gwinnett County",
        "71" => "Cobb-West",
        "72" => "Cobb-West",
        "73" => "Cobb-West",
        "74" => "Cobb-West",
        "75" => "Cobb-West",
        "81" => "Cobb-East",
        "82" => "Cobb-East",
        "83" => "Cobb-East",
        "91" => "Douglas County",
        "101" => "Rockdale County",
        "102" => "Rockdale County",
        "111" => "Cherokee County",
        "112" => "Cherokee County",
        "113" => "Cherokee County",
        "114" => "Cherokee County",
        "121" => "Dunwoody",
        "131" => "Sandy Springs",
        "132" => "Sandy Springs",
        "141" => "Walton County",
        "142" => "Walton County",
        "151" => "Newton County",
        "161" => "Clayton County",
        "171" => "Fayette County",
        "181" => "Coweta County",
        "191" => "Paulding County",
        "192" => "Paulding County",
        "201" => "Bartow County",
        "202" => "Bartow County",
        "203" => "Bartow County",
        "204" => "Bartow County",
        "211" => "Henry County",
        "221" => "Forsyth County",
        "222" => "Forsyth County",
        "223" => "Forsyth County",
        "224" => "Forsyth County",
        "231" => "Carroll County",
        "232" => "Heard",
        "241" => "Butts County",
        "242" => "Jasper",
        "243" => "Putnam",
        "251" => "Spalding County",
        "261" => "Hall County",
        "262" => "Hall County",
        "263" => "Hall County",
        "264" => "Hall County",
        "265" => "Hall County",
        "271" => "Dawson County",
        "272" => "Dawson County",
        "273" => "Dawson County",
        "275" => "White County",
        "278" => "Lumpkin County",
        "281" => "Banks County",
        "285" => "Habersham County",
        "286" => "Stephens County",
        "287" => "Rabun County",
        "291" => "Jackson County",
        "292" => "Jackson County",
        "293" => "Jackson County",
        "294" => "Madison",
        "295" => "Franklin",
        "301" => "Barrow County",
        "302" => "Barrow County",
        "311" => "Oconee County",
        "321" => "Morgan County",
        "322" => "Greene",
        "323" => "Oglethorpe",
        "331" => "Pickens County",
        "332" => "Pickens County",
        "336" => "Gilmer County",
        "341" => "Gordon County",
        "342" => "Gordon County",
        "343" => "Murray",
        "345" => "Catoosa",
        "350" => "Whitfield",
        "351" => "Floyd-West Rome",
        "352" => "Floyd-Armuchee",
        "353" => "Floyd-Northeast",
        "354" => "Floyd-East Rome",
        "355" => "Floyd-South Rome",
        "356" => "Floyd-Downtown",
        "357" => "Chattooga",
        "358" => "Walker",
        "359" => "Dade",
        "361" => "Polk County",
        "371" => "Clarke County",
        "381" => "Haralson County",
        "400" => "Fannin",
        "404" => "Union",
        "408" => "Towns",
        "412" => "Hart",
        "416" => "Elbert",
        "420" => "Wilkes",
        "424" => "Lincoln",
        "428" => "Taliaferro",
        "432" => "Warren",
        "436" => "Mcduffie",
        "440" => "Columbia",
        "444" => "Hancock",
        "448" => "Glascock",
        "452" => "Richmond",
        "460" => "Troup",
        "464" => "Meriwether",
        "468" => "Pike",
        "472" => "Lamar",
        "476" => "Monroe",
        "480" => "Jones",
        "484" => "Baldwin",
        "488" => "Washington",
        "492" => "Jefferson",
        "496" => "Burke",
        "500" => "Harris",
        "504" => "Talbot",
        "508" => "Upson",
        "512" => "Crawford",
        "516" => "Bibb",
        "524" => "Twiggs",
        "528" => "Wilkinson",
        "532" => "Johnson",
        "536" => "Emanuel",
        "540" => "Jenkins",
        "544" => "Screven",
        "548" => "Muscogee",
        "556" => "Chattahoochee",
        "560" => "Marion",
        "564" => "Taylor",
        "568" => "Macon",
        "572" => "Peach",
        "576" => "Houston",
        "580" => "Bleckley",
        "584" => "Laurens",
        "588" => "Treutlen",
        "592" => "Candler",
        "596" => "Bulloch",
        "600" => "Effingham",
        "604" => "Stewart",
        "608" => "Webster",
        "612" => "Schley",
        "616" => "Sumter",
        "620" => "Dooly",
        "624" => "Pulaski",
        "628" => "Dodge",
        "632" => "Wheeler",
        "636" => "Montgomery",
        "640" => "Toombs",
        "644" => "Tattnall",
        "648" => "Evans",
        "652" => "Bryan",
        "656" => "Chatham",
        "664" => "Quitman",
        "668" => "Randolph",
        "672" => "Terrell",
        "676" => "Lee",
        "680" => "Crisp",
        "684" => "Wilcox",
        "688" => "Telfair",
        "692" => "Jeff Davis",
        "696" => "Appling County",
        "700" => "Wayne",
        "704" => "Long",
        "708" => "Liberty",
        "712" => "Mcintosh",
        "716" => "Clay",
        "720" => "Calhoun",
        "724" => "Dougherty",
        "732" => "Worth",
        "736" => "Turner",
        "740" => "Ben Hill",
        "744" => "Irwin",
        "748" => "Coffee",
        "752" => "Bacon",
        "756" => "Pierce",
        "760" => "Early",
        "764" => "Baker",
        "768" => "Miller",
        "772" => "Mitchell",
        "776" => "Colquitt",
        "780" => "Tift",
        "784" => "Cook",
        "788" => "Berrien",
        "792" => "Atkinson",
        "796" => "Ware",
        "800" => "Brantley",
        "804" => "Glynn",
        "812" => "Seminole",
        "816" => "Decatur",
        "820" => "Grady",
        "824" => "Thomas",
        "828" => "Brooks",
        "832" => "Lowndes",
        "836" => "Lanier",
        "840" => "Clinch",
        "844" => "Echols",
        "848" => "Charlton",
        "852" => "Camden",
        "900" => "Alaska",
        "901" => "Arizona",
        "902" => "Arkansas",
        "903" => "California",
        "904" => "Colorado",
        "905" => "Connecticut",
        "906" => "Delaware",
        "907" => "District Of Columbia",
        "908" => "Hawaii",
        "909" => "Idaho",
        "910" => "Illinois",
        "911" => "Indiana",
        "912" => "Iowa",
        "913" => "Kansas",
        "914" => "Kentucky",
        "915" => "Louisiana",
        "916" => "Maine",
        "917" => "Maryland",
        "918" => "Massachusetts",
        "919" => "Michigan",
        "920" => "Minnesota",
        "921" => "Mississippi",
        "922" => "Missouri",
        "923" => "Montana",
        "924" => "Nebraska",
        "925" => "Nevada",
        "926" => "New Hampshire",
        "927" => "New Jersey",
        "928" => "New Mexico",
        "929" => "New York",
        "931" => "North Dakota",
        "932" => "Ohio",
        "933" => "Oklahoma",
        "934" => "Oregon",
        "935" => "Pennsylvania",
        "936" => "Rhode Island",
        "937" => "Texas",
        "938" => "Utah",
        "939" => "Vermont",
        "940" => "Virginia",
        "941" => "Washington",
        "942" => "West Virginia",
        "943" => "Wisconsin",
        "944" => "Wyoming",
        "945" => "South Dakota",
        "950" => "Tennessee",
        "960" => "North Carolina",
        "970" => "South Carolina",
        "980" => "Florida",
        "990" => "Alabama"
     },
     :stories => {
        "ONEHL" => "1 & 1/2 Stories",
        "ONEST" => "1 Story",
        "MORE2" => "2 Or + Stories",
        "TWOST" => "2 Stories",
        "SPLFY" => "Split Foyer",
        "SPLVL" => "Split Level"
      },
     :style_of_building => {
        "BUNGL" => "Bungalow",
        "CABIN" => "Cabin",
        "CAPEC" => "Cape Cod",
        "CHALT" => "Chalet",
        "CLSTR" => "Cluster Home",
        "COLON" => "Colonial",
        "CNTMP" => "Contemporary",
        "COTTG" => "Cottage",
        "CNTRY" => "Country",
        "CRFMN" => "Craftsman",
        "EUROP" => "European",
        "FRNCH" => "French Provncial",
        "MEDIT" => "Mediterranean",
        "MOBIL" => "Mobile",
        "MODLR" => "Modular",
        "OTHER" => "Other",
        "RANCH" => "Ranch",
        "RUSTC" => "Rustic",
        "TRADT" => "Traditional",
        "TUDOR" => "Tudor",
        "VICTR" => "Victorian"
     },
     :mls_elementary_school => {
        "ASCLARK" => "A. S. Clark",
        "ABBOTTS" => "Abbotts Hill",
        "ACWORTH" => "Acworth Intermediate",
        "ADAIRSVIL" => "Adairsville",
        "ADAMSVLLE" => "Adamsville",
        "ADDISON" => "Addison",
        "ADRIANOF" => "Adrian of Performing Arts (PK-08)",
        "ALCOVA" => "Alcova",
        "ALEXANDE" => "Alexander Ii",
        "ALFORD" => "Alford",
        "ALICECO" => "Alice Coachman",
        "ALLATOONA" => "Allatoona",
        "ALLENEL" => "Allen",
        "ALLGOOD" => "Allgood - Dekalb",
        "ALLGOODP" => "Allgood - Paulding",
        "ALPHARETTA" => "Alpharetta",
        "ALPSROA" => "Alps Road",
        "ALTAMA" => "Altama",
        "ALTAMAHA" => "Altamaha",
        "ALTOPARK" => "Alto Park",
        "AMANA" => "Amana Academy",
        "AMBROSE" => "Ambrose",
        "ANDERSON" => "Anderson",
        "ANDERSONL" => "Anderson-Livsey",
        "ANNAKDAVIE" => "Anna K. Davie",
        "ANNEST" => "Anne Street",
        "ANNETTEWINN" => "Annette Winn",
        "ANNIEBE" => "Annie Belle Clark Primary",
        "ANNISTOWN" => "Annistown",
        "ANTIOCH" => "Antioch",
        "APPLING" => "Appling County",
        "ARBORSPGS" => "Arbor Springs",
        "ARBORSTAT" => "Arbor Station",
        "ARCADO" => "Arcado",
        "ARGYLE" => "Argyle",
        "ARMUCHEE" => "Armuchee",
        "ARNCOSARGARNCO" => "Arnco-Sargent",
        "ARNOLD" => "Arnold",
        "ARNOLDMILL" => "Arnold Mill",
        "ASHFRDPR" => "Ashford Park",
        "ATHARD" => "Atha Road",
        "ATKINSONB" => "Atkinson - Brantley",
        "ATKINSONC" => "Atkinson - Coweta",
        "ATKINSONOTH" => "Atkinson - Other",
        "ATKINSONS" => "Atkinson - Spalding",
        "AUBURN" => "Auburn",
        "AUSTELL" => "Austell",
        "AUSTELLPRIMARY" => "Austell Primary",
        "AUSTIN" => "Austin",
        "AUSTINRD" => "Austin Road",
        "AVERY" => "Avery",
        "AVONDALEE" => "Avondale",
        "BACONEL" => "Bacon",
        "BACONCO" => "Bacon County",
        "BACONTON" => "Baconton Community Charter(PK - 09)",
        "BAKER" => "Baker",
        "BAKERCO" => "Baker County",
        "BAKERPL" => "Baker Place",
        "BALDWIN" => "Baldwin",
        "BALLGROND" => "Ball Ground",
        "BANKSCO" => "Banks County",
        "BANKSCOUP" => "Banks County Upper",
        "BARDEN" => "Barden",
        "BARKSDALE" => "Barksdale",
        "BARNETTSH" => "Barnett Shoals",
        "BARNWELL" => "Barnwell",
        "BARROW" => "Barrow",
        "BARTONC" => "Barton Chapel",
        "BARTOW" => "Bartow",
        "BASCOMB" => "Bascomb",
        "BATTLEFI" => "Battlefield",
        "BAYCREEK" => "Bay Creek",
        "BAYVALE" => "Bayvale",
        "USHER" => "Bazoline E. Usher",
        "BEAVERRDG" => "Beaver Ridge",
        "BEAVERBRK" => "Beaverbrook",
        "BEAVERDALE" => "Beaverdale",
        "BEAVERDA" => "Beaverdam",
        "BECHRHLLS" => "Beecher Hills",
        "BELAIR" => "Bel Air",
        "BELLSFRR" => "Bells Ferry",
        "BELMTHLLS" => "Belmont Hills",
        "BELWOOD" => "Belwood",
        "BENHILL" => "Ben Hill",
        "BENEFIELD" => "Benefield",
        "BENNING" => "Benning Hills",
        "BENTEEN" => "Benteen",
        "BENTON" => "Benton",
        "BERKELYL" => "Berkeley Lake",
        "BERND" => "Bernd",
        "BERRIEN" => "Berrien",
        "BERTAWE" => "Berta Weathersbee",
        "BESSIELBAGGETT" => "Bessie L. Baggett",
        "BETHESDA" => "Bethesda",
        "BETHLEHEM" => "Bethlehem - Barrow",
        "BETHLEHEMH" => "Bethlehem - Henry",
        "BETHUNE" => "Bethune - Charlton",
        "BETHUNECP" => "Bethune - College Park",
        "BETHUNEF" => "Bethune - Fulton",
        "BEULAH" => "Beulah",
        "BIGA" => "Big A",
        "BIGCREEK" => "Big Creek",
        "BIGSHANTY" => "Big Shanty",
        "BILLARP" => "Bill Arp",
        "BIRMINGHAM" => "Birmingham Falls",
        "BIRNEY" => "Birney",
        "BLACKBURN" => "Blackburn",
        "BLACKSMILL" => "Blacks Mill",
        "BLACKSHE" => "Blackshear",
        "BLKSHRTRL" => "Blackshear Trail",
        "BLACKWELL" => "Blackwell - Cobb",
        "BLKWELLELB" => "Blackwell - Elbert",
        "BLAKENEY" => "Blakeney",
        "BLALCOCK" => "Blalock",
        "BLANCHAR" => "Blanchard",
        "BLANFORD" => "Blandford",
        "BLANDYH" => "Blandy Hills",
        "BLECKLEY" => "Bleckley County",
        "BLMINGDLE" => "Bloomingdale",
        "BLUERDGC" => "Blue Ridge - Columbia",
        "BLUERDGF" => "Blue Ridge - Fannin",
        "BLUERIDGE" => "Blue Ridge - Whitfield",
        "BLYTHEE" => "Blythe",
        "BMATHIS" => "Bob Mathis",
        "BOLTONA" => "Bolton Academy",
        "BONAIRE" => "Bonaire",
        "BOSTON" => "Boston",
        "BOWDON" => "Bowdon",
        "BOWMAN" => "Bowman",
        "BOYD" => "Boyd",
        "BOYNTON" => "Boynton",
        "BRAELINN" => "Braelinn",
        "BRAMLETT" => "Bramlett",
        "BRANDON" => "Brandon",
        "BREWERE" => "Brewer",
        "BRIARVSTA" => "Briar Vista",
        "BRIARLAKE" => "Briarlake",
        "BRIGHTSTAR" => "Bright Star",
        "WBRITT" => "Britt",
        "BRITTDA" => "Britt David  Computer",
        "BROCKETT" => "Brockett",
        "BROOKDAL" => "Brookdale",
        "BROOKLET" => "Brooklet",
        "BROOKSC" => "Brooks - Coweta",
        "BROOKS" => "Brooks - Fayette",
        "BROOKVIEW" => "Brookview",
        "BROOKWDC" => "Brookwood - Columbia",
        "BROOKWOODF" => "Brookwood - Forsyth",
        "BROOKWOO" => "Brookwood - Gwinnett",
        "BROOKWOOD" => "Brookwood - Whitfield",
        "CROWNCL" => "Brown - Clayton",
        "BROWNCB" => "Brown - Cobb",
        "BROWNSMIL" => "Browns Mill",
        "BROXTONMAR" => "Broxton-Mary Hayes",
        "BRUCE" => "Bruce",
        "BRUMBY" => "Brumby",
        "BRYANCO" => "Bryan County",
        "BRYANTBUL" => "Bryant - Bulloch",
        "BRYANT" => "Bryant - Cobb",
        "BUCHANAN" => "Buchanan",
        "BUFORD" => "Buford",
        "BUFORDA" => "Buford Academy",
        "BULLARD" => "Bullard",
        "BURGESSPE" => "Burgess-Peterson",
        "BURGHARD" => "Burghard",
        "BURKE" => "Burke",
        "BURNETT" => "Burnett",
        "BURNETTE" => "Burnette",
        "BURNTHICKORY" => "Burnt Hickory",
        "BURROUGH" => "Burroughs-Molette",
        "BURRUSS" => "Burruss",
        "BURWELL" => "Burwell",
        "BUTLER" => "Butler",
        "BUTTONG" => "Button Gwinnett",
        "BYRONEL" => "Byron",
        "CTWALKER" => "C T Walker",
        "HILL" => "C. W. Hill",
        "CAROBERTS" => "C.A. Roberts",
        "CALHOUNG" => "Calhoun",
        "CALHOUNC" => "Calhoun County",
        "CALLAWAY" => "Callaway - Clayton",
        "CALLAWAYT" => "Callaway - Troup",
        "CAMPCREEK" => "Camp Creek",
        "CAMPBELL" => "Campbell",
        "CANBYLANE" => "Canby Lane",
        "CANNONS" => "Cannon Street",
        "CANONGATE" => "Canongate",
        "CANTON" => "Canton",
        "CAPVIEW" => "Capitol View",
        "CARMEL" => "Carmel",
        "CARNESVI" => "Carnesville",
        "CARROLLT" => "Carrollton",
        "CARTER" => "Carter",
        "CARTERSE" => "Cartersville",
        "CARTERSP" => "Cartersville Primary",
        "CARVERJ" => "Carver - Jefferson",
        "CARVERT" => "Carver - Terrell",
        "CARYREYN" => "Cary Reynolds",
        "CASCADE" => "Cascade",
        "CAVESPRING" => "Cave Spring",
        "CEDRGROVE" => "Cedar Grove",
        "CEDARHILL" => "Cedar Hill",
        "CEDARRIDGE" => "Cedar Ridge - Columbia",
        "CEDARRDG" => "Cedar Ridge - Whitfield",
        "CENTENNIAL" => "Centennial Arts Academy",
        "CENTENPL" => "Centennial Place",
        "CENTERE" => "Center",
        "CENTERVILL" => "Centerville - Gwinnett",
        "CENTERVI" => "Centerville - Houston",
        "CENTRALC" => "Central - Carroll",
        "CENTRALFRANKLIN" => "Central Franklin",
        "CENTRALH" => "Centralhatchee",
        "CHALKER" => "Chalker",
        "CHAPELHLDE" => "Chapel Hill - Dekalb",
        "CHAPELHLDO" => "Chapel Hill - Douglas",
        "CHAPMAN" => "Chapman Intermediate",
        "CHARLESBRA" => "Charles Brant Chesney",
        "CHARLES" => "Charles Spencer",
        "CHASEST" => "Chase Street",
        "CHATSWOR" => "Chatsworth",
        "CHATTAHOOC" => "Chattahoochee - Forsyth",
        "CHATTAHOO" => "Chattahoochee - Gwinnett",
        "CHATTAHO" => "Chattahoochee County Education Ctr. (PK-5)",
        "CHATTANOOG" => "Chattanooga Valley",
        "CHEATHAM" => "Cheatham Hill",
        "CHEROKEEP" => "Cherokee - Polk",
        "CHEROKEES" => "Cherokee - Sumter",
        "CHEROKEERI" => "Cherokee Ridge",
        "CHESTNUT" => "Chesnut",
        "CHESTATEE" => "Chestatee",
        "CHESTNUTMT" => "Chestnut Mountain",
        "WESTSTUBBS" => "Cliftondale",
        "CHICKAMAUG" => "Chickamauga",
        "CHICOPEEWOODS" => "Chicopee",
        "CHURCHST" => "Church Street",
        "CITYPARK" => "City Park",
        "CLAIREMONT" => "Clairemont",
        "CLARKCREEK" => "Clark Creek",
        "CLARKDALE" => "Clarkdale",
        "CLARKESV" => "Clarkesville",
        "CLAXTON" => "Claxton",
        "CLAY" => "Clay",
        "CLAYCOU" => "Clay County",
        "CLAYTON" => "Clayton",
        "CLYOT" => "Clayton - Other",
        "CLEARCREEK" => "Clear Creek",
        "CLEVELAND" => "Cleveland - Fulton",
        "CLVCLK" => "Cleveland Road",
        "CLEVELANDF" => "Cleveland - Fayette",
        "COLHAMFERR" => "Colham Ferry",
        "COGBURNWOO" => "Cogburn Woods",
        "COHUTTA" => "Cohutta",
        "COKER" => "Coker",
        "COLBERT" => "Colbert",
        "COLLEGEPA" => "College Park",
        "COLLINSR" => "Collins - Richmond",
        "CLIFTON" => "Clifton",
        "CLINCHC" => "Clinch County",
        "CLOUDSP" => "Cloud Springs",
        "CLOVERLEAF" => "Cloverleaf",
        "CLUBVIEW" => "Clubview",
        "CLYATTVI" => "Clyattville",
        "COALMT" => "Coal Mountain",
        "COLGEHGTS" => "College Heights",
        "COOKCOOK" => "Cook - Cook",
        "COMER" => "Comer",
        "COMMERCE" => "Commerce",
        "COMPTON" => "Compton",
        "CONLEYHLS" => "Conley Hills",
        "CONNALLY" => "Connally",
        "COLLINST" => "Collins - Tattnall",
        "COLUMBIAE" => "Columbia",
        "CONNIEDUGAN" => "Connie Dugan",
        "CONTLCLN" => "Continental Colony",
        "COPELAND" => "Copeland",
        "CORLEY" => "Corley",
        "CORNELIA" => "Cornelia",
        "COTTONIND" => "Cotton Indian",
        "COUNTYLIN" => "County Line",
        "COWANRD" => "Cowan Road",
        "COX" => "Cox",
        "CRABAPPLE" => "Crabapple Crossing",
        "CRABAPPLELANE" => "Crabapple Lane",
        "CRAIG" => "Craig",
        "CRAIGHO" => "Craig-Houghton",
        "COOK" => "Cook - Fulton",
        "CRAWFORD" => "Crawford County",
        "CREEKVIEW" => "Creek View",
        "CREEKSID" => "Creekside",
        "CRESCENT" => "Crescent Road",
        "CROOKED" => "Crooked River",
        "CROSSCR" => "Cross Creek",
        "CROSSROADSC" => "Crossroads",
        "CROSSROADS" => "Crossroads Academy",
        "CUMMING" => "Cumming",
        "CUSSETA" => "Cusseta Road",
        "DSTANTON" => "D. H. Stanton",
        "DACULA" => "Dacula",
        "DADE" => "Dade",
        "DALLAS" => "Dallas",
        "DAMESFE" => "Dames Ferry",
        "DANIELSV" => "Danielsville",
        "DAVESCREEK" => "Daves Creek",
        "DAVIDL" => "David L Rainer",
        "DAVIS" => "Davis - Cobb",
        "DAVISDADE" => "Davis - Dade",
        "DAVISEL" => "Davis - Muscogee",
        "DAWNVILLE" => "Dawnville",
        "DAWSONE" => "Dawson",
        "DEARING" => "Dearing",
        "DEERCHASE" => "Deer Chase",
        "DEERWOOD" => "Deerwood",
        "DEKALBPATHACADEMY" => "Dekalb Path Academy",
        "DEMOREST" => "Demorest",
        "DEWAREL" => "Dewar",
        "DIAMONDLAKES" => "Diamond Lakes",
        "DIMONEL" => "Dimon",
        "DOBBS" => "Dobbs",
        "DOERUN" => "Doerun",
        "DOLVIN" => "Dolvin",
        "DRSTHLS" => "Dorsett Shoals",
        "DOUBLEC" => "Double Churches",
        "DOOLY" => "Dooly County",
        "DOWNTOWN" => "Downtown  Magnet Academy",
        "DRGEOR" => "Dr. George Washington Carver",
        "DOVESCR" => "Doves Creek",
        "DOWELL" => "Dowell",
        "DRESDEN" => "Dresden",
        "DUEWEST" => "Due West",
        "DUGGAP" => "Dug Gap",
        "DUNAIRE" => "Dunaire",
        "DUNBAR" => "Dunbar",
        "DUNCANCREEK" => "Duncan Creek",
        "DUNLEITH" => "Dunleith",
        "DUNWOODY" => "Dunwoody",
        "DUNWDYSPGS" => "Dunwoody Springs",
        "DUTCHTOWN" => "Dutchtown",
        "DYER" => "Dyer",
        "ECWEST" => "E.C. West",
        "EAGLERIDGE" => "Eagle Ridge",
        "EAGLESP" => "Eagle Springs",
        "EARLYCO" => "Early County",
        "EASTBRO" => "East Broad Street",
        "ECENTRAL" => "East Central",
        "EASTCLAY" => "East Clayton",
        "EASTFAN" => "East Fannin",
        "EASTJACKSO" => "East Jackson",
        "EASTLAKEH" => "East Lake - Henry",
        "EASTLAU" => "East Laurens",
        "EASTNEWTON" => "East Newton",
        "EASTDCOBB" => "East Side",
        "EASTANOL" => "Eastanollee",
        "ESCOFFEE" => "Eastside - Coffee",
        "EASTSDCOWT" => "Eastside - Coweta",
        "EASTSIDED" => "Eastside - Douglas",
        "ESGRADY" => "Eastside - Grady",
        "ESPOLK" => "Eastside - Polk",
        "EASTSIDE" => "Eastside - Whitfield",
        "EASTVALLEY" => "Eastvalley",
        "EBENEZER" => "Ebenezer",
        "ECHOLSC" => "Echols County",
        "EDGEWOOD" => "Edgewood",
        "EDMONDS" => "Edmonds",
        "EBOUIE" => "Edward L. Bouie Sr.",
        "ELCANKING" => "Elcan-King",
        "ELDRIDGEM" => "Eldridge L. Miller",
        "ELLIJAY" => "Ellijay",
        "ELLIS" => "Ellis",
        "ELMST" => "Elm Street - Coweta",
        "ELMSTREET" => "Elm Street - Floyd",
        "EMERSON" => "Emerson",
        "ENOTA" => "Enota Multiple Intelligences Academy",
        "EPHESUS" => "Ephesus",
        "ESTHERJACKSON" => "Esther Jackson",
        "ETHELKN" => "Ethel W. Knight Magnet",
        "ETON" => "Eton",
        "EAGLERIDGEA" => "Eagle Ridge Academy",
        "EUCHEEC" => "Euchee Creek",
        "EUHARLEE" => "Euharlee",
        "EVANS" => "Evans",
        "EVANSDALE" => "Evansdale",
        "FSTANTON" => "F.L. Stanton",
        "FACTORYSH" => "Factory Shoals",
        "EASTLAKEF" => "East Lake - Fulton",
        "FAIROAKS" => "Fair Oaks",
        "FAIRST" => "Fair Street International",
        "FAIN" => "Fain",
        "FAIRINGTON" => "Fairington",
        "FAIRMOUNT" => "Fairmount",
        "FVHABER" => "Fairview - Habersham",
        "FAIRVIEW" => "Fairview - Henry",
        "FAIRVIEWN" => "Fairview - Newton",
        "FAIRYLAND" => "Fairyland",
        "FALLING" => "Falling Creek",
        "WHITEEDDIE" => "Eddie White Academy",
        "FARGOCH" => "Fargo Charter (Kk-03)",
        "FAYETTEVIL" => "Fayetteville Intermediate",
        "FELDWOOD" => "Feldwood",
        "FERGUSON" => "Ferguson",
        "FERNBANK" => "Fernbank",
        "FICKETT" => "Fickett",
        "FICQUETT" => "Ficquett",
        "FINDLEYOAK" => "Findley Oaks",
        "FLATROCK" => "Flat Rock",
        "FLATSHLSD" => "Flat Shoals - Dekalb",
        "FLATSHLSR" => "Flat Shoals - Rockdale",
        "FINCH" => "Finch",
        "FLINTHILL" => "Flint Hill",
        "FLIPPEN" => "Flippen",
        "FLOWERYBR" => "Flowery Branch",
        "SHELTON" => "Floyd L. Shelton",
        "FOLKSTON" => "Folkston",
        "FORD" => "Ford",
        "FORESTHILL" => "Forest Hills Academy",
        "FORREST" => "Forrest Road",
        "FORTDANIEL" => "Fort Daniel",
        "FOUNTAIN" => "Fountain",
        "FOURTHD" => "Fourth District",
        "FOWLERD" => "Fowler Drive",
        "FOXELEM" => "Fox",
        "FRANKLO" => "Frank Long",
        "FRANKLINFOREST" => "Franklin Forest",
        "FREEHOME" => "Free Home",
        "FREEDOM" => "Freedom Park",
        "FREEMAN" => "Freeman",
        "FRMANSML" => "Freemans Mill",
        "FREY" => "Frey",
        "FRIENDSHIP" => "Friendship",
        "FUNSTON" => "Funston",
        "FUTRAL" => "Futral Road",
        "GOBAI" => "G.O. Bailey Primary",
        "GADSDEN" => "Gadsden",
        "GAINES" => "Gaines",
        "GAINESVI" => "Gainesville Exploration Academy",
        "GARDENC" => "Garden City",
        "GARDNHLLS" => "Garden Hills",
        "GARDENLAKE" => "Garden Lakes",
        "GARRETT" => "Garrett",
        "GARRISONC" => "Garrison",
        "GARSNMILL" => "Garrison Mill",
        "GARRISONT" => "Garrison-Pilcher",
        "GENTIAN" => "Gentian",
        "GEORGEE" => "George E. Washington",
        "GEORGETOWNC" => "Georgetown - Chatham",
        "GEORGETOWNM" => "Georgetown - Muscogee",
        "WHITLOW" => "George W. Whitlow",
        "GIDEONS" => "Gideons",
        "GILBERT" => "Gilbert",
        "GLANTON" => "Glanton",
        "GLANTONHIND" => "Glanton-Hindsman",
        "GLASCOCK" => "Glascock County",
        "GLENNHI" => "Glenn Hills",
        "GLENNVIL" => "Glennville",
        "GLENWOOD" => "Glennwood",
        "GLENWOODPR" => "Glenwood Primary",
        "GLYNDALE" => "Glyndale",
        "GOLDENI" => "Golden Isles",
        "GOODYRG" => "Goodyear",
        "GOSHENE" => "Goshen",
        "GOULD" => "Gould",
        "GRACEWOO" => "Gracewood",
        "GRANTVILLE" => "Grantville",
        "GRAYELE" => "Gray",
        "GRAYSON" => "Grayson",
        "GRAYSVIL" => "Graysville",
        "GREENACRES" => "Green Acres",
        "GREENBRI" => "Greenbrier",
        "GREENSBO" => "Greensboro",
        "GREER" => "Greer",
        "GROVEPRK" => "Grove Park",
        "GROVETOW" => "Grovetown",
        "GULLATT" => "Gullatt",
        "GUMSPRINGS" => "Gum Springs",
        "GUYTON" => "Guyton",
        "GWINOAKS" => "Gwin Oaks",
        "HAHIRAE" => "Hahira",
        "HAINSEL" => "Hains",
        "HUTCHENS" => "Hal Hutchens",
        "HAMBRICK" => "Hambrick",
        "HAMILTONC" => "Hamilton",
        "HAMILTON" => "Hamilton Crossing",
        "HAMILTONEH" => "Hamilton E. Holmes",
        "HAMPTON" => "Hampton",
        "HAMPTO" => "Hampton L. Daughtry",
        "HANNANE" => "Hannan",
        "HAPEVILLE" => "Hapeville",
        "HARBINS" => "Harbins",
        "HARMONY" => "Harmony - Gwinnett",
        "HARMONYP" => "Harmony - Pickens",
        "BLAINE" => "Harmony - Walton",
        "HARMONYLELAND" => "Harmony-Leland",
        "HARPE" => "Harper - Clayton",
        "HARPER" => "Harper - Thomas",
        "HARRISG" => "Harris",
        "HARTLEY" => "Hartley",
        "HARTWELL" => "Hartwell",
        "HAVEN" => "Haven",
        "HAVENACADE" => "Haven Academy",
        "HAWCREEK" => "Haw Creek",
        "HAWTHRNE" => "Hawthorne - Clayton",
        "HAWTHORNE" => "Hawthorne - Dekalb",
        "HAYES" => "Hayes",
        "HAYNIE" => "Haynie",
        "HAZELGR" => "Hazel Grove",
        "RHEAD" => "Head",
        "HEARDB" => "Heard - Bibb",
        "HEARDC" => "Heard - Chatham",
        "HEARDH" => "Heard County",
        "HEARDMIXONHEARD" => "Heard-Mixon",
        "HEARDSFER" => "Heards Ferry",
        "HEMBREESPRGS" => "Hembree Springs",
        "HENDERSONM" => "Henderson Mill",
        "HENDRICKS" => "Hendricks",
        "HENDRIXDR" => "Hendrix Drive",
        "HEPHZIBA" => "Hephzibah",
        "HERITAGEB" => "Heritage - Bibb",
        "HERITAGE" => "Heritage - Fulton",
        "HESSE" => "Hesse",
        "HERNDON" => "Herndon",
        "HCKRYFLAT" => "Hickory Flat - Cherokee",
        "HICKRYFLAT" => "Hickory Flat - Henry",
        "HICKORYHLS" => "Hickory Hills",
        "HICKS" => "Hicks",
        "HIGHPOINT" => "High Point",
        "HIGHSHOALS" => "High Shoals - Clarke",
        "HIGHSHOALSO" => "High Shoals - Oconee",
        "HIGHTOWERD" => "Hightower",
        "HIGHTOWERR" => "Hightower Trail",
        "HILLCITY" => "Hill City",
        "HILLCRES" => "Hillcrest - Troup",
        "HILLSIDE" => "Hillside",
        "HILLTOP" => "Hilltop",
        "HIRAM" => "Hiram",
        "HOBOKEN" => "Hoboken",
        "HODGE" => "Hodge",
        "HOGANSVI" => "Hogansville",
        "HOLLISH" => "Hollis Hand",
        "HOLLYSPGSC" => "Holly Springs - Cherokee",
        "HOLLYSPGSD" => "Holly Springs - Douglas",
        "HOLLYDALE" => "Hollydale",
        "HOLSENBECK" => "Holsenbeck",
        "HONEYCRK" => "Honey Creek",
        "HOODAVE" => "Hood Avenue Primary",
        "HOPE" => "Hope-Hill",
        "HOPKINS" => "Hopkins",
        "HOUSE" => "House",
        "HOWARD" => "Howard",
        "HOWARDBSTROUD" => "Howard B. Stroud",
        "HUBBARD" => "Hubbard",
        "HILLCREST" => "Hillcrest - Laurens",
        "HUDDLESTON" => "Huddleston",
        "HUIE" => "Huie",
        "HULLSAN" => "Hull-Sanford",
        "HUMPHRIES" => "Humphries",
        "HUNTELE" => "Hunt",
        "HUNTBUR" => "Hunt-Burdell",
        "HUNTLEYHLLS" => "Huntley Hills",
        "HUTCHINSON" => "Hutchinson",
        "HWY341" => "Hwy 341",
        "IDLEWOOD" => "Idlewood",
        "ILA" => "Ila",
        "IMAGINEWESLEY" => "Imagine Wesley Intl Academy",
        "INDIANC" => "Indian Creek - Coffee",
        "INDIANCRK" => "Indian Creek - Dekalb",
        "INDIANKNOLL" => "Indian Knoll",
        "INGRAMP" => "Ingram-Pye",
        "INMANROAD" => "Inman",
        "INTERNATSE" => "International Studies",
        "IRWINCO" => "Irwin County",
        "ISLANDS" => "Islands",
        "ISLEOF" => "Isle Of Hope",
        "ISON" => "Ison Springs",
        "ITHICA" => "Ithica",
        "IVYCREEK" => "Ivy Creek",
        "JAALFORD" => "J. A. Alford",
        "JDYER" => "J. G. Dyer",
        "KNOX" => "J. Knox",
        "JSPATE" => "J. S. Pate",
        "JACOBGS" => "J.G. Smith",
        "LOMAXPINEV" => "J.L. Lomax",
        "JTREDDICK" => "J.T. Reddick",
        "JACKPNIXPR" => "Jack P. Nix Primary",
        "JACKSONB" => "Jackson - Butts",
        "JACKSONG" => "Jackson - Gwinnett",
        "JACKSONH" => "Jackson Heights",
        "JACKSONRD" => "Jackson Road",
        "JAMESJA" => "James Jackson",
        "JAMESTOW" => "Jamestown",
        "JASPER" => "Jasper",
        "JASPERCOUNTY" => "Jasper County",
        "JEFFDAV" => "Jeff Davis",
        "JEFFERSONJ" => "Jefferson",
        "JEFFERSON" => "Jefferson Parkway",
        "JEFFERSONV" => "Jeffersonville",
        "JENKINSGWIN" => "Jenkins",
        "JENKINSJ" => "Jenkins County",
        "JENKINSR" => "Jenkins-White",
        "JERGER" => "Jerger",
        "JESUPEL" => "Jesup",
        "JOHNJOHNSO" => "John Johnson",
        "JOHNSCREEK" => "Johns Creek",
        "JOHNSONM" => "Johnson  - Muscogee",
        "JOHNSON" => "Johnson - Floyd",
        "JOHNSONJ" => "Johnson County",
        "JOHNSTON" => "Johnston",
        "JOLLY" => "Jolly",
        "JONESB" => "Jones - Bibb",
        "HAJONES" => "Jones - Haralson",
        "JONESWHEAT" => "Jones-Wheat",
        "JORDANHILL" => "Jordan Hill Road",
        "JORDYEB" => "Jordye Bacon",
        "JOSEPHM" => "Joseph Martin",
        "JJHARRIS" => "Judia Jackson Harris",
        "KANOHEDA" => "Kanoheda",
        "KAYRD" => "Kay Road",
        "KEDRON" => "Kedron",
        "KEHELEY" => "Keheley",
        "KELLEYLK" => "Kelley Lake",
        "KELLYMILL" => "Kelly Mill",
        "KEMP" => "Kemp - Clayton",
        "KEMPC" => "Kemp - Cobb",
        "KENNEDY" => "Kennedy",
        "KENNESAW" => "Kennesaw",
        "KEYELEM" => "Key",
        "KILOUGH" => "Kilough",
        "KILPATRICK" => "Kilpatrick",
        "KIMBERLY" => "Kimberly",
        "KINCAID" => "Kincaid",
        "KINGSPGS" => "King Springs",
        "KBSUTTON" => "Katherine B. Sutton",
        "MLKBIBB" => "King-Danforth",
        "KINGSCH" => "Kings Chapel",
        "KINGSLAN" => "Kingsland",
        "KINGSLEY" => "Kingsley",
        "KINGSTON" => "Kingston",
        "KITTREDGE" => "Kittredge Magnet",
        "KNIGHT" => "Knight",
        "KNOLLWOOD" => "Knollwood",
        "LKMOSS" => "L.K. Moss",
        "LABELLE" => "Labelle",
        "LAKECITY" => "Lake City",
        "LAKEFOREST" => "Lake Forest",
        "LAKEFOR" => "Lake Forest Hills",
        "LAKEJOY" => "Lake Joy",
        "LAKEPARKD" => "Lake Park - Dougherty",
        "LAKEPARKL" => "Lake Park - Lowndes",
        "LAKERIDGE" => "Lake Ridge",
        "LAKEWINDW" => "Lake Windward",
        "LAMARCO" => "Lamar County",
        "LAMARRE" => "Lamar Reese",
        "LAMAREL" => "Lamar-Milledge",
        "LANE" => "Lane",
        "LANGSTON" => "Langston Chapel",
        "LANIER" => "Lanier",
        "LANIERC" => "Lanier County",
        "LARGOTIBET" => "Largo-Tibet",
        "LAURELRDG" => "Laurel Ridge",
        "LAVONIA" => "Lavonia",
        "LWRNCVLLE" => "Lawrenceville",
        "LEE" => "Lee",
        "LEECOUN" => "Lee County",
        "LEERTIPPENS" => "Lee R Tippens",
        "LEEST" => "Lee Street",
        "LENLAST" => "Len Lastinger Primary",
        "MILES" => "Lenora P. Miles",
        "LEROYMASSEY" => "Leroy Massey",
        "LANIERP" => "Lanier Primary",
        "LEVELCREEK" => "Level Creek",
        "LEVELGROVE" => "Level Grove",
        "LEWIS" => "Lewis - Cobb",
        "LEWISEL" => "Lewis - Hancock",
        "LEWISTON" => "Lewiston",
        "LIBERTYL" => "Liberty",
        "LIBERTY" => "Liberty - Cherokee",
        "LIBERTYS" => "Liberty - Stephens",
        "LIBERTYPOINT" => "Liberty Point",
        "LILBURN" => "Lilburn",
        "LILLIANCPO" => "Lillian C. Poole",
        "LINCOLN" => "Lincoln County",
        "LINCOLND" => "Lincoln Fundamental (KK-05)",
        "LINDSEY" => "Lindsey",
        "LINWOOD" => "Linwood",
        "LITHASPGS" => "Lithia Springs",
        "LTLERIVER" => "Little River",
        "LIVEOAK" => "Live Oak - Dougherty",
        "LIVEOAKN" => "Live Oak - Newton",
        "LIVINGSTON" => "Livingston",
        "LIVSEY" => "Livsey",
        "LOCKHEED" => "Lockheed",
        "LOCUSTGR" => "Locust Grove",
        "LOGANVILLE" => "Loganville",
        "LOGANVILLEP" => "Loganville Primary",
        "LNGBRNCH" => "Long Branch",
        "LONGCAN" => "Long Cane",
        "LONGCOU" => "Long County(PK-12)",
        "LONNIEJAK" => "Lonnie Jackson Academy",
        "LORRAINE" => "Lorraine",
        "LOUISVIL" => "Louisville Academy",
        "LOVIN" => "Lovin",
        "LOW" => "Low",
        "LUELLA" => "Luella",
        "LULA" => "Lula",
        "LUMPKIN" => "Lumpkin County",
        "LYERLY" => "Lyerly",
        "LYMANHALL" => "Lyman Hall - Hall",
        "LYMANHA" => "Lyman Hall - Liberty",
        "LYONSEL" => "Lyons",
        "MJONES" => "M. A. Jones",
        "MASO" => "M.H. Mason",
        "MABLETON" => "Mableton",
        "MACEDONIA" => "Macedonia",
        "MACONCO" => "Macon County",
        "MAGILL" => "Magill",
        "MAGNOLIA" => "Magnolia",
        "MAIN" => "Main",
        "MAINSTREET" => "Mainstreet",
        "MALCOMBRID" => "Malcom Bridge",
        "MAMIELO" => "Mamie Lou Gross",
        "MANNINGOKS" => "Manning Oaks",
        "MANSFIELD" => "Mansfield",
        "MARBUT" => "Marbut",
        "MARGARETWI" => "Margaret Winn Holt",
        "MARIONC" => "Marion County",
        "MARLOW" => "Marlow",
        "MARSHPOI" => "Marshpoint",
        "MARTINH" => "Martin",
        "MLKC" => "Martin Luther King Jr - Clayton",
        "MLKD" => "Martin Luther King Jr - Dougherty",
        "MLKM" => "Martin Luther King Jr - Muscogee",
        "MARTINEZ" => "Martinez",
        "MARYLEE" => "Mary Lee Clark",
        "MARYLIN" => "Mary Lin",
        "MASHBURN" => "Mashburn",
        "MASONCREEK" => "Mason Creek",
        "MATHEWS" => "Mathews",
        "MATILDA" => "Matilda Harris",
        "MATT" => "Matt",
        "MATTWIL" => "Matt Wilson",
        "MATTHEW" => "Matthew Arthur",
        "MATTIEL" => "Mattie Lively",
        "MAXWELL" => "Maxwell",
        "MAYSVILLE" => "Maysville",
        "MRSMITH" => "Martha Rawls Smith",
        "MCBEANE" => "Mcbean",
        "MCCALLPRIMARY" => "Mccall Primary",
        "MCDONOUGH" => "Mcdonough",
        "MCEVER" => "Mcever",
        "MMGARITY" => "Mcgarity",
        "MCHENRYPRI" => "Mchenry Primary",
        "MCKENDREE" => "Mckendree",
        "MCLAENDON" => "Mclendon",
        "MEADOWBR" => "Meadowbrook",
        "MEADOWCREEK" => "Meadowcreek",
        "MEADOWVIEW" => "Meadowview",
        "MEDLOCKBR" => "Medlock Bridge",
        "MEMORIAL" => "Memorial Drive",
        "MENIO" => "Menlo",
        "MERRYEL" => "Merry",
        "METTER" => "Metter",
        "METTERI" => "Metter Intermediate (4-5)",
        "MIDDLERDG" => "Middle Ridge",
        "MIDLAND" => "Midland Academy",
        "MIDVALE" => "Midvale",
        "MIDWAYB" => "Midway - Baldwin",
        "MIDWAYD" => "Midway - Dekalb",
        "MIDWAYF" => "Midway - Forsyth",
        "MIDWAYPRIM" => "Midway Primary",
        "MILFORD" => "Milford",
        "MILLCRE" => "Mill Creek",
        "MILLERB" => "Miller - Bibb",
        "MILLERE" => "Miller - Houston",
        "MILLERC" => "Miller County",
        "MIMOSA" => "Mimosa",
        "MIDWAYP" => "Midway - Pierce",
        "RMINOR" => "Minor",
        "MIRRORLAKE" => "Mirror Lake",
        "MISSIONRD" => "Mission Road",
        "MODEL" => "Model",
        "MONROE" => "Monroe",
        "MONROEP" => "Monroe Primary",
        "MONTCLAIR" => "Montclair",
        "MONTESA" => "Monte Sano",
        "MONTGOMERY" => "Montgomery",
        "MONTGOME" => "Montgomery County",
        "MOORE" => "Moore",
        "MOORESTREE" => "Moore Street",
        "MORELAN" => "Moreland",
        "MORELAND" => "Moreland Rd",
        "MORGAN" => "Morgan",
        "MORGANC" => "Morgan County",
        "MORNINGSD" => "Morningside - Dougherty",
        "MORNINGSH" => "Morningside - Houston",
        "MORNINGSID" => "Morningside-",
        "MORROW" => "Morrow",
        "MOSSYCREEK" => "Mossy Creek",
        "MOULTONB" => "Moulton-Branch",
        "MTBETHEL" => "Mount Bethel",
        "MTCARMELD" => "Mount Carmel - Douglas",
        "MTCARMELH" => "Mount Carmel - Henry",
        "MTOLIVE" => "Mount Olive",
        "MTVERNON" => "Mount Vernon",
        "MTYONAH" => "Mount Yonah",
        "MTZIONCR" => "Mount Zion - Carroll",
        "MTZIONCL" => "Mount Zion - Clayton",
        "MTPRKF" => "Mountain Park - Fulton",
        "MTPARKG" => "Mountain Park - Gwinnett",
        "MTROAD" => "Mountain Road",
        "MTVIEW" => "Mountain View - Cobb",
        "MOUNTAING" => "Mountain View - Gilmer",
        "MOUNTAINM" => "Mountain View - Meriwether",
        "MOUNTVIL" => "Mountville",
        "MULBERRYG" => "Mulberry",
        "MULBERRY" => "Mulberry Creek",
        "MURDOCK" => "Murdock",
        "MURPHYCAN" => "Murphy Candler",
        "MUSCOGEE" => "Muscogee",
        "MUSEUM" => "Museum School",
        "MYERS" => "Myers",
        "NAHUNTA" => "Nahunta",
        "NAOMI" => "Naomi",
        "NARVIEH" => "Narvie Harris",
        "NATIONAL" => "National Hills",
        "NEBO" => "Nebo",
        "NBHDCHARTER" => "Neighborhood Charter",
        "NESBIT" => "Nesbit",
        "NEVILS" => "Nevils",
        "NEWGEORGI" => "New Georgia",
        "NEWHOLL" => "New Holland Core Knowledge Academy",
        "NEWHOPEH" => "New Hope - Henry",
        "NEWHOPE" => "New Hope - Whitfield",
        "NEWMANCH" => "New Manchester",
        "NEWMOUN" => "New Mountain Hill",
        "NEWPROSPEC" => "New Prospect",
        "NEWNANCRO" => "Newnan Crossing",
        "NICHOLLS" => "Nicholls",
        "NICHOLSON" => "Nicholson",
        "NICKAJACK" => "Nickajack",
        "NOLAN" => "Nolan",
        "NORCROSS" => "Norcross",
        "NORMANP" => "Norman Park",
        "NORRISE" => "Norris",
        "NORTHBR" => "North Brooks",
        "NORTHCO" => "North Columbia",
        "NORTHCOLUMBUS" => "North Columbus",
        "NORTHDO" => "North Dodge",
        "NORTHDOUGLAS" => "North Douglas",
        "NFAYETTE" => "North Fayette",
        "NORTHHARLEM" => "North Harlem",
        "NORTHHART" => "North Hart",
        "NHEIGHTS" => "North Heights",
        "NORTHJACKSON" => "North Jackson",
        "NORTHLAFAY" => "North Lafayette",
        "MITCHELLP" => "North Mitchell County",
        "NORTHMU" => "North Mulberry",
        "NORTHCUTT" => "Northcutt",
        "NORTHSIDEC" => "Northside - Coweta",
        "NORTHSIDED" => "Northside - Dougherty",
        "NORTHSIDEG" => "Northside - Grady",
        "NORTHSIDEH" => "Northside - Houston",
        "NORTHSIDEP" => "Northside - Paulding",
        "NORTHSIDEPOLK" => "Northside - Polk",
        "NORTHSIDET" => "Northside Primary",
        "NORTHWES" => "Northwest",
        "NORTHWESTL" => "Northwest Laurens",
        "NORTHWOOD" => "Northwood",
        "RNORTON" => "Norton",
        "NORTNPRK" => "Norton Park",
        "NUNN" => "Nunn",
        "OAKGROVEC" => "Oak Grove - Cherokee",
        "OAKGROVED" => "Oak Grove - Dekalb",
        "OAKGROVEF" => "Oak Grove - Fayette",
        "OAKHILL" => "Oak Hill",
        "OAKKNOLL" => "Oak Knoll",
        "OAKCLIFF" => "Oakcliff",
        "OAKHUHURST" => "Oakhurst",
        "OAKLANDG" => "Oakland - Gilmer",
        "OAKLAND" => "Oakland - Henry",
        "OAKLEY" => "Oakley",
        "OAKVIEW" => "Oakview",
        "OAKWOOD" => "Oakwood",
        "OCEE" => "Ocee",
        "OCONEECOUN" => "Oconee County",
        "ODOM" => "Odom",
        "ODUMELE" => "Odum",
        "OGLEAVE" => "Oglethorpe Avenue",
        "OGLEC" => "Oglethorpe County",
        "OGLEPOINT" => "Oglethorpe Point",
        "OKAPILCO" => "Okapilco",
        "OLAELA" => "Ola",
        "OLIVER" => "Oliver",
        "OMEGAEL" => "Omega",
        "ORRS" => "Orrs",
        "PALMERSTON" => "Palmer Stone",
        "PALMETTO" => "Palmetto",
        "PANOLAWA" => "Panola Way",
        "PARKELE" => "Park",
        "PARKCREEK" => "Park Creek",
        "PARKST" => "Park Street",
        "PARKERM" => "Parker Mathis",
        "PARKLANE" => "Parklane",
        "PARKSID" => "Parkside",
        "PARKWOOD" => "Parkwood",
        "PARSONS" => "Parsons",
        "PARTEE" => "Partee",
        "PATESCREEK" => "Pates Creek",
        "PATRICK" => "Patrick",
        "PATTERSO" => "Patterson",
        "PEACHTREEG" => "Peachtree",
        "PEACHTREEF" => "Peachtree City",
        "PEARLST" => "Pearl Stephens",
        "PEARSON" => "Pearson",
        "PEEKSCHAPEL" => "Peeks Chapel",
        "PEEPLES" => "Peeples",
        "PELHAM" => "Pelham",
        "PEPPERELL" => "Pepperell",
        "PEPPERELLP" => "Pepperell Primary",
        "PERDUEE" => "Perdue",
        "PERKERSON" => "Perkerson",
        "PERRYEL" => "Perry",
        "PEYTNFRST" => "Peyton Forest",
        "PHARR" => "Pharr",
        "PICKETTSMILL" => "Picketts Mill",
        "PIKECOU" => "Pike County",
        "PINEFOREST" => "Pine Forest",
        "PINEGRO" => "Pine Grove",
        "PINELOG" => "Pine Log",
        "PINERIDGE" => "Pine Ridge - Dekalb",
        "PINERID" => "Pine Ridge - Harris",
        "PINEST" => "Pine Street",
        "PITNER" => "Pitner",
        "PLEASENTGR" => "Pleasant Grove - Henry",
        "PLEASANTGR" => "Pleasant Grove - Whitfield",
        "PLEASENTDL" => "Pleasantdale",
        "PTSOUTH" => "Pointe South",
        "POOLER" => "Pooler",
        "POPLAR" => "Poplar Road",
        "PORTWEN" => "Port Wentworth",
        "PORTAL" => "Portal",
        "PORTER" => "Porter",
        "RANDOLPH" => "Randolph",
        "PORTERDALE" => "Porterdale",
        "POTTERSTRE" => "Potter Street",
        "PWDRSPGS" => "Powder Springs",
        "POWRSFRRY" => "Powers Ferry",
        "PRINCETON" => "Princeton",
        "PUCKETTSMILL" => "Pucketts Mill",
        "PULASKI" => "Pulaski",
        "PULASKIC" => "Pulaski County",
        "PUTNAMC" => "Putnam County",
        "QUAILRU" => "Quail Run",
        "PROVIDENCE" => "Providence",
        "QUITMAN" => "Quitman",
        "NEWQUIT" => "Quitman County",
        "MOOREC" => "R.M. Moore",
        "RABUNCOUNTY" => "Rabun County",
        "RADIUMS" => "Radium Springs",
        "RAINBOW" => "Rainbow",
        "RANDOLPHR" => "Randolph County",
        "REDBUDG" => "Red Bud - Gordon",
        "REDBUD" => "Red Bud - Gordon",
        "REDOAK" => "Red Oak",
        "REDANE" => "Redan",
        "REESERO" => "Reese Road",
        "REIDSVIL" => "Reidsville",
        "RENAISS" => "Renaissance",
        "RICE" => "Rice",
        "RICHMOND" => "Richmond Hill",
        "SANDERSV" => "Ridge Road",
        "RIGDONR" => "Rigdon Road",
        "RILEY" => "Riley",
        "RINCON" => "Rincon",
        "RINGGOLD" => "Ringgold",
        "RITCH" => "Ritch",
        "RIVEREVES" => "River Eves",
        "RIVERRIDGE" => "River Ridge",
        "RIVERRO" => "River Road",
        "RIVERBEND" => "Riverbend",
        "RIVERDALE" => "Riverdale",
        "RIVERS" => "Rivers - Fayette",
        "ERIVERS" => "Rivers - Fulton",
        "RIVERSEDGE" => "Rivers Edge",
        "RIVERSIDE" => "Riverside - Cobb",
        "RIVERSIDEC" => "Riverside - Columbia",
        "RIVERSIDEF" => "Riverside - Fulton",
        "RIVERSIDEG" => "Riverside - Gwinnett",
        "RIVERVIEW" => "Riverview",
        "ROAN" => "Roan",
        "BURCH" => "Robert J. Burch",
        "ROBERTSHAWTHEME" => "Robert Shaw Theme",
        "ROBERTSTS" => "Roberta T. Smith",
        "ROBERTS" => "Roberts",
        "ROBINSON" => "Robinson",
        "RKCHAPEL" => "Rock Chapel",
        "ROCKSPRNG" => "Rock Spring - Henry",
        "ROCKSPRING" => "Rock Spring - Walker",
        "ROCKSPRINGS" => "Rock Springs",
        "ROCKBRIDGD" => "Rockbridge - Dekalb",
        "ROCKBRIDGG" => "Rockbridge - Gwinnett",
        "ROCKYBRANC" => "Rocky Branch",
        "ROCKYCRK" => "Rocky Creek",
        "ROCKYMT" => "Rocky Mount",
        "ROCKYPLAINS" => "Rocky Plains",
        "ROLANDWRUS" => "Roland W. Russom",
        "ROLLINS" => "Rollins",
        "MCNAIRDISCOVERY" => "Ronald E McNair Discover Learning Acad",
        "ROOPVILLE" => "Roopville",
        "ROSEBUD" => "Rosebud",
        "ROSEMONT" => "Rosemont",
        "ROSSVILLE" => "Rossville",
        "ROSWELLN" => "Roswell North",
        "ROWLAND" => "Rowland",
        "ROYSTON" => "Royston",
        "RUSKINE" => "Ruskin",
        "RUSSELL" => "Russell - Cobb",
        "RUSSELLH" => "Russell - Houston",
        "RUTHHILL" => "Ruth Hill",
        "SGA" => "S G A",
        "SLLEWIS" => "S.L. Lewis",
        "MASON" => "S.L. Mason",
        "SAGMRHLLS" => "Sagamore Hills",
        "SAINTMA" => "Saint Marys",
        "SALLASMAHO" => "Sallas Mahone",
        "SALLIEZ" => "Sallie Zetterower",
        "SALLYDAILE" => "Sally Dailey Meadows",
        "PANTER" => "Sam D. Panter",
        "SANDHILL" => "Sand Hill - Carroll",
        "SANDHILLE" => "Sand Hill - Effingham",
        "SANDERS" => "Sanders",
        "SARAHARPMINTER" => "Sara Harp Minter",
        "SARAHCO" => "Sarah Cobb",
        "SRAGSDALE" => "Sarah Ragsdale",
        "SARDIS" => "Sardis",
        "SATILLA" => "Satilla",
        "SATILLAM" => "Satilla Marsh",
        "SAWNEE" => "Sawnee",
        "SAWNPRI" => "Sawnee Primary",
        "SAWYER" => "Sawyer Road",
        "SAXONHEIGH" => "Saxon Heights",
        "SCHLEYC" => "Schley County",
        "SCOT" => "Scott - Fulton",
        "SCOTT" => "Scott - Thomas",
        "SCREVENW" => "Screven",
        "SCREVEN" => "Screven County",
        "SEBRNLEE" => "Seaborn Lee",
        "SEDLIAPRK" => "Sedalia Park",
        "SEMINOLE" => "Seminole County",
        "SETTLEBR" => "Settles Bridge",
        "SHADOWROC" => "Shadow Rock",
        "SHAKERAG" => "Shakerag",
        "SHALLOWFOR" => "Shallowford Falls",
        "SHARON" => "Sharon - Forsyth",
        "SHARONW" => "Sharon - Walton",
        "SHARPCREEK" => "Sharp Creek",
        "SHERWOOD" => "Sherwood",
        "SHILOH" => "Shiloh",
        "SHILOHPT" => "Shiloh Point",
        "SHIRLEY" => "Shirley Hills",
        "SHIVER" => "Shiver",
        "SHOALCREEK" => "Shoal Creek",
        "SMYRNA" => "Smyrna",
        "SHUMAN" => "Shuman",
        "SILVERCITY" => "Silver City",
        "SIMONTON" => "Simonton",
        "SIMPSON" => "Simpson",
        "SIMS" => "Sims",
        "SKYVIEW" => "Sky View",
        "SIXES" => "Sixes",
        "SKYVIEWB" => "Skyview",
        "SMITH" => "Smith",
        "SLATER" => "Slater",
        "SMILEYE" => "Smiley",
        "SMITHBARNESMITH" => "Smith-Barnes",
        "SMOKERIS" => "Smoke Rise",
        "SNAPFINGER" => "Snapfinger",
        "SOCIALCIRC" => "Social Circle",
        "SONORAVILLE" => "Sonoraville",
        "SOPECREEK" => "Sope Creek",
        "SOUTHCOC" => "South Columbia",
        "SOUTHCOM" => "South Columbus",
        "SOUTHDO" => "South Dodge",
        "SOUTHDGLS" => "South Douglas",
        "SOUTHEF" => "South Effingham",
        "SOUTHHA" => "South Hart",
        "SOUTHJACKSON" => "South Jackson",
        "MITCHELL" => "South Mitchell",
        "SOUTHRA" => "South Rabun",
        "SOUTHSALEM" => "South Salem",
        "SEAST" => "Southeast - Floyd",
        "SOUTHEAST" => "Southeast - Loundes",
        "SOUTHSIDEG" => "Southside - Grady",
        "SOUTHSIDER" => "Southside - Richmond",
        "SWHANCOCK" => "Southwest  - Hancock",
        "SWCHATHAM" => "Southwest - Chatham",
        "SWLAURENS" => "Southwest Laurens",
        "SWCRISP" => "Southwestern",
        "SPALDINGDR" => "Spalding Drive",
        "SPENCER" => "Spencer",
        "SPOUTSPRI" => "Spout Springs",
        "SPRINGHILL" => "Spring Hill",
        "SPRINGP" => "Spring Place",
        "SPRINGDA" => "Springdale",
        "SPRINGDALED" => "Springdale Park",
        "SPRINGFI" => "Springfield",
        "STGEOR" => "St. George",
        "STMARYS" => "St. Marys",
        "STSIMON" => "St. Simons",
        "STARK" => "Stark",
        "STARLING" => "Starling",
        "STATEBR" => "State Bridge Crossing",
        "STATHAM" => "Statham",
        "STERLING" => "Sterling",
        "STEVENS" => "Stevens Creek",
        "STEWART" => "Stewart County",
        "STILL" => "Still",
        "STILSON" => "Stilson",
        "STOCKBRIDG" => "Stockbridge",
        "STONECREEK" => "Stone Creek",
        "STONEMILL" => "Stone Mill",
        "STMTN" => "Stone Mountain",
        "STONEVIEW" => "Stoneview",
        "STONEWALL" => "Stonewall Tell",
        "STRINGFE" => "Stringfellow",
        "SUDER" => "Suder",
        "SUEREYN" => "Sue Reynolds",
        "SUGARHILL" => "Sugar Hill - Gwinnett",
        "SUGARHILLH" => "Sugar Hill - Hall",
        "SUGARMIL" => "Sugarmill",
        "SUMMERVI" => "Summerville",
        "SUMMITTHILL" => "Summit Hill",
        "SUMTERC" => "Sumter County",
        "SUNSET" => "Sunset",
        "STRIPLING" => "Susan Stripling",
        "SUSIEDASHE" => "Susie Dasher",
        "SUWANEE" => "Suwanee",
        "SWAINSBO" => "Swainsboro",
        "SWEETAPPLE" => "Sweet Apple",
        "SWEETWATER" => "Sweetwater",
        "SWINT" => "Swint",
        "SWAIN" => "Swain",
        "SYCAMORE" => "Sycamore",
        "SYLVESTERD" => "Sylvester Road",
        "TGSCOTT" => "T G Scott",
        "TADMORE" => "Tadmore",
        "CENTAL" => "Talbot County",
        "TALIAFER" => "Taliaferro County",
        "TALLAPOOSA" => "Tallapoosa Primary",
        "TARA" => "Tara",
        "TATE" => "Tate",
        "TAYLOR" => "Taylor - Gwinnett",
        "TAYLORC" => "Taylor County",
        "TAYLORB" => "Taylor Rosa",
        "TAYLORS" => "Taylors Creek",
        "TAYLORSVIL" => "Taylorsville",
        "TEASLEY" => "Teasley",
        "TELFAIR" => "Telfair County",
        "TEMPLE" => "Temple",
        "TENNILLE" => "Tennille",
        "TERRACE" => "Terrace Manor",
        "THOMASCROS" => "Thomas Crossroads",
        "THOMASVILL" => "Thomasville Heights",
        "THOMSON" => "Thomson",
        "THUNDERB" => "Thunderbolt",
        "MARSHALL" => "Thurgood  Marshall",
        "TIGERCR" => "Tiger Creek",
        "TIMBERRDG" => "Timber Ridge - Cobb",
        "TIMBERRIDGE" => "Timber Ridge - Henry",
        "TIMOTHY" => "Timothy",
        "TOBACCO" => "Tobacco Road",
        "TOCCOA" => "Toccoa",
        "TODDGRA" => "Todd Grant",
        "TOLBERT" => "Tolbert",
        "TONEY" => "Toney",
        "TOOMBSC" => "Toombs Central",
        "FREDATOOMER" => "Toomer",
        "TOWNS" => "Towns",
        "TOWNSCO" => "Towns County",
        "TREUTLEN" => "Treutlen County",
        "TRION" => "Trion",
        "TUBMAN" => "Tubman",
        "TRIP" => "Trip",
        "TURNERWOODS" => "Turner Woods",
        "TRITT" => "Tritt",
        "TUCKERE" => "Tucker",
        "TUNNELHILL" => "Tunnel Hill",
        "TURNER" => "Turner",
        "TURNERC" => "Turner County",
        "TURNERCS" => "Turner County Specialty(Pk-12)",
        "TUSSAHAW" => "Tussahaw",
        "TWINCIT" => "Twin City",
        "TWINOAK" => "Twin Oaks",
        "TYRONE" => "Tyrone",
        "UNIONB" => "Union - Bibb",
        "UNION" => "Union - Paulding",
        "UNIONCO" => "Union County",
        "UNIONPO" => "Union Point",
        "UNITYM" => "Unity - Meriwether",
        "UNITYT" => "Unity - Troup",
        "UNITYGROVE" => "Unity Grove",
        "UNIVERSITYCHARTER" => "University Community Charter",
        "UPSONLEEN" => "Upson-Lee North",
        "USONLEES" => "Upson-Lee South",
        "VALLEYPOIN" => "Valley Point",
        "VANWERT" => "Van Wert",
        "VANDERLYN" => "Vanderlyn",
        "VARNELL" => "Varnell",
        "VARNER" => "Varner",
        "VAUGHAN" => "Vaughan",
        "VENETIAN" => "Venetian Hills",
        "VICKERYE" => "Vickery Creek",
        "VILLARICA" => "Villa Rica",
        "VINEVILL" => "Vineville Academy (Kk-06)",
        "WJCOOPER" => "W.J. Cooper",
        "HORNSBY" => "W.S. Hornsby",
        "ABNEY" => "WC Abney",
        "WACONAE" => "Wacona",
        "WADDELL" => "Waddell",
        "WALDOPA" => "Waldo Pafford",
        "WALKERINMAN" => "Walker Inman",
        "WALKERPRK" => "Walker Park",
        "WALNUTCREEK" => "Walnut Creek",
        "WALNUTGRO" => "Walnut Grove - Gwinnett",
        "WALNUTGROV" => "Walnut Grove - Walton",
        "WAREMAG" => "Ware Magnet(Kk-12)",
        "WARESBOR" => "Waresboro",
        "WARRENR" => "Warren Road",
        "JACKSON" => "Warren T. Jackson",
        "WAPARK" => "Washington Park",
        "WAWILKES" => "Washington-Wilkes",
        "WATERS" => "Waters",
        "WAUKAMT" => "Wauka Mountain",
        "WAYNESVI" => "Waynesville Primary",
        "WEBSTER" => "Webster County",
        "WELCH" => "Welch",
        "WELLSEL" => "Wells",
        "WESLEYH" => "Wesley Heights",
        "WESLEYLAKE" => "Wesley Lakes",
        "WESTBAINBR" => "West Bainbridge",
        "WCENTRAL" => "West Central",
        "WESTCHA" => "West Chatham",
        "WESTCLTN" => "West Clayton",
        "WESTSIDEC" => "West Side - Catoosa",
        "WESTSID" => "West Side - Cobb",
        "WESTSIDET" => "West Side Magnet(3-8)",
        "WEND" => "West End",
        "WESTFAN" => "West Fannin",
        "WESTTOW" => "West Town",
        "WESTGORDON" => "West Gordon",
        "WESTGRE" => "West Green",
        "WESTERN" => "Western",
        "WESTMINI" => "Westminister Christian Academy (PK-12)",
        "WESTHARALS" => "West Haralson",
        "WESTJACKSON" => "West Jackson",
        "WESTMONT" => "Westmont",
        "WESTSIDECO" => "Westside - Coffee",
        "WESTSIDEH" => "Westside - Houston",
        "WESTJACK" => "West Jackson Primary",
        "WESTMANOR" => "West Manor",
        "WESTSIDEL" => "Westside - Lowndes",
        "WESTSIDEP" => "Westside - Polk",
        "WESTNEWTON" => "West Newton",
        "WESTPOI" => "West Point",
        "WESTSIDE" => "Westside - Whitfield",
        "WESTWOOD" => "Westwood",
        "WHEELER" => "Wheeler County",
        "WHEELESS" => "Wheeless Road",
        "WHIGHAM" => "Whigham",
        "WHITEDA" => "Whit Davis Road",
        "WHITEB" => "White - Bartow",
        "WHITEF" => "White - Fulton",
        "WHITEBL" => "White Bluff",
        "WHITECOUNT" => "White County Intermediate",
        "WHITEOAK" => "White Oak - Coweta",
        "WHITEOAKG" => "White Oak - Gwinnett",
        "WHITESULR" => "White Sulphur",
        "WHITEFOORD" => "Whitefoord",
        "WHITEHEA" => "Whitehead Road",
        "WHITESBURG" => "Whitesburg",
        "WHITESVI" => "Whitesville Road",
        "WILCOX" => "Wilcox County",
        "WILKINSO" => "Wilkinson County",
        "WILKINSONG" => "Wilkinson Garden",
        "WGHASTY" => "William G. Hasty- SR.",
        "MCGARRAH" => "William M. Mcgarrah",
        "WILLIAMSB" => "Williams - Bibb",
        "WILLIAMS" => "Williams - Fulton",
        "WILLIAMSW" => "Williams Heights",
        "WILLISF" => "Willis Foreman",
        "WILLISROAD" => "Willis Road",
        "WILLOCOO" => "Willocoochee",
        "WILSONCREE" => "Wilson Creek",
        "WINDSORC" => "Windsor Forest",
        "WINDSORR" => "Windsor Spring Road",
        "WINNHOLT" => "Winn Holt",
        "WINONAPRK" => "Winnona Park",
        "WINSTON" => "Winston",
        "WINTERVI" => "Winterville",
        "WOODBINE" => "Woodbine",
        "WOODLAND" => "Woodland",
        "WOODLANDCH" => "Woodland Charter",
        "WOODLAWN" => "Woodlawn",
        "WOODRIDGE" => "Woodridge",
        "WOODSON" => "Woodson",
        "WOODSTATION" => "Woodstation",
        "WOODSTOCK" => "Woodstock",
        "WOODVILL" => "Woodville",
        "WOODWARD" => "Woodward",
        "WOODWARDMILL" => "Woodward Mill",
        "WOODYGA" => "Woody Gap",
        "SYLVESTERW" => "Worth County",
        "WRENSEL" => "Wrens",
        "WRIGHT" => "Wright",
        "WYNBROOKE" => "Wynbrooke",
        "WYNNTON" => "Wynnton",
        "YARGO" => "Yargo",
        "YOUNGSGRV" => "Youngs Grove",
        "YOUTH" => "Youth",
        "BAROT" => "Bartow - Other",
        "BACONOTH" => "Bacon - Other",
        "APPLINGOTH" => "Appling - Other",
        "BAKEROTH" => "Baker - Other",
        "BALDWINOTH" => "Baldwin - Other",
        "OTHERBAN" => "Banks - Other",
        "BRWOT" => "Barrow - Other",
        "BENHILLOTH" => "Ben Hill - Other",
        "CANDLEROTH" => "Candler - Other",
        "OTHERBT" => "Butts - Other",
        "BERRIENOTH" => "Berrien - Other",
        "BLECKLEYOTH" => "Bleckley - Other",
        "BROOKSOTH" => "Brooks - Other",
        "BIBBOTH" => "Bibb - Other",
        "OTHERCAR" => "Carroll - Other",
        "OTHERCAT" => "Catoosa - Other",
        "BRANTLEYOTH" => "Brantley - Other",
        "BRYANOTH" => "Bryan - Other",
        "CHROT" => "Cherokee - Other",
        "BULLOCHOTH" => "Bulloch - Other",
        "BURKEOTH" => "Burke - Other",
        "CAMDENOTH" => "Camden - Other",
        "CALHOUNOTH" => "Calhoun - Other",
        "CHARLTONOTH" => "Charlton - Other",
        "CHATHAMOTH" => "Chatham - Other",
        "COWOT" => "Coweta - Other",
        "COBOT" => "Cobb - Other",
        "CHATTAHOOCHEEOTH" => "Chattahoochee - Other",
        "OTHERCHA" => "Chattooga - Other",
        "OTHERCLA" => "Clarke - Other",
        "COFFEEOTH" => "Coffee - Other",
        "CRAWFORDOTH" => "Crawford - Other",
        "CLAYOTH" => "Clay - Other",
        "COLUMBIAOTH" => "Columbia - Other",
        "CLINCHOTH" => "Clinch - Other",
        "COOKOTH" => "Cook - Other",
        "DEKOT" => "Dekalb - Other",
        "COLQUITTOTH" => "Colquitt - Other",
        "CRISPOTH" => "Crisp - Other",
        "ELBERTOTH" => "Elbert - Other",
        "OTHERDAD" => "Dade - Other",
        "DAWOT" => "Dawson - Other",
        "DECATUROTH" => "Decatur - Other",
        "DODGEOTH" => "Dodge - Other",
        "DOOLYOTH" => "Dooly - Other",
        "DOUOT" => "Douglas - Other",
        "DOUGHERTYOTH" => "Dougherty - Other",
        "FOROT" => "Forsyth - Other",
        "ECHOLSOTH" => "Echols - Other",
        "EARLYOTH" => "Early - Other",
        "EFFINGHAMOTH" => "Effingham - Other",
        "OTHERFLO" => "Floyd - Other",
        "EVANSOTH" => "Evans - Other",
        "GLASCOCKOTH" => "Glascock - Other",
        "EMANUELOTH" => "Emanuel - Other",
        "FANNINOTH" => "Fannin - Other",
        "GWIOT" => "Gwinnett - Other",
        "FAYOT" => "Fayette - Other",
        "OTHERGOR" => "Gordon - Other",
        "HALOT" => "Hall - Other",
        "OTHERHAB" => "Habersham - Other",
        "OTHERFRA" => "Franklin - Other",
        "FULOT" => "Fulton - Other",
        "OTHERGIL" => "Gilmer - Other",
        "HARTOTH" => "Hart - Other",
        "HOUSTONOTH" => "Houston - Other",
        "GLYNNOTH" => "Glynn - Other",
        "GRADYOTH" => "Grady - Other",
        "OTHERGRE" => "Greene - Other",
        "OTHERJAS" => "Jasper - Other",
        "HARRISOTH" => "Harris - Other",
        "IRWINOTH" => "Irwin - Other",
        "OTHERHAN" => "Hancock - Other",
        "HENOT" => "Henry - Other",
        "OTHERHAR" => "Haralson - Other",
        "OTHERHEA" => "Heard - Other",
        "LAMAROTH" => "Lamar - Other",
        "LAURENSOTH" => "Laurens - Other",
        "OTHERJAC" => "Jackson - Other",
        "JEFFDAVISOTH" => "Jeff Davis - Other",
        "JEFFERSONOTH" => "Jefferson - Other",
        "JENKINSOTH" => "Jenkins - Other",
        "JOHNSONOTH" => "Johnson - Other",
        "JONESOTH" => "Jones - Other",
        "LANIEROTH" => "Lanier - Other",
        "MCDUFFIEOTH" => "Mcduffie - Other",
        "LEEOTH" => "Lee - Other",
        "LIBERTYOTH" => "Liberty - Other",
        "LINCOLNOTH" => "Lincoln - Other",
        "MARIONOTH" => "Marion - Other",
        "LONGOTH" => "Long - Other",
        "MERIWETHEROTH" => "Meriwether - Other",
        "LOWNDESOTH" => "Lowndes - Other",
        "MACONOTH" => "Macon - Other",
        "LUMOT" => "Lumpkin - Other",
        "MAINEOTH" => "Maine - Other",
        "OTHERMAD" => "Madison - Other",
        "MONTANAOTH" => "Montana - Other",
        "MCINTOSHOTH" => "Mcintosh - Other",
        "OTHERMOR" => "Morgan - Other",
        "MITCHELLOTH" => "Mitchell - Other",
        "MILLEROTH" => "Miller - Other",
        "MONROEOTH" => "Monroe - Other",
        "PAUOT" => "Paulding - Other",
        "MONTGOMERYOTH" => "Montgomery - Other",
        "NEWOT" => "Newton - Other",
        "OTHERMUR" => "Murray - Other",
        "OTHERPUT" => "Putnam - Other",
        "MUSCOGEEOTH" => "Muscogee - Other",
        "OTHERPOL" => "Polk - Other",
        "OTHEROCO" => "Oconee - Other",
        "OTHEROGL" => "Oglethorpe - Other",
        "PEACHOTH" => "Peach - Other",
        "OTHERPIC" => "Pickens - Other",
        "PIERCEOTH" => "Pierce - Other",
        "PIKEOTH" => "Pike - Other",
        "PULASKIOTH" => "Pulaski - Other",
        "SCREVENOTH" => "Screven - Other",
        "QUITMANOTH" => "Quitman - Other",
        "SEMINOLEOTH" => "Seminole - Other",
        "OTHERRAB" => "Rabun - Other",
        "RANDOLPHOTH" => "Randolph - Other",
        "RICHMONDOTH" => "Richmond - Other",
        "STEWARTOTH" => "Stewart - Other",
        "SCHLEYOTH" => "Schley - Other",
        "OTHERROC" => "Rockdale - Other",
        "OTHERSPA" => "Spalding - Other",
        "OTHERSTE" => "Stephens - Other",
        "SUMTEROTH" => "Sumter - Other",
        "TALBOTOTH" => "Talbot - Other",
        "THOMASOTH" => "Thomas - Other",
        "TALIAFERROOTH" => "Taliaferro - Other",
        "TATTNALLOTH" => "Tattnall - Other",
        "TAYLOROTH" => "Taylor - Other",
        "TURNEROTH" => "Turner - Other",
        "TELFAIROTH" => "Telfair - Other",
        "TERRELLOTH" => "Terrell - Other",
        "TIFTOTH" => "Tift - Other",
        "OTHERWAL" => "Walker - Other",
        "TOOMBSOTH" => "Toombs - Other",
        "TOWNSOTH" => "Towns - Other",
        "TREUTLENOTH" => "Treutlen - Other",
        "TROUPOTH" => "Troup - Other",
        "WALOT" => "Walton - Other",
        "TWIGGSOTH" => "Twiggs - Other",
        "UNIONOTH" => "Union - Other",
        "UPSONOTH" => "Upson - Other",
        "WEBSTEROTH" => "Webster - Other",
        "WAREOTH" => "Ware - Other",
        "WAYNEOTH" => "Wayne - Other",
        "WARRENOTH" => "Warren - Other",
        "WASHINGTONOTH" => "Washington - Other",
        "WILKINSONOTH" => "Wilkinson - Other",
        "WHEELEROTH" => "Wheeler - Other",
        "OTHERWHI" => "White - Other",
        "WILKESOTH" => "Wilkes - Other",
        "OTHERWHT" => "Whitfield - Other",
        "WILCOXOTH" => "Wilcox - Other",
        "WORTHOTH" => "Worth - Other",
        "OTHERALABAMA" => "Alabama - Other",
        "ALASKAOTH" => "Alaska - Other",
        "ARIZONAOTH" => "Arizona - Other",
        "ARKANSASOTH" => "Arkansas - Other",
        "CALIFORNIAOTH" => "California - Other",
        "CONNECTICUTOTH" => "Connecticut - Other",
        "COLORADOOTH" => "Colorado - Other",
        "DELAWAREOTH" => "Delaware - Other",
        "DOCOTH" => "District Of Columbia - Other",
        "OTHERFLORIDA" => "Florida - Other",
        "HAWAIIOTH" => "Hawaii - Other",
        "ILLINOISOTH" => "Illinois - Other",
        "IDAHOOTH" => "Idaho - Other",
        "IOWAOTH" => "Iowa - Other",
        "INDIANAOTH" => "Indiana - Other",
        "KANSASOTH" => "Kansas - Other",
        "KENTUCKYOTH" => "Kentucky - Other",
        "LOUISIANAOTH" => "Louisiana - Other",
        "MASSACHUSETTSOTH" => "Massachusetts - Other",
        "MARYLANDOTH" => "Maryland - Other",
        "MICHIGANOTH" => "Michigan - Other",
        "MINNESOTAOTH" => "Minnesota - Other",
        "MISSISSIPPIOTH" => "Mississippi - Other",
        "NEBRASKAOTH" => "Nebraska - Other",
        "MISSOURIOTH" => "Missouri - Other",
        "NEVADAOTH" => "Nevada - Other",
        "NEWHAMPSHIREOTH" => "New Hampshire - Other",
        "NEWJERSEYOTH" => "New Jersey - Other",
        "NEWYORKOTH" => "New York - Other",
        "NEWMEXICOOTH" => "New Mexico - Other",
        "OTHERNORTHCAROLINA" => "North Carolina - Other",
        "OHIOOTH" => "Ohio - Other",
        "NDAKOTAOTH" => "North Dakota - Other",
        "OKLAHOMAOTH" => "Oklahoma - Other",
        "OREGONOTH" => "Oregon - Other",
        "PENNSYLVANIAOTH" => "Pennsylvania - Other",
        "RHODEISLANOTH" => "Rhode Island - Other",
        "SDAKOTAOTH" => "South Dakota - Other",
        "OTHERSOUTHCAROLINA" => "South Carolina - Other",
        "TEXASOTH" => "Texas - Other",
        "OTHERTENNESSEE" => "Tennessee - Other",
        "VERMONTOTH" => "Vermont - Other",
        "VIRGINIAOTH" => "Virginia - Other",
        "UTAHOTH" => "Utah - Other",
        "WASHINGOTH" => "Washington - Other",
        "WISCONSINOTH" => "Wisconsin - Other",
        "WYOMINGOTH" => "Wyoming - Other",
        "WVIRGINIAOTH" => "West Virginia - Other",
        "OTHER" => "Other",
        "OTHEROT" => "Other",
        "OTHEROTH" => "Other"
     },
     :mls_high_school => {
        "9THGRADE" => "9Th Grade Academy",
        "RICHACAD" => "Academy Of Richmond County",
        "ADAIRSVILL" => "Adairsville",
        "ALBANY" => "Albany",
        "ALCOVY" => "Alcovy",
        "ALEXANDER" => "Alexander",
        "ALLATOONA" => "Allatoona",
        "ALPHARETTA" => "Alpharetta",
        "AMERICUS" => "Americus Sumter High North",
        "SUMTERCO" => "Americus Sumter High South",
        "APALACHEE" => "Apalachee",
        "APPLINGC" => "Appling County",
        "ARABIA" => "Arabia Mountain",
        "ARCHER" => "Archer",
        "ARMUCHEE" => "Armuchee",
        "ATKINSON" => "Atkinson County",
        "BACONCOU" => "Bacon County",
        "BAINBRIDG" => "Bainbridge - Decatur",
        "BAKER" => "Baker County",
        "BALDWIN" => "Baldwin",
        "BANKSCOU" => "Banks County",
        "BEBANNEK" => "Banneker",
        "BEACH" => "Beach",
        "BERKMAR" => "Berkmar",
        "BERRIEN" => "Berrien",
        "BISHOPHALL" => "Bishop Hall (9-12)",
        "BLECKLEY" => "Bleckley County",
        "BOWDON" => "Bowdon",
        "BRADWELL" => "Bradwell Institute",
        "BRANTLEY" => "Brantley County",
        "BREMEN" => "Bremen",
        "BROOKSCO" => "Brooks County",
        "BROOKWOOD" => "Brookwood",
        "BRUNSWIC" => "Brunswick",
        "BRYANCOU" => "Bryan County",
        "BUFORD" => "Buford",
        "BURKECOU" => "Burke County",
        "BUTLER" => "Butler",
        "CAIRO" => "Cairo",
        "CALHOUN" => "Calhoun",
        "CALHOUNC" => "Calhoun County",
        "CALLAWAY" => "Callaway",
        "CAMBRIDGE" => "Cambridge",
        "CAMDENCO" => "Camden County",
        "CAMPBELL" => "Campbell",
        "CARROLLT" => "Carrollton",
        "CARTERH" => "Cartersville",
        "CARVE" => "Carver - Fulton",
        "CARVER" => "Carver - Muscogee",
        "CCHSNINTHG" => "Cchs Ninth Grade",
        "CDRGROVE" => "Cedar Grove",
        "CEDARSHO" => "Cedar Shoals",
        "CEDARTOW" => "Cedartown",
        "CENTENNIAL" => "Centennial",
        "CENTRALBIBB" => "Central - Bibb",
        "CENTRAL" => "Central - Carroll",
        "CENTRALGWI" => "Central Gwinnett",
        "CHAMBLEE" => "Chamblee",
        "CHAPELHILL" => "Chapel Hill",
        "DREWC" => "Charles R. Drew",
        "CHARLTON" => "Charlton County",
        "CHATTAHOOC" => "Chattahoochee",
        "CHATTAHOOCHEECOU" => "Chattahoochee County",
        "CHATTOOG" => "Chattooga",
        "CHEROKEE" => "Cherokee",
        "CHESTATEE" => "Chestatee",
        "CLARKECE" => "Clarke Central",
        "CLARKSTON" => "Clarkston",
        "CLAXTON" => "Claxton",
        "CLINCHCO" => "Clinch County",
        "COAHULLA" => "Coahulla Creek",
        "COFFEECO" => "Coffee",
        "COLLINSHIL" => "Collins Hill",
        "COLQUITT" => "Colquitt County",
        "COLUMBIAH" => "Columbia",
        "COLUMBUS" => "Columbus",
        "COMMERCE" => "Commerce",
        "COOKCOUN" => "Cook County",
        "COOSA" => "Coosa",
        "CRAWFORD" => "Crawford County",
        "CREEKSIDE" => "Creekside",
        "CREEKVIEWC" => "Creekview",
        "CRIM" => "Crim",
        "CRISPCOU" => "Crisp County",
        "CROSSCRE" => "Cross Creek",
        "CROSSKEYS" => "Cross Keys",
        "CRSSROADS" => "Crossroads",
        "CROSSROAD" => "Crossroads - Cherokee",
        "CROSSROADS" => "Crossroads Academy",
        "DACULA" => "Dacula",
        "DADECOUN" => "Dade County",
        "DALTON" => "Dalton",
        "DAVIDSON" => "Davidson Magnet(6-12)",
        "DAWSON" => "Dawson County",
        "DECATUR" => "Decatur",
        "DEKALBSCHO" => "Dekalb School Of The Arts",
        "DODGECOU" => "Dodge County",
        "DOOLYCOU" => "Dooly County",
        "DOUGHERT" => "Dougherty Comprehensive",
        "DOUGLAS" => "Douglas County",
        "DOUGLASS" => "Douglass",
        "DRUIDHLLS" => "Druid Hills",
        "DUBLIN" => "Dublin",
        "DULUTH" => "Duluth",
        "DUNWOODY" => "Dunwoody",
        "DUTCHTOWN" => "Dutchtown",
        "EAGLESLAN" => "Eagles Landing",
        "EARLYCOU" => "Early County",
        "ECOWETA" => "East Coweta",
        "EASTHALL" => "East Hall",
        "EASTJACKSON" => "East Jackson",
        "EASTLAUR" => "East Laurens",
        "EASTPAULD" => "East Paulding",
        "EASTSIDE" => "Eastside",
        "ECHOLSCO" => "Echols County",
        "EFFINGHA" => "Effingham County",
        "ELBERTCO" => "Elbert County",
        "ELANDREWS" => "Elizabeth Andrews",
        "EMANUELC" => "Emanuel County Institute (6-12)",
        "ETOWAH" => "Etowah",
        "EVANS" => "Evans",
        "FANNINCO" => "Fannin County",
        "FAYETTECO" => "Fayette County",
        "FITZGERA" => "Fitzgerald",
        "FLOWERYBR" => "Flowery Branch",
        "FORESTPRK" => "Forest Park",
        "FORSYTHCE" => "Forsyth Central",
        "FRANKLIN" => "Franklin County",
        "GAINESVILL" => "Gainesville",
        "GILMER" => "Gilmer",
        "GLASCOCK" => "Glascock County",
        "GLENNHIL" => "Glenn Hills",
        "GLYNNACA" => "Glynn Academy",
        "GORDONCENTRAL" => "Gordon Central",
        "GORDONLEE" => "Gordon Lee",
        "GORDONSTRE" => "Gordon Street",
        "GRADY" => "Grady",
        "GRAYSON" => "Grayson",
        "GREENBRI" => "Greenbrier",
        "GREENECO" => "Greene County",
        "GREENVIL" => "Greenville",
        "GRIFFIN" => "Griffin",
        "GROVES" => "Groves",
        "GROVETOWN" => "Grovetown",
        "HABERSHA" => "Habersham Central",
        "HANCOCKC" => "Hancock Central",
        "HARALSONCO" => "Haralson County",
        "HARDAWAY" => "Hardaway",
        "HARLEM" => "Harlem",
        "HARRISCO" => "Harris County",
        "HARRISON" => "Harrison",
        "HARTCOUN" => "Hart County",
        "HAVENACADE" => "Haven Academy",
        "HAWKINSV" => "Hawkinsville",
        "HEARDCOU" => "Heard County",
        "HENRY" => "Henry County",
        "HEPHZIBA" => "Hephzibah",
        "HERITAGEC" => "Heritage - Catoosa",
        "HERITAGE" => "Heritage - Rockdale",
        "HILLGROVE" => "Hillgrove",
        "HIRAMP" => "Hiram",
        "HOUSTONC" => "Houston County",
        "HOUSTONTECH" => "Houston County Career & Technology Ctr (9-12)",
        "HOWARD" => "Howard",
        "INDEPEND" => "Independence",
        "IRWINCOU" => "Irwin County",
        "ISLANDS" => "Islands",
        "JACKSON" => "Jackson",
        "JACKSONCOUNTY" => "Jackson County",
        "JASPERCO" => "Jasper County",
        "JEFFDAVI" => "Jeff Davis",
        "JEFFERSO" => "Jefferson",
        "JEFFCOUNTY" => "Jefferson County",
        "JENKINS" => "Jenkins",
        "JENKINSC" => "Jenkins County",
        "JOHNSCREEK" => "Johns Creek",
        "JOHNCHAT" => "Johnson - Chatham",
        "JOHNSON" => "Johnson - Hall",
        "JOHNSONC" => "Johnson County",
        "JOHNSONM" => "Johnson Magnet (9-12)",
        "JONESCOU" => "Jones County",
        "JONESBORO" => "Jonesboro",
        "JORDANVO" => "Jordan Vocational",
        "JOSEY" => "Josey",
        "KELL" => "Kell",
        "KENDRICK" => "Kendrick",
        "KENNESAW" => "Kennesaw Mountain",
        "LAFAYETTE" => "Lafayette",
        "LAGRANGE" => "Lagrange",
        "LAKESIDEC" => "Lakeside - Columbia",
        "LAKESIDE" => "Lakeside - Dekalb",
        "LAKEVIEW" => "Lakeview-Fort Oglethorpe",
        "LAMARCOU" => "Lamar County",
        "LAMBERT" => "Lambert",
        "LANEY" => "Laney",
        "LANGSTONHUGHES" => "Langston Hughes",
        "LANIER" => "Lanier",
        "LANIERCO" => "Lanier County",
        "LASSITER" => "Lassiter",
        "LEECOUNT" => "Lee County",
        "LIBERTYC" => "Liberty County",
        "LINCOLNC" => "Lincoln County",
        "LITHASPGS" => "Lithia Springs",
        "LITHONIA" => "Lithonia",
        "LOCUSTGRV" => "Locust Grove",
        "LOGANVILLE" => "Loganville",
        "LONGCOUN" => "Long County",
        "LOVEJOY" => "Lovejoy",
        "LOW" => "Low",
        "LOWNDES" => "Lowndes",
        "LUELLA" => "Luella",
        "LUMPKIN" => "Lumpkin County",
        "MACONCOU" => "Macon County",
        "MADISONC" => "Madison County",
        "MANCHEST" => "Manchester",
        "MARGARETHARRIS" => "Margaret Harris",
        "MARIETTA" => "Marietta",
        "MARIONCOUNTY" => "Marion County",
        "MARTINLUTHERKINGJR" => "Martin Luther King Jr",
        "MARYPERS" => "Mary Persons",
        "SOUTHSIDE" => "Maynard H. Jackson, Jr.",
        "MAYS" => "Mays",
        "MCCLARIN" => "Mcclarin",
        "MCEACHERN" => "Mceachern",
        "MCINTOSH" => "Mcintosh",
        "MCINTOSHCNT" => "Mcintosh County Academy",
        "MCNAIR" => "Mcnair",
        "MEADOWCRK" => "Meadowcreek",
        "METTER" => "Metter",
        "MILLCREEK" => "Mill Creek",
        "MILLERCO" => "Miller County",
        "MILLERGROVE" => "Miller Grove High",
        "MILTON" => "Milton",
        "MITCHELL" => "Mitchell County",
        "MODEL" => "Model",
        "MONRO" => "Monroe - Dougherty",
        "MONROE" => "Monroe Area",
        "MONTGOME" => "Montgomery County",
        "MORGANCO" => "Morgan County",
        "MORROW" => "Morrow",
        "MTZIONCL" => "Mount Zion - Clayton",
        "MOUNTAIN" => "Mountain Education Center (9-12)",
        "MOUNTAINVIEW" => "Mountain View",
        "MTZIONCR" => "Mt. Zion - Carroll",
        "MUNDYSMILL" => "Mundys Mill",
        "MURRAYCO" => "Murray County",
        "CASS" => "New Cass",
        "NEWHAMSTEAD" => "New Hamstead",
        "NEWMANCHESTER" => "New Manchester",
        "SEBULL" => "New Southeast Bulloch",
        "NEWNAN" => "Newnan",
        "NEWTON" => "Newton",
        "NORCROSS" => "Norcross",
        "NORTHATLA" => "North Atlanta",
        "NORTHCLTN" => "North Clayton",
        "NORTHCOBB" => "North Cobb",
        "FORSYTHNO" => "North Forsyth",
        "NORTHGWIN" => "North Gwinnett",
        "NORTHHALL" => "North Hall",
        "NORTHMUR" => "North Murray",
        "NORTHOCONEE" => "North Oconee",
        "NORTHPAULDING" => "North Paulding",
        "NORTHSPGS" => "North Springs",
        "NEBIBB" => "Northeast",
        "NETIFT" => "Northeast Campus Tift County",
        "NORTHGATE" => "Northgate",
        "NORTHSIDH" => "Northside - Houston",
        "NORTHSIDM" => "Northside - Muscogee",
        "NORTHVIEW" => "Northview",
        "NORTHWESTW" => "Northwest Whitfield County",
        "OAKWOOD" => "Oakwood",
        "OCONEECOUN" => "Oconee County",
        "OGLETHOR" => "Oglethorpe County",
        "OLA" => "Ola",
        "OSBORNE" => "Osborne",
        "PARKVIEW" => "Parkview",
        "PATHENRY" => "Patrick Henry",
        "PAULDING" => "Paulding County",
        "PEACHCOU" => "Peach County",
        "PEACHTREERIDGE" => "Peachtree Ridge",
        "PEBBLEBRK" => "Pebblebrook",
        "PELHAM" => "Pelham",
        "PEPPERELL" => "Pepperell",
        "PERRY" => "Perry",
        "PHOENIX" => "Phoenix - Gwinnett",
        "PHOENI" => "Phoenix - Whitfield",
        "PICKENS" => "Pickens",
        "PIERCECO" => "Pierce County",
        "PIKECOUN" => "Pike County",
        "POPE" => "Pope",
        "PORTAL" => "Portal",
        "PUTNAMCO" => "Putnam County",
        "QUITMANCO" => "Quitman County",
        "RABUNCOU" => "Rabun County",
        "RANDOLPH" => "Randolph Clay",
        "REDANH" => "Redan",
        "RICHMOND" => "Richmond Hill",
        "RIDGELAND" => "Ridgeland",
        "RINGGOLD" => "Ringgold",
        "RIVERRIDGE" => "River Ridge",
        "RIVERDALE" => "Riverdale",
        "RIVERWOOD" => "Riverwood",
        "ROCKDALE" => "Rockdale County",
        "ROCKMART" => "Rockmart",
        "ROME" => "Rome",
        "ROSWELL" => "Roswell",
        "RUTLAND" => "Rutland",
        "SALEM" => "Salem",
        "SANDYCREE" => "Sandy Creek",
        "SAVANNAH" => "Savannah",
        "SAVARTS" => "Savannah Arts Academy (9-12)",
        "SCHLEY" => "Schley",
        "SCREVENC" => "Screven County",
        "SEMINOLE" => "Seminole County",
        "SEQUOYAH" => "Sequoyah",
        "SHAW" => "Shaw",
        "SHILOH" => "Shiloh",
        "SOCIALCIR" => "Social Circle",
        "SONORAVILLE" => "Sonoraville",
        "SOUTHATL" => "South Atlanta",
        "SOUTHCOBB" => "South Cobb",
        "SOUTHEFF" => "South Effingham",
        "FORSYTHSO" => "South Forsyth",
        "SOUTHGWIN" => "South Gwinnett",
        "SPAULDING" => "South Paulding",
        "SOUTHEASTW" => "Southeast Whitfield County",
        "SWBIBB" => "Southwest",
        "STHWSTDKB" => "Southwest Dekalb",
        "SPALDING" => "Spalding",
        "SPENCER" => "Spencer",
        "SPRAYBERRY" => "Sprayberry",
        "STARSMILL" => "Starrs Mill",
        "STATESBO" => "Statesboro",
        "STEPHENS" => "Stephens County",
        "STEPHENSON" => "Stephenson",
        "STEWQUIT" => "Stewart County",
        "STOCKBRIDG" => "Stockbridge",
        "STONEMTH" => "Stone Mountain",
        "SUMMERVILLE" => "Summerville",
        "SWAINSBO" => "Swainsboro",
        "CENTRALH" => "Talbot County",
        "TALIAFER" => "Taliaferro County",
        "TATTNALL" => "Tattnall County",
        "TAYLORCO" => "Taylor County",
        "TECHHIGH" => "Tech",
        "TELFAIRC" => "Telfair County",
        "TEMPLE" => "Temple",
        "TERRELLC" => "Terrell County",
        "THERRELL" => "Therrell",
        "THOMASCO" => "Thomas County Central",
        "THOMASVILL" => "Thomasville",
        "THOMSON" => "Thomson",
        "TIFTCOUN" => "Tift County",
        "TOOMBSCO" => "Toombs County",
        "TOWERS" => "Towers",
        "TOWNSCOU" => "Towns County",
        "TREUTLEN" => "Treutlen",
        "TRICITIESTRI" => "Tri-Cities",
        "TRICOUN" => "Tri-County",
        "TRION" => "Trion",
        "TROUPCOU" => "Troup County",
        "TUCKER" => "Tucker",
        "TURNERCO" => "Turner County",
        "TURNERSPEC" => "Turner County Specialty(Pk-12)",
        "TWIGGSCO" => "Twiggs County",
        "UNIONCOU" => "Union County",
        "UNIONGRV" => "Union Grove",
        "UPSONLE" => "Upson-Lee",
        "VALDOSTA" => "Valdosta",
        "VETERANS" => "Veterans",
        "VIDALIACOM" => "Vidalia Comprehensive",
        "VILLARICA" => "Villa Rica",
        "WALNUTGROVE" => "Walnut Grove",
        "WALTON" => "Walton",
        "WAM" => "Wam",
        "WARECOUN" => "Ware County",
        "WAREMAGN" => "Ware Magnet(Kk-12)",
        "WARNERRO" => "Warner Robins",
        "WARRENCO" => "Warren County",
        "WASHINGTON" => "Washington",
        "WASHCO" => "Washington County",
        "WASHWILK" => "Washington-Wilkes",
        "WAYNECOU" => "Wayne County",
        "WAYNENIGHT" => "Wayne County  Night",
        "WEBSTERCO" => "Webster County",
        "WESTFORSYTH" => "West Forsyth",
        "WESTHALL" => "West Hall",
        "WESTLAUR" => "West Laurens",
        "WESTLAKE" => "Westlake",
        "WESTMINI" => "Westminister Christian Academy (K-12)",
        "WESTOVER" => "Westover",
        "WBIBB" => "Westside - Bibb",
        "WESTSIDE" => "Westside - Richmond",
        "WHEELER" => "Wheeler",
        "WHEELERC" => "Wheeler County",
        "WHITE" => "White County",
        "WHITEWATER" => "Whitewater",
        "WILCOX" => "Wilcox County",
        "WILKINSO" => "Wilkinson County",
        "WILLHUTCH" => "William S. Hutchings Career Ctr",
        "WINDERBAR" => "Winder Barrow",
        "WINDSORF" => "Windsor Forest",
        "WOODSMILL" => "Wood's Mill Academy",
        "WOODLANDB" => "Woodland - Bartow",
        "WOODLANDH" => "Woodland - Henry",
        "WOODSTOCK" => "Woodstock",
        "WVTHOMPKINS" => "Woodville-Thompkins",
        "WOODYGAP" => "Woody Gap",
        "APPLINGOTH" => "Appling - Other",
        "ATKINSONOTH" => "Atkinson - Other",
        "WORTHCOU" => "Worth County",
        "BACONOTH" => "Bacon - Other",
        "BAKEROTH" => "Baker - Other",
        "BALDWINOTH" => "Baldwin - Other",
        "OTHERBAN" => "Banks - Other",
        "BRWOT" => "Barrow - Other",
        "BAROT" => "Bartow - Other",
        "BENHILLOTH" => "Ben Hill - Other",
        "BERRIENOTH" => "Berrien - Other",
        "BIBBOTH" => "Bibb - Other",
        "BLECKLEYOTH" => "Bleckley - Other",
        "BRANTLEYOTH" => "Brantley - Other",
        "BROOKSOTH" => "Brooks - Other",
        "BRYANOTH" => "Bryan - Other",
        "BULLOCHOTH" => "Bulloch - Other",
        "OTHERBT" => "Butts - Other",
        "CALHOUNOTH" => "Calhoun - Other",
        "BURKEOTH" => "Burke - Other",
        "CAMDENOTH" => "Camden - Other",
        "CANDLEROTH" => "Candler - Other",
        "OTHERCAR" => "Carroll - Other",
        "OTHERCAT" => "Catoosa - Other",
        "CHARLTONOTH" => "Charlton - Other",
        "CHATHAMOTH" => "Chatham - Other",
        "CHATTAHOOCHEEOTH" => "Chattahoochee - Other",
        "OTHERCHA" => "Chattooga - Other",
        "CHEOT" => "Cherokee - Other",
        "OTHERCLA" => "Clarke - Other",
        "CLAYOTH" => "Clay - Other",
        "CLAOT" => "Clayton - Other",
        "CLINCHOTH" => "Clinch - Other",
        "COBOT" => "Cobb - Other",
        "COFFEEOTH" => "Coffee - Other",
        "COLQUITTOTH" => "Colquitt - Other",
        "COLUMBIAOTH" => "Columbia - Other",
        "COOKOTH" => "Cook - Other",
        "COWOT" => "Coweta - Other",
        "CRAWFORDOTH" => "Crawford - Other",
        "CRISPOTH" => "Crisp - Other",
        "OTHERDAD" => "Dade - Other",
        "DAWOT" => "Dawson - Other",
        "DECATUROTH" => "Decatur - Other",
        "DEKOT" => "Dekalb - Other",
        "DODGEOTH" => "Dodge - Other",
        "DOOLYOTH" => "Dooly - Other",
        "DOUGHERTYOTH" => "Dougherty - Other",
        "DOUOT" => "Douglas - Other",
        "EARLYOTH" => "Early - Other",
        "ECHOLSOTH" => "Echols - Other",
        "EFFINGHAMOTH" => "Effingham - Other",
        "ELBERTOTH" => "Elbert - Other",
        "EMANUELOTH" => "Emanuel - Other",
        "EVANSOTH" => "Evans - Other",
        "FANNINOTH" => "Fannin - Other",
        "FAYOT" => "Fayette - Other",
        "FLORIDA" => "Florida - Other",
        "OTHERFLO" => "Floyd - Other",
        "OTHERFRA" => "Franklin - Other",
        "FOROT" => "Forsyth - Other",
        "FULOT" => "Fulton - Other",
        "GLASCOCKOTH" => "Glascock - Other",
        "GLYNNOTH" => "Glynn - Other",
        "OTHERGIL" => "Gilmer - Other",
        "OTHERGOR" => "Gordon - Other",
        "GRADYOTH" => "Grady - Other",
        "OTHERGRE" => "Greene - Other",
        "GWIOT" => "Gwinnett - Other",
        "OTHERHAB" => "Habersham - Other",
        "HALOT" => "Hall - Other",
        "OTHERHAN" => "Hancock - Other",
        "OTHERHAR" => "Haralson - Other",
        "HARRISOTH" => "Harris - Other",
        "HARTOTH" => "Hart - Other",
        "OTHERHEA" => "Heard - Other",
        "HENOT" => "Henry - Other",
        "HOUSTONOTH" => "Houston - Other",
        "IRWINOTH" => "Irwin - Other",
        "JACKOTHER" => "Jackson - Other",
        "OTHERJAC" => "Jackson - Other",
        "OTHERJAS" => "Jasper - Other",
        "JEFFDAVISOTH" => "Jeff Davis - Other",
        "JEFFERSONOTH" => "Jefferson - Other",
        "JENKINSOTH" => "Jenkins - Other",
        "JOHNSONOTH" => "Johnson - Other",
        "JONESOTH" => "Jones - Other",
        "LAMAROTH" => "Lamar - Other",
        "LAURENSOTH" => "Laurens - Other",
        "LANIEROTH" => "Lanier - Other",
        "LEEOTH" => "Lee - Other",
        "LIBERTYOTH" => "Liberty - Other",
        "LINCOLNOTH" => "Lincoln - Other",
        "LONGOTH" => "Long - Other",
        "LOWNDESOTH" => "Lowndes - Other",
        "OTHERLUM" => "Lumpkin - Other",
        "MACONOTH" => "Macon - Other",
        "OTHERMAD" => "Madison - Other",
        "MARIONOTH" => "Marion - Other",
        "MCDUFFIEOTH" => "Mcduffie - Other",
        "MCINTOSHOTH" => "Mcintosh - Other",
        "MERIWETHEROTH" => "Meriwether - Other",
        "MILLEROTH" => "Miller - Other",
        "MITCHELLOTH" => "Mitchell - Other",
        "MONROEOTH" => "Monroe - Other",
        "MONTGOMERYOTH" => "Montgomery - Other",
        "OTHERMOR" => "Morgan - Other",
        "OTHERMUR" => "Murray - Other",
        "MUSCOGEEOTH" => "Muscogee - Other",
        "NEWOT" => "Newton - Other",
        "OTHEROCO" => "Oconee - Other",
        "OTHEROGL" => "Oglethorpe - Other",
        "OTHER" => "Other",
        "PAUOT" => "Paulding - Other",
        "OHIOOTH" => "Ohio - Other",
        "PEACHOTH" => "Peach - Other",
        "OTHERPIC" => "Pickens - Other",
        "PICKOTHER" => "Pickens - Other",
        "PIERCEOTH" => "Pierce - Other",
        "PIKEOTH" => "Pike - Other",
        "OTHERPOL" => "Polk - Other",
        "PULASKIOTH" => "Pulaski - Other",
        "OTHERPUT" => "Putnam - Other",
        "QUITMANOTH" => "Quitman - Other",
        "OTHERRAB" => "Rabun - Other",
        "RANDOLPHOTH" => "Randolph - Other",
        "RICHMONDOTH" => "Richmond - Other",
        "OTHERROC" => "Rockdale - Other",
        "SCHLEYOTH" => "Schley - Other",
        "SCREVENOTH" => "Screven - Other",
        "SEMINOLEOTH" => "Seminole - Other",
        "GRIOT" => "Spalding - Other",
        "OTHERSTE" => "Stephens - Other",
        "STEWARTOTH" => "Stewart - Other",
        "SUMTEROTH" => "Sumter - Other",
        "TALBOTOTH" => "Talbot - Other",
        "TALIAFERROOTH" => "Taliaferro - Other",
        "TATTNALLOTH" => "Tattnall - Other",
        "TAYLOROTH" => "Taylor - Other",
        "TELFAIROTH" => "Telfair - Other",
        "TERRELLOTH" => "Terrell - Other",
        "THOMASOTH" => "Thomas - Other",
        "TOOMBSOTH" => "Toombs - Other",
        "TOWNSOTH" => "Towns - Other",
        "TIFTOTH" => "Tift - Other",
        "TREUTLENOTH" => "Treutlen - Other",
        "TROUPOTH" => "Troup - Other",
        "TURNEROTH" => "Turner - Other",
        "TWIGGSOTH" => "Twiggs - Other",
        "UNIONOTH" => "Union - Other",
        "UTAHOTH" => "Utah - Other",
        "UPSONOTH" => "Upson - Other",
        "OTHERWAL" => "Walker - Other",
        "OTHERWLT" => "Walton - Other",
        "WAREOTH" => "Ware - Other",
        "WARRENOTH" => "Warren - Other",
        "WASHINGTONOTH" => "Washington - Other",
        "WAYNEOTH" => "Wayne - Other",
        "WASHINGOTH" => "Washington - Other",
        "WEBSTEROTH" => "Webster - Other",
        "WHEELEROTH" => "Wheeler - Other",
        "OTHERWHI" => "White - Other",
        "OTHERWHT" => "Whitfield - Other",
        "WILCOXOTH" => "Wilcox - Other",
        "WILKESOTH" => "Wilkes - Other",
        "WILKINSONOTH" => "Wilkinson - Other",
        "WORTHOTH" => "Worth - Other",
        "ALABAMA" => "Alabama - Other",
        "ALASKAOTH" => "Alaska - Other",
        "ARIZONAOTH" => "Arizona - Other",
        "ARKANSASOTH" => "Arkansas - Other",
        "COLORADOOTH" => "Colorado - Other",
        "CALIFORNIAOTH" => "California - Other",
        "CONNECTICUTOTH" => "Connecticut - Other",
        "DELAWAREOTH" => "Delaware - Other",
        "DOCOTH" => "District Of Columbia - Other",
        "HAWAIIOTH" => "Hawaii - Other",
        "IDAHOOTH" => "Idaho - Other",
        "ILLINOISOTH" => "Illinois - Other",
        "INDIANAOTH" => "Indiana - Other",
        "IOWAOTH" => "Iowa - Other",
        "KANSASOTH" => "Kansas - Other",
        "KENTUCKYOTH" => "Kentucky - Other",
        "LOUISIANAOTH" => "Louisiana - Other",
        "MAINEOTH" => "Maine - Other",
        "MARYLANDOTH" => "Maryland - Other",
        "MASSACHUSETTSOTH" => "Massachusetts - Other",
        "MICHIGANOTH" => "Michigan - Other",
        "MINNESOTAOTH" => "Minnesota - Other",
        "MISSISSIPPIOTH" => "Mississippi - Other",
        "MISSOURIOTH" => "Missouri - Other",
        "MONTANAOTH" => "Montana - Other",
        "NEBRASKAOTH" => "Nebraska - Other",
        "NEVADAOTH" => "Nevada - Other",
        "NEWHAMPSHIREOTH" => "New Hampshire - Other",
        "NEWJERSEYOTH" => "New Jersey - Other",
        "NEWMEXICOOTH" => "New Mexico - Other",
        "NEWYORKOTH" => "New York - Other",
        "NORTHCAROLINA" => "North Carolina - Other",
        "NDAKOTAOTH" => "North Dakota - Other",
        "OKLAHOMAOTH" => "Oklahoma - Other",
        "OREGONOTH" => "Oregon - Other",
        "PENNSYLVANIAOTH" => "Pennsylvania - Other",
        "RHODEISLANOTH" => "Rhode Island - Other",
        "SOUTHCAROLINA" => "South Carolina - Other",
        "TENNESSEE" => "Tennessee - Other",
        "SDAKOTAOTH" => "South Dakota - Other",
        "TEXASOTH" => "Texas - Other",
        "VERMONTOTH" => "Vermont - Other",
        "VIRGINIAOTH" => "Virginia - Other",
        "WVIRGINIAOTH" => "West Virginia - Other",
        "WISCONSINOTH" => "Wisconsin - Other",
        "WYOMINGOTH" => "Wyoming - Other",
        "OTHE" => "Other",
        "OTHEROTH" => "Other"
     },
     :mls_middle_school => {
        "KELSEYAVE" => "A.Z. Kelsey",
        "ADADAIRSVILL" => "Adairsville",
        "ADAMSON" => "Adamson",
        "ADRIANOF" => "Adrianof Performing Arts (Pk-08",
        "ALBANY" => "Albany",
        "CREWS" => "Alton C. Crews",
        "ANITAWHI" => "Anita White Carson",
        "APPLINGB" => "Appling",
        "APPLINGC" => "Appling County",
        "ARMUCHEE" => "Armuchee",
        "ELLISARNAL" => "Arnall",
        "ARNOLD" => "Arnold",
        "ARTHURWI" => "Arthur Williams",
        "ASHWORTH" => "Ashworth",
        "ATKINSONCO" => "Atkinson County",
        "ATLCHARTER" => "Atlanta Charter",
        "AUSTINROADM" => "Austin Road",
        "AUTREYMILL" => "Autrey Mill",
        "AWTREY" => "Awtrey",
        "BESTACADEMY" => "B.E.S.T. Academy",
        "BABB" => "Babb",
        "BACONCOU" => "Bacon County",
        "BACONTON" => "Baconton Community Charter(Pk -",
        "WESTBAINBR" => "Bainbridge",
        "BAKER" => "Baker",
        "BAKERCO" => "Baker County",
        "BALLARDH" => "Ballard Hudson",
        "BANKSCOU" => "Banks County",
        "BANKSSTEPHENS" => "Banks Stephens",
        "BARBER" => "Barber",
        "BARTLETT" => "Bartlett",
        "BAYCREEK" => "Bay Creek",
        "BAYSPRINGS" => "Bay Springs",
        "BEARCREEK" => "Bear Creek",
        "BENHILLC" => "Ben Hill County",
        "BENNETTSMILL" => "Bennetts Mill",
        "BERKMAR" => "Berkmar",
        "BERRIEN" => "Berrien",
        "BETHUNE" => "Bethune",
        "BLACKMON" => "Blackmon Road",
        "BLECKLEY" => "Bleckley County",
        "BLOOMFIELD" => "Bloomfield",
        "BONAIRE" => "Bonaire",
        "ETBOOTH" => "Booth - Cherokee",
        "BOOTHJC" => "Booth - Fayette",
        "JONESVILLE" => "Bowdon",
        "BRADWELL" => "Bradwell Institute (06-12)",
        "BRANTLEY" => "Brantley County",
        "BREMAN" => "Bremen",
        "BROOKSCO" => "Brooks County",
        "BROWN" => "Brown",
        "BRYANCOU" => "Bryan County",
        "BUFORD" => "Buford",
        "BUNCHE" => "Bunche",
        "BURKECOU" => "Burke County",
        "BURNEYHA" => "Burney-Harris-Lyons",
        "BUTLEREA" => "Butler Early Childhood Ctr. (PK-12)",
        "BYRON" => "Byron",
        "CWDAVIS" => "C.W. Davis",
        "GRAY" => "CA Gray",
        "CALHOUN" => "Calhoun",
        "CALHOUNC" => "Calhoun County",
        "CALIFF" => "Califf",
        "CALLAWAY" => "Callaway",
        "CAMDEN" => "Camden",
        "CAMPCREEK" => "Camp Creek",
        "CAMPBELL" => "Campbell",
        "SCOGGINS" => "Carl Scoggins Sr.",
        "CARROLLT" => "Carrollton Jr.",
        "CARTERSM" => "Cartersville",
        "CARVER" => "Carver - Walton",
        "CARVERS" => "Carver Road",
        "CASS" => "Cass",
        "CEDARGROVE" => "Cedar Grove",
        "CEDARTOW" => "Cedartown",
        "CENTRALCR" => "Central - Carroll",
        "CENTRAL" => "Central - Thomas",
        "CHAMBLEE" => "Chamblee",
        "CHAMPIONS" => "Champions Theme School",
        "CHAPELHLDE" => "Chapel Hill - Dekalb",
        "CHAPELHLDO" => "Chapel Hill - Douglas",
        "CHATTAHO" => "Chattahoochee County",
        "CHATTANOOG" => "Chattanooga Valley",
        "CHESTATEE" => "Chestatee",
        "CHESTNUT" => "Chestnut Log",
        "CLARKE" => "Clarke",
        "CLAXTON" => "Claxton",
        "CLAYCOUN" => "Clay County",
        "CLEARCREEK" => "Clear Creek",
        "CLEMENTS" => "Clements",
        "CLINCHC" => "Clinch County",
        "CLIFTONR" => "Clifton Ridge",
        "SAMMYEECOAN" => "Coan",
        "COASTAL" => "Coastal",
        "COFFEE" => "Coffee",
        "COILE" => "Coile",
        "COLLINS" => "Collins",
        "COLUMBIAC" => "Columbia - Columbia",
        "COLUMBIA" => "Columbia - Dekalb",
        "COMMERCE" => "Commerce",
        "CONYERS" => "Conyers",
        "COOKCOUN" => "Cook County",
        "COOPER" => "Cooper",
        "COOSA" => "Coosa",
        "COUCH" => "Couch",
        "COUSINS" => "Cousins",
        "COWANROAD" => "Cowan Road",
        "CRABAPPLE" => "Crabapple",
        "CRAWFORD" => "Crawford County",
        "CREEKLAND" => "Creekland - Cherokee",
        "CREEKLAN" => "Creekland - Gwinnett",
        "CRISPCOU" => "Crisp County",
        "CROSSROADSC" => "Crossroads - Carroll",
        "CROSSROADS" => "Crossroads - Douglas",
        "DACULA" => "Dacula",
        "DADE" => "Dade",
        "DALTON" => "Dalton",
        "DANIELL" => "Daniell",
        "DAVIDSON" => "Davidson Magnet(6-12)",
        "DEANRUSK" => "Dean Rusk",
        "DERENNE" => "Derenne",
        "DICKERSON" => "Dickerson",
        "DODGECOU" => "Dodge County",
        "DODGEN" => "Dodgen",
        "DOOLYCOU" => "Dooly County",
        "DOUBLECH" => "Double Churches",
        "DOUGHERT" => "Dougherty International Education",
        "SHAMROCK" => "Druid Hills",
        "DUBLIN" => "Dublin",
        "DULUTH" => "Duluth",
        "DURHAM" => "Durham",
        "DUTCHTOWN" => "Dutchtown",
        "EAGLESLANDM" => "Eagles Landing",
        "EARLYCOU" => "Early County",
        "EASTBRO" => "East Broad Street",
        "EASTAUGU" => "East Augusta",
        "EASTCOBB" => "East Cobb",
        "EASTCOFF" => "East Coffee",
        "EASTCOLU" => "East Columbus Magnet Academy",
        "EASTCOWET" => "East Coweta",
        "EASTHALLM" => "East Hall",
        "EASTJACKSON" => "East Jackson",
        "EASTLAUR" => "East Laurens",
        "EASTPAULD" => "East Paulding",
        "EASTBROOK" => "Eastbrook",
        "ECHOLSC" => "Echols County",
        "WHITEEDDIE" => "Eddie White Academy",
        "EBENEZER" => "Ebenezer",
        "EDDY" => "Eddy",
        "EDWARDS" => "Edwards",
        "EFFINGHA" => "Effingham County",
        "EIGHTHST" => "Eighth Street",
        "ELBERTCO" => "Elbert County",
        "ELLIS" => "Ellis",
        "ELKINSPT" => "Elkins Pointe",
        "EMANUELC" => "Emanuel County Institute (6-12)",
        "EVANS" => "Evans - Columbia",
        "EVAN" => "Evans - Coweta",
        "EVANSOTH" => "Evans - Other",
        "FACTORYSHOALS" => "Factory Shoals",
        "FAIRPLAY" => "Fairplay",
        "FANNINCO" => "Fannin County",
        "FAYETTE" => "Fayette",
        "FEAGINMI" => "Feagin Mill",
        "FIVEFORKS" => "Five Forks",
        "FLATROCK" => "Flat Rock",
        "FLOYD" => "Floyd",
        "FORESTPRK" => "Forest Park",
        "FORT" => "Fort",
        "FORTVALL" => "Fort Valley",
        "FRANKNOSBO" => "Frank N. Osborne",
        "FRANKLIN" => "Franklin County",
        "FREEDO" => "Freedom - Cherokee",
        "FREEDOMP" => "Freedom Park",
        "FREEDOM" => "Freedom - Dekalb",
        "GAINESVIL" => "Gainesville",
        "GARDNERN" => "Gardner-Newman",
        "GARRETT" => "Garrett",
        "GEORGETOWNC" => "Georgetown",
        "GENERALRAY" => "General Ray Davis",
        "GILMER" => "Gilmer",
        "GLADDEN" => "Gladden",
        "GLASCOCK" => "Glascock County",
        "GLENNCJONE" => "Glenn C. Jones",
        "GLENNHIL" => "Glenn Hills",
        "GLENNVIL" => "Glennville",
        "GLYNN" => "Glynn",
        "GORDONLEE" => "Gordon Lee",
        "GRACESN" => "Grace Snell",
        "GRAYSTATION" => "Gray Station",
        "GREENBRI" => "Greenbrier",
        "GREENVIL" => "Greenville",
        "GRIFFIN" => "Griffin",
        "GROVETOW" => "Grovetown",
        "HAHIRA" => "Hahira",
        "HAMPTON" => "Hampton",
        "HANCOCKCENTRAL" => "Hancock Central",
        "HARALSONCO" => "Haralson County",
        "HARLEM" => "Harlem",
        "HARPERAR" => "Harper-Archer",
        "HARRISCO" => "Harris County Carver",
        "HARTCOUN" => "Hart County",
        "HAVENACADE" => "Haven Academy",
        "HAYMONMORRIS" => "Haymon-Morris",
        "HYNSBRDGE" => "Haynes Bridge",
        "HEARDCOU" => "Heard County",
        "HENDERB" => "Henderson - Butts",
        "HENDERSON" => "Henderson - Dekalb",
        "HENRYM" => "Henry County",
        "HEPHZIBA" => "Hephzibah",
        "HERITAGE" => "Heritage",
        "HESSE" => "Hesse",
        "HJONES" => "Herschel Jones",
        "HIGHTOWERC" => "Hightower Trail",
        "HILSMAN" => "Hilsman",
        "HLCMBRDGE" => "Holcomb Bridge",
        "HOPEWELL" => "Hopewell",
        "HOWARD" => "Howard",
        "HUBERT" => "Hubert",
        "HULL" => "Hull",
        "HUNTINGTON" => "Huntington",
        "HUTTO" => "Hutto",
        "INDIANCR" => "Indian Creek",
        "INMAN" => "Inman",
        "ISLEOF" => "Isle Of Hope",
        "IRMACAUSTIN" => "Irma C. Austin",
        "IRWINCOU" => "Irwin County",
        "DOBBINS" => "J.A. Dobbins",
        "JRICHARDS" => "J.E. Richards",
        "JRTRIPPE" => "J.R. Trippe",
        "JANEMACO" => "Jane Macon",
        "JASPER" => "Jasper",
        "JASPERCO" => "Jasper County",
        "JEFFDAVI" => "Jeff Davis",
        "JEFFERSO" => "Jefferson",
        "JEFFTWIGG" => "Jeffersonville Intermediate (5-6)",
        "JENKINSC" => "Jenkins County",
        "STEWART" => "Stewart",
        "JOHNSONC" => "Johnson County",
        "JONESBORO" => "Jonesboro",
        "KENDRICK" => "Kendrick",
        "KENNEDY" => "Kennedy",
        "KENNEDYROAD" => "Kennedy Road",
        "KING" => "King",
        "KINGSBRIDGE" => "Kings Bridge",
        "KIPP" => "Kipp",
        "LAFAYETTE" => "Lafayette",
        "LAKESIDE" => "Lakeside - Columbia",
        "LAKESIDEF" => "Lakeside - Forsyth",
        "LAKEVIEW" => "Lakeview",
        "LAMARCOU" => "Lamar County",
        "LANGFORD" => "Langford",
        "LANGSTON" => "Langston Chapel",
        "LANIER" => "Lanier",
        "LANIERCO" => "Lanier County",
        "LEE" => "Lee",
        "LEECOUNT" => "Lee County",
        "LMMOSES" => "Lena Mae Moses",
        "LEWISFRA" => "Lewis Frasier",
        "LIBERTY" => "Liberty - Forsyth",
        "LIBERTYN" => "Liberty - Newton",
        "LILBURN" => "Lilburn",
        "LINCOLNC" => "Lincoln County",
        "LINDLEY" => "Lindley",
        "LITHONIA" => "Lithonia",
        "LITTLEMILL" => "Little Mill",
        "LOCUSTGROVE" => "Locust Grove",
        "LOGANVILLEM" => "Loganville",
        "LONG" => "Long",
        "LONGCANE" => "Long Cane",
        "LONGCOUN" => "Long County(Pk-12)",
        "LOSTMOUNT" => "Lost Mountain",
        "LOUISERADL" => "Louise Radloff",
        "LOUISVIL" => "Louisville",
        "LOVEJOY" => "Lovejoy",
        "LOW" => "Low",
        "LOVINGGOOD" => "Lovinggood",
        "LOWNDES" => "Lowndes",
        "LUELLA" => "Luella",
        "LYERLY" => "Lyerly",
        "LUMPKIN" => "Lumpkin County",
        "ROBERTSMD" => "M.D. Roberts",
        "MABRY" => "Mabry",
        "MACINTYREP" => "Macintyre Park",
        "MACONCOU" => "Macon County",
        "MADISONC" => "Madison County",
        "MADRAS" => "Madras",
        "MALCOMBRID" => "Malcom Bridge",
        "MANCHEST" => "Manchester",
        "MARIETTAJR" => "Marietta",
        "MARIETTA6" => "Marietta 6Th Grade",
        "MARIONCOUNTY" => "Marion County",
        "MARSHALL" => "Marshall",
        "MARTHAPU" => "Martha Puckett",
        "MARYMCLEODBETHUNE" => "Mary Mcleod Bethune",
        "MASONCREEK" => "Mason Creek",
        "MCCLESKEY" => "Mccleskey",
        "MCCLURE" => "Mcclure",
        "MCCONNELL" => "Mcconnell",
        "MCINTOSH" => "Mcintosh County",
        "MCNAIRD" => "McNair - Dekalb",
        "MCAIRF" => "Mcnair",
        "MENIO" => "Menlo",
        "MEMORIAL" => "Memorial",
        "MERCER" => "Mercer",
        "MERRYACR" => "Merry Acres",
        "METTER" => "Metter",
        "MIDLAND" => "Midland",
        "MIDWAY" => "Midway",
        "MILLCREEK" => "Mill Creek",
        "MILLERCO" => "Miller County",
        "MGROVE" => "Miller Grove",
        "MILLERMA" => "Miller Magnet",
        "MITCHELL" => "Mitchell County",
        "MODEL" => "Model",
        "MONROECO" => "Monroe County",
        "MONTGOME" => "Montgomery County",
        "MOORE" => "Moore",
        "MORGANCO" => "Morgan County",
        "MORGANRO" => "Morgan Road",
        "MORROW" => "Morrow",
        "MOSSYCREEK" => "Mossy Creek",
        "MOUNTZION" => "Mt. Zion",
        "MUNDYSMLL" => "Mundys Mill",
        "MURPHEY" => "Murphey",
        "MYERS" => "Myers",
        "NEEDWOOD" => "Needwood",
        "BAGLEY" => "New Bagley",
        "DAWSON" => "New Dawson County",
        "NEWHOPE" => "New Hope",
        "NEWBERN" => "Newbern",
        "NORTHCLTN" => "North Clayton",
        "NORTHFORS" => "North Forsyth",
        "NORTHGWINNETT" => "North Gwinnett",
        "NORTHHAB" => "North Habersham",
        "NORTHHALLM" => "North Hall",
        "NORTHWHITF" => "North Whitfield",
        "NORTHSIDEH" => "Northside",
        "NORTHWSTRN" => "Northwestern",
        "OAKHILL" => "Oak Hill",
        "OCONEECOUN" => "Oconee County",
        "OGLEACAD" => "Oglethorpe Academy (6-8)",
        "OGLETHOR" => "Oglethorpe County",
        "OLA" => "Ola",
        "PBRITCH" => "P.B. Ritch",
        "OTWELL" => "Otwell",
        "PALMER" => "Palmer",
        "PARKS" => "Parks",
        "PATRICKHENRY" => "Patrick Henry",
        "PWEST" => "Paul D. West",
        "PEACHTREE" => "Peachtree",
        "PELHAM" => "Pelham",
        "PEPPERELL" => "Pepperell",
        "PERRY" => "Perry",
        "PICKENS" => "Pickens County",
        "PIERCECO" => "Pierce County",
        "PIKECOUN" => "Pike County",
        "PNCKNYVLLE" => "Pinckneyville",
        "PINEHILL" => "Pine Hill",
        "PINEGROVE" => "Pine Grove",
        "PINEMT" => "Pine Mountain",
        "PINEYGROVE" => "Piney Grove",
        "PTSOUTH" => "Pointe South",
        "PORTAL" => "Portal",
        "PRICE" => "Price",
        "PULASKIC" => "Pulaski County",
        "NEWQUIT" => "Quitman County",
        "PUTNAMCO" => "Putnam County",
        "RABUNCOU" => "Rabun County",
        "RADIUMSP" => "Radium Springs",
        "RANDOLPH" => "Randolph Clay",
        "REDAN" => "Redan",
        "REDBUD" => "Red Bud",
        "REHOBRD" => "Rehoboth Road",
        "REIDSVIL" => "Reidsville",
        "RENAISSANCE" => "Renaissance",
        "CRENFROE" => "Renfroe",
        "REXMILL" => "Rex Mill",
        "RICHARDSG" => "Richards - Gwinnett",
        "RICHARDSM" => "Richards - Muscogee",
        "RICHMOND" => "Richmond Hill",
        "RIDGEVIEW" => "Ridgeview",
        "RINGGOLD" => "Ringgold",
        "RISINGSTAR" => "Rising Star",
        "RISLEY" => "Risley",
        "RIVERTRAIL" => "River Trail",
        "RIVERDALE" => "Riverdale",
        "RIVERSIDEC" => "Riverside",
        "RIVERVIEW" => "Riverview",
        "RIVERWATCH" => "Riverwatch",
        "ROBERTAC" => "Robert A. Cross Fundamental",
        "ROCKMART" => "Rockmart",
        "ROME" => "Rome",
        "ROSSVILLE" => "Rossville",
        "ROTHSCHI" => "Rothschild",
        "RRUSSELL" => "Russell",
        "RUTLAND" => "Rutland",
        "SAINTMAR" => "Saint Marys",
        "SALEM" => "Salem",
        "MCCLUREP" => "Sammy Mcclure Sr.",
        "SANDTOWN" => "Sandtown",
        "SANDYSPGS" => "Sandy Springs",
        "SCHLEY" => "Schley",
        "SCREVENC" => "Screven County",
        "SEGO" => "Sego",
        "SEMINOLE" => "Seminole County",
        "SEQUOYAH" => "Sequoyah - Clayton",
        "SEQUOYA" => "Sequoyah - DeKalb",
        "SEWELL" => "Sewell",
        "SHIVER" => "Shiver",
        "SHILOH" => "Shiloh",
        "SHUMAN" => "Shuman",
        "SIMPSON" => "Simpson",
        "SMITHA" => "Smitha",
        "SMOKEYRD" => "Smokey Road",
        "SNELLVILLE" => "Snellville",
        "SNELSONG" => "Snelson-Golden",
        "SOCIALCIRCLE" => "Social Circle",
        "SONORAVILLEE" => "Sonoraville East",
        "SOUTHCENT" => "South Central",
        "SOUTHEFF" => "South Effingham",
        "SOUTHFORS" => "South Forsyth",
        "SOUTHHAB" => "South Habersham",
        "SOUTHHALLM" => "South Hall",
        "SOUTHPAUL" => "South Paulding",
        "SOUTHEAS" => "Southeast Bulloch",
        "SOUTHSID" => "Southside",
        "SOUTHWES" => "Southwest",
        "SPIRITCR" => "Spirit Creek",
        "STALEY" => "Staley",
        "STALLINGSISLAND" => "Stallings Island",
        "STEPHENS" => "Stephens County",
        "STEPHENSON" => "Stephenson",
        "STEWARTC" => "Stewart County",
        "STOCKBRIDG" => "Stockbridge",
        "STONEMTM" => "Stone Mountain",
        "SUMMEROUR" => "Summerour",
        "SUMMERVI" => "Summerville",
        "SUMTERCO" => "Sumter County",
        "SUTTON" => "Sutton",
        "SWAINSBO" => "Swainsboro",
        "SWEETWATER" => "Sweetwater",
        "SYLVANHIL" => "Sylvan Hills",
        "TJELDER" => "T.J. Elder",
        "CENTRALT" => "Talbot County",
        "TALIAFER" => "Taliaferro County",
        "TAPP" => "Tapp",
        "TAYLORCO" => "Taylor County",
        "TAYLORRD" => "Taylor Road",
        "TEASLEY" => "Teasley",
        "TELFAIRC" => "Telfair County",
        "TEMPLE" => "Temple",
        "TERRELLC" => "Terrell County",
        "THOMASCOUNTY" => "Thomas County",
        "THOMSONH" => "Thomson - Houston",
        "THOMSON" => "Thomson - Mcduffie",
        "TOOMBSCO" => "Toombs County",
        "TOWNSCOU" => "Towns County Comprehensive(PK-12)",
        "TREUTLEN" => "Treutlen",
        "TRICKUM" => "Trickum",
        "TRION" => "Trion",
        "TUBMAN" => "Tubman",
        "TUCKER" => "Tucker",
        "TURNERD" => "Turner - Douglas",
        "TURNERCO" => "Turner County",
        "TURNERSPEC" => "Turner County Specialty(Pk-12)",
        "TUTT" => "Tutt",
        "TWIGGSCO" => "Twiggs",
        "TWINRIVERS" => "Twin Rivers",
        "UNIONCOU" => "Union County",
        "UNIONGRVM" => "Union Grove",
        "UPSONLE" => "Upson-Lee",
        "VALDOSTA" => "Valdosta",
        "VALLEYPOIN" => "Valley Point",
        "VETERANSMEMORIAL" => "Veterans Memorial - Muscogee",
        "VETERANS" => "Veterans Memorial - Newton",
        "VICKERYM" => "Vickery Creek",
        "VILLARICA" => "Villa Rica",
        "HORNSBY" => "W.S. Hornsby",
        "WALKER" => "Walker",
        "WARECOUN" => "Ware County",
        "WAREMAGN" => "Ware Magnet(Kk-12)",
        "WARNERRO" => "Warner Robins",
        "WARREN" => "Warren County",
        "WASHINGT" => "Washington",
        "WASHWILKES" => "Washington-Wilkes",
        "WATSONCE" => "Watson Center (6-8)",
        "WAYCROSS" => "Waycross",
        "WEAVER" => "Weaver",
        "WEBBBR" => "Webb Bridge",
        "WEBSTER" => "Webster County",
        "WESTCHAT" => "West Chatham",
        "WESTCOFF" => "West Coffee",
        "WESTHALLM" => "West Hall",
        "WESTJACKSON" => "West Jackson",
        "WESTLAUR" => "West Laurens",
        "WESTSIDET" => "West Side Magnet(3-8)",
        "WHEELERC" => "Wheeler County",
        "WHIGHAM" => "Whigham",
        "WESTMINI" => "Westminister Christian Academy (K-12)",
        "WESTSIDE" => "Westside - Barrow",
        "WESTSI" => "Westside - Whitfield",
        "WILBANKS" => "Wilbanks",
        "WHITECOU" => "White County",
        "WHITEWATER" => "Whitewater",
        "WILCOX" => "Wilcox County",
        "WILKINSO" => "Wilkinson County",
        "WILLIAMJ" => "William James",
        "WILLIAMHUBBARD" => "William M. Hubbard",
        "WILLIEJW" => "Willie J. Williams",
        "WINDERBARWINDER" => "Winder-Barrow",
        "WOODLANDB" => "Woodland - Bartow",
        "WOODLAND" => "Woodland - Fulton",
        "WOODLANDH" => "Woodland - Henry",
        "WOODSTOCK" => "Woodstock",
        "WORTHCOU" => "Worth County",
        "WRENS" => "Wrens",
        "YEAGER" => "Yeager",
        "YOUNG" => "Young",
        "YOUTH" => "Youth",
        "MARIONOTH" => "Marion - Other",
        "COOKOTH" => "Cook - Other",
        "CALHOUNOTH" => "Calhoun - Other",
        "COLUMBIAOTH" => "Columbia - Other",
        "TOOMBSOTH" => "Toombs - Other",
        "BROOKSOTH" => "Brooks - Other",
        "BLECKLEYOTH" => "Bleckley - Other",
        "PIKEOTH" => "Pike - Other",
        "OTHERROC" => "Rockdale - Other",
        "NEWOT" => "Newton - Other",
        "MITCHELLOTH" => "Mitchell - Other",
        "DODGEOTH" => "Dodge - Other",
        "TALBOTOTH" => "Talbot - Other",
        "OTHERGRE" => "Greene - Other",
        "LOWNDESOTH" => "Lowndes - Other",
        "LIBERTYOTH" => "Liberty - Other",
        "LEEOTH" => "Lee - Other",
        "EFFINGHAMOTH" => "Effingham - Other",
        "DOUOT" => "Douglas - Other",
        "ECHOLSOTH" => "Echols - Other",
        "TURNEROTH" => "Turner - Other",
        "WAREOTH" => "Ware - Other",
        "TOWNSE" => "Towns - Other",
        "OTHERCAT" => "Catoosa - Other",
        "PAUOT" => "Paulding - Other",
        "BENHILLOTH" => "Ben Hill - Other",
        "OTHERCAR" => "Carroll - Other",
        "BACONOTH" => "Bacon - Other",
        "WILCOXOTH" => "Wilcox - Other",
        "TREUTLENOTH" => "Treutlen - Other",
        "CHATTAHOOCHEEOTH" => "Chattahoochee - Other",
        "DECATUROTH" => "Decatur - Other",
        "QUITMANOTH" => "Quitman - Other",
        "PEACHOTH" => "Peach - Other",
        "OTHERLUM" => "Lumpkin - Other",
        "OTHERCLA" => "Clarke - Other",
        "OTHERCHA" => "Chattooga - Other",
        "SCHLEYOTH" => "Schley - Other",
        "OTHERWAL" => "Walker - Other",
        "PIERCEOTH" => "Pierce - Other",
        "OTHERHAN" => "Hancock - Other",
        "OTHERBAN" => "Banks - Other",
        "MONTGOMERYOTH" => "Montgomery - Other",
        "LONGOTH" => "Long - Other",
        "JENKINSOTH" => "Jenkins - Other",
        "DOOLYOTH" => "Dooly - Other",
        "DAWOT" => "Dawson - Other",
        "DOUGHERTYOTH" => "Dougherty - Other",
        "OTHERFLO" => "Floyd - Other",
        "OTHERMOR" => "Morgan - Other",
        "OTHERGOR" => "Gordon - Other",
        "STEWARTOTH" => "Stewart - Other",
        "BERRIENOTH" => "Berrien - Other",
        "SUMTEROTH" => "Sumter - Other",
        "MONTANAOTH" => "Montana - Other",
        "SEMINOLEOTH" => "Seminole - Other",
        "THOMASOTH" => "Thomas - Other",
        "BAROT" => "Bartow - Other",
        "WILKINSONOTH" => "Wilkinson - Other",
        "WALOT" => "Walton - Other",
        "MUSCOGEEOTH" => "Muscogee - Other",
        "MERIWETHEROTH" => "Meriwether - Other",
        "IRWINOTH" => "Irwin - Other",
        "GLASCOCKOTH" => "Glascock - Other",
        "FULOT" => "Fulton - Other",
        "DEKOT" => "Dekalb - Other",
        "EARLYOTH" => "Early - Other",
        "CHEOT" => "Cherokee - Other",
        "WHITEOTH" => "White - Other",
        "WASHINGOTH" => "Washington - Other",
        "UPSONOTH" => "Upson - Other",
        "TIFTOTH" => "Tift - Other",
        "COLQUITTOTH" => "Colquitt - Other",
        "FOROT" => "Forsyth - Other",
        "SPALDOTH" => "Spalding - Other",
        "OTHERGIL" => "Gilmer - Other",
        "CLINCHOTH" => "Clinch - Other",
        "BURKEOTH" => "Burke - Other",
        "BULLOCHOTH" => "Bulloch - Other",
        "LANIEROTH" => "Lanier - Other",
        "MONROEOTH" => "Monroe - Other",
        "MILLEROTH" => "Miller - Other",
        "RICHMONDOTH" => "Richmond - Other",
        "OTHERRAB" => "Rabun - Other",
        "OTHERMUR" => "Murray - Other",
        "LINCOLNOTH" => "Lincoln - Other",
        "JOHNSONOTH" => "Johnson - Other",
        "JEFFERSONOTH" => "Jefferson - Other",
        "JEFFDAVISOTH" => "Jeff Davis - Other",
        "GRADYOTH" => "Grady - Other",
        "FAYOT" => "Fayette - Other",
        "WHEELEROTH" => "Wheeler - Other",
        "WARRENOTH" => "Warren - Other",
        "WORTHOTH" => "Worth - Other",
        "TAYLOROTH" => "Taylor - Other",
        "CHATHAMOTH" => "Chatham - Other",
        "BRWOT" => "Barrow - Other",
        "OTHERSTE" => "Stephens - Other",
        "LAMAROTH" => "Lamar - Other",
        "HARTOTH" => "Hart - Other",
        "HALOT" => "Hall - Other",
        "FANNINOTH" => "Fannin - Other",
        "TALIAFERROOTH" => "Taliaferro - Other",
        "CHARLTONOTH" => "Charlton - Other",
        "BRYANOTH" => "Bryan - Other",
        "TROUPOTH" => "Troup - Other",
        "BAKEROTH" => "Baker - Other",
        "BALDWINOTH" => "Baldwin - Other",
        "BRANTLEYOTH" => "Brantley - Other",
        "OTHEROCO" => "Oconee - Other",
        "GLYNNOTH" => "Glynn - Other",
        "BIBBOTH" => "Bibb - Other",
        "EMANUELOTH" => "Emanuel - Other",
        "OTHERDAD" => "Dade - Other",
        "TERRELLOTH" => "Terrell - Other",
        "TATTNALLOTH" => "Tattnall - Other",
        "OTHERWHI" => "Whitfield - Other",
        "OTHERPIC" => "Pickens - Other",
        "OTHEROGL" => "Oglethorpe - Other",
        "OTHERMAD" => "Madison - Other",
        "JONESOTH" => "Jones - Other",
        "OTHERJAC" => "Jackson - Other",
        "OTHERHAR" => "Haralson - Other",
        "WEBSTEROTH" => "Webster - Other",
        "OTHERFRA" => "Franklin - Other",
        "RANDOLPHOTH" => "Randolph - Other",
        "SCREVENOTH" => "Screven - Other",
        "PULASKIOTH" => "Pulaski - Other",
        "CRAWFORDOTH" => "Crawford - Other",
        "ATKINSONOTH" => "Atkinson - Other",
        "OTHERHAB" => "Habersham - Other",
        "OTHERPOL" => "Polk - Other",
        "OTHERBT" => "Butts - Other",
        "MCINTOSHOTH" => "Mcintosh - Other",
        "MAINEOTH" => "Maine - Other",
        "MACONOTH" => "Macon - Other",
        "COFFEEOTH" => "Coffee - Other",
        "HENOT" => "Henry - Other",
        "HARRISOTH" => "Harris - Other",
        "COBOT" => "Cobb - Other",
        "WILKESOTH" => "Wilkes - Other",
        "CRISPOTH" => "Crisp - Other",
        "OTHERPUT" => "Putnam - Other",
        "WAYNEOTH" => "Wayne - Other",
        "UNIONOTH" => "Union - Other",
        "TWIGGSOTH" => "Twiggs - Other",
        "TELFAIROTH" => "Telfair - Other",
        "OTHERJAS" => "Jasper - Other",
        "MCDUFFIEOTH" => "Mcduffie - Other",
        "CLAYOTH" => "Clay - Other",
        "CLAOT" => "Clayton - Other",
        "LAURENSOTH" => "Laurens - Other",
        "HOUSTONOTH" => "Houston - Other",
        "CAMDENOTH" => "Camden - Other",
        "GWIOT" => "Gwinnett - Other",
        "ELBERTOTH" => "Elbert - Other",
        "COWOT" => "Coweta - Other",
        "CANDLEROTH" => "Candler - Other",
        "APPLINGOTH" => "Appling - Other",
        "OTHE" => "White - Other",
        "OTHERHEA" => "Heard - Other",
        "ARIZONAOTH" => "Arizona - Other",
        "FLORIDA" => "Florida - Other",
        "IDAHOOTH" => "Idaho - Other",
        "KANSASOTH" => "Kansas - Other",
        "AARONCOHN" => "Aaron Cohn",
        "MINNESOTAOTH" => "Minnesota - Other",
        "CONNECTICUTOTH" => "Connecticut - Other",
        "DELAWAREOTH" => "Delaware - Other",
        "HAWAIIOTH" => "Hawaii - Other",
        "ILLINOISOTH" => "Illinois - Other",
        "INDIANAOTH" => "Indiana - Other",
        "KENTUCKYOTH" => "Kentucky - Other",
        "MARYLANDOTH" => "Maryland - Other",
        "DOCOTH" => "District Of Columbia - Other",
        "CALIFORNIAOTH" => "California - Other",
        "LOUISIANAOTH" => "Louisiana - Other",
        "NEVADAOTH" => "Nevada - Other",
        "NEWJERSEYOTH" => "New Jersey - Other",
        "ALABAMA" => "Alabama - Other",
        "ALASKAOTH" => "Alaska - Other",
        "TEXASOTH" => "Texas - Other",
        "NORTHCAROLINA" => "North Carolina - Other",
        "MASSACHUSETTSOTH" => "Massachusetts - Other",
        "GRIOT" => "Other",
        "UTAHOTH" => "Utah - Other",
        "VIRGINIAOTH" => "Virginia - Other",
        "RHODEISLANOTH" => "Rhode Island - Other",
        "OKLAHOMAOTH" => "Oklahoma - Other",
        "NEBRASKAOTH" => "Nebraska - Other",
        "ARKANSASOTH" => "Arkansas - Other",
        "MICHIGANOTH" => "Michigan - Other",
        "NEWHAMPSHIREOTH" => "New Hampshire - Other",
        "OHIOOTH" => "Ohio - Other",
        "MISSISSIPPIOTH" => "Mississippi - Other",
        "COLORADOOTH" => "Colorado - Other",
        "IOWAOTH" => "Iowa - Other",
        "OTHER" => "Other",
        "OTHEROTH" => "Other",
        "MISSOURIOTH" => "Missouri - Other",
        "OREGONOTH" => "Oregon - Other",
        "NEWYORKOTH" => "New York - Other",
        "NDAKOTAOTH" => "North Dakota - Other",
        "NEWMEXICOOTH" => "New Mexico - Other",
        "SDAKOTAOTH" => "South Dakota - Other",
        "WASHINGTONOTH" => "Washington - Other",
        "WYOMINGOTH" => "Wyoming - Other",
        "TENNESSEE" => "Tennessee - Other",
        "SOUTHCAROLINA" => "South Carolina - Other",
        "PENNSYLVANIAOTH" => "Pennsylvania - Other",
        "VERMONTOTH" => "Vermont - Other",
        "WISCONSINOTH" => "Wisconsin - Other",
        "WVIRGINIAOTH" => "West Virginia - Other",
        "OTHERO" => "Other"
     },
     
     
     
     :road_surface => {
        "DIRT" => "Dirt",
        "EASEM" => "Easement",
        "GRAVL" => "Gravel",
        "PAVED" => "Paved",
        "PRIVT" => "Private Maintain",
        "PUBLC" => "Public Maintain",
        "NONE" => "None",
        "INTST" => "Interstate",
        "STATE" => "State Highway",
        "USHWY" => "US Highway",
        "OTHER" => "Other"   
     },
     :roof_type => {
        "COMPS" => "Composition",
          "COPPR" => "Copper",
          "WDSHN" => "Wood Shingle",
          "OTHER" => "Other",
          "TILE" => "Tile",
          "TARGV" => "Tar/Gravel",
          "METAL" => "Metal/Steel",
          "TIN" => "Tin",
          "CONCR" => "Concrete",
          "SLATE" => "Slate"
          },
     :showing_instructions => {
          "ANYTM" => "Anytime Access",
          "APPNT" => "Appointment Only",
          "CLAGT" => "Courtesy Call-Agent",
          "CLOFF" => "Call Listing Office/Agent Before Showing",
          "CLOWN" => "Courtesy Call-Owner",
          "CLTEN" => "Courtesy Call-Tenant",
          "DAYSL" => "Day Sleeper",
          "GATED" => "Gated",
          "KEYOF" => "Key In Office",
          "LCKBX" => "Lockbox - Supra",
          "LCKBXOTHER" => "Lockbox - Other",
          "NODIS" => "No Disturb",
          "NOLCK" => "No Lockbox",
          "NOSGN" => "No Sign",
          "NOSHW" => "No Showings In The A.M.",
          "PETS" => "Caution Pets",
          "SECUR" => "Security",
          "VACNT" => "Vacant"
      },
     
      
      :waterfront => {
        "12STEP" => "1-2 Step Entry",
        "BARNS" => "Barn(s)",
        "CARIGE" => "Carriage House",
        "DECK" => "Deck",
        "FENCE" => "Fenced Yard",
        "FRPOR" => "Front Porch",
        "GRDEN" => "Garden Area",
        "GASGR" => "Gas Grill",
        "GUEST" => "Guest House",
        "GAZBO" => "Gazebo",
        "HOTUB" => "Hot Tub/Spa",
        "INVFN" => "Invisible Fence",
        "IRRGT" => "Irrigation System",
        "OUTBL" => "Out-Buildings",
        "OUTFRE" => "Outdoor Fireplace",
        "PATIO" => "Patio",
        "POOLH" => "Pool House",
        "LNDSC" => "Prof Landscaping",
        "PLSCR" => "Pool Screened/Enclosed",
        "SATLT" => "Satellite Dish",
        "STBLE" => "Stables",
        "WRPOR" => "Wrap-Around Porch",
        "OTHER" => "Other"
        
      },
  }

  DATA_ENUMS2 = {

  }
  DATA_MAPPING2 = {
    
    
    
    # access_lock_box:
    # access_to_complex:
    # acreage:
    # address:
    # adj_sale_price:
    # adj_sale_price_psf:
    annual_maintenance_fee: 'ASSOCFEE',
    # appointment_phone_description:
    # appraise_subdivision:
    # back_on_market:
    basement_desc: 'BASEDESC',
    # bathrooms:
    bathrooms_full: 'BATHSFULL',
    bathrooms_half: 'BATHSHALF',
    beds: 'BEDROOMS',
    # builder_name:
    # building_name:
    # building_stories:
    # buyer_agent_bonus:
    # buyer_agent_compensation:
    category:  proc { |d|
      cats = {
        "RES" => "single_family",
        "CND" => "townhouse_condo",
        "LND" => "ranch_acerage",
        "LOT" => "lots",
        "MUL" => "multi_family",
        "RNT" => "rental",
        "COM" => "commercial"
     }

     cats[d['PROPERTYTYPE']]
    },
    cdom: 'DAYSONMARKET',
    city: 'CITY',
    # city_state_zip:  
    closed_date:   proc { |d|  to_date_sql(d['CLOSEDATE'])  }, 
    # completion_date:
    cooling_system_type: 'COOLSYSTEM',
    # countertop_type: 
    county: 'COUNTYID',
    # data_source:
    # data_updated_at:
    # deed_date:
    # defects:
    disclosures: 'SPECIAL',
    dom: 'DAYSONMARKET',
    # dom_updated_at:
    equipment: 'APPLIANCES', #*
    estimated_close_date:   proc { |d|  to_date_sql(d['CLOSEDDATEPROPOSED'])  },  #*
    # exemption:
    expiration_date:  proc { |d|  to_date_sql(d['EXPIRATIONDATE'])  },   
    exterior_description: 'EXTERIOR',
    # exterior_type:
    # fence_type:
    # fenced_yard:
    financing_available: 'OWNERFINANCE',
    fireplace_number: 'FIREPLACE',
    # floor_type:
    # foundation_type:
    garage_attach: proc { |d| 
      fvals = d['PARKING'].to_s 

      fval_items = fvals.split(",")

      fval_items  = fval_items.map { |d| 
        d.try(:gsub,/"/,'')
      }
      ret = nil
      GATTCH = {
          "ATTCH" => "Y",
          "DTACH" => "N",
          "NONE" =>  nil }

      
      fval_items.each { |d|
         case d 
          when 'ATTCH'
            ret = GATTCH[d]
            break
          when 'DTACH'
            ret =  GATTCH[d]
            break
         end
         
  
      }
      ret
      
      
    },
    garage_capacity: proc { |d| 
      fvals = d['PARKING'].to_s 

      fval_items = fvals.split(",")

      fval_items  = fval_items.map { |d| 
        d.try(:gsub,/"/,'')
      }
      CAPAC = {
          "ASSGN" => "0",
          "1CARP" => "0",
          "1GARG" => "1",
          "1UNASSGN" => "0",
          "2ASSGN" => "0",
          "2CARP" => "0",
          "2GARG" => "2",
          "2UNASSGN" => "0",
          "3CARP" => "3",
          "4GARG" => "4",
          "ATTCH" => "0",
          "12STEP" => "0",
          "AUTOG" => "0",
          "COVERED" => "0",
          "DTACH" => "0",
          "DRVUN" => "0",
          "DRVWY" => "0",
          "KTCHN" => "0",
          "PRKLT" => "0",
          "PRKPD" => "0",
          "PDECK" => "0",
          "SIDER" => "0",
          "STORE" => "0",
          "STRET" => "0",
          "NONE" => "0"}

      result = 0
      fval_items.each { |d|

        incre = CAPAC[d].to_s.to_i

        result += incre
      }

      result.to_s
    },
    # geo_market_area:
    heating_system:  'HEATSYSTEM',
    # hirise_fees:
    hoa_bill_freq: proc {|d| "1" },
    hoa_membership: 'ASSOCFEEPAID',
    interior_description:  'INTERIOR',
    key_map_book:  'PLATBOOK',
    key_map_page: 'PLATPAGE',
    # latitude:
    # legal_description:
    # legal_subdivision:
    # level_of_unit:
    list_price: 'LISTPRICE',
    # list_price_psf:
    # listing_agent_email:
    listing_agent_fax: 'AGENTFAX',
    listing_agent_id: 'LISTINGPUBLICID',
    listing_agent_name: 'AGENTNAME',
    listing_agent_phone: 'AGENTLISTMOBILE',
    # listing_broker_id:
    listing_broker_name: 'OFFICELIST_OFFICENAME',
    # :status => {
    #     "ACT" => "active",
    #     "CONDD" => "pending",
    #     "CONKO" => "pending",
    #     "CONTG" => "pending",
    #     "PEND" => "pending",
    #     "CLOSD" => "sold",
    #     "POA" => "pending",
    #     "EXP" => "expired",
    #     "WITH" => "withdrawn",
    #     "TERM" => "terminated"
    #   },
    listing_date:  proc { |d| 
      to_date_sql(d['LISTDATE'])
      # case d['STATUS']
       # when 'CLOSD','EXP','WITH','TERM'
        # to_date_sql(d['LISTDATE'])  
       # else
        # to_date_sql(d['LASTTRANSDATE'])
        # # to_date_sql(Time.new.strftime(Date::DATE_FORMATS[:default]))  
      # end

    },

    # lock_box_code:
    # longitude:
    # loss_mitigation:
    lot: 'LOT',
    # lot_back_dimensions:
    lot_description: 'LOTDESC',
    # lot_front_dimension:
    # lot_improvements:
    # lot_left_dimentsion:
    # lot_number:
    # lot_right_dimension: 
    lot_square_feet:   proc { |d|
      case d["LOTSIZE"]
        when "13ACR"
            "10000"
        when "1312A"
            "20000"
        when "HLF34"
            "30000"
        when "3/41"
            "40000"
        when "1-2AC"
            "60000"
        when "2-3AC"
            "100000"
        when "3-5AC"
            "160000"
        when "510A"
            "300000"
        when "1020"
            "600000"
        when "20+AC"
            "1200000"
        else
            nil
      end
    },
    # lot_use:
    maintenance_fee: 'ASSOCFEE',
    # millage_rate:
    # mineral_rights:
    mls_area_key: 'AREA',
    mls_area_name: 'AREA', 
    mls_elementary_school: 'SCHOOLELEM',
    # mls_elementary_school_number: 
    # mls_flood:
    mls_high_school: 'SCHOOLHIGH',
    # mls_high_school_number: 
    mls_middle_school: 'SCHOOLJUNIOR',
    # mls_middle_school_number: 
    mls_number: 'MLNUMBER',
    # mls_record_creation_date: 
    mls_record_modified: proc { |d| to_date_sql(d['LASTTRANSDATE']) }, 
    # mortgagee: 
    # neighborhood_number:
    # new_construction:
    # new_constructiion_description:
    # night_phone:
    number_of_photos: 'IMAGECOUNT',
    # number_of_units:
    # occupancy_rate:
    # office_price:  
    office_phone:  'OFFICELISTPHONE',
    original_list_price:  'ORIGINALLISTPRICE',
    # other_building_fees:
    # other_fees
    # owner_city
    # owner_city_state_zip:
    # owner_email:
    # owner_estimated_loan_balance: 
    owner_name: 'OWNERNAME',
    # owner_occupied: 
    owner_phone: 'OWNERPHONE',
    # owner_state: 
    # owner_street_address: 
    # owner_zip: 
    # owner_zip_4: 
    # owner_zip_code: 
    pending_date: proc { |d| to_date_sql(d['CONTRACTDATE']) },
    # photo_count: 'IMAGECOUNT',
    # pool_area: 
    
    pool_private: proc { |d| 
      fvals = d['POOL'].to_s 

      fval_items = fvals.split(",")

      fval_items  = fval_items.map { |d| 
        d.try(:gsub,/"/,'')
      }
      ret = nil
      POOLCONST = {
          "ABOVE" => "Y",
        "FIBER" => "Y",
        "GUNIT" => "Y",
        "HEATD" => "Y",
        "INGRN" => "Y",
        "VINYL" => "Y",
        "NONE" => "N"
      }

      
      fval_items.each { |d|
         case d 
          when 'ABOVE','FIBER','GUNIT','HEATD','INGRN','VINYL','NONE'
            ret = POOLCONST[d]
            break
          else 
            ret =  "N"
            break
         end
         
  
      }
      ret
      
      
    },
    # previous_list_price
    public_remarks: 'REMARKS',
    # range_number: #*
    realtor_remarks: 'REALREMARKS',
    rental_class: proc { |d|
      rcs = {
        "RES" => "single_family",
        "CND" => "townhouse_condo",
        "LND" => "ranch_acerage",
        "LOT" => "lots",
        "MUL" => "multi_family",
        "RNT" => "rental",
        "COM" => "commercial"
     }
     rcs[d['PROPERTYTYPE']]
    },
    # rental_sale_mls_number:   #*
    # rental_terms:   #*
    rental_type: proc { |d|
      rts = {
        "CONDO" => "condo",
        "DUPLX" => "duplex",
        "HOUSE" => "freestanding",
        "QUADP" => "fourplex",
        "TRIPL" => "Triplex",
        "ZEROL" => "Town house"
       }
       rts[d['TYPE']]
    },
    road_surface:  proc { |d|
      fvals = d['ROADTYPE'].to_s 

      fval_items = fvals.split(",")

      fval_items  = fval_items.map { |d| 
        d.try(:gsub,/"/,'')
      }

      fval_items.join(",")
    },    
    roof_type: proc { |d|
      fvals = d['ROOFTYPE'].to_s 

      fval_items = fvals.split(",")

      fval_items  = fval_items.map { |d| 
        d.try(:gsub,/"/,'')
      }

      fval_items.join(",")
    },

     
    # rooms:   #*
    # sale_price: 'SALEPRICE',  #*
    # sale_price_psf:   #*
    # school_district:   #*
    school_district_name: 'COUNTYID',  #*
    section_number: 'SECTION', #*  
    # seller_paid_to_buyer_closing_costs: #*  
    # selling_repair_credits: #*  
    # selling_agent_email:   #*
    selling_agent_id: 'SELLINGPUBLICID', #*  
    # selling_agent_name:   #*
    # selling_agent_phone:   #*
    showing_instructions: 'SHOWINSTR',  #*
    sold_price: 'SALEPRICE',
    # siding_type: #*
    single_family_type:   proc { |d|
      case d["PROPERTYTYPE"]
      when "RES"
        'freestanding'
      when "CND"
        'townhome'
      else
        nil
      end
    },
    sold_lease_price: 'SALEPRICE',   #*
    square_feet: 'SQFTTOTAL',   #*
    status: proc { |d|
      sts = {"ACT" => "active",
      "CONDD" => "pending",
      "CONKO" => "pending",
      "CONTG" => "pending",
      "PEND" => "pending",
      "CLOSD" => "sold",
      "LEASE" => "sold",
      "POA" => "pending",
      "EXP" => "expired",
      "WITH" => "withdrawn",
      "TERM" => "terminated" }

      sts[d['STATUS']]
    },
    # stories: 'STORIES',  #*
    street_direction: 'STREETDIRSUFFIX',  #*
    street_name: proc { |d| [d['STREETNAME'],d['STREETTYPE']].reject(&:blank?).join(" ") },  #*
    street_number: 'STREETNUMBER',  #*
    style_of_building: 'STYLE',  #*
    state: 'STATE',  #*
    subdivision: 'SUBDIVISION',  #*
    tax_amount: 'TAXAMOUNT',  #*
    tax_id: 'TAXID',  #*
    tax_year: 'TAXYEAR',  #*
    # termination_date: 
    # township: 'DIST',  #*
    # waterfront:  { proc |d|
    #   fvals = d['EXTERIOR'].to_s 

    #   fval_items = fvals.split(",")

    #   fval_items  = fval_items.map { |d| 
    #     d.try(:gsub,/"/,'')
    #   }
    #   ret = nil
            
    #   fval_items.each { |d|
    #      case d 
    #       when 'DPWTRAC','DPWTDOK','FLDPL','LAGOON','LKEFT','LKEVW','OCNFT','OCNVW','POND','RVRFT','RVRVW'
    #         ret =  "Y"
    #         break
    #       when 'BORDR','CTYVW','CORNR','CREEK','CULDE','GOLFC','GLFVW','LEVEL','LVLDR','MARSH','MTNFT','MTNVW','PASTR','PRVBK','PLTEN','SLOPE','STEEP','TIDAL','WOODD'
    #         ret =  "N"            
    #      end
         
  
    #   }
    #   ret
    # },    
    withdrawn_date: proc { |d| to_date_sql(d['WITHDRAWNDATE']) }, 
    year_built:  'YEARBUILT',
    zip_4: 'ZIPPLUS4',  #*
    zip_code: 'ZIPCODE',  #*    
    # cooling_system_type: 'CoolingType',
    # listing_agent_fax: 'IDD2E0CBF1',
    # road_surface: 'RDFR',
    # hirise_fees: 'CAM',
    # multi_owner_operating: 'GEX',
    # muti_gross_operating_income: 'GIN',
    # heating_system: 'HeatingTyp',
    # buyer_agent_compensation: 'ComDollar',
    # lot_back_dimensions: 'LTDM',
    # pending_date: proc { |d| to_date_sql(d['SD']) },
    # mls_elementary_school: 'ES',
    # mls_middle_school: 'MS',
    # mls_high_school: 'HS',
    # mls_elementary_school_number: 'ES',
    # mls_middle_school_number: 'MS',
    # mls_high_school_number: 'HS'
    sub_category: proc { |d| "1" }

  }
  
  

  
  def rets_login
    @client = Rets::Client.new(:login_url => "http://gamls.rets.reinsightmls.com/RetsServer/Login/index.rails",:username => market.mls_login,:password => market.mls_password)
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "Alanta #1 RETS logged in"
    true
  end

  def rets_login2
    @client = Rets::Client.new(:login_url => "http://fml.rets.interealty.com/Login.asmx/Login",:username => "MLKAY",:password => "gus@Pr4b", :agent => "RETS-Connector1.2_mlktech")
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "Alanta #2 RETS logged in"
    true
  end

  def rets_logout
    @client.logout
    Delayed::Worker.logger.debug "logged out"
    true
  end

  def mls_url_for(property)
    "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
  end

  def grab(days)

    Delayed::Worker.logger.debug "~~@~~~ loggin in ~~~~~~"
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
      parse_rets()
      rets_logout
      
    else
      self.errors[:login] = "could not login MLS1"
    end

    # if rets_login2
    #   Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
    #   parse_rets2()
    #   rets_logout
    # else 
    #   self.errors[:login] = "could not login MLS2"
    # end

    notify_listeners
  end

  def sync_rets(days)

    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
      parse_rets()
      rets_logout
      
    else
      self.errors[:login] = "could not login MLS1"
    end

    # if rets_login2
    #   Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
    #   parse_rets2()
    #   rets_logout
    # else 
    #   self.errors[:login] = "could not login MLS2"
    # end

    notify_listeners
  end
  
  def add_dom(days)
    
    super
    
    x = 0
    Delayed::Worker.logger.debug "#{market.name} specific update ##{market.id}"
    x = self.market.properties.active_and_pending.update_all ["cdom = ifnull(cdom,ifnull(dom,0))  + DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL ifnull(dom,0) DAY) - 1) )
                          , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          ["(listing_date is not null) and ((cdom is null) or (dom is null)) "]
                          
    Delayed::Worker.logger.debug "rows affected: #{x}"
    self.rows_affected = (self.rows_affected.nil? ? 0 : self.rows_affected ) + x
    
    notify_listeners
  end

  def self.to_date_sql(dt)
    #"2014-05-09T15:40:14"
    field = dt.to_s
    pattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil
    subpattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil
    ma =  field.match(pattern).captures.try(:[],0) rescue nil
    ta =  field.match(subpattern).captures.try(:[],0) rescue nil
    if field && field.match(/(\d+\/\d+\/\d+)/)
      strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil
    else
      strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil
    end
  rescue
    nil
  end

  protected

  private

    def parse_raw(data, filename)

      begin
        CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

          # row['PropType'] ||= filename.try(:slice, -7, 3)
          save_property(row, DATA_MAPPING, DATA_ENUMS)
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "error #{e.message}"
      end
    end

    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

    # def parse_rets()
      
    #     # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
    #     timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
    #     timerange = "2014-01-01-2014-06-11"
    #     sd = Date.parse('2014-10-22')
    #     ed = Date.parse('2014-10-22')
    #     Delayed::Worker.logger.debug "--------- pulling single family ---------"
    #     ed.downto(sd) { |date|
          
    #       Delayed::Worker.logger.debug "---------  #{date} ---------"
    #       date1 = date.strftime("%Y-%m-%dT00:00:00")
    #       date2 = date.strftime("%Y-%m-%dT06:00:00")
    #       date3 = date.strftime("%Y-%m-%dT12:00:00")
    #       date4 = date.strftime("%Y-%m-%dT18:00:00")
    #       tomorrow = date + 1.day
    #       date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
    #       dates = []
    #       dates = [date1,date2,date3,date4,date5]
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
    #       for i in 0..3
    #         Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
    #         begin  
             
    #           recdate = "#{dates[i]}-#{dates[i+1]}"
               
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling SFA ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=SFA)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing SFA ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #             end
    #           rescue  Exception => e
    #             Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling SFD ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=SFD)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing SFD ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
              
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling RR ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=RR)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing RR ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
              
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling LL ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=LL)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing LL ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
              
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling AF ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=AF)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing AF ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
            
    #         rescue Exception => e
    #           Delayed::Worker.logger.debug "No record found #{e.message}"
    #         end
            
            
          
    #       end
          
         
    #     }

       

    # end

    # def parse_rets2()
      
    #     # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
    #     timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
    #     timerange = "2014-01-01-2014-06-11"
    #     sd = Date.parse('2014-10-22')
    #     ed = Date.parse('2014-10-22')
    #     Delayed::Worker.logger.debug "--------- pulling single family ---------"
    #     ed.downto(sd) { |date|
          
    #       Delayed::Worker.logger.debug "---------  #{date} ---------"
    #       date1 = date.strftime("%Y-%m-%dT00:00:00")
    #       date2 = date.strftime("%Y-%m-%dT06:00:00")
    #       date3 = date.strftime("%Y-%m-%dT12:00:00")
    #       date4 = date.strftime("%Y-%m-%dT18:00:00")
    #       tomorrow = date + 1.day
    #       date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
    #       dates = []
    #       dates = [date1,date2,date3,date4,date5]
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
    #       for i in 0..3
    #         Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
    #         begin  
             
    #           recdate = "#{dates[i]}-#{dates[i+1]}"
               
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling RES ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing RES ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling CND ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing CND ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling LND ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing LND ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling LOT ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing LOT ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling RNT ---------"
                
    #             # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5347139),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
    #             results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=CLOSD,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing RNT ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
            
    #         rescue Exception => e
    #           Delayed::Worker.logger.debug "No record found #{e.message}"
    #         end
            
            
          
    #       end
          
         
    #     }

       

    # end
    
    def pull_rets(property)

      begin
        
        

          begin
            Delayed::Worker.logger.debug "--------- pulling SFD ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STN=#{property.street_number}),(STR=#{property.street_direction}*),(ZIP=#{property.zip_code})",:limit => 60000)
            
            
            Delayed::Worker.logger.debug "--------- importing SFD ---------"
            
            
            if results && results.count > 0
              row = results.first 
              puts "##row: #{row}"
              result = push_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              
              selected_fields = result.select{|x| ![:street_number,:street_direction,:street_name].include?(x)}
              
              property.assign_attributes(selected_fields)
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
            
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

         

        

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    # def parse_rets()
# 
      # begin
#           
        # begin
          # Delayed::Worker.logger.debug "--------- pulling AF ---------"
          # results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(LN=07346752),(TYP=AF)",:limit => 60000)
          # Delayed::Worker.logger.debug "--------- importing AF ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
          # end
        # rescue  Exception => e
           # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
#         
# 
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end


    def parse_rets()

      begin
        
        divisor = 720
        offset = 840
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling SFA ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFA)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing SFA ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling SFD ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing SFD ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling RR ---------"
            results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=RR)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RR ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling LL ---------"
            results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=LL)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing LL ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling AF ---------"
            results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=AF)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing AF ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    def parse_rets2()

      begin
        
        divisor = 720
        offset = 840
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling CND ---------"
            results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing CND ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling LND ---------"
            results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing LND ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling LOT ---------"
            results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing LOT ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling RNT ---------"
            
            # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5347139),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=CLOSD,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing RNT ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
    # def parse_rets()
# 
      # begin
#         
#        
          # begin
            # Delayed::Worker.logger.debug "--------- pulling SFD ---------"
            # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07353317,07353359),(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(TYP=SFD)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing SFD ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # # begin
            # # Delayed::Worker.logger.debug "--------- pulling CND ---------"
             # # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7195336,7197394),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            # # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # # Delayed::Worker.logger.debug "--------- importing CND ---------"
            # # results.each do |row|
              # # Delayed::Worker.logger.debug "##row: #{row}"
# #               
              # # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # # end
          # # rescue  Exception => e
             # # Delayed::Worker.logger.debug "No record found #{e.message}"
          # # end
#           
          # # begin
            # # Delayed::Worker.logger.debug "--------- pulling CND ---------"
             # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            # # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # # Delayed::Worker.logger.debug "--------- importing CND ---------"
            # # results.each do |row|
              # # Delayed::Worker.logger.debug "##row: #{row}"
# #               
              # # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # # end
          # # rescue  Exception => e
             # # Delayed::Worker.logger.debug "No record found #{e.message}"
          # # end
# #           
          # # begin
            # # Delayed::Worker.logger.debug "--------- pulling CND ---------"
             # # results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            # # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # # Delayed::Worker.logger.debug "--------- importing CND ---------"
            # # results.each do |row|
              # # Delayed::Worker.logger.debug "##row: #{row}"
# #               
              # # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # # end
          # # rescue  Exception => e
             # # Delayed::Worker.logger.debug "No record found #{e.message}"
          # # end
# #           
          # # begin
            # # Delayed::Worker.logger.debug "--------- pulling CND ---------"
             # # results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            # # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # # Delayed::Worker.logger.debug "--------- importing CND ---------"
            # # results.each do |row|
              # # Delayed::Worker.logger.debug "##row: #{row}"
# #               
              # # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # # end
          # # rescue  Exception => e
             # # Delayed::Worker.logger.debug "No record found #{e.message}"
          # # end
# #           
          # # begin
            # # Delayed::Worker.logger.debug "--------- pulling CND ---------"
             # # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            # # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # # Delayed::Worker.logger.debug "--------- importing CND ---------"
            # # results.each do |row|
              # # Delayed::Worker.logger.debug "##row: #{row}"
# #               
              # # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # # end
          # # rescue  Exception => e
             # # Delayed::Worker.logger.debug "No record found #{e.message}"
          # # end
# 
#           
#           
# 
#           
# 
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end

    def parse_sale_price()

      begin
        
        
        # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
        # timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
        # timerange = "2014-01-01-2014-06-11"
        sd = Date.parse('2013-09-12')
        ed = Date.parse('2013-12-31')
        Delayed::Worker.logger.debug "--------- pulling single family ---------"
        ed.downto(sd) { |date|
          
          Delayed::Worker.logger.debug "---------  #{date} ---------"
          date1 = date.strftime("%Y-%m-%dT00:00:00")
          date2 = date.strftime("%Y-%m-%dT06:00:00")
          date3 = date.strftime("%Y-%m-%dT12:00:00")
          date4 = date.strftime("%Y-%m-%dT18:00:00")
          tomorrow = date + 1.day
          date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
          dates = []
          dates = [date1,date2,date3,date4,date5]
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
          for i in 0..3
            Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
            begin  
             
               recdate = "#{dates[i]}-#{dates[i+1]}"
               Delayed::Worker.logger.debug "--------- pulling SFD ---------"
                results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=S,RL),(RECMODDATE=#{recdate}),(TYP=RR)",:limit => 60000)
                Delayed::Worker.logger.debug "--------- importing SFD  ---------"
                results.each do |row|
                  Delayed::Worker.logger.debug "##row: #{row}"
                  correct_sale_price(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
                end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end
          
          end
        }

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
        
    def parse_special_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= 'res'
        correct_property(row, DATA_MAPPING2, DATA_ENUMS2)
      end
    end
    
    def correct_property(original, mapping=nil, enums={}, update_only=false)
      # exectute mappings and retrieve raw data
      
      if mapping
       
        original = original.to_hash.with_case_insensitive_access
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "Attribute #{attr}"  #$$
          result[attr] =
            
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.to_s.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
           
            elsif (string.is_a?(BigDecimal))
              Delayed::Worker.logger.debug "Big Decimal"  #$$
              string
              
              
            else
              if attr == :listing_date
                elems = string.split("/")
                mme = elems[0].to_s.rjust(2, '0')
                dde = elems[1].to_s.rjust(2, '0')
                if elems[2].to_s.include?("14")
                  yye = "2014"
                elsif elems[2].to_s.include?("13")
                  yye = "2013"
                elsif elems[2].to_s.include?("12")
                  yye = "2012"
                elsif elems[2].to_s.include?("11")
                  yye = "2011"
                elsif elems[2].to_s.include?("10")
                  yye = "2010"
                elsif elems[2].to_s.include?("09")
                  yye = "2009"
                elsif elems[2].to_s.include?("08")
                  yye = "2008"
                elsif elems[2].to_s.include?("07")
                  yye = "2007"          
                end
                # yye = elems[2].length == 2 ? "20#{elems[2]}" : elems[2]
                dt_string = "#{mme}/#{dde}/#{yye}"
                Property.parse(attr, dt_string)
              elsif attr == :sub_category
                self.timestamp
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
              
            end
          
          
            # if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              # nil 
            # elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # elsif (string.is_a?(BigDecimal))
              # string
            # else
              # if attr == :listing_date
                # elems = string.split("/")
                # mme = elems[0].to_s.rjust(2, '0')
                # dde = elems[1].to_s.rjust(2, '0')
                # yye = elems[2].length == 2 ? "20#{elems[2]}" : elems[2]
                # dt_string = "#{mme}/#{dde}/#{yye}"
                # Property.parse(attr, dt_string)
              # else
                # Property.parse(attr, string)
              # end
            # end
          result
        end
      else
        raw = {}
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      end
     
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      property = market.import7(self.source, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      
      # self.errors[current_filename] ||= {}
      # if property.nil? || property.is_a?(String) 
        # self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
        # self.results[current_filename] ||= 0
        # self.results[current_filename] += 1
        # self.ids << property.id
        # unless property.errors.empty?
          # self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        # end
      # end
    end
    
    def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          Delayed::Worker.logger.debug "~~self.job_id: #{job_id}"
          result[attr] =

            
            
            
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Delayed::Worker.logger.debug "$$ raw #{raw}"
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'gamls' } 
      
      property = market.import3(self.source, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end


    def save_rets2(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              # Delayed::Worker.logger.debug "$$ raw #{raw}"
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp
              elsif attr == :tax_id
                string
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'fmls' } 
      property = market.import4(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end
    
    
    def push_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
               if attr == :sub_category
                nil
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      
      end
      
    end
    
end
