class Mred < Provider
  

  LOOKUP_TYPE_ENUMS = {
    :basement_desc => {
      "K" => "Bathroom Rough-In",
      "A" => "Finished",
      "B" => "Partially Finished",
      "C" => "Unfinished",
      "D" => "Crawl",
      "E" => "Cellar",
      "F" => "English",
      "G" => "Sub-Basement",
      "H" => "Slab",
      "I" => "Exterior Access",
      "J" => "Other",
      "N" => "None"
    },
    :cooling_system_type => {
      "D" => "1 (Window/Wall Unit)",
      "E" => "2 (Window/Wall Unit)", 
      "G" => "3+ (Window/Wall Unit)",
      "A" => "Central Air",
      "I" => "None",
      "B" => "Partial",
      "C" => "Space Pac",
      "H" => "Zoned",
    },
    :exemption => {
      "F" => "Senior Freeze",
      "H" => "Homeowner",
      "N" => "None",
      "O" => "Other",
      "S" => "Senior"
     },
    :exterior_type => {
      "A" => "Aluminum/Vinyl/Steel Siding",
      "O" => "Asbestos Siding",
      "E" => "Block",
      "B" => "Brick",
      "C" => "Cedar",
      "S" => "Clad Trim",
      "N" => "Concrete",
      "F" => "EIFS (e.g. Dryvit).",
      "D" => "Frame",
      "G" => "Glass",
      "V" => "Heat Tape-Pipes",
      "P" => "Limestone",
      "H" => "Log",
      "M" => "Marble/Granite",
      "I" => "Masonite",
      "R" => "Other",
      "J" => "Shakes",
      "T" => "Sheltered Porch",
      "Q" => "Slate",
      "L" => "Stone",
      "K" => "Stucco",
      "U" => "Underskirt",
      "X" => "None",
      "W" => "Pre-cast",
      "A1" => "Aluminum Siding",
      "A2" => "Vinyl Siding",
      "A3" => "Steel Siding"
    },
    :floor_type => {
      "I" => "Sustainable",
      "D" => "Stone",
      "E" => "Slate",
      "F" => "Travertine",
      "G" => "Granite",
      "U" => "Porcelain Tile",
      "B" => "Terracotta",
      "C" => "Carpet",
      "H" => "Hardwood",
      "M" => "Marble",
      "O" => "Other",
      "P" => "Parquet",
      "T" => "Ceramic Tile",
      "V" => "Vinyl",
      "W" => "Wood Laminate"
      },
    :foundation_type => {
        "A" => "Block",
        "B" => "Brick",
        "C" => "Concrete",
        "H" => "Other",
        "D" => "Pillars",
        "E" => "Reinforced Caisson",
        "F" => "Stone",
        "G" => "Wood"
    },
    :heating_system => {
      "N" => "2+ Sep Heating Systems",
      "H" => "Baseboard",
      "B" => "Electric",
      "F" => "Forced Air",
      "A" => "Gas",
      "J" => "Gravity Air",
      "K" => "Heat Pump",
      "G" => "Hot Water/Steam",
      "O" => "Indv Controls",
      "R" => "None",
      "C" => "Oil",
      "Q" => "Other",
      "D" => "Propane",
      "I" => "Radiant",
      "L" => "Radiators",
      "E" => "Solar",
      "M" => "Space Heater/s",
      "P" => "Zoned",
    },
    :interior_description => {
      "A" => "Vaulted/Cathedral Ceilings",
      "B" => "Skylight(s)",
      "C" => "Sauna/Steam Room",
      "D" => "Hot Tub",
      "E" => "Bar-Dry",
      "F" => "Bar-Wet",
      "G" => "Elevator",
      "H" => "Hardwood Floors",
      "I" => "Wood Laminate Floors",
      "J" => "Heated Floors",
      "K" => "Solar Tubes/Light Tubes",
      "L" => "1st Floor Bedroom",
      "O" => "In-Law Arrangement",
      "P" => "1st Floor Laundry",
      "Q" => "2nd Floor Laundry",
      "R" => "Pool Indoors",
      "S" => "1st Floor Full Bath",
      "M" => "Theatre Room",
      "T" => "Laundry Hook-Up in Unit",
      "U" => "Storage",
      "V" => "Flexicore"
    },
    :loan_term => {
      "D" => "Assumption-Conv",
      "E" => "Assumption-FHA",
      "F" => "Assumption-VA",
      "O" => "Cash Only",
      "H" => "Contract (Articles) for Deed",
      "A" => "Conventional",
      "B" => "FHA",
      "Q" => "Land/Lease",
      "I" => "Lease/Purchase",
      "P" => "Other",
      "J" => "Owner May Help/Assist",
      "K" => "Purchase Money Mortgage",
      "G" => "Release Required",
      "L" => "Rent w/ Option",
      "M" => "Rewrite/Blend",
      "N" => "Trade/Exchange",
      "C" => "VA",
      "R" => "Wrap Around",
      "S" => "Negotiable",
      "T" => "Lease-back"
    },
    :rental_terms => {
      "Q" => "1 Year Lease",
      "R" => "2 Year Minimum Lease",
      "S" => "Available for Sale",
      "T" => "Board Approval",
      "U" => "Cancellation Clause",
      "V" => "Furnished",
      "H" => "Lease/Purchase",
      "W" => "Month to Month",
      "P" => "Other",
      "L" => "Rent w/ Option",
      "X" => "Short Term Lease",
      "Y" => "Sublet"
    },
    :roof_type => {
       "A" => "Asphalt/Glass (Rolled)",
       "B" => "Asphalt/Glass (Shingles)",
       "J" => "Flat Roof",
       "C" => "Metal",
       "L" => "Other",
       "K" => "Pitched Roof",
       "D" => "Rubber",
       "I" => "Shingles",
       "E" => "Slate",
       "F" => "Tar and Gravel",
       "G" => "Tile",
       "H" => "Wood Shakes/Shingles"
    },
    :style_of_building => {
       "A" => "American 4-Sq.",
       "B" => "Bi-Level",
       "C" => "Brownstone",
       "D" => "Bungalow",
       "E" => "Cape Cod",
       "F" => "Colonial",
       "G" => "Contemporary",
       "H" => "Cottage",
       "I" => "English",
       "J" => "Farmhouse",
       "K" => "French Provincial",
       "L" => "Georgian",
       "M" => "Greystone",
       "N" => "Log",
       "O" => "Mediter./Spanish",
       "Z" => "Other",
       "P" => "Prairie",
       "Q" => "Quad Level",
       "R" => "Queen Anne",
       "S" => "Ranch",
       "T" => "Row House",
       "U" => "Step Ranch",
       "V" => "Traditional",
       "W" => "Tri-Level",
       "X" => "Tudor",
       "Y" => "Victorian",
       "AA" => "Mid Level",
       "AB" => "A-Frame",
       "AC" => "Walk-Out Ranch"
         
    },
    :equipment => {
        "R" => "Air Cleaner",
        "S" => "Air Exchanger",
        "L" => "CO Detectors",
        "M" => "Ceiling Fan",
        "D" => "Central Vacuum",
        "N" => "Fan-Attic Exhaust",
        "O" => "Fan-Whole House",
        "K" => "Fire Sprinklers",
        "A" => "Humidifier",
        "J" => "Intercom",
        "I" => "Security System",
        "Q" => "Sprinkler-Lawn",
        "P" => "Sump Pump",
        "G" => "TV Antenna",
        "E" => "TV-Cable",
        "F" => "TV-Dish",
        "H" => "TV-Rotor",
        "B" => "Water-Softener Owned",
        "C" => "Water-Softener Rented",
        "T" => "Backup Sump Pump;",
        "U" => "Radon Mitigation System(s)",
        "V" => "Power Generator"
     },
     :exterior_description => {
        "A" => "Balcony",
        "B" => "Deck",
        "C" => "Patio",
        "D" => "Porch",
        "E" => "Greenhouse",
        "F" => "Hot Tub",
        "G" => "Roof Deck",
        "H" => "Porch Screened",
        "I" => "Gazebo",
        "J" => "Storage Shed",
        "K" => "Dog Run and/or Invisible Fence",
        "L" => "Horse Barn/Pole Barn",
        "M" => "Tennis Court(s)",
        "N" => "Screened Patio",
        "O" => "Screened Deck",
        "P" => "Stamped Concrete Patio",
        "Q" => "Brick Paver Patio",
        "R" => "Boat Dock/Mooring",
        "S" => "Pool Above Ground",
        "T" => "Pool In-Ground",
        "U" => "Storms/Screens",
        "V" => "Grill-Outdoors",
        "W" => "Outdoor Fireplace",
        "Y" => "End Unit",
        "Z" => "Door Monitored By TV",
        "AA" => "Master Antenna",
        "AB" => "Cable Access",
        "AC" => "2 Houses",
        "AD" => "Breezeway"
      },
    :financing_available => {
        "N" => "None",
        "D" => "Assumption-Conv",
        "E" => "Assumption-FHA",
        "F" => "Assumption-VA",
        "O" => "Cash Only",
        "H" => "Contract (Articles) for Deed",
        "A" => "Conventional",
        "B" => "FHA",
        "Q" => "Land/Lease",
        "I" => "Lease/Purchase",
        "P" => "Other",
        "J" => "Owner May Help/Assist",
        "K" => "Purchase Money Mortgage",
        "G" => "Release Required",
        "L" => "Rent w/ Option",
        "M" => "Rewrite/Blend",
        "N" => "Trade/Exchange",
        "C" => "VA",
        "R" => "Wrap Around",
        "S" => "Negotiable",
        "T" => "Lease-back"
    }
  }
  
  LSZ = {
      "B" => ".25-.49 Acre",
      "C" => ".50-.99 Acre",
      "D" => "1.0-1.99 Acres",
      "G" => "10+ Acres",
      "H" => "10.0-24.99 Acres",
      "J" => "100+ Acres",
      "E" => "2.0-4.99 Acres",
      "I" => "25.0-99.99 Acres",
      "F" => "5.0-9.99 Acres",
      "A" => "Less Than .25 Acre",
      "K" => "Oversized Chicago Lot",
      "L" => "Standard Chicago Lot",
      "E1" => "2.0-2.99 Acres",
      "E2" => "3.0-3.99 Acres",
      "E3" => "4.0-4.99 Acres",
      "F1" => "5.0-5.99 Acres",
      "F2" => "6.0-7.99 Acres",
      "F3" => "8.0-9.99 Acres"
  }
  
  LDS = {
      "A" => "Beach",
      "B" => "Chain of Lakes Frontage",
      "C" => "Channel Front",
      "D" => "Common Grounds",
      "E" => "Corner",
      "F" => "Cul-de-sac",
      "G" => "Dimensions to Center of Road",
      "H" => "Fenced Yard",
      "I" => "Forest Preserve Adjacent",
      "J" => "Golf Course Lot",
      "K" => "Horses Allowed",
      "L" => "Irregular",
      "M" => "Lake Front",
      "N" => "Landscaped Professionally",
      "O" => "Legal Non-Conforming",
      "P" => "Paddock",
      "Q" => "Park Adjacent",
      "R" => "Pond",
      "X" => "Rear of Lot",
      "S" => "River Front",
      "T" => "Stream",
      "U" => "Water Rights",
      "V" => "Water View",
      "W" => "Wooded",
      "J1" => "Nature Preserve Adjacent",
      "J2" => "Wetlands adjacent"
  }
    
  HOTSHEET_ENUMS = {
    # status: hash_from_csv('statuses'),
  }
  
  HOTSHEET_MAPPING = {
    list_price: 'LP',
    mls_number: 'LN',
    sold_lease_price: 'RNP',
    # sold_price: 'Sp',
    status: 'ST',
  }

  # DATA_LOCATION = hash_from_csv('locations')
  DATA_MLS_AREA = hash_from_csv('mls_areas')
  # DATA_MLS_SUB_AREA = hash_composite('mls_sub_areas')
  DATA_CATEGORIES = {
    category: hash_composite('at_categories')
    # single_family_type: hash_composite('at_single_family_types')
  }
  
  DATA_SINGLE_FAMILY_TYPE = {
    single_family_type: hash_composite('at_single_family_types')
  }
  DATA_ENUMS = {
    # category: hash_from_csv('categories'),
    # lot_use: hash_from_csv('lot_uses'),
    # rental_class: hash_from_csv('rental_classes'),
    rental_type: hash_from_csv('rental_types'),
    # single_family_type: hash_from_csv('single_family_types'),
    status: hash_from_csv('statuses'),
    state: hash_from_csv('states'),
    hoa_bill_freq: hash_from_csv('hoa_bill_freqs')
  }
  DATA_MAPPING = {
    access_lock_box: 'LOCKBOX',
    # access_to_complex: '',
    acreage: 'ACR',
    # address: 'STREETDIR',
    # adj_sale_price: 'SALESPRICE',
    # adj_sale_price_psf: 'SQFTPRICESOLD',

    # appointment_phone_description: 'SHOWING',
    bathrooms: 'BTH',
    bathrooms_full: 'FULL_BATHS',
    bathrooms_half: 'HALF_BATHS',
    beds: 'BR',
    # block: 'Block',
    # building_name: 'BUILDING',
    # building_stories: 'STORIESBLDG',
    buyer_agent_compensation: 'CC',
    sub_category: proc { |d|  
      case d['TYP']
        when 'AT'     
          if !d['TPC'].blank? 
            d['TPC'].split(",").first 
          elsif !d['TPE'].blank? 
            d['TPE'].split(",").first  
          elsif !d['TMF'].blank?
            d['TMF'].split(",").first   
          end 
        when 'DE','MH','MU','RN','MF'
          'NA'
      end 
    },
    single_family_type:proc { |d| 
      field = nil
      case d['TYP']
        when 'AT'
          if !d['TPC'].blank? 
            field = d['TPC'].split(",").first 
          elsif !d['TPE'].blank? 
            field = d['TPE'].split(",").first
          elsif !d['TMF'].blank?
            field = d['TMF'].split(",").first  
          end  
          lookup_category(DATA_SINGLE_FAMILY_TYPE[:single_family_type],d['TYP'],field)  
        when 'DE'
          'freestanding'
        when 'MH'
          'manufactured'
        when 'MU'  
          'duplex'
        when 'RN'
          'rental'
      end    
    },
    category: 'TYP',
     # proc { |d| 
      # field = nil
      # case d['TYP']
        # when 'AT'
          # if !d['TPC'].blank? 
            # field = d['TPC'].split(",").first 
          # elsif !d['TPE'].blank? 
            # field = d['TPE'] 
          # elsif !d['TMF'].blank?
            # field = d['TMF']  
          # end  
          # lookup_category(DATA_CATEGORIES[:category],d['TYP'],field)
        # when 'DE','MH','MU'  
          # 'single_family'
        # when 'MF'
          # if !d['TMF'].blank? && d['TMF'] = 'K' then
            # 'townhouse_condo'
          # else
            # 'multi_family'
          # end
        # when 'RN'
          # 'rental'
      # end    
    # }, 
    cdom: 'MT',
    city: 'CIT',
    closed_date: proc {|d| field = d['CLOSEDDATE']||d['RD']||""; to_date_sql(field) },
    county: 'CNY',
    disclosures: 'ATTDISCL',
    dom: 'LMT',   # proc {|d| field = d['DAY']||d['DAYS'];  field.blank? ? "0" : field  },
    mls_elementary_school: 'GS',
    exemption: 'TXC',
    expiration_date: proc {|d|   field = 'XD'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    exterior_type: 'EXT',
    financing_available: 'TRM',
    floor_type: ['FRF', 'LRF'],
    foundation_type: 'FND',
    garage_capacity: 'NO_GARAGE_SPACES',
    heating_system: 'HEA',
    # hoa_payment: 'ASSOCFEE',
    hoa_bill_freq: 'FREQ',
    mls_high_school: 'HS',
    interior_description: 'INT_PROP_FEATS',
    # key_map_page: 'MAPPAGE',
    land_size: 'DIM',
    latitude:  proc { |d| d['LAT'] ? d['LAT'].to_d : nil },
    # legal_description: 'LEGAL',
    list_price: ['LP', 'RP'],
    # list_price_psf: 'SQFTPRICE',
    listing_agent_email:  proc { |d| if !d['LAEMAIL'].blank? then d['LAEMAIL'].split(";").first end },
    listing_agent_id: 'LAID',
    listing_agent_name: proc { |d| [ d["LAFIRSTNAME"], d["LALASTNAME"]].compact.join(" ")  },
    listing_broker_id: 'LOID',
    listing_broker_name: 'LONAME',
    listing_agent_phone: 'LACELLPHONE',
    listing_date:  proc {|d|   field = 'LD'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    # loan_amortized_years: 'LOAN1YEARS',
    # loan_amount_interest_rate: 'LOAN1INTERESTRATE',
    # loan_amount_new: 'LOAN2AMOUNT',
    loan_term: 'TRM',
    # lock_box_code: 'KEYBOXNUM',
    longitude:  proc { |d|  d['LNG'] ? d['LNG'].to_d : nil },
    # lot: 'LOTNUM',
    lot_back_dimensions: proc {|d| [ lookup_source(LDS,d['LDS']), d['DIM'] ].compact.join(' ') },
    lot_description: proc {|d| lookup_source(LSZ,d['LSZ']) },
    lot_number: 'SLN',
    lot_square_feet: proc {|d|   (d['LSZ'] &&   (/^[\d]+(\.[\d]+){0,1}$/ === d['LSZ'])  ) ? d['LSZ']  : ( !d['ACR'] ?  '0' : (d['ACR'].to_d * 43560).to_i.to_s  )    },
    mls_middle_school: 'JH',
    # mineral_rights: 'SURFACERIGHTS',
    mls_area_key: 'AR',
    mls_area_name: lookup(DATA_MLS_AREA, 'AR'),
    mls_number: 'LN',
    # new_construction_description: 'YEARBUILTDESC',
    new_construction: 'NC',
    number_of_photos: 'PHOTOCOUNT',
    number_of_units: 'TNU',
    # occupancy_rate: 'OCCUPANCY',
    # office_phone: 'S1.PREFERREDPHONE',
    original_list_price: [ 'OLP', 'ORP' ],
    owner_name: 'OWR',
    pool_area: proc {|d| if d['EXT_PROP_FEATS'].split(",").select { |e| e.in?(['S','T']) }.length > 0  then "Y" else "N" end },
    # owner_occupied:  'OCCUPANCY',
    # pending_date:  proc {|d|   field = 'PENDINGDATE'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    pool_private:  proc {|d| if d['EXT_PROP_FEATS'].split(",").select { |e| e.in?(['S','T']) }.length > 0    then "Y" else "N" end },
    public_remarks: 'REMARKS',
    realtor_remarks: 'AGENTREMARKS',
    rental_class: proc {|d| if d['RENTAL_PROPERTY_TYPE'] == 'MF' then "multi_family"  end },
    rental_type: 'RENTAL_PROPERTY_TYPE',
    # rental_type: proc {|d|
    #   if d['RENTAL_PROPERTY_TYPE'] == 'LA' then "apartment"
    #   elsif d['RENTAL_PROPERTY_TYPE'] == 'LT' then "condo"
    #   elsif d['RENTAL_PROPERTY_TYPE'] == 'LX' then "duplex"
    #   elsif d['RENTAL_PROPERTY_TYPE'] == 'LP' then "fourplex"
    #   elsif d['RENTAL_PROPERTY_TYPE'] == 'LH' then "freestanding"
    #   elsif d['RENTAL_PROPERTY_TYPE'] == 'LD' then "duplex"  end    },
    # rental_sale_mls_number: 'LEASEMLSNUM',
    roof_type:  'ROF',
    # school_district: proc {|d| [ d["GSD"], d["JHD"], d["HSD"] ].compact.join(" ") },
    # school_district_name: 'SCHOOLDISTRICT',
    selling_for_lot_value:  proc {|d| if d['TPE'] && d['TPE'].split(",").select { |e| e.in?(['N']) }.length > 0  then "Y"  end },
    showing_instructions: 'SHO',
    
    siding_type: 'CNS',
    sold_price: 'SP',
    sold_lease_price: 'RNP',
    square_feet: 'ASF',
    status: 'ST',
    stories: 'NO_STORIES',
    street_direction: 'POST_DIRECTION',
    street_name: proc { |d|  [d['CP'], d['STR'], d['STREET_SUFFIX']].compact.join(" ")  },
    street_number: 'HSN',
    style_of_building: 'STY',
    state: proc { |d| 'IL' },
    subdivision: 'SUB',
    tax_amount: 'TAX',
    tax_id: 'PIN',
    unit_number: 'UN',
    year_built: 'BLT',
    selling_agent_id: 'SAID',
    # mls_record_creation_date: proc {|d|   field = 'CREATIONDATE'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    equipment: 'EQP',
    exterior_description: 'EXT_PROP_FEATS',
    # fence_type: 'FENCE',
    # fenced_yard: 'FENCEDYARD',
    fireplace_number: 'FP',
    hoa_membership: 'MAF',
    mls_record_modified:  proc {|d|  field = 'RECORDMODDATE'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/  rescue nil   ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/  rescue nil   ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?   (Delayed::Worker.logger.debug "~~Date: #{ma} #{ta}";"#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end   },
    # mud_district: 'MUD DISTRICT',
    roof_type: 'ROF',
    # mls_sub_area:  proc { |d| (d['SUBAREA'] == "0")  ? "0" : d['SUBAREA']  },
    # mls_sub_area_name: lookup_composite(DATA_MLS_SUB_AREA, 'AREA','SUBAREA'),
    # maintenance_fee: proc { |d|  if (d['ASM']) then d['ASM'] end},
    annual_maintenance_fee: proc { |d| payment = nil; freq = nil; if (d['ASM'] && d['FREQ']) then payment = d['ASM'].to_d ; freq =  d['FREQ'] end; if freq == 'A' then (payment*1).to_s elsif freq == 'M' then (payment*12).to_s elsif freq == 'N' then (payment*1).to_s elsif freq == 'Q' then (payment*4).to_s elsif freq == 'V' then (payment*1).to_s else nil end },
    zip_4: 'ZP4',
    zip_code: 'ZP',
    # back_on_market: proc {|d|   field = 'BMD'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    basement_desc: 'BAS',
    garage_attach: 'GARAGE_ONSITE',
    land_use_code: 'AZN',
    location_name: 'LOCAT', #'LO_LOCATION',
    owner_phone: 'OPH',
    previous_list_price: 'LLP',
    rental_terms: 'TLR',
    road_surface: 'RSF',
    rooms: 'RMS',
    # sale_price: 'SP',
    selling_agent_email: 'SAEMAIL',
    selling_agent_name: proc {|d| [ d['SAFIRSTNAME'], d['SALASTNAME'] ].compact.join(' ') },
    selling_agent_phone: 'SACELLPHONE',
    termination_date: proc {|d| to_date_sql(d['OMD']) },
    tax_year: 'TXY',
    township: 'TWN',
    waterfront: 'WF',
    withdrawn_date: proc {|d| to_date_sql(d['OMD']) },
    completion_date: proc {|d| to_date_sql(d['AVAILABLE_DATE']) },
    cooling_system_type: 'AIR',
    level_of_unit: 'UFL',
    listing_agent_fax: 'LAFAXNUMBER',
    road_surface: 'RSF',
    mls_elementary_school_number: 'GSD',
    mls_middle_school_number: 'JHD',
    mls_high_school_number: 'HSD'
   
  }
  
  def login
    page = agent.post 'https://members.har.com/login/dispLogin.cfm?app=engine', USERNAME: market.mls_login, PWD: market.mls_password, USERTYPE: 'M', SUBMIT: 'Log In'
    page = agent.get 'http://members.har.com/mopx/goHARMLS.cfm'
    current_url == "http://www.harmls.com/default.aspx?showStartPage=true"
  end

  def logout
    agent.get 'http://members.har.com/mopx/doLogout.cfm'
    true
  end

  def rets_login

    @client = Rets::Client.new(:login_url => "http://connectmls-rets.mredllc.com/rets/server/login",:username => market.mls_login,:password => market.mls_password)
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "Chicago RETS logged in"
    true
  end

  def rets_logout
    @client.logout
    Delayed::Worker.logger.debug "logged out"
    true
  end

  def mls_url_for(property)
    "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
  end

  def grab(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_rets()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
 
  def self.find_attached_category(typ)
   
    category_list = {
      "A" => "single_family",
      "B" => "single_family",
      "C" => "townhouse_condo",
      "D" => "townhouse_condo",
      "E" => "townhouse_condo",
      "F" => "townhouse_condo",
      "G" => "townhouse_condo",
      "H" => "townhouse_condo",
      "I" => "townhouse_condo",
      "J" => "townhouse_condo",
      "K" => "single_family",
      "L" => "multi_family",
      "M" => "multi_family",
      "N" => "multi_family",
      "O1" => "single_family",
      "O2" => "single_family",
      "O3" => "single_family",
      "O4" => "single_family",
      "O" => "single_family",
      "R" => "multi_family",
      "S" => "single_family",
      "T1" => "townhouse_condo",
      "T2" => "townhouse_condo",
      "T3" => "townhouse_condo",
      "T4" => "townhouse_condo",
      "T" => "townhouse_condo",
      "U" => "single_family",
      "V" => "single_family",
      "W" => "townhouse_condo",
      "X" => "multi_family"
    }
    
    category_list[typ]
  end
  
  def self.find_attached_single_family_type(typ)
    
    single_family_type_list = {
    "A" => "duplex",
    "B" => "patio",
    "C" => nil,
    "D" => nil,
    "E" => nil,
    "F" => nil,
    "G" => nil,
    "H" => nil,
    "I" => nil,
    "J" => nil,
    "K" => "multiplex",
    "L" => nil,
    "M" => nil,
    "N" => nil,
    "O1" => "multiplex",
    "O2" => "multiplex",
    "O3" => "multiplex",
    "O4" => "multiplex",
    "O" => "multiplex",
    "R" => nil,
    "S" => nil,
    "T1" => nil,
    "T2" => nil,
    "T3" => nil,
    "T4" => nil,
    "T" => nil,
    "U" => "historic",
    "V" => nil,
    "W" => nil,
    "X" => nil
    }
    
    single_family_type_list[typ]
  end
  
  def self.find_multiple_family_category(typ)
   nil
  end

  def self.to_date_sql(dt)
    #"2014-05-09T15:40:14"
    field = dt.to_s
    pattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil
    subpattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil
    ma =  field.match(pattern).captures.try(:[],0) rescue nil
    ta =  field.match(subpattern).captures.try(:[],0) rescue nil
    if field && field.match(/(\d+\/\d+\/\d+)/)
      strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil
    else
      strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil
    end
  rescue
    nil
  end

  protected

  private

    def parse_raw(data, filename)

      begin
        CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

          # row['PropType'] ||= filename.try(:slice, -7, 3)
          save_property(row, DATA_MAPPING, DATA_ENUMS)
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "error #{e.message}"
      end
    end

    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end
    
    def pull_rets(property)

      begin
        
        
          
          begin
            Delayed::Worker.logger.debug "--------- pulling single family ---------"
            results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(HSN=#{property.street_number}),(STR=#{property.street_direction}*),(ZP=#{property.zip_code})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing single family ---------"
    
            if results && results.count > 0
              row = results.first 
              puts "##row: #{row}"
              result = push_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            
              selected_fields = result.select{|x| ![:street_number,:street_direction,:street_name,:category,:square_feet].include?(x)}
                
              property.assign_attributes(selected_fields)
            end
          rescue  Exception => e
            Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          


      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    def parse_rets()

      begin
        
        divisor = 720
        offset = 1560
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"
          
          begin
            Delayed::Worker.logger.debug "--------- pulling single family ---------"
            results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{date_range}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing single family ---------"
    
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
            end
          rescue  Exception => e
            Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling rental ---------"
            results = client.find(:all,:search_type => "Property",:class => "RentalHome",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{date_range}),(TYP=RN)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing rental ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
            end
          rescue  Exception => e
            Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    # def parse_rets()
      # begin
         # if Rails.env.development?
          # offset = 1200
        # else
          # offset = 1560
        # end
#         
        # timenow = (Time.now - offset.minutes).strftime("%Y-%m-%dT%H:%M:%S")
#         
        # Delayed::Worker.logger.debug "--------- pulling single family ---------"
        # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}+),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
        # Delayed::Worker.logger.debug "--------- importing single family ---------"
# 
        # results.each do |row|
          # Delayed::Worker.logger.debug "##row: #{row}"
          # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
        # end
# 
        # Delayed::Worker.logger.debug "--------- pulling rental ---------"
        # results = client.find(:all,:search_type => "Property",:class => "RentalHome",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}+),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
        # Delayed::Worker.logger.debug "--------- importing rental ---------"
        # results.each do |row|
          # Delayed::Worker.logger.debug "##row: #{row}"
          # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
        # end
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end
    
    # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLSNUM=12045765)",:limit => 10)
        # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE,ACT),(CLOSEDDATE=2013-12-01-2014-01-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100000)
        # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=LSE,ACT),(MODIFIED=2013-12-01-2014-01-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100000)
        # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=LSE),(MODIFIED=2013-12-01-2014-01-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100)
        # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(CLOSEDDATE=2013-12-01-2014-01-31),(PROPSUBTYPE=S)",:limit => 10)
        # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=ACT),(MODIFIED=2013-12-01-2014-01-31),(PROPSUBTYPE=S)",:limit => 10)
        
        # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2013-01-01-2013-03-31),(PROPSUBTYPE=S)",:limit => 100000)
        
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=ACT),(MODIFIED=2014-02-01-2014-03-11),(PROPSUBTYPE=S)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=ACT),(MODIFIED=2014-02-01-2014-03-11),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2013-10-01-2013-12-31),(PROPSUBTYPE=S)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2013-07-01-2013-09-30),(PROPSUBTYPE=S)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2013-04-01-2013-06-30),(PROPSUBTYPE=S)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2013-02-13-2013-02-23),(PROPSUBTYPE=S)",:limit => 100000)
        # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2013-02-01-2013-02-13),(PROPSUBTYPE=S)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2012-12-01-2013-02-05),(PROPSUBTYPE=S)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2014-01-01-2014-01-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 1000000)
        ##results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2013-10-01-2013-12-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 1000000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2013-07-01-2013-09-30),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 1000000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2013-04-01-2013-06-30),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 1000000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2013-02-01-2013-03-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 1000000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2013-01-01-2013-01-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100000)
        
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2012-10-01-2012-11-30),(PROPSUBTYPE=S)",:limit => 20000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2012-10-15-2012-11-30),(PROPSUBTYPE=S)",:limit => 80000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2012-10-01-2012-12-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2012-08-01-2012-10-14),(PROPSUBTYPE=S)",:limit => 80000)
        ## results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=SLD,LSE),(MODIFIED=2012-07-01-2012-09-30),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 100000)
        ## results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=SLD),(MODIFIED=2012-09-01-2012-10-14),(PROPSUBTYPE=S)",:limit => 100000)
        # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2014-01-01-2014-03-31),(PROPSUBTYPE=S)",:limit => 60000)
        # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2013-10-01-2013-12-31),(PROPSUBTYPE=S)",:limit => 60000)
         # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2014-01-01-2014-03-11),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 80000)
         # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2013-10-01-2013-12-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 80000)
         # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2013-07-01-2013-09-30),(PROPSUBTYPE=S)",:limit => 60000)
         # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2013-07-01-2013-09-30),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 80000)
         # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2013-01-01-2013-06-30),(PROPSUBTYPE=S)",:limit => 80000)
         # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2013-01-01-2013-06-30),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 80000)
         # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2012-07-01-2012-12-31),(PROPSUBTYPE=S)",:limit => 80000)
         # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=CON,PND,KO,OPT),(MODIFIED=2012-07-01-2012-12-31),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 80000)

    def parse_special_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

    def parse_hotsheet(data, filename)
      Delayed::Worker.logger.debug "entering parsing hotsheet "
      parser = Nokogiri::HTML(data)
      tables = parser.css("table[@width='100%']")
      tables.each_with_index do |table, table_index|
        title = table.at_xpath("./tr/td")
        next if title.nil? || title.text.blank? || title.attr('colspan').blank?
        headers = html_strip(table.xpath("./tr[2]/td"), true)
        table.xpath("./tr").each_with_index do |row, row_index|
          next if row_index <= 1
          values = Hash[headers.zip(html_strip(row.xpath("./td")))]
          update_property(values, HOTSHEET_MAPPING, HOTSHEET_ENUMS)
        end
      end
    end

    def fetch_raw(days)
      tries = 0

      begin
          Delayed::Worker.logger.debug "fetching raw"
           Delayed::Worker.logger.debug " keys: #{DATA_ENUMS[:category].keys}"
          DATA_ENUMS[:category].keys.each do |category|

            statuses = []
            statuses = ['act','op','psho']

            page = agent.post 'http://www.harmls.com/thirdparty/Scripts/GetData.asp', {
              appMinorVersion: '0', appVersion: '4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2)',
              Name: '3RDPARTY', Pass: 'DHA0ATFA', Custom: 'ALL', Num: '4500', NumberOfProperties: '4500',
              Where: "(ListStatus%20IN%20('#{statuses.join('\',\'')}'))%20AND%20(DaysOnMarket%3E=1%20AND%20DaysOnMarket%3C=#{days})",
              ColumnHeader: 'Y', ItemList: '', Format: '0', Tax: 'undefined', DownloadDecodeOverride: '',
              Join: '', Tbl: category, preventDecodeList: '', D1SelectedIndex: '0', D1: 'ALL'
            }

            import(page) if page.is_a?(Mechanize::File)
          end
        rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        end
    end

    def fetch_hotsheet(days)
      tries = 0
      result = nil
      begin
        Delayed::Worker.logger.debug "fetching hotsheet"
        date_format = (Date.today - days).strftime('%m/%d/%Y')
        today_format = Date.today.strftime('%m/%d/%Y')
        page = agent.post 'http://www.harmls.com/Reports/Scripts/display.asp', {
          formname: 'TodaysActivity',
          dateend: "#{today_format}%2023%3A59",
          datestart: "#{date_format}%2000%3A00",
          EndDate: "#{today_format}%2023%3A59",
          EndStatement: 'End%20of%20Activity%20Report%20for%20Today',
          heading: "#{date_format}",
          propertytype: '%22-ALL-%22',
          rpt: '%27BACK%27%2C%20%27EXP%27%2C%20%27NEW%27%2C%20%27optionp%27%2C%20%27PSHO%27%2C%20%27PEND%27%2C%20%27PRICE%27%2C%20%27CLOSD%27%2C%20%27TERM%27%2C%20%27WITH%27',
          StartDate: "#{date_format}%2000%3A00",
          today: "#{today_format}",
          todaysactivity: 'x',
          fourtharea: '',
          fiftharea: 'Display.asp',
          updatehotsheetdate: '0',
          where: '',
          Area: '-ALL-',
          Location: '-ALL-',
          proptype: 'RES%2C%20CND%2C%20LND%2C%20MUL%2C%20ACR%2C%20HIR%2C%20RNT',
          aReportDetails: 'BACK%2C%20EXP%2C%20NEW%2C%20optionp%2C%20PSHO%2C%20PEND%2C%20PRICE%2C%20CLOSD%2C%20TERM%2C%20WITH',
          narrow: 'true',
          sb: '0'
        }

        #$$Delayed::Worker.logger.debug "Is mechanize file2? #{page.is_a?(Mechanize::File)}"
        #$$Delayed::Worker.logger.debug "body2: #{page.body}"
        import(page) if page.is_a?(Mechanize::File)
      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      rescue Exception => e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      end
    end
    
    def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
               if attr == :sub_category
                nil
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import3(self.source, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end
    
    
    def push_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
               if attr == :sub_category
                nil
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
     
      end
      
    end

end
