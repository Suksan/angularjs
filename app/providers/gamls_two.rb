class GamlsTwo < Provider
  
  COUNTYLIST = "APPLING,ATKINSON,BACON,BAKER,BALDWIN,BANKS,BARROW,BARTOW,BENHILL,BERRIEN,BIBB,BLECKLEY,BRANTLEY,BROOKS,BRYAN,BULLOCH,BURKE,BUTTS,CALHOUN,CAMDEN,CANDLER,CARROLL,CATOOSA,CHARLTON,CHATHAM,CHATTAHOOCH,CHATTOOGA,CHEROKEE,CLARKE,CLAY,CLAYTON,CLINCH,COBB,COFFEE,COLQUITT,COLUMBIA,COOK,COWETA,CRAWFORD,CRISP,DADE,DAWSON,DECATUR,DEKALB,DODGE,DOOLY,DOUGHERTY,DOUGLAS,EARLY,ECHOLS,EFFINGHAM,ELBERT,EMANUEL,EVANS,FANNIN,FAYETTE,FLOYD,FORSYTH,FRANKLIN,FULTON,GILMER,GLASCOCK,GLYNN,GORDON,GRADY,GREENE,GWINNETT,HABERSHA,HALL,HANCOCK,HARALSON,HARRIS,HART,HEARD,HENRY,HOUSTON,IRWIN,JACKSON,JASPER,JEFFDAVIS,JEFFERSON,JENKINS,JOHNSON,JONES,LAMAR,LANIER,LAURENS,LEE,LIBERTY,LINCOLN,LONG,LOWNDES,LUMPKIN,MACON,MADISON,MARION,MCDUFFIE,MCINTOSH,MERIWETHER,MILLER,MITCHELL,MONROE,MONTGOMERY,MORGAN,MURRAY,MUSCOGEE,NEWTON,OCONEE,OGLETHORPE,ALABAMAOTH,AKOTH,AZOTH,AROTH,CAOTH,COOTH,CTOTH,DEOTH,DCOTH,FLORIDAOTH,HIOTH,IDOTH,ILOTH,INOTH,IAOTH,KSOTH,KYOTH,LAOTH,MEOTH,MDOTH,MAOTH,MIOTH,MNOTH,MSOTH,MOOTH,MTOTH,NEOTH,NVOTH,NHOTH,NJOTH,NMOTH,NYOTH,NCAROLINOTH,NDOTH,OHOTH,OKOTH,OROTH,PAOTH,RIOTH,SCAROLINOTH,SDOTH,TENNESSEOTH,TXOTH,UTOTH,VTOTH,VAOTH,WAOTH,WVOTH,WIOTH,WYOTH,PAULDING,PEACH,PICKENS,PIERCE,PIKE,POLK,PULASKI,PUTNAM,QUITMAN,RABUN,RANDOLPH,RICHMOND,ROCKDALE,SCHLEY,SCREVEN,SEMINOLE,SPALDING,STEPHENS,STEWART,SUMTER,TALBOT,TALIAFERRO,TATTNALL,TAYLOR,TELFAIR,TERRELL,THOMAS,TIFT,TOOMBS,TOWNS,TREUTLEN,TROUP,TURNER,TWIGGS,UNION,UPSON,WALKER,WALTON,WARE,WARREN,WASHINGTON,WAYNE,WEBSTER,WHEELER,WHITE,WHITFIELD,WILCOX,WILKES,WILKINSON,WORTH"
  CITYLIST = "ABBEVI,ACWORT,ADAIRS,ADEL,ADRIAN,AILEY,ALAMO,ALAPAH,ALBANY,ALDORA,ALLENH,ALLENT,ALMA,ALPHAR,ALSTON,ALTO,AMBROS,AMERIC,ANDERS,APPLIN,ARABI,ARAGON,ARCADE,ARGYLE,ARLING,ARMUCH,ARNOLD,ASHBUR,ATHENS,ATLANT,ATTAPU,AUBURN,AUGUST,AUSTEL,AVALON,AVERA,AVONDA,AXSON,BACONT,BAINBR,BALDWN,BALLGR,BARNES,BARNEY,BARTOW,BARWIC,BAXLEY,BELLVI,BELVED,BERKEL,BERLIN,BETHLE,BETWEN,BIBBCT,BIGCAN,BISHOP,BLCKSH,BLACKS,BLAINE,BLAIRS,BLAKEL,BLOOMI,BLUE,BLUFFT,BLYTHE,BOGART,BOLING,BONAIR,BONANZ,BONEVI,BOSTON,BOSTWI,BOWDON,BOWJUN,BOWERS,BOWMAN,BOXSPR,BRASEL,BRASWE,BREMEN,BRINSO,BRISTO,BRONWO,BROOKF,BROOKH,BROOKL,BROOKS,BROXTO,BRUNSW,BUCHAN,BUCKED,BUENAV,BUFORD,BUTLER,BYROMV,BYRON,CADWEL,CAIRO,CALHOU,CALVAR,CAMAK,CAMILL,CANDLE,CANDLM,CANON,CANTON,CARL,CARLTO,CARNES,CARROL,CARTEC,CARTER,CASSVI,CATAUL,CAVESP,CECIL,CEDARS,CEDART,CENTER,CENTRA,CHAMBL,CHATAH,CHATTS,CHATTA,CHAUNC,CHERRY,CHESTE,CHESTN,CHICKA,CHULA,CISCO,CLARKD,CLRKSV,CLARKS,CLAXTO,CLAYTN,CLERMO,CLEVEL,CLIMAX,CLINCH,CLOUD,CLYO,COBB,COBBTO,COCHRA,COHUTT,COLBER,COLEMA,COLLEG,COLLIN,COLQUI,COLUMB,COMER,COMMER,CONCOR,CONLEY,CONYER,COOLID,COOSA,CORDEL,CORINT,CORNEL,COTTON,COUNTR,COVING,CRABAP,CRANDA,CRAWFO,CRAWFV,CRESCE,CULLOD,CUMMIN,CUSSET,CUTHBE,DACULA,DAHLON,DAISY,DALLAS,DALTON,DAMASC,DANIEL,DANVIL,DARIEN,DASHER,DAVISB,DAWSN,DAWSON,DESOTO,DEARIN,DECATR,DEENWO,DEEPST,DEMORE,DENTON,DEWYRO,DEXTER,DILARD,DIXIE,DOCKJU,DOERUN,DONALS,DOOLIN,DORAVI,DOUGLS,DOUGLA,DOVER,DRUIDH,DRYBRA,DUPONT,DUBLIN,DUDLEY,DULUTH,DUNWDY,EASTBO,EASTDU,EASTEL,EASTGR,EASTNE,EASTPT,EASTON,EASTMA,EVILLE,EATON,EBENEZ,EDEN,EDGEHI,EDISON,ELBERT,ELKO,ELLABE,ELLAVI,ELLENT,ELLENW,ELLERS,ELLIJAY,EMERSN,ENIGMA,EPHESU,EPWORT,ESOMHI,ETON,EUHARL,EVANS,EXPERI,FAIROA,FAIRPL,FAIRBN,FAIRMT,FAIRVI,FARGO,FARMIN,FAYETT,FELTON,FITZGE,FLEMIG,FLEMIN,FLINTS,FLOVIL,FLOWER,FOLKST,FOREST,FORSYT,FRTBEN,FORTGA,FRTGOR,FRTOGL,FRTSTE,FRTVAL,FORTSO,FOWLST,FRANKN,FRANKL,FREEHM,FUNSTO,GAINSC,GAINES,GAINSV,GARDEN,GARFIE,GAY,GENEVA,GEORGE,GIBSON,GILLSV,GIRARD,GLENN,GLENNV,GLENWO,GOOD,GORDON,GOUGH,GRACEW,GRAHAM,GRNTV,GRAVES,GRAY,GRAYSN,GRAYSV,GREENS,GREENV,GRESHA,GRIFFI,GROVET,GUMBRA,GUMLOG,GUYTON,HADDOC,HAGAN,HAHIRA,HAMILT,HAMPTO,HANNAH,HAPEVI,HARALS,HARDWI,HARLEM,HARRIS,HARTSF,HARTWE,HAWKIN,HAZLEH,HELEN,HELENA,HEPHZI,HIAWAS,HIGGST,HIGHSH,HILLSB,HILLTO,HINESV,HIRAM,HOBOKE,HOGANS,HOLLYS,HOMELA,HOMER,HOMERV,HORTEN,HOSCTN,HOWARD,HULL,IDEAL,ILA,INDIAN,INMAN,IRONCI,IRONDA,IRWINT,IRWINV,ISLEOF,IVEY,JACKSO,JCKSNV,JAKIN,JASPER,JEFFER,JEFFRV,JEKYLL,JENKIN,JERSEY,JESUP,JEWELL,JOHCRK,JONESE,JULIET,JUNCTI,KATHLE,KENNES,KEYSVI,KINGSB,KINGSL,KINGST,KITE,KNOXVI,LAFAYE,LAGRAN,LAKECI,LAKEPA,SPIVEY,LAKELA,LAKEMT,LAKEVI,LAKEVW,LAVONI,LAWREN,LEARY,LEBANO,LEESBU,LENOX,LESLIE,LEXING,LILBUR,LILLY,LINCOL,LINDAL,LINWOO,LITHIA,LITHON,LIZELL,LOCUST,LOGANV,LONEOA,LOOKOU,LOUISV,LOUVAL,LOVEJO,LUDOWI,LULA,LUMBER,LUMPKI,LUTHER,LYERLY,LYONS,MABLET,MACON,MADISN,MANASS,MANCHE,MANOR,MANSFI,MARBLE,MARIET,MARSHA,MARTIN,MARTNZ,MATT,MATTHE,MAUK,MAXEYS,MAYSVI,MCCAYS,MCDONO,MCINTY,MCRAE,MEANSV,MEIGS,MELDRI,MENLO,MERIDI,MERSHO,MESENA,METTER,MIDLAN,MIDVIL,MIDWAY,MDWYHR,MILAN,MILLED,MILLEN,MILLWO,MILNER,MILTON,MINERA,MITCHE,MOLENA,MONROE,MONTEZ,MONTGO,MONTIC,MONTRO,MOODYA,MORELA,MRGAN,MORGAN,MORRIS,MORROW,MORVEN,MOULTR,MOUNTA,MOUNTB,MOUNTV,MOUNTZ,MNTCTY,MNTPRK,MURRAY,MUSELL,MYSTIC,NAHUNT,NASHVI,NAYLOR,NELSON,NEWBOR,NEWHOP,NEWING,NEWNAN,NEWTON,NICHLL,NICHOL,NORCRO,NORMAN,NORRIS,NRTATL,NRTDEC,NRTDRU,NHIGHS,NORTHM,NORWOO,NUNEZ,OAKPAR,OAKFIE,OAKHIL,OAKMAN,OAKWOD,OCHLOC,OCILLA,OCONEE,ODUM,OFFERM,OGLETH,OLIVER,OMAHA,OMEGA,ORCHAR,OTHREA,OTHRNO,OTHRSO,OTHRST,OTHRWE,OTHRAL,OTHRFL,OTHRNC,OTHRSC,OTHRTN,OXFORD,PALMET,PANTHE,PARROT,PATTER,PAVO,PAYNE,PEACHT,PEACHC,PEARSO,PELHAM,PEMBRO,PENDER,PERKIN,PERRY,PHILLI,PINELK,PINEMT,PINEMV,PINEHU,PINEVI,PITTS,PLAINF,PLAINS,PLAINV,POOLER,PORTWE,PORTAL,PORTER,POULAN,POWDER,PRESTO,PULASK,PUTNEY,QUITMA,RABUN,RANGER,RAOUL,RAYCIT,RAYLE,REBECC,REDOAK,REDAN,REEDCR,REGIST,REIDSV,REMERT,RENTZ,RESACA,RESTHA,REX,REYNOL,RHINE,RICEBO,RICHLA,RICHMO,RIDDLE,RINCON,RINGGO,RISING,RIVERD,RIVERS,ROBERT,ROBINS,ROCHEL,ROCKSP,ROCKLE,ROCKMA,ROCKY,ROCKYF,ROME,ROOPVI,ROSSVI,ROSWEL,ROYSTN,RUPERT,RUSSEL,RUTLED,RYDAL,SAINTG,SALECI,SANDHI,SANDER,SANDYS,SANTAC,SAPELO,SARDIS,SARGEN,SASSER,SAUTEE,SAVANN,SCOTLA,SCOTTS,SCREVE,SEAISL,SENOA,SEVILL,SHADYD,SHANNO,SHARON,SHRPS,SHELLM,SHILOH,SILOAM,SILCRK,SKIDAW,SKYVAL,SMARR,SMITHV,SMOKER,SMYRNA,SNELLV,SOCIAL,SOPERT,SOUTHA,SPARKS,SPARTA,SPRING,ST.MAR,ST.SIM,STAPLE,STATEN,STATES,STATHA,STEPHE,STILES,STILLM,STOCKB,STOCKT,STONEM,SUCHES,SUGARH,SUGARV,SMRTWN,SMMRVL,SUMNER,SUNNYS,SURREN,SUWANE,SWAINS,SYCAMO,SYLVAN,SYLVES,TALBOT,TALKIN,TALLAP,TALLUL,TALMO,TARRYT,TATE,TAYLOR,TEMPLE,TENNGA,TENNIL,THEROC,THMSTN,THMSVL,THOMSO,THUNDE,TIFTON,TIGER,TIGNAL,TOCCOA,TOCFAL,TOOMSB,TOWNSE,TRENTO,TRION,TUCKER,TUNNEL,TURIN,TURNER,TWINCI,TYTY,TYBEEI,TYRONE,UNADIL,UNION,UNIONP,UNIONV,UPATOI,UVALDA,VALDOS,VARNEL,VERNON,VIDALI,VIENNA,VILLA,VINING,WACO,WADLEY,WALESK,WALNUT,WALTHO,WARESB,WARMSP,WARNER,WARREN,WARTHE,WARWIC,WASHIN,WATKIN,WAVRLY,WAVERL,WAYCRO,WAYNES,WAYNBO,WESTAU,WESTGR,WESTPO,WESTON,WESTSI,WHIGHA,WHITE,WHOAK,WHITEP,WHITEM,WHITES,WILDWO,WILEY,WILLAC,WILLIA,WILMIN,WINDER,WINSTN,WINTER,WOODBI,WOODBU,WOODLA,WOODST,WOODVI,WOOLSE,WRAY,WRENS,WRIGHT,YATESV,YORKVI,YOUNGH,ZEBULO"
  CON = {
     "A" => "Aluminum/Vinyl",
      "O" => "Brick 3 Sided",
      "B" => "Brick 4 Sided",
      "P" => "Brick Front",
      "C" => "Brick/Frame",
      "L" => "Concrete Siding",
      "D" => "Log",
      "K" => "Other (See Remarks)",
      "E" => "Press Board Siding",
      "F" => "Rough-Sawn Siding",
      "Q" => "Steel Frame",
      "G" => "Stone",
      "H" => "Stone/Frame",
      "M" => "Stucco EIFS",
      "N" => "Stucco Real",
      "I" => "Stucco Unspecified",
      "R" => "Tabby",
      "J" => "Wood Siding"
    
  }
  
  CON_CM = {
      "A" => "Block",
      "B" => "Brick",
      "C" => "Frame",
      "H" => "Front/Other",
      "F" => "Other (See Remarks)",
      "G" => "Precast Concrete",
      "J" => "Precast Stone",
      "I" => "Steel & Glass",
      "D" => "Steel/Metal",
      "K" => "Stucco EIFS",
      "L" => "Stucco Real",
      "E" => "Stucco Unspecified"
  }
  
  EQP = {
    "A" => "Alarm - Burglar",
    "K" => "Alarm - Carbon Monoxide",
    "B" => "Alarm - Smoke/Fire",
    "C" => "Central Vacuum",
    "D" => "Dryer",
    "E" => "Electric Air Filter",
    "J" => "Fire Sprinkler",
    "L" => "Generator",
    "F" => "Intercom/Radio",
    "G" => "Satellite Dish",
    "H" => "Washer",
    "I" => "Water Softener"
  }
  
  EQP_KITCHEN = {
    "N" => "Convection Oven",
    "A" => "Cooktop - Separate",
    "B" => "Dishwasher",
    "K" => "Double Oven",
    "C" => "Garbage Disposal",
    "D" => "Icemaker Line",
    "E" => "Indoor Grill",
    "F" => "Microwave - Built In",
    "M" => "None",
    "L" => "Other (See Remarks)",
    "G" => "Oven - Wall",
    "H" => "Range/Oven",
    "I" => "Refrigerator",
    "J" => "Trash Compactor"
  }
  
  INT_ENUM = {
    "A" => "Attic Expandable",
    "B" => "Bookcases",
    "D" => "Cable In Street",
    "C" => "Cable TV Connections",
    "E" => "Ceilings - Trey",
    "F" => "Ceilings - Vaulted",
    "G" => "Ceilings 9 Ft Plus",
    "W" => "Double Vanity",
    "U" => "Elevator",
    "1" => "Exposed Beams",
    "I" => "Foyer - 2 Story",
    "H" => "Foyer - Entrance",
    "X" => "Garden Tub",
    "K" => "Gas Logs",
    "R" => "Handicap Access",
    "J" => "Hardwood Floors",
    "S" => "Other (See Remarks)",
    "L" => "Pulldown Attic Stairs",
    "O" => "Rear Stairs",
    "V" => "Recently Renovated",
    "T" => "Sauna",
    "Z" => "Separate Shower",
    "M" => "Skylight",
    "2" => "Tile Bath",
    "3" => "Tile Floors",
    "Y" => "Walk-in Closet",
    "N" => "Wall-to-wall Carpet",
    "P" => "Wet Bar",
    "Q" => "Whirlpool Bath"
  }

  ROOMS_ENUM = {
    "A" => "Bonus Room",
    "B" => "Den",
    "D" => "Dining Rm/Living Rm Combo",
    "C" => "Dining Room - L Shaped",
    "T" => "Dining Room Seats 12+",
    "E" => "DR - Separate",
    "P" => "Exercise Room",
    "F" => "Family Room",
    "G" => "Great Room",
    "H" => "In-law Suite or Apartment",
    "I" => "Library/Office",
    "Q" => "Loft",
    "J" => "LR Separate J J",
    "K" => "Master On Main Level",
    "O" => "Other (See Remarks)",
    "L" => "Rec Room",
    "R" => "Roommate Plan",
    "M" => "Solarium/Sun Room",
    "S" => "Split Bedroom Plan",
    "N" => "Stubbed Bath",
    "U" => "Theater/Media Room",
    "V" => "Wine Cellar"
  }

  LOOKUP_TYPE_ENUMS = {
    :basement_desc => {"40"=>"Bath Finished", "41"=>"Bath Stubbed", "42"=>"Block", "43"=>"Boat Door", "44"=>"Concrete", "45"=>"Crawlspace", "46"=>"Daylight", "47"=>"Earthen", "48"=>"Entrance - Inside", "49"=>"Entrance - Outside", "50"=>"Finished Rooms", "51"=>"Full", "52"=>"Partial", "53"=>"Pier", "54"=>"Slab/None", "485"=>"Bath Finished", "486"=>"Bath Stubbed", "487"=>"Block", "488"=>"Boat Door", "489"=>"Concrete", "490"=>"Crawlspace", "491"=>"Daylight", "492"=>"Earthen", "493"=>"Entrance - Inside", "494"=>"Entrance - Outside", "495"=>"Finished Rooms", "496"=>"Full", "497"=>"Partial", "498"=>"Pier", "499"=>"Slab/None", "1936"=>"Bath Finished", "1937"=>"Bath Stubbed", "1938"=>"Block", "1939"=>"Boat Door", "1940"=>"Concrete", "1941"=>"Crawlspace", "1942"=>"Daylight", "1943"=>"Earthen", "1944"=>"Entrance - Inside", "1945"=>"Entrance - Outside", "1946"=>"Finished Rooms", "1947"=>"Full", "1948"=>"Partial", "1949"=>"Pier", "1950"=>"Slab/None"},
    :cooling_system_type => {"93"=>"Ceiling Fan", "94"=>"Central", "95"=>"Heat Pump", "96"=>"No Cooling", "97"=>"Other (See Remarks)", "98"=>"Whole House Fan", "99"=>"Window Units", "100"=>"Zoned/Dual", "542"=>"Ceiling Fan", "543"=>"Central", "544"=>"Common", "545"=>"Heat Pump", "546"=>"No Cooling", "547"=>"Other (See Remarks)", "548"=>"Whole House Fan", "549"=>"Window Units", "550"=>"Zoned/Dual", "1993"=>"Ceiling Fan", "1994"=>"Central", "1995"=>"Common", "1996"=>"Heat Pump", "1997"=>"No Cooling", "1998"=>"Other (See Remarks)", "1999"=>"Whole House Fan", "2000"=>"Window Units", "2001"=>"Zoned/Dual"},
    :county => {"Al Cnties"=>"Al Counties", "Appling"=>"Appling", "Atkinson"=>"Atkinson", "Bacon"=>"Bacon", "Baker"=>"Baker", "Baldwin"=>"Baldwin", "Banks"=>"Banks", "Barrow"=>"Barrow", "Bartow"=>"Bartow", "Ben Hill"=>"Ben Hill", "Berrien"=>"Berrien", "Bibb"=>"Bibb", "Bldwin(AL)"=>"Baldwin (AL)", "Bleckley"=>"Bleckley", "Brantley"=>"Brantley", "Brooks"=>"Brooks", "Bryan"=>"Bryan", "Bulloch"=>"Bulloch", "Burke"=>"Burke", "Butts"=>"Butts", "Calhoun"=>"Calhoun", "Camden"=>"Camden", "Candler"=>"Candler", "Carroll"=>"Carroll", "Catoosa"=>"Catoosa", "Charlton"=>"Charlton", "Chatahoche"=>"Chattahoochee", "Chatham"=>"Chatham", "Chattooga"=>"Chattooga", "Cherokee"=>"Cherokee", "Chmbrs(AL)"=>"Chambers (AL)", "Chroke(AL)"=>"Cherokee (AL)", "Clarke"=>"Clarke", "Clay"=>"Clay", "Clayton"=>"Clayton", "Clebrn(AL)"=>"Cleburne (AL)", "Clhoun(AL)"=>"Calhoun (AL)", "Clinch"=>"Clinch", "Cobb"=>"Cobb", "Coffee"=>"Coffee", "Colquitt"=>"Colquitt", "Columbia"=>"Columbia", "Cook"=>"Cook", "Coweta"=>"Coweta", "Crawford"=>"Crawford", "Crisp"=>"Crisp", "Dade"=>"Dade", "Dawson"=>"Dawson", "Decatur"=>"Decatur", "DeKalb"=>"DeKalb", "DeKalb(AL)"=>"DeKalb (AL)", "Dodge"=>"Dodge", "Dooly"=>"Dooly", "Dougherty"=>"Dougherty", "Douglas"=>"Douglas", "Early"=>"Early", "Echols"=>"Echols", "Effingham"=>"Effingham", "Elbert"=>"Elbert", "Emanuel"=>"Emanuel", "Evans"=>"Evans", "Fannin"=>"Fannin", "Fayette"=>"Fayette", "Fl Cnties"=>"Fl Counties", "Floyd"=>"Floyd", "Forsyth"=>"Forsyth", "Franklin"=>"Franklin", "Fulton"=>"Fulton", "Gilmer"=>"Gilmer", "Glascock"=>"Glascock", "Glynn"=>"Glynn", "Gordon"=>"Gordon", "Grady"=>"Grady", "Greene"=>"Greene", "Gwinnett"=>"Gwinnett", "Habersham"=>"Habersham", "Hall"=>"Hall", "Hancock"=>"Hancock", "Haralson"=>"Haralson", "Harris"=>"Harris", "Hart"=>"Hart", "Heard"=>"Heard", "Henry"=>"Henry", "Houston"=>"Houston", "Irwin"=>"Irwin", "Jackson"=>"Jackson", "Jasper"=>"Jasper", "Jeff Davis"=>"Jeff Davis", "Jefferson"=>"Jefferson", "Jenkins"=>"Jenkins", "Jffrsn(AL)"=>"Jefferson (AL)", "Johnson"=>"Johnson", "Jones"=>"Jones", "Lamar"=>"Lamar", "Lanier"=>"Lanier", "Laurens"=>"Laurens", "Lee"=>"Lee", "Lee (AL)"=>"Lee (AL)", "Liberty"=>"Liberty", "Lincoln"=>"Lincoln", "Long"=>"Long", "Lowndes"=>"Lowndes", "Lumpkin"=>"Lumpkin", "Macon"=>"Macon", "Madison"=>"Madison", "Marion"=>"Marion", "Mcduffie"=>"Mcduffie", "Mcintosh"=>"Mcintosh", "Meriwether"=>"Meriwether", "Miller"=>"Miller", "Mitchell"=>"Mitchell", "Monroe"=>"Monroe", "Montgomery"=>"Montgomery", "Morgan"=>"Morgan", "Murray"=>"Murray", "Muscogee"=>"Muscogee", "NC Cnties"=>"NC Counties", "Newton"=>"Newton", "Oconee"=>"Oconee", "Oglethorpe"=>"Oglethorpe", "Paulding"=>"Paulding", "Peach"=>"Peach", "Pickens"=>"Pickens", "Pierce"=>"Pierce", "Pike"=>"Pike", "Polk"=>"Polk", "Pulaski"=>"Pulaski", "Putnam"=>"Putnam", "Quitman"=>"Quitman", "Rabun"=>"Rabun", "Randolph"=>"Randolph", "Richmond"=>"Richmond", "Rndlph(AL)"=>"Randolph (AL)", "Rockdale"=>"Rockdale", "Rssell(AL)"=>"Russell (AL)", "Sc Cnties"=>"Sc Counties", "Schley"=>"Schley", "Screven"=>"Screven", "Seminole"=>"Seminole", "Shelby(AL)"=>"Shelby (AL)", "Spalding"=>"Spalding", "Stephens"=>"Stephens", "Stewart"=>"Stewart", "Sumter"=>"Sumter", "Talbot"=>"Talbot", "Taliaferro"=>"Taliaferro", "Tattnall"=>"Tattnall", "Taylor"=>"Taylor", "Telfair"=>"Telfair", "Terrell"=>"Terrell", "Thomas"=>"Thomas", "Tift"=>"Tift", "Tladga(AL)"=>"Talladega (AL)", "Tn Cnties"=>"Tn Counties", "Toombs"=>"Toombs", "Towns"=>"Towns", "Tposa (AL)"=>"Tallapoosa (AL)", "Treutlen"=>"Treutlen", "Troup"=>"Troup", "Turner"=>"Turner", "Twiggs"=>"Twiggs", "Union"=>"Union", "Upson"=>"Upson", "Walker"=>"Walker", "Walton"=>"Walton", "Ware"=>"Ware", "Warren"=>"Warren", "Washington"=>"Washington", "Wayne"=>"Wayne", "Webster"=>"Webster", "Wheeler"=>"Wheeler", "White"=>"White", "Whitfield"=>"Whitfield", "Wilcox"=>"Wilcox", "Wilkes"=>"Wilkes", "Wilkinson"=>"Wilkinson", "Worth"=>"Worth"},
    :equipment => {"236"=>"Convection Oven", "237"=>"Cooktop - Separate", "238"=>"Dishwasher", "239"=>"Double Oven", "240"=>"Garbage Disposal", "241"=>"Icemaker Line", "242"=>"Indoor Grill", "243"=>"Microwave - Built In", "244"=>"None", "245"=>"Other (See Remarks)", "246"=>"Oven - Wall", "247"=>"Range/Oven", "248"=>"Refrigerator", "249"=>"Trash Compactor", "2647"=>"Stainless Steel Appliance", "687"=>"Convection Oven", "688"=>"Cooktop - Separate", "689"=>"Dishwasher", "690"=>"Double Oven", "691"=>"Garbage Disposal", "692"=>"Icemaker Line", "693"=>"Indoor Grill", "694"=>"Microwave - Built In", "695"=>"None", "696"=>"Other (See Remarks)", "697"=>"Oven - Wall", "698"=>"Range/Oven", "699"=>"Refrigerator", "700"=>"Trash Compactor", "2648"=>"Stainless Steel Appliance", "2134"=>"Convection Oven", "2135"=>"Cooktop - Separate", "2136"=>"Dishwasher", "2137"=>"Double Oven", "2138"=>"Garbage Disposal", "2139"=>"Icemaker Line", "2140"=>"Indoor Grill", "2141"=>"Microwave - Built In", "2142"=>"None", "2143"=>"Other (See Remarks)", "2144"=>"Oven - Wall", "2145"=>"Range/Oven", "2146"=>"Refrigerator", "2147"=>"Trash Compactor", "2649"=>"Stainless Steel Appliance"},
    :exterior_type => {"71"=>"Aluminum/Vinyl", "72"=>"Brick 3 Sided", "73"=>"Brick 4 Sided", "74"=>"Brick Front", "75"=>"Brick/Frame", "76"=>"Concrete Siding", "77"=>"Log", "78"=>"Other (See Remarks)", "79"=>"Press Board Siding", "80"=>"Rough-Sawn Siding", "81"=>"Steel Frame", "82"=>"Stone", "83"=>"Stone/Frame", "84"=>"Stucco EIFS", "85"=>"Stucco Real", "86"=>"Stucco Unspecified", "87"=>"Tabby", "88"=>"Wood Siding", "520"=>"Aluminum/Vinyl", "521"=>"Brick 3 Sided", "522"=>"Brick 4 Sided", "523"=>"Brick Front", "524"=>"Brick/Frame", "525"=>"Concrete Siding", "526"=>"Log", "527"=>"Other (See Remarks)", "528"=>"Press Board Siding", "529"=>"Rough-Sawn Siding", "530"=>"Steel Frame", "531"=>"Stone", "532"=>"Stone/Frame", "533"=>"Stucco EIFS", "534"=>"Stucco Real", "535"=>"Stucco Unspecified", "536"=>"Tabby", "537"=>"Wood Siding", "1971"=>"Aluminum/Vinyl", "1972"=>"Brick 3 Sided", "1973"=>"Brick 4 Sided", "1974"=>"Brick Front", "1975"=>"Brick/Frame", "1976"=>"Concrete Siding", "1977"=>"Log", "1978"=>"Other (See Remarks)", "1979"=>"Press Board Siding", "1980"=>"Rough-Sawn Siding", "1981"=>"Steel Frame", "1982"=>"Stone", "1983"=>"Stone/Frame", "1984"=>"Stucco EIFS", "1985"=>"Stucco Real", "1986"=>"Stucco Unspecified", "1987"=>"Tabby", "1988"=>"Wood Siding"},
    
    :financing_available => {"1"=>"100%", "2"=>"ASM", "3"=>"BOND", "4"=>"CASH", "5"=>"CN80", "6"=>"CN90", "7"=>"CN95", "8"=>"FHA", "9"=>"GRFA", "10"=>"OTHR", "11"=>"OWNR", "12"=>"URFA", "13"=>"VA", "14"=>"WRAP", "16"=>"Unknown", "17"=>"Leased"},
    
    
    
    
    # :garage_attach => {"287"=>"1 Car", "288"=>"2 Car", "289"=>"3 Car Or More", "290"=>"Assigned Space", "291"=>"Attached", "292"=>"Auto Garage Door", "293"=>"Carport", "294"=>"Detached", "295"=>"Drive Under/Basement", "296"=>"Garage", "2636"=>"Guest Parking", "297"=>"Kitchen Level Entry", "298"=>"None", "2637"=>"Off Street", "299"=>"Parking Pad", "300"=>"Parking Shed", "301"=>"RV/Boat Parking", "302"=>"Side/Rear Entrance", "303"=>"Storage"},
    # :garage_capacity => {"287"=>"1 Car", "288"=>"2 Car", "289"=>"3 Car Or More", "290"=>"Assigned Space", "291"=>"Attached", "292"=>"Auto Garage Door", "293"=>"Carport", "294"=>"Detached", "295"=>"Drive Under/Basement", "296"=>"Garage", "2636"=>"Guest Parking", "297"=>"Kitchen Level Entry", "298"=>"None", "2637"=>"Off Street", "299"=>"Parking Pad", "300"=>"Parking Shed", "301"=>"RV/Boat Parking", "302"=>"Side/Rear Entrance", "303"=>"Storage"},
    
    # :gated_community => {"13"=>"Airstrip", "14"=>"Boat/Camper/Van Prkg", "15"=>"Clubhouse", "16"=>"Gated Community", "17"=>"Golf Course", "18"=>"Guest Lodging", "19"=>"Lake", "20"=>"Marina", "21"=>"Neighborhood Association", "22"=>"None", "23"=>"Park", "24"=>"Physical Fit Facilities", "25"=>"Playground", "26"=>"Pool", "27"=>"Racquetball", "28"=>"Retirement Community", "29"=>"Security", "30"=>"Sidewalks", "31"=>"Stables", "32"=>"Street Lights", "33"=>"Swim Team", "34"=>"Tennis Courts", "35"=>"Tennis Team", "36"=>"Underground Utilities", "37"=>"Walk To Marta", "38"=>"Walk To Schools", "39"=>"Walk To Shopping"},
    # :golf_course => {"13"=>"Airstrip", "14"=>"Boat/Camper/Van Prkg", "15"=>"Clubhouse", "16"=>"Gated Community", "17"=>"Golf Course", "18"=>"Guest Lodging", "19"=>"Lake", "20"=>"Marina", "21"=>"Neighborhood Association", "22"=>"None", "23"=>"Park", "24"=>"Physical Fit Facilities", "25"=>"Playground", "26"=>"Pool", "27"=>"Racquetball", "28"=>"Retirement Community", "29"=>"Security", "30"=>"Sidewalks", "31"=>"Stables", "32"=>"Street Lights", "33"=>"Swim Team", "34"=>"Tennis Courts", "35"=>"Tennis Team", "36"=>"Underground Utilities", "37"=>"Walk To Marta", "38"=>"Walk To Schools", "39"=>"Walk To Shopping"},
    :heating_system => {"195"=>"Baseboard", "196"=>"Ceiling Electric", "197"=>"Central", "198"=>"Floor Furnace", "199"=>"Floor Radiant", "200"=>"Forced Air", "201"=>"Heat Pump", "202"=>"No Heating", "203"=>"Other (See Remarks)", "204"=>"Space Heater", "205"=>"Steam/Hot Water", "206"=>"Zoned/Dual", "645"=>"Baseboard", "646"=>"Ceiling Electric", "647"=>"Central", "648"=>"Common", "649"=>"Floor Furnace", "650"=>"Floor Radiant", "651"=>"Forced Air", "652"=>"Heat Pump", "653"=>"No Heating", "654"=>"Other (See Remarks)", "655"=>"Space Heater", "656"=>"Steam/Hot Water", "657"=>"Zoned/Dual", "2092"=>"Baseboard", "2093"=>"Ceiling Electric", "2094"=>"Central", "2095"=>"Common", "2096"=>"Floor Furnace", "2097"=>"Floor Radiant", "2098"=>"Forced Air", "2099"=>"Heat Pump", "2100"=>"No Heating", "2101"=>"Other (See Remarks)", "2102"=>"Space Heater", "2103"=>"Steam/Hot Water", "2104"=>"Zoned/Dual"},
    :interior_description => {"207"=>"Attic Expandable", "208"=>"Bookcases", "209"=>"Cable In Street", "210"=>"Cable TV Connections", "211"=>"Ceilings - Trey", "212"=>"Ceilings - Vaulted", "213"=>"Ceilings 9 Ft Plus", "214"=>"Double Vanity", "215"=>"Elevator", "216"=>"Exposed Beams", "217"=>"Foyer - 2 Story", "218"=>"Foyer - Entrance", "219"=>"Garden Tub", "220"=>"Gas Logs", "221"=>"Handicap Access", "222"=>"Hardwood Floors", "223"=>"Other (See Remarks)", "224"=>"Pulldown Attic Stairs", "225"=>"Rear Stairs", "226"=>"Recently Renovated", "227"=>"Sauna", "228"=>"Separate Shower", "229"=>"Skylight", "230"=>"Tile Bath", "231"=>"Tile Floors", "232"=>"Walk-in Closet", "233"=>"Carpet", "234"=>"Wet Bar", "235"=>"Whirlpool Bath", "2646"=>"Laminate Flooring", "658"=>"Attic Expandable", "659"=>"Bookcases", "660"=>"Cable In Street", "661"=>"Cable TV Connections", "662"=>"Ceilings - Trey", "663"=>"Ceilings - Vaulted", "664"=>"Ceilings 9 Ft Plus", "665"=>"Double Vanity", "666"=>"Elevator", "667"=>"Exposed Beams", "668"=>"Foyer - 2 Story", "669"=>"Foyer - Entrance", "670"=>"Garden Tub", "671"=>"Gas Logs", "672"=>"Handicap Access", "673"=>"Hardwood Floors", "674"=>"Other (See Remarks)", "675"=>"Pulldown Attic Stairs", "676"=>"Rear Stairs", "677"=>"Recently Renovated", "678"=>"Sauna", "679"=>"Separate Shower", "680"=>"Skylight", "681"=>"Tile Bath", "682"=>"Tile Floors", "683"=>"Walk-in Closet", "684"=>"Carpet", "685"=>"Wet Bar", "686"=>"Whirlpool Bath", "2645"=>"Laminate Flooring", "2105"=>"Attic Expandable", "2106"=>"Bookcases", "2107"=>"Cable In Street", "2108"=>"Cable TV Connections", "2109"=>"Ceilings - Trey", "2110"=>"Ceilings - Vaulted", "2111"=>"Ceilings 9 Ft Plus", "2112"=>"Double Vanity", "2113"=>"Elevator", "2114"=>"Exposed Beams", "2115"=>"Foyer - 2 Story", "2116"=>"Foyer - Entrance", "2117"=>"Garden Tub", "2118"=>"Gas Logs", "2119"=>"Handicap Access", "2120"=>"Hardwood Floors", "2121"=>"Other (See Remarks)", "2122"=>"Pulldown Attic Stairs", "2123"=>"Rear Stairs", "2124"=>"Recently Renovated", "2125"=>"Sauna", "2126"=>"Separate Shower", "2127"=>"Skylight", "2128"=>"Tile Bath", "2129"=>"Tile Floors", "2130"=>"Walk-in Closet", "2131"=>"Carpet", "2132"=>"Wet Bar", "2133"=>"Whirlpool Bath", "2644"=>"Laminate Flooring"},
    :lot_description => {"264"=>"City View", "265"=>"Corner", "266"=>"Cul De Sac", "267"=>"Golf Course", "268"=>"Greenbelt", "269"=>"Level Lot", "270"=>"Mountain View", "271"=>"None", "272"=>"Open Land", "273"=>"Private Backyard", "274"=>"Sloping", "275"=>"Valley View", "276"=>"Wooded", "2662"=>"Seasonal View", "724"=>"City View", "725"=>"Corner", "726"=>"Cul De Sac", "727"=>"Golf Course", "728"=>"Greenbelt", "729"=>"Level Lot", "730"=>"Mountain View", "731"=>"None", "732"=>"Open Land", "733"=>"Private Backyard", "734"=>"Sloping", "735"=>"Valley View", "736"=>"Wooded", "2663"=>"Seasonal View", "2171"=>"City View", "2172"=>"Corner", "2173"=>"Cul De Sac", "2174"=>"Golf Course", "2175"=>"Greenbelt", "2176"=>"Level Lot", "2177"=>"Mountain View", "2178"=>"None", "2179"=>"Open Land", "2180"=>"Private Backyard", "2181"=>"Sloping", "2182"=>"Valley View", "2183"=>"Wooded", "2664"=>"Seasonal View"},
    :mls_area_name => {"1"=>"FUN1", "2"=>"FUN2", "3"=>"ATN1", "4"=>"ATN2", "5"=>"ATN3", "6"=>"ATN4", "7"=>"ATSW", "8"=>"ATSE", "9"=>"FULS", "10"=>"DKE1", "11"=>"DKE2", "12"=>"DKE3", "13"=>"DKW1", "14"=>"DKW2", "15"=>"DKW3", "16"=>"GWN1", "17"=>"GWN2", "18"=>"GWN3", "19"=>"GWN4", "20"=>"GWN5", "21"=>"GWN6", "22"=>"COB1", "23"=>"COB2", "24"=>"COB3", "25"=>"COB4", "26"=>"COB5", "27"=>"COE1", "28"=>"COE2", "29"=>"COE3", "30"=>"DOUG", "31"=>"AL", "32"=>"ROC1", "33"=>"ROC2", "34"=>"CHK1", "35"=>"CHK2", "36"=>"CHK3", "37"=>"CHK4", "38"=>"DSS1", "39"=>"DSS2", "40"=>"DSS3", "41"=>"WAL1", "42"=>"WAL2", "43"=>"NEWT", "44"=>"CLAY", "45"=>"FAYE", "46"=>"COWE", "47"=>"PAU1", "48"=>"PAU2", "49"=>"FL", "50"=>"BTW1", "51"=>"BTW2", "52"=>"BTW3", "53"=>"BTW4", "54"=>"HENR", "55"=>"FOR1", "56"=>"FOR2", "57"=>"FOR3", "58"=>"FOR4", "59"=>"CARR", "60"=>"BUT1", "61"=>"SPAL", "62"=>"HAL1", "63"=>"HAL2", "64"=>"HAL3", "65"=>"HAL4", "66"=>"HAL5", "67"=>"DAW1", "68"=>"DAW2", "69"=>"DAW3", "70"=>"BNK1", "71"=>"JKS1", "72"=>"JKS2", "73"=>"JKS3", "74"=>"NC", "75"=>"BRO1", "76"=>"BRO2", "77"=>"OCN1", "78"=>"MGN1", "79"=>"PKN1", "80"=>"PKN2", "81"=>"GDN1", "82"=>"GDN2", "83"=>"FLD1", "84"=>"FLD2", "85"=>"FLD3", "86"=>"FLD4", "87"=>"FLD5", "88"=>"FLD6", "89"=>"PLK1", "90"=>"CLK1", "91"=>"HRL1", "92"=>"HRD1", "93"=>"SC", "94"=>"TRP1", "95"=>"JSP1", "96"=>"GRN1", "97"=>"PUT1", "98"=>"LMK1", "99"=>"RBN1", "100"=>"HBS1", "101"=>"WHT1", "102"=>"TN", "103"=>"TWN1", "104"=>"MERW", "105"=>"PIKE", "106"=>"LAMR", "107"=>"MNRO", "108"=>"UPSN", "109"=>"APLN", "110"=>"ATKN", "111"=>"BACN", "112"=>"BAKR", "113"=>"BLWN", "114"=>"BNHL", "115"=>"BERN", "116"=>"BIBB", "117"=>"BLKY", "118"=>"BRNT", "119"=>"BRKS", "120"=>"BRYN", "121"=>"BULL", "122"=>"BURK", "123"=>"CLHN", "124"=>"CMSE", "125"=>"CMSW", "126"=>"CMNE", "127"=>"CMNW", "128"=>"CNDL", "129"=>"CTOO", "130"=>"CHRL", "131"=>"CHTM", "132"=>"CHTA", "133"=>"CHTO", "134"=>"CLY1", "135"=>"CLNH", "136"=>"COFE", "137"=>"CLQT", "138"=>"CLMB", "139"=>"COOK", "140"=>"CRAW", "141"=>"CRSP", "142"=>"DADE", "143"=>"DCTR", "144"=>"DODG", "145"=>"DOOL", "146"=>"DRTY", "147"=>"ERLY", "148"=>"ECHL", "149"=>"EFHM", "150"=>"ELBR", "151"=>"EMUL", "152"=>"EVAN", "153"=>"FNIN", "154"=>"FRKN", "155"=>"GLMR", "156"=>"GLCK", "157"=>"GLYN", "158"=>"GRDY", "159"=>"HNCK", "160"=>"HRRS", "161"=>"HART", "162"=>"HSTN", "163"=>"IRWN", "164"=>"JFDV", "165"=>"JEFF", "166"=>"JNKN", "167"=>"JOHN", "168"=>"JONE", "169"=>"LANR", "170"=>"LRNS", "171"=>"LEE1", "172"=>"LBRT", "173"=>"LNCN", "174"=>"LONG", "175"=>"LNDS", "176"=>"MACN", "177"=>"MDSN", "178"=>"MARN", "179"=>"MCDF", "180"=>"MCIN", "181"=>"MILR", "182"=>"MTCH", "183"=>"MONT", "184"=>"MURY", "185"=>"MSCO", "186"=>"OGLE", "187"=>"PEAC", "188"=>"PIER", "189"=>"PULS", "190"=>"QUIT", "191"=>"RAND", "192"=>"RICH", "193"=>"SCHL", "194"=>"SCRV", "195"=>"SMNL", "196"=>"STEP", "197"=>"STEW", "198"=>"SMTR", "199"=>"TLBT", "200"=>"TLFR", "201"=>"TATN", "202"=>"TALR", "203"=>"TELF", "204"=>"TERL", "205"=>"TOMS", "206"=>"TIFT", "207"=>"TMBS", "208"=>"TREU", "209"=>"TRNR", "210"=>"TWIG", "211"=>"UNIN", "212"=>"WLKR", "213"=>"WARE", "214"=>"WARN", "215"=>"WASH", "216"=>"WAYN", "217"=>"WBST", "218"=>"WHLR", "219"=>"WHIT", "220"=>"WLCX", "221"=>"WLKS", "222"=>"WLKN", "223"=>"WRTH", "224"=>"Bald (AL)", "225"=>"Clhn (AL)", "226"=>"Chmb (AL)", "227"=>"Cher (AL)", "228"=>"Cleb (AL)", "229"=>"Dekb (AL)", "230"=>"Jeff (AL)", "231"=>"Lee (AL)", "232"=>"Rndl (AL)", "233"=>"Russ (AL)", "234"=>"Shel (AL)", "235"=>"Tald (AL)", "236"=>"Talp (AL)", "237"=>"Not Found"},
    :mls_elementary_school => {"A B Clark"=>"Annie Belle Clark", "A Coachman"=>"Alice Coachman", "A S Clark"=>"A S Clark", "Abbotts Hl"=>"Abbotts Hill", "Abney"=>"Abney", "Abrams"=>"Abrams", "Acworth"=>"Acworth", "Adairsvle"=>"Adairsville", "Adamsville"=>"Adamsville", "Addison"=>"Addison", "AH Watwood"=>"AH Watwood", "Alcova"=>"Alcova", "Alexander"=>"Alexander II Magnet", "Alexandria"=>"Alexandria", "Allatoona"=>"Allatoona", "Allen"=>"Allen", "Allgood"=>"Allgood", "Alpharetta"=>"Alpharetta", "Alps Road"=>"Alps Road", "Altama"=>"Altama", "Altamaha"=>"Altamaha", "Alto Park"=>"Alto Park", "Ambrose"=>"Ambrose", "Anderson"=>"Anderson", "Andrson Lv"=>"Anderson-Livesy", "Anna Davie"=>"Anna K Davie", "Anne Str"=>"Anne Street", "Annette W"=>"Annette Winn", "Annistown"=>"Annistown", "Antioch"=>"Antioch", "Appling Co"=>"Appling County", "Arbor Spg"=>"Arbor Springs", "Arbor Stat"=>"Arbor Station", "Arcado"=>"Arcado", "Argyle"=>"Argyle", "Armuchee"=>"Armuchee", "Arnco Srgt"=>"Arnco Sargent", "Arnld Mill"=>"Arnold Mill", "Arnold"=>"Arnold", "Ashford Pk"=>"Ashford Park", "Atha Road"=>"Atha Road", "Atkinson"=>"Atkinson", "Auburn"=>"Auburn", "Auburn Ed"=>"Auburn Early Education", "Austell"=>"Austell", "Austin"=>"Austin", "Austin Rd"=>"Austin Road", "Avery"=>"Avery", "Avondale"=>"Avondale", "B B Harris"=>"B B Harris", "B Gwinnett"=>"Button Gwinnett", "B Molette"=>"Burroughs Molette", "B Weir"=>"B Weir", "Bacon"=>"Bacon", "Baggett"=>"Baggett", "Baker"=>"Baker", "Baker Co"=>"Baker County", "Baker Plc"=>"Baker Place", "Baldwin"=>"Baldwin", "Ball Grnd"=>"Ball Ground", "Banks ES"=>"Banks Co Primary/Elementary", "Barden"=>"Barden", "Barksdale"=>"Barksdale", "Barnett Sh"=>"Barnett Shoals", "Barnwell"=>"Barnwell", "Barrett"=>"Barrett", "Barrow"=>"Barrow", "Barton Chp"=>"Barton Chapel", "Bartow"=>"Bartow", "Bascomb"=>"Bascomb", "Battlefld"=>"Battlefield Primary/Elementary", "Bay Creek"=>"Bay Creek", "Bay Minett"=>"Bay Minette", "Bayvale"=>"Bayvale", "BB Comer"=>"BB Comer Memorial", "Beauregard"=>"Beauregard", "Beaver Rdg"=>"Beaver Ridge", "Beaverbr"=>"Beaverbrook", "Beaverdale"=>"Beaverdale", "Beaverdam"=>"Beaverdam", "Beecher Hl"=>"Beecher Hills", "Bel Air"=>"Bel Air", "Bells Fry"=>"Bells Ferry", "Belmnt Hil"=>"Belmont Hills", "Belwood"=>"Belwood", "Ben Hill"=>"Ben Hill Co. Primary/Elementar", "Benefield"=>"Benefield", "Benteen"=>"Benteen", "Benton"=>"Benton", "Berkely Lk"=>"Berkeley Lake", "Bernd"=>"Bernd", "Berrien ES"=>"Berrien Primary/Elementary", "Berta Wbee"=>"Berta Weathersbee", "Bessie Bgt"=>"Bessie Baggett", "Bethesda"=>"Bethesda", "Bethlehem"=>"Bethlehem", "Bethun AP"=>"Bethune APS", "Bethune"=>"Bethune", "Beulah"=>"Beulah", "Big A"=>"Big A", "Big Creek"=>"Big Creek", "Big Shnty"=>"Big Shanty", "Bill Arp"=>"Bill Arp", "Birm Falls"=>"Birmingham Falls", "Birney"=>"Birney", "Blackburn"=>"Blackburn", "Blacks Mil"=>"Blacks Mill", "Blacksh Tr"=>"Blackshear Trail", "Blackshear"=>"Blackshear", "Blackwell"=>"Blackwell", "Blaine St"=>"Blaine Street", "Blanchard"=>"Blanchard", "Blandy Hil"=>"Blandy Hills", "Blanford"=>"Blanford", "Bleckley"=>"Bleckley Co Primary/Elementary", "Bloomingd"=>"Bloomingdale", "Blue Ridge"=>"Blue Ridge", "Bluff Park"=>"Bluff Park", "Blythe"=>"Blythe", "Bob Maths"=>"Bob Mathis", "Bolton"=>"Bolton", "Bonaire"=>"Bonaire", "Boston"=>"Boston", "Bowdon"=>"Bowdon", "Bowman"=>"Bowman", "Boyd"=>"Boyd", "Boynton"=>"Boynton", "Braelinn"=>"Braelinn", "Bramlett"=>"Bramlett", "Brandon ES"=>"Brandon Primary/Elementary", "Brewer"=>"Brewer", "Brian Mery"=>"Brian Merry", "Briar Vsta"=>"Briar Vista", "Briarlake"=>"Briarlake", "Bright Str"=>"Bright Star", "Britt"=>"Britt", "Britt Dvid"=>"Britt David", "Brock"=>"Brock", "Brockett"=>"Brockett", "Brocks Gap"=>"Brock's Gap", "Brookdale"=>"Brookdale", "Brooklet"=>"Brooklet", "Brooks"=>"Brooks", "Brookview"=>"Brookview", "Brookville"=>"Brookville", "Brookwood"=>"Brookwood", "Brown"=>"Brown", "Browns Mil"=>"Browns Mill", "Broxton MH"=>"Broxton Mary Hayes", "Bruce"=>"Bruce", "Brumby"=>"Brumby", "Bryan"=>"Bryan", "Bryan Co"=>"Bryan County", "Bryant"=>"Bryant", "Buchanan"=>"Buchanan Primary/Elementary", "Buford"=>"Buford", "Buford Acd"=>"Buford Academy", "Bullard"=>"Bullard", "Burdell Ht"=>"Burdell Hunt", "Burg Peter"=>"Burgess-Peterson", "Burghard"=>"Burghard", "Burnett"=>"Burnett", "Burnette"=>"Burnette", "Burnt Hkry"=>"Burnt Hickory", "Burruss"=>"Burruss", "Bush"=>"Bush", "Butler"=>"Butler", "Byron"=>"Byron", "C A Robert"=>"C A Roberts", "C L Salter"=>"C L Salter", "Cahaba Hgt"=>"Cahaba Heights", "Calera"=>"Calera", "Calhoun Ct"=>"Calhoun City", "Calhoun Cy"=>"Calhoun County", "Callaway"=>"Callaway", "Camp Creek"=>"Camp Creek", "Campbell"=>"Campbell", "Canby Lane"=>"Canby Lane", "Cannongate"=>"Cannongate", "Canton"=>"Canton", "Capitol Vw"=>"Capitol View", "Carmel"=>"Carmel", "Carrollton"=>"Carrollton", "Carter"=>"Carter", "Carver"=>"Carver", "Cary Rnold"=>"Cary Reynolds", "Cary Wds"=>"Cary Woods", "Cascade"=>"Cascade", "Cave Sprng"=>"Cave Spring", "Cb Chesney"=>"Cb Chesney", "CB Watson"=>"CB Watson/Pearl Stephens Prima", "CE Hanna"=>"CE Hanna", "Cedar Grve"=>"Cedar Grove", "Cedar Hill"=>"Cedar Hill", "Cedar Rdge"=>"Cedar Ridge", "Cent Place"=>"Centennial Place", "Centennial"=>"Centennial", "Center"=>"Center", "Center Pnt"=>"Center Point", "Centervill"=>"Centerville", "Centra Prk"=>"Central Park", "Central"=>"Central", "Central Fr"=>"Central Franklin", "Centre"=>"Centre", "Chalker"=>"Chalker", "Chalkville"=>"Chalkville", "Chapl Hill"=>"Chapel Hill", "Chapman"=>"Chapman Intermediate", "Chas A Brn"=>"Charles A Brown", "Chas F Hrd"=>"Charles F Hard", "Chas Spncr"=>"Charles Spencer", "Chase Str"=>"Chase Street", "Chat Vally"=>"Chattanooga Valley", "Chathooche"=>"Chattahoochee", "Chatsworth"=>"Chatsworth", "Chckmauga"=>"Chickamauga", "Chelse Prk"=>"Chelsea Park", "Cherokee"=>"Cherokee", "Chesnut"=>"Chesnut", "Chestatee"=>"Chestatee Primary", "Chic Woods"=>"Chicopee Woods", "Childrsbrg"=>"Childersburg", "Chrkee Rdg"=>"Cherokee Ridge", "Chsnut Mnt"=>"Chestnut Mountain", "Chtham Hil"=>"Cheatham Hill", "Church Str"=>"Church Street", "City Park"=>"City Park", "Clairemont"=>"Clairemont", "Clark Crk"=>"Clark Creek", "Clarkdale"=>"Clarkdale", "Clarkesvil"=>"Clarkesville", "Claxton"=>"Claxton", "Clay"=>"Clay", "Clay Co"=>"Clay County", "Clayton"=>"Clayton", "Clear Crk"=>"Clear Creek", "Cleburn Co"=>"Cleburne County", "Cleveland"=>"Cleveland", "ClevelandR"=>"Cleveland Road", "Clifton"=>"Clifton", "Cliftondle"=>"Cliftondale", "Clinch Co"=>"Clinch County", "Cloud Sprg"=>"Cloud Springs", "Cloverleaf"=>"Cloverleaf", "Clubview"=>"Clubview", "Clyattvill"=>"Clyattville", "Cntrlhatch"=>"Centralhatchee", "Coal Mtn"=>"Coal Mountain", "Coastl Em"=>"Coastal Empire", "Cobb"=>"Cobb", "Cogburn Wd"=>"Cogburn Woods", "Cohutta"=>"Cohutta", "Coker"=>"Coker", "Colbert"=>"Colbert", "Coldwater"=>"Coldwater", "Colham Fry"=>"Colham Ferry", "Collins"=>"Collins", "Columbia"=>"Columbia", "Comer"=>"Comer", "Commerce"=>"Commerce Primary/Elementary", "Compton"=>"Compton", "Concord"=>"Concord", "Conley Hls"=>"Conley Hills", "Connally"=>"Connally", "Constantne"=>"Constantine", "Cont Colny"=>"Continental Colony", "Cook"=>"Cook", "Cook Pr"=>"Cook Primary/Elementary", "Copeland"=>"Copeland", "Corley"=>"Corley", "Cornelia"=>"Cornelia", "Corner"=>"Corner", "County Lne"=>"County Line", "Cowan Rd"=>"Cowan Road", "Cox"=>"Cox", "CrabappCr"=>"Crabapple Crossing", "Crabapple"=>"Crabapple", "Craig"=>"Craig", "Craig Hgtn"=>"Craig Houghton", "Crawford"=>"Crawford County", "Creek View"=>"Creek View", "Creekside"=>"Creekside", "Crescnt Rd"=>"Crescent Road", "Crisp Co"=>"Crisp County Primary", "Crookd Riv"=>"Crooked River", "Cross Crk"=>"Cross Creek", "Crossville"=>"Crossville", "Crumly Chp"=>"Crumly Chapel", "Crvill ES"=>"Cartersville Primary/Elementar", "Ctn Indian"=>"Cotton Indian", "Cumming"=>"Cumming", "Cusseta Rd"=>"Cusseta Road", "Cvill Cent"=>"Carnesville-Central Franklin P", "D H Stantn"=>"D H Stanton", "D Hains"=>"Dorothy Hains", "D Height"=>"Dorothy Height", "D L Rainer"=>"David L Rainer", "Dacula"=>"Dacula", "Dade"=>"Dade", "Dadeville"=>"Dadeville", "Dallas"=>"Dallas", "Dames Fry"=>"Dames Ferry", "Danielsvl"=>"Danielsville", "Daphne"=>"Daphne", "Daphne Est"=>"Daphne East", "Daughtry"=>"Daughtry", "Daves Crk"=>"Daves Creek", "Davis"=>"Davis", "Dawnville"=>"Dawnville", "Dawson"=>"Dawson", "De Armanvl"=>"De Armanville", "Dean Road"=>"Dean Road", "Dearing"=>"Dearing", "Deer Chase"=>"Deer Chase", "Deer Vally"=>"Deer Valley", "Deerwood"=>"Deerwood Academy", "Dekalb Anx"=>"Dekalb Annex", "Delta"=>"Delta", "Demorest"=>"Demorest", "Dewar"=>"Dewar", "Diamnd Lks"=>"Diamond Lakes", "Dimon"=>"Dimon", "Dixie"=>"Dixie", "Dobbs"=>"Dobbs", "Doerun"=>"Doerun", "Dolvin"=>"Dolvin", "Donald"=>"Donald", "Dooly Co"=>"Dooly County", "Dorsett Sh"=>"Dorsett Shoals", "Double Ch"=>"Double Churches", "Doves Crk"=>"Doves Creek", "Dowell"=>"Dowell", "Downtown"=>"Downtown", "Dresden"=>"Dresden", "Due West"=>"Due West", "Dug Gap"=>"Dug Gap", "Dugan"=>"Dugan", "Dunaire"=>"Dunaire", "Dunbar"=>"Dunbar", "Duncan Crk"=>"Duncan Creek", "Dunleith"=>"Dunleith", "Dunwdy Sp"=>"Dunwoody Springs", "Dunwoody"=>"Dunwoody", "Dutchtown"=>"Dutchtown", "Dyer"=>"Dyer", "E Bouie"=>"Edward Bouie", "E Broad St"=>"East Broad Street", "E C West"=>"E C West", "E Central"=>"East Central", "E Clayton"=>"East Clayton", "E D Houstn"=>"Evelyn D Houston", "E Fannin"=>"East Fannin", "E Jackson"=>"East Jackson", "E Laurens"=>"East Laurens Primary/Elementar", "E Newton"=>"East Newton", "E Smith St"=>"East Smiths Station", "E W Kight"=>"Ethel W Kight", "E White Ac"=>"Eddie White Academy", "Eagle Rdge"=>"Eagle Ridge", "Eagle Spr"=>"Eagle Springs", "Early Co"=>"Early County", "East"=>"East", "East Lake"=>"East Lake", "East Side"=>"East Side", "Eastanolle"=>"Eastanollee", "Eastside"=>"Eastside", "Eastvalley"=>"Eastvalley", "Ebenezer"=>"Ebenezer", "Echols Co"=>"Echols County", "Edgewood"=>"Edgewood", "Edmonds"=>"Edmonds", "Elbert Co"=>"Elbert Co Primary/Elem", "Elberta"=>"Elberta", "Elcan King"=>"Elcan King", "Eldridge M"=>"Eldridge Miller", "Ellijay"=>"Ellijay Primary/Elementary", "Ellis"=>"Ellis", "Elm Street"=>"Elm Street", "Elsanor"=>"Elsanor", "Elvin Hill"=>"Elvin Hill", "Emerson"=>"Emerson", "Enota"=>"Enota", "Ephesus"=>"Ephesus", "Epic"=>"Epic", "Erwin"=>"Erwin", "Esthr Json"=>"Esther Jackson", "Eton"=>"Eton", "Euchee Crk"=>"Euchee Creek", "Euharlee"=>"Euharlee", "Evans"=>"Evans", "Evansdale"=>"Evansdale", "F L Sheltn"=>"Floyd L Shelton", "F L Stantn"=>"F L Stanton", "Fact Shoal"=>"Factory Shoals", "Fain"=>"Fain", "Fair Oaks"=>"Fair Oaks", "Fair Str"=>"Fair Street", "Fairfax"=>"Fairfax", "Fairhope"=>"Fairhope", "Fairington"=>"Fairington", "Fairmount"=>"Fairmount", "Fairview"=>"Fairview", "Fairyland"=>"Fairyland", "Falling Cr"=>"Falling Creek", "Family Crt"=>"Family Court", "Fayettvill"=>"Fayetteville", "Feldwood"=>"Feldwood", "Ferguson"=>"Ferguson", "Fernbank"=>"Fernbank", "Fickett"=>"Fickett", "Finch"=>"Finch", "Findly Oks"=>"Findley Oaks", "Five Point"=>"Five Points", "Flat Rock"=>"Flat Rock", "Flat Shoal"=>"Flat Shoals", "Flint Hill"=>"Flint Hill", "Flippen"=>"Flippen", "Flowery Br"=>"Flowery Branch", "Foley"=>"Foley", "Folkston"=>"Folkston", "Ford"=>"Ford", "Forest Oak"=>"Forest Oaks", "Forrest Rd"=>"Forrest Road", "Fountain"=>"Fountain", "Fourth Dis"=>"Fourth District", "Fowler Dr"=>"Fowler Drive", "Fox"=>"Fox", "Frank Long"=>"Frank Long", "Franklin F"=>"Franklin Forest", "Free Home"=>"Free Home", "Freedom Pk"=>"Freedom Park", "Freeman"=>"Freeman", "Freemans M"=>"Freemans Mill", "Frey"=>"Frey", "Friendship"=>"Friendship", "Frt Daniel"=>"Fort Daniel", "Fruithurst"=>"Fruithurst", "Fultondale"=>"Fultondale", "Funston"=>"Funston", "Futral Rd"=>"Futral Road", "G E Washtn"=>"George E Washington", "G Hindsman"=>"Glanton Hindsman", "G O Bailey"=>"G O Bailey", "G W Carver"=>"George W Carver", "Gadsden"=>"Gadsden", "Gaines"=>"Gaines", "Gainesvill"=>"Gainesville", "Garden Cty"=>"Garden City", "Garden Hl"=>"Garden Hills", "Garden Lks"=>"Garden Lakes", "Gardendale"=>"Gardendale", "Garrison"=>"Garrison", "GarrisonPi"=>"Garrison Pilcher", "Garrson Ml"=>"Garrison Mill", "Gentian"=>"Gentian", "Georgetown"=>"Georgetown", "Geraldine"=>"Geraldine", "Gideons"=>"Gideons", "Gilbert"=>"Gilbert", "Glanton"=>"Glanton", "Glascock"=>"Glascock County", "Glen Iris"=>"Glen Iris", "Glen Oaks"=>"Glen Oaks", "Glenn Hill"=>"Glenn Hills", "Glennville"=>"Glennville", "Glennwood"=>"Glennwood", "Glenwood"=>"Glenwood Primary", "Glyndale"=>"Glyndale", "Godley Sta"=>"Godley Station", "Golden Is"=>"Golden Isles", "Golden Sp"=>"Golden Springs", "Goodyear"=>"Goodyear", "Goshen"=>"Goshen", "Gould"=>"Gould", "Gracewood"=>"Gracewood", "Graham"=>"Graham", "Grantswood"=>"Grantswood", "Graves"=>"Graves", "Gray"=>"Gray", "Grayson"=>"Grayson", "Graysville"=>"Graysville", "Green Acre"=>"Green Acres", "Green Valy"=>"Green Valley", "Greenbrier"=>"Greenbrier", "Greensboro"=>"Greensboro", "Greenwood"=>"Greenwood", "Greer"=>"Greer", "Gresham"=>"Gresham", "Grove Park"=>"Grove Park", "Grovetown"=>"Grovetown", "Gulf Shore"=>"Gulf Shores", "Gullatt"=>"Gullatt", "Gum Spring"=>"Gum Springs", "Guyton"=>"Guyton", "Gwin"=>"Gwin", "Gwin Oaks"=>"Gwin Oaks", "H A Jones"=>"H A Jones", "H B Stroud"=>"Howard B Stroud", "Hahira"=>"Hahira", "Hal Hutchn"=>"Hal Hutchens", "Hall Kent"=>"Hall Kent", "Hambrick"=>"Hambrick", "Hamiltn Cr"=>"Hamilton Crossing", "Hamilton"=>"Hamilton", "Hamltn Hol"=>"Hamilton Holmes", "Hampton"=>"Hampton", "Hannan"=>"Hannan", "Hapeville"=>"Hapeville", "Har Garret"=>"Harry Garrett", "Harbins"=>"Harbins", "Hard Shaw"=>"Bob Harding-Shawmut", "Harmony"=>"Harmony", "Harmony Le"=>"Harmony Leland", "Harper"=>"Harper", "HarrTubman"=>"Harriet Tubman", "Hartley"=>"Hartley", "Hartwell"=>"Hartwell", "Hasty"=>"Hasty", "Haven"=>"Haven", "Haw Creek"=>"Haw Creek", "Hawthorne"=>"Hawthorne", "Hayes"=>"Hayes", "Haynie"=>"Haynie", "Hazel Grve"=>"Hazel Grove", "Head"=>"Head", "Heard"=>"Heard", "Heard Co"=>"Heard County", "Heard Mixn"=>"Heard Mixon", "Heards Fry"=>"Heards Ferry", "Helena"=>"Helena", "Hembre Spr"=>"Hembree Springs", "Hemphill"=>"Hemphill", "Hendersn M"=>"Henderson Mill", "Hendricks"=>"Hendricks", "Hephzibah"=>"Hephzibah", "Heritage"=>"Heritage", "Herndon"=>"Herndon", "Hertge Aca"=>"Heritage Academy", "Hesse"=>"Hesse", "Hickory Fl"=>"Hickory Flat", "Hickory H"=>"Hickory Hills", "Hicks"=>"Hicks", "High Point"=>"High Point", "High Shoal"=>"High Shoals", "Hightower"=>"Hightower", "Hightwr Tr"=>"Hightower Trail", "Hill City"=>"Hill City", "Hillcrest"=>"Hillcrest", "Hillside"=>"Hillside", "Hilltop"=>"Hilltop", "Hillview"=>"Hillview", "Hiram"=>"Hiram", "Hoboken"=>"Hoboken", "Hodge"=>"Hodge", "Hogansvill"=>"Hogansville", "Hollis Hnd"=>"Hollis Hand", "Holly Sprg"=>"Holly Springs", "Hollydale"=>"Hollydale", "Holsenbeck"=>"Holsenbeck", "Homebound"=>"Homebound", "Honey Crk"=>"Honey Creek", "Hood Ave"=>"Hood Avenue Primary", "Hope Hill"=>"Hope Hill", "Hopkins"=>"Hopkins", "Hornsby"=>"Hornsby", "House"=>"House", "Howard"=>"Howard", "Hubbard"=>"Hubbard", "Huddleston"=>"Huddleston", "Hudson"=>"Hudson", "Hueytown"=>"Hueytown", "Huffmn Aca"=>"Huffman Academy", "Huguley"=>"Huguley", "Huie"=>"Huie", "Hull Safrd"=>"Hull Sanford", "Humphries"=>"Humphries", "Hunt"=>"Hunt", "Huntly Hl"=>"Huntley Hills", "Hutchinson"=>"Hutchinson", "Ider"=>"Ider", "Idlewood"=>"Idlewood", "Ila"=>"Ila", "Indian Crk"=>"Indian Creek", "Indian Kn"=>"Indian Knoll", "Indian Vy"=>"Indian Valley", "Inglenook"=>"Inglenook", "Ingram Pye"=>"Ingram Pye", "Inman"=>"Inman", "Inverness"=>"Inverness", "Irondale"=>"Irondale", "Irwin Co"=>"Irwin County", "Islands"=>"Islands", "Isle Of Hp"=>"Isle Of Hope", "Ison Sprgs"=>"Ison Springs", "Ithica"=>"Ithica", "Ivy Creek"=>"Ivy Creek", "J A Alford"=>"J A Alford", "J Bacon"=>"Jordye Bacon", "J D Dickrn"=>"J D Dickerson Primary", "J G Smith"=>"Jacob G Smith", "J Johnson"=>"John Johnson", "J L Newton"=>"J Larry Newton", "J Low"=>"Juliette Low", "J Martin"=>"Joseph Martin", "J P Nix"=>"Jack P Nix Primary", "Jackson"=>"Jackson", "Jackson Hg"=>"Jackson Heights", "Jackson Rd"=>"Jackson Road", "Jamestown"=>"Jamestown", "Jasper"=>"Jasper", "Jasper Co"=>"Jasper County Primary", "Jeff Davis"=>"Jeff Davis Primary/Elementary", "JeffersnPk"=>"Jefferson Parkway", "Jefferson"=>"Jefferson", "Jeffrsnvl"=>"Jeffersonville", "Jenkins"=>"Jenkins", "Jenkins Co"=>"Jenkins County", "Jenkns Wht"=>"Jenkins White", "Jerger"=>"Jerger", "Jesup"=>"Jesup", "Jeter"=>"Jeter", "Jim Pearsn"=>"Jim Pearson", "JJ Harris"=>"JJ Harris", "Johns Crk"=>"Johns Creek", "Johnson"=>"Johnson", "Johnson Co"=>"Johnson County", "Johnston"=>"Johnston", "Jolly"=>"Jolly", "Jones"=>"Jones", "Jones Wht"=>"Jones Wheat", "Jonesboro"=>"Jonesboro", "Jordan H R"=>"Jordan Hill Road", "K Danforth"=>"K Danforth", "K Johnson"=>"Kermit Johnson", "Kanoheda"=>"Kanoheda", "Kay Road"=>"Kay Road", "KB Sutton"=>"KB Sutton", "Kedron"=>"Kedron", "Keheley"=>"Keheley", "Kelley Lk"=>"Kelley Lake", "Kelly Mill"=>"Kelly Mill", "Kemp"=>"Kemp", "Kennedy"=>"Kennedy", "Kennesaw"=>"Kennesaw", "Key"=>"Key", "Kilough"=>"Kilough", "Kilpatrick"=>"Kilpatrick", "Kimberly"=>"Kimberly", "Kincaid"=>"Kincaid", "Kinchafone"=>"Kinchafoonee Primary", "King Spr"=>"King Springs", "Kings Chpl"=>"Kings Chapel", "Kingsland"=>"Kingsland", "Kingsley"=>"Kingsley", "Kingston"=>"Kingston", "Kittredge"=>"Kittredge", "Kitty Stn"=>"Kitty Stone", "Knight"=>"Knight", "Knight En"=>"Knight Enloe", "Knollwood"=>"Knollwood", "Knox"=>"Knox", "L Cooper"=>"Lillie Cooper", "L Jackson"=>"Lonnie Jackson", "L K Moss"=>"L K Moss Primary", "Labelle"=>"Labelle", "Ladonia"=>"Ladonia", "Lafay East"=>"Lafayette Eastside", "Lafay Lanr"=>"Lafayette Lanier", "Lafayette"=>"Lafayette", "Lake City"=>"Lake City", "Lake Frst"=>"Lake Forest", "Lake Joy"=>"Lake Joy Primary/Elementary", "Lake Ocone"=>"Lake Oconee", "Lake Park"=>"Lake Park", "Lake Ridge"=>"Lake Ridge", "Lakewood"=>"Lakewood", "Lamar Co"=>"Lamar County Primary/Elementar", "Lamar Rees"=>"Lamar Reese", "LamarMill"=>"Lamar-Milledge", "Langstn Ch"=>"Langston Chapel", "Langstn Rd"=>"Langston Road", "Lanier"=>"Lanier", "Lanier Co"=>"Lanier County Primary/Elementa", "Lanier Pr"=>"Lanier Primary", "Largo Tibt"=>"Largo Tibet", "Laurel Rdg"=>"Laurel Ridge", "Lavonia"=>"Lavonia", "Lee Co"=>"Lee County Primary/Elementary", "Lee Street"=>"Lee Street", "Leeds"=>"Leeds", "Len Last"=>"Len Lastinger", "Leroy Mass"=>"Leroy Massey", "Level Crk"=>"Level Creek", "Level Grv"=>"Level Grove", "Lewis"=>"Lewis", "Lewiston"=>"Lewiston", "Lg Branch"=>"Long Branch", "Liberty"=>"Liberty", "Liberty Pk"=>"Liberty Park", "Liberty Pt"=>"Liberty Point", "Lilburn"=>"Lilburn", "Lin"=>"Lin", "Lincoln"=>"Lincoln", "Lincoln Co"=>"Lincoln County", "Lindsey"=>"Lindsey", "Linwood"=>"Linwood", "Lipscomb"=>"Lipscomb", "Lithia Spr"=>"Lithia Springs", "Live Oak"=>"Live Oak", "Livingston"=>"Livingston", "Livsey"=>"Livsey", "Lk Frst Hl"=>"Lake Forest Hills", "Lk Windwrd"=>"Lake Windward", "Loachapoka"=>"Loachapoka", "Lockheed"=>"Lockheed", "Locust Gr"=>"Locust Grove", "Loganville"=>"Loganville", "Lomax"=>"Lomax", "Long Cane"=>"Long Cane", "Lorraine"=>"Lorraine", "Lovin"=>"Lovin", "Loxley"=>"Loxley", "Ltl River"=>"Little River Primary/Elementar", "Luella"=>"Luella", "Lula"=>"Lula", "Lumpkin"=>"Lumpkin County", "LVille"=>"Lawrenceville", "LVille Ac"=>"Louisville Academy", "Lyerly"=>"Lyerly", "Lyman Hall"=>"Lyman Hall", "Lyons Co"=>"Lyons County Primary/Elementar", "M E Lewis"=>"M E Lewis", "M Gaskins"=>"Martha Gaskins", "M H Mason"=>"M H Mason", "M Harris"=>"Matilda Harris", "M L Clark"=>"Mary Lee Clark", "M L Gross"=>"Mamie Lou Gross", "M L K Jr"=>"Martin Luther King Jr", "M Lane"=>"M Lane", "M Lively"=>"Mattie Lively", "M Smith"=>"Martha Smith", "M W Holt"=>"Margaret Winn Holt", "M Wells"=>"Mattie Wells", "M Yarbrogh"=>"Margaret Yarbrough", "Mableton"=>"Mableton", "Macedonia"=>"Macedonia", "Macon Co"=>"Macon County", "Magill"=>"Magill", "Magnolia"=>"Magnolia", "Main"=>"Main", "Malcom Br"=>"Malcom Bridge", "Maning Oks"=>"Manning Oaks", "Mansfield"=>"Mansfield", "Marbut"=>"Marbut", "Marlow"=>"Marlow", "Marshall"=>"Marshall", "Marshpoint"=>"Marshpoint", "Martin"=>"Martin", "Martinez"=>"Martinez", "Mashburn"=>"Mashburn", "Mason"=>"Mason", "Mason Crk"=>"Mason Creek", "Mat Arthur"=>"Matt Arthur", "Mat Wilson"=>"Matt Wilson", "Mathews"=>"Mathews", "Matt"=>"Matt", "Maxwell"=>"Maxwell", "Maysville"=>"Maysville", "McAdory"=>"McAdory", "Mcbean"=>"Mcbean", "McCall"=>"McCall", "Mcdonough"=>"Mcdonough", "Mcever"=>"Mcever", "Mcgarity"=>"Mcgarity", "Mcgarrah"=>"Mcgarrah", "Mchenry"=>"Mchenry Primary", "Mckendree"=>"Mckendree", "Mclendon"=>"Mclendon", "Meadowbrok"=>"Meadowbrook", "Meadowcrek"=>"Meadowcreek", "Meadowlane"=>"Meadowlane", "Meadowview"=>"Meadowview", "Medlock Br"=>"Medlock Bridge", "Memrial Dr"=>"Memorial Drive", "Menlo"=>"Menlo", "Metter"=>"Metter", "Middle Rdg"=>"Middle Ridge", "Midland Ac"=>"Midland Academy", "Midvale"=>"Midvale", "Midway"=>"Midway", "MidwayPrim"=>"Midway Primary", "Miles"=>"Miles", "Milford"=>"Milford", "Mill Creek"=>"Mill Creek", "Miller"=>"Miller", "Miller Co"=>"Miller County", "Mimosa"=>"Mimosa", "Minor"=>"Minor", "Minor Comm"=>"Minor Community", "Mirror Lk"=>"Mirror Lake", "Mission Rd"=>"Mission Road", "Mnt Zion"=>"Mount Zion Primary/Elementary", "Mntville"=>"Mountville", "Model"=>"Model", "Monroe"=>"Monroe", "Montclair"=>"Montclair", "Monte Sano"=>"Monte Sano", "Montevallo"=>"Montevallo", "MontgmryCo"=>"Montgomery County", "Montgomery"=>"Montgomery", "Moon Lake"=>"Moon Lake", "Moore"=>"Moore", "Moore Str"=>"Moore Street", "Moreland"=>"Moreland", "Morgan"=>"Morgan", "Morgan Co"=>"Morgan County Primary/Elementa", "Morland Rd"=>"Moreland Road", "Morningsid"=>"Morningside", "Morris Ave"=>"Morris Avenue", "Morrow"=>"Morrow", "Mossy Cr"=>"Mossy Creek", "Moulton Br"=>"Moulton Branch", "Mt Bethel"=>"Mount Bethel", "Mt Carmel"=>"Mount Carmel", "Mt Laurel"=>"Mt Laurel", "Mt Olive"=>"Mount Olive", "Mt Vernon"=>"Mount Vernon", "Mt Yonah"=>"Mt Yonah", "Mt Zion"=>"Mount Zion", "Mtn Hill"=>"Mountain Hill", "Mtn Park"=>"Mountain Park", "Mtn Road"=>"Mountain Road", "Mtn View"=>"Mountain View", "MtOlive"=>"Mt Olive", "Mulberry"=>"Mulberry", "Mulbery Cr"=>"Mulberry Creek", "Munford"=>"Munford", "Murdock"=>"Murdock", "Murphy Can"=>"Murphy Candler", "Muscogee"=>"Muscogee", "Myers"=>"Myers", "N Brooks"=>"North Brooks", "N Columbia"=>"North Columbia", "N Columbs"=>"North Columbus Ele", "N Dodge"=>"North Dodge", "N Douglas"=>"North Douglas", "N Fayette"=>"North Fayette", "N H Stphns"=>"Nathaniel H Stephens", "N Harlem"=>"North Harlem", "N Harris"=>"Narvie Harris", "N Heights"=>"North Heights", "N Highland"=>"North Highland", "N Jackson"=>"North Jackson", "N Lafayett"=>"North Lafayette", "N Mitchell"=>"North Mitchell", "Nahunta"=>"Nahunta Primary/Elementary", "Naomi"=>"Naomi", "Nat Hills"=>"National Hills", "Nebo"=>"Nebo", "Nesbit"=>"Nesbit", "Nevils"=>"Nevils", "New Hope"=>"New Hope", "Newnan Crs"=>"Newnan Crossing", "Nicholls"=>"Nicholls", "Nicholson"=>"Nicholson", "Nickajack"=>"Nickajack", "Nolan"=>"Nolan", "Norcross"=>"Norcross", "Norman Pak"=>"Norman Park", "Norris"=>"Norris", "North Hart"=>"North Hart", "Northcutt"=>"Northcutt", "Northside"=>"Northside", "Northwest"=>"Northwest", "Northwood"=>"Northwood", "Norton"=>"Norton", "Norton Prk"=>"Norton Park", "Nunn"=>"Nunn", "Nw Georgia"=>"New Georgia", "Nw Holland"=>"New Holland", "NW Laurens"=>"Northwest Laurens", "Nw Mnchstr"=>"New Manchester", "Nw Prospct"=>"New Prospect", "Nw Quitman"=>"New Quitman County", "Oak Grove"=>"Oak Grove", "Oak Hill"=>"Oak Hill", "Oak Knoll"=>"Oak Knoll", "Oak Mnt"=>"Oak Mountain", "Oak View"=>"Oak View", "Oakcliff"=>"Oakcliff", "Oakhurst"=>"Oakhurst", "Oakland"=>"Oakland", "Oakley"=>"Oakley", "Oakwood"=>"Oakwood", "Ocee"=>"Ocee", "Oconee Co"=>"Oconee County Primary/Elementa", "Odom"=>"Odom", "Odum"=>"Odum", "Ogleth Ave"=>"Oglethorpe Avenue", "Oglethp Co"=>"Oglethorpe County Primary/Elem", "Oglethp Pt"=>"Oglethorpe Point", "Ogletree"=>"Ogletree", "Ohatchee"=>"Ohatchee", "Okapilco"=>"Okapilco", "Ola"=>"Ola", "Oliver"=>"Oliver", "Omega"=>"Omega", "Orange Bch"=>"Orange Beach", "Orrs"=>"Orrs", "Other"=>"Other", "Oxford"=>"Oxford", "Oxmoor"=>"Oxmoor", "P Mathis"=>"Parker Mathis", "Paine"=>"Paine", "Palmer St"=>"Palmer Stone", "Palmetto"=>"Palmetto", "Panola Way"=>"Panola Way", "Park"=>"Park", "Park Creek"=>"Park Creek", "Park Str"=>"Park Street", "Parklane"=>"Parklane", "Parkside"=>"Parkside", "Parkwood"=>"Parkwood", "Parsons"=>"Parsons", "Partee"=>"Partee", "Pate"=>"Pate", "Pates Cr"=>"Pates Creek", "Patrick"=>"Patrick", "Patterson"=>"Patterson", "Peachcrest"=>"Peachcrest", "Peachtree"=>"Peachtree", "Pearson"=>"Pearson", "Peeks Chp"=>"Peeks Chapel", "Peeples"=>"Peeples", "Pelham"=>"Pelham", "Pepperell"=>"Pepperell Primary/Elementary", "Perdido"=>"Perdido", "Perdue"=>"Perdue Primary/Elementary", "Perkerson"=>"Perkerson", "Perry"=>"Perry", "Peyton For"=>"Peyton Forest", "Pharr"=>"Pharr", "Phenix Cty"=>"Phenix City", "Phlips Aca"=>"Phillips Academy", "Pick"=>"Pick", "Pickett Ml"=>"Picketts Mill", "Piedmont"=>"Piedmont", "Pike Co"=>"Pike County Primary/Elementary", "Pine Grove"=>"Pine Grove", "Pine Log"=>"Pine Log", "Pine Ridge"=>"Pine Ridge", "Pine Str"=>"Pine Street", "Pinecrest"=>"Pinecrest", "Pinevale"=>"Pinevale", "Pinson"=>"Pinson", "Pitner"=>"Pitner", "Plainview"=>"Plainview", "Plsnt Grve"=>"Pleasant Grove", "Plsnt Vall"=>"Pleasant Valley", "Plsntdale"=>"Pleasantdale", "Pointe S"=>"Pointe South", "Poole"=>"Poole", "Pooler"=>"Pooler", "Poplar Rd"=>"Poplar Road", "Port Wentw"=>"Port Wentworth", "Portal"=>"Portal", "Porter"=>"Porter", "Porterdale"=>"Porterdale", "Potter St"=>"Potter Street", "Powder Spr"=>"Powder Springs", "Powers Fr"=>"Powers Ferry", "Princeton"=>"Princeton", "Providence"=>"Providence", "PTree City"=>"Peachtree City", "Puckett Ml"=>"Pucketts Mill", "Pulaski"=>"Pulaski", "Putnam Co"=>"Putnam County Primary/Elementa", "Quail Run"=>"Quail Run", "Quitman"=>"Quitman", "R E McNair"=>"Ronald E McNair", "R J Burch"=>"Robert J Burch", "R L Young"=>"Raymond L Young", "R M Moore"=>"R M Moore", "R Smith"=>"Roberta Smith", "Rabun Co"=>"Rabun County Primary/Elementar", "Rabun Gap"=>"Rabun Gap Community", "Radium Spr"=>"Radium Springs", "Rainbow"=>"Rainbow", "Ranburne"=>"Ranburne", "Randlph Pk"=>"Randolph Park", "Randolph"=>"Randolph", "RandolphCo"=>"Randolph County", "Rck Chapel"=>"Rock Chapel", "Red Bud"=>"Red Bud", "Red Oak"=>"Red Oak", "Redan"=>"Redan", "Reese Road"=>"Reese Road", "Reidsville"=>"Reidsville", "Renaisance"=>"Renaissance", "Rice"=>"Rice", "Richland"=>"Richland", "Richmond H"=>"Richmond Hill Primary/Elementa", "Ridge Road"=>"Ridge Road Prrmary/Elementary", "Ridgecrest"=>"Ridgecrest", "Rigdon Rd"=>"Rigdon Road", "Riley"=>"Riley", "Rincon"=>"Rincon", "Ringgold"=>"Ringgold Primary/Elementary", "Ritch"=>"Ritch", "River Eves"=>"River Eves", "River Rdg"=>"River Ridge", "River Road"=>"River Road", "Riverbend"=>"Riverbend", "Riverchase"=>"Riverchase", "Riverdale"=>"Riverdale", "Rivers"=>"Rivers", "Rivers Edg"=>"Rivers Edge", "Riverside"=>"Riverside", "RiversideP"=>"Riverside Primary/Elementary", "Riverview"=>"Riverview", "Roan"=>"Roan", "Roberts"=>"Roberts", "Robertsdal"=>"Robertsdale", "Robinson"=>"Robinson", "Rock Sprg"=>"Rock Spring", "Rock Sprgs"=>"Rock Springs", "Rockbridge"=>"Rockbridge", "Rockwell"=>"Rockwell", "Rocky Br"=>"Rocky Branch", "Rocky Crk"=>"Rocky Creek", "Rocky Mt"=>"Rocky Mount", "Rocky Pln"=>"Rocky Plains", "Rocky Rdg"=>"Rocky Ridge", "Roopville"=>"Roopville", "Rosebud"=>"Rosebud", "Rosemont"=>"Rosemont", "Rosinton"=>"Rosinton", "Rossville"=>"Rossville", "Roswell N"=>"Roswell North", "Rowland"=>"Rowland", "Roy Rollin"=>"Roy Rollins", "Royston"=>"Royston", "RShaw"=>"Robert Shaw", "Ruskin"=>"Ruskin", "Russell"=>"Russell", "Russom"=>"Russom", "Ruth Hill"=>"Ruth Hill", "S Columbia"=>"South Columbia", "S Columbus"=>"South Columbus", "S D Meadow"=>"Sally Dailey Meadows", "S D Panter"=>"Sam D Panter", "S Dasher"=>"Susie Dasher", "S Dodge"=>"South Dodge", "S Douglas"=>"South Douglas", "S Effinghm"=>"South Effingham", "S G A"=>"S G A", "S H Minter"=>"Sara Harp Minter", "S Hampton"=>"South Hampton", "S Jackson"=>"South Jackson", "S L Lewis"=>"S L Lewis", "S Mahone"=>"Sallas Mahone", "S Mitchell"=>"South Mitchell", "S Rabun"=>"South Rabun", "S Ragsdle"=>"Sarah Ragsdale", "S Reynolds"=>"Sue Reynolds", "S Salem"=>"South Salem", "S Shade Cr"=>"South Shades Crest", "S Striplng"=>"Susan Stripling", "S Zetterwr"=>"Sallie Zetterower", "Saddle Rdg"=>"Saddle Ridge", "Sagamor Hl"=>"Sagamore Hills", "Saks"=>"Saks", "Sand Hill"=>"Sand Hill", "Sanders"=>"Sanders", "Sandhill"=>"Sandhill", "Sarah Cobb"=>"Sarah Cobb", "Sardis"=>"Sardis", "Satilla"=>"Satilla", "Satilla Mh"=>"Satilla Marsh", "Sawnee"=>"Sawnee", "Sawyer Rd"=>"Sawyer Road", "Saxon Hght"=>"Saxon Heights", "Schley Co"=>"Schley County", "Scott"=>"Scott", "Screven"=>"Screven", "Screven Co"=>"Screven County", "Seabrn Lee"=>"Seaborn Lee", "Sedalia Pk"=>"Sedalia Park", "Seminole"=>"Seminole County", "Settles Br"=>"Settles Bridge", "Shade Cah"=>"Shades Cahaba", "Shades Mnt"=>"Shades Mountain", "Shadow Rck"=>"Shadow Rock", "Shakerag"=>"Shakerag", "Sharon"=>"Sharon", "Sharp Crk"=>"Sharp Creek", "Shelby"=>"Shelby", "Sher Acr"=>"Sherwood Acres", "Sherwood"=>"Sherwood", "Shiloh"=>"Shiloh", "Shiloh Pt"=>"Shiloh Point", "Shirley Hl"=>"Shirley Hills", "Shiver"=>"Shiver", "Shlwfrd F"=>"Shallowford Falls", "Shoal Crk"=>"Shoal Creek", "Shuman"=>"Shuman", "Silver Cty"=>"Silver City", "Silverhill"=>"Silverhill", "Simonton"=>"Simonton", "Simpson"=>"Simpson", "Sims"=>"Sims", "Sixes"=>"Sixes", "Sky View"=>"Sky View", "Skyview"=>"Skyview", "Slater"=>"Slater", "Smiley"=>"Smiley", "Smith"=>"Smith Primary/Elementary", "Smith Barn"=>"Smith Barnes", "Smoke Rise"=>"Smoke Rise", "Smyrna"=>"Smyrna", "Snapfinger"=>"Snapfinger", "Snow Roger"=>"Snow Rogers", "Social Cir"=>"Social Circle Primary/Elementa", "Sonoravill"=>"Sonoraville", "Sope Creek"=>"Sope Creek", "South Hart"=>"South Hart", "South Smth"=>"South Smiths Station", "Southeast"=>"Southeast", "Southside"=>"Southside", "Southview"=>"Southview", "Southwest"=>"Southwest", "Southwstrn"=>"Southwestern", "Sp Springs"=>"Spout Springs", "Spaldng Dr"=>"Spalding Drive", "Spansh Frt"=>"Spanish Fort", "Spencer"=>"Spencer", "Spr Hill"=>"Spring Hill", "Spr Place"=>"Spring Place", "Springdale"=>"Springdale", "Springfld"=>"Springfield Central", "Sprngdle P"=>"Springdale Park", "St Br Cros"=>"State Bridge Crossing", "St George"=>"St George", "St Marys"=>"Saint Marys", "St Mtn"=>"Stone Mountain", "St Simons"=>"St Simons", "Stapleton"=>"Stapleton", "Stark"=>"Stark", "Starling"=>"Starling", "Statham"=>"Statham", "Stemley Rd"=>"Stemley Road", "Sterling"=>"Sterling", "Stevens Cr"=>"Stevens Creek", "Stewart Co"=>"Stewart County", "Still"=>"Still", "Stilson"=>"Stilson", "Stockbridg"=>"Stockbridge", "Stone Crk"=>"Stone Creek", "Stone Mill"=>"Stone Mill", "Stoneview"=>"Stoneview", "Stonwl Tel"=>"Stonewall Tell", "Stringfllw"=>"Stringfellow", "Suder"=>"Suder", "Sugar Hill"=>"Sugar Hill", "Sugarmill"=>"Sugarmill", "Summervill"=>"Summerville", "Summit Hll"=>"Summit Hill", "Sumter Co"=>"Sumter County Primary/Elementa", "Sun Valley"=>"Sun Valley", "Sunset"=>"Sunset", "Suwanee"=>"Suwanee", "SW Laurens"=>"Southwest Laurens", "Swain"=>"Swain", "Swainsboro"=>"Swainsboro Primary/Elementary", "Sweet Appl"=>"Sweet Apple", "Sweetwater"=>"Sweetwater", "Swift Con"=>"Swift Consolidated", "Swint"=>"Swint", "Sycamore"=>"Sycamore", "Sylvania"=>"Sylvania", "Sylvstr Rd"=>"Sylvester Road", "Tadmore"=>"Tadmore", "Taliaferro"=>"Taliaferro County", "Tallapoosa"=>"Tallapoosa Primary", "Tara"=>"Tara", "Tarrant"=>"Tarrant", "Tate"=>"Tate", "Taylor"=>"Taylor", "Taylor Co"=>"Taylor County Primary/Elementa", "Taylors Cr"=>"Taylors Creek", "Taylorsvil"=>"Taylorsville", "Teasley"=>"Teasley Primary/Elementary", "Telfair Co"=>"Telfair County", "Temple"=>"Temple", "Tenth St"=>"Tenth Street", "Terrace Mn"=>"Terrace Manor", "Tg Scott"=>"Tg Scott", "Thomas Cr"=>"Thomas Crossroads", "Thomson"=>"Thomson", "Thunderblt"=>"Thunderbolt", "Tiger Cr"=>"Tiger Creek", "Timber Rdg"=>"Timber Ridge", "Timothy"=>"Timothy", "Tobacco Rd"=>"Tobacco Road", "Toccoa"=>"Toccoa", "Todd Grant"=>"Todd Grant", "Tolbert"=>"Tolbert", "Toney"=>"Toney", "Toombs Cen"=>"Toombs Central", "Toomer"=>"Toomer", "Towns"=>"Towns", "Towns Co"=>"Towns County", "Trace Crss"=>"Trace Crossings", "Treutlen"=>"Treutlen", "Trion"=>"Trion", "Trip"=>"Trip", "Tritt"=>"Tritt", "Tucker"=>"Tucker", "Tuggle"=>"Tuggle", "Tunnel Hil"=>"Tunnel Hill", "Turner"=>"Turner", "Turner Co"=>"Turner County", "Turner Wd"=>"Turner Woods", "Tussahaw"=>"Tussahaw", "Tville Hgt"=>"Thomasville Heights", "Twin City"=>"Twin City", "Twin Oaks"=>"Twin Oaks", "Tyrone"=>"Tyrone", "U Collins"=>"Ursula Collins", "U Lee Nor"=>"Upson Lee North", "U Lee Sth"=>"Upson Lee South", "Union"=>"Union", "Union Co"=>"Union County Primary/Elementar", "Union Pnt"=>"Union Point", "Unity"=>"Unity", "Unity Grve"=>"Unity Grove", "Usher"=>"Usher", "Valley"=>"Valley", "Valley Pt"=>"Valley Point", "Van Wert"=>"Van Wert", "Vanderlyn"=>"Vanderlyn", "Varnell"=>"Varnell", "Varner"=>"Varner", "Vaughan"=>"Vaughan", "Venetian H"=>"Venetian Hills", "Vickery Cr"=>"Vickery Creek", "Villa Rica"=>"Villa Rica", "Vincent"=>"Vincent", "Vnville Ac"=>"Vineville Academy", "W Bainbrdg"=>"West Bainbridge", "W Central"=>"West Central", "W Chatham"=>"West Chatham", "W Clayton"=>"West Clayton", "W End Acad"=>"West End Academy", "W Forest"=>"West Forest", "W Hralson"=>"West Haralson", "W J Carrol"=>"W J Carroll", "W J Cooper"=>"W J Cooper", "W Jeferson"=>"West Jefferson", "W L Radney"=>"William L Radney", "W Newton"=>"West Newton", "W O Lance"=>"W. O. Lance", "W Pafford"=>"Waldo Pafford", "W Sm Stat"=>"West Smiths Station", "Wacona"=>"Wacona", "Wacoochee"=>"Wacoochee", "Waddell"=>"Waddell", "Wadsworth"=>"Wadsworth", "Walker"=>"Walker", "Walker Prk"=>"Walker Park", "Walnut Crk"=>"Walnut Creek", "Walnut Grv"=>"Walnut Grove", "Waresboro"=>"Waresboro", "Warren Rd"=>"Warren Road", "Warrior"=>"Warrior", "Wash Pk"=>"Washington Park", "Wash Wilke"=>"Washington Wilkes Primary/Elem", "Washington"=>"Washington", "Wauka Mnt"=>"Wauka Mountain", "Wayn Atkin"=>"Waynesville-Atkinson Primary/E", "Wayn Blake"=>"Waynesboro-Blakeney Primary/El", "Waynesboro"=>"Waynesboro Primary", "Waynesvill"=>"Waynesville", "Wdwrd Ml"=>"Woodward Mill", "Weaver"=>"Weaver", "Webster Co"=>"Webster County", "Wedowee"=>"Wedowee", "Welch"=>"Welch", "Wellborn"=>"Wellborn", "Wesly Hght"=>"Wesley Heights", "Wesly Lake"=>"Wesley Lakes", "West"=>"West", "West End"=>"West End", "West Green"=>"West Green", "West Manor"=>"West Manor", "West Point"=>"West Point", "West Side"=>"West Side", "West Town"=>"West Town", "Westchestr"=>"Westchester", "Western"=>"Western", "Westhills"=>"Westhills", "Westmont"=>"Westmont", "Westside"=>"Westside", "Westview"=>"Westview", "Westwood"=>"Westwood", "Wheeler Co"=>"Wheeler County", "Wheelss Rd"=>"Wheeless Road", "Whigham"=>"Whigham", "Whit Davis"=>"Whit Davis", "White"=>"White", "White Bluf"=>"White Bluff", "White Co"=>"White Co Intermediate", "White Oak"=>"White Oak", "White Pln"=>"White Plains", "White Sphr"=>"White Sulphur", "Whitefoord"=>"Whitefoord", "Whitesburg"=>"Whitesburg", "Whitlow"=>"Whitlow", "Whthead Rd"=>"Whitehead Road", "Whtsvle Rd"=>"Whitesville Road", "Wilcox Co"=>"Wilcox County", "Wilkinson"=>"Wilkinson County", "Will Formn"=>"Willis Foreman", "Willacoche"=>"Willacoochee", "William Av"=>"Williams Avenue", "William He"=>"Williams Heights", "Williams"=>"Williams", "Willis Rd"=>"Willis Road", "Wills Vly"=>"Wills Valley", "Wilson Crk"=>"Wilson Creek", "Wilsonvill"=>"Wilsonville", "Windsor F"=>"Windsor Forest", "Windsor Sp"=>"Windsor Spring", "Winnona Pk"=>"Winnona Park", "Winston"=>"Winston", "Wintervill"=>"Winterville", "WJackson"=>"West Jackson", "Wlknson Gr"=>"Wilkinson Gardens", "Woodbine"=>"Woodbine", "Woodland"=>"Woodland", "WoodLawn"=>"WoodLawn", "Woodridge"=>"Woodridge", "Woodson"=>"Woodson", "Woodstaton"=>"Woodstation", "Woodstock"=>"Woodstock", "Woodville"=>"Woodville", "Woodward"=>"Woodward", "Woody Gap"=>"Woody Gap", "Worth Co"=>"Worth County Primary/Elementar", "Wrens"=>"Wrens", "Wright"=>"Wright", "Wrts Ml Rd"=>"Wrights Mill Road", "Wt Fannin"=>"West Fannin", "Wynbrooke"=>"Wynbrooke", "Wynnton"=>"Wynnton", "Yargo"=>"Yargo", "Youngs Grv"=>"Youngs Grove", "Youth"=>"Youth"},
    :mls_high_school => {"A Sumter N"=>"A Sumter No", "A Sumter S"=>"A Sumter So", "Acd Of Rmn"=>"Academy Of Richmond", "Adairsvl"=>"Adairsville", "Albany"=>"Albany", "Alcovy"=>"Alcovy", "Alexander"=>"Alexander", "Alexandria"=>"Alexandria", "Allatoona"=>"Allatoona", "Alpharetta"=>"Alpharetta", "Anniston"=>"Anniston", "Apalachee"=>"Apalachee", "Appling Co"=>"Appling County", "Arabia Mtn"=>"Arabia Mountain", "Archer"=>"Archer", "Armuchee"=>"Armuchee", "Atknson Co"=>"Atkinson County", "Auburn"=>"Auburn", "Bacon Co"=>"Bacon County", "Bainbridge"=>"Bainbridge", "Baker Co"=>"Baker County", "Baldwin"=>"Baldwin", "Baldwin Co"=>"Baldwin County", "Banks Co"=>"Banks County", "Banneker"=>"Banneker", "BB Cmr Mml"=>"BB Comer Memorial", "Beach"=>"Beach", "Beauregard"=>"Beauregard", "Berkmar"=>"Berkmar", "Berrien"=>"Berrien", "Beulah"=>"Beulah", "Blckley Co"=>"Bleckley County", "Bnjmn Rsl"=>"Benjamin Russell", "Bowdon"=>"Bowdon", "Brdwll Ins"=>"Bradwell Institute", "Bremen"=>"Bremen", "Brntley Co"=>"Brantley County", "Brooks Co"=>"Brooks County", "Brookwood"=>"Brookwood", "Brunswick"=>"Brunswick", "Bryan Co"=>"Bryan County", "Bshop Hall"=>"Bishop Hall", "Bssmr City"=>"Bessemer City", "Buford"=>"Buford", "Burke Co"=>"Burke County", "C Sct King"=>"Coretta Scott King", "Cairo"=>"Cairo", "Calera Hgh"=>"Calera High", "Calhoun Co"=>"Calhoun County", "Callaway"=>"Callaway", "Cambridge"=>"Cambridge", "Camden Co"=>"Camden County", "Campbell"=>"Campbell", "Carrollton"=>"Carrollton", "Cartersvl"=>"Cartersville", "Carver"=>"Carver", "Cass"=>"Cass", "Cdr Shls"=>"Cedar Shoals", "Cedar Blff"=>"Cedar Bluff", "Cedar Grv"=>"Cedar Grove", "Cedartown"=>"Cedartown", "Centennial"=>"Centennial", "Center Pt"=>"Center Point", "Central"=>"Central", "Chamblee"=>"Chamblee", "Chattooga"=>"Chattooga", "Chelsea"=>"Chelsea", "Cherokee"=>"Cherokee", "Chestatee"=>"Chestatee", "Chldersbrg"=>"Childersburg", "Chpel Hill"=>"Chapel Hill", "Chrlton Co"=>"Charlton County", "Chrokee Co"=>"Cherokee County", "Chttahchee"=>"Chattahoochee", "Chulla Crk"=>"Coahulla Creek", "Clarkston"=>"Clarkston", "Claxton"=>"Claxton", "Clburne Co"=>"Cleburne County", "Clhoun Cty"=>"Calhoun City", "Clinch Co"=>"Clinch County", "Cllins Hll"=>"Collins Hill", "Clmbs Mgnt"=>"Columbus Magnet", "Clquitt Co"=>"Colquitt County", "Clrke Ctrl"=>"Clarke Central", "Clssc City"=>"Classic City", "Cly-Chlkvl"=>"Clay-Chalkville", "Coffee Co"=>"Coffee County", "Collinsvl"=>"Collinsville", "Columbia"=>"Columbia", "Commerce"=>"Commerce", "Cook Co"=>"Cook County", "Coosa"=>"Coosa", "Corner"=>"Corner", "Creekside"=>"Creekside", "Creekview"=>"Creekview", "Crim"=>"Crim", "Crisp Co"=>"Crisp County", "Cross Crk"=>"Cross Creek", "Cross Keys"=>"Cross Keys", "Crossroads"=>"Crossroads", "Crossville"=>"Crossville", "Crver Mgnt"=>"Carver Magnet", "Crwford Co"=>"Crawford County", "Dacula"=>"Dacula", "Dade Co"=>"Dade County", "Dadeville"=>"Dadeville", "Dalton"=>"Dalton", "Daphne"=>"Daphne", "Dawson Co"=>"Dawson County", "Decatur"=>"Decatur", "Discovery"=>"Discovery", "Dklb Annex"=>"Dekalb Annex", "Dklb Arts"=>"Dekalb Arts", "Dodge Co"=>"Dodge County", "Dooly Co"=>"Dooly County", "Dougherty"=>"Dougherty", "Douglas Co"=>"Douglas County", "Douglass"=>"Douglass", "Drew"=>"Drew", "Druid Hlls"=>"Druid Hills", "Dublin"=>"Dublin", "Duluth"=>"Duluth", "Dunwoody"=>"Dunwoody", "Dutchtown"=>"Dutchtown", "E County"=>"Early County", "E Coweta"=>"East Coweta", "E Jksn Cmp"=>"East Jackson Comp", "E Laurens"=>"East Laurens", "E Paulding"=>"East Paulding", "East Hall"=>"East Hall", "Eastside"=>"Eastside", "Echols Co"=>"Echols County", "Effnghm Co"=>"Effingham County", "Egls Lndng"=>"Eagles Landing", "Elbert Co"=>"Elbert County", "Elzbt Adws"=>"Elizabeth Andrews", "Eml Cty In"=>"Emanuel County Inst", "Etowah"=>"Etowah", "Evans"=>"Evans", "Fairhope"=>"Fairhope", "Family Co"=>"Family Court", "Fannin Co"=>"Fannin County", "Fayette Co"=>"Fayette County", "Fayettevl"=>"Fayetteville", "Fitzgerald"=>"Fitzgerald", "Flwry Brnc"=>"Flowery Branch", "Foley"=>"Foley", "Forest Pk"=>"Forest Park", "Fort Payne"=>"Fort Payne", "Frnklin Co"=>"Franklin County", "Frsth Cntl"=>"Forsyth Central", "Fultondale"=>"Fultondale", "Fyffe"=>"Fyffe", "G Wash Cvr"=>"George Washington Carver", "Gainesvl"=>"Gainesville", "Gardendale"=>"Gardendale", "Gateway"=>"Gateway", "Gaylesvl"=>"Gaylesville", "Geraldine"=>"Geraldine", "Gilmer"=>"Gilmer", "Glnn Hills"=>"Glenn Hills", "Glscock Co"=>"Glascock County", "Glynn Acd"=>"Glynn Academy", "Gordon Lee"=>"Gordon Lee", "Grady"=>"Grady", "Grayson"=>"Grayson", "Grdon Cntl"=>"Gordon Central", "Greenbrier"=>"Greenbrier", "Greene Co"=>"Greene County", "Greenville"=>"Greenville", "Grge Btlr"=>"George Butler", "Griffin"=>"Griffin", "Groves"=>"Groves", "Grovetown"=>"Grovetown", "Gulf Shrs"=>"Gulf Shores", "Hampton"=>"Hampton", "Handley"=>"Handley", "Hardaway"=>"Hardaway", "Harlem"=>"Harlem", "Harris Co"=>"Harris County", "Harrison"=>"Harrison", "Hart Co"=>"Hart County", "Hawkinsvl"=>"Hawkinsville", "Hayes"=>"Hayes", "Hbrshm Ctl"=>"Habersham Central", "Heard Co"=>"Heard County", "Henry Co"=>"Henry County", "Hephzibah"=>"Hephzibah", "Heritage"=>"Heritage", "Hffmn Mgnt"=>"Huffman Magnet", "Hillgrove"=>"Hillgrove", "Hiram"=>"Hiram", "Hncck Cntl"=>"Hancock Central", "Homebound"=>"Homebound", "Homewood"=>"Homewood", "Hoover"=>"Hoover", "Houston Co"=>"Houston County", "Howard"=>"Howard", "Hrlson Co"=>"Haralson County", "Hrsshoe Bd"=>"Horseshoe Bend", "Hueytown"=>"Hueytown", "Hutchings"=>"Hutchings", "Hwt-Trsvle"=>"Hewitt-Trussville", "Ider"=>"Ider", "Indpndnce"=>"Independence", "Irwin Co"=>"Irwin County", "Islands"=>"Islands", "Jackson"=>"Jackson", "Jackson Co"=>"Jackson County", "Jacksonvl"=>"Jacksonville", "Jasper Co"=>"Jasper County", "Jckson-Oln"=>"Jackson-Olin", "Jeff Davis"=>"Jeff Davis", "Jefferson"=>"Jefferson", "Jenkins"=>"Jenkins", "Jenkins Co"=>"Jenkins County", "Jffrson Co"=>"Jefferson County", "Johns Crk"=>"Johns Creek", "Johnson"=>"Johnson", "Johnson Co"=>"Johnson County", "Jones Co"=>"Jones County", "Jonesboro"=>"Jonesboro", "Jordan"=>"Jordan", "Josey"=>"Josey", "Kell"=>"Kell", "Kendrick"=>"Kendrick", "Knnsaw Mtn"=>"Kennesaw Mountain", "Lafayette"=>"Lafayette", "Lagrange"=>"Lagrange", "Lakeside"=>"Lakeside", "Lamar Co"=>"Lamar County", "Lambert"=>"Lambert", "Lanett Sr"=>"Lanett Senior", "Lanier"=>"Lanier", "Lanier Co"=>"Lanier County", "Lassiter"=>"Lassiter", "Lee County"=>"Lee County", "Leeds"=>"Leeds", "Lgstn Hghs"=>"Langston Hughes", "Liberty Co"=>"Liberty County", "Lincoln"=>"Lincoln", "Lincoln Co"=>"Lincoln County", "Lithonia"=>"Lithonia", "Lkvw Ogltp"=>"Lakeview Ft.Oglethorpe", "Loachapoka"=>"Loachapoka", "Locust Grv"=>"Locust Grove", "Loganville"=>"Loganville", "Long Co"=>"Long County", "Lovejoy"=>"Lovejoy", "Lowndes"=>"Lowndes", "Lthia Spgs"=>"Lithia Springs", "Lucy Laney"=>"Lucy Laney", "Luella"=>"Luella", "Macon Co"=>"Macon County", "Madison Co"=>"Madison County", "Magnolia"=>"Magnolia", "Manchester"=>"Manchester", "Marietta"=>"Marietta", "Marion"=>"Marion", "Mays"=>"Mays", "McAdory"=>"McAdory", "McClarin"=>"McClarin", "Mceachern"=>"Mceachern", "Mcintosh"=>"Mcintosh", "Mcnair"=>"Mcnair", "Mcntsh Acd"=>"Mcintosh Academy", "Meadowcrk"=>"Meadowcreek", "Metter"=>"Metter", "MH Jksn Jr"=>"MH Jackson Jr", "Mill Creek"=>"Mill Creek", "Miller Co"=>"Miller County", "Miller Grv"=>"Miller Grove", "Milton"=>"Milton", "Minor"=>"Minor", "MLK Jr"=>"Martin Luther King Jr", "Mndys Mill"=>"Mundys Mill", "Mnroe Area"=>"Monroe Area", "Mntgmry Co"=>"Montgomery County", "Model"=>"Model", "Monroe"=>"Monroe", "Montevallo"=>"Montevallo", "Morgan Co"=>"Morgan County", "Morrow"=>"Morrow", "Mount Zion"=>"Mount Zion", "Mrrs Invtv"=>"Morris Innovative", "Mrtmr Jrdn"=>"Mortimer Jordan", "Mry Prsons"=>"Mary Persons", "Mtchell Co"=>"Mitchell County", "Mtn View"=>"Mountain View", "Munford"=>"Munford", "Murray Co"=>"Murray County", "N Atl"=>"North Atlanta", "N Clayton"=>"North Clayton", "N Forsyth"=>"North Forsyth", "N Gwinnett"=>"North Gwinnett", "N Murray"=>"North Murray", "N Oconee"=>"North Oconee", "N Pauldng"=>"North Paulding", "N Springs"=>"North Springs", "NE Tift"=>"North East Tift", "New Hmpstd"=>"New Hampstead", "New Mncstr"=>"New Manchester", "Newnan"=>"Newnan", "Newton"=>"Newton", "Norcross"=>"Norcross", "North Cobb"=>"North Cobb", "North Hall"=>"North Hall", "Northeast"=>"Northeast", "Northgate"=>"Northgate", "Northside"=>"Northside", "Northview"=>"Northview", "Nw Lmkn Co"=>"New Lumpkin County", "NW Whtfld"=>"Northwest Whitfield", "Oak Grove"=>"Oak Grove", "Oak Mtn"=>"Oak Mountain", "Oconee Co"=>"Oconee County", "Oglthrp Co"=>"Oglethorpe County", "Ohatchee"=>"Ohatchee", "Ola"=>"Ola", "Opelika"=>"Opelika", "Osborne"=>"Osborne", "Other"=>"Other", "Oxford"=>"Oxford", "Parker"=>"Parker", "Parkview"=>"Parkview", "Pchtr. Rdg"=>"Peachtree Ridge", "Peach Co"=>"Peach County", "Pebblebrk"=>"Pebblebrook", "Pelham"=>"Pelham", "Pepperell"=>"Pepperell", "Perry"=>"Perry", "Phoenix"=>"Phoenix", "Pickens Co"=>"Pickens County", "Piedmont"=>"Piedmont", "Pierce Co"=>"Pierce County", "Pike Co"=>"Pike County", "Pinson Vly"=>"Pinson Valley", "Plainview"=>"Plainview", "Plding Co"=>"Paulding County", "Plsnt Grv"=>"Pleasant Grove", "Plsnt Vly"=>"Pleasant Valley", "Pope"=>"Pope", "Portal"=>"Portal", "Ptrck Hnry"=>"Patrick Henry", "Putnam Co"=>"Putnam County", "Rabun Co"=>"Rabun County", "Ramsay"=>"Ramsay", "Ranburne"=>"Ranburne", "Rchmnd Hll"=>"Richmond Hill", "Rckdle Co"=>"Rockdale County", "Redan"=>"Redan", "Reeltown"=>"Reeltown", "Ridgeland"=>"Ridgeland", "Ringgold"=>"Ringgold", "River Rdg"=>"River Ridge", "Riverdale"=>"Riverdale", "Riverwood"=>"Riverwood", "Rndlph Cly"=>"Randolph Clay", "Rndolph Co"=>"Randolph County", "Robertsdle"=>"Robertsdale", "Rockmart"=>"Rockmart", "Rome"=>"Rome", "Roswell"=>"Roswell", "Russell Co"=>"Russell County", "Rutland"=>"Rutland", "S Atlanta"=>"South Atlanta", "S Effngham"=>"South Effingham", "S Forsyth"=>"South Forsyth", "S Gwinnett"=>"South Gwinnett", "S Paulding"=>"South Paulding", "Saks"=>"Saks", "Salem"=>"Salem", "Sand Rock"=>"Sand Rock", "Sandy Crk"=>"Sandy Creek", "Savannah"=>"Savannah", "Schley Co"=>"Schley County", "Screven Co"=>"Screven County", "SE Bulloch"=>"Southeast Bulloch", "SE Whtfld"=>"Southeast Whitfield", "Semnle Co"=>"Seminole County", "Sequoyah"=>"Sequoyah", "Shades Vly"=>"Shades Valley", "Shaw"=>"Shaw", "Shelby Co"=>"Shelby County", "Shiloh"=>"Shiloh", "Smths Sttn"=>"Smiths Station", "Social Cir"=>"Social Circle", "Sonorav'le"=>"Sonoraville", "South Cobb"=>"South Cobb", "Southwest"=>"Southwest", "Spalding"=>"Spalding", "Span Fort"=>"Spanish Fort", "Spencer"=>"Spencer", "Sprayberry"=>"Sprayberry", "Spring Gdn"=>"Spring Garden", "Statesboro"=>"Statesboro", "Stephenson"=>"Stephenson", "Stewart Co"=>"Stewart County", "Stockbridg"=>"Stockbridge", "Stone Mtn"=>"Stone Mountain", "Stphns Co"=>"Stephens County", "Strrs Mill"=>"Starrs Mill", "Stwrt Qtmn"=>"Stewart Quitman", "Summerdale"=>"Summerdale", "SW Dekalb"=>"Southwest Dekalb", "Swainsboro"=>"Swainsboro", "Sylacauga"=>"Sylacauga", "Talladega"=>"Talladega", "Tarrant"=>"Tarrant", "Tattnal Co"=>"Tattnall County", "Taylor Co"=>"Taylor County", "Telfair Co"=>"Telfair County", "Temple"=>"Temple", "Terrell Co"=>"Terrell County", "Therrell"=>"Therrell", "Thomas Co"=>"Thomas County", "Thomasv'le"=>"Thomasville", "Thomson"=>"Thomson", "Tift Co"=>"Tift County", "Tlferro Co"=>"Taliaferro County", "Toombs Co"=>"Toombs County", "Towers"=>"Towers", "Towns Co"=>"Towns County", "Treutlen"=>"Treutlen", "Tri Cities"=>"Tri Cities", "Trion"=>"Trion", "Troup Co"=>"Troup County", "Tucker"=>"Tucker", "Turner Co"=>"Turner County", "Twiggs Co"=>"Twiggs County", "Union Co"=>"Union County", "Union Grv"=>"Union Grove", "Upson Lee"=>"Upson Lee", "Valdosta"=>"Valdosta", "Valley"=>"Valley", "Vdla Comp"=>"Vidalia Comprehensive", "Veterans"=>"Veterans", "Villa Rica"=>"Villa Rica", "Vly Head"=>"Valley Head", "Vncnt Mdl"=>"Vincent Middle", "Vstvia Hls"=>"Vestavia Hills", "W'dvl Tom."=>"Woodville Tompkins", "W. Forsyth"=>"West Forsyth", "W. Laurens"=>"West Laurens", "Wadley"=>"Wadley", "Walnut Grv"=>"Walnut Grove", "Walton"=>"Walton", "Ware Co"=>"Ware County", "Warren Co"=>"Warren County", "Wash. Co"=>"Washington County", "Wash. Wlks"=>"Washington Wilkes", "Washington"=>"Washington", "Wayne Co"=>"Wayne County", "Wdlwn Mgnt"=>"Woodlawn Magnet", "Weaver"=>"Weaver", "Webster"=>"Webster", "Wellborn"=>"Wellborn", "Wenonah"=>"Wenonah", "West Hall"=>"West Hall", "Westlake"=>"Westlake", "Westover"=>"Westover", "Westside"=>"Westside", "Wheeler"=>"Wheeler", "Wheeler Co"=>"Wheeler County", "White Co"=>"White County", "White Plns"=>"White Plains", "Whitewater"=>"Whitewater", "Wilcox Co"=>"Wilcox County", "Winterboro"=>"Winterboro", "Wlknson Co"=>"Wilkinson County", "Wnder Brrw"=>"Winder Barrow", "Wndsr Frst"=>"Windsor Forest", "Woodland"=>"Woodland", "Woods Mill"=>"Wood's Mill", "Woodstock"=>"Woodstock", "Woody Gap"=>"Woody Gap", "Worth Co"=>"Worth County", "Wrnr Rbns"=>"Warner Robins", "Wylam"=>"Wylam"},
    :mls_middle_school => {"A C Crews"=>"Alton C Crews", "A W Carson"=>"Anita White Carson", "A Z Kelsey"=>"A Z Kelsey", "Aaron Cohn"=>"Aaron Cohn", "Adairsvill"=>"Adairsville", "Adamson"=>"Adamson", "Albany"=>"Albany", "Alex Cty"=>"Alexander City", "Anniston"=>"Anniston", "Appling"=>"Appling", "Appling Co"=>"Appling County", "Ar William"=>"Arthur Williams", "Armuchee"=>"Armuchee", "Arnall"=>"Arnall", "Arnold"=>"Arnold", "Arrington"=>"Arrington", "Ashworth"=>"Ashworth", "Atkinsn Co"=>"Atkinson County", "Auburn"=>"Auburn", "Austin"=>"Austin", "Austin Rd"=>"Austin Road", "Autrey Mil"=>"Autrey Milll", "Awtrey"=>"Awtrey", "B H Lyons"=>"Burney Harris Lyons", "Babb"=>"Babb", "Bacon Co"=>"Bacon County", "Bagley"=>"Bagley", "Bainbridge"=>"Bainbridge", "Baker"=>"Baker", "Baker Co"=>"Baker County", "Bal Huds"=>"Ballard Hudson", "Banks Co"=>"Banks County", "Barber"=>"Barber", "Bartlett"=>"Bartlett", "Bay Creek"=>"Bay Creek", "Bay Minett"=>"Bay Minette", "Bay Spring"=>"Bay Springs", "Bear Creek"=>"Bear Creek", "Ben Hill"=>"Ben Hill County", "Benetts Ml"=>"Bennetts Mill", "Berkmar"=>"Berkmar", "Berrien"=>"Berrien", "Bessmr Cty"=>"Bessemer City", "Best Acad"=>"Best Academy", "Bethune"=>"Bethune", "Blackmon"=>"Blackmon Road", "Bleckley"=>"Bleckley County", "Bloomfield"=>"Bloomfield", "Bnks Stevn"=>"Banks Stevens", "Bonaire"=>"Bonaire", "Booth"=>"Booth", "Bottenfeld"=>"Bottenfield", "Bowdon"=>"Bowdon", "Bragg"=>"Bragg", "Brantley"=>"Brantley County", "Bremen"=>"Bremen Middle", "Brighton"=>"Brighton", "Brocks Gp"=>"Brock's Gap", "Brooks Co"=>"Brooks County", "Brown"=>"Brown", "Bryan Co"=>"Bryan County", "Buford"=>"Buford", "Bunche"=>"Bunche", "Burke Co"=>"Burke County", "Bush"=>"Bush", "Byron"=>"Byron", "C Ellis K8"=>"C Ellis K8", "C S King"=>"Coretta Scott King", "C W Davis"=>"C W Davis", "Calera"=>"Calera Middle", "Calhoun Co"=>"Calhoun County", "Callaway"=>"Callaway", "Camden"=>"Camden", "Camp Creek"=>"Camp Creek", "Campbell"=>"Campbell", "Carrollton"=>"Carrollton", "Cartrsvill"=>"Cartersville", "Carver"=>"Carver", "Carver Rd"=>"Carver Road", "Cass"=>"Cass", "CE Hanna"=>"CE Hanna", "Cedar Grov"=>"Cedar Grove", "Cedartown"=>"Cedartown", "Cen Baldwn"=>"Central Baldwin", "Central"=>"Central", "Centre"=>"Centre", "Ch R Drew"=>"Charles R Drew", "Chamblee"=>"Chamblee", "Champion"=>"Champion", "Chapel Hil"=>"Chapel Hill", "Chat Vly"=>"Chattanooga Valley", "Chatthoche"=>"Chattahoochee County", "Chelsea"=>"Chelsea", "Chestatee"=>"Chestatee", "Childrsbrg"=>"Childersburg", "Chstnt Log"=>"Chestnut Log", "Clarke"=>"Clarke", "Claxton"=>"Claxton", "Clay Chalk"=>"Clay-Chalkville", "Clay Co"=>"Clay County", "Clear Crk"=>"Clear Creek", "Cleburne"=>"Cleburne County", "Clements"=>"Clements", "Clhoun Cty"=>"Calhoun City", "Clifton Rg"=>"Clifton Ridge", "Clinch Co"=>"Clinch County", "Coan"=>"Coan", "Coastal"=>"Coastal", "Coffee"=>"Coffee", "Coile"=>"Coile", "Collins"=>"Collins", "Columbia"=>"Columbia", "Columbiana"=>"Columbiana", "Commerce"=>"Commerce", "Conyers"=>"Conyers", "Cook Co"=>"Cook County", "Cooper"=>"Cooper", "Coosa"=>"Coosa", "Corner"=>"Corner", "Couch"=>"Couch", "Councill"=>"Councill", "Cousins"=>"Cousins", "Cowan Road"=>"Cowan Road", "Crabapple"=>"Crabapple", "Crawford"=>"Crawford County", "Creekland"=>"Creekland", "Crisp Co"=>"Crisp County", "Crossroads"=>"Crossroads", "Crossville"=>"Crossville", "Dacula"=>"Dacula", "Dade"=>"Dade", "Dalton"=>"Dalton", "Daniell"=>"Daniell", "Daphne"=>"Daphne", "Dawson Co"=>"Dawson County", "Dbl Church"=>"Double Churches", "Dean Rusk"=>"Dean Rusk", "Derenne"=>"Derenne", "Dickerson"=>"Dickerson", "Dkalb Anx"=>"Dekalb Annex", "Dodge Co"=>"Dodge County", "Dodgen"=>"Dodgen", "Dooly Co"=>"Dooly County", "Dougherty"=>"Dougherty", "Drake"=>"Drake", "Druid Hill"=>"Druid Hills", "Dublin"=>"Dublin", "Duluth"=>"Duluth", "Durham"=>"Durham", "Dutchtown"=>"Dutchtown", "E Broad St"=>"E Broad St K8", "E Cobb"=>"East Cobb", "E Columbus"=>"East Columbus", "E Coweta"=>"East Coweta", "E F Garrsn"=>"E. F. Garrison K8", "E Jackson"=>"East Jackson", "E Laurens"=>"East Laurens", "E Lee Co"=>"East Lee County", "E Paulding"=>"East Paulding", "E W Acad"=>"Eddie White Academy", "Eagles Ld"=>"Eagles Landing", "Early Co"=>"Early County", "East Hall"=>"East Hall", "Eastbrook"=>"Eastbrook", "Ebenezer"=>"Ebenezer", "Echols Co"=>"Echols County", "Eddy"=>"Eddy", "Edwards"=>"Edwards", "Effingham"=>"Effingham County", "Eighth St"=>"Eighth Street", "Elbert Co"=>"Elbert County", "Elberta"=>"Elberta", "Elkins Pnt"=>"Elkins Pointe", "Elsanor"=>"Elsanor", "Emanuel"=>"Emanuel County Inst", "Erwin"=>"Erwin", "Evans"=>"Evans", "F N Osborn"=>"Frank N Osborne", "Factry Shl"=>"Factory Shoals", "Fairhope"=>"Fairhope", "Fairplay"=>"Fairplay", "Fannin Co"=>"Fannin County", "Fayette"=>"Fayette", "Feagin Mil"=>"Feagin Mill", "Five Forks"=>"Five Forks", "Flat Rock"=>"Flat Rock", "Floyd"=>"Floyd", "Foley"=>"Foley", "Forest H"=>"Forest Hills", "Forest Prk"=>"Forest Park", "Fort"=>"Fort", "Fort Payne"=>"Fort Payne", "Franklin"=>"Franklin County", "Freedom"=>"Freedom", "Frt Valley"=>"Fort Valley", "G C Jones"=>"Glenn C Jones", "Gainesvill"=>"Gainesville", "Gardner Nw"=>"Gardner Newman", "Garrett"=>"Garrett", "Georgetown"=>"Georgetown K8", "Geraldine"=>"Geraldine", "Gilmer"=>"Gilmer", "Gladden"=>"Gladden", "Glascock"=>"Glascock County", "Glenn Hill"=>"Glenn Hills", "Glennville"=>"Glennville", "Glynn"=>"Glynn", "Gn R Davis"=>"Gen Ray Davis", "Godley St"=>"Godley Station K8", "Gordon Lee"=>"Gordon Lee", "Grace Snl"=>"Grace Snell", "Gray"=>"Gray", "Gray Stat"=>"Gray Station", "Green Acre"=>"Green Acres", "Greenbrier"=>"Greenbrier", "Greenville"=>"Greenville", "Griffin"=>"Griffin", "Grovetown"=>"Grovetown", "Gulf Shr"=>"Gulf Shores", "H A Wilbnk"=>"H A Wilbanks", "H Jones"=>"Herschel Jones", "H Morris"=>"Haymon Morris", "H W Hesse"=>"H W Hesse K8", "Hahira"=>"Hahira", "Hampton"=>"Hampton", "Hancock"=>"Hancock Central", "Handley"=>"Handley", "Haralsn Co"=>"Haralson County", "Harlem"=>"Harlem", "Harpr Arch"=>"Harper Archer", "Harris Co"=>"Harris County Carver", "Hart Co"=>"Hart County", "Haynes Br"=>"Haynes Bridge", "Heard Co"=>"Heard County", "Helena Mid"=>"Helena Middle", "Henagar"=>"Henagar", "Henderson"=>"Henderson", "Henry Co"=>"Henry County", "Hephzibah"=>"Hephzibah", "Heritage"=>"Heritage", "HewTruss"=>"Hewitt-Trussville", "Hightwr Tr"=>"Hightower Trail", "Hilsman"=>"Hilsman", "Holcomb Br"=>"Holcomb Bridge", "Homewood"=>"Homewood", "Hopewell"=>"Hopewell", "Hornsby K8"=>"Hornsby K8", "Howard"=>"Howard", "Hubbard"=>"Hubbard", "Hubert"=>"Hubert", "Hudson"=>"Hudson", "Hueytown"=>"Hueytown", "Huffman"=>"Huffman", "Huntington"=>"Huntington", "Hutto"=>"Hutto", "I F Simm"=>"Ira F Simmons", "Ider"=>"Ider", "Indian Cr"=>"Indian Creek", "Inglenook"=>"Inglenook", "Inman"=>"Inman", "Irondale"=>"Irondale", "Irwin Co"=>"Irwin County", "Isle of Hp"=>"Isle of Hope", "J A Dobbin"=>"J A Dobbins", "J L Newton"=>"J Larry Newton", "J M Tutt"=>"John M Tutt", "J P Powell"=>"John P Powell", "J R Trippe"=>"J R Trippe", "J Richards"=>"J Richards", "Jane Macon"=>"Jane Macon", "Jasper"=>"Jasper", "Jasper Co"=>"Jasper County", "Jeff Davis"=>"Jeff Davis", "Jefferson"=>"Jefferson", "Jenkins Co"=>"Jenkins County", "Johnson Co"=>"Johnson County", "Jones Vly"=>"Jones Valley", "Jonesboro"=>"Jonesboro", "Jordan"=>"Jordan", "Kendrick"=>"Kendrick", "Kennedy"=>"Kennedy", "Kennedy Rd"=>"Kennedy Road", "King"=>"King", "Kings Brg"=>"Kings Bridge", "L Frasier"=>"Lewis Frasier", "L Radloff"=>"Louise Radloff", "Lafayette"=>"Lafayette", "Lakeside"=>"Lakeside", "Lakeview"=>"Lakeview", "Lamar Co"=>"Lamar County", "Lanett"=>"Lanett", "Langford"=>"Langford", "Langstn Ch"=>"Langston Chapel", "Lanier"=>"Lanier", "Lanier Co"=>"Lanier County", "Lee"=>"Lee", "Leeds"=>"Leeds", "Liberty"=>"Liberty", "Liberty Pk"=>"Liberty Park", "Lilburn"=>"Lilburn", "Lincoln Co"=>"Lincoln County", "Lindley"=>"Lindley", "Lithonia"=>"Lithonia", "Littl Mill"=>"Little Mill", "Locust Grv"=>"Locust Grove", "Loganville"=>"Loganville", "Long"=>"Long", "Long Cane"=>"Long Cane", "Long Co"=>"Long County", "Lost Mtn"=>"Lost Mountain", "Louisville"=>"Louisville", "Lovejoy"=>"Lovejoy", "Lovinggood"=>"Lovinggood", "Lowndes"=>"Lowndes", "Luella"=>"Luella", "Lumpkin Co"=>"Lumpkin County", "M M Bethun"=>"Mary Mcleod Bethune", "M Puckett"=>"Martha Puckett", "Mabry"=>"Mabry", "Macon Co"=>"Macon County", "Madison Co"=>"Madison County", "Madras"=>"Madras", "Malcom Br"=>"Malcom Bridge", "Manchester"=>"Manchester", "Marietta"=>"Marietta", "Marion Co"=>"Marion County", "Marshall"=>"Marshall", "Mason Cr"=>"Mason Creek", "McAdory"=>"McAdory", "Mccleskey"=>"Mccleskey", "McClure"=>"McClure", "Mcconnell"=>"Mcconnell", "Mcintsh Co"=>"Mcintosh County", "Mcintyr Pk"=>"Macintyre Park", "Mcnair"=>"Mcnair", "Memorial"=>"Memorial", "Mercer"=>"Mercer", "Merry Acre"=>"Merry Acres", "Metter"=>"Metter", "Midland"=>"Midland", "Midway"=>"Midway", "Mill Creek"=>"Mill Creek", "Miller Co"=>"Miller County", "Miller Grv"=>"Miller Grove", "Miller Mag"=>"Miller Magnet", "Mitchel Co"=>"Mitchell County", "Model"=>"Model", "Monroe Co"=>"Monroe County", "Montevallo"=>"Montevallo", "Montgomery"=>"Montgomery County", "Moon Lake"=>"Moon Lake", "Moore"=>"Moore", "Morgan Co"=>"Morgan County", "Morgan Rd"=>"Morgan Road", "Morrow"=>"Morrow", "Moses"=>"Moses", "Moss Creek"=>"Mossy Creek", "Mt Olive"=>"Mount Olive", "Mt Zion"=>"Mt Zion", "Mundy Mill"=>"Mundys Mill", "Munford"=>"Munford", "Murphey"=>"Murphey", "Myers"=>"Myers", "N Clayton"=>"North Clayton", "N Forsyth"=>"North Forsyth", "N Gwinnett"=>"North Gwinnett", "N Habershm"=>"North Habersham", "N Jeffersn"=>"North Jefferson", "N Whitfeld"=>"North Whitfield", "Needwood"=>"Needwood", "New Hope"=>"New Hope", "Newbern"=>"Newbern", "NHall"=>"North Hall", "Nich Lawsn"=>"Nichols-Lawson", "NorthBrook"=>"NorthBrook", "Northside"=>"Northside", "Northwstrn"=>"Northwestern", "Nw Quit Co"=>"New Quitman County", "O W Mitchl"=>"Ossie Ware Mitchell", "Oak Hill"=>"Oak Hill", "Oak Mnt"=>"Oak Mountain", "Oconee C"=>"Oconee County", "Oglthorpe"=>"Oglethorpe County", "Ola"=>"Ola", "Opelika"=>"Opelika", "Other"=>"Other", "Otwell"=>"Otwell", "Oxford"=>"Oxford", "P D West"=>"Paul D West", "P Henry"=>"Patrick Henry", "P.B. Ritch"=>"P.B. Ritch", "Palmer"=>"Palmer", "Parks"=>"Parks", "Peachtree"=>"Peachtree", "Pelham Cty"=>"Pelham City", "Pepperell"=>"Pepperell", "Perry"=>"Perry", "Phenix Cty"=>"Phenix City", "Phlips Aca"=>"Phillips Academy", "Pickens Co"=>"Pickens County", "Piedmont"=>"Piedmont", "Pierce Co"=>"Pierce County", "Pike Co"=>"Pike County", "Pine Grove"=>"Pine Grove", "Pine Hill"=>"Pine Hill", "Pine Mnt"=>"Pine Mountain", "Piney Grv"=>"Piney Grove", "Pizitz"=>"Pizitz", "Plainview"=>"Plainview", "Plesant Gr"=>"Pleasant Grove", "Pnckneyvil"=>"Pinckneyville", "Pointe S"=>"Pointe South", "Portal"=>"Portal", "Price"=>"Price", "Pulaski Co"=>"Pulaski County", "Putnam Co"=>"Putnam County", "R A Cross"=>"Robert A Cross", "R F Bumpus"=>"Robert F Bumpus", "Rabun Co"=>"Rabun County", "Radium Spr"=>"Radium Springs", "Rand Clay"=>"Randolph Clay", "Rchard Hll"=>"Richard Hull", "Red Bud"=>"Red Bud", "Redan"=>"Redan", "Rehobth Rd"=>"Rehoboth Road", "Reidsville"=>"Reidsville", "Renfroe"=>"Renfroe", "Renisance"=>"Renaissance", "Rex Mill"=>"Rex Mill", "Rice Creek"=>"Rice Creek", "Richards"=>"Richards", "Richmond H"=>"Richmond Hill", "Ridgeview"=>"Ridgeview", "Ringgold"=>"Ringgold", "Rising St"=>"Rising Starr", "Risley"=>"Risley", "River Trl"=>"River Trail", "Riverchase"=>"Riverchase", "Riverdale"=>"Riverdale", "Riverside"=>"Riverside", "Riverview"=>"Riverview", "Riverwatch"=>"Riverwatch", "Roberts"=>"Roberts", "Rock Mills"=>"Rock Mills", "Rockmart"=>"Rockmart", "Rome"=>"Rome", "Rosinton"=>"Rosinton", "Rossville"=>"Rossville", "Rothschild"=>"Rothschild", "Rudd"=>"Rudd", "Ruhuma"=>"Ruhuma", "Russell"=>"Russell", "Russell Co"=>"Russell County", "Rutland"=>"Rutland", "S Central"=>"South Central", "S Efingham"=>"South Effingham", "S Forsyth"=>"South Forsyth", "S Girard"=>"South Girard", "S Habershm"=>"South Habersham", "S Hampton"=>"South Hampton", "Saddle Rdg"=>"Saddle Ridge", "Saks"=>"Saks", "Salem"=>"Salem", "Sandtown"=>"Sandtown", "Sanford"=>"Sanford", "Schley Co"=>"Schley County", "Scoggins"=>"Scoggins", "Screven Co"=>"Screven County", "SE Bulloch"=>"Southeast Bulloch", "Sego"=>"Sego", "Seminole"=>"Seminole County", "Sequoyah"=>"Sequoyah", "Shiloh"=>"Shiloh", "Silverhill"=>"Silverhill", "Simpson"=>"Simpson", "Smith"=>"Smith", "Smitha"=>"Smitha", "Smiths Sta"=>"Smiths Station", "Smokey Rd"=>"Smokey Road", "Sndy Sprg"=>"Sandy Springs", "Snellville"=>"Snellville", "Snelson Gl"=>"Snelson Golden", "Social Cir"=>"Social Circle", "Sonoravill"=>"Sonoraville East", "South Hall"=>"South Hall", "Southside"=>"Southside", "Southwest"=>"Southwest", "Spanish Ft"=>"Spanish Fort", "SPaulding"=>"South Paulding", "Spirit Cr"=>"Spirit Creek", "St Marys"=>"Saint Marys", "St Mtn"=>"Stone Mountain", "Staley"=>"Staley", "Stallings"=>"Stallings Island", "Stapleton"=>"Stapleton", "Stephen Co"=>"Stephens County", "Stephenson"=>"Stephenson", "Stewart"=>"Stewart", "Stewart Co"=>"Stewart County", "Stockbrdge"=>"Stockbridge", "Summerdale"=>"Summerdale", "Summerour"=>"Summerour", "Summervill"=>"Summerville", "Sumter Co"=>"Sumter County", "Sutton"=>"Sutton", "Swainsboro"=>"Swainsboro", "Sweetwater"=>"Sweetwater", "Sylvan Hil"=>"Sylvan Hills", "Sylvania"=>"Sylvania", "T J Elder"=>"T J Elder", "Taliaferro"=>"Taliaferro County", "Tapp"=>"Tapp", "Tarrant"=>"Tarrant", "Taylor Co"=>"Taylor County", "Taylor Rd"=>"Taylor Road", "Teasley"=>"Teasley", "Telfair Co"=>"Telfair County", "Temple"=>"Temple", "Terrell Co"=>"Terrell County", "Thom McDuf"=>"Thomson-McDuffie", "Thomson"=>"Thomson", "Toombs Co"=>"Toombs County", "Towns Co"=>"Towns County", "Treutlen"=>"Treutlen", "Trickum"=>"Trickum", "Trion"=>"Trion", "Tucker"=>"Tucker", "Turner"=>"Turner", "Turner Co"=>"Turner County", "Twiggs Co"=>"Twiggs County", "Twin River"=>"Twin Rivers", "Union Co"=>"Union County", "Union Grv"=>"Union Grove", "unty"=>"Thomas County", "Upson Lee"=>"Upson Lee", "Valdosta"=>"Valdosta", "Valley Pt"=>"Valley Point", "Veterans M"=>"Veterans Memorial", "Vickery Cr"=>"Vickery Creek", "Villa Rica"=>"Villa Rica", "W Chatham"=>"West Chatham", "W F Burns"=>"W F Burns", "W J Carrol"=>"W J Carroll", "W J Will"=>"Willie J Williams", "W Jackson"=>"West Jackson", "W James"=>"William James", "W Laurens"=>"West Laurens", "Walker Mid"=>"Walker Middle", "Ware Co"=>"Ware County", "Warner Rob"=>"Warner Robins", "Warren Co"=>"Warren County", "WashingtnW"=>"Washington Wilkes", "Washington"=>"Washington", "Waycross"=>"Waycross", "WE Putnam"=>"WE Putnam Magnet", "Weaver"=>"Weaver", "Webb Br"=>"Webb Bridge", "Webster Co"=>"Webster County", "Wedowee"=>"Wedowee", "West Lee"=>"West Lee County", "Westside"=>"Westside", "Wheeler Co"=>"Wheeler County", "White Co"=>"White County", "White Pl"=>"White Plains", "Whitewater"=>"Whitewater", "Wilcox Co"=>"Wilcox County", "Wilkerson"=>"Wilkerson", "Wilkinson"=>"Wilkinson County", "Winder Bar"=>"Winder Barrow", "Woodland"=>"Woodland", "Woodstock"=>"Woodstock", "Woody Gap"=>"Woody Gap", "Worth Co"=>"Worth County", "Wrens"=>"Wrens", "Wt Hall"=>"West Hall", "Wylam"=>"Wylam", "Yeager"=>"Yeager", "Young"=>"Young", "Youth Mid"=>"Youth Middle", "Zora Ellis"=>"Zora Ellis"},
    # :new_construction => {"71"=>"Aluminum/Vinyl", "72"=>"Brick 3 Sided", "73"=>"Brick 4 Sided", "74"=>"Brick Front", "75"=>"Brick/Frame", "76"=>"Concrete Siding", "77"=>"Log", "78"=>"Other (See Remarks)", "79"=>"Press Board Siding", "80"=>"Rough-Sawn Siding", "81"=>"Steel Frame", "82"=>"Stone", "83"=>"Stone/Frame", "84"=>"Stucco EIFS", "85"=>"Stucco Real", "86"=>"Stucco Unspecified", "87"=>"Tabby", "88"=>"Wood Siding"},
    # :pool_area => {"135"=>"Balcony", "136"=>"Barn", "137"=>"Deck/Patio", "138"=>"Dog Run/Pen", "139"=>"Fenced Yard", "140"=>"Garden Area", "141"=>"Gas Grill", "142"=>"Gazebo", "143"=>"Green House", "144"=>"Hot Tub", "145"=>"Other (See Remarks)", "146"=>"Out Building", "147"=>"Outdoor Kitchen", "148"=>"Pool House", "149"=>"Pool Screened/Enclosed", "150"=>"Pool-above Ground", "151"=>"Pool-in Ground", "152"=>"Porch", "153"=>"Screen Porch", "154"=>"Sprinkler System", "155"=>"Stables", "156"=>"Tennis Court", "157"=>"Veranda", "158"=>"Water Feature", "159"=>"Workshop"},
    # :pool_private => {"135"=>"Balcony", "136"=>"Barn", "137"=>"Deck/Patio", "138"=>"Dog Run/Pen", "139"=>"Fenced Yard", "140"=>"Garden Area", "141"=>"Gas Grill", "142"=>"Gazebo", "143"=>"Green House", "144"=>"Hot Tub", "145"=>"Other (See Remarks)", "146"=>"Out Building", "147"=>"Outdoor Kitchen", "148"=>"Pool House", "149"=>"Pool Screened/Enclosed", "150"=>"Pool-above Ground", "151"=>"Pool-in Ground", "152"=>"Porch", "153"=>"Screen Porch", "154"=>"Sprinkler System", "155"=>"Stables", "156"=>"Tennis Court", "157"=>"Veranda", "158"=>"Water Feature", "159"=>"Workshop"},
    :category => {"1" => "single_family",
                  "2" => "townhouse_condo",
                  "3" => "multi_family",
                  "4" => "lots",
                  "5" => "commercial",
                  "6" => "ranch_acerage",
                  "7" => "rental",
                  "8" => "rental"
                  },
    :rental_class => {"2221"=>"mobile_home", "2222"=>"multi_family", "2223"=>"townhouse_condo", "2224"=>"single_family"},
    :roof_type => {"328"=>"Composition", "329"=>"Concrete", "330"=>"Copper", "331"=>"Metal/Steel", "332"=>"Other (See Remarks)", "333"=>"Slate", "334"=>"Tar/Gravel", "335"=>"Tile", "336"=>"Tin", "337"=>"Wood Shingle", "791"=>"Composition", "792"=>"Concrete", "793"=>"Copper", "794"=>"Metal/Steel", "795"=>"Other (See Remarks)", "796"=>"Slate", "797"=>"Tar/Gravel", "798"=>"Tile", "799"=>"Tin", "800"=>"Wood Shingle", "2225"=>"Composition", "2226"=>"Concrete", "2227"=>"Copper", "2228"=>"Metal/Steel", "2229"=>"Other (See Remarks)", "2230"=>"Slate", "2231"=>"Tar/Gravel", "2232"=>"Tile", "2233"=>"Tin", "2234"=>"Wood Shingle"},
    # :rooms => {"338"=>"Bonus Room", "339"=>"Den", "340"=>"Dining Rm/Living Rm Combo", "341"=>"Dining Room - L Shaped", "342"=>"Dining Room Seats 12+", "343"=>"DR - Separate", "344"=>"Exercise Room", "345"=>"Family Room", "346"=>"Great Room", "347"=>"In-law Suite or Apartment", "348"=>"Library/Office", "349"=>"Loft", "350"=>"LR Separate", "351"=>"Master On Main Level", "352"=>"Other (See Remarks)", "353"=>"Rec Room", "354"=>"Roommate Plan", "355"=>"Solarium/Sun Room", "356"=>"Split Bedroom Plan", "357"=>"Stubbed Bath", "358"=>"Theater/Media Room", "359"=>"Wine Cellar", "801"=>"Bonus Room", "802"=>"Den", "803"=>"Dining Rm/Living Rm Combo", "804"=>"Dining Room - L Shaped", "805"=>"Dining Room Seats 12+", "806"=>"DR - Separate", "807"=>"Exercise Room", "808"=>"Family Room", "809"=>"Great Room", "810"=>"In-law Suite or Apartment", "811"=>"Library/Office", "812"=>"Loft", "813"=>"LR Separate", "814"=>"Master On Main Level", "815"=>"Other (See Remarks)", "816"=>"Rec Room", "817"=>"Roommate Plan", "818"=>"Solarium/Sun Room", "819"=>"Split Bedroom Plan", "820"=>"Stubbed Bath", "821"=>"Theater/Media Room", "822"=>"Wine Cellar", "2235"=>"Bonus Room", "2236"=>"Den", "2237"=>"Dining Rm/Living Rm Combo", "2238"=>"Dining Room - L Shaped", "2239"=>"Dining Room Seats 12+", "2240"=>"DR - Separate", "2241"=>"Exercise Room", "2242"=>"Family Room", "2243"=>"Great Room", "2244"=>"In-law Suite or Apartment", "2245"=>"Library/Office", "2246"=>"Loft", "2247"=>"LR Separate", "2248"=>"Master On Main Level", "2249"=>"Other (See Remarks)", "2250"=>"Rec Room", "2251"=>"Roommate Plan", "2252"=>"Solarium/Sun Room", "2253"=>"Split Bedroom Plan", "2254"=>"Stubbed Bath", "2255"=>"Theater/Media Room", "2256"=>"Wine Cellar"},
    :showing_instructions => {"360"=>"24 Hour Access", "361"=>"9 AM to 10 PM", "362"=>"Appointment Agent", "363"=>"Appointment Owner", "364"=>"Appointment Tenant", "365"=>"Call Agent Only", "366"=>"Caution Alarm", "367"=>"Caution Pet", "368"=>"Courtesy Call - Leave Msg", "369"=>"Day Sleeper", "370"=>"Key In Office", "371"=>"Lockbox GAMLS Compatible", "372"=>"Lockbox Non-GAMLS Compat", "373"=>"See Remarks", "374"=>"Special Hours", "375"=>"Under Construction", "376"=>"Vacant", "823"=>"24 Hour Access", "824"=>"9 AM to 10 PM", "825"=>"Appointment Agent", "826"=>"Appointment Owner", "827"=>"Appointment Tenant", "828"=>"Call Agent Only", "829"=>"Caution Alarm", "830"=>"Caution Pet", "831"=>"Courtesy Call - Leave Msg", "832"=>"Day Sleeper", "833"=>"Key In Office", "834"=>"Lockbox GAMLS Compatible", "835"=>"Lockbox Non-GAMLS Compat", "836"=>"See Remarks", "837"=>"Special Hours", "838"=>"Under Construction", "839"=>"Vacant", "2257"=>"24 Hour Access", "2258"=>"9 AM to 10 PM", "2259"=>"Appointment Agent", "2260"=>"Appointment Owner", "2261"=>"Appointment Tenant", "2262"=>"Call Agent Only", "2263"=>"Caution Alarm", "2264"=>"Caution Pet", "2265"=>"Courtesy Call - Leave Msg", "2266"=>"Day Sleeper", "2267"=>"Key In Office", "2268"=>"Lockbox GAMLS Compatible", "2269"=>"Lockbox Non-GAMLS Compat", "2270"=>"See Remarks", "2271"=>"Special Hours", "2272"=>"Under Construction", "2273"=>"Vacant"},
    
    # :state => {"AB"=>"AB", "AK"=>"AK", "AL"=>"AL", "AR"=>"AR", "AZ"=>"AZ", "BC"=>"BC", "CA"=>"CA", "CO"=>"CO", "CT"=>"CT", "DC"=>"DC", "DE"=>"DE", "FL"=>"FL", "GA"=>"GA", "HI"=>"HI", "IA"=>"IA", "ID"=>"ID", "IL"=>"IL", "IN"=>"IN", "KS"=>"KS", "KY"=>"KY", "LA"=>"LA", "MA"=>"MA", "MB"=>"MB", "MD"=>"MD", "ME"=>"ME", "MI"=>"MI", "MN"=>"MN", "MO"=>"MO", "MS"=>"MS", "MT"=>"MT", "NB"=>"NB", "NC"=>"NC", "ND"=>"ND", "NE"=>"NE", "NH"=>"NH", "NJ"=>"NJ", "NL"=>"NL", "NM"=>"NM", "NS"=>"NS", "NT"=>"NT", "NU"=>"NU", "NV"=>"NV", "NY"=>"NY", "OH"=>"OH", "OK"=>"OK", "ON"=>"ON", "OR"=>"OR", "PA"=>"PA", "PE"=>"PE", "QC"=>"QC", "RI"=>"RI", "SC"=>"SC", "SD"=>"SD", "SK"=>"SK", "TN"=>"TN", "TX"=>"TX", "UT"=>"UT", "VA"=>"VA", "VT"=>"VT", "WA"=>"WA", "WI"=>"WI", "WV"=>"WV", "WY"=>"WY", "YT"=>"YT"},
    :status => {"1"=>"active", 
                "2"=>"sold", 
                "3"=>"pending", 
                "4"=>"expired", 
                "5"=>"withdrawn", 
                "6"=>"sold"},    
    :new_stories => {"393" => "1",
        "394" => "1.5",
        "395" => "2",
        "396" => "3",
        "397" => "2",
        "398" => "1",
        "399" => "1",
        "856" => "1",
        "857" => "1.5",
        "858" => "2",
        "859" => "3",
        "860" => "2",
        "861" => "1",
        "862" => "1",
        "2274" => "1",
        "2275" => "1.5",
        "2276" => "2",
        "2277" => "3",
        "2278" => "2",
        "2279" => "1",
        "2280" => "1"},
    :new_style_of_building => {"400"=>"Adirondack", "401"=>"A-frame", "402"=>"Bungalow/Cottage", "403"=>"Cabin", "404"=>"Cape Cod", "405"=>"Cluster", "406"=>"Colonial", "407"=>"Contemporary", "408"=>"Country/Rustic", "409"=>"Craftsman", "410"=>"European", "411"=>"French Provincial", "412"=>"Mediterranean", "413"=>"Mobile Home", "414"=>"Modular Home", "415"=>"Other (See Remarks)", "416"=>"Ranch", "417"=>"Traditional", "418"=>"Tudor", "419"=>"Victorian", "863"=>"A-frame", "864"=>"Bungalow/Cottage", "865"=>"Cape Cod", "866"=>"Cluster", "867"=>"Colonial", "868"=>"Contemporary", "869"=>"Country/Rustic", "870"=>"Craftsman", "871"=>"European", "872"=>"French Provincial", "873"=>"Garden/Patio Home", "874"=>"High Rise", "875"=>"Loft", "876"=>"Mediterranean", "877"=>"Mid Rise", "878"=>"Other (See Remarks)", "879"=>"Ranch", "880"=>"Traditional", "881"=>"Tudor", "882"=>"Victorian", "2281"=>"Adirondack", "2282"=>"A-frame", "2283"=>"Bungalow/Cottage", "2284"=>"Cabin", "2285"=>"Cape Cod", "2286"=>"Cluster", "2287"=>"Colonial", "2288"=>"Contemporary", "2289"=>"Country/Rustic", "2290"=>"Craftsman", "2291"=>"European", "2292"=>"French Provincial", "2293"=>"Garden/Patio Home", "2294"=>"High Rise", "2295"=>"Loft", "2296"=>"Mediterranean", "2297"=>"Mid Rise", "2298"=>"Mobile Home", "2299"=>"Modular Home", "2300"=>"Other (See Remarks)", "2301"=>"Ranch", "2302"=>"Traditional", "2303"=>"Tudor", "2304"=>"Victorian"},
    :water_sewer => {"432"=>"Community Well", "433"=>"Low Flow Fixtures", "434"=>"Private Sewer", "435"=>"Private Water", "436"=>"Public Water", "437"=>"Septic Tank", "438"=>"Sewer Connected", "439"=>"Sewer In Street", "440"=>"Well", "900"=>"Community Well", "901"=>"Low Flow Fixtures", "902"=>"Private Sewer", "903"=>"Private Water", "904"=>"Public Water", "905"=>"Septic Tank", "906"=>"Sewer Connected", "907"=>"Sewer In Street", "908"=>"Well", "2322"=>"Community Well", "2323"=>"Low Flow Fixtures", "2324"=>"Private Sewer", "2325"=>"Private Water", "2326"=>"Public Water", "2327"=>"Septic Tank", "2328"=>"Sewer Connected", "2329"=>"Sewer In Street", "2330"=>"Well"}
    
  }

  

  DATA_ENUMS = {
    # category: hash_from_csv('categories'),
    # lot_use: hash_from_csv('lot_uses'),
    # rental_class: hash_from_csv('rental_classes'),
    # rental_type: hash_from_csv('rental_types'),
    # single_family_type: hash_from_csv('single_family_types'),
    # status: hash_from_csv('statuses'),
    # state: hash_from_csv('states'),
    # hoa_bill_freq: hash_from_csv('hoa_bill_freqs')
    # mls_area_name: hash_from_csv('mls_areas'),
    # mls_elementary_school: hash_from_csv('mls_elementary_schools'),
    # mls_middle_school: hash_from_csv('mls_middle_schools'),
    # mls_high_school: hash_from_csv('mls_high_schools')
  }
  DATA_MAPPING = {
    acreage: 'L_NumAcres', 
    annual_maintenance_fee: 'LM_Int4_4', 
    back_on_market: proc { |d| to_date_sql(d['LM_DateTime_6']) }, 
    basement_desc: proc {|d| [d["LFD_Basement_3"],d["LFD_Basement_37"],d[""],d["LFD_Basement_161"]].reject(&:blank?).join('') },
       # 'LFD_Basement_3', 
    bathrooms_full: 'LM_Int4_5', 
    bathrooms_half: 'LM_Int4_6', 
    beds: 'LM_Int4_7', 
    buyer_agent_compensation: 'LM_Dec_10', 
    cdom: 'L_DOM', 
    city: 'L_City', 
    closed_date: proc { |d|  to_date_sql(d['L_ClosingDate'])  },
    cooling_system_type: proc {|d| [d["LFD_CoolingType_7"],d["LFD_CoolingType_42"],d[""],d["LFD_CoolingType_166"]].reject(&:blank?).join('') }, 
    
    # 'LFD_CoolingType_7', 
    county: 'LM_Char10_3', 
    dom: 'L_DOMLS', 
    equipment: proc { |d| [d["LFD_KitchenEquipment_18"],d["LFD_KitchenEquipment_53"],d[""],d["LFD_KitchenEquipment_176"]].reject(&:blank?).join('') }, 
    
    # 'LFD_KitchenEquipment_18', 
    expiration_date: proc { |d|  to_date_sql(d['L_ExpirationDate'])  },    
    exterior_type: proc { |d| [d["LFD_Construction_5"],d["LFD_Construction_40"],d[""],d["LFD_Construction_164"]].reject(&:blank?).join('') },
     # 'LFD_Construction_5', 
    financing_available: 'L_HowSold', 
    fireplace_number: 'LM_Int1_1', 
    garage_attach: proc { |d|
        ret = []
        retu = nil
        
        field = [d["LFD_Parking_23"],d["LFD_Parking_59"],d[""],d["LFD_Parking_182"]].reject(&:blank?).join('')

        unless field.blank?
          field.split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              ite = nil
              its =  {"287"=>"Y", 
                      "288"=>"Y", 
                      "289"=>"Y", 
                      "290"=>"N", 
                      "291"=>"Y", 
                      "292"=>"N", 
                      "293"=>"N", 
                      "294"=>"Y", 
                      "295"=>"Y", 
                      "296"=>"Y", 
                      "2636"=>"N", 
                      "297"=>"N", 
                      "298"=>"N", 
                      "2637"=>"N", 
                      "299"=>"N", 
                      "300"=>"N", 
                      "301"=>"N", 
                      "302"=>"N", 
                      "303"=>"N",
                      "747" => "Y",
                      "748" => "Y",
                      "749" => "Y",
                      "750" => "N",
                      "751" => "Y",
                      "752" => "N",
                      "753" => "N",
                      "754" => "Y",
                      "755" => "Y",
                      "756" => "Y",
                      "757" => "N",
                      "758" => "N",
                      "759" => "N",
                      "760" => "N",
                      "761" => "N",
                      "762" => "N",
                      "763" => "N",
                      "764" => "N",
                      "765" => "N",
                      "766" => "N",
                      "2194" => "Y",
                      "2195" => "Y",
                      "2196" => "Y",
                      "2197" => "N",
                      "2198" => "Y",
                      "2199" => "N",
                      "2200" => "N",
                      "2201" => "Y",
                      "2202" => "Y",
                      "2203" => "Y",
                      "2204" => "N",
                      "2205" => "N",
                      "2206" => "N",
                      "2630" => "N",
                      "2631" => "N",
                      "2207" => "N",
                      "2208" => "N",
                      "2209" => "N",
                      "2210" => "N",
                      "2211" => "N"} 
              ite = its[item]
              ret << ite if ite
            rescue
              ret = []
            end
            
          }

          Delayed::Worker.logger.debug ">>ret item: #{ret}"
          rsl = 0
          if (ret && (ret.length > 0))
            rs = ret.reject do |x| x.in?(['N']) end
            if (rs && (rs.length > 0))
              retu = 'Y'
            else
              retu = 'N'
            end
          else
            retu = nil
          end
        end
        retu
      },
    # 'LFD_Parking_23', 
    garage_capacity:  proc { |d|
        ret = []
        retu = nil
        field = [d["LFD_Parking_23"],d["LFD_Parking_59"],d[""],d["LFD_Parking_182"]].reject(&:blank?).join('')
        unless field.blank?
          field.split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              ite = nil
              its =  {"287"=>"1", 
                      "288"=>"2", 
                      "289"=>"3", 
                      "290"=>"0", 
                      "291"=>"0", 
                      "292"=>"0", 
                      "293"=>"0", 
                      "294"=>"0", 
                      "295"=>"0", 
                      "296"=>"0", 
                      "2636"=>"0", 
                      "297"=>"0", 
                      "298"=>"0", 
                      "2637"=>"0", 
                      "299"=>"0", 
                      "300"=>"0", 
                      "301"=>"0", 
                      "302"=>"0", 
                      "303"=>"0",
                      "747" => "1",
                      "748" => "2",
                      "749" => "3",
                      "750" => "0",
                      "751" => "0",
                      "752" => "0",
                      "753" => "0",
                      "754" => "0",
                      "755" => "0",
                      "756" => "0",
                      "757" => "0",
                      "758" => "0",
                      "759" => "0",
                      "760" => "0",
                      "761" => "0",
                      "762" => "0",
                      "763" => "0",
                      "764" => "0",
                      "765" => "0",
                      "766" => "0",
                      "2194" => "1",
                      "2195" => "2",
                      "2196" => "3",
                      "2197" => "0",
                      "2198" => "0",
                      "2199" => "0",
                      "2200" => "0",
                      "2201" => "0",
                      "2202" => "0",
                      "2203" => "0",
                      "2204" => "0",
                      "2205" => "0",
                      "2206" => "0",
                      "2630" => "0",
                      "2631" => "0",
                      "2207" => "0",
                      "2208" => "0",
                      "2209" => "0",
                      "2210" => "0",
                      "2211" => "0"}

              ite = its[item]
              ret << ite.to_i if (ite && ite.to_s.to_i >= 0)
            rescue
              ret = []
            end
            
          }

          rsl = 0
          if (ret && (ret.length > 0))
            ret.each { |rt|
              rsl = rsl + rt 
            }
            rsl.to_s
          else
            nil
          end
        end
        
      },
    # 'LFD_Parking_23', 
    gated_community: proc { |d|
        ret = []
        retu = nil
        field = [d["LFD_Amenities_2"],d["LFD_Amenities_35"],d["LFD_Amenities_106"],d["LFD_Amenities_160"]].reject(&:blank?).join('')
        unless field.blank?
          field.split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              ite = nil
              its =  {"13"=>"N", 
                      "14"=>"N", 
                      "15"=>"N", 
                      "16"=>"Y", 
                      "17"=>"N", 
                      "18"=>"N", 
                      "19"=>"N", 
                      "20"=>"N", 
                      "21"=>"N", 
                      "22"=>"N", 
                      "23"=>"N", 
                      "24"=>"N", 
                      "25"=>"N", 
                      "26"=>"N", 
                      "27"=>"N", 
                      "28"=>"N", 
                      "29"=>"N", 
                      "30"=>"N", 
                      "31"=>"N", 
                      "32"=>"N", 
                      "33"=>"N", 
                      "34"=>"N", 
                      "35"=>"N", 
                      "36"=>"N", 
                      "37"=>"N", 
                      "38"=>"N", 
                      "39"=>"N",  
                      "13" => "N",
                      "14" => "N",
                      "15" => "N",
                      "16" => "Y",
                      "17" => "N",
                      "18" => "N",
                      "19" => "N",
                      "20" => "N",
                      "21" => "N",
                      "22" => "N",
                      "23" => "N",
                      "24" => "N",
                      "25" => "N",
                      "26" => "N",
                      "27" => "N",
                      "28" => "N",
                      "29" => "N",
                      "30" => "N",
                      "31" => "N",
                      "32" => "N",
                      "33" => "N",
                      "34" => "N",
                      "35" => "N",
                      "36" => "N",
                      "37" => "N",
                      "38" => "N",
                      "39" => "N",
                      "453" => "N",
                      "454" => "N",
                      "455" => "N",
                      "456" => "Y",
                      "457" => "N",
                      "458" => "N",
                      "459" => "N",
                      "460" => "N",
                      "461" => "N",
                      "462" => "N",
                      "463" => "N",
                      "464" => "N",
                      "465" => "N",
                      "466" => "N",
                      "467" => "N",
                      "468" => "N",
                      "469" => "N",
                      "470" => "N",
                      "471" => "N",
                      "472" => "N",
                      "473" => "N",
                      "474" => "N",
                      "475" => "N",
                      "476" => "N",
                      "477" => "N",
                      "478" => "N",
                      "479" => "N",
                      "1237" => "N",
                      "1238" => "N",
                      "1239" => "N",
                      "1240" => "Y",
                      "1241" => "N",
                      "1242" => "N",
                      "1243" => "N",
                      "1244" => "N",
                      "1245" => "N",
                      "1246" => "N",
                      "1247" => "N",
                      "1248" => "N",
                      "1249" => "N",
                      "1250" => "N",
                      "1251" => "N",
                      "1252" => "N",
                      "1253" => "N",
                      "1254" => "N",
                      "1255" => "N",
                      "1256" => "N",
                      "1257" => "N",
                      "1258" => "N",
                      "1259" => "N",
                      "1260" => "N",
                      "1261" => "N",
                      "1262" => "N",
                      "1263" => "N",
                      "1909" => "N",
                      "1910" => "N",
                      "1911" => "N",
                      "1912" => "Y",
                      "1913" => "N",
                      "1914" => "N",
                      "1915" => "N",
                      "1916" => "N",
                      "1917" => "N",
                      "1918" => "N",
                      "1919" => "N",
                      "1920" => "N",
                      "1921" => "N",
                      "1922" => "N",
                      "1923" => "N",
                      "1924" => "N",
                      "1925" => "N",
                      "1926" => "N",
                      "1927" => "N",
                      "1928" => "N",
                      "1929" => "N",
                      "1930" => "N",
                      "1931" => "N",
                      "1932" => "N",
                      "1933" => "N",
                      "1934" => "N",
                      "1935" => "N"}
              ite = its[item]
              ret << ite if ite
            rescue
              ret = []
            end
            
          }

          Delayed::Worker.logger.debug ">>ret item: #{ret}"
          rsl = 0
          if (ret && (ret.length > 0))
            rs = ret.reject do |x| x.in?(['N']) end
            if (rs && (rs.length > 0))
              retu = 'Y'
            else
              retu = 'N'
            end
          else
            retu = nil
          end
        end
        retu
      },
     # 'LFD_Amenities_2', 
    golf_course:   proc { |d|
        ret = []
        retu = nil
        field = [d["LFD_Amenities_2"],d["LFD_Amenities_35"],d["LFD_Amenities_106"],d["LFD_Amenities_160"]].reject(&:blank?).join('')
        unless field.blank?
          field.split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              ite = nil
              its =  {"13"=>"N", 
                      "14"=>"N", 
                      "15"=>"N", 
                      "16"=>"N", 
                      "17"=>"Y", 
                      "18"=>"N", 
                      "19"=>"N", 
                      "20"=>"N", 
                      "21"=>"N", 
                      "22"=>"N", 
                      "23"=>"N", 
                      "24"=>"N", 
                      "25"=>"N", 
                      "26"=>"N", 
                      "27"=>"N", 
                      "28"=>"N", 
                      "29"=>"N", 
                      "30"=>"N", 
                      "31"=>"N", 
                      "32"=>"N", 
                      "33"=>"N", 
                      "34"=>"N", 
                      "35"=>"N", 
                      "36"=>"N", 
                      "37"=>"N", 
                      "38"=>"N", 
                      "39"=>"N", 
                      "13" => "N",
                      "14" => "N",
                      "15" => "N",
                      "16" => "N",
                      "17" => "Y",
                      "18" => "N",
                      "19" => "N",
                      "20" => "N",
                      "21" => "N",
                      "22" => "N",
                      "23" => "N",
                      "24" => "N",
                      "25" => "N",
                      "26" => "N",
                      "27" => "N",
                      "28" => "N",
                      "29" => "N",
                      "30" => "N",
                      "31" => "N",
                      "32" => "N",
                      "33" => "N",
                      "34" => "N",
                      "35" => "N",
                      "36" => "N",
                      "37" => "N",
                      "38" => "N",
                      "39" => "N",
                      "453" => "N",
                      "454" => "N",
                      "455" => "N",
                      "456" => "N",
                      "457" => "Y",
                      "458" => "N",
                      "459" => "N",
                      "460" => "N",
                      "461" => "N",
                      "462" => "N",
                      "463" => "N",
                      "464" => "N",
                      "465" => "N",
                      "466" => "N",
                      "467" => "N",
                      "468" => "N",
                      "469" => "N",
                      "470" => "N",
                      "471" => "N",
                      "472" => "N",
                      "473" => "N",
                      "474" => "N",
                      "475" => "N",
                      "476" => "N",
                      "477" => "N",
                      "478" => "N",
                      "479" => "N",
                      "1237" => "N",
                      "1238" => "N",
                      "1239" => "N",
                      "1240" => "N",
                      "1241" => "Y",
                      "1242" => "N",
                      "1243" => "N",
                      "1244" => "N",
                      "1245" => "N",
                      "1246" => "N",
                      "1247" => "N",
                      "1248" => "N",
                      "1249" => "N",
                      "1250" => "N",
                      "1251" => "N",
                      "1252" => "N",
                      "1253" => "N",
                      "1254" => "N",
                      "1255" => "N",
                      "1256" => "N",
                      "1257" => "N",
                      "1258" => "N",
                      "1259" => "N",
                      "1260" => "N",
                      "1261" => "N",
                      "1262" => "N",
                      "1263" => "N",
                      "1909" => "N",
                      "1910" => "N",
                      "1911" => "N",
                      "1912" => "N",
                      "1913" => "Y",
                      "1914" => "N",
                      "1915" => "N",
                      "1916" => "N",
                      "1917" => "N",
                      "1918" => "N",
                      "1919" => "N",
                      "1920" => "N",
                      "1921" => "N",
                      "1922" => "N",
                      "1923" => "N",
                      "1924" => "N",
                      "1925" => "N",
                      "1926" => "N",
                      "1927" => "N",
                      "1928" => "N",
                      "1929" => "N",
                      "1930" => "N",
                      "1931" => "N",
                      "1932" => "N",
                      "1933" => "N",
                      "1934" => "N",
                      "1935" => "N"}   
              ite = its[item]
              ret << ite if ite
            rescue
              ret = []
            end
            
          }

          Delayed::Worker.logger.debug ">>ret item: #{ret}"
          rsl = 0
          if (ret && (ret.length > 0))
            rs = ret.reject do |x| x.in?(['N']) end
            if (rs && (rs.length > 0))
              retu = 'Y'
            else
              retu = 'N'
            end
          else
            retu = nil
          end
        end
        retu
      },
    heating_system: proc { |d| [d["LFD_HeatingType_16"],d["LFD_HeatingType_51"],d[""],d["LFD_HeatingType_174"]].reject(&:blank?).join('') },
     # 'LFD_HeatingType_16', 
    mls_record_modified: proc { |d|  to_date_sql(d['L_UpdateDate'])  },
    interior_description:  proc { |d| [d["LFD_Interior_17"],d["LFD_Interior_52"],d[""],d["LFD_Interior_175"]].reject(&:blank?).join('')  },
    # 'LFD_Interior_17', 
    key_map_page: 'LM_Char10_6', 
    listing_agent_phone: 'LA1_PhoneNumber1', 
    list_price: 'L_AskingPrice', 
    listing_agent_email: 'LA1_Email', 
    listing_agent_name: proc {|d| [d['LA1_UserFirstName'],d['LA1_UserLastName']].reject(&:blank?).join(" ") },
    listing_broker_id: 'L_ListOffice1', 
    listing_broker_name: 'LO1_OrganizationName', 
    listing_date: proc { |d|  to_date_sql(d['L_ListingDate'])  },
    lot: 'LM_char5_26', 
    lot_number: 'LM_char5_26', 
    lot_description: proc { |d| [d["LFD_LotDescription_21"],d["LFD_LotDescription_57"],d[""],d["LFD_LotDescription_180"]].reject(&:blank?).join('') },
     # 'LFD_LotDescription_21', 
    lot_square_feet: proc { |d|  (d['L_NumAcres'].to_s.to_d * 43560).to_i.to_s }, 
    mls_area_key: 'L_Area',
    mls_area_name: 'L_Area', 
    mls_elementary_school: 'LM_Char10_19', 
    mls_high_school: 'LM_Char10_20', 
    mls_middle_school: 'LM_Char10_21', 
    mls_number: 'L_DisplayId', 
    new_construction: proc { |d|
      
      dt = [d["LFD_Construction_5"],d["LFD_Construction_40"],d[""],d["LFD_Construction_164"]].reject(&:blank?).join('')
      its =  {"71" => "N",
              "72" => "N",
              "73" => "N",
              "74" => "N",
              "75" => "N",
              "76" => "N",
              "77" => "N",
              "78" => "N",
              "79" => "N",
              "80" => "N",
              "81" => "N",
              "82" => "N",
              "83" => "N",
              "84" => "N",
              "85" => "N",
              "86" => "N",
              "87" => "N",
              "88" => "N",
              "520" => "N",
              "521" => "N",
              "522" => "N",
              "523" => "N",
              "524" => "N",
              "525" => "N",
              "526" => "N",
              "527" => "N",
              "528" => "N",
              "529" => "N",
              "530" => "N",
              "531" => "N",
              "532" => "N",
              "533" => "N",
              "534" => "N",
              "535" => "N",
              "536" => "N",
              "537" => "N",
              "1971" => "N",
              "1972" => "N",
              "1973" => "N",
              "1974" => "N",
              "1975" => "N",
              "1976" => "N",
              "1977" => "N",
              "1978" => "N",
              "1979" => "N",
              "1980" => "N",
              "1981" => "N",
              "1982" => "N",
              "1983" => "N",
              "1984" => "N",
              "1985" => "N",
              "1986" => "N",
              "1987" => "N",
              "1988" => "N"}
      its[dt]
    
    },

    # 'LFD_Construction_5', 
    number_of_photos: 'L_PictureCount', 
    office_phone: 'LO1_Char10_1', 
    original_list_price: 'L_OriginalPrice', 
    owner_name: 'LM_char30_1', 
    owner_phone: 'LM_Char50_2', 
    pending_date: proc { |d|  to_date_sql(d['L_OffMarketDate'])  },
    pool_area:  proc { |d|
        ret = []
        retu = nil
        field = [d["LFD_Exterior_11"],d["LFD_Exterior_46"],d[""],d["LFD_Exterior_169"]].reject(&:blank?).join('')
        unless field.blank?
          field.split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              ite = nil
              its =  {"135"=>"N", 
                      "136"=>"N", 
                      "137"=>"N", 
                      "138"=>"N", 
                      "139"=>"N", 
                      "140"=>"N", 
                      "141"=>"N", 
                      "142"=>"N", 
                      "143"=>"N", 
                      "144"=>"N", 
                      "145"=>"N", 
                      "146"=>"N", 
                      "147"=>"N", 
                      "148"=>"N", 
                      "149"=>"Y", 
                      "150"=>"Y", 
                      "151"=>"Y", 
                      "152"=>"N", 
                      "153"=>"N", 
                      "154"=>"N", 
                      "155"=>"N", 
                      "156"=>"N", 
                      "157"=>"N", 
                      "158"=>"N", 
                      "159"=>"N",
                      "585" => "N",
                      "586" => "N",
                      "587" => "N",
                      "588" => "N",
                      "589" => "N",
                      "590" => "N",
                      "591" => "N",
                      "592" => "N",
                      "593" => "N",
                      "594" => "N",
                      "595" => "N",
                      "596" => "N",
                      "597" => "N",
                      "598" => "N",
                      "599" => "Y",
                      "600" => "Y",
                      "601" => "Y",
                      "602" => "N",
                      "603" => "N",
                      "604" => "N",
                      "605" => "N",
                      "606" => "N",
                      "607" => "N",
                      "608" => "N",
                      "609" => "N",
                      "2032" => "N",
                      "2033" => "N",
                      "2034" => "N",
                      "2035" => "N",
                      "2036" => "N",
                      "2037" => "N",
                      "2038" => "N",
                      "2039" => "N",
                      "2040" => "N",
                      "2041" => "N",
                      "2042" => "N",
                      "2043" => "N",
                      "2044" => "N",
                      "2045" => "N",
                      "2046" => "Y",
                      "2047" => "Y",
                      "2048" => "Y",
                      "2049" => "N",
                      "2050" => "N",
                      "2051" => "N",
                      "2052" => "N",
                      "2053" => "N",
                      "2054" => "N",
                      "2055" => "N",
                      "2056" => "N"}
              ite = its[item]
              ret << ite if ite
            rescue
              ret = []
            end
            
          }

          Delayed::Worker.logger.debug ">>ret item: #{ret}"
          rsl = 0
          if (ret && (ret.length > 0))
            rs = ret.reject do |x| x.in?(['N']) end
            if (rs && (rs.length > 0))
              retu = 'Y'
            else
              retu = 'N'
            end
          else
            retu = nil
          end
        end
        retu
      }, 
    
    # 'LFD_Exterior_11', 
    pool_private: proc { |d|
        ret = []
        retu = nil
        field = [d["LFD_Exterior_11"],d["LFD_Exterior_46"],d[""],d["LFD_Exterior_169"]].reject(&:blank?).join('')
        unless field.blank?
          field.split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              ite = nil
              its =  {"135"=>"N", 
                      "136"=>"N", 
                      "137"=>"N", 
                      "138"=>"N", 
                      "139"=>"N", 
                      "140"=>"N", 
                      "141"=>"N", 
                      "142"=>"N", 
                      "143"=>"N", 
                      "144"=>"N", 
                      "145"=>"N", 
                      "146"=>"N", 
                      "147"=>"N", 
                      "148"=>"N", 
                      "149"=>"Y", 
                      "150"=>"Y", 
                      "151"=>"Y", 
                      "152"=>"N", 
                      "153"=>"N", 
                      "154"=>"N", 
                      "155"=>"N", 
                      "156"=>"N", 
                      "157"=>"N", 
                      "158"=>"N", 
                      "159"=>"N",
                      "585" => "N",
                      "586" => "N",
                      "587" => "N",
                      "588" => "N",
                      "589" => "N",
                      "590" => "N",
                      "591" => "N",
                      "592" => "N",
                      "593" => "N",
                      "594" => "N",
                      "595" => "N",
                      "596" => "N",
                      "597" => "N",
                      "598" => "N",
                      "599" => "Y",
                      "600" => "Y",
                      "601" => "Y",
                      "602" => "N",
                      "603" => "N",
                      "604" => "N",
                      "605" => "N",
                      "606" => "N",
                      "607" => "N",
                      "608" => "N",
                      "609" => "N",
                      "2032" => "N",
                      "2033" => "N",
                      "2034" => "N",
                      "2035" => "N",
                      "2036" => "N",
                      "2037" => "N",
                      "2038" => "N",
                      "2039" => "N",
                      "2040" => "N",
                      "2041" => "N",
                      "2042" => "N",
                      "2043" => "N",
                      "2044" => "N",
                      "2045" => "N",
                      "2046" => "Y",
                      "2047" => "Y",
                      "2048" => "Y",
                      "2049" => "N",
                      "2050" => "N",
                      "2051" => "N",
                      "2052" => "N",
                      "2053" => "N",
                      "2054" => "N",
                      "2055" => "N",
                      "2056" => "N"}  
              ite = its[item]
              ret << ite if ite
            rescue
              ret = []
            end
            
          }

          Delayed::Worker.logger.debug ">>ret item: #{ret}"
          rsl = 0
          if (ret && (ret.length > 0))
            rs = ret.reject do |x| x.in?(['N']) end
            if (rs && (rs.length > 0))
              retu = 'Y'
            else
              retu = 'N'
            end
          else
            retu = nil
          end
        end
        retu
      }, 
    category: 'L_Class', 
    public_remarks: 'LR_remarks55', 
    realtor_remarks: 'LR_remarks33', 
    rental_class: 'LFD_RentalType_185', 
    roof_type: proc { |d| [d["LFD_RoofType_26"],d["LFD_RoofType_62"],d[""],d["LFD_RoofType_186"]].reject(&:blank?).join('') }, 
    # 'LFD_RoofType_26', 
    # rooms: proc { |d| [d["LFD_Rooms_27"],d["LFD_Rooms_63"],d[""],d["LFD_Rooms_187"]].reject(&:blank?).join('') },
    # 'LFD_Rooms_27', 
    sold_price: 'L_SoldPrice',  
    section_number: 'LM_char5_33', 
    showing_instructions:  proc { |d| [d["LFD_ShowingInstructions_28"],d["LFD_ShowingInstructions_64"],d[""],d["LFD_ShowingInstructions_188"]].reject(&:blank?).join('') },
    # 'LFD_ShowingInstructions_28', 
    sold_lease_price: 'L_SoldPrice', 
    square_feet: 'LM_Int4_9', 
    state: proc { |d|  'GA' }, 
    status: 'L_StatusCatID', 
    new_stories: proc { |d| [d["LFD_Stories_30"],d["LFD_Stories_66"],d[""],d["LFD_Stories_189"]].reject(&:blank?).join('') },
    # 'LFD_Stories_30', 
    street_name: 'L_AddressStreet', 
    street_direction: 'L_AddressDirection', 
    street_number: 'L_AddressNumber', 
    new_style_of_building: proc { |d| [d["LFD_Style_31"],d["LFD_Style_67"],d[""],d["LFD_Style_190"]].reject(&:blank?).join('') },
    # 'LFD_Style_31', 
    subdivision: 'LM_Char50_5', 
    tax_amount: 'LM_Dec_8', 
    tax_id: 'LM_Char50_3', 
    tax_year: 'LM_Int2_2', 
    water_sewer: proc { |d| [d["LFD_WaterSewer_33"],d["LFD_WaterSewer_70"],d[""],d["LFD_WaterSewer_193"]].reject(&:blank?).join('') },
    # 'LFD_WaterSewer_33', 
    waterfront: proc { |d| 
      if d['LM_Int4_10'].to_s.to_i <= 0
        'N'
      else
        'Y'
      end
    },
    # 'LM_Int4_10', 
    year_built: 'LM_Int2_1', 
    zip_code: proc { |d|  d['L_Zip'].to_s.length <= 5 ? d['L_Zip'] : d['L_Zip'][0,5] },
    zip_4: proc { |d|  d['L_Zip'].to_s.length <= 5 ? nil : d['L_Zip'][6,4] }

  }

  LOOKUP_TYPE_ENUMS2 = {
    :basement_desc => {
      "BATH" => "Bath",
      "BTDOR" => "Boat Door",
      "BTHST" => "Bath/Stubbed",
      "CRAWL" => "Crawl Space",
      "DAYLT" => "Daylight",
      "DRVWY" => "Driveway Access",
      "EXTEN" => "Exterior Entry",
      "FINSH" => "Finished",
      "FULL" => "Full",
      "INTER" => "Interior Entry",
      "PARTL" => "Partial",
      "SLABN" => "Slab/None",
      "UNFIN" => "Unfinished"
    },
    :city => {
      "ABBEVI" => "Abbeville",
      "ACWORT" => "Acworth",
      "ADAIRS" => "Adairsville",
      "ADEL" => "Adel",
      "ADRIAN" => "Adrian",
      "AILEY" => "Ailey",
      "ALAMO" => "Alamo",
      "ALAPAH" => "Alapaha",
      "ALBANY" => "Albany",
      "ALDORA" => "Aldora",
      "ALLENH" => "Allenhurst",
      "ALLENT" => "Allentown",
      "ALMA" => "Alma",
      "ALPHAR" => "Alpharetta",
      "ALSTON" => "Alston",
      "ALTO" => "Alto",
      "AMBROS" => "Ambrose",
      "AMERIC" => "Americus",
      "ANDERS" => "Andersonville",
      "APPLIN" => "Appling",
      "ARABI" => "Arabi",
      "ARAGON" => "Aragon",
      "ARCADE" => "Arcade",
      "ARGYLE" => "Argyle",
      "ARLING" => "Arlington",
      "ARMUCH" => "Armuchee",
      "ARNOLD" => "Arnoldsville",
      "ASHBUR" => "Ashburn",
      "ATHENS" => "Athens",
      "ATLANT" => "Atlanta",
      "ATTAPU" => "Attapulgus",
      "AUBURN" => "Auburn",
      "AUGUST" => "Augusta",
      "AUSTEL" => "Austell",
      "AVALON" => "Avalon",
      "AVERA" => "Avera",
      "AVONDA" => "Avondale Estates",
      "AXSON" => "Axson",
      "BACONT" => "Baconton",
      "BAINBR" => "Bainbridge",
      "BALDWN" => "Baldwin",
      "BALLGR" => "Ball Ground",
      "BARNES" => "Barnesville",
      "BARNEY" => "Barney",
      "BARTOW" => "Bartow",
      "BARWIC" => "Barwick",
      "BAXLEY" => "Baxley",
      "BELLVI" => "Bellville",
      "BELVED" => "Belvedere Park",
      "BERKEL" => "Berkeley Lake",
      "BERLIN" => "Berlin",
      "BETHLE" => "Bethlehem",
      "BETWEN" => "Between",
      "BIBBCT" => "Bibb City",
      "BIGCAN" => "Big Canoe",
      "BISHOP" => "Bishop",
      "BLACKS" => "Blacksville",
      "BLAINE" => "Blaine",
      "BLAIRS" => "Blairsville",
      "BLAKEL" => "Blakely",
      "BLCKSH" => "Blackshear",
      "BLOOMI" => "Bloomingdale",
      "BLUE" => "Blue Ridge",
      "BLUFFT" => "Bluffton",
      "BLYTHE" => "Blythe",
      "BOGART" => "Bogart",
      "BOLING" => "Bolingbroke",
      "BONAIR" => "Bonaire",
      "BONANZ" => "Bonanza",
      "BONEVI" => "Boneville",
      "BOSTON" => "Boston",
      "BOSTWI" => "Bostwick",
      "BOWDON" => "Bowdon",
      "BOWERS" => "Bowersville",
      "BOWJUN" => "Bowdon Junction",
      "BOWMAN" => "Bowman",
      "BOXSPR" => "Box Springs",
      "BRASEL" => "Braselton",
      "BRASWE" => "Braswell",
      "BREMEN" => "Bremen",
      "BRINSO" => "Brinson",
      "BRISTO" => "Bristol",
      "BRONWO" => "Bronwood",
      "BROOKF" => "Brookfield",
      "BROOKH" => "Brookhaven",
      "BROOKL" => "Brooklet",
      "BROOKS" => "Brooks",
      "BROXTO" => "Broxton",
      "BRUNSW" => "Brunswick",
      "BUCHAN" => "Buchanan",
      "BUCKED" => "Buckhead",
      "BUENAV" => "Buena Vista",
      "BUFORD" => "Buford",
      "BUTLER" => "Butler",
      "BYROMV" => "Byromville",
      "BYRON" => "Byron",
      "CADWEL" => "Cadwell",
      "CAIRO" => "Cairo",
      "CALHOU" => "Calhoun",
      "CALVAR" => "Calvary",
      "CAMAK" => "Camak",
      "CAMILL" => "Camilla",
      "CANDLE" => "Candler",
      "CANDLM" => "Candler-Mcafee",
      "CANON" => "Canon",
      "CANTON" => "Canton",
      "CARL" => "Carl",
      "CARLTO" => "Carlton",
      "CARNES" => "Carnesville",
      "CARROL" => "Carrollton",
      "CARTEC" => "Cartecay",
      "CARTER" => "Cartersville",
      "CASSVI" => "Cassville",
      "CATAUL" => "Cataula",
      "CAVESP" => "Cave Spring",
      "CECIL" => "Cecil",
      "CEDARS" => "Cedar Springs",
      "CEDART" => "Cedartown",
      "CENTER" => "Centerville",
      "CENTRA" => "Centralhatchee",
      "CHAMBL" => "Chamblee",
      "CHATAH" => "Chattahoochee Hills",
      "CHATTA" => "Chattanooga Valley",
      "CHATTS" => "Chatsworth",
      "CHAUNC" => "Chauncey",
      "CHERRY" => "Cherrylog",
      "CHESTE" => "Chester",
      "CHESTN" => "Chestnut Mountain",
      "CHICKA" => "Chickamauga",
      "CHULA" => "Chula",
      "CISCO" => "Cisco",
      "CLARKD" => "Clarkdale",
      "CLARKS" => "Clarkston",
      "CLAXTO" => "Claxton",
      "CLAYTN" => "Clayton",
      "CLERMO" => "Clermont",
      "CLEVEL" => "Cleveland",
      "CLIMAX" => "Climax",
      "CLINCH" => "Clinchfield",
      "CLOUD" => "Cloudland",
      "CLRKSV" => "Clarkesville",
      "CLYO" => "Clyo",
      "COBB" => "Cobb",
      "COBBTO" => "Cobbtown",
      "COCHRA" => "Cochran",
      "COHUTT" => "Cohutta",
      "COLBER" => "Colbert",
      "COLEMA" => "Coleman",
      "COLLEG" => "College Park",
      "COLLIN" => "Collins",
      "COLQUI" => "Colquitt",
      "COLUMB" => "Columbus",
      "COMER" => "Comer",
      "COMMER" => "Commerce",
      "CONCOR" => "Concord",
      "CONLEY" => "Conley",
      "CONYER" => "Conyers",
      "COOLID" => "Coolidge",
      "COOSA" => "Coosa",
      "CORDEL" => "Cordele",
      "CORINT" => "Corinth",
      "CORNEL" => "Cornelia",
      "COTTON" => "Cotton",
      "COUNTR" => "Country Club Estates",
      "COVING" => "Covington",
      "CRABAP" => "Crabapple",
      "CRANDA" => "Crandall",
      "CRAWFO" => "Crawford",
      "CRAWFV" => "Crawfordville",
      "CRESCE" => "Crescent",
      "CULLOD" => "Culloden",
      "CUMMIN" => "Cumming",
      "CUSSET" => "Cusseta",
      "CUTHBE" => "Cuthbert",
      "DACULA" => "Dacula",
      "DAHLON" => "Dahlonega",
      "DAISY" => "Daisy",
      "DALLAS" => "Dallas",
      "DALTON" => "Dalton",
      "DAMASC" => "Damascus",
      "DANIEL" => "Danielsville",
      "DANVIL" => "Danville",
      "DARIEN" => "Darien",
      "DASHER" => "Dasher",
      "DAVISB" => "Davisboro",
      "DAWSN" => "Dawson",
      "DAWSON" => "Dawsonville",
      "DEARIN" => "Dearing",
      "DECATR" => "Decatur",
      "DEENWO" => "Deenwood",
      "DEEPST" => "Deepstep",
      "DEMORE" => "Demorest",
      "DENTON" => "Denton",
      "DESOTO" => "De Soto",
      "DEWYRO" => "Dewy Rose",
      "DEXTER" => "Dexter",
      "DILARD" => "Dillard",
      "DIXIE" => "Dixie",
      "DOCKJU" => "Dock Junction",
      "DOERUN" => "Doerun",
      "DONALS" => "Donalsonville",
      "DOOLIN" => "Dooling",
      "DORAVI" => "Doraville",
      "DOUGLA" => "Douglasville",
      "DOUGLS" => "Douglas",
      "DOVER" => "Dover",
      "DRUIDH" => "Druid Hills",
      "DRYBRA" => "Dry Branch",
      "DUBLIN" => "Dublin",
      "DUDLEY" => "Dudley",
      "DULUTH" => "Duluth",
      "DUNWDY" => "Dunwoody",
      "DUPONT" => "Du Pont",
      "EASTBO" => "East Boundary",
      "EASTDU" => "East Dublin",
      "EASTEL" => "East Ellijay",
      "EASTGR" => "East Griffin",
      "EASTMA" => "Eastman",
      "EASTNE" => "East Newnan",
      "EASTON" => "Eastanollee",
      "EASTPT" => "East Point",
      "EATON" => "Eatonton",
      "EBENEZ" => "Ebenezer",
      "EDEN" => "Eden",
      "EDGEHI" => "Edge Hill",
      "EDISON" => "Edison",
      "ELBERT" => "Elberton",
      "ELKO" => "Elko",
      "ELLABE" => "Ellabell",
      "ELLAVI" => "Ellaville",
      "ELLENT" => "Ellenton",
      "ELLENW" => "Ellenwood",
      "ELLERS" => "Ellerslie",
      "ELLIJAY" => "Ellijay",
      "EMERSN" => "Emerson",
      "ENIGMA" => "Enigma",
      "EPHESU" => "Ephesus",
      "EPWORT" => "Epworth",
      "ESOMHI" => "Esom Hill",
      "ETON" => "Eton",
      "EUHARL" => "Euharlee",
      "EVANS" => "Evans",
      "EVILLE" => "Eastville",
      "EXPERI" => "Experiment",
      "FAIRBN" => "Fairburn",
      "FAIRMT" => "Fairmount",
      "FAIROA" => "Fair Oaks",
      "FAIRPL" => "Fair Play",
      "FAIRVI" => "Fairview",
      "FARGO" => "Fargo",
      "FARMIN" => "Farmington",
      "FAYETT" => "Fayetteville",
      "FELTON" => "Felton",
      "FITZGE" => "Fitzgerald",
      "FLEMIG" => "Fleming",
      "FLEMIN" => "Flemington",
      "FLINTS" => "Flintstone",
      "FLOVIL" => "Flovilla",
      "FLOWER" => "Flowery Branch",
      "FOLKST" => "Folkston",
      "FOREST" => "Forest Park",
      "FORSYT" => "Forsyth",
      "FORTGA" => "Fort Gaines",
      "FORTSO" => "Fortson",
      "FOWLST" => "Fowlstown",
      "FRANKL" => "Franklin Springs",
      "FRANKN" => "Franklin",
      "FREEHM" => "Freehome",
      "FRTBEN" => "Fort Benning South",
      "FRTGOR" => "Fort Gordon",
      "FRTOGL" => "Fort Oglethorpe",
      "FRTSTE" => "Fort Stewart",
      "FRTVAL" => "Fort Valley",
      "FUNSTO" => "Funston",
      "GAINES" => "Gainesville",
      "GAINSC" => "Gaines School",
      "GAINSV" => "Gainesville Mills",
      "GARDEN" => "Garden City",
      "GARFIE" => "Garfield",
      "GAY" => "Gay",
      "GENEVA" => "Geneva",
      "GEORGE" => "Georgetown",
      "GIBSON" => "Gibson",
      "GILLSV" => "Gillsville",
      "GIRARD" => "Girard",
      "GLENN" => "Glenn",
      "GLENNV" => "Glennville",
      "GLENWO" => "Glenwood",
      "GOOD" => "Good Hope",
      "GORDON" => "Gordon",
      "GOUGH" => "Gough",
      "GRACEW" => "Gracewood",
      "GRAHAM" => "Graham",
      "GRAVES" => "Graves",
      "GRAY" => "Gray",
      "GRAYSN" => "Grayson",
      "GRAYSV" => "Graysville",
      "GREENS" => "Greensboro",
      "GREENV" => "Greenville",
      "GRESHA" => "Gresham Park",
      "GRIFFI" => "Griffin",
      "GRNTV" => "Grantville",
      "GROVET" => "Grovetown",
      "GUMBRA" => "Gumbranch",
      "GUMLOG" => "Gumlog",
      "GUYTON" => "Guyton",
      "HADDOC" => "Haddock",
      "HAGAN" => "Hagan",
      "HAHIRA" => "Hahira",
      "HAMILT" => "Hamilton",
      "HAMPTO" => "Hampton",
      "HANNAH" => "Hannah",
      "HAPEVI" => "Hapeville",
      "HARALS" => "Haralson",
      "HARDWI" => "Hardwick",
      "HARLEM" => "Harlem",
      "HARRIS" => "Harrison",
      "HARTSF" => "Hartsfield",
      "HARTWE" => "Hartwell",
      "HAWKIN" => "Hawkinsville",
      "HAZLEH" => "Hazlehurst",
      "HELEN" => "Helen",
      "HELENA" => "Helena",
      "HEPHZI" => "Hephzibah",
      "HIAWAS" => "Hiawassee",
      "HIGGST" => "Higgston",
      "HIGHSH" => "High Shoals",
      "HILLSB" => "Hillsboro",
      "HILLTO" => "Hilltonia",
      "HINESV" => "Hinesville",
      "HIRAM" => "Hiram",
      "HOBOKE" => "Hoboken",
      "HOGANS" => "Hogansville",
      "HOLLYS" => "Holly Springs",
      "HOMELA" => "Homeland",
      "HOMER" => "Homer",
      "HOMERV" => "Homerville",
      "HORTEN" => "Hortense",
      "HOSCTN" => "Hoschton",
      "HOWARD" => "Howard",
      "HULL" => "Hull",
      "IDEAL" => "Ideal",
      "ILA" => "Ila",
      "INDIAN" => "Indian Springs",
      "INMAN" => "Inman",
      "IRONCI" => "Iron City",
      "IRONDA" => "Irondale",
      "IRWINT" => "Irwinton",
      "IRWINV" => "Irwinville",
      "ISLEOF" => "Isle Of Hope-Dutch Island",
      "IVEY" => "Ivey",
      "JACKSO" => "Jackson",
      "JAKIN" => "Jakin",
      "JASPER" => "Jasper",
      "JCKSNV" => "Jacksonville",
      "JEFFER" => "Jefferson",
      "JEFFRV" => "Jeffersonville",
      "JEKYLL" => "Jekyll Island",
      "JENKIN" => "Jenkinsburg",
      "JERSEY" => "Jersey",
      "JESUP" => "Jesup",
      "JEWELL" => "Jewell",
      "JOHCRK" => "Johns Creek",
      "JONESE" => "Jonesboro",
      "JULIET" => "Juliette",
      "JUNCTI" => "Junction City",
      "KATHLE" => "Kathleen",
      "KENNES" => "Kennesaw",
      "KEYSVI" => "Keysville",
      "KINGSB" => "Kings Bay Base",
      "KINGSL" => "Kingsland",
      "KINGST" => "Kingston",
      "KITE" => "Kite",
      "KNOXVI" => "Knoxville",
      "LAFAYE" => "La Fayette",
      "LAGRAN" => "Lagrange",
      "LAKECI" => "Lake City",
      "LAKELA" => "Lakeland",
      "LAKEMT" => "Lakemont",
      "LAKEPA" => "Lake Park",
      "LAKEVI" => "Lakeview Estates",
      "LAKEVW" => "Lakeview",
      "LAVONI" => "Lavonia",
      "LAWREN" => "Lawrenceville",
      "LEARY" => "Leary",
      "LEBANO" => "Lebanon",
      "LEESBU" => "Leesburg",
      "LENOX" => "Lenox",
      "LESLIE" => "Leslie",
      "LEXING" => "Lexington",
      "LILBUR" => "Lilburn",
      "LILLY" => "Lilly",
      "LINCOL" => "Lincolnton",
      "LINDAL" => "Lindale",
      "LINWOO" => "Linwood",
      "LITHIA" => "Lithia Springs",
      "LITHON" => "Lithonia",
      "LIZELL" => "Lizella",
      "LOCUST" => "Locust Grove",
      "LOGANV" => "Loganville",
      "LONEOA" => "Lone Oak",
      "LOOKOU" => "Lookout Mountain",
      "LOUISV" => "Louisville",
      "LOUVAL" => "Louvale",
      "LOVEJO" => "Lovejoy",
      "LUDOWI" => "Ludowici",
      "LULA" => "Lula",
      "LUMBER" => "Lumber City",
      "LUMPKI" => "Lumpkin",
      "LUTHER" => "Luthersville",
      "LYERLY" => "Lyerly",
      "LYONS" => "Lyons",
      "MABLET" => "Mableton",
      "MACON" => "Macon",
      "MADISN" => "Madison",
      "MANASS" => "Manassas",
      "MANCHE" => "Manchester",
      "MANOR" => "Manor",
      "MANSFI" => "Mansfield",
      "MARBLE" => "Marblehill",
      "MARIET" => "Marietta",
      "MARSHA" => "Marshallville",
      "MARTIN" => "Martin",
      "MARTNZ" => "Martinez",
      "MATT" => "Matt",
      "MATTHE" => "Matthews",
      "MAUK" => "Mauk",
      "MAXEYS" => "Maxeys",
      "MAYSVI" => "Maysville",
      "MCCAYS" => "Mccaysville",
      "MCDONO" => "Mcdonough",
      "MCINTY" => "Mcintyre",
      "MCRAE" => "Mcrae",
      "MDWYHR" => "Midway-Hardwick",
      "MEANSV" => "Meansville",
      "MEIGS" => "Meigs",
      "MELDRI" => "Meldrim",
      "MENLO" => "Menlo",
      "MERIDI" => "Meridian",
      "MERSHO" => "Mershon",
      "MESENA" => "Mesena",
      "METTER" => "Metter",
      "MIDLAN" => "Midland",
      "MIDVIL" => "Midville",
      "MIDWAY" => "Midway",
      "MILAN" => "Milan",
      "MILLED" => "Milledgeville",
      "MILLEN" => "Millen",
      "MILLWO" => "Millwood",
      "MILNER" => "Milner",
      "MILTON" => "Milton",
      "MINERA" => "Mineral Bluff",
      "MITCHE" => "Mitchell",
      "MNTCTY" => "Mountain City",
      "MNTPRK" => "Mountain Park",
      "MOLENA" => "Molena",
      "MONROE" => "Monroe",
      "MONTEZ" => "Montezuma",
      "MONTGO" => "Montgomery",
      "MONTIC" => "Monticello",
      "MONTRO" => "Montrose",
      "MOODYA" => "Moody Afb",
      "MORELA" => "Moreland",
      "MORGAN" => "Morganton",
      "MORRIS" => "Morris",
      "MORROW" => "Morrow",
      "MORVEN" => "Morven",
      "MOULTR" => "Moultrie",
      "MOUNTA" => "Mount Airy",
      "MOUNTB" => "Mount Berry",
      "MOUNTV" => "Mount Vernon",
      "MOUNTZ" => "Mount Zion",
      "MRGAN" => "Morgan",
      "MURRAY" => "Murrayville",
      "MUSELL" => "Musella",
      "MYSTIC" => "Mystic",
      "NAHUNT" => "Nahunta",
      "NASHVI" => "Nashville",
      "NAYLOR" => "Naylor",
      "NELSON" => "Nelson",
      "NEWBOR" => "Newborn",
      "NEWHOP" => "Newhope",
      "NEWING" => "Newington",
      "NEWNAN" => "Newnan",
      "NEWTON" => "Newton",
      "NHIGHS" => "North High Shoals",
      "NICHLL" => "Nicholls",
      "NICHOL" => "Nicholson",
      "NORCRO" => "Norcross",
      "NORMAN" => "Norman Park",
      "NORRIS" => "Norristown",
      "NORTHM" => "North Metro",
      "NORWOO" => "Norwood",
      "NRTATL" => "North Atlanta",
      "NRTDEC" => "North Decatur",
      "NRTDRU" => "North Druid Hills",
      "NUNEZ" => "Nunez",
      "OAKFIE" => "Oakfield",
      "OAKHIL" => "Oakhill",
      "OAKMAN" => "Oakman",
      "OAKPAR" => "Oak Park",
      "OAKWOD" => "Oakwood",
      "OCHLOC" => "Ochlocknee",
      "OCILLA" => "Ocilla",
      "OCONEE" => "Oconee",
      "ODUM" => "Odum",
      "OFFERM" => "Offerman",
      "OGLETH" => "Oglethorpe",
      "OLIVER" => "Oliver",
      "OMAHA" => "Omaha",
      "OMEGA" => "Omega",
      "ORCHAR" => "Orchard Hill",
      "OTHRAL" => "Other-Alabama",
      "OTHREA" => "Other East",
      "OTHRFL" => "Other-Florida",
      "OTHRNC" => "Other-North Carolina",
      "OTHRNO" => "Other North",
      "OTHRSC" => "Other-South Carolina",
      "OTHRSO" => "Other South",
      "OTHRST" => "Other States",
      "OTHRTN" => "Other-Tennessee",
      "OTHRWE" => "Other West",
      "OXFORD" => "Oxford",
      "PALMET" => "Palmetto",
      "PANTHE" => "Panthersville",
      "PARROT" => "Parrott",
      "PATTER" => "Patterson",
      "PAVO" => "Pavo",
      "PAYNE" => "Payne",
      "PEACHC" => "Peachtree Corners",
      "PEACHT" => "Peachtree City",
      "PEARSO" => "Pearson",
      "PELHAM" => "Pelham",
      "PEMBRO" => "Pembroke",
      "PENDER" => "Pendergrass",
      "PERKIN" => "Perkins",
      "PERRY" => "Perry",
      "PHILLI" => "Phillipsburg",
      "PINEHU" => "Pinehurst",
      "PINELK" => "Pine Lake",
      "PINEMT" => "Pine Mountain",
      "PINEMV" => "Pine Mountain Valley",
      "PINEVI" => "Pineview",
      "PITTS" => "Pitts",
      "PLAINF" => "Plainfield",
      "PLAINS" => "Plains",
      "PLAINV" => "Plainville",
      "POOLER" => "Pooler",
      "PORTAL" => "Portal",
      "PORTER" => "Porterdale",
      "PORTWE" => "Port Wentworth",
      "POULAN" => "Poulan",
      "POWDER" => "Powder Springs",
      "PRESTO" => "Preston",
      "PULASK" => "Pulaski",
      "PUTNEY" => "Putney",
      "QUITMA" => "Quitman",
      "RABUN" => "Rabun Gap",
      "RANGER" => "Ranger",
      "RAOUL" => "Raoul",
      "RAYCIT" => "Ray City",
      "RAYLE" => "Rayle",
      "REBECC" => "Rebecca",
      "REDAN" => "Redan",
      "REDOAK" => "Red Oak",
      "REEDCR" => "Reed Creek",
      "REGIST" => "Register",
      "REIDSV" => "Reidsville",
      "REMERT" => "Remerton",
      "RENTZ" => "Rentz",
      "RESACA" => "Resaca",
      "RESTHA" => "Rest Haven",
      "REX" => "Rex",
      "REYNOL" => "Reynolds",
      "RHINE" => "Rhine",
      "RICEBO" => "Riceboro",
      "RICHLA" => "Richland",
      "RICHMO" => "Richmond Hill",
      "RIDDLE" => "Riddleville",
      "RINCON" => "Rincon",
      "RINGGO" => "Ringgold",
      "RISING" => "Rising Fawn",
      "RIVERD" => "Riverdale",
      "RIVERS" => "Riverside",
      "ROBERT" => "Roberta",
      "ROBINS" => "Robins Afb",
      "ROCHEL" => "Rochelle",
      "ROCKLE" => "Rockledge",
      "ROCKMA" => "Rockmart",
      "ROCKSP" => "Rock Spring",
      "ROCKY" => "Rocky Face",
      "ROCKYF" => "Rocky Ford",
      "ROME" => "Rome",
      "ROOPVI" => "Roopville",
      "ROSSVI" => "Rossville",
      "ROSWEL" => "Roswell",
      "ROYSTN" => "Royston",
      "RUPERT" => "Rupert",
      "RUSSEL" => "Russell",
      "RUTLED" => "Rutledge",
      "RYDAL" => "Rydal",
      "SAINTG" => "Saint George",
      "SALECI" => "Sale City",
      "SANDER" => "Sandersville",
      "SANDHI" => "Sand Hill",
      "SANDYS" => "Sandy Springs",
      "SANTAC" => "Santa Claus",
      "SAPELO" => "Sapelo Island",
      "SARDIS" => "Sardis",
      "SARGEN" => "Sargent",
      "SASSER" => "Sasser",
      "SAUTEE" => "Sautee",
      "SAVANN" => "Savannah",
      "SCOTLA" => "Scotland",
      "SCOTTS" => "Scottdale",
      "SCREVE" => "Screven",
      "SEAISL" => "Sea Island",
      "SENOA" => "Senoia",
      "SEVILL" => "Seville",
      "SHADYD" => "Shady Dale",
      "SHANNO" => "Shannon",
      "SHARON" => "Sharon",
      "SHELLM" => "Shellman",
      "SHILOH" => "Shiloh",
      "SHRPS" => "Sharpsburg",
      "SILCRK" => "Silver Creek",
      "SILOAM" => "Siloam",
      "SKIDAW" => "Skidaway Island",
      "SKYVAL" => "Sky Valley",
      "SMARR" => "Smarr",
      "SMITHV" => "Smithville",
      "SMMRVL" => "Summerville",
      "SMOKER" => "Smoke Rise",
      "SMRTWN" => "Summertown",
      "SMYRNA" => "Smyrna",
      "SNELLV" => "Snellville",
      "SOCIAL" => "Social Circle",
      "SOPERT" => "Soperton",
      "SOUTHA" => "South Augusta",
      "SPARKS" => "Sparks",
      "SPARTA" => "Sparta",
      "SPIVEY" => "Lake Spivey",
      "SPRING" => "Springfield",
      "STAPLE" => "Stapleton",
      "STATEN" => "Statenville",
      "STATES" => "Statesboro",
      "STATHA" => "Statham",
      "STEPHE" => "Stephens",
      "STILES" => "Stilesboro",
      "STILLM" => "Stillmore",
      "STMAR" => "St. Marys",
      "STOCKB" => "Stockbridge",
      "STOCKT" => "Stockton",
      "STONEM" => "Stone Mountain",
      "STSIM" => "St. Simons",
      "SUCHES" => "Suches",
      "SUGARH" => "Sugar Hill",
      "SUGARV" => "Sugar Valley",
      "SUMNER" => "Sumner",
      "SUNNYS" => "Sunnyside",
      "SURREN" => "Surrency",
      "SUWANE" => "Suwanee",
      "SWAINS" => "Swainsboro",
      "SYCAMO" => "Sycamore",
      "SYLVAN" => "Sylvania",
      "SYLVES" => "Sylvester",
      "TALBOT" => "Talbotton",
      "TALKIN" => "Talking Rock",
      "TALLAP" => "Tallapoosa",
      "TALLUL" => "Tallulah Falls",
      "TALMO" => "Talmo",
      "TARRYT" => "Tarrytown",
      "TATE" => "Tate",
      "TAYLOR" => "Taylorsville",
      "TEMPLE" => "Temple",
      "TENNGA" => "Tennga",
      "TENNIL" => "Tennille",
      "THEROC" => "The Rock",
      "THMSTN" => "Thomaston",
      "THMSVL" => "Thomasville",
      "THOMSO" => "Thomson",
      "THUNDE" => "Thunderbolt",
      "TIFTON" => "Tifton",
      "TIGER" => "Tiger",
      "TIGNAL" => "Tignall",
      "TOCCOA" => "Toccoa",
      "TOCFAL" => "Toccoa Falls",
      "TOOMSB" => "Toomsboro",
      "TOWNSE" => "Townsend",
      "TRENTO" => "Trenton",
      "TRION" => "Trion",
      "TUCKER" => "Tucker",
      "TUNNEL" => "Tunnel Hill",
      "TURIN" => "Turin",
      "TURNER" => "Turnerville",
      "TWINCI" => "Twin City",
      "TYBEEI" => "Tybee Island",
      "TYRONE" => "Tyrone",
      "TYTY" => "Ty Ty",
      "UNADIL" => "Unadilla",
      "UNION" => "Union City",
      "UNIONP" => "Union Point",
      "UNIONV" => "Unionville",
      "UPATOI" => "Upatoi",
      "UVALDA" => "Uvalda",
      "VALDOS" => "Valdosta",
      "VARNEL" => "Varnell",
      "VERNON" => "Vernonburg",
      "VIDALI" => "Vidalia",
      "VIENNA" => "Vienna",
      "VILLA" => "Villa Rica",
      "VINING" => "Vinings",
      "WACO" => "Waco",
      "WADLEY" => "Wadley",
      "WALESK" => "Waleska",
      "WALNUT" => "Walnut Grove",
      "WALTHO" => "Walthourville",
      "WARESB" => "Waresboro",
      "WARMSP" => "Warm Springs",
      "WARNER" => "Warner Robins",
      "WARREN" => "Warrenton",
      "WARTHE" => "Warthen",
      "WARWIC" => "Warwick",
      "WASHIN" => "Washington",
      "WATKIN" => "Watkinsville",
      "WAVERL" => "Waverly Hall",
      "WAVRLY" => "Waverly",
      "WAYCRO" => "Waycross",
      "WAYNBO" => "Waynesville",
      "WAYNES" => "Waynesboro",
      "WESTAU" => "West Augusta",
      "WESTGR" => "West Green",
      "WESTON" => "Weston",
      "WESTPO" => "West Point",
      "WESTSI" => "Westside",
      "WHIGHA" => "Whigham",
      "WHITE" => "White",
      "WHITEM" => "Whitemarsh Island",
      "WHITEP" => "White Plains",
      "WHITES" => "Whitesburg",
      "WHOAK" => "White Oak",
      "WILDWO" => "Wildwood",
      "WILEY" => "Wiley",
      "WILLAC" => "Willacoochee",
      "WILLIA" => "Williamson",
      "WILMIN" => "Wilmington Island",
      "WINDER" => "Winder",
      "WINSTN" => "Winston",
      "WINTER" => "Winterville",
      "WOODBI" => "Woodbine",
      "WOODBU" => "Woodbury",
      "WOODLA" => "Woodland",
      "WOODST" => "Woodstock",
      "WOODVI" => "Woodville",
      "WOOLSE" => "Woolsey",
      "WRAY" => "Wray",
      "WRENS" => "Wrens",
      "WRIGHT" => "Wrightsville",
      "YATESV" => "Yatesville",
      "YORKVI" => "Yorkville",
      "YOUNGH" => "Young Harris",
      "ZEBULO" => "Zebulon"
    },
    :cooling_system_type => {
      "CLFAN" => "Ceiling Fans",
      "CNELE" => "Central Electric",
      "CNGAS" => "Central Gas",
      "CNTRL" => "Central System",
      "EXFAN" => "Exhaust Fan",
      "HPUMP" => "Heat Pump",
      "HSFAN" => "Fan",
      "HUMID" => "Humidity Control",
      "INDMT" => "Individual Meters",
      "NONE" => "None",
      "OTHER" => "Other",
      "SOLAR" => "Solar A/C ",
      "WNDOW" => "Window Units",
      "ZONED" => "Zoned"
    },
    :county => {
      "AKOTH" => "Other-Alaska",
      "ALABAMAOTH" => "Other-Alabama",
      "APPLING" => "Appling",
      "AROTH" => "Other-Arkansas",
      "ATKINSON" => "Atkinson",
      "AZOTH" => "Other-Arizona",
      "BACON" => "Bacon",
      "BAKER" => "Baker",
      "BALDWIN" => "Baldwin",
      "BANKS" => "Banks",
      "BARROW" => "Barrow",
      "BARTOW" => "Bartow",
      "BENHILL" => "Ben Hill",
      "BERRIEN" => "Berrien",
      "BIBB" => "Bibb",
      "BLECKLEY" => "Bleckley",
      "BRANTLEY" => "Brantley",
      "BROOKS" => "Brooks",
      "BRYAN" => "Bryan",
      "BULLOCH" => "Bulloch",
      "BURKE" => "Burke",
      "BUTTS" => "Butts",
      "CALHOUN" => "Calhoun",
      "CAMDEN" => "Camden",
      "CANDLER" => "Candler",
      "CAOTH" => "Other-California",
      "CARROLL" => "Carroll",
      "CATOOSA" => "Catoosa",
      "CHARLTON" => "Charlton",
      "CHATHAM" => "Chatham",
      "CHATTAHOOCH" => "Chattahoochee",
      "CHATTOOGA" => "Chattooga",
      "CHEROKEE" => "Cherokee",
      "CLARKE" => "Clarke",
      "CLAY" => "Clay",
      "CLAYTON" => "Clayton",
      "CLINCH" => "Clinch",
      "COBB" => "Cobb",
      "COFFEE" => "Coffee",
      "COLQUITT" => "Colquitt",
      "COLUMBIA" => "Columbia",
      "COOK" => "Cook",
      "COOTH" => "Other-Colorado",
      "COWETA" => "Coweta",
      "CRAWFORD" => "Crawford",
      "CRISP" => "Crisp",
      "CTOTH" => "Other-Connecticut",
      "DADE" => "Dade",
      "DAWSON" => "Dawson",
      "DCOTH" => "Other-District Of Columbia",
      "DECATUR" => "Decatur",
      "DEKALB" => "Dekalb",
      "DEOTH" => "Other-Delaware",
      "DODGE" => "Dodge",
      "DOOLY" => "Dooly",
      "DOUGHERTY" => "Dougherty",
      "DOUGLAS" => "Douglas",
      "EARLY" => "Early",
      "ECHOLS" => "Echols",
      "EFFINGHAM" => "Effingham",
      "ELBERT" => "Elbert",
      "EMANUEL" => "Emanuel",
      "EVANS" => "Evans",
      "FANNIN" => "Fannin",
      "FAYETTE" => "Fayette",
      "FLORIDAOTH" => "Other-Florida",
      "FLOYD" => "Floyd",
      "FORSYTH" => "Forsyth",
      "FRANKLIN" => "Franklin",
      "FULTON" => "Fulton",
      "GILMER" => "Gilmer",
      "GLASCOCK" => "Glascock",
      "GLYNN" => "Glynn",
      "GORDON" => "Gordon",
      "GRADY" => "Grady",
      "GREENE" => "Greene",
      "GWINNETT" => "Gwinnett",
      "HABERSHA" => "Habersham",
      "HALL" => "Hall",
      "HANCOCK" => "Hancock",
      "HARALSON" => "Haralson",
      "HARRIS" => "Harris",
      "HART" => "Hart",
      "HEARD" => "Heard",
      "HENRY" => "Henry",
      "HIOTH" => "Other-Hawaii",
      "HOUSTON" => "Houston",
      "IAOTH" => "Other-Iowa",
      "IDOTH" => "Other-Idaho",
      "ILOTH" => "Other-Illinois",
      "INOTH" => "Other-Indiana",
      "IRWIN" => "Irwin",
      "JACKSON" => "Jackson",
      "JASPER" => "Jasper",
      "JEFFDAVIS" => "Jeff Davis",
      "JEFFERSON" => "Jefferson",
      "JENKINS" => "Jenkins",
      "JOHNSON" => "Johnson",
      "JONES" => "Jones",
      "KSOTH" => "Other-Kansas",
      "KYOTH" => "Other-Kentucky",
      "LAMAR" => "Lamar",
      "LANIER" => "Lanier",
      "LAOTH" => "Other-Louisiana",
      "LAURENS" => "Laurens",
      "LEE" => "Lee",
      "LIBERTY" => "Liberty",
      "LINCOLN" => "Lincoln",
      "LONG" => "Long",
      "LOWNDES" => "Lowndes",
      "LUMPKIN" => "Lumpkin",
      "MACON" => "Macon",
      "MADISON" => "Madison",
      "MAOTH" => "Other-Massachusetts",
      "MARION" => "Marion",
      "MCDUFFIE" => "Mcduffie",
      "MCINTOSH" => "Mcintosh",
      "MDOTH" => "Other-Maryland",
      "MEOTH" => "Other-Maine",
      "MERIWETHER" => "Meriwether",
      "MILLER" => "Miller",
      "MIOTH" => "Other-Michigan",
      "MITCHELL" => "Mitchell",
      "MNOTH" => "Other-Minnesota",
      "MONROE" => "Monroe",
      "MONTGOMERY" => "Montgomery",
      "MOOTH" => "Other-Missouri",
      "MORGAN" => "Morgan",
      "MSOTH" => "Other-Mississippi",
      "MTOTH" => "Other-Montana",
      "MURRAY" => "Murray",
      "MUSCOGEE" => "Muscogee",
      "NCAROLINOTH" => "Other-North Carolina",
      "NDOTH" => "Other-North Dakota",
      "NEOTH" => "Other-Nebraska",
      "NEWTON" => "Newton",
      "NHOTH" => "Other-New Hampshire",
      "NJOTH" => "Other-New Jersey",
      "NMOTH" => "Other-New Mexico",
      "NVOTH" => "Other-Nevada",
      "NYOTH" => "Other-New York",
      "OCONEE" => "Oconee",
      "OGLETHORPE" => "Oglethorpe",
      "OHOTH" => "Other-Ohio",
      "OKOTH" => "Other-Oklahoma",
      "OROTH" => "Other-Oregon",
      "PAOTH" => "Other-Pennsylvania",
      "PAULDING" => "Paulding",
      "PEACH" => "Peach",
      "PICKENS" => "Pickens",
      "PIERCE" => "Pierce",
      "PIKE" => "Pike",
      "POLK" => "Polk",
      "PULASKI" => "Pulaski",
      "PUTNAM" => "Putnam",
      "QUITMAN" => "Quitman",
      "RABUN" => "Rabun",
      "RANDOLPH" => "Randolph",
      "RICHMOND" => "Richmond",
      "RIOTH" => "Other-Rhode Island",
      "ROCKDALE" => "Rockdale",
      "SCAROLINOTH" => "Other-South Carolina",
      "SCHLEY" => "Schley",
      "SCREVEN" => "Screven",
      "SDOTH" => "Other-South Dakota",
      "SEMINOLE" => "Seminole",
      "SPALDING" => "Spalding",
      "STEPHENS" => "Stephens",
      "STEWART" => "Stewart",
      "SUMTER" => "Sumter",
      "TALBOT" => "Talbot",
      "TALIAFERRO" => "Taliaferro",
      "TATTNALL" => "Tattnall",
      "TAYLOR" => "Taylor",
      "TELFAIR" => "Telfair",
      "TENNESSEOTH" => "Other-Tennessee",
      "TERRELL" => "Terrell",
      "THOMAS" => "Thomas",
      "TIFT" => "Tift",
      "TOOMBS" => "Toombs",
      "TOWNS" => "Towns",
      "TREUTLEN" => "Treutlen",
      "TROUP" => "Troup",
      "TURNER" => "Turner",
      "TWIGGS" => "Twiggs",
      "TXOTH" => "Other-Texas",
      "UNION" => "Union",
      "UPSON" => "Upson",
      "UTOTH" => "Other-Utah",
      "VAOTH" => "Other-Virginia",
      "VTOTH" => "Other-Vermont",
      "WALKER" => "Walker",
      "WALTON" => "Walton",
      "WAOTH" => "Other-Washington",
      "WARE" => "Ware",
      "WARREN" => "Warren",
      "WASHINGTON" => "Washington",
      "WAYNE" => "Wayne",
      "WEBSTER" => "Webster",
      "WHEELER" => "Wheeler",
      "WHITE" => "White",
      "WHITFIELD" => "Whitfield",
      "WILCOX" => "Wilcox",
      "WILKES" => "Wilkes",
      "WILKINSON" => "Wilkinson",
      "WIOTH" => "Other-Wisconsin",
      "WORTH" => "Worth",
      "WVOTH" => "Other-West Virginia",
      "WYOTH" => "Other-Wyoming"
    },
    :equipment => {
      "AIRFL" => "Elec Air Filter",
      "CMPCT" => "Trash Compactor",
      "CNTVC" => "Central Vacuum",
      "DBLOV" => "Double Ovens",
      "DISHW" => "Dishwasher",
      "DRYER" => "Dryer",
      "DSPSL" => "Garbage Disposal",
      "ELERN" => "Elec Ovn/Rng/Ctop",
      "ELEWH" => "Elec Water Heater",
      "ENRGSTAPP" => "Energy Star Appliances",
      "FRIDGE" => "Refrigerator",
      "GASRN" => "Gas Ovn/Rng/Ctop",
      "GASWH" => "Gas Water Heater",
      "INGRL" => "Indoor Grill",
      "INTCM" => "Intercom/Radio",
      "MICRO" => "Microwave",
      "OTHER" => "Other",
      "REFIN" => "Refrig Included",
      "SLFCL" => "Self-Clean Oven",
      "SMOKE" => "Smoke/Fire Alarm",
      "SSLSE" => "Sec System Leased",
      "SSOWN" => "Sec System Owned",
      "TNKLSWTRHTR" => "Tankless Water Heater",
      "WASHER" => "Washer"
    },
    :exterior_description => {
      "12STEP" => "1-2 Step Entry",
      "BARNS" => "Barn(s)",
      "CARIGE" => "Carriage House",
      "CNTST" => "Central Storage",
      "COVPR" => "Cov/Enclosed Porch",
      "DECK" => "Deck",
      "FENCE" => "Fenced Yard",
      "FRPOR" => "Front Porch",
      "GASGR" => "Gas Grill",
      "GAZBO" => "Gazebo",
      "GRDEN" => "Garden Area",
      "GUEST" => "Guest House",
      "HOTUB" => "Hot Tub/Spa",
      "INVFN" => "Invisible Fence",
      "IRRGT" => "Irrigation System",
      "LNDSC" => "Prof Landscaping",
      "NONE" => "None",
      "OTHER" => "Other",
      "OUTBL" => "Out-Buildings",
      "OUTFRE" => "Outdoor Fireplace",
      "PATIO" => "Patio",
      "PLSCR" => "Pool Screened/Enclosed",
      "POOLH" => "Pool House",
      "PRVBY" => "Private Backyard",
      "SATLT" => "Satellite Dish",
      "STBLE" => "Stables",
      "WBALC" => "Walk out balcony",
      "WCTYD" => "Walk out courtyard",
      "WRPOR" => "Wrap-Around Porch",
      "YARD" => "Yard"
    },
    :floor_type => {
      "CARPT" => "Carpeting",
      "CERMC" => "Ceramic Tile",
      "CNCRT" => "Concrete",
      "HRDWD" => "Hardwood",
      "NONE" => "None",
      "OTHER" => "Other",
      "TERAZ" => "Terrazzo",
      "VINYL" => "Vinyl Tile"
    },
    :heating_system => {
      "BASBD" => "Baseboard",
      "BOILE" => "Boiler",
      "CNTSY" => "Central System",
      "ELECT" => "Electric",
      "FORCE" => "Forced Air",
      "GAS" => "Gas",
      "GRVTY" => "Gravity/Floor Htg",
      "HPUMP" => "Heat Pump",
      "METER" => "Metered",
      "NONE" => "None",
      "OIL" => "Oil",
      "OTHER" => "Other",
      "PROPN" => "Propane",
      "SOLAR" => "Solar",
      "SPACE" => "Space Heater",
      "STEAM" => "Steam/Hot Water",
      "ZONED" => "Zoned"
    },
    :interior_description => {
      "10CLM" => "10 ft+ Ceil Main",
      "10CLW" => "10 ft + Ceil Lower",
      "10CUP" => "10 ft + Ceil Upper",
      "2STFY" => "2-Story Foyer",
      "9CEIL" => "9 ft + Ceil Main",
      "9CLLW" => "9 ft + Ceil Lower",
      "9CLUP" => "9 ft+ Ceil Upper",
      "BOOKS" => "Bookcases",
      "CATHD" => "Cathedral Ceiling",
      "DBLVN" => "Double Vnty Other",
      "DISAT" => "Disp Attic Stairs",
      "DSLCN" => "High Speed Internet Available",
      "ELEVT" => "Elevator",
      "ENTFY" => "Entrance Foyer",
      "EXBMS" => "Exposed Beams",
      "HHCLS" => "His & Her Closets",
      "HRDWD" => "Hardwood Floors",
      "OTHER" => "Other",
      "PINFL" => "Pine Floor",
      "PRMAT" => "Perm Attic Stairs",
      "PRVFT" => "Priv Front Entry",
      "PRVTR" => "Priv Rear Entry",
      "REARS" => "Rear Stairs",
      "SAUNA" => "Sauna",
      "SKYLT" => "Skylights",
      "SPRNK" => "Fire Sprinklers",
      "TREYC" => "Trey Ceilings",
      "WETBR" => "Wet Bar",
      "WICLO" => "Walk-In Closet(s)",
      "WWCAR" => "Wall/Wall Carpet"
    },
    :lot_description => {
      "BORDR" => "Borders US/State Park",
      "CORNR" => "Corner",
      "CREEK" => "Creek",
      "CTYVW" => "City View",
      "CULDE" => "Cul-De-Sac",
      "DPWTDOC" => "Deepwater at Dock",
      "DPWTRAC" => "Deepwater Access",
      "FENCE" => "Fenced",
      "FLDPL" => "Flood Plain",
      "GLFVW" => "Golf View",
      "GOLFC" => "Golf Course",
      "LAGOON" => "Lagoon",
      "LEVEL" => "Level",
      "LKEFT" => "Lake Frontage",
      "LKEVW" => "Lake View",
      "LVLDR" => "Level Driveway",
      "MARSH" => "Marsh",
      "MTNFT" => "Mountain Frontage",
      "MTNVW" => "Mountain View",
      "OCNFT" => "Ocean Frontage",
      "OCNVW" => "Ocean View",
      "PASTR" => "Pasture",
      "PLTEN" => "Rm-Pool/Tennis",
      "POND" => "Pond",
      "PRIVT" => "Private Backyard",
      "RVRFT" => "River Frontage",
      "RVRVW" => "River View",
      "SLOPE" => "Sloped",
      "STEEP" => "Steep",
      "TIDAL" => "Tidal",
      "WOODD" => "Wooded"
    },
    :lot_improvements => {
      "BARN" => "Barn",
      "CRSFN" => "X-Fenced",
      "CURBS" => "Curbs & Gutters",
      "FENCE" => "Fenced",
      "IRRIG" => "Irrig Sys",
      "NONE" => "None",
      "OTHER" => "Other",
      "OUTBL" => "Misc Outbldgs",
      "SDEWL" => "Sidewalk",
      "STBLE" => "Stable",
      "STLGT" => "Street Lights",
      "UNGRD" => "Undergrnd Utils"
    },
    :mls_area_name => {
      "101" => "Rockdale County",
      "102" => "Rockdale County",
      "111" => "Cherokee County",
      "112" => "Cherokee County",
      "113" => "Cherokee County",
      "114" => "Cherokee County",
      "121" => "Dunwoody",
      "13" => "Fulton North",
      "131" => "Sandy Springs",
      "132" => "Sandy Springs",
      "14" => "Fulton North",
      "141" => "Walton County",
      "142" => "Walton County",
      "151" => "Newton County",
      "161" => "Clayton County",
      "171" => "Fayette County",
      "181" => "Coweta County",
      "191" => "Paulding County",
      "192" => "Paulding County",
      "201" => "Bartow County",
      "202" => "Bartow County",
      "203" => "Bartow County",
      "204" => "Bartow County",
      "21" => "Atlanta North",
      "211" => "Henry County",
      "22" => "Atlanta North",
      "221" => "Forsyth County",
      "222" => "Forsyth County",
      "223" => "Forsyth County",
      "224" => "Forsyth County",
      "23" => "Atlanta North",
      "231" => "Carroll County",
      "232" => "Heard",
      "24" => "Atlanta North",
      "241" => "Butts County",
      "242" => "Jasper",
      "243" => "Putnam",
      "251" => "Spalding County",
      "261" => "Hall County",
      "262" => "Hall County",
      "263" => "Hall County",
      "264" => "Hall County",
      "265" => "Hall County",
      "271" => "Dawson County",
      "272" => "Dawson County",
      "273" => "Dawson County",
      "275" => "White County",
      "278" => "Lumpkin County",
      "281" => "Banks County",
      "285" => "Habersham County",
      "286" => "Stephens County",
      "287" => "Rabun County",
      "291" => "Jackson County",
      "292" => "Jackson County",
      "293" => "Jackson County",
      "294" => "Madison",
      "295" => "Franklin",
      "301" => "Barrow County",
      "302" => "Barrow County",
      "31" => "Fulton South",
      "311" => "Oconee County",
      "32" => "Fulton South",
      "321" => "Morgan County",
      "322" => "Greene",
      "323" => "Oglethorpe",
      "33" => "Fulton South",
      "331" => "Pickens County",
      "332" => "Pickens County",
      "336" => "Gilmer County",
      "341" => "Gordon County",
      "342" => "Gordon County",
      "343" => "Murray",
      "345" => "Catoosa",
      "350" => "Whitfield",
      "351" => "Floyd-West Rome",
      "352" => "Floyd-Armuchee",
      "353" => "Floyd-Northeast",
      "354" => "Floyd-East Rome",
      "355" => "Floyd-South Rome",
      "356" => "Floyd-Downtown",
      "357" => "Chattooga",
      "358" => "Walker",
      "359" => "Dade",
      "361" => "Polk County",
      "371" => "Clarke County",
      "381" => "Haralson County",
      "400" => "Fannin",
      "404" => "Union",
      "408" => "Towns",
      "41" => "Dekalb-East",
      "412" => "Hart",
      "416" => "Elbert",
      "42" => "Dekalb-East",
      "420" => "Wilkes",
      "424" => "Lincoln",
      "428" => "Taliaferro",
      "43" => "Dekalb-East",
      "432" => "Warren",
      "436" => "Mcduffie",
      "440" => "Columbia",
      "444" => "Hancock",
      "448" => "Glascock",
      "452" => "Richmond",
      "460" => "Troup",
      "464" => "Meriwether",
      "468" => "Pike",
      "472" => "Lamar",
      "476" => "Monroe",
      "480" => "Jones",
      "484" => "Baldwin",
      "488" => "Washington",
      "492" => "Jefferson",
      "496" => "Burke",
      "500" => "Harris",
      "504" => "Talbot",
      "508" => "Upson",
      "51" => "Dekalb-West",
      "512" => "Crawford",
      "516" => "Bibb",
      "52" => "Dekalb-West",
      "524" => "Twiggs",
      "528" => "Wilkinson",
      "53" => "Dekalb-West",
      "532" => "Johnson",
      "536" => "Emanuel",
      "540" => "Jenkins",
      "544" => "Screven",
      "548" => "Muscogee",
      "556" => "Chattahoochee",
      "560" => "Marion",
      "564" => "Taylor",
      "568" => "Macon",
      "572" => "Peach",
      "576" => "Houston",
      "580" => "Bleckley",
      "584" => "Laurens",
      "588" => "Treutlen",
      "592" => "Candler",
      "596" => "Bulloch",
      "600" => "Effingham",
      "604" => "Stewart",
      "608" => "Webster",
      "61" => "Gwinnett County",
      "612" => "Schley",
      "616" => "Sumter",
      "62" => "Gwinnett County",
      "620" => "Dooly",
      "624" => "Pulaski",
      "628" => "Dodge",
      "63" => "Gwinnett County",
      "632" => "Wheeler",
      "636" => "Montgomery",
      "64" => "Gwinnett County",
      "640" => "Toombs",
      "644" => "Tattnall",
      "648" => "Evans",
      "65" => "Gwinnett County",
      "652" => "Bryan",
      "656" => "Chatham",
      "66" => "Gwinnett County",
      "664" => "Quitman",
      "668" => "Randolph",
      "672" => "Terrell",
      "676" => "Lee",
      "680" => "Crisp",
      "684" => "Wilcox",
      "688" => "Telfair",
      "692" => "Jeff Davis",
      "696" => "Appling County",
      "700" => "Wayne",
      "704" => "Long",
      "708" => "Liberty",
      "71" => "Cobb-West",
      "712" => "Mcintosh",
      "716" => "Clay",
      "72" => "Cobb-West",
      "720" => "Calhoun",
      "724" => "Dougherty",
      "73" => "Cobb-West",
      "732" => "Worth",
      "736" => "Turner",
      "74" => "Cobb-West",
      "740" => "Ben Hill",
      "744" => "Irwin",
      "748" => "Coffee",
      "75" => "Cobb-West",
      "752" => "Bacon",
      "756" => "Pierce",
      "760" => "Early",
      "764" => "Baker",
      "768" => "Miller",
      "772" => "Mitchell",
      "776" => "Colquitt",
      "780" => "Tift",
      "784" => "Cook",
      "788" => "Berrien",
      "792" => "Atkinson",
      "796" => "Ware",
      "800" => "Brantley",
      "804" => "Glynn",
      "81" => "Cobb-East",
      "812" => "Seminole",
      "816" => "Decatur",
      "82" => "Cobb-East",
      "820" => "Grady",
      "824" => "Thomas",
      "828" => "Brooks",
      "83" => "Cobb - East",
      "832" => "Lowndes",
      "836" => "Lanier",
      "840" => "Clinch",
      "844" => "Echols",
      "848" => "Charlton",
      "852" => "Camden",
      "900" => "Alaska",
      "901" => "Arizona",
      "902" => "Arkansas",
      "903" => "California",
      "904" => "Colorado",
      "905" => "Connecticut",
      "906" => "Delaware",
      "907" => "District Of Columbia",
      "908" => "Hawaii",
      "909" => "Idaho",
      "91" => "Douglas County",
      "910" => "Illinois",
      "911" => "Indiana",
      "912" => "Iowa",
      "913" => "Kansas",
      "914" => "Kentucky",
      "915" => "Louisiana",
      "916" => "Maine",
      "917" => "Maryland",
      "918" => "Massachusetts",
      "919" => "Michigan",
      "920" => "Minnesota",
      "921" => "Mississippi",
      "922" => "Missouri",
      "923" => "Montana",
      "924" => "Nebraska",
      "925" => "Nevada",
      "926" => "New Hampshire",
      "927" => "New Jersey",
      "928" => "New Mexico",
      "929" => "New York",
      "931" => "North Dakota",
      "932" => "Ohio",
      "933" => "Oklahoma",
      "934" => "Oregon",
      "935" => "Pennsylvania",
      "936" => "Rhode Island",
      "937" => "Texas",
      "938" => "Utah",
      "939" => "Vermont",
      "940" => "Virginia",
      "941" => "Washington",
      "942" => "West Virginia",
      "943" => "Wisconsin",
      "944" => "Wyoming",
      "945" => "South Dakota",
      "950" => "Tennessee",
      "960" => "North Carolina",
      "970" => "South Carolina",
      "980" => "Florida",
      "990" => "Alabama"
    },
    :mls_elementary_school => {
      "ABBOTTS" => "Abbotts Hill",
      "ABNEY" => "WC Abney",
      "ACWORTH" => "Acworth Intermediate",
      "ADAIRSVIL" => "Adairsville",
      "ADAMSVLLE" => "Adamsville",
      "ADDISON" => "Addison",
      "ADRIANOF" => "Adrian of Performing Arts (PK-08)",
      "ALASKAOTH" => "Alaska - Other",
      "ALCOVA" => "Alcova",
      "ALEXANDE" => "Alexander Ii",
      "ALFORD" => "Alford",
      "ALICECO" => "Alice Coachman",
      "ALLATOONA" => "Allatoona",
      "ALLENEL" => "Allen ",
      "ALLGOOD" => "Allgood - Dekalb",
      "ALLGOODP" => "Allgood - Paulding",
      "ALPHARETTA" => "Alpharetta",
      "ALPSROA" => "Alps Road",
      "ALTAMA" => "Altama",
      "ALTAMAHA" => "Altamaha",
      "ALTOPARK" => "Alto Park",
      "AMANA" => "Amana Academy",
      "AMBROSE" => "Ambrose",
      "ANDERSON" => "Anderson",
      "ANDERSONL" => "Anderson-Livsey",
      "ANNAKDAVIE" => "Anna K. Davie",
      "ANNEST" => "Anne Street",
      "ANNETTEWINN" => "Annette Winn",
      "ANNIEBE" => "Annie Belle Clark Primary",
      "ANNISTOWN" => "Annistown",
      "ANTIOCH" => "Antioch",
      "APPLING" => "Appling County",
      "APPLINGOTH" => "Appling - Other",
      "ARBORSPGS" => "Arbor Springs",
      "ARBORSTAT" => "Arbor Station",
      "ARCADO" => "Arcado",
      "ARGYLE" => "Argyle",
      "ARIZONAOTH" => "Arizona - Other",
      "ARKANSASOTH" => "Arkansas - Other",
      "ARMUCHEE" => "Armuchee",
      "ARNCOSARGARNCO" => "Arnco-Sargent",
      "ARNOLD" => "Arnold",
      "ARNOLDMILL" => "Arnold Mill",
      "ASAHILLIARD" => "Asa Hilliard",
      "ASCLARK" => "A. S. Clark",
      "ASHFRDPR" => "Ashford Park",
      "ATHARD" => "Atha Road",
      "ATKINSONB" => "Atkinson - Brantley",
      "ATKINSONC" => "Atkinson - Coweta",
      "ATKINSONOTH" => "Atkinson - Other",
      "ATKINSONS" => "Atkinson - Spalding",
      "AUBURN" => "Auburn",
      "AUSTELL" => "Austell",
      "AUSTELLPRIMARY" => "Austell Primary",
      "AUSTIN" => "Austin",
      "AUSTINRD" => "Austin Road",
      "AVERY" => "Avery",
      "AVONDALEE" => "Avondale",
      "BACONCO" => "Bacon County",
      "BACONEL" => "Bacon ",
      "BACONOTH" => "Bacon - Other",
      "BACONTON" => "Baconton Community Charter(PK - 09)",
      "BAGGETT" => "Baggett",
      "BAKER" => "Baker",
      "BAKERCO" => "Baker County",
      "BAKEROTH" => "Baker - Other",
      "BAKERPL" => "Baker Place",
      "BALDWIN" => "Baldwin",
      "BALDWINOTH" => "Baldwin - Other",
      "BALLGROND" => "Ball Ground",
      "BANKSCO" => "Banks County",
      "BANKSCOUP" => "Banks County Upper",
      "BARDEN" => "Barden",
      "BARKSDALE" => "Barksdale",
      "BARNETTSH" => "Barnett Shoals",
      "BARNWELL" => "Barnwell",
      "BAROT" => "Bartow - Other",
      "BARROW" => "Barrow",
      "BARTONC" => "Barton Chapel ",
      "BARTOW" => "Bartow",
      "BASCOMB" => "Bascomb",
      "BATTLEFI" => "Battlefield",
      "BAYCREEK" => "Bay Creek",
      "BAYVALE" => "Bayvale",
      "BEAVERBRK" => "Beaverbrook",
      "BEAVERDA" => "Beaverdam",
      "BEAVERDALE" => "Beaverdale",
      "BEAVERRDG" => "Beaver Ridge",
      "BECHRHLLS" => "Beecher Hills",
      "BELAIR" => "Bel Air",
      "BELLSFRR" => "Bells Ferry",
      "BELMTHLLS" => "Belmont Hills",
      "BELWOOD" => "Belwood",
      "BENEFIELD" => "Benefield",
      "BENHILL" => "Ben Hill",
      "BENHILLOTH" => "Ben Hill - Other",
      "BENNING" => "Benning Hills ",
      "BENTEEN" => "Benteen",
      "BENTON" => "Benton",
      "BERKELYL" => "Berkeley Lake",
      "BERND" => "Bernd",
      "BERRIEN" => "Berrien",
      "BERRIENOTH" => "Berrien - Other",
      "BERTAWE" => "Berta Weathersbee ",
      "BESSIELBAGGETT" => "Bessie L. Baggett",
      "BETHESDA" => "Bethesda",
      "BETHLEHEM" => "Bethlehem - Barrow",
      "BETHLEHEMH" => "Bethlehem - Henry",
      "BETHUNE" => "Bethune - Charlton",
      "BETHUNECP" => "Bethune - College Park",
      "BETHUNEF" => "Bethune - Atlanta",
      "BEULAH" => "Beulah",
      "BIBBOTH" => "Bibb - Other",
      "BIGA" => "Big A",
      "BIGCREEK" => "Big Creek",
      "BIGSHANTY" => "Big Shanty",
      "BILLARP" => "Bill Arp",
      "BIRMINGHAM" => "Birmingham Falls",
      "BIRNEY" => "Birney",
      "BLACKBURN" => "Blackburn",
      "BLACKSHE" => "Blackshear",
      "BLACKSMILL" => "Blacks Mill",
      "BLACKWELL" => "Blackwell - Cobb",
      "BLAINE" => "Harmony - Walton",
      "BLAKENEY" => "Blakeney",
      "BLALCOCK" => "Blalock",
      "BLANCHAR" => "Blanchard",
      "BLANDYH" => "Blandy Hills",
      "BLANFORD" => "Blandford",
      "BLECKLEY" => "Bleckley County",
      "BLECKLEYOTH" => "Bleckley - Other",
      "BLKSHRTRL" => "Blackshear Trail",
      "BLKWELLELB" => "Blackwell - Elbert",
      "BLMINGDLE" => "Bloomingdale",
      "BLUERDGC" => "Blue Ridge - Columbia",
      "BLUERDGF" => "Blue Ridge - Fannin",
      "BLUERIDGE" => "Blue Ridge - Whitfield",
      "BLYTHEE" => "Blythe",
      "BMATHIS" => "Bob Mathis",
      "BOLTONA" => "Bolton Academy",
      "BONAIRE" => "Bonaire ",
      "BOSTON" => "Boston",
      "BOWDON" => "Bowdon",
      "BOWMAN" => "Bowman",
      "BOYD" => "Boyd",
      "BOYNTON" => "Boynton",
      "BRAELINN" => "Braelinn",
      "BRAMLETT" => "Bramlett",
      "BRANDON" => "Brandon",
      "BRANTLEYOTH" => "Brantley - Other",
      "BREWERE" => "Brewer ",
      "BRIARLAKE" => "Briarlake",
      "BRIARVSTA" => "Briar Vista",
      "BRIGHTSTAR" => "Bright Star",
      "BRITTDA" => "Britt David",
      "BROCKETT" => "Brockett",
      "BROOKDAL" => "Brookdale",
      "BROOKLET" => "Brooklet",
      "BROOKS" => "Brooks - Fayette",
      "BROOKSC" => "Brooks - Coweta",
      "BROOKSOTH" => "Brooks - Other",
      "BROOKVIEW" => "Brookview",
      "BROOKWDC" => "Brookwood - Columbia",
      "BROOKWOO" => "Brookwood - Gwinnett",
      "BROOKWOOD" => "Brookwood - Whitfield",
      "BROOKWOODF" => "Brookwood - Forsyth",
      "BROWNCB" => "Brown - Cobb",
      "BROWNSMIL" => "Browns Mill",
      "BROXTONMAR" => "Broxton-Mary Hayes",
      "BRUCE" => "Bruce",
      "BRUMBY" => "Brumby",
      "BRWOT" => "Barrow - Other",
      "BRYANCO" => "Bryan County",
      "BRYANOTH" => "Bryan - Other",
      "BRYANT" => "Bryant - Cobb",
      "BRYANTBUL" => "Julia P. Bryant",
      "BUCHANAN" => "Buchanan",
      "BUFORD" => "Buford",
      "BUFORDA" => "Buford Academy",
      "BULLARD" => "Bullard",
      "BULLOCHOTH" => "Bulloch - Other",
      "BURCH" => "Robert J. Burch",
      "BURGESSPE" => "Burgess-Peterson",
      "BURGHARD" => "Burghard",
      "BURKE" => "Burke",
      "BURKEOTH" => "Burke - Other",
      "BURNETT" => "Burnett",
      "BURNETTE" => "Burnette",
      "BURNTHICKORY" => "Burnt Hickory",
      "BURROUGH" => "Burroughs-Molette",
      "BURRUSS" => "Burruss",
      "BURWELL" => "Burwell",
      "BUTLER" => "Butler",
      "BUTTONG" => "Button Gwinnett ",
      "BYRONEL" => "Byron ",
      "CALHOUNC" => "Calhoun County",
      "CALHOUNG" => "Calhoun",
      "CALHOUNOTH" => "Calhoun - Other",
      "CALIFORNIAOTH" => "California - Other",
      "CALLAWAY" => "Callaway - Clayton",
      "CALLAWAYT" => "Callaway - Troup",
      "CAMDENOTH" => "Camden - Other",
      "CAMPBELL" => "Campbell",
      "CAMPCREEK" => "Camp Creek",
      "CANBYLANE" => "Canby Lane",
      "CANDLEROTH" => "Candler - Other",
      "CANNONS" => "Cannon Street ",
      "CANONGATE" => "Canongate",
      "CANTON" => "Canton",
      "CAPVIEW" => "Capitol View",
      "CARMEL" => "Carmel",
      "CARNESVI" => "Carnesville",
      "CAROBERTS" => "C.A. Roberts",
      "CARROLLT" => "Carrollton",
      "CARTER" => "Carter",
      "CARTERSE" => "Cartersville ",
      "CARTERSP" => "Cartersville Primary",
      "CARVERJ" => "Carver - Jefferson",
      "CARVERT" => "Carver - Terrell",
      "CARYREYN" => "Cary Reynolds",
      "CASCADE" => "Cascade",
      "CAVESPRING" => "Cave Spring",
      "CEDARHILL" => "Cedar Hill",
      "CEDARRDG" => "Cedar Ridge - Whitfield",
      "CEDARRIDGE" => "Cedar Ridge - Columbia",
      "CEDRGROVE" => "Cedar Grove",
      "CENTAL" => "Central - Talbot",
      "CENTENNIAL" => "Centennial Arts Academy",
      "CENTENPL" => "Centennial Place",
      "CENTERE" => "Center ",
      "CENTERVI" => "Centerville - Houston",
      "CENTERVILL" => "Centerville - Gwinnett",
      "CENTRALC" => "Central - Carroll",
      "CENTRALFRANKLIN" => "Central Franklin",
      "CENTRALH" => "Centralhatchee",
      "CHALKER" => "Chalker",
      "CHAPELHLDE" => "Chapel Hill - Dekalb",
      "CHAPELHLDO" => "Chapel Hill - Douglas",
      "CHAPMAN" => "Chapman Intermediate",
      "CHARLES" => "Charles Spencer ",
      "CHARLESBRA" => "Charles Brant Chesney",
      "CHARLTONOTH" => "Charlton - Other",
      "CHASEST" => "Chase Street",
      "CHATHAMOTH" => "Chatham - Other",
      "CHATSWOR" => "Chatsworth",
      "CHATTAHO" => "Chattahoochee County",
      "CHATTAHOO" => "Chattahoochee - Gwinnett",
      "CHATTAHOOC" => "Chattahoochee - Forsyth",
      "CHATTAHOOCHEEOTH" => "Chattahoochee - Other",
      "CHATTANOOG" => "Chattanooga Valley",
      "CHEATHAM" => "Cheatham Hill",
      "CHEROKEEP" => "Cherokee - Polk",
      "CHEROKEERI" => "Cherokee Ridge",
      "CHEROKEES" => "Cherokee - Sumter",
      "CHESTATEE" => "Chestatee",
      "CHESTNUT" => "Chesnut",
      "CHESTNUTMT" => "Chestnut Mountain",
      "CHICKAMAUG" => "Chickamauga",
      "CHICOPEEWOODS" => "Chicopee",
      "CHROT" => "Cherokee - Other",
      "CHURCHST" => "Church Street",
      "CITYPARK" => "City Park",
      "CLAIREMONT" => "Clairemont",
      "CLARKCREEK" => "Clark Creek",
      "CLARKDALE" => "Clarkdale",
      "CLARKESV" => "Clarkesville",
      "CLAXTON" => "Claxton",
      "CLAY" => "Clay",
      "CLAYCOU" => "Clay County",
      "CLAYOTH" => "Clay - Other",
      "CLAYTON" => "Clayton",
      "CLEARCREEK" => "Clear Creek",
      "CLEVELAND" => "Cleveland - Fulton",
      "CLEVELANDF" => "Cleveland - Fayette",
      "CLIFTON" => "Clifton",
      "CLINCHC" => "Clinch County",
      "CLINCHOTH" => "Clinch - Other",
      "CLOUDSP" => "Cloud Springs",
      "CLOVERLEAF" => "Cloverleaf",
      "CLUBVIEW" => "Clubview ",
      "CLVCLK" => "Cleveland Road",
      "CLYATTVI" => "Clyattville ",
      "CLYOT" => "Clayton - Other",
      "COALMT" => "Coal Mountain",
      "COBOT" => "Cobb - Other",
      "COFFEEOTH" => "Coffee - Other",
      "COGBURNWOO" => "Cogburn Woods",
      "COHUTTA" => "Cohutta",
      "COKER" => "Coker",
      "COLBERT" => "Colbert",
      "COLGEHGTS" => "College Heights",
      "COLHAMFERR" => "Colham Ferry",
      "COLLEGEPA" => "College Park",
      "COLLINSR" => "Collins - Richmond",
      "COLLINST" => "Collins - Tattnall",
      "COLORADOOTH" => "Colorado - Other",
      "COLQUITTOTH" => "Colquitt - Other",
      "COLUMBIAE" => "Columbia",
      "COLUMBIAOTH" => "Columbia - Other",
      "COMER" => "Comer",
      "COMMERCE" => "Commerce",
      "COMPTON" => "Compton",
      "CONLEYHLS" => "Conley Hills",
      "CONNALLY" => "Connally",
      "CONNECTICUTOTH" => "Connecticut - Other",
      "CONNIEDUGAN" => "Connie Dugan",
      "CONTLCLN" => "Continental Colony",
      "COOK" => "Cook - Fulton",
      "COOKCOOK" => "Cook - Cook",
      "COOKOTH" => "Cook - Other",
      "COPELAND" => "Copeland ",
      "CORLEY" => "Corley",
      "CORNELIA" => "Cornelia",
      "COTTONIND" => "Cotton Indian",
      "COUNTYLIN" => "County Line",
      "COWANRD" => "Cowan Road",
      "COWOT" => "Coweta - Other",
      "COX" => "Cox",
      "CRABAPPLE" => "Crabapple Crossing",
      "CRABAPPLELANE" => "Crabapple Lane",
      "CRAIG" => "Craig",
      "CRAIGHO" => "Craig-Houghton ",
      "CRAWFORD" => "Crawford County",
      "CRAWFORDOTH" => "Crawford - Other",
      "CREEKSID" => "Creekside",
      "CREEKVIEW" => "Creek View",
      "CRESCENT" => "Crescent Road",
      "CRISPCOU" => "Crisp County",
      "CRISPOTH" => "Crisp - Other",
      "CROOKED" => "Crooked River",
      "CROSSCR" => "Cross Creek ",
      "CROSSROADS" => "Crossroads Academy",
      "CROSSROADSC" => "Crossroads",
      "CROWNCL" => "Brown - Clayton",
      "CTWALKER" => "C T Walker",
      "CUMMING" => "Cumming",
      "CUSSETA" => "Cusseta Road ",
      "DACULA" => "Dacula",
      "DADE" => "Dade",
      "DALLAS" => "Dallas",
      "DAMESFE" => "Dames Ferry ",
      "DANIELSV" => "Danielsville",
      "DAVESCREEK" => "Daves Creek",
      "DAVIDL" => "David L Rainer",
      "DAVIS" => "Davis - Cobb",
      "DAVISDADE" => "Davis - Dade",
      "DAVISEL" => "Davis - Muscogee",
      "DAWNVILLE" => "Dawnville",
      "DAWOT" => "Dawson - Other",
      "DAWSONE" => "Dawson ",
      "DEARING" => "Dearing ",
      "DECATUROTH" => "Decatur - Other",
      "DEERCHASE" => "Deer Chase",
      "DEERWOOD" => "Deerwood Academy",
      "DEKALBPATHACADEMY" => "Dekalb Path Academy",
      "DEKOT" => "Dekalb - Other",
      "DELAWAREOTH" => "Delaware - Other",
      "DEMOREST" => "Demorest",
      "DEWAREL" => "Dewar ",
      "DIAMONDLAKES" => "Diamond Lakes",
      "DIMONEL" => "Dimon ",
      "DOBBS" => "Dobbs",
      "DOCOTH" => "District Of Columbia - Other",
      "DODGEOTH" => "Dodge - Other",
      "DOERUN" => "Doerun",
      "DOLVIN" => "Dolvin",
      "DOOLY" => "Dooly County",
      "DOOLYOTH" => "Dooly - Other",
      "DOROTHYH" => "Dorothy Height",
      "DOUBLEC" => "Double Churches ",
      "DOUGHERTYOTH" => "Dougherty - Other",
      "DOUOT" => "Douglas - Other",
      "DOVESCR" => "Doves Creek",
      "DOWELL" => "Dowell",
      "DOWNTOWN" => "Downtown",
      "DRESDEN" => "Dresden",
      "DRGEOR" => "Dr. George Washington Carver",
      "DRSTHLS" => "Dorsett Shoals",
      "DSTANTON" => "D. H. Stanton",
      "DUEWEST" => "Due West",
      "DUGGAP" => "Dug Gap",
      "DUNAIRE" => "Dunaire",
      "DUNBAR" => "Dunbar",
      "DUNCANCREEK" => "Duncan Creek",
      "DUNLEITH" => "Dunleith",
      "DUNWDYSPGS" => "Dunwoody Springs",
      "DUNWOODY" => "Dunwoody",
      "DUTCHTOWN" => "Dutchtown",
      "DYER" => "Dyer",
      "EAGLERIDGE" => "Eagle Ridge",
      "EAGLERIDGEA" => "Eagle Ridge Academy",
      "EAGLESP" => "Eagle Springs ",
      "EARLYCO" => "Early County",
      "EARLYOTH" => "Early - Other",
      "EASTANOL" => "Eastanollee",
      "EASTBRO" => "East Broad Street",
      "EASTCLAY" => "East Clayton",
      "EASTDCOBB" => "East Side",
      "EASTFAN" => "East Fannin",
      "EASTJACKSO" => "East Jackson",
      "EASTLAKEF" => "East Lake - Fulton",
      "EASTLAKEH" => "East Lake - Henry",
      "EASTLAU" => "East Laurens ",
      "EASTNEWTON" => "East Newton",
      "EASTSDCOWT" => "Eastside - Coweta",
      "EASTSIDE" => "Eastside - Whitfield",
      "EASTSIDED" => "Eastside - Douglas",
      "EASTVALLEY" => "Eastvalley",
      "EBENEZER" => "Ebenezer",
      "EBOUIE" => "Edward L. Bouie Sr.",
      "ECENTRAL" => "East Central",
      "ECHOLSC" => "Echols County",
      "ECHOLSOTH" => "Echols - Other",
      "ECWEST" => "E.C. West",
      "EDGEWOOD" => "Edgewood ",
      "EDMONDS" => "Edmonds",
      "EFFINGHAMOTH" => "Effingham - Other",
      "ELBERTCO" => "Elbert County",
      "ELBERTOTH" => "Elbert - Other",
      "ELCANKING" => "Elcan-King",
      "ELDRIDGEM" => "Eldridge L. Miller",
      "ELLIJAY" => "Ellijay",
      "ELLIS" => "Ellis",
      "ELMST" => "Elm Street - Coweta",
      "ELMSTREET" => "Elm Street - Floyd",
      "EMANUELOTH" => "Emanuel - Other",
      "EMERSON" => "Emerson",
      "ENOTA" => "Enota Multiple Intelligences Academy",
      "EPHESUS" => "Ephesus",
      "ERIVERS" => "Rivers - Fulton",
      "ESCOFFEE" => "Eastside - Coffee",
      "ESGRADY" => "Eastside - Grady",
      "ESPOLK" => "Eastside - Polk",
      "ESTHERJACKSON" => "Jackson - Fulton",
      "ETHELKN" => "Ethel W. Kight",
      "ETON" => "Eton",
      "EUCHEEC" => "Euchee Creek",
      "EUHARLEE" => "Euharlee",
      "EVANS" => "Evans",
      "EVANSDALE" => "Evansdale",
      "EVANSOTH" => "Evans - Other",
      "FACTORYSH" => "Factory Shoals",
      "FAIN" => "Fain",
      "FAIRINGTON" => "Fairington",
      "FAIRMOUNT" => "Fairmount",
      "FAIROAKS" => "Fair Oaks",
      "FAIRST" => "Fair Street International Baccalaureate",
      "FAIRVIEW" => "Fairview - Henry",
      "FAIRVIEWN" => "Fairview - Newton",
      "FAIRYLAND" => "Fairyland",
      "FALLING" => "Falling Creek",
      "FANNINOTH" => "Fannin - Other",
      "FARGOCH" => "Fargo Charter (Kk-03)",
      "FAYETTEVIL" => "Fayetteville",
      "FAYOT" => "Fayette - Other",
      "FELDWOOD" => "Feldwood",
      "FERGUSON" => "Ferguson",
      "FERNBANK" => "Fernbank",
      "FICKETT" => "Fickett",
      "FICQUETT" => "Ficquett",
      "FINCH" => "Finch",
      "FINDLEYOAK" => "Findley Oaks",
      "FLATROCK" => "Flat Rock",
      "FLATSHLSD" => "Flat Shoals - Dekalb",
      "FLATSHLSR" => "Flat Shoals - Rockdale",
      "FLINTHILL" => "Flint Hill",
      "FLIPPEN" => "Flippen",
      "FLOWERYBR" => "Flowery Branch",
      "FOLKSTON" => "Folkston",
      "FORD" => "Ford",
      "FORESTHILL" => "Forest Hills Academy",
      "FOROT" => "Forsyth - Other",
      "FORREST" => "Forrest Road ",
      "FORTDANIEL" => "Fort Daniel",
      "FOUNTAIN" => "Fountain",
      "FOURTHD" => "Fourth District",
      "FOWLERD" => "Fowler Drive",
      "FOXELEM" => "Fox ",
      "FRANKLINFOREST" => "Franklin Forest",
      "FRANKLO" => "Frank Long ",
      "FREDATOOMER" => "Toomer",
      "FREEDOM" => "Freedom Park ",
      "FREEHOME" => "Free Home",
      "FREEMAN" => "Freeman ",
      "FREY" => "Frey",
      "FRIENDSHIP" => "Friendship",
      "FRMANSML" => "Freeman's Mill",
      "FSTANTON" => "F.L. Stanton",
      "FULOT" => "Fulton - Other",
      "FUNSTON" => "Funston",
      "FUTRAL" => "Futral Road",
      "FVHABER" => "Fairview - Habersham",
      "GADSDEN" => "Gadsden",
      "GAINES" => "Gaines",
      "GAINESVI" => "Gainesville Exploration Academy",
      "GARDENC" => "Garden City",
      "GARDENLAKE" => "Garden Lakes",
      "GARDNHLLS" => "Garden Hills",
      "GARRETT" => "Garrett ",
      "GARRISONC" => "Garrison",
      "GARRISONT" => "Garrison-Pilcher ",
      "GARSNMILL" => "Garrison Mill",
      "GENTIAN" => "Gentian ",
      "GEORGEE" => "George E. Washington ",
      "GEORGETOWNC" => "Georgetown - Chatham",
      "GEORGETOWNM" => "Georgetown - Muscogee",
      "GIDEONS" => "Gideons",
      "GILBERT" => "Gilbert",
      "GLANTON" => "Glanton",
      "GLANTONHIND" => "Glanton-Hindsman",
      "GLASCOCK" => "Glascock County",
      "GLASCOCKOTH" => "Glascock - Other",
      "GLENNHI" => "Glenn Hills ",
      "GLENNVIL" => "Glennville ",
      "GLENWOOD" => "New Glennwood",
      "GLENWOODPR" => "Glenwood Primary",
      "GLYNDALE" => "Glyndale",
      "GLYNNOTH" => "Glynn - Other",
      "GOBAI" => "G.O. Bailey Primary",
      "GODLYST" => "Godley Station",
      "GOLDENI" => "Golden Isles",
      "GOODYRG" => "Goodyear",
      "GOSHENE" => "Goshen ",
      "GOULD" => "Gould",
      "GRACEWOO" => "Gracewood ",
      "GRADYOTH" => "Grady - Other",
      "GRANTVILLE" => "Grantville",
      "GRAVES" => "Graves",
      "GRAYELE" => "Gray ",
      "GRAYSON" => "Grayson",
      "GRAYSVIL" => "Graysville",
      "GREENACRES" => "Green Acres",
      "GREENBRI" => "Greenbrier",
      "GREENSBO" => "Greensboro",
      "GREER" => "Greer",
      "GROVEPRK" => "Woodson/Grove Park",
      "GROVETOW" => "Grovetown",
      "GULLATT" => "Gullatt",
      "GUMSPRINGS" => "Gum Springs",
      "GUYTON" => "Guyton",
      "GWINOAKS" => "Gwin Oaks",
      "GWIOT" => "Gwinnett - Other",
      "HAHIRAE" => "Hahira ",
      "HAINSEL" => "Dorothy Hains",
      "HAJONES" => "Jones - Haralson",
      "HALOT" => "Hall - Other",
      "HAMBRICK" => "Hambrick",
      "HAMILTON" => "Hamilton Crossing",
      "HAMILTONC" => "Hamilton",
      "HAMILTONEH" => "Hamilton E. Holmes",
      "HAMPTO" => "Hampton L. Daughtry",
      "HAMPTON" => "Hampton",
      "HANNANE" => "Hannan ",
      "HAPEVILLE" => "Hapeville",
      "HARBINS" => "Harbins",
      "HARMONY" => "Harmony - Gwinnett",
      "HARMONYLELAND" => "Harmony-Leland",
      "HARMONYP" => "Harmony - Pickens",
      "HARPE" => "Harper - Clayton",
      "HARPER" => "Harper - Thomas",
      "HARRISG" => "Harris",
      "HARRISOTH" => "Harris - Other",
      "HARTLEY" => "Hartley",
      "HARTOTH" => "Hart - Other",
      "HARTWELL" => "Hartwell ",
      "HAVEN" => "Haven",
      "HAVENACADE" => "Haven Academy",
      "HAWAIIOTH" => "Hawaii - Other",
      "HAWCREEK" => "Haw Creek",
      "HAWTHORNE" => "Hawthorne - Dekalb",
      "HAWTHRNE" => "Hawthorne - Clayton",
      "HAYES" => "Hayes",
      "HAYNIE" => "Haynie",
      "HAZELGR" => "Hazel Grove",
      "HCKRYFLAT" => "Hickory Flat - Cherokee",
      "HEARDB" => "Heard - Bibb",
      "HEARDC" => "Heard - Chatham",
      "HEARDH" => "Heard County",
      "HEARDMIXONHEARD" => "Heard-Mixon",
      "HEARDSFER" => "Heards Ferry",
      "HEMBREESPRGS" => "Hembree Springs",
      "HENDERSONM" => "Henderson Mill",
      "HENDRICKS" => "Hendricks",
      "HENDRIXDR" => "Hendrix Drive",
      "HENOT" => "Henry - Other",
      "HEPHZIBA" => "Hephzibah ",
      "HERITAGE" => "Heritage - Fulton",
      "HERITAGEACADEMY" => "Heritage Academy",
      "HERITAGEB" => "Heritage - Bibb",
      "HERNDON" => "Herndon",
      "HESSE" => "Hesse",
      "HICKORYHLS" => "Hickory Hills",
      "HICKRYFLAT" => "Hickory Flat - Henry",
      "HICKS" => "Hicks",
      "HIGHPOINT" => "High Point",
      "HIGHSHOALS" => "High Shoals - Clarke",
      "HIGHSHOALSO" => "High Shoals - Oconee",
      "HIGHTOWERD" => "Hightower",
      "HIGHTOWERR" => "Hightower Trail",
      "HILL" => "C. W. Hill",
      "HILLCITY" => "Hill City",
      "HILLCRES" => "Hillcrest - Troup",
      "HILLCREST" => "Hillcrest - Laurens",
      "HILLSIDE" => "Hillside",
      "HILLTOP" => "Hilltop",
      "HIRAM" => "Hiram",
      "HOBOKEN" => "Hoboken",
      "HODGE" => "Hodge",
      "HOGANSVI" => "Hogansville ",
      "HOLLISH" => "Hollis Hand ",
      "HOLLYDALE" => "Hollydale",
      "HOLLYSPGSC" => "Holly Springs - Cherokee",
      "HOLLYSPGSD" => "Holly Springs - Douglas",
      "HOLSENBECK" => "Holsenbeck",
      "HONEYCRK" => "Honey Creek",
      "HOODAVE" => "Fayetteville Intermediate",
      "HOPE" => "Hope-Hill",
      "HOPKINS" => "Hopkins",
      "HORNSBY" => "W.S. Hornsby",
      "HOUSE" => "House",
      "HOUSTONOTH" => "Houston - Other",
      "HOWARD" => "Howard",
      "HOWARDBSTROUD" => "Howard B. Stroud",
      "HUBBARD" => "Samuel E. Hubbard",
      "HUDDLESTON" => "Huddleston",
      "HUIE" => "Huie",
      "HULLSAN" => "Hull-Sanford",
      "HUMPHRIES" => "Humphries",
      "HUNTBUR" => "Burdell",
      "HUNTELE" => "Hunt ",
      "HUNTLEYHLLS" => "Huntley Hills",
      "HUTCHENS" => "Hal Hutchens",
      "HUTCHINSON" => "Hutchinson",
      "HWY341" => "Hwy 341",
      "IDAHOOTH" => "Idaho - Other",
      "IDLEWOOD" => "Idlewood",
      "ILA" => "Ila",
      "ILLINOISOTH" => "Illinois - Other",
      "IMAGINEWESLEY" => "Imagine Wesley Intl Academy",
      "INDIANAOTH" => "Indiana - Other",
      "INDIANC" => "Indian Creek - Coffee",
      "INDIANCRK" => "Indian Creek - Dekalb",
      "INDIANKNOLL" => "Indian Knoll",
      "INGRAMP" => "Ingram/Pye",
      "INMANROAD" => "Inman",
      "INTERNATSE" => "International Studies",
      "IOWAOTH" => "Iowa - Other",
      "IRWINCO" => "Irwin County ",
      "IRWINOTH" => "Irwin - Other",
      "ISLANDS" => "Islands",
      "ISLEOF" => "Isle Of Hope",
      "ISON" => "Ison Springs",
      "ITHICA" => "Ithica",
      "IVYCREEK" => "Ivy Creek",
      "JAALFORD" => "J. A. Alford",
      "JACKPNIXPR" => "Jack P. Nix Primary",
      "JACKSON" => "Jackson - Atlanta",
      "JACKSONB" => "Jackson - Butts",
      "JACKSONG" => "Jackson - Gwinnett",
      "JACKSONH" => "Jackson Heights",
      "JACKSONRD" => "Jackson Road",
      "JACOBGS" => "Jacob G. Smith",
      "JAMESJA" => "James Jackson",
      "JAMESTOW" => "Jamestown ",
      "JASPER" => "Jasper",
      "JASPERCOUNTY" => "Jasper County",
      "JDYER" => "J. G. Dyer",
      "JEFFDAV" => "Jeff Davis ",
      "JEFFDAVISOTH" => "Jeff Davis - Other",
      "JEFFERSON" => "Jefferson Parkway",
      "JEFFERSONJ" => "Jefferson",
      "JEFFERSONOTH" => "Jefferson - Other",
      "JEFFERSONV" => "Jeffersonville",
      "JENKINSGWIN" => "Jenkins",
      "JENKINSJ" => "Jenkins County ",
      "JENKINSOTH" => "Jenkins - Other",
      "JENKINSR" => "Jenkins-White ",
      "JERGER" => "Jerger",
      "JESUPEL" => "Jesup ",
      "JJHARRIS" => "Judia Jackson Harris",
      "JOHNJOHNSO" => "John Johnson",
      "JOHNSCREEK" => "Johns Creek",
      "JOHNSON" => "Johnson - Floyd",
      "JOHNSONJ" => "Johnson County ",
      "JOHNSONM" => "Johnson - Muscogee",
      "JOHNSONOTH" => "Johnson - Other",
      "JOHNSTON" => "Johnston",
      "JOLLY" => "Jolly",
      "JONESB" => "Jones - Bibb",
      "JONESOTH" => "Jones - Other",
      "JONESWHEAT" => "Jones-Wheat",
      "JORDANHILL" => "Jordan Hill Road",
      "JORDYEB" => "Jordye Bacon ",
      "JOSEPHM" => "Joseph Martin ",
      "JSPATE" => "J. S. Pate",
      "JTREDDICK" => "J.T. Reddick ",
      "KANOHEDA" => "Kanoheda",
      "KANSASOTH" => "Kansas - Other",
      "KAYRD" => "Kay Road",
      "KBSUTTON" => "Katherine B. Sutton",
      "KEDRON" => "Kedron",
      "KEHELEY" => "Keheley",
      "KELLEYLK" => "Kelley Lake",
      "KELLYMILL" => "Kelly Mill",
      "KEMP" => "Kemp - Clayton",
      "KEMPC" => "Kemp - Cobb",
      "KENNEDY" => "Kennedy",
      "KENNESAW" => "Kennesaw",
      "KENTUCKYOTH" => "Kentucky - Other",
      "KEYELEM" => "Key ",
      "KILOUGH" => "Kilough",
      "KILPATRICK" => "Kilpatrick",
      "KIMBERLY" => "Kimberly",
      "KINCAID" => "Kincaid",
      "KINGSCH" => "Kings Chapel ",
      "KINGSLAN" => "Kingsland",
      "KINGSLEY" => "Kingsley",
      "KINGSPGS" => "King Springs",
      "KINGSTON" => "Kingston",
      "KITTREDGE" => "Kittredge Magnet",
      "KNIGHT" => "Knight",
      "KNOLLWOOD" => "Knollwood",
      "KNOX" => "J. Knox",
      "LABELLE" => "Labelle",
      "LAKECITY" => "Lake City",
      "LAKEFOR" => "Lake Forest Hills ",
      "LAKEFOREST" => "Lake Forest",
      "LAKEJOY" => "Lake Joy",
      "LAKEPARKD" => "Lake Park - Dougherty",
      "LAKEPARKL" => "Lake Park - Lowndes",
      "LAKERIDGE" => "Lake Ridge",
      "LAKEWINDW" => "Lake Windward",
      "LAMARCO" => "Lamar County",
      "LAMAREL" => "Lamar-Milledge",
      "LAMAROTH" => "Lamar - Other",
      "LAMARRE" => "Lamar Reese",
      "LANE" => "Lane",
      "LANGSTON" => "Langston Chapel",
      "LANGSTONROAD" => "Langston Road",
      "LANIER" => "Lanier",
      "LANIERC" => "Lanier County ",
      "LANIEROTH" => "Lanier - Other",
      "LANIERP" => "Lanier Primary",
      "LARGOTIBET" => "Largo-Tibet",
      "LAURELRDG" => "Laurel Ridge",
      "LAURENSOTH" => "Laurens - Other",
      "LAVONIA" => "Lavonia",
      "LEE" => "Lee",
      "LEECOUN" => "Lee County ",
      "LEEOTH" => "Lee - Other",
      "LEERTIPPENS" => "Lee R Tippens",
      "LEEST" => "Lee Street",
      "LENLAST" => "Len Lastinger Primary",
      "LEROYMASSEY" => "Leroy Massey",
      "LEVELCREEK" => "Level Creek",
      "LEVELGROVE" => "Level Grove",
      "LEWIS" => "Lewis - Cobb",
      "LEWISEL" => "Lewis - Hancock",
      "LEWISTON" => "Lewiston",
      "LIBERTY" => "Liberty - Cherokee",
      "LIBERTYL" => "Liberty ",
      "LIBERTYOTH" => "Liberty - Other",
      "LIBERTYPOINT" => "Liberty Point",
      "LIBERTYS" => "Liberty - Stephens",
      "LILBURN" => "Lilburn",
      "LILLIANCPO" => "Lillian C. Poole",
      "LINCOLN" => "Lincoln County ",
      "LINCOLND" => "Lincoln - Dougherty",
      "LINCOLNOTH" => "Lincoln - Other",
      "LINDSEY" => "Lindsey ",
      "LINWOOD" => "Linwood ",
      "LITHASPGS" => "Lithia Springs",
      "LIVEOAK" => "Live Oak - Dougherty",
      "LIVEOAKN" => "Live Oak - Newton",
      "LIVINGSTON" => "Livingston",
      "LIVSEY" => "Livsey",
      "LKMOSS" => "L.K. Moss",
      "LNGBRNCH" => "Long Branch",
      "LOCKHEED" => "Lockheed",
      "LOCUSTGR" => "Locust Grove",
      "LOGANVILLE" => "Loganville",
      "LOGANVILLEP" => "Loganville Primary",
      "LOMAXPINEV" => "J.L. Lomax",
      "LONGCAN" => "Long Cane ",
      "LONGCOU" => "Long County(PK-12)",
      "LONGOTH" => "Long - Other",
      "LONNIEJAK" => "Lonnie Jackson Academy",
      "LORRAINE" => "Lorraine",
      "LOUISIANAOTH" => "Louisiana - Other",
      "LOUISVIL" => "Louisville Academy",
      "LOVIN" => "Lovin",
      "LOW" => "Low",
      "LOWNDESOTH" => "Lowndes - Other",
      "LTLERIVER" => "Little River",
      "LUELLA" => "Luella",
      "LULA" => "Lula",
      "LUMOT" => "Lumpkin - Other",
      "LUMPKIN" => "Lumpkin County",
      "LWRNCVLLE" => "Lawrenceville",
      "LYERLY" => "Lyerly",
      "LYMANHA" => "Lyman Hall - Liberty",
      "LYMANHALL" => "Lyman Hall - Hall",
      "LYONSEL" => "Lyons ",
      "MABLETON" => "Mableton",
      "MACEDONIA" => "Macedonia",
      "MACONCO" => "Macon County ",
      "MACONOTH" => "Macon - Other",
      "MAGILL" => "Magill",
      "MAGNOLIA" => "Magnolia",
      "MAIN" => "Main",
      "MAINEOTH" => "Maine - Other",
      "MAINSTREET" => "Mainstreet",
      "MALCOMBRID" => "Malcom Bridge",
      "MAMIELO" => "Mamie Lou Gross",
      "MANNINGOKS" => "Manning Oaks",
      "MANSFIELD" => "Mansfield",
      "MARBUT" => "Marbut",
      "MARGARETWI" => "Margaret Winn Holt",
      "MARIONC" => "Marion County ",
      "MARIONOTH" => "Marion - Other",
      "MARLOW" => "Marlow",
      "MARSHALL" => "Thurgood Marshall",
      "MARSHPOI" => "Marshpoint",
      "MARTINEZ" => "Martinez",
      "MARTINH" => "Martin",
      "MARYLANDOTH" => "Maryland - Other",
      "MARYLEE" => "Mary Lee Clark",
      "MARYLIN" => "Lin",
      "MASHBURN" => "Mashburn",
      "MASO" => "M.H. Mason",
      "MASON" => "S.L. Mason",
      "MASONCREEK" => "Mason Creek",
      "MASSACHUSETTSOTH" => "Massachusetts - Other",
      "MATHEWS" => "Mathews ",
      "MATILDA" => "Matilda Harris",
      "MATT" => "Matt",
      "MATTHEW" => "Matthew Arthur ",
      "MATTIEL" => "Mattie Lively",
      "MATTWIL" => "Matt Wilson ",
      "MAXWELL" => "Maxwell ",
      "MAYSVILLE" => "Maysville",
      "MCALLISTER" => "McAllister",
      "MCBEANE" => "McBean",
      "MCCALLPRIMARY" => "McCall Primary",
      "MCDONOUGH" => "McDonough",
      "MCDUFFIEOTH" => "Mcduffie - Other",
      "MCEVER" => "McEver",
      "MCGARRAH" => "William M. McGarrah",
      "MCHENRYPRI" => "McHenry Primary",
      "MCINTOSHOTH" => "Mcintosh - Other",
      "MCKENDREE" => "McKendree",
      "MCLAENDON" => "McLendon",
      "MCNAIRDISCOVERY" => "Ronald E McNair Discover Learning Acad",
      "MEADOWBR" => "Meadowbrook ",
      "MEADOWCREEK" => "Meadowcreek",
      "MEADOWVIEW" => "Meadowview",
      "MEDLOCKBR" => "Medlock Bridge",
      "MEMORIAL" => "Memorial Drive ",
      "MENIO" => "Menlo",
      "MERIWETHEROTH" => "Meriwether - Other",
      "MERRYEL" => "Merry ",
      "METTER" => "Metter",
      "METTERI" => "Metter Intermediate (4-5)",
      "MICHIGANOTH" => "Michigan - Other",
      "MIDDLERDG" => "Middle Ridge",
      "MIDLAND" => "Midland Academy",
      "MIDVALE" => "Midvale",
      "MIDWAYB" => "Midway - Baldwin",
      "MIDWAYD" => "Midway - Dekalb",
      "MIDWAYF" => "Midway - Forsyth",
      "MIDWAYP" => "Midway - Pierce",
      "MIDWAYPRIM" => "Midway Primary",
      "MILES" => "Lenora P. Miles",
      "MILFORD" => "Milford",
      "MILLCRE" => "Mill Creek",
      "MILLERB" => "Miller - Bibb",
      "MILLERC" => "Miller County ",
      "MILLERE" => "Miller - Houston",
      "MILLEROTH" => "Miller - Other",
      "MIMOSA" => "Mimosa",
      "MINNESOTAOTH" => "Minnesota - Other",
      "MIRRORLAKE" => "Mirror Lake",
      "MISSIONRD" => "Mission Road",
      "MISSISSIPPIOTH" => "Mississippi - Other",
      "MISSOURIOTH" => "Missouri - Other",
      "MITCHELL" => "South Mitchell County",
      "MITCHELLOTH" => "Mitchell - Other",
      "MITCHELLP" => "North Mitchell County",
      "MJONES" => "M. A. Jones",
      "MLKBIBB" => "Martin Luther King Jr - Bibb",
      "MLKC" => "Martin Luther King Jr - Clayton",
      "MLKD" => "Martin Luther King Jr - Dougherty",
      "MLKM" => "Martin Luther King Jr - Muscogee",
      "MMGARITY" => "McGarity",
      "MODEL" => "Model",
      "MONROE" => "Monroe",
      "MONROEOTH" => "Monroe - Other",
      "MONROEP" => "Monroe Primary",
      "MONTANAOTH" => "Montana - Other",
      "MONTCLAIR" => "Montclair",
      "MONTESA" => "Monte Sano ",
      "MONTGOME" => "New Montgomery County",
      "MONTGOMERY" => "Montgomery",
      "MONTGOMERYOTH" => "Montgomery - Other",
      "MOORE" => "Moore",
      "MOOREC" => "R.M. Moore",
      "MOORESTREE" => "Moore Street",
      "MORELAN" => "Moreland",
      "MORELAND" => "Moreland Rd",
      "MORGAN" => "Morgan",
      "MORGANC" => "Morgan County",
      "MORNINGSD" => "Morningside - Dougherty",
      "MORNINGSH" => "Morningside - Houston",
      "MORNINGSID" => "Morningside-",
      "MORROW" => "Morrow",
      "MOSSYCREEK" => "Mossy Creek",
      "MOULTONB" => "Moulton-Branch ",
      "MOUNTAING" => "Mountain View - Gilmer",
      "MOUNTAINM" => "Mountain View - Meriwether",
      "MOUNTVIL" => "Mountville ",
      "MRSMITH" => "Martha Rawls Smith",
      "MTBETHEL" => "Mount Bethel",
      "MTCARMELD" => "Mount Carmel - Douglas",
      "MTCARMELH" => "Mount Carmel - Henry",
      "MTOLIVE" => "Mount Olive",
      "MTPARKG" => "Mountain Park - Gwinnett",
      "MTPRKF" => "Mountain Park - Fulton",
      "MTROAD" => "Mountain Road",
      "MTVERNON" => "Mount Vernon",
      "MTVIEW" => "Mountain View - Cobb",
      "MTYONAH" => "Mount Yonah",
      "MTZIONCL" => "Mount Zion - Clayton",
      "MTZIONCR" => "Mount Zion - Carroll",
      "MULBERRY" => "Mulberry Creek ",
      "MULBERRYG" => "Mulberry",
      "MURDOCK" => "Murdock",
      "MURPHYCAN" => "Murphy Candler",
      "MUSCOGEE" => "Muscogee ",
      "MUSCOGEEOTH" => "Muscogee - Other",
      "MUSEUM" => "Not Valid",
      "MYERS" => "Myers",
      "NAHUNTA" => "Nahunta",
      "NAOMI" => "Naomi",
      "NARVIEH" => "Narvie Harris",
      "NATIONAL" => "National Hills ",
      "NBHDCHARTER" => "Atlanta Neighborhood Charter",
      "NDAKOTAOTH" => "North Dakota - Other",
      "NEBO" => "Nebo",
      "NEBRASKAOTH" => "Nebraska - Other",
      "NESBIT" => "Nesbit",
      "NEVADAOTH" => "Nevada - Other",
      "NEVILS" => "Nevils",
      "NEWGEORGI" => "New Georgia",
      "NEWHAMPSHIREOTH" => "New Hampshire - Other",
      "NEWHOLL" => "New Holland Core Knowledge Academy",
      "NEWHOPE" => "New Hope - Whitfield",
      "NEWHOPEH" => "New Hope - Henry",
      "NEWJERSEYOTH" => "New Jersey - Other",
      "NEWMANCH" => "New Manchester",
      "NEWMEXICOOTH" => "New Mexico - Other",
      "NEWMOUN" => "New Mountain Hill ",
      "NEWNANCRO" => "Newnan Crossing",
      "NEWOT" => "Newton - Other",
      "NEWPROSPEC" => "New Prospect",
      "NEWQUIT" => "Quitman County",
      "NEWYORKOTH" => "New York - Other",
      "NFAYETTE" => "North Fayette",
      "NHEIGHTS" => "North Heights",
      "NICHOLLS" => "Nicholls",
      "NICHOLSON" => "Nicholson",
      "NICKAJACK" => "Nickajack",
      "NOLAN" => "Nolan",
      "NORCROSS" => "Norcross",
      "NORMANP" => "Norman Park",
      "NORRISE" => "Norris ",
      "NORTHBR" => "North Brooks",
      "NORTHCO" => "North Columbia",
      "NORTHCOLUMBUS" => "North Columbus",
      "NORTHCUTT" => "Northcutt",
      "NORTHDO" => "North Dodge",
      "NORTHDOUGLAS" => "North Douglas",
      "NORTHHARLEM" => "North Harlem",
      "NORTHHART" => "North Hart ",
      "NORTHJACKSON" => "North Jackson",
      "NORTHLAFAY" => "North LaFayette",
      "NORTHMU" => "North Mulberry",
      "NORTHSIDEC" => "Northside - Coweta",
      "NORTHSIDED" => "Northside - Dougherty",
      "NORTHSIDEG" => "Northside - Grady",
      "NORTHSIDEH" => "Northside - Houston",
      "NORTHSIDEP" => "Northside - Paulding",
      "NORTHSIDEPOLK" => "Northside - Polk",
      "NORTHSIDET" => "Northside Primary",
      "NORTHWES" => "Northwest",
      "NORTHWESTL" => "Northwest Laurens ",
      "NORTHWOOD" => "Northwood",
      "NORTNPRK" => "Norton Park",
      "NUNN" => "Nunn",
      "OAKCLIFF" => "Oakcliff",
      "OAKGROVEC" => "Oak Grove - Cherokee",
      "OAKGROVED" => "Oak Grove - Dekalb",
      "OAKGROVEF" => "Oak Grove - Fayette",
      "OAKHILL" => "Oak Hill",
      "OAKHUHURST" => "Oakhurst",
      "OAKKNOLL" => "Oak Knoll",
      "OAKLAND" => "Oakland - Henry",
      "OAKLANDG" => "Oakland - Gilmer",
      "OAKLEY" => "Oakley",
      "OAKVIEW" => "Oakview",
      "OAKWOOD" => "Oakwood",
      "OCEE" => "Ocee",
      "OCONEECOUN" => "Oconee County",
      "ODOM" => "Odom",
      "ODUMELE" => "Odum ",
      "OGLEAVE" => "Oglethorpe Avenue",
      "OGLEC" => "Oglethorpe County",
      "OGLEPOINT" => "Oglethorpe Point",
      "OHIOOTH" => "Ohio - Other",
      "OKAPILCO" => "Okapilco",
      "OKLAHOMAOTH" => "Oklahoma - Other",
      "OLAELA" => "Ola",
      "OLIVER" => "Oliver",
      "OMEGAEL" => "Omega ",
      "OREGONOTH" => "Oregon - Other",
      "ORRS" => "Orrs",
      "OTHER" => "Other",
      "OTHERALABAMA" => "Alabama - Other",
      "OTHERBAN" => "Banks - Other",
      "OTHERBT" => "Butts - Other",
      "OTHERCAR" => "Carroll - Other",
      "OTHERCAT" => "Catoosa - Other",
      "OTHERCHA" => "Chattooga - Other",
      "OTHERCLA" => "Clarke - Other",
      "OTHERDAD" => "Dade - Other",
      "OTHERFLO" => "Floyd - Other",
      "OTHERFLORIDA" => "Florida - Other",
      "OTHERFRA" => "Franklin - Other",
      "OTHERGIL" => "Gilmer - Other",
      "OTHERGOR" => "Gordon - Other",
      "OTHERGRE" => "Greene - Other",
      "OTHERHAB" => "Habersham - Other",
      "OTHERHAN" => "Hancock - Other",
      "OTHERHAR" => "Haralson - Other",
      "OTHERHEA" => "Heard - Other",
      "OTHERJAC" => "Jackson - Other",
      "OTHERJAS" => "Jasper - Other",
      "OTHERMAD" => "Madison - Other",
      "OTHERMOR" => "Morgan - Other",
      "OTHERMUR" => "Murray - Other",
      "OTHERNORTHCAROLINA" => "North Carolina - Other",
      "OTHEROCO" => "Oconee - Other",
      "OTHEROGL" => "Oglethorpe - Other",
      "OTHERPIC" => "Pickens - Other",
      "OTHERPOL" => "Polk - Other",
      "OTHERPUT" => "Putnam - Other",
      "OTHERRAB" => "Rabun - Other",
      "OTHERROC" => "Rockdale - Other",
      "OTHERSOUTHCAROLINA" => "South Carolina - Other",
      "OTHERSPA" => "Spalding - Other",
      "OTHERSTE" => "Stephens - Other",
      "OTHERTENNESSEE" => "Tennessee - Other",
      "OTHERWAL" => "Walker - Other",
      "OTHERWHI" => "White - Other",
      "OTHERWHT" => "Whitfield - Other",
      "OTISJBROCK" => "Otis J Brock III",
      "PALMERSTON" => "Palmer Stone",
      "PALMETTO" => "Palmetto",
      "PANOLAWA" => "Panola Way",
      "PANTER" => "Sam D. Panter",
      "PARKCREEK" => "Park Creek",
      "PARKELE" => "Park ",
      "PARKERM" => "Parker Mathis ",
      "PARKLANE" => "Parklane",
      "PARKSID" => "Parkside",
      "PARKST" => "Park Street",
      "PARKWOOD" => "Parkwood ",
      "PARSONS" => "Parsons",
      "PARTEE" => "Partee",
      "PATESCREEK" => "Pate's Creek",
      "PATRICK" => "Patrick",
      "PATTERSO" => "Patterson ",
      "PAUOT" => "Paulding - Other",
      "PEACHCREST" => "Peachcrest",
      "PEACHOTH" => "Peach - Other",
      "PEACHTREEF" => "Peachtree City",
      "PEACHTREEG" => "Peachtree",
      "PEARLST" => "Pearl Stephens ",
      "PEARSON" => "Pearson",
      "PEEKSCHAPEL" => "Peek's Chapel",
      "PEEPLES" => "Peeples",
      "PELHAM" => "Pelham",
      "PENNSYLVANIAOTH" => "Pennsylvania - Other",
      "PEPPERELL" => "Pepperell",
      "PEPPERELLP" => "Pepperell Primary",
      "PERDUEE" => "Perdue ",
      "PERKERSON" => "Perkerson",
      "PERRYEL" => "Perry ",
      "PEYTNFRST" => "Peyton Forest",
      "PHARR" => "Pharr",
      "PICKETTSMILL" => "Pickett's Mill",
      "PIERCEOTH" => "Pierce - Other",
      "PIKECOU" => "Pike County ",
      "PIKEOTH" => "Pike - Other",
      "PINEFOREST" => "Pine Forest",
      "PINEGRO" => "Pine Grove ",
      "PINELOG" => "Pine Log",
      "PINERID" => "Pine Ridge - Harris",
      "PINERIDGE" => "Pine Ridge - Dekalb",
      "PINEST" => "Pine Street",
      "PINEVALE" => "Pinevale",
      "PITNER" => "Pitner",
      "PLEASANTGR" => "Pleasant Grove - Whitfield",
      "PLEASENTDL" => "Pleasantdale",
      "PLEASENTGR" => "Pleasant Grove - Henry",
      "POOLER" => "Pooler",
      "POPLAR" => "Poplar Road",
      "PORTAL" => "Portal",
      "PORTER" => "Porter",
      "PORTERDALE" => "Porterdale",
      "PORTWEN" => "Port Wentworth",
      "POTTERSTRE" => "Potter Street",
      "POWRSFRRY" => "Powers Ferry",
      "PRINCETON" => "Princeton",
      "PROVIDENCE" => "Providence",
      "PTSOUTH" => "Pointe South",
      "PUCKETTSMILL" => "Puckett's Mill",
      "PULASKI" => "Pulaski",
      "PULASKIC" => "Pulaski County ",
      "PULASKIOTH" => "Pulaski - Other",
      "PUTNAMC" => "Putnam County",
      "PWDRSPGS" => "Powder Springs",
      "QUAILRU" => "Quail Run ",
      "QUITMAN" => "Quitman",
      "QUITMANOTH" => "Quitman - Other",
      "RABUNCOUNTY" => "Rabun County",
      "RADIUMS" => "Radium Springs",
      "RAINBOW" => "Rainbow",
      "RANDOLPH" => "Randolph",
      "RANDOLPHOTH" => "Randolph - Other",
      "RANDOLPHR" => "Randolph County",
      "REDANE" => "Redan",
      "REDBUD" => "Red Bud",
      "REDOAK" => "Red Oak",
      "REESERO" => "Reese Road ",
      "REIDSVIL" => "Reidsville ",
      "RENAISS" => "Renaissance",
      "RHEAD" => "Head",
      "RHODEISLANOTH" => "Rhode Island - Other",
      "RICE" => "Rice",
      "RICHMOND" => "Richmond Hill",
      "RICHMONDOTH" => "Richmond - Other",
      "RIGDONR" => "Rigdon Road ",
      "RILEY" => "Riley",
      "RINCON" => "Rincon",
      "RINGGOLD" => "Ringgold",
      "RITCH" => "Ritch",
      "RIVERBEND" => "Riverbend",
      "RIVERDALE" => "Riverdale",
      "RIVEREVES" => "River Eves",
      "RIVERRIDGE" => "River Ridge",
      "RIVERRO" => "River Road ",
      "RIVERS" => "Rivers - Fayette",
      "RIVERSEDGE" => "Rivers Edge",
      "RIVERSIDE" => "Riverside - Cobb",
      "RIVERSIDEC" => "Riverside - Columbia",
      "RIVERSIDEF" => "Riverside - Fulton",
      "RIVERSIDEG" => "Riverside - Gwinnett",
      "RIVERVIEW" => "Riverview",
      "RKCHAPEL" => "Rock Chapel",
      "RMINOR" => "Minor",
      "RNORTON" => "Norton",
      "ROAN" => "Roan",
      "ROBERTS" => "Roberts",
      "ROBERTSHAWTHEME" => "Robert Shaw Theme",
      "ROBERTSTS" => "Roberta T. Smith",
      "ROBINSON" => "Robinson",
      "ROCKBRIDGD" => "Rockbridge - Dekalb",
      "ROCKBRIDGG" => "Rockbridge - Gwinnett",
      "ROCKSPRING" => "Rock Spring - Walker",
      "ROCKSPRINGS" => "Rock Springs",
      "ROCKSPRNG" => "Rock Spring - Henry",
      "ROCKYBRANC" => "Rocky Branch",
      "ROCKYCRK" => "Rocky Creek",
      "ROCKYMT" => "Rocky Mount",
      "ROCKYPLAINS" => "Rocky Plains",
      "ROLANDWRUS" => "Roland W. Russom",
      "ROLLINS" => "Rollins ",
      "ROOPVILLE" => "Roopville",
      "ROSEBUD" => "Rosebud",
      "ROSEMONT" => "Rosemont ",
      "ROSSVILLE" => "Rossville",
      "ROSWELLN" => "Roswell North",
      "ROWLAND" => "Rowland",
      "ROYSTON" => "Royston",
      "RUSKINE" => "Ruskin ",
      "RUSSELL" => "Russell - Cobb",
      "RUSSELLH" => "Russell - Houston",
      "RUTHHILL" => "Ruth Hill",
      "SADDLERIDGE" => "Saddle Ridge",
      "SAGMRHLLS" => "Sagamore Hills",
      "SAINTMA" => "Saint Marys",
      "SALLASMAHO" => "Sallas Mahone",
      "SALLIEZ" => "Sallie Zetterower",
      "SALLYDAILE" => "Sally Dailey Meadows",
      "SANDERS" => "Sanders",
      "SANDERSV" => "Ridge Road",
      "SANDHILL" => "Sand Hill - Carroll",
      "SANDHILLE" => "Sand Hill - Effingham",
      "SARAHARPMINTER" => "Sara Harp Minter",
      "SARAHCO" => "Sarah Cobb ",
      "SARDIS" => "Sardis",
      "SATILLA" => "Satilla",
      "SATILLAM" => "Satilla Marsh",
      "SAWNEE" => "Sawnee",
      "SAWNPRI" => "Sawnee Primary",
      "SAWYER" => "Sawyer Road",
      "SAXONHEIGH" => "Saxon Heights",
      "SCHLEYC" => "Schley County ",
      "SCHLEYOTH" => "Schley - Other",
      "SCOT" => "Scott - Fulton",
      "SCOTT" => "Scott - Thomas",
      "SCREVEN" => "Screven County ",
      "SCREVENOTH" => "Screven - Other",
      "SCREVENW" => "Screven ",
      "SDAKOTAOTH" => "South Dakota - Other",
      "SEAST" => "Southeast - Floyd",
      "SEBRNLEE" => "Seaborn Lee",
      "SEDLIAPRK" => "Sedalia Park",
      "SEMINOLE" => "Seminole County ",
      "SEMINOLEOTH" => "Seminole - Other",
      "SETTLEBR" => "Settles Bridge",
      "SGA" => "S G A",
      "SHADOWROC" => "Shadow Rock",
      "SHAKERAG" => "Shakerag",
      "SHALLOWFOR" => "Shallowford Falls",
      "SHARON" => "Sharon - Forsyth",
      "SHARONW" => "Sharon - Walton",
      "SHARPCREEK" => "Sharp Creek",
      "SHELTON" => "Floyd L. Shelton",
      "SHERWOOD" => "Sherwood Acres",
      "SHILOH" => "Shiloh",
      "SHILOHPT" => "Shiloh Point",
      "SHIRLEY" => "Shirley Hills ",
      "SHIVER" => "Shiver",
      "SHOALCREEK" => "Shoal Creek",
      "SHUMAN" => "Shuman",
      "SILVERCITY" => "Silver City",
      "SIMONTON" => "Simonton",
      "SIMPSON" => "Simpson",
      "SIMS" => "Sims",
      "SIXES" => "Sixes",
      "SKYVIEW" => "Sky View",
      "SKYVIEWB" => "Skyview",
      "SLATER" => "Slater",
      "SLLEWIS" => "S.L. Lewis",
      "SMILEYE" => "Smiley ",
      "SMITH" => "Smith",
      "SMITHBARNESMITH" => "Smith-Barnes",
      "SMOKERIS" => "Smoke Rise",
      "SMYRNA" => "Smyrna",
      "SNAPFINGER" => "Snapfinger",
      "SOCIALCIRC" => "Social Circle",
      "SONORAVILLE" => "Sonoraville",
      "SOPECREEK" => "Sope Creek",
      "SOUTHCOC" => "South Columbia",
      "SOUTHCOM" => "South Columbus ",
      "SOUTHDGLS" => "South Douglas",
      "SOUTHDO" => "South Dodge",
      "SOUTHEAST" => "Southeast - Loundes",
      "SOUTHEF" => "South Effingham",
      "SOUTHFIELD" => "Southfield",
      "SOUTHHA" => "South Hart ",
      "SOUTHJACKSON" => "South Jackson",
      "SOUTHRA" => "South Rabun",
      "SOUTHSALEM" => "South Salem",
      "SOUTHSIDEG" => "Southside - Grady",
      "SOUTHSIDER" => "Southside - Richmond",
      "SPALDINGDR" => "Spalding Drive",
      "SPENCER" => "Spencer",
      "SPOUTSPRI" => "Spout Springs",
      "SPRINGDA" => "Springdale",
      "SPRINGDALED" => "Springdale Park",
      "SPRINGFI" => "Springfield",
      "SPRINGHILL" => "Spring Hill",
      "SPRINGP" => "Spring Place",
      "SRAGSDALE" => "Sara M. Ragsdale",
      "STARK" => "Stark",
      "STARLING" => "Starling",
      "STATEBR" => "State Bridge Crossing",
      "STATHAM" => "Statham",
      "STERLING" => "Sterling",
      "STEVENS" => "Stevens Creek",
      "STEWART" => "Stewart County ",
      "STEWARTOTH" => "Stewart - Other",
      "STGEOR" => "St. George",
      "STILL" => "Still",
      "STILSON" => "Stilson",
      "STMARYS" => "St. Marys",
      "STMTN" => "Stone Mountain",
      "STOCKBRIDG" => "Stockbridge",
      "STONECREEK" => "Stone Creek",
      "STONEMILL" => "Stone Mill",
      "STONEVIEW" => "Stoneview",
      "STONEWALL" => "Stonewall Tell",
      "STRINGFE" => "Stringfellow",
      "STRIPLING" => "Susan Stripling",
      "STSIMON" => "St. Simons",
      "SUDER" => "Suder",
      "SUEREYN" => "Sue Reynolds ",
      "SUGARHILL" => "Sugar Hill - Gwinnett",
      "SUGARHILLH" => "Sugar Hill - Hall",
      "SUGARMIL" => "Sugarmill",
      "SUMMERVI" => "Summerville",
      "SUMMITTHILL" => "Summit Hill",
      "SUMTERC" => "Sumter County ",
      "SUMTEROTH" => "Sumter - Other",
      "SUNSET" => "Sunset",
      "SUSIEDASHE" => "Susie Dasher",
      "SUWANEE" => "Suwanee",
      "SWAIN" => "Swain",
      "SWAINSBO" => "Swainsboro",
      "SWCHATHAM" => "Southwest - Chatham",
      "SWCRISP" => "Southwestern",
      "SWEETAPPLE" => "Sweet Apple",
      "SWEETWATER" => "Sweetwater",
      "SWHANCOCK" => "Southwest - Hancock",
      "SWINT" => "Swint",
      "SWLAURENS" => "Southwest Laurens ",
      "SYCAMORE" => "Sycamore",
      "SYLVESTERD" => "Sylvester Road",
      "SYLVESTERW" => "Worth County",
      "TADMORE" => "Tadmore",
      "TALBOTOTH" => "Talbot - Other",
      "TALIAFER" => "Taliaferro County",
      "TALIAFERROOTH" => "Taliaferro - Other",
      "TALLAPOOSA" => "Tallapoosa Primary",
      "TARA" => "Tara",
      "TATE" => "Tate",
      "TATTNALLOTH" => "Tattnall - Other",
      "TAYLOR" => "Taylor - Gwinnett",
      "TAYLORB" => "Rosa Taylor",
      "TAYLORC" => "Taylor County ",
      "TAYLOROTH" => "Taylor - Other",
      "TAYLORS" => "Taylors Creek ",
      "TAYLORSVIL" => "Taylorsville",
      "TEASLEY" => "Teasley",
      "TELFAIR" => "Telfair County",
      "TELFAIROTH" => "Telfair - Other",
      "TEMPLE" => "Temple",
      "TENNILLE" => "Tennille ",
      "TERRACE" => "Terrace Manor",
      "TERRELLOTH" => "Terrell - Other",
      "TEXASOTH" => "Texas - Other",
      "TGSCOTT" => "T G Scott",
      "THOMASCROS" => "Thomas Crossroads",
      "THOMASOTH" => "Thomas - Other",
      "THOMASVILL" => "Thomasville Heights",
      "THOMSON" => "Thomson ",
      "THUNDERB" => "Thunderbolt",
      "TIFTOTH" => "Tift - Other",
      "TIGERCR" => "Tiger Creek",
      "TIMBERRDG" => "Timber Ridge - Cobb",
      "TIMBERRIDGE" => "Timber Ridge - Henry",
      "TIMOTHY" => "Timothy",
      "TOBACCO" => "Tobacco Road ",
      "TOCCOA" => "Toccoa",
      "TODDGRA" => "Todd Grant ",
      "TOLBERT" => "Tolbert",
      "TONEY" => "Toney",
      "TOOMBSC" => "Toombs Central ",
      "TOOMBSOTH" => "Toombs - Other",
      "TOWNS" => "Towns",
      "TOWNSCO" => "Towns County",
      "TOWNSOTH" => "Towns - Other",
      "TREUTLEN" => "Treutlen County",
      "TREUTLENOTH" => "Treutlen - Other",
      "TRION" => "Trion",
      "TRIP" => "Trip",
      "TRITT" => "Tritt",
      "TROUPOTH" => "Troup - Other",
      "TUBMAN" => "Tubman",
      "TUCKERE" => "Tucker ",
      "TUNNELHILL" => "Tunnel Hill",
      "TURNER" => "Turner",
      "TURNERC" => "Turner County ",
      "TURNERCS" => "Turner County Specialty(Pk-12)",
      "TURNEROTH" => "Turner - Other",
      "TURNERWOODS" => "Turner Woods",
      "TUSSAHAW" => "Tussahaw",
      "TWIGGSOTH" => "Twiggs - Other",
      "TWINCIT" => "Twin City",
      "TWINOAK" => "Twin Oaks ",
      "TYRONE" => "Tyrone",
      "UNION" => "Union - Paulding",
      "UNIONB" => "Union - Bibb",
      "UNIONCO" => "Union County ",
      "UNIONOTH" => "Union - Other",
      "UNIONPO" => "Union Point",
      "UNITYGROVE" => "Unity Grove",
      "UNITYM" => "Unity - Meriwether",
      "UNITYT" => "Unity - Troup",
      "UNIVERSITYCHARTER" => "University Community Charter",
      "UPSONLEEN" => "Upson-Lee North",
      "UPSONOTH" => "Upson - Other",
      "USHER" => "Bazoline E. Usher/Collier Heights",
      "USONLEES" => "Upson-Lee South",
      "UTAHOTH" => "Utah - Other",
      "VALLEYPOIN" => "Valley Point",
      "VANDERLYN" => "Vanderlyn",
      "VANWERT" => "Van Wert",
      "VARNELL" => "Varnell",
      "VARNER" => "Varner",
      "VAUGHAN" => "Vaughan",
      "VENETIAN" => "Venetian Hills",
      "VERMONTOTH" => "Vermont - Other",
      "VICKERYE" => "Vickery Creek",
      "VILLARICA" => "Villa Rica",
      "VINEVILL" => "Vineville Academy",
      "VIRGINIAOTH" => "Virginia - Other",
      "WACONAE" => "Wacona ",
      "WADDELL" => "Waddell ",
      "WALDOPA" => "Waldo Pafford ",
      "WALKER" => "Walker",
      "WALKERINMAN" => "Walker Inman",
      "WALKERPRK" => "Walker Park",
      "WALNUTCREEK" => "Walnut Creek",
      "WALNUTGRO" => "Walnut Grove - Gwinnett",
      "WALNUTGROV" => "Walnut Grove - Walton",
      "WALOT" => "Walton - Other",
      "WAPARK" => "Washington Park",
      "WAREMAG" => "Ware Magnet(Kk-12)",
      "WAREOTH" => "Ware - Other",
      "WARESBOR" => "Waresboro ",
      "WARRENOTH" => "Warren - Other",
      "WARRENR" => "Warren Road ",
      "WASHINGOTH" => "Washington - Other",
      "WASHINGTONOTH" => "Washington State - Other",
      "WATERS" => "Waters",
      "WAUKAMT" => "Wauka Mountain",
      "WAWILKES" => "Washington-Wilkes ",
      "WAYNEOTH" => "Wayne - Other",
      "WAYNESVI" => "Waynesville Primary",
      "WBRITT" => "Britt",
      "WCENTRAL" => "West Central",
      "WEBSTER" => "Webster County ",
      "WEBSTEROTH" => "Webster - Other",
      "WELCH" => "Welch",
      "WELLSEL" => "Mattie Wells",
      "WEND" => "West End",
      "WESLEYH" => "Wesley Heights ",
      "WESLEYLAKE" => "Wesley Lakes",
      "WESTBAINBR" => "West Bainbridge",
      "WESTCHA" => "West Chatham",
      "WESTCHEST" => "Westchester",
      "WESTCLTN" => "West Clayton",
      "WESTERN" => "Western",
      "WESTFAN" => "West Fannin",
      "WESTGORDON" => "West Gordon",
      "WESTGRE" => "West Green",
      "WESTHARALS" => "West Haralson",
      "WESTJACK" => "West Jackson Primary",
      "WESTJACKSON" => "West Jackson",
      "WESTMANOR" => "West Manor",
      "WESTMINI" => "Westminister Christian Academy (PK-12)",
      "WESTMONT" => "Westmont",
      "WESTNEWTON" => "West Newton",
      "WESTPOI" => "West Point ",
      "WESTSID" => "West Side - Cobb",
      "WESTSIDE" => "Westside - Whitfield",
      "WESTSIDEC" => "West Side - Catoosa",
      "WESTSIDECO" => "Westside - Coffee",
      "WESTSIDEH" => "Westside - Houston",
      "WESTSIDEL" => "Westside - Lowndes",
      "WESTSIDEP" => "Westside - Polk",
      "WESTSIDET" => "West Side Magnet(3-8)",
      "WESTSTUBBS" => "Cliftondale",
      "WESTTOW" => "West Town",
      "WESTWOOD" => "Westwood",
      "WGHASTY" => "William G. Hasty, Sr.",
      "WHEELER" => "Wheeler County ",
      "WHEELEROTH" => "Wheeler - Other",
      "WHEELESS" => "Wheeless Road ",
      "WHIGHAM" => "Whigham",
      "WHITEB" => "White - Bartow",
      "WHITEBL" => "White Bluff",
      "WHITECOUNT" => "White County Intermediate",
      "WHITEDA" => "Whit Davis Road",
      "WHITEEDDIE" => "Eddie White Academy",
      "WHITEF" => "White - Fulton",
      "WHITEFOORD" => "Whitefoord",
      "WHITEHEA" => "Whitehead Road",
      "WHITEOAK" => "White Oak - Coweta",
      "WHITEOAKG" => "White Oak - Gwinnett",
      "WHITESBURG" => "Whitesburg",
      "WHITESULR" => "White Sulphur",
      "WHITESVI" => "Whitesville Road ",
      "WHITLOW" => "George W. Whitlow",
      "WILCOX" => "Wilcox County",
      "WILCOXOTH" => "Wilcox - Other",
      "WILKESOTH" => "Wilkes - Other",
      "WILKINSO" => "Wilkinson County ",
      "WILKINSONG" => "Wilkinson Garden",
      "WILKINSONOTH" => "Wilkinson - Other",
      "WILLIAMS" => "Williams - Fulton",
      "WILLIAMSB" => "Williams - Bibb",
      "WILLIAMSW" => "Williams Heights ",
      "WILLISF" => "Willis Foreman ",
      "WILLISROAD" => "Willis Road",
      "WILLOCOO" => "Willocoochee",
      "WILSONCREE" => "Wilson Creek",
      "WINDSORC" => "Windsor Forest",
      "WINDSORR" => "Windsor Spring Road ",
      "WINNHOLT" => "Winn Holt",
      "WINONAPRK" => "Winnona Park",
      "WINSTON" => "Winston",
      "WINTERVI" => "Winterville",
      "WISCONSINOTH" => "Wisconsin - Other",
      "WJCOOPER" => "W.J. Cooper",
      "WOODBINE" => "Woodbine",
      "WOODLAND" => "Woodland - Henry",
      "WOODLANDCH" => "Woodland - Fulton",
      "WOODLAWN" => "Woodlawn",
      "WOODRIDGE" => "Woodridge",
      "WOODSON" => "Woodson",
      "WOODSTATION" => "Woodstation",
      "WOODSTOCK" => "Woodstock",
      "WOODVILL" => "Woodville",
      "WOODWARD" => "Woodward",
      "WOODWARDMILL" => "Woodward Mill",
      "WOODYGA" => "Woody Gap",
      "WORTHOTH" => "Worth - Other",
      "WRENSEL" => "Wrens ",
      "WRIGHT" => "Wright",
      "WVIRGINIAOTH" => "West Virginia - Other",
      "WYNBROOKE" => "Wynbrooke",
      "WYNNTON" => "Wynnton ",
      "WYOMINGOTH" => "Wyoming - Other",
      "YARGO" => "Yargo",
      "YOUNGSGRV" => "Youngs Grove",
      "YOUTH" => "Youth"
    },
    :mls_middle_school => {
      "AARONCOHN" => "Aaron Cohn",
      "ADADAIRSVILL" => "Adairsville",
      "ADAMSON" => "Adamson",
      "ADRIANOF" => "Adrianof Performing Arts (Pk-08",
      "ALABAMA" => "Alabama - Other",
      "ALASKAOTH" => "Alaska - Other",
      "ALBANY" => "Albany",
      "ANITAWHI" => "Anita White Carson",
      "APPLINGB" => "Appling",
      "APPLINGC" => "Appling County",
      "APPLINGOTH" => "Appling - Other",
      "ARIZONAOTH" => "Arizona - Other",
      "ARKANSASOTH" => "Arkansas - Other",
      "ARMUCHEE" => "Armuchee",
      "ARNOLD" => "Arnold",
      "ARTHURWI" => "Arthur Williams",
      "ASHWORTH" => "Ashworth",
      "ATKINSONCO" => "Atkinson County",
      "ATKINSONOTH" => "Atkinson - Other",
      "ATLCHARTER" => "Atlanta Neighborhood Charter",
      "AUSTINROADM" => "Austin Road",
      "AUTREYMILL" => "Autrey Mill",
      "AWTREY" => "Awtrey",
      "BABB" => "Babb",
      "BACONCOU" => "Bacon County",
      "BACONOTH" => "Bacon - Other",
      "BACONTON" => "Baconton Community Charter(Pk -",
      "BAGLEY" => "Bagley",
      "BAKER" => "Baker",
      "BAKERCO" => "Baker County",
      "BAKEROTH" => "Baker - Other",
      "BALDWINOTH" => "Baldwin - Other",
      "BALLARDH" => "Ballard Hudson",
      "BANKSCOU" => "Banks County",
      "BANKSSTEPHENS" => "Banks Stephens",
      "BARBER" => "Barber",
      "BAROT" => "Bartow - Other",
      "BARTLETT" => "Bartlett",
      "BAYCREEK" => "Bay Creek",
      "BAYSPRINGS" => "Bay Springs",
      "BCREEK" => "Bear Creek - Barrow",
      "BEARCREEK" => "Bear Creek - Fulton",
      "BENHILLC" => "Ben Hill County",
      "BENHILLOTH" => "Ben Hill - Other",
      "BENNETTSMILL" => "Bennetts Mill",
      "BERKMAR" => "Berkmar",
      "BERRIEN" => "Berrien",
      "BERRIENOTH" => "Berrien - Other",
      "BESTACADEMY" => "B.E.S.T. Academy",
      "BETHUNE" => "Bethune",
      "BIBBOTH" => "Bibb - Other",
      "BLACKMON" => "Blackmon Road",
      "BLECKLEY" => "Bleckley County",
      "BLECKLEYOTH" => "Bleckley - Other",
      "BLOOMFIELD" => "Bloomfield",
      "BONAIRE" => "Bonaire",
      "BOOTHJC" => "Booth",
      "BRADWELL" => "Bradwell Institute (06-12)",
      "BRANTLEY" => "Brantley County",
      "BRANTLEYOTH" => "Brantley - Other",
      "BREMAN" => "Bremen",
      "BROOKSCO" => "Brooks County",
      "BROOKSOTH" => "Brooks - Other",
      "BROWN" => "Brown",
      "BRWOT" => "Barrow - Other",
      "BRYANCOU" => "Bryan County",
      "BRYANOTH" => "Bryan - Other",
      "BUFORD" => "Buford",
      "BULLOCHOTH" => "Bulloch - Other",
      "BUNCHE" => "Bunche",
      "BURKECOU" => "Burke County",
      "BURKEOTH" => "Burke - Other",
      "BURNEYHA" => "Burney-Harris-Lyons",
      "BUTLEREA" => "Butler Early Childhood Ctr. (PK-12)",
      "BYRON" => "Byron",
      "CALHOUN" => "Calhoun",
      "CALHOUNC" => "Calhoun County",
      "CALHOUNOTH" => "Calhoun - Other",
      "CALIFF" => "Califf",
      "CALIFORNIAOTH" => "California - Other",
      "CALLAWAY" => "Callaway",
      "CAMDEN" => "Camden",
      "CAMDENOTH" => "Camden - Other",
      "CAMPBELL" => "Campbell",
      "CAMPCREEK" => "Camp Creek",
      "CANDLEROTH" => "Candler - Other",
      "CARROLLT" => "Carrollton Jr.",
      "CARTERSM" => "Cartersville",
      "CARVER" => "Carver",
      "CARVERS" => "Carver Road",
      "CASS" => "Cass",
      "CEDARGROVE" => "Cedar Grove",
      "CEDARTOW" => "Cedartown",
      "CENTRAL" => "Central - Thomas",
      "CENTRALCR" => "Central - Carroll",
      "CENTRALT" => "Central - Talbot",
      "CHAMBLEE" => "Chamblee",
      "CHAMPIONS" => "Champions Theme School",
      "CHAPELHLDE" => "Chapel Hill - Dekalb",
      "CHAPELHLDO" => "Chapel Hill - Douglas",
      "CHARLTONOTH" => "Charlton - Other",
      "CHATHAMOTH" => "Chatham - Other",
      "CHATTAHO" => "Chattahoochee County",
      "CHATTAHOOCHEEOTH" => "Chattahoochee - Other",
      "CHATTANOOG" => "Chattanooga Valley",
      "CHEOT" => "Cherokee - Other",
      "CHESTATEE" => "Chestatee",
      "CHESTNUT" => "Chestnut Log",
      "CLAOT" => "Clayton - Other",
      "CLARKE" => "Clarke",
      "CLAXTON" => "Claxton",
      "CLAYCOUN" => "Clay County",
      "CLAYOTH" => "Clay - Other",
      "CLEARCREEK" => "Clear Creek",
      "CLEMENTS" => "Clements",
      "CLIFTONR" => "Clifton Ridge",
      "CLINCHC" => "Clinch County",
      "CLINCHOTH" => "Clinch - Other",
      "COASTAL" => "Coastal",
      "COBOT" => "Cobb - Other",
      "COFFEE" => "Coffee",
      "COFFEEOTH" => "Coffee - Other",
      "COILE" => "Coile",
      "COLLINS" => "Collins",
      "COLORADOOTH" => "Colorado - Other",
      "COLQUITTOTH" => "Colquitt - Other",
      "COLUMBIA" => "Columbia - Dekalb",
      "COLUMBIAC" => "Columbia - Columbia",
      "COLUMBIAOTH" => "Columbia - Other",
      "COMMERCE" => "Commerce",
      "CONNECTICUTOTH" => "Connecticut - Other",
      "CONYERS" => "Conyers",
      "COOKCOUN" => "Cook County",
      "COOKOTH" => "Cook - Other",
      "COOPER" => "Cooper",
      "COOSA" => "Coosa",
      "COUCH" => "Couch",
      "COUSINS" => "Cousins",
      "COWANROAD" => "Cowan Road",
      "COWOT" => "Coweta - Other",
      "CRABAPPLE" => "Crabapple",
      "CRAWFORD" => "Crawford County",
      "CRAWFORDOTH" => "Crawford - Other",
      "CREEKLAN" => "Creekland - Gwinnett",
      "CREEKLAND" => "Creekland - Cherokee",
      "CRENFROE" => "Renfroe",
      "CREWS" => "Alton C. Crews",
      "CRISPCOU" => "Crisp County",
      "CRISPOTH" => "Crisp - Other",
      "CROSSROADS" => "Crossroads - Douglas",
      "CROSSROADSC" => "Crossroads - Carroll",
      "CWDAVIS" => "C.W. Davis",
      "DACULA" => "Dacula",
      "DADE" => "Dade",
      "DALTON" => "Dalton",
      "DANIELL" => "Daniell",
      "DAVIDSON" => "Davidson Magnet(6-12)",
      "DAWOT" => "Dawson - Other",
      "DAWSON" => "New Dawson County",
      "DEANRUSK" => "Rusk",
      "DECATUROTH" => "Decatur - Other",
      "DEKOT" => "Dekalb - Other",
      "DELAWAREOTH" => "Delaware - Other",
      "DERENNE" => "DeRenne",
      "DICKERSON" => "Dickerson",
      "DOBBINS" => "J.A. Dobbins",
      "DOCOTH" => "District Of Columbia - Other",
      "DODGECOU" => "Dodge County",
      "DODGEN" => "Dodgen",
      "DODGEOTH" => "Dodge - Other",
      "DOOLYCOU" => "Dooly County",
      "DOOLYOTH" => "Dooly - Other",
      "DOUBLECH" => "Double Churches",
      "DOUGHERT" => "Dougherty International Education",
      "DOUGHERTYOTH" => "Dougherty - Other",
      "DOUOT" => "Douglas - Other",
      "DUBLIN" => "Dublin",
      "DULUTH" => "Duluth",
      "DURHAM" => "Durham",
      "DUTCHTOWN" => "Dutchtown",
      "EAGLESLANDM" => "Eagles Landing",
      "EARLYCOU" => "Early County",
      "EARLYOTH" => "Early - Other",
      "EASTAUGU" => "East Augusta",
      "EASTBRO" => "East Broad Street",
      "EASTBROOK" => "Eastbrook",
      "EASTCOBB" => "East Cobb",
      "EASTCOFF" => "East Coffee",
      "EASTCOLU" => "East Columbus Magnet Academy",
      "EASTCOWET" => "East Coweta",
      "EASTHALLM" => "East Hall",
      "EASTJACKSON" => "East Jackson",
      "EASTLAUR" => "East Laurens",
      "EASTPAULD" => "East Paulding",
      "EBENEZER" => "Ebenezer",
      "ECHOLSC" => "Echols County",
      "ECHOLSOTH" => "Echols - Other",
      "EDDY" => "Eddy",
      "EDWARDS" => "Edwards",
      "EFFINGHA" => "Effingham County",
      "EFFINGHAMOTH" => "Effingham - Other",
      "EIGHTHST" => "Eighth Street",
      "ELBERTCO" => "Elbert County",
      "ELBERTOTH" => "Elbert - Other",
      "ELKINSPT" => "Elkins Pointe",
      "ELLIS" => "Ellis",
      "ELLISARNAL" => "Arnall",
      "EMANUELC" => "Emanuel County Institute",
      "EMANUELOTH" => "Emanuel - Other",
      "ETBOOTH" => "E.T. Booth",
      "EVAN" => "Evans - Coweta",
      "EVANS" => "Evans - Columbia",
      "EVANSOTH" => "Evans - Other",
      "FACTORYSHOALS" => "Factory Shoals",
      "FAIRPLAY" => "Fairplay",
      "FANNINCO" => "Fannin County",
      "FANNINOTH" => "Fannin - Other",
      "FAYETTE" => "Fayette",
      "FAYOT" => "Fayette - Other",
      "FEAGINMI" => "Feagin Mill",
      "FIVEFORKS" => "Five Forks",
      "FLATROCK" => "Flat Rock",
      "FLORIDA" => "Florida - Other",
      "FLOYD" => "Floyd",
      "FORESTPRK" => "Forest Park",
      "FOROT" => "Forsyth - Other",
      "FORT" => "Fort",
      "FORTVALL" => "Fort Valley",
      "FRANKLIN" => "Franklin County",
      "FRANKNOSBO" => "Frank N. Osborne",
      "FREEDO" => "Freedom - Cherokee",
      "FREEDOM" => "Freedom - Dekalb",
      "FREEDOMP" => "Freedom Park",
      "FULOT" => "Fulton - Other",
      "GAINESVIL" => "Gainesville",
      "GARDNERN" => "Gardner-Newman",
      "GARRETT" => "Garrett",
      "GENERALRAY" => "General Ray Davis",
      "GEORGETOWNC" => "Georgetown",
      "GILMER" => "Gilmer",
      "GLADDEN" => "Gladden",
      "GLASCOCK" => "Glascock County",
      "GLASCOCKOTH" => "Glascock - Other",
      "GLENNCJONE" => "Glenn C. Jones",
      "GLENNHIL" => "Glenn Hills",
      "GLENNVIL" => "Glennville",
      "GLYNN" => "Glynn",
      "GLYNNOTH" => "Glynn - Other",
      "GORDONLEE" => "Gordon Lee",
      "GRACESN" => "Grace Snell",
      "GRADYOTH" => "Grady - Other",
      "GRAY" => "CA Gray",
      "GRAYSTATION" => "Gray Station",
      "GREENBRI" => "Greenbrier",
      "GREENVIL" => "Greenville",
      "GRIFFIN" => "Griffin",
      "GRIOT" => "Other",
      "GROVETOW" => "Grovetown",
      "GWIOT" => "Gwinnett - Other",
      "HAHIRA" => "Hahira",
      "HALOT" => "Hall - Other",
      "HAMPTON" => "Hampton",
      "HANCOCKCENTRAL" => "Hancock Central",
      "HARALSONCO" => "Haralson County",
      "HARLEM" => "Harlem",
      "HARPERAR" => "Harper-Archer",
      "HARRISCO" => "Harris County Carver",
      "HARRISOTH" => "Harris - Other",
      "HARTCOUN" => "Hart County",
      "HARTOTH" => "Hart - Other",
      "HAVENACADE" => "Haven Academy",
      "HAWAIIOTH" => "Hawaii - Other",
      "HAYMONMORRIS" => "Haymon-Morris",
      "HEARDCOU" => "Heard County",
      "HENDERB" => "Henderson - Butts",
      "HENDERSON" => "Henderson - Dekalb",
      "HENOT" => "Henry - Other",
      "HENRYM" => "Henry County",
      "HEPHZIBA" => "Hephzibah",
      "HERITAGE" => "Heritage",
      "HESSE" => "Hesse",
      "HIGHTOWERC" => "Hightower Trail",
      "HILSMAN" => "Hilsman",
      "HJONES" => "Herschel Jones",
      "HLCMBRDGE" => "Holcomb Bridge",
      "HOPEWELL" => "Hopewell",
      "HORNSBY" => "W.S. Hornsby",
      "HOUSTONOTH" => "Houston - Other",
      "HOWARD" => "Howard",
      "HUBERT" => "Hubert",
      "HULL" => "Hull",
      "HUNTINGTON" => "Huntington",
      "HUTTO" => "Hutto",
      "HYNSBRDGE" => "Haynes Bridge",
      "IDAHOOTH" => "Idaho - Other",
      "ILLINOISOTH" => "Illinois - Other",
      "INDIANAOTH" => "Indiana - Other",
      "INDIANCR" => "Indian Creek",
      "INMAN" => "Inman",
      "IOWAOTH" => "Iowa - Other",
      "IRMACAUSTIN" => "Irma C. Austin",
      "IRWINCOU" => "Irwin County",
      "IRWINOTH" => "Irwin - Other",
      "ISLEOF" => "Isle Of Hope",
      "JANEMACO" => "Jane Macon",
      "JASPER" => "Jasper",
      "JASPERCO" => "Jasper County",
      "JEFFDAVI" => "Jeff Davis",
      "JEFFDAVISOTH" => "Jeff Davis - Other",
      "JEFFERSO" => "Jefferson",
      "JEFFERSONOTH" => "Jefferson - Other",
      "JEFFTWIGG" => "Jeffersonville Intermediate (5-6)",
      "JENKINSC" => "Jenkins County",
      "JENKINSOTH" => "Jenkins - Other",
      "JOHNSONC" => "Johnson County",
      "JOHNSONOTH" => "Johnson - Other",
      "JONESBORO" => "Jonesboro",
      "JONESOTH" => "Jones - Other",
      "JONESVILLE" => "Bowdon",
      "JORDAN" => "Jordan",
      "JRICHARDS" => "J.E. Richards",
      "JRTRIPPE" => "J.R. Trippe",
      "KANSASOTH" => "Kansas - Other",
      "KELSEYAVE" => "A.Z. Kelsey",
      "KENDRICK" => "Kendrick",
      "KENNEDY" => "Kennedy",
      "KENNEDYROAD" => "Kennedy Road",
      "KENTUCKYOTH" => "Kentucky - Other",
      "KING" => "King",
      "KINGSBRIDGE" => "Kings Bridge",
      "KIPP" => "Kipp",
      "LAFAYETTE" => "LaFayette",
      "LAKESIDE" => "Lakeside - Columbia",
      "LAKESIDEF" => "Lakeside - Forsyth",
      "LAKEVIEW" => "Lakeview",
      "LAMARCOU" => "Lamar County",
      "LAMAROTH" => "Lamar - Other",
      "LANGFORD" => "Langford",
      "LANGSTON" => "Langston Chapel",
      "LANIER" => "Lanier",
      "LANIERCO" => "Lanier County",
      "LANIEROTH" => "Lanier - Other",
      "LAURENSOTH" => "Laurens - Other",
      "LEE" => "Lee",
      "LEECOUNT" => "Lee County West",
      "LEECOUNTE" => "Lee County East",
      "LEEOTH" => "Lee - Other",
      "LEWISFRA" => "Lewis Frasier",
      "LIBERTY" => "Liberty - Forsyth",
      "LIBERTYN" => "Liberty - Newton",
      "LIBERTYOTH" => "Liberty - Other",
      "LILBURN" => "Lilburn",
      "LINCOLNC" => "Lincoln County",
      "LINCOLNOTH" => "Lincoln - Other",
      "LINDLEY" => "Lindley",
      "LITHONIA" => "Lithonia",
      "LITTLEMILL" => "Little Mill",
      "LMMOSES" => "Lena Mae Moses",
      "LOCUSTGROVE" => "Locust Grove",
      "LOGANVILLEM" => "Loganville",
      "LONG" => "Long",
      "LONGCANE" => "Long Cane",
      "LONGCOUN" => "Long County",
      "LONGOTH" => "Long - Other",
      "LOSTMOUNT" => "Lost Mountain",
      "LOUISERADL" => "Louise Radloff",
      "LOUISIANAOTH" => "Louisiana - Other",
      "LOUISVIL" => "Louisville",
      "LOVEJOY" => "Lovejoy",
      "LOVINGGOOD" => "Lovinggood",
      "LOW" => "Low",
      "LOWNDES" => "Lowndes",
      "LOWNDESOTH" => "Lowndes - Other",
      "LUELLA" => "Luella",
      "LUMPKIN" => "Lumpkin County",
      "LYERLY" => "Lyerly",
      "MABRY" => "Mabry",
      "MACINTYREP" => "Macintyre Park",
      "MACONCOU" => "Macon County",
      "MACONOTH" => "Macon - Other",
      "MADISONC" => "Madison County",
      "MADRAS" => "Madras",
      "MAINEOTH" => "Maine - Other",
      "MALCOMBRID" => "Malcom Bridge",
      "MANCHEST" => "Manchester",
      "MARIETTA6" => "Marietta 6Th Grade",
      "MARIETTAJR" => "Marietta",
      "MARIONCOUNTY" => "Marion County",
      "MARIONOTH" => "Marion - Other",
      "MARSHALL" => "Marshall",
      "MARTHAPU" => "Martha Puckett",
      "MARYLANDOTH" => "Maryland - Other",
      "MARYMCLEODBETHUNE" => "Mary McLeod Bethune",
      "MASONCREEK" => "Mason Creek",
      "MASSACHUSETTSOTH" => "Massachusetts - Other",
      "MCAIRF" => "McNair - Fulton",
      "MCCLESKEY" => "McCleskey",
      "MCCLURE" => "McClure",
      "MCCLUREP" => "Sammy McClure Sr.",
      "MCCONNELL" => "McConnell",
      "MCDUFFIEOTH" => "Mcduffie - Other",
      "MCINTOSH" => "McIntosh County",
      "MCINTOSHOTH" => "Mcintosh - Other",
      "MCNAIRD" => "McNair - Dekalb",
      "MEMORIAL" => "Memorial",
      "MENIO" => "Menlo",
      "MERCER" => "Mercer",
      "MERIWETHEROTH" => "Meriwether - Other",
      "MERRYACR" => "Merry Acres",
      "METTER" => "Metter",
      "MGROVE" => "Miller Grove",
      "MICHIGANOTH" => "Michigan - Other",
      "MIDLAND" => "Midland",
      "MIDWAY" => "Midway",
      "MILLCREEK" => "Mill Creek",
      "MILLERCO" => "Miller County",
      "MILLERMA" => "Miller Magnet",
      "MILLEROTH" => "Miller - Other",
      "MINNESOTAOTH" => "Minnesota - Other",
      "MISSISSIPPIOTH" => "Mississippi - Other",
      "MISSOURIOTH" => "Missouri - Other",
      "MITCHELL" => "Mitchell County",
      "MITCHELLOTH" => "Mitchell - Other",
      "MODEL" => "Model",
      "MONROECO" => "Monroe County",
      "MONROEOTH" => "Monroe - Other",
      "MONTANAOTH" => "Montana - Other",
      "MONTGOME" => "Montgomery County",
      "MONTGOMERYOTH" => "Montgomery - Other",
      "MOORE" => "Moore",
      "MORGANCO" => "Morgan County",
      "MORGANRO" => "Morgan Road",
      "MORROW" => "Morrow",
      "MOSSYCREEK" => "Mossy Creek",
      "MOUNTAINCREEK" => "Mountain Creek Academy",
      "MOUNTZION" => "Mt. Zion",
      "MUNDYSMLL" => "Mundys Mill",
      "MURPHEY" => "Murphey",
      "MUSCOGEEOTH" => "Muscogee - Other",
      "MYERS" => "Myers",
      "NDAKOTAOTH" => "North Dakota - Other",
      "NEBRASKAOTH" => "Nebraska - Other",
      "NEEDWOOD" => "Needwood",
      "NEVADAOTH" => "Nevada - Other",
      "NEWBERN" => "Newbern",
      "NEWHAMPSHIREOTH" => "New Hampshire - Other",
      "NEWHOPE" => "New Hope",
      "NEWJERSEYOTH" => "New Jersey - Other",
      "NEWMEXICOOTH" => "New Mexico - Other",
      "NEWOT" => "Newton - Other",
      "NEWQUIT" => "Quitman County",
      "NEWYORKOTH" => "New York - Other",
      "NORTHBROOK" => "Northbrook",
      "NORTHCAROLINA" => "North Carolina - Other",
      "NORTHCLTN" => "North Clayton",
      "NORTHFORS" => "North Forsyth",
      "NORTHGWINNETT" => "North Gwinnett",
      "NORTHHAB" => "North Habersham",
      "NORTHHALLM" => "North Hall",
      "NORTHSIDEH" => "Northside",
      "NORTHWHITF" => "North Whitfield",
      "NORTHWSTRN" => "Northwestern",
      "OAKHILL" => "Oak Hill",
      "OCONEECOUN" => "Oconee County",
      "OGLEACAD" => "Oglethorpe Academy (6-8)",
      "OGLETHOR" => "Oglethorpe County",
      "OHIOOTH" => "Ohio - Other",
      "OKLAHOMAOTH" => "Oklahoma - Other",
      "OLA" => "Ola",
      "OREGONOTH" => "Oregon - Other",
      "OTHE" => "White - Other",
      "OTHERBAN" => "Banks - Other",
      "OTHERBT" => "Butts - Other",
      "OTHERCAR" => "Carroll - Other",
      "OTHERCAT" => "Catoosa - Other",
      "OTHERCHA" => "Chattooga - Other",
      "OTHERCLA" => "Clarke - Other",
      "OTHERDAD" => "Dade - Other",
      "OTHERFLO" => "Floyd - Other",
      "OTHERFRA" => "Franklin - Other",
      "OTHERGIL" => "Gilmer - Other",
      "OTHERGOR" => "Gordon - Other",
      "OTHERGRE" => "Greene - Other",
      "OTHERHAB" => "Habersham - Other",
      "OTHERHAN" => "Hancock - Other",
      "OTHERHAR" => "Haralson - Other",
      "OTHERHEA" => "Heard - Other",
      "OTHERJAC" => "Jackson - Other",
      "OTHERJAS" => "Jasper - Other",
      "OTHERLUM" => "Lumpkin - Other",
      "OTHERMAD" => "Madison - Other",
      "OTHERMOR" => "Morgan - Other",
      "OTHERMUR" => "Murray - Other",
      "OTHEROCO" => "Oconee - Other",
      "OTHEROGL" => "Oglethorpe - Other",
      "OTHERPIC" => "Pickens - Other",
      "OTHERPOL" => "Polk - Other",
      "OTHERPUT" => "Putnam - Other",
      "OTHERRAB" => "Rabun - Other",
      "OTHERROC" => "Rockdale - Other",
      "OTHERSTE" => "Stephens - Other",
      "OTHERWAL" => "Walker - Other",
      "OTHERWHI" => "Whitfield - Other",
      "OTWELL" => "Otwell",
      "PALMER" => "Palmer",
      "PARKS" => "Parks",
      "PATRICKHENRY" => "Patrick Henry",
      "PAUOT" => "Paulding - Other",
      "PBRITCH" => "P.B. Ritch",
      "PEACHOTH" => "Peach - Other",
      "PEACHTREE" => "Peachtree",
      "PELHAM" => "Pelham",
      "PENNSYLVANIAOTH" => "Pennsylvania - Other",
      "PEPPERELL" => "Pepperell",
      "PERRY" => "Perry",
      "PICKENS" => "Pickens County",
      "PIERCECO" => "Pierce County",
      "PIERCEOTH" => "Pierce - Other",
      "PIKECOUN" => "Pike County",
      "PIKEOTH" => "Pike - Other",
      "PINEGROVE" => "Pine Grove",
      "PINEHILL" => "Pine Hill",
      "PINEMT" => "Pine Mountain",
      "PINEYGROVE" => "Piney Grove",
      "PNCKNYVLLE" => "Pinckneyville",
      "PORTAL" => "Portal",
      "PRICE" => "Price",
      "PTSOUTH" => "Pointe South",
      "PULASKIC" => "Pulaski County",
      "PULASKIOTH" => "Pulaski - Other",
      "PUTNAMCO" => "Putnam County",
      "PWEST" => "Paul D. West",
      "QUITMANOTH" => "Quitman - Other",
      "RABUNCOU" => "Rabun County",
      "RADIUMSP" => "Radium Springs",
      "RANDOLPH" => "Randolph Clay",
      "RANDOLPHOTH" => "Randolph - Other",
      "REDAN" => "Redan",
      "REDBUD" => "Red Bud",
      "REHOBRD" => "Rehoboth Road",
      "REIDSVIL" => "Reidsville",
      "RENAISSANCE" => "Renaissance",
      "REXMILL" => "Rex Mill",
      "RHODEISLANOTH" => "Rhode Island - Other",
      "RICHARDSG" => "Richards - Gwinnett",
      "RICHARDSM" => "Richards - Muscogee",
      "RICHMOND" => "Richmond Hill",
      "RICHMONDOTH" => "Richmond - Other",
      "RIDGEVIEW" => "Ridgeview Charter",
      "RINGGOLD" => "Ringgold",
      "RISINGSTAR" => "Rising Star",
      "RISLEY" => "Risley",
      "RIVERDALE" => "Riverdale",
      "RIVERSIDEC" => "Riverside",
      "RIVERTRAIL" => "River Trail",
      "RIVERVIEW" => "Riverview",
      "RIVERWATCH" => "Riverwatch",
      "ROBERTAC" => "Robert A. Cross",
      "ROBERTSMD" => "M.D. Roberts",
      "ROCKMART" => "Rockmart",
      "ROME" => "Rome",
      "ROSSVILLE" => "Rossville",
      "ROTHSCHI" => "Rothschild",
      "RRUSSELL" => "Russell",
      "RUTLAND" => "Rutland",
      "SADDLERIDGE" => "Saddle Ridge",
      "SAINTMAR" => "Saint Marys",
      "SALEM" => "Salem",
      "SAMMYEECOAN" => "Coan",
      "SANDTOWN" => "Sandtown",
      "SANDYSPGS" => "Sandy Springs",
      "SCHLEY" => "Schley",
      "SCHLEYOTH" => "Schley - Other",
      "SCOGGINS" => "Carl Scoggins Sr.",
      "SCREVENC" => "Screven County",
      "SCREVENOTH" => "Screven - Other",
      "SDAKOTAOTH" => "South Dakota - Other",
      "SEGO" => "Sego",
      "SEMINOLE" => "Seminole County",
      "SEMINOLEOTH" => "Seminole - Other",
      "SEQUOYA" => "Sequoyah - DeKalb",
      "SEQUOYAH" => "Sequoyah - Clayton",
      "SEWELL" => "Sewell",
      "SHAMROCK" => "Druid Hills",
      "SHILOH" => "Shiloh",
      "SHIVER" => "Shiver",
      "SHUMAN" => "Shuman",
      "SIMPSON" => "Simpson",
      "SMITHA" => "Smitha",
      "SMOKEYRD" => "Smokey Road",
      "SNELLVILLE" => "Snellville",
      "SNELSONG" => "Snelson-Golden",
      "SOCIALCIRCLE" => "Social Circle",
      "SONORAVILLEE" => "Sonoraville East",
      "SOUTHCAROLINA" => "South Carolina - Other",
      "SOUTHCENT" => "South Central",
      "SOUTHEAS" => "Southeast Bulloch",
      "SOUTHEFF" => "South Effingham",
      "SOUTHFORS" => "South Forsyth",
      "SOUTHHAB" => "South Habersham",
      "SOUTHHALLM" => "South Hall",
      "SOUTHPAUL" => "South Paulding",
      "SOUTHSID" => "Southside",
      "SOUTHWES" => "Southwest",
      "SPALDOTH" => "Spalding - Other",
      "SPIRITCR" => "Spirit Creek",
      "STALEY" => "Sumter County Intermediate",
      "STALLINGSISLAND" => "Stallings Island",
      "STEPHENS" => "Stephens County",
      "STEPHENSON" => "Stephenson",
      "STEWART" => "Stewart",
      "STEWARTC" => "Stewart County",
      "STEWARTOTH" => "Stewart - Other",
      "STOCKBRIDG" => "Stockbridge",
      "STONEMTM" => "Stone Mountain",
      "SUMMEROUR" => "Summerour",
      "SUMMERVI" => "Summerville",
      "SUMTERCO" => "Sumter County",
      "SUMTEROTH" => "Sumter - Other",
      "SUTTON" => "Sutton",
      "SWAINSBO" => "Swainsboro",
      "SWEETWATER" => "Sweetwater",
      "SYLVANHIL" => "Sylvan Hills",
      "TALBOTOTH" => "Talbot - Other",
      "TALIAFER" => "Taliaferro County",
      "TALIAFERROOTH" => "Taliaferro - Other",
      "TAPP" => "Tapp",
      "TATTNALLOTH" => "Tattnall - Other",
      "TAYLORCO" => "Taylor County",
      "TAYLOROTH" => "Taylor - Other",
      "TAYLORRD" => "Taylor Road",
      "TEASLEY" => "Teasley",
      "TELFAIRC" => "Telfair County",
      "TELFAIROTH" => "Telfair - Other",
      "TEMPLE" => "Temple",
      "TENNESSEE" => "Tennessee - Other",
      "TERRELLC" => "Terrell County",
      "TERRELLOTH" => "Terrell - Other",
      "TEXASOTH" => "Texas - Other",
      "THOMASCOUNTY" => "Thomas County",
      "THOMASOTH" => "Thomas - Other",
      "THOMSON" => "Thomson-McDuffie",
      "THOMSONH" => "Thomson - Houston",
      "TIFTOTH" => "Tift - Other",
      "TJELDER" => "T.J. Elder",
      "TOOMBSCO" => "Toombs County",
      "TOOMBSOTH" => "Toombs - Other",
      "TOWNSCOU" => "Towns County Comprehensive(PK-12)",
      "TOWNSE" => "Towns - Other",
      "TREUTLEN" => "Treutlen",
      "TREUTLENOTH" => "Treutlen - Other",
      "TRICKUM" => "Trickum",
      "TRION" => "Trion",
      "TROUPOTH" => "Troup - Other",
      "TUBMAN" => "Tubman",
      "TUCKER" => "Tucker",
      "TURNERCO" => "Turner County",
      "TURNERD" => "Turner - Douglas",
      "TURNEROTH" => "Turner - Other",
      "TURNERSPEC" => "Turner County Specialty(Pk-12)",
      "TUTT" => "Tutt",
      "TWIGGSCO" => "Twiggs",
      "TWIGGSOTH" => "Twiggs - Other",
      "TWINRIVERS" => "Twin Rivers",
      "UNIONCOU" => "Union County",
      "UNIONGRVM" => "Union Grove",
      "UNIONOTH" => "Union - Other",
      "UPSONLE" => "Upson-Lee",
      "UPSONOTH" => "Upson - Other",
      "UTAHOTH" => "Utah - Other",
      "VALDOSTA" => "Valdosta",
      "VALLEYPOIN" => "Valley Point",
      "VERMONTOTH" => "Vermont - Other",
      "VETERANS" => "Veterans Memorial - Newton",
      "VETERANSMEMORIAL" => "Veterans Memorial - Muscogee",
      "VICKERYM" => "Vickery Creek",
      "VILLARICA" => "Villa Rica",
      "VIRGINIAOTH" => "Virginia - Other",
      "WALKER" => "Walker",
      "WALOT" => "Walton - Other",
      "WARECOUN" => "Ware County",
      "WAREMAGN" => "Ware Magnet(Kk-12)",
      "WAREOTH" => "Ware - Other",
      "WARNERRO" => "Warner Robins",
      "WARREN" => "Warren County",
      "WARRENOTH" => "Warren - Other",
      "WASHINGOTH" => "Washington - Other",
      "WASHINGT" => "Washington",
      "WASHINGTONOTH" => "Washington State - Other",
      "WASHWILKES" => "Washington-Wilkes",
      "WATSONCE" => "Watson Center (6-8)",
      "WAYCROSS" => "Waycross",
      "WAYNEOTH" => "Wayne - Other",
      "WEAVER" => "Weaver",
      "WEBBBR" => "Webb Bridge",
      "WEBSTER" => "Webster County",
      "WEBSTEROTH" => "Webster - Other",
      "WESTBAINBR" => "Bainbridge",
      "WESTCHAT" => "West Chatham",
      "WESTCOFF" => "West Coffee",
      "WESTHALLM" => "West Hall",
      "WESTJACKSON" => "West Jackson",
      "WESTLAUR" => "West Laurens",
      "WESTMINI" => "Westminister Christian Academy (K-12)",
      "WESTSI" => "Westside - Whitfield",
      "WESTSIDE" => "Westside - Barrow",
      "WESTSIDET" => "West Side Magnet(3-8)",
      "WHEELERC" => "Wheeler County",
      "WHEELEROTH" => "Wheeler - Other",
      "WHIGHAM" => "Whigham",
      "WHITECOU" => "White County",
      "WHITEEDDIE" => "Eddie White Academy",
      "WHITEWATER" => "Whitewater",
      "WILBANKS" => "Wilbanks",
      "WILCOX" => "Wilcox County",
      "WILCOXOTH" => "Wilcox - Other",
      "WILKESOTH" => "Wilkes - Other",
      "WILKINSO" => "Wilkinson County",
      "WILKINSONOTH" => "Wilkinson - Other",
      "WILLIAMHUBBARD" => "William M. Hubbard",
      "WILLIAMJ" => "William James",
      "WILLIEJW" => "Willie J. Williams",
      "WINDERBARWINDER" => "Winder-Barrow",
      "WISCONSINOTH" => "Wisconsin - Other",
      "WOODLAND" => "Woodland - Fulton",
      "WOODLANDB" => "Woodland - Bartow",
      "WOODLANDH" => "Woodland - Henry",
      "WOODSTOCK" => "Woodstock",
      "WOODYGAP" => "Woody Gap",
      "WORTHCOU" => "Worth County",
      "WORTHOTH" => "Worth - Other",
      "WRENS" => "Wrens",
      "WVIRGINIAOTH" => "West Virginia - Other",
      "WYOMINGOTH" => "Wyoming - Other",
      "YEAGER" => "Yeager",
      "YOUNG" => "Young",
      "YOUTH" => "Youth"
    },
    :mls_high_school => {
      "9THGRADE" => "9Th Grade Academy",
      "ADAIRSVILL" => "Adairsville",
      "ALABAMA" => "Alabama - Other",
      "ALASKAOTH" => "Alaska - Other",
      "ALBANY" => "Albany",
      "ALCOVY" => "Alcovy",
      "ALEXANDER" => "Alexander",
      "ALLATOONA" => "Allatoona",
      "ALPHARETTA" => "Alpharetta",
      "AMERICUS" => "Americus Sumter 9th Grade Academy",
      "APALACHEE" => "Apalachee",
      "APPLINGC" => "Appling County",
      "APPLINGOTH" => "Appling - Other",
      "ARABIA" => "Arabia Mountain",
      "ARCHER" => "Archer",
      "ARIZONAOTH" => "Arizona - Other",
      "ARKANSASOTH" => "Arkansas - Other",
      "ARMUCHEE" => "Armuchee",
      "ATKINSON" => "Atkinson County",
      "ATKINSONOTH" => "Atkinson - Other",
      "BACONCOU" => "Bacon County",
      "BACONOTH" => "Bacon - Other",
      "BAINBRIDG" => "Bainbridge - Decatur",
      "BAKER" => "Baker County",
      "BAKEROTH" => "Baker - Other",
      "BALDWIN" => "Baldwin",
      "BALDWINOTH" => "Baldwin - Other",
      "BANKSCOU" => "Banks County",
      "BAROT" => "Bartow - Other",
      "BEACH" => "Beach",
      "BEBANNEK" => "Banneker",
      "BENHILLOTH" => "Ben Hill - Other",
      "BERKMAR" => "Berkmar",
      "BERRIEN" => "Berrien",
      "BERRIENOTH" => "Berrien - Other",
      "BIBBOTH" => "Bibb - Other",
      "BISHOPHALL" => "Bishop Hall (9-12)",
      "BLECKLEY" => "Bleckley County",
      "BLECKLEYOTH" => "Bleckley - Other",
      "BOWDON" => "Bowdon",
      "BRADWELL" => "Bradwell Institute",
      "BRANTLEY" => "Brantley County",
      "BRANTLEYOTH" => "Brantley - Other",
      "BREMEN" => "Bremen",
      "BROOKSCO" => "Brooks County",
      "BROOKSOTH" => "Brooks - Other",
      "BROOKWOOD" => "Brookwood",
      "BRUNSWIC" => "Brunswick",
      "BRWOT" => "Barrow - Other",
      "BRYANCOU" => "Bryan County",
      "BRYANOTH" => "Bryan - Other",
      "BUFORD" => "Buford",
      "BULLOCHOTH" => "Bulloch - Other",
      "BURKECOU" => "Burke County",
      "BURKEOTH" => "Burke - Other",
      "BUTLER" => "Butler",
      "CAIRO" => "Cairo",
      "CALHOUN" => "Calhoun",
      "CALHOUNC" => "Calhoun County",
      "CALHOUNOTH" => "Calhoun - Other",
      "CALIFORNIAOTH" => "California - Other",
      "CALLAWAY" => "Callaway",
      "CAMBRIDGE" => "Cambridge",
      "CAMDENCO" => "Camden County",
      "CAMDENOTH" => "Camden - Other",
      "CAMPBELL" => "Campbell",
      "CANDLEROTH" => "Candler - Other",
      "CARROLLT" => "Carrollton",
      "CARTERH" => "Cartersville",
      "CARVE" => "Carver - Fulton",
      "CARVER" => "Carver - Muscogee",
      "CASS" => "Cass",
      "CCHSNINTHG" => "Cchs Ninth Grade",
      "CDRGROVE" => "Cedar Grove",
      "CEDARSHO" => "Cedar Shoals",
      "CEDARTOW" => "Cedartown",
      "CENTENNIAL" => "Centennial",
      "CENTRAL" => "Central - Carroll",
      "CENTRALH" => "Central - Talbot",
      "CENTRALBIBB" => "Central - Bibb",
      "CENTRALGWI" => "Central Gwinnett",      
      "CHAMBLEE" => "Chamblee Charter",
      "CHAPELHILL" => "Chapel Hill",
      "CHARLTON" => "Charlton County",
      "CHARLTONOTH" => "Charlton - Other",
      "CHATHAMOTH" => "Chatham - Other",
      "CHATTAHOOC" => "Chattahoochee",
      "CHATTAHOOCHEECOU" => "Chattahoochee County",
      "CHATTAHOOCHEEOTH" => "Chattahoochee - Other",
      "CHATTOOG" => "Chattooga",
      "CHEOT" => "Cherokee - Other",
      "CHEROKEE" => "Cherokee",
      "CHESTATEE" => "Chestatee",
      "CLAOT" => "Clayton - Other",
      "CLARKECE" => "Clarke Central",
      "CLARKSTON" => "Clarkston",
      "CLAXTON" => "Claxton",
      "CLAYOTH" => "Clay - Other",
      "CLINCHCO" => "Clinch County",
      "CLINCHOTH" => "Clinch - Other",
      "COAHULLA" => "Coahulla Creek",
      "COBOT" => "Cobb - Other",
      "COFFEECO" => "Coffee",
      "COFFEEOTH" => "Coffee - Other",
      "COLLINSHIL" => "Collins Hill",
      "COLORADOOTH" => "Colorado - Other",
      "COLQUITT" => "Colquitt County",
      "COLQUITTOTH" => "Colquitt - Other",
      "COLUMBIAH" => "Columbia",
      "COLUMBIAOTH" => "Columbia - Other",
      "COLUMBUS" => "Columbus",
      "COMMERCE" => "Commerce",
      "CONNECTICUTOTH" => "Connecticut - Other",
      "COOKCOUN" => "Cook",
      "COOKOTH" => "Cook - Other",
      "COOSA" => "Coosa",
      "COWOT" => "Coweta - Other",
      "CRAWFORD" => "Crawford County",
      "CRAWFORDOTH" => "Crawford - Other",
      "CREEKSIDE" => "Creekside",
      "CREEKVIEWC" => "Creekview",
      "CRIM" => "Crim",
      "CRISPCOU" => "Crisp County",
      "CRISPOTH" => "Crisp - Other",
      "CROSSCRE" => "Cross Creek",
      "CROSSKEYS" => "Cross Keys",
      "CROSSROAD" => "Crossroads - Cherokee",
      "CROSSROADS" => "Crossroads Academy",
      "CRSSROADS" => "Crossroads ",
      "DACULA" => "Dacula",
      "DADECOUN" => "Dade County",
      "DALTON" => "Dalton",
      "DAVIDSON" => "Davidson Magnet(6-12)",
      "DAWOT" => "Dawson - Other",
      "DAWSON" => "Dawson County",
      "DECATUR" => "Decatur",
      "DECATUROTH" => "Decatur - Other",
      "DEKALBSCHO" => "Dekalb School Of The Arts",
      "DEKOT" => "Dekalb - Other",
      "DELAWAREOTH" => "Delaware - Other",
      "DISCOVERY" => "Discovery",
      "DOCOTH" => "District Of Columbia - Other",
      "DODGECOU" => "Dodge County",
      "DODGEOTH" => "Dodge - Other",
      "DOOLYCOU" => "Dooly County",
      "DOOLYOTH" => "Dooly - Other",
      "DOUGHERT" => "Dougherty Comprehensive",
      "DOUGHERTYOTH" => "Dougherty - Other",
      "DOUGLAS" => "Douglas County",
      "DOUGLASS" => "Douglass",
      "DOUOT" => "Douglas - Other",
      "DREWC" => "Charles R. Drew",
      "DRUIDHLLS" => "Druid Hills",
      "DUBLIN" => "Dublin",
      "DULUTH" => "Duluth",
      "DUNWOODY" => "Dunwoody",
      "DUTCHTOWN" => "Dutchtown",
      "EAGLESLAN" => "Eagles Landing",
      "EARLYCOU" => "Early County",
      "EARLYOTH" => "Early - Other",
      "EASTHALL" => "East Hall",
      "EASTJACKSON" => "East Jackson",
      "EASTLAUR" => "East Laurens",
      "EASTPAULD" => "East Paulding",
      "EASTSIDE" => "Eastside",
      "ECHOLSCO" => "Echols County",
      "ECHOLSOTH" => "Echols - Other",
      "ECOWETA" => "East Coweta",
      "EFFINGHA" => "Effingham County",
      "EFFINGHAMOTH" => "Effingham - Other",
      "ELANDREWS" => "Elizabeth Andrews",
      "ELBERTCO" => "Elbert County",
      "ELBERTOTH" => "Elbert - Other",
      "EMANUELC" => "Emanuel County Institute",
      "EMANUELOTH" => "Emanuel - Other",
      "ETOWAH" => "Etowah",
      "EVANS" => "Evans",
      "EVANSOTH" => "Evans - Other",
      "FANNINCO" => "Fannin County",
      "FANNINOTH" => "Fannin - Other",
      "FAYETTECO" => "Fayette County",
      "FAYOT" => "Fayette - Other",
      "FITZGERA" => "Fitzgerald",
      "FLORIDA" => "Florida - Other",
      "FLOWERYBR" => "Flowery Branch",
      "FORESTPRK" => "Forest Park",
      "FOROT" => "Forsyth - Other",
      "FORSYTHCE" => "Forsyth Central",
      "FORSYTHNO" => "North Forsyth",
      "FORSYTHSO" => "South Forsyth",
      "FRANKLIN" => "Franklin County",
      "FULOT" => "Fulton - Other",
      "GAINESVILL" => "Gainesville",
      "GILMER" => "Gilmer",
      "GLASCOCK" => "Glascock County",
      "GLASCOCKOTH" => "Glascock - Other",
      "GLENNHIL" => "Glenn Hills",
      "GLYNNACA" => "Glynn Academy",
      "GLYNNOTH" => "Glynn - Other",
      "GORDONCENTRAL" => "Gordon Central",
      "GORDONLEE" => "Gordon Lee",
      "GORDONSTRE" => "Gordon Street",
      "GRADY" => "Grady",
      "GRADYOTH" => "Grady - Other",
      "GRAYSON" => "Grayson",
      "GREENBRI" => "Greenbrier",
      "GREENECO" => "Greene County",
      "GREENVIL" => "Greenville",
      "GRIFFIN" => "Griffin",
      "GRIOT" => "Spalding - Other",
      "GROVES" => "Groves",
      "GROVETOWN" => "Grovetown",
      "GWIOT" => "Gwinnett - Other",
      "HABERSHA" => "Habersham Central",
      "HALOT" => "Hall - Other",
      "HAMPTON" => "Hampton",
      "HANCOCKC" => "Hancock Central",
      "HARALSONCO" => "Haralson County",
      "HARDAWAY" => "Hardaway",
      "HARLEM" => "Harlem",
      "HARRISCO" => "Harris County",
      "HARRISON" => "Harrison",
      "HARRISOTH" => "Harris - Other",
      "HARTCOUN" => "Hart County",
      "HARTOTH" => "Hart - Other",
      "HAVENACADE" => "Haven Academy",
      "HAWAIIOTH" => "Hawaii - Other",
      "HAWKINSV" => "Hawkinsville",
      "HEARDCOU" => "Heard County",
      "HENOT" => "Henry - Other",
      "HENRY" => "Henry County",
      "HEPHZIBA" => "Hephzibah",
      "HERITAGE" => "Heritage - Rockdale",
      "HERITAGEC" => "Heritage - Catoosa",
      "HILLGROVE" => "Hillgrove",
      "HIRAMP" => "Hiram",
      "HOUSTONC" => "Houston County",
      "HOUSTONOTH" => "Houston - Other",
      "HOUSTONTECH" => "Houston County Career & Technology Ctr (9-12)",
      "HOWARD" => "Howard",
      "IDAHOOTH" => "Idaho - Other",
      "ILLINOISOTH" => "Illinois - Other",
      "INDEPEND" => "Independence",
      "INDIANAOTH" => "Indiana - Other",
      "IOWAOTH" => "Iowa - Other",
      "IRWINCOU" => "Irwin County",
      "IRWINOTH" => "Irwin - Other",
      "ISLANDS" => "Islands",
      "JACKOTHER" => "Jackson - Other",
      "JACKSON" => "Jackson",
      "JACKSONCOUNTY" => "Jackson County",
      "JASPERCO" => "Jasper County",
      "JEFFCOUNTY" => "Jefferson County",
      "JEFFDAVI" => "Jeff Davis",
      "JEFFDAVISOTH" => "Jeff Davis - Other",
      "JEFFERSO" => "Jefferson",
      "JEFFERSONOTH" => "Jefferson - Other",
      "JENKINS" => "Jenkins",
      "JENKINSC" => "Jenkins County",
      "JENKINSOTH" => "Jenkins - Other",
      "JOHNCHAT" => "Johnson - Chatham",
      "JOHNSCREEK" => "Johns Creek",
      "JOHNSON" => "Johnson - Hall",
      "JOHNSONC" => "Johnson County",
      "JOHNSONM" => "Johnson Magnet (9-12)",
      "JOHNSONOTH" => "Johnson - Other",
      "JONESBORO" => "Jonesboro",
      "JONESCOU" => "Jones County",
      "JONESOTH" => "Jones - Other",
      "JORDANVO" => "Jordan Vocational",
      "JOSEY" => "Josey",
      "KANSASOTH" => "Kansas - Other",
      "KELL" => "Kell",
      "KENDRICK" => "Kendrick",
      "KENNESAW" => "Kennesaw Mountain",
      "KENTUCKYOTH" => "Kentucky - Other",
      "LAFAYETTE" => "LaFayette",
      "LAGRANGE" => "Lagrange",
      "LAKESIDE" => "Lakeside - Dekalb",
      "LAKESIDEC" => "Lakeside - Columbia",
      "LAKEVIEW" => "Lakeview-Fort Oglethorpe",
      "LAMARCOU" => "Lamar County Comprehensive",
      "LAMAROTH" => "Lamar - Other",
      "LAMBERT" => "Lambert",
      "LANEY" => "Laney",
      "LANGSTONHUGHES" => "Langston Hughes",
      "LANIER" => "Lanier",
      "LANIERCO" => "Lanier County",
      "LANIEROTH" => "Lanier - Other",
      "LASSITER" => "Lassiter",
      "LAURENSOTH" => "Laurens - Other",
      "LEECOUNT" => "Lee County",
      "LEEOTH" => "Lee - Other",
      "LIBERTYC" => "Liberty County",
      "LIBERTYOTH" => "Liberty - Other",
      "LINCOLNC" => "Lincoln County",
      "LINCOLNOTH" => "Lincoln - Other",
      "LITHASPGS" => "Lithia Springs",
      "LITHONIA" => "Lithonia",
      "LOCUSTGRV" => "Locust Grove",
      "LOGANVILLE" => "Loganville",
      "LONGCOUN" => "Long County",
      "LONGOTH" => "Long - Other",
      "LOUISIANAOTH" => "Louisiana - Other",
      "LOVEJOY" => "Lovejoy",
      "LOW" => "Low",
      "LOWNDES" => "Lowndes",
      "LOWNDESOTH" => "Lowndes - Other",
      "LUELLA" => "Luella",
      "LUMPKIN" => "Lumpkin County",
      "MACONCOU" => "Macon County",
      "MACONOTH" => "Macon - Other",
      "MADISONC" => "Madison County",
      "MAINEOTH" => "Maine - Other",
      "MANCHEST" => "Manchester",
      "MARGARETHARRIS" => "Margaret Harris",
      "MARIETTA" => "Marietta",
      "MARIONCOUNTY" => "Marion County",
      "MARIONOTH" => "Marion - Other",
      "MARTINLUTHERKINGJR" => "Martin Luther King Jr",
      "MARYLANDOTH" => "Maryland - Other",
      "MARYPERS" => "Mary Persons",
      "MASSACHUSETTSOTH" => "Massachusetts - Other",
      "MAYS" => "Mays",
      "MCCLARIN" => "McClarin",
      "MCDUFFIEOTH" => "Mcduffie - Other",
      "MCEACHERN" => "McEachern",
      "MCINTOSH" => "Mcintosh",
      "MCINTOSHCNT" => "McIntosh Academy",
      "MCINTOSHOTH" => "Mcintosh - Other",
      "MCNAIR" => "McNair",
      "MEADOWCRK" => "Meadowcreek",
      "MERIWETHEROTH" => "Meriwether - Other",
      "METTER" => "Metter",
      "MICHIGANOTH" => "Michigan - Other",
      "MILLCREEK" => "Mill Creek",
      "MILLERCO" => "Miller County",
      "MILLERGROVE" => "Miller Grove",
      "MILLEROTH" => "Miller - Other",
      "MILTON" => "Milton",
      "MINNESOTAOTH" => "Minnesota - Other",
      "MISSISSIPPIOTH" => "Mississippi - Other",
      "MISSOURIOTH" => "Missouri - Other",
      "MITCHELL" => "Mitchell County",
      "MITCHELLOTH" => "Mitchell - Other",
      "MODEL" => "Model",
      "MONRO" => "Monroe - Dougherty",
      "MONROE" => "Monroe Area",
      "MONROEOTH" => "Monroe - Other",
      "MONTANAOTH" => "Montana - Other",
      "MONTGOME" => "Montgomery County",
      "MONTGOMERYOTH" => "Montgomery - Other",
      "MORGANCO" => "Morgan County",
      "MORROW" => "Morrow",
      "MOUNTAIN" => "Mountain Education Center (9-12)",
      "MOUNTAINVIEW" => "Mountain View",
      "MTZIONCL" => "Mount Zion - Clayton",
      "MTZIONCR" => "Mt. Zion - Carroll",
      "MUNDYSMILL" => "Mundys Mill",
      "MURRAYCO" => "Murray County",
      "MUSCOGEEOTH" => "Muscogee - Other",
      "NDAKOTAOTH" => "North Dakota - Other",
      "NEBIBB" => "Northeast",
      "NEBRASKAOTH" => "Nebraska - Other",
      "NETIFT" => "Northeast Campus Tift County",
      "NEVADAOTH" => "Nevada - Other",
      "NEWHAMPSHIREOTH" => "New Hampshire - Other",
      "NEWHAMSTEAD" => "New Hamstead",
      "NEWJERSEYOTH" => "New Jersey - Other",
      "NEWMANCHESTER" => "New Manchester",
      "NEWMEXICOOTH" => "New Mexico - Other",
      "NEWNAN" => "Newnan",
      "NEWOT" => "Newton - Other",
      "NEWTON" => "Newton",
      "NEWYORKOTH" => "New York - Other",
      "NORCROSS" => "Norcross",
      "NORTHATLA" => "North Atlanta",
      "NORTHCAROLINA" => "North Carolina - Other",
      "NORTHCLTN" => "North Clayton",
      "NORTHCOBB" => "North Cobb",
      "NORTHGATE" => "Northgate",
      "NORTHGWIN" => "North Gwinnett",
      "NORTHHALL" => "North Hall",
      "NORTHMUR" => "North Murray",
      "NORTHOCONEE" => "North Oconee",
      "NORTHPAULDING" => "North Paulding",
      "NORTHSIDH" => "Northside - Houston",
      "NORTHSIDM" => "Northside - Muscogee",
      "NORTHSPGS" => "North Springs",
      "NORTHVIEW" => "Northview",
      "NORTHWESTW" => "Northwest Whitfield County",
      "OAKWOOD" => "Oakwood",
      "OCONEECOUN" => "Oconee County",
      "OGLETHOR" => "Oglethorpe County",
      "OHIOOTH" => "Ohio - Other",
      "OKLAHOMAOTH" => "Oklahoma - Other",
      "OLA" => "Ola",
      "OREGONOTH" => "Oregon - Other",
      "OSBORNE" => "Osborne",
      "OTHE" => "Other",
      "OTHERBAN" => "Banks - Other",
      "OTHERBT" => "Butts - Other",
      "OTHERCAR" => "Carroll - Other",
      "OTHERCAT" => "Catoosa - Other",
      "OTHERCHA" => "Chattooga - Other",
      "OTHERCLA" => "Clarke - Other",
      "OTHERDAD" => "Dade - Other",
      "OTHERFLO" => "Floyd - Other",
      "OTHERFRA" => "Franklin - Other",
      "OTHERGIL" => "Gilmer - Other",
      "OTHERGOR" => "Gordon - Other",
      "OTHERGRE" => "Greene - Other",
      "OTHERHAB" => "Habersham - Other",
      "OTHERHAN" => "Hancock - Other",
      "OTHERHAR" => "Haralson - Other",
      "OTHERHEA" => "Heard - Other",
      "OTHERJAS" => "Jasper - Other",
      "OTHERLUM" => "Lumpkin - Other",
      "OTHERMAD" => "Madison - Other",
      "OTHERMOR" => "Morgan - Other",
      "OTHERMUR" => "Murray - Other",
      "OTHEROCO" => "Oconee - Other",
      "OTHEROGL" => "Oglethorpe - Other",
      "OTHERPIC" => "Pickens - Other",
      "OTHERPOL" => "Polk - Other",
      "OTHERPUT" => "Putnam - Other",
      "OTHERRAB" => "Rabun - Other",
      "OTHERROC" => "Rockdale - Other",
      "OTHERSTE" => "Stephens - Other",
      "OTHERWAL" => "Walker - Other",
      "OTHERWHI" => "White - Other",
      "OTHERWHT" => "Whitfield - Other",
      "OTHERWLT" => "Walton - Other",
      "PARKVIEW" => "Parkview",
      "PATHENRY" => "Patrick Henry",
      "PAULDING" => "Paulding County",
      "PAUOT" => "Paulding - Other",
      "PEACHCOU" => "Peach County",
      "PEACHOTH" => "Peach - Other",
      "PEACHTREERIDGE" => "Peachtree Ridge",
      "PEBBLEBRK" => "Pebblebrook",
      "PELHAM" => "Pelham",
      "PENNSYLVANIAOTH" => "Pennsylvania - Other",
      "PEPPERELL" => "Pepperell",
      "PERRY" => "Perry",
      "PHOENI" => "Phoenix - Whitfield",
      "PHOENIX" => "Phoenix - Gwinnett",
      "PICKENS" => "Pickens",
      "PIERCECO" => "Pierce County",
      "PIERCEOTH" => "Pierce - Other",
      "PIKECOUN" => "Pike County",
      "PIKEOTH" => "Pike - Other",
      "POPE" => "Pope",
      "PORTAL" => "Portal",
      "PULASKIOTH" => "Pulaski - Other",
      "PUTNAMCO" => "Putnam County",
      "QUITMANCO" => "Quitman County",
      "QUITMANOTH" => "Quitman - Other",
      "RABUNCOU" => "Rabun County",
      "RANDOLPH" => "Randolph Clay",
      "RANDOLPHOTH" => "Randolph - Other",
      "REDANH" => "Redan",
      "RHODEISLANOTH" => "Rhode Island - Other",
      "RICHACAD" => "Academy Of Richmond County",
      "RICHMOND" => "Richmond Hill",
      "RICHMONDOTH" => "Richmond - Other",
      "RIDGELAND" => "Ridgeland",
      "RINGGOLD" => "Ringgold",
      "RIVERDALE" => "Riverdale",
      "RIVERRIDGE" => "River Ridge",
      "RIVERWOOD" => "Riverwood International Charter",
      "ROCKDALE" => "Rockdale County",
      "ROCKMART" => "Rockmart",
      "ROME" => "Rome",
      "ROSWELL" => "Roswell",
      "RUTLAND" => "Rutland",
      "SALEM" => "Salem",
      "SANDYCREE" => "Sandy Creek",
      "SAVANNAH" => "Savannah",
      "SAVARTS" => "Savannah Arts Academy (9-12)",
      "SCHLEY" => "Schley",
      "SCHLEYOTH" => "Schley - Other",
      "SCREVENC" => "Screven County",
      "SCREVENOTH" => "Screven - Other",
      "SDAKOTAOTH" => "South Dakota - Other",
      "SEBULL" => "Southeast Bulloch",
      "SEMINOLE" => "Seminole County",
      "SEMINOLEOTH" => "Seminole - Other",
      "SEQUOYAH" => "Sequoyah",
      "SHAW" => "Shaw",
      "SHILOH" => "Shiloh",
      "SOCIALCIR" => "Social Circle",
      "SONORAVILLE" => "Sonoraville",
      "SOUTHATL" => "South Atlanta",
      "SOUTHCAROLINA" => "South Carolina - Other",
      "SOUTHCOBB" => "South Cobb",
      "SOUTHEASTW" => "Southeast Whitfield County",
      "SOUTHEFF" => "South Effingham",
      "SOUTHGWIN" => "South Gwinnett",
      "SOUTHSIDE" => "Maynard H. Jackson, Jr.",
      "SPALDING" => "Spalding",
      "SPAULDING" => "South Paulding",
      "SPENCER" => "Spencer",
      "SPRAYBERRY" => "Sprayberry",
      "STARSMILL" => "Starrs Mill",
      "STATESBO" => "Statesboro",
      "STEPHENS" => "Stephens County",
      "STEPHENSON" => "Stephenson",
      "STEWARTOTH" => "Stewart - Other",
      "STEWQUIT" => "Stewart County",
      "STHWSTDKB" => "Southwest Dekalb",
      "STOCKBRIDG" => "Stockbridge",
      "STONEMTH" => "Stone Mountain",
      "SUMMERVILLE" => "Summerville",
      "SUMTERCO" => "Americus Sumter",
      "SUMTEROTH" => "Sumter - Other",
      "SWAINSBO" => "Swainsboro",
      "SWBIBB" => "Southwest",
      "TALBOTOTH" => "Talbot - Other",
      "TALIAFER" => "Taliaferro County",
      "TALIAFERROOTH" => "Taliaferro - Other",
      "TATTNALL" => "Tattnall County",
      "TATTNALLOTH" => "Tattnall - Other",
      "TAYLORCO" => "Taylor County",
      "TAYLOROTH" => "Taylor - Other",
      "TECHHIGH" => "Tech",
      "TELFAIRC" => "Telfair County",
      "TELFAIROTH" => "Telfair - Other",
      "TEMPLE" => "Temple",
      "TENNESSEE" => "Tennessee - Other",
      "TERRELLC" => "Terrell County",
      "TERRELLOTH" => "Terrell - Other",
      "TEXASOTH" => "Texas - Other",
      "THERRELL" => "Therrell",
      "THOMASCO" => "Thomas County Central",
      "THOMASOTH" => "Thomas - Other",
      "THOMASVILL" => "Thomasville",
      "THOMSON" => "Thomson",
      "TIFTCOUN" => "Tift County",
      "TIFTOTH" => "Tift - Other",
      "TOOMBSCO" => "Toombs County",
      "TOOMBSOTH" => "Toombs - Other",
      "TOWERS" => "Towers",
      "TOWNSCOU" => "Towns County",
      "TOWNSOTH" => "Towns - Other",
      "TREUTLEN" => "Treutlen",
      "TREUTLENOTH" => "Treutlen - Other",
      "TRICITIESTRI" => "Tri-Cities",
      "TRICOUN" => "Tri-County",
      "TRION" => "Trion",
      "TROUPCOU" => "Troup County",
      "TROUPOTH" => "Troup - Other",
      "TUCKER" => "Tucker",
      "TURNERCO" => "Turner County",
      "TURNEROTH" => "Turner - Other",
      "TURNERSPEC" => "Turner County Specialty(Pk-12)",
      "TWIGGSCO" => "Twiggs County",
      "TWIGGSOTH" => "Twiggs - Other",
      "UNIONCOU" => "Union County",
      "UNIONGRV" => "Union Grove",
      "UNIONOTH" => "Union - Other",
      "UPSONLE" => "Upson-Lee",
      "UPSONOTH" => "Upson - Other",
      "UTAHOTH" => "Utah - Other",
      "VALDOSTA" => "Valdosta",
      "VERMONTOTH" => "Vermont - Other",
      "VETERANS" => "Veterans",
      "VIDALIACOM" => "Vidalia Comprehensive",
      "VILLARICA" => "Villa Rica",
      "VIRGINIAOTH" => "Virginia - Other",
      "WALNUTGROVE" => "Walnut Grove",
      "WALTON" => "Walton",
      "WAM" => "Wam",
      "WARECOUN" => "Ware County",
      "WAREMAGN" => "Ware Magnet(Kk-12)",
      "WAREOTH" => "Ware - Other",
      "WARNERRO" => "Warner Robins",
      "WARRENCO" => "Warren County",
      "WARRENOTH" => "Warren - Other",
      "WASHCO" => "Washington County",
      "WASHINGOTH" => "Washington - Other",
      "WASHINGTON" => "Booker T. Washington",
      "WASHINGTONOTH" => "Washington State - Other",
      "WASHWILK" => "Washington-Wilkes",
      "WAYNECOU" => "Wayne County",
      "WAYNENIGHT" => "Wayne County Night",
      "WAYNEOTH" => "Wayne - Other",
      "WBIBB" => "Westside - Bibb",
      "WEBSTERCO" => "Webster County",
      "WEBSTEROTH" => "Webster - Other",
      "WESTFORSYTH" => "West Forsyth",
      "WESTHALL" => "West Hall",
      "WESTLAKE" => "Westlake",
      "WESTLAUR" => "West Laurens",
      "WESTMINI" => "Westminister Christian Academy (K-12)",
      "WESTOVER" => "Westover",
      "WESTSIDE" => "Westside - Richmond",
      "WHEELER" => "Wheeler",
      "WHEELERC" => "Wheeler County",
      "WHEELEROTH" => "Wheeler - Other",
      "WHITE" => "White County",
      "WHITEWATER" => "Whitewater",
      "WILCOX" => "Wilcox County",
      "WILCOXOTH" => "Wilcox - Other",
      "WILKESOTH" => "Wilkes - Other",
      "WILKINSO" => "Wilkinson County",
      "WILKINSONOTH" => "Wilkinson - Other",
      "WILLHUTCH" => "William S. Hutchings Career Ctr",
      "WINDERBAR" => "Winder-Barrow",
      "WINDSORF" => "Windsor Forest",
      "WISCONSINOTH" => "Wisconsin - Other",
      "WOODLANDB" => "Woodland - Bartow",
      "WOODLANDH" => "Woodland - Henry",
      "WOODSMILL" => "Wood's Mill Academy",
      "WOODSTOCK" => "Woodstock",
      "WOODYGAP" => "Woody Gap",
      "WORTHCOU" => "Worth County",
      "WORTHOTH" => "Worth - Other",
      "WVIRGINIAOTH" => "West Virginia - Other",
      "WVTHOMPKINS" => "Woodville-Thompkins",
      "WYOMINGOTH" => "Wyoming - Other"
    },
    :new_style_of_building => {
      "AFRAM" => "A-Frame",
      "BUNGL" => "Bungalow",
      "CABIN" => "Cabin",
      "CAPEC" => "Cape Cod",
      "CHALT" => "Chalet",
      "CLSTR" => "Cluster Home",
      "CNTMP" => "Contemporary",
      "CNTRY" => "Country",
      "COLON" => "Colonial",
      "COTTG" => "Cottage",
      "CRFMN" => "Craftsman",
      "EUROP" => "European",
      "FRNCH" => "French Provncial",
      "GRDEN" => "Garden (1 Level)",
      "HIRIS" => "High Rise (6 or more stories)",
      "LOFT" => "Loft",
      "MEDIT" => "Mediterranean",
      "MIRIS" => "Mid-Rise (up to 5 stories)",
      "MOBIL" => "Mobile",
      "MODLR" => "Modular",
      "OTHER" => "Other",
      "PATIO" => "Patio Home",
      "RANCH" => "Ranch",
      "RUSTC" => "Rustic",
      "TOWNH" => "Townhouse",
      "TRADT" => "Traditional",
      "TUDOR" => "Tudor",
      "VICTR" => "Victorian"
      
    },    
    :rental_class => {
     'BUILT' => 'townhouse_condo',
    'CLUST' => 'townhouse_condo',
    'CONDO' => 'townhouse_condo',
    'CONVR' => 'manufactured',
    'DEEDD' => 'townhouse_condo',
    'DUPLX' => 'single_family',
    'HOUSE' => 'single_family',
    'MOBIL' => 'manufactured',
    'MULTI' => 'multi_family',
    'OTHER' => 'commercial',
    'QUADP' => 'multi_family',
    'SINGL' => 'single_family',
    'TRIPL' => 'multi_family',
    'ZEROL' => 'townhouse_condo',
    },
    :rental_type => {
      'BUILT' => 'condo',
      'CLUST' => 'patio_home',
      'CONDO' => 'town_house',      
      'DEEDD' => 'town_house',
      'DUPLX' => 'duplex',
      'HOUSE' => 'freestanding',      
      'OTHER' => 'other',
      'QUADP' => 'fourplex',
      'SINGL' => 'freestanding',
      'TRIPL' => 'triplex',
      'ZEROL' => 'town_house',
    },
    :road_surface => {
      "BLACK" => "Black Top",
      "CNCRT" => "Concrete",
      "DIRT" => "Dirt",
      "GRAVL" => "Gravel",
      "NONE" => "None",
      "OTHER" => "Other"
    },
    :roof_type => {
      "BUILT" => "Built-Up",
      "COMPS" => "Composition",
      "CONCR" => "Concrete",
      "COPPR" => "Copper",
      "FLAT" => "Flat",
      "GLASS" => "FGlass/Asphalt",
      "METAL" => "Metal/Steel",
      "NONE" => "None",
      "OTHER" => "Other",
      "SLATE" => "Slate",
      "TARGV" => "Tar/Gravel",
      "TILE" => "Tile",
      "TIN" => "Tin",
      "WDSHN" => "Wood Shingle",
      "WOOD" => "Wood"
    },
    :single_family_type => {
      'BUILT' => 'townhome',
      'CLUST' => 'townhome',
      'CONDO' => 'townhome',
      'CONVR' => 'manufactured',
      'DEEDD' => 'townhome',
      'DUPLX' => 'duplex',
      'HOUSE' => 'single_family',
      'MOBIL' => 'manufactured',
      'MULTI' => 'multiplex',
      'OTHER' => 'other',
      'QUADP' => 'multiplex',
      'SINGL' => 'single_family',
      'TRIPL' => 'multiplex',
      'ZEROL' => 'townhome'
    },
    :new_siding_type => {
      "ALUMN" => "Aluminum",
      "BRK3S" => "Brick 3 Sides",
      "BRK4S" => "Brick 4 Sides",
      "BRKFR" => "Brick & Frame",
      "BRKFT" => "Brick-Front",
      "CEDAR" => "Cedar",
      "CNCRT" => "Cement Siding - Not Stucco",
      "FRAME" => "Frame",
      "LOG" => "Log",
      "METAL" => "Metal Siding",
      "OTHER" => "Other",
      "ROUGH" => "Rough Sawn",
      "SHNGL" => "Shingle",
      "STEEL" => "Steel Frame",
      "STONE" => "Stone",
      "STUCHARD" => "Stucco - Hardcoat",
      "STUCO" => "Stucco - Unknown",
      "STUCSYN" => "Stucco - Synthetic",
      "TABBY" => "Tabby",
      "VINYL" => "Vinyl"
    },
    :new_stories => {
      'MORE2' => '3',
      'ONEHL' => '1',
      'ONEST' => '1',
      'SPLFY' => '1',
      'SPLVL' => '1',
      'TWOST' => '2'
    },
    :street_surface_type => {
      "DIRT" => "Dirt",
      "EASEM" => "Easement",
      "GRAVL" => "Gravel",
      "INTST" => "Interstate",
      "NONE" => "None",
      "OTHER" => "Other",
      "PAVED" => "Paved",
      "PRIVT" => "Private Maintain",
      "PUBLC" => "Public Maintain",
      "STATE" => "State Highway",
      "USHWY" => "US Highway"
    },    
    :waterfront => {
      'LAKE' => 'Y',
      'LAKES' => 'Y',
      'NAVRV' => 'Y',
      'NONE' => 'N',
      'OTHER' => 'Y',
      'POND' => 'Y',
      'SPRNG' => 'Y',
      'STRMS' => 'Y'
    }
    

  }

  DATA_ENUMS2 = {

  }
  DATA_MAPPING2 = {
    access_to_complex: 'ComplexBuildingAccess',
    acreage: 'Acres',
    annual_maintenance_fee: 'AnnualAssocFee',
    basement_desc: 'BasementDesc',
    bathrooms_full: 'BathsFull',
    bathrooms_half: 'BathsHalf',
    beds: 'BedsTotal',
    # block: 'Block',
    building_stories: 'MainDwellingStories',
    category: proc { |d|
      cats = {
        'ATT' => 'townhouse_condo',
        'COM' => 'commercial',
        'DET' => 'single_family',
        'LND' => 'ranch_acerage',
        'LOT' => 'lots',
        'MUL' => 'multi_family',
        'RNT' => 'rental'
     }

     cats[d['PropertyType']]
    },
      # 'PropertyType',
    cdom: 'CDOM',
    city: 'City',
    closed_date: proc { |d|  to_date_sql(d['CloseDate'])  },
    cooling_system_type: 'CoolingDescription',
    county: 'CountyOrParish',
    dom: 'DOM',
    equipment: 'ApplianceDesc',
    expiration_date: proc { |d|  to_date_sql(d['ExpirationDate'])  },
    exterior_description: 'Exterior',
    fireplace_number: 'FireplacesNum',
    floor_type: 'FloorDescription',
    # garage_attach: 'ParkingDesc',
    garage_attach: proc {|d|
      ret = []
      retu = nil
      Delayed::Worker.logger.debug ">>garage_capacity: }"
      unless d['ParkingDesc'].blank?
        d['ParkingDesc'].split(",").each { |item|
          begin
            Delayed::Worker.logger.debug ">>item: #{item}"
            ite = nil
            its = {
              '12STEP' => 'N',
              '1CARP' => 'N',
              '1GARG' => 'Y',
              '1UNASSGN' => 'N',
              '2ASSGN' => 'N',
              '2CARP' => 'N',
              '2GARG' => 'Y',
              '2UNASSGN' => 'N',
              '3ASSGN' => 'N',
              '3CARP' => 'Y',
              '3UNASSGN' => 'N',
              '4GARG' => 'Y',
              'ASSGN' => 'N',
              'ATTCH' => 'N',
              'AUTOG' => 'N',
              'COVERED' => 'N',
              'CPORT' => 'N',
              'DEEDD' => 'N',
              'DRVUN' => 'N',
              'DRVWY' => 'N',
              'DTACH' => 'N',
              'ECCST' => 'N',
              'GARGE' => 'N',
              'KTCHN' => 'N',
              'NONE' => 'N',
              'ONESP' => 'N',
              'PDECK' => 'N',
              'PRKLT' => 'N',
              'PRKPD' => 'N',
              'SIDER' => 'N',
              'STORE' => 'N',
              'STRET' => 'N',
              'TWOSP' => 'N',
              'UNDER' => 'N'
              }
            ite = its[item]
            ret << ite if ite
          rescue
            ret = []
          end
          
        }

        Delayed::Worker.logger.debug ">>ret item: #{ret}"
        rsl = 0
        if (ret && (ret.length > 0))
          rs = ret.reject do |x| x.in?(['N']) end
          if (rs && (rs.length > 0))
            retu = 'Y'
          else
            retu = 'N'
          end
        else
          retu = nil
        end
      end
      retu
      
    },
    # garage_capacity: 'NumParkingSpaces',
    garage_capacity: proc {|d|
      ret = []
      Delayed::Worker.logger.debug ">>garage_capacity: }"
      unless d['ParkingDesc'].blank?
        d['ParkingDesc'].split(",").each { |item|
          begin
            Delayed::Worker.logger.debug ">>item: #{item}"
            ite = nil
            its = {
              '12STEP' => '0',
              '1CARP' => '1',
              '1GARG' => '1',
              '1UNASSGN' => '1',
              '2ASSGN' => '2',
              '2CARP' => '3',
              '2GARG' => '2',
              '2UNASSGN' => '3',
              '3ASSGN' => '4',
              '3CARP' => '4',
              '3UNASSGN' => '4',
              '4GARG' => '5',
              'ASSGN' => '1',
              'ATTCH' => '0',
              'AUTOG' => '0',
              'COVERED' => '0',
              'CPORT' => '0',
              'DEEDD' => '0',
              'DRVUN' => '0',
              'DRVWY' => '0',
              'DTACH' => '0',
              'ECCST' => '0',
              'GARGE' => '0',
              'KTCHN' => '0',
              'NONE' => '0',
              'ONESP' => '1',
              'PDECK' => '0',
              'PRKLT' => '0',
              'PRKPD' => '0',
              'SIDER' => '0',
              'STORE' => '0',
              'STRET' => '0',
              'TWOSP' => '2',
              'UNDER' => '0'
              }
            ite = its[item]
            ret << ite.to_i if (ite && ite.to_s.to_i >= 0)
          rescue
            ret = []
          end
          
        }

        rsl = 0
        if (ret && (ret.length > 0))
          ret.each { |rt|
            rsl = rsl + rt 
          }
          rsl.to_s
        else
          nil
        end
      end
      
    },
    heating_system: 'HeatType',
    interior_description: 'Interior',
    key_map_book: 'DeedBook',
    key_map_page: 'DeedPage',
    list_price: 'ListPrice',
    listing_agent_id: 'ListAgentMLSID',
    listing_agent_name: 'ListAgentFullName',
    listing_agent_phone: 'ListAgentDirectWorkPhone',
    listing_broker_id: 'ListOfficeMLSID',
    listing_broker_name: 'ListOfficeName',
    listing_date:  proc { |d|  
      if !(d['Status'] == "A")
        to_date_sql(d['ListingContractDate'])  
      else
        if !d['StatusChangeTimestamp'].blank?
          to_date_sql(d['StatusChangeTimestamp'])  
        else
          to_date_sql(Time.now)  
        end
      end
    },
    lot: 'Lot',
    lot_back_dimensions: 'LotDimensions',
    lot_description: 'LotDesc',
    lot_improvements: 'Improvements',
    lot_square_feet: proc { |d| (43560 * d['Acres'].to_d).to_i.to_s   },
    maintenance_fee: 'MonthlyAssocFee',
    mls_area_key: 'SearchArea',
    mls_area_name: 'SearchArea',
    mls_elementary_school: 'ElementarySchool',
    mls_high_school: 'HighSchool',
    mls_middle_school: 'MiddleSchool',
    mls_number: 'MLSNumber',
    mls_record_modified: proc { |d| to_date_sql(d['MatrixModifiedDT']) }, 
    multi_owner_operating: 'GrossExpensesAmt',
    muti_gross_income: 'GrossIncomeAmt',
    mutli_income_net_operation: 'NOIAmt',
    new_construction: proc { |d|
      cats = {
        'CN510' => 'N',
        'CON10' => 'N',
        'CON15' => 'N',
        'NEWUC' => 'Y',
        'RS60PLUS' => 'N',
        'RSALE' => 'N',
        'TBBLT' => 'Y'
      }
      cats[d['AgeDesc']]
    },
    number_of_units: 'NumofUnits',
    original_list_price: 'OriginalListPrice',
    owner_name: 'OwnerName',
    owner_phone: 'OwnerPhone',
    pending_date: proc { |d|  to_date_sql(d['PendingDate']) },
    number_of_photos: 'PhotoCount',
    # pool_private: 'PoolonProperty',
    pool_private: proc {|d|
      ret = []
      retu = nil
      Delayed::Worker.logger.debug ">>pool_private: }"
      unless d['PoolonProperty'].blank?
        d['PoolonProperty'].split(",").each { |item|
          begin
            Delayed::Worker.logger.debug ">>item: #{item}"
            ite = nil
            its = {
              'ABOVE' => 'Y',
              'FIBER' => 'Y',
              'GUNIT' => 'Y',
              'HEATD' => 'Y',
              'INGRN' => 'Y',
              'NONE' => 'N',
              'VINYL' => 'Y'
              }
            ite = its[item]
            ret << ite if ite
          rescue
            ret = []
          end
          
        }

        Delayed::Worker.logger.debug ">>ret item: #{ret}"
        rsl = 0
        if (ret && (ret.length > 0))
          rs = ret.reject do |x| x.in?(['N']) end
          if (rs && (rs.length > 0))
            retu = 'Y'
          else
            retu = 'N'
          end
          Delayed::Worker.logger.debug ">>retu item: #{retu}"
        else
          retu = nil
        end
      end
      retu
      
    },
    public_remarks: 'PublicRemarksConsumerView',
    realtor_remarks: 'PrivateRemarksMembersOnly',
    rental_class: 'PropertySubType',
    rental_terms: 'Terms',
    rental_type: 'PropertySubType',
    road_surface: 'RoadSurface',
    roof_type: 'RoofType',
    sold_price: 'ClosePrice',
    sold_lease_price: 'ClosePrice',
    # sale_price_psf: 'SPSqft',
    section_number: 'SectionGMD',
    # selling_agent_id: 'SellingAgentMLSID',
    selling_agent_name: 'SellingAgentFullName',
    selling_agent_phone: 'SellingAgentDirectWorkPhone',
    
    single_family_type: 'PropertySubType',
    square_feet: 'SqFtTotal',
    state: 'StateOrProvince',
    status: proc { |d|
      cats = {
        'A' => 'active',
        'CONDD' => 'pending',
        'CONKO' => 'pending',
        'CONTG' => 'pending',
        'I' => 'expired',
        'L' => 'sold',
        'P' => 'pending',
        'POA' => 'pending',
        'S' => 'sold',
        'TERM' => 'terminated',
        'W' => 'withdrawn',
        'X' => 'expired'
     }

     cats[d['Status']]
    },
    new_stories: 'Stories',
    # street_address: proc { |d|
      
    # },
    # 'nil',
    street_direction: 'StreetDirPrefix',
    street_name: proc { |d|
      [d['StreetName'],d['StreetSuffix']].compact.join(" ")
    },
    street_number: 'StreetNumber',
    street_surface_type: 'RoadType', ##
    new_siding_type: 'ConstructionDesc',
    new_style_of_building: 'Style', ##
    subdivision: 'SubdComplex',
    tax_amount: 'Taxes',
    tax_id: 'TaxID',
    tax_year: 'TaxYear',
    waterfront: 'WaterOnLand',
    withdrawn_date:  proc { |d|  to_date_sql(d['WithdrawnDate']) },
    year_built: 'YearBuilt',
    zip_4: 'PostalCodePlus4',
    zip_code: 'PostalCode'
  }
  
  

  
  def rets_login
    begin
      @client = Rets::Client.new(:login_url => "https://gamls-rets.paragonrels.com/rets/fnisrets.aspx/GAMLS/login?rets-version=rets/1.7.2",:username => market.mls_login,:password => market.mls_password)
      Delayed::Worker.logger.debug "RETS client #{@client}"
      Delayed::Worker.logger.debug "Alanta #1 RETS logged in"
      @client.login
      true
    rescue Exception => e
      false
    end
  end

  def rets_login2
    # @client = Rets::Client.new(:login_url => "http://fml.rets.interealty.com/Login.asmx/Login",:username => "MLKAY",:password => "gus@Pr4b", :agent => "RETS-Connector1.2_mlktech")
    @client = Rets::Client.new(:login_url => "http://fmlsrets.mlsmatrix.com/rets/login.ashx",:username => "MLKAY",:password => "gus@Pr4b", :agent => "RETS-Connector1.2_mlktech", :ua_password => "123456", :version => "RETS/1.5")
    #"http://fmlsrets.mlsmatrix.com/rets/GetMetadata?Type=METADATA-TABLE&ID=0&Format=COMPACT"
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "Alanta #2 RETS logged in"
    true
  end

  def rets_logout
    begin
      @client.logout
      Delayed::Worker.logger.debug "Paragon logged out"
      true
      
    rescue Exception => e
      Delayed::Worker.logger.debug "Paragon logged out error"
    end
  end
  
  def rets_logout2
    begin
      @client.logout
      Delayed::Worker.logger.debug "logged out"
      true
      
    rescue Exception => e
      Delayed::Worker.logger.debug "logged out error"
    end
  end

  def mls_url_for(property)
    "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
  end

  def grab(days)

    t =  !Rails.env.development? ? Time.now  : Time.now - 840.minutes
    
    Delayed::Worker.logger.debug "~~@~~~ loggin in ~~~~~~"
     
    begin
      if rets_login
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
        parse_rets(t)
        rets_logout
        
      else
        self.errors[:login] = "could not login MLS1"
      end
    rescue Exception => e
      Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    end
    
    begin 
      if rets_login2
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
        parse_rets2(t)
        rets_logout
      else 
        self.errors[:login] = "could not login MLS2"
      end    
    rescue Exception => e
      Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
    end
    
    begin
      notify_listeners
    rescue Exception => e
      Delayed::Worker.logger.debug "~~~~~~~ Error Notification ~~~~~~~~"
    end
    
  end

  def grab_by_interval(minutes=nil)
    # t = DateTime.strptime('2015-02-09T08:08:00-06:00', '%Y-%m-%dT%H:%M:%S%z')
    t =  !Rails.env.development? ? Time.now + 295.minutes  : Time.now - 840.minutes + 295.minutes
    t2 =  !Rails.env.development? ? Time.now + 63.minutes   : Time.now - 840.minutes
    Delayed::Worker.logger.debug "~~t #{t}"
    Delayed::Worker.logger.debug "~~@~~~ loggin in ~~~~~~"
     
    # begin
      # # if rets_login
        # Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
      # parse_rets_by_interval(t,minutes)
        # # rets_logout
#         
      # # else
        # # self.errors[:login] = "could not login MLS1"
      # # end
    # rescue Exception => e
       # Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    # end
    
    begin
      if rets_login2
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
        parse_rets2_by_interval(t2,minutes)
        rets_logout2
      else 
        self.errors[:login] = "could not login MLS2"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
    end
    
    begin
      notify_listeners
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
    end
  end
  
  
   def test_by_interval(minutes=nil)
    # t = DateTime.strptime('2015-02-09T08:08:00-06:00', '%Y-%m-%dT%H:%M:%S%z')
    t =  !Rails.env.development? ? Time.now + 365.minutes : Time.now - 840.minutes
    Delayed::Worker.logger.debug "~~t #{t}"
    Delayed::Worker.logger.debug "~~@~~~ loggin in ~~~~~~"
     
    begin
      if rets_login
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
        test_rets_by_interval(t,minutes)
        rets_logout
        
      else
        self.errors[:login] = "could not login MLS1"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    end
    
    # begin
      # if rets_login2
        # Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
        # parse_rets2_by_interval(t,minutes)
        # rets_logout2
      # else 
        # self.errors[:login] = "could not login MLS2"
      # end
    # rescue Exception => e
       # Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
    # end
    
    begin
      notify_listeners
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
    end
  end



  def grab_by_specified_period(start_time,end_time)

    begin
      if rets_login
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
        parse_rets_by_specified_period(start_time,end_time) 
        rets_logout
        
      else
        self.errors[:login] = "could not login MLS1"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    end
    
    begin
      if rets_login2
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
        parse_rets2_by_specified_period(start_time,end_time) 
        rets_logout2
      else 
        self.errors[:login] = "could not login MLS2"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
    end
    
    begin
      notify_listeners
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
    end
  end
  

  def correct_by_specified_period(start_time,end_time,idx)
=begin  
    begin
      if rets_login
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
        correct_rets_by_specified_period(start_time,end_time,idx) 
        rets_logout
        
      else
        self.errors[:login] = "could not login MLS1"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    end
=end    
    begin
      if rets_login2
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
        correct_rets2_by_specified_period(start_time,end_time,idx) 
        rets_logout2
        
      else
        self.errors[:login] = "could not login MLS1"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    end
    
    begin
      notify_listeners
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
    end
  end

  def sync_rets(days)
    t = DateTime.strptime('2015-02-09T08:08:00-06:00', '%Y-%m-%dT%H:%M:%S%z')
    # t =  !Rails.env.development? ? Time.now  : Time.now - 840.minutes
    
    Delayed::Worker.logger.debug "~~@~~~ loggin in ~~~~~~"
     
    begin
      if rets_login
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
        parse_rets_by_interval(t,minutes)
        rets_logout
        
      else
        self.errors[:login] = "could not login MLS1"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
    end
    
    begin
      if rets_login2
        Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
        parse_rets2_by_interval(t,minutes)
        rets_logout2
      else 
        self.errors[:login] = "could not login MLS2"
      end
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
    end
    
    begin
      notify_listeners
    rescue Exception => e
       Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
    end
  end
  
  def add_dom(days)
    
    super
    
    x = 0
    Delayed::Worker.logger.debug "#{market.name} specific update ##{market.id}"
    x = self.market.properties.active_and_pending.update_all ["cdom = ifnull(cdom,ifnull(dom,0))  + DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL ifnull(dom,0) DAY) - 1) )
                          , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          ["(listing_date is not null) and ((cdom is null) or (dom is null)) "]
                          
    Delayed::Worker.logger.debug "rows affected: #{x}"
    self.rows_affected = (self.rows_affected.nil? ? 0 : self.rows_affected ) + x
    
    notify_listeners
  end

  def self.to_date_sql(dt)
    #"2014-05-09T15:40:14"
    field = dt.to_s
    pattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil
    subpattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil
    ma =  field.match(pattern).captures.try(:[],0) rescue nil
    ta =  field.match(subpattern).captures.try(:[],0) rescue nil
    if field && field.match(/(\d+\/\d+\/\d+)/)
      strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil
    else
      strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil
    end
  rescue
    nil
  end

  protected

  private

    def parse_raw(data, filename)

      begin
        CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

          # row['PropType'] ||= filename.try(:slice, -7, 3)
          save_property(row, DATA_MAPPING, DATA_ENUMS)
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "error #{e.message}"
      end
    end

    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

    
    # def parse_rets()
      
    #     # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
    #     timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
    #     timerange = "2014-01-01-2014-06-11"
    #     sd = Date.parse('2014-11-12')
    #     ed = Date.parse('2014-11-12')
    #     Delayed::Worker.logger.debug "--------- pulling single family ---------"
    #     ed.downto(sd) { |date|
          
    #       Delayed::Worker.logger.debug "---------  #{date} ---------"
    #       date1 = date.strftime("%Y-%m-%dT00:00:00")
    #       date2 = date.strftime("%Y-%m-%dT03:00:00")
    #       date3 = date.strftime("%Y-%m-%dT06:00:00")
    #       date4 = date.strftime("%Y-%m-%dT09:00:00")
    #       date5 = date.strftime("%Y-%m-%dT12:00:00")
    #       date6 = date.strftime("%Y-%m-%dT15:00:00")
    #       date7 = date.strftime("%Y-%m-%dT18:00:00")
    #       date8 = date.strftime("%Y-%m-%dT21:00:00")
    #       tomorrow = date + 1.day
    #       date9 = tomorrow.strftime("%Y-%m-%dT00:00:00")
    #       dates = []
    #       dates = [date1,date2,date3,date4,date5,date6,date7,date8,date9]
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
    #       for i in 7..7
    #         Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
    #         begin  
             
    #           recdate = "#{dates[i]}-#{dates[i+1]}"
               
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling SFA ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=SFA)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing SFA ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #             end
    #           rescue  Exception => e
    #             Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling SFD ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=SFD)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing SFD ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
              
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling RR ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=RR)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing RR ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
              
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling LL ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=LL)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing LL ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
              
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling AF ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{recdate}),(TYP=AF)",:limit => 60000)
    #             Delayed::Worker.logger.debug "--------- importing AF ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
            
    #         rescue Exception => e
    #           Delayed::Worker.logger.debug "No record found #{e.message}"
    #         end
            
            
          
    #       end
          
         
    #     }

       

    # end

    # def parse_rets2()
      
    #     # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
    #     timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
    #     timerange = "2014-01-01-2014-06-11"
    #     sd = Date.parse('2014-11-12')
    #     ed = Date.parse('2014-11-12')
    #     Delayed::Worker.logger.debug "--------- pulling single family ---------"
    #     ed.downto(sd) { |date|
          
    #       Delayed::Worker.logger.debug "---------  #{date} ---------"
    #       date1 = date.strftime("%Y-%m-%dT00:00:00")
    #       date2 = date.strftime("%Y-%m-%dT03:00:00")
    #       date3 = date.strftime("%Y-%m-%dT06:00:00")
    #       date4 = date.strftime("%Y-%m-%dT09:00:00")
    #       date5 = date.strftime("%Y-%m-%dT12:00:00")
    #       date6 = date.strftime("%Y-%m-%dT15:00:00")
    #       date7 = date.strftime("%Y-%m-%dT18:00:00")
    #       date8 = date.strftime("%Y-%m-%dT21:00:00")
    #       tomorrow = date + 1.day
    #       date9 = tomorrow.strftime("%Y-%m-%dT00:00:00")
    #       dates = []
    #       dates = [date1,date2,date3,date4,date5,date6,date7,date8,date9]
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
    #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
    #       for i in 7..7
    #         Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
    #         begin  
             
    #           recdate = "#{dates[i]}-#{dates[i+1]}"
               
    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling RES ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=CLOSD),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing RES ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling CND ---------"
    #              results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=CLOSD),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing CND ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling LND ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=CLOSD),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing LND ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling LOT ---------"
    #             results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=CLOSD),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing LOT ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end

    #           begin
    #             Delayed::Worker.logger.debug "--------- pulling RNT ---------"
                
    #             # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5347139),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
    #             results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=CLOSD,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{recdate})",:limit => 50000)
    #             # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=CLOSD,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
    #             Delayed::Worker.logger.debug "--------- importing RNT ---------"
    #             results.each do |row|
    #               Delayed::Worker.logger.debug "##row: #{row}"
    #               save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
    #             end
    #           rescue  Exception => e
    #              Delayed::Worker.logger.debug "No record found #{e.message}"
    #           end
            
    #         rescue Exception => e
    #           Delayed::Worker.logger.debug "No record found #{e.message}"
    #         end
            
            
          
    #       end
          
         
    #     }

       

    # end
    
    
    # def pull_rets(property)

    #   begin
        
        

    #       begin
    #         Delayed::Worker.logger.debug "--------- pulling SFD ---------"
    #         results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STN=#{property.street_number}),(STR=#{property.street_direction}*),(ZIP=#{property.zip_code})",:limit => 60000)
            
            
    #         Delayed::Worker.logger.debug "--------- importing SFD ---------"
            
            
    #         if results && results.count > 0
    #           row = results.first 
    #           puts "##row: #{row}"
    #           result = push_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              
    #           selected_fields = result.select{|x| ![:street_number,:street_direction,:street_name].include?(x)}
              
    #           property.assign_attributes(selected_fields)
    #           # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
    #         end
            
    #       rescue  Exception => e
    #          Delayed::Worker.logger.debug "No record found #{e.message}"
    #       end

         

        

    #   rescue Exception => e
    #     Delayed::Worker.logger.debug "No record found #{e.message}"
    #   end

    # end

    # def parse_rets()
# 
      # begin
#           
        # begin
          # Delayed::Worker.logger.debug "--------- pulling AF ---------"
          # results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(LN=07346752),(TYP=AF)",:limit => 60000)
          # Delayed::Worker.logger.debug "--------- importing AF ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
          # end
        # rescue  Exception => e
           # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
#         
# 
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end


    def parse_rets(t=nil)

      begin
        
        divisor = 720
        offset = 500
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling SFA ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFA)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing SFA ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling SFD ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing SFD ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling RR ---------"
            results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=RR)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RR ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling LL ---------"
            results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=LL)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing LL ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling AF ---------"
            results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=AF)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing AF ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    def parse_rets2(t=nil)

      begin
        
        divisor = 720
        offset = 500
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling CND ---------"
            results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing CND ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling LND ---------"
            results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing LND ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling LOT ---------"
            results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing LOT ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling RNT ---------"
            
            # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5347139),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=CLOSD,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            Delayed::Worker.logger.debug "--------- importing RNT ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    def correct_rets_by_interval(t=nil,minutes=nil)

      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        Delayed::Worker.logger.debug "~~timenow #{timenow}"

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling SFA ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFA)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(LN=07575601)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing SFA ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              correct_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling SFD ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
             # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07578034),(TYP=SFD)",:limit => 60000)

             # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07577020),(TYP=SFD)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing SFD ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              correct_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling RR ---------"
            results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=RR)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RR ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              correct_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling LL ---------"
            results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=LL)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing LL ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              correct_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling AF ---------"
            results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=AF)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing AF ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              correct_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end


    def parse_rets_by_interval(t=nil,minutes=nil)

      begin
        
        divisor = 120
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now + 3.minutes  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        Delayed::Worker.logger.debug "~~timenow #{timenow}"

        step.downto(1) do |increment|
          
          
            
            
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:00") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:00") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:00") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"
  
          begin
            
            if rets_login
              begin
                Delayed::Worker.logger.debug "--------- pulling SFD ---------"
                # results = client.find(:all,:search_type => "Property",:class => "SD_1",:query => "(L_UpdateDate=#{date_range}),(L_StatusCatID=1,2,3,4,5,6)",:limit => 2500)
                results = client.find(:all,:search_type => "Property",:class => "SD_1",:query => "(L_DisplayId=5690971,8001248,5693012,5694665,5695485,5695382,5695515,8005034,8004765,8005326,5695701,5695714,5695966,5695674,5695991,5601254,7583785,5661319,5682138,7630674,5631393,5684576,5628526,7621952,5683941,5684361,5684226,5686042,7638290,7636794,5687646,7632929,7637019,7640352,5662096,7638392,7628411,5688701,5690822,5692085,5692011,5690813,5629477,5692568,5689097,8001628,8002948,8000456,7642217)",:limit => 2500)
                Delayed::Worker.logger.debug "--------- importing SFD ---------"
                results.each do |row|
                  Delayed::Worker.logger.debug "##row: #{row}"
                  save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
                end
              rescue  Exception => e
                 Delayed::Worker.logger.debug "No record found #{e.message}"
              end
            else
              Delayed::Worker.logger.debug "--------- Login Error ---------"
            end
            
          rescue Exception => e
            Delayed::Worker.logger.debug "Error #{e.message}"
          ensure
            rets_logout
          end
          
  
          begin
            
            if rets_login
              begin
                Delayed::Worker.logger.debug "--------- pulling RR ---------"
                results = client.find(:all,:search_type => "Property",:class => "RR_7",:query => "(L_UpdateDate=#{date_range}),(L_StatusCatID=1,2,3,4,5,6)",:limit => 2500)
                Delayed::Worker.logger.debug "--------- importing RR ---------"
                results.each do |row|
                  Delayed::Worker.logger.debug "##row: #{row}"
                  save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
                end
              rescue  Exception => e
                 Delayed::Worker.logger.debug "No record found #{e.message}"
              end
            else
              Delayed::Worker.logger.debug "--------- Login Error ---------"
            end
            
          rescue Exception => e
            Delayed::Worker.logger.debug "Error #{e.message}"
          ensure
            rets_logout
          end
              
            
          

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
    def test_rets_by_interval(t=nil,minutes=nil)

      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now + 365.minutes  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        puts "~~timenow #{timenow}"

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          puts "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          # begin
            # Delayed::Worker.logger.debug "--------- pulling SFA ---------"
            # results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFA)",:limit => 60000)
            # # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(LN=07575601)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing SFA ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end

          begin
            puts "--------- pulling SFD ---------"
            results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
             # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=5649986),(TYP=SFD)",:limit => 60000)

             # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07577020),(TYP=SFD)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
            puts "--------- importing SFD ---------"
            results.each do |row|
              puts  "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             puts "No record found #{e.message}"
          end
          
          
          
          

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        puts "No record found #{e.message}"
      end

    end

    def parse_rets_by_specified_period(start_time,end_time)

      begin
        
        divisor = 720
        end_time = Time.parse(end_time)
        start_time = Time.parse(start_time)
        Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
      
        offset = (end_time - start_time).to_i / 60
        time_zone_diff = 840
        
        status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
        active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD,S,RL'
        # active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD'
        # active_list = 'S,RL'
                
        timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
        new_offset = offset
        
        Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

           begin
            Delayed::Worker.logger.debug "--------- pulling SFD ---------"
            results = client.find(:all,:search_type => "Property",:class => "SD_1",:query => "(L_UpdateDate=#{date_range}),(L_StatusCatID=1,2,3,4,5,6)",:limit => 60000)
          
            Delayed::Worker.logger.debug "--------- importing SFD ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end



 
          begin
            Delayed::Worker.logger.debug "--------- pulling RR ---------"
            results = client.find(:all,:search_type => "Property",:class => "RR_7",:query => "(L_UpdateDate=#{date_range}),(L_StatusCatID=1,2,3,4,5,6)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RR ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    
    
    def parse_rets2_by_interval(t=nil,minutes=nil)

      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5653885,5654050,5654059),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT)",:limit => 50000)
            #,(MLSNumber=5320614)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
             results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5690971,8001248,5693012,5694665,5695485,5695382,5695515,8005034,8004765,8005326,5695701,5695714,5695966,5695674,5695991,5601254,7583785,5661319,5682138,7630674,5631393,5684576,5628526,7621952,5683941,5684361,5684226,5686042,7638290,7636794,5687646,7632929,7637019,7640352,5662096,7638392,7628411,5688701,5690822,5692085,5692011,5690813,5629477,5692568,5689097,8001628,8002948,8000456,7642217)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MLSNumber=5659635)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,CONDD,CONKO,CONTG,P,POA),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            
            
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling INCOMP ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=I,TERM,W,X),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            
            
            Delayed::Worker.logger.debug "--------- importing INCOMP ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling SOLD ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=S,L),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,CONDD,CONKO,CONTG,P,POA,I,TERM,W,X,S,L),(MLSNumber=5544597)",:limit => 50000)


            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            
            
            Delayed::Worker.logger.debug "--------- importing SOLD ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          
          

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    def parse_rets2_by_specified_period(start_time,end_time)

      begin
        
        divisor = 720
        end_time = Time.parse(end_time)
        start_time = Time.parse(start_time)
        Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
      
        offset = (end_time - start_time).to_i / 60
        time_zone_diff = 840
        
        status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
                
        timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
        new_offset = offset
        
        Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
        # [1].each do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          
          
          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5636714),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT)",:limit => 50000)
            #,(MLSNumber=5320614)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MLSNumber=5639089)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,CONDD,CONKO,CONTG,P,POA),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5690971,8001248,5693012,5694665,5695485,5695382,5695515,8005034,8004765,8005326,5695701,5695714,5695966,5695674,5695991,5601254,7583785,5661319,5682138,7630674,5631393,5684576,5628526,7621952,5683941,5684361,5684226,5686042,7638290,7636794,5687646,7632929,7637019,7640352,5662096,7638392,7628411,5688701,5690822,5692085,5692011,5690813,5629477,5692568,5689097,8001628,8002948,8000456,7642217)",:limit => 50000)
            
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5649986,5648576),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            
            
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          begin
            Delayed::Worker.logger.debug "--------- pulling INCOMP ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=I,TERM,W,X),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=I,TERM,W,X),(PropertyType=DET,RNT),(MLSNumber=3981584)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            
            
            Delayed::Worker.logger.debug "--------- importing INCOMP ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          

          begin
            Delayed::Worker.logger.debug "--------- pulling SOLD ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=S,L),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            
            
            Delayed::Worker.logger.debug "--------- importing SOLD ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
# 
          new_offset = new_offset - divisor
# 
        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    def parse_rets3_by_interval(t=nil,minutes=nil)
      
      export_file = 'grawd.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "raw"
      
      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(Status=A,CONDD,CONKO,CONTG,P,POA,I,TERM,W,X,S,L)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5649986,5648576),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5637145,5637140),(Status=A,CONDD,CONKO,CONTG,P,POA,I,TERM,W,X,S,L)",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            res =   client.http_post(client.capability_url("GetMetadata"),
              { "Format" => "STANDARD-XML",
                "Type"   => "METADATA-TABLE",
              "ID"     => "0" })
            str_metadata = res.body.squish
            
            doc = Nokogiri::XML(str_metadata)
            
            Delayed::Worker.logger.debug "##str_metadata: #{str_metadata}"

            res2 =  client.http_post(client.capability_url("GetMetadata"),
            { "Format" => "STANDARD-XML",
              "Type"   => "METADATA-LOOKUP_TYPE",
              "ID"     => "0" })
            str_metadata2 = res2.body.squish

            doc2 = Nokogiri::XML(str_metadata2)
            
            Delayed::Worker.logger.debug "##str_metadata2: #{str_metadata2}"
            
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            
            
            ix = 0
            i = 0  
            results.each do |row|
              puts "MLSNumber: #{row["MLSNumber"]}"
              group_name = "Record ##{i+1}, MLSNumber: #{row["MLSNumber"]}"
              sheet1.row(ix).push group_name
              ix = ix + 1
              
              row.each do |x,y|
                
                system_name = x.to_s
                long_name = ""
                lookup_name =  ""
                lookup_value = ""
                actual_value = ""
                composed_lu_final_result = ""
                    
                things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Resource='Property' and @Class='Listing']/Field/SystemName[text()='#{x}']")
                if (things && (things.length > 0))
                  long_name = things[0].parent.at("./LongName").text
                  lookup_name = things[0].parent.at("./LookupName").text
                  actual_value = y
                  lookup_value = y

                  # GFLU20130226165731591186000000
                  things2 = doc2.xpath("//RETS/METADATA/METADATA-LOOKUP_TYPE[@Lookup='#{lookup_name}']/Lookup")
                  if (things2 && (things2.length > 0))
                    multi_items = lookup_value.split(",")
                    composed_lu_val = multi_items.inject([]) do |memo,e|
                      lu_lv = things2.at("./Value[text()='#{e}']").parent.at("./LongValue")
                      memo << lu_lv
                      memo
                    end
                    composed_lu_final_result = composed_lu_val.join(",")
                  else
                    composed_lu_final_result = ""

                  end

                  

                end

                puts ">>>>system_name: #{system_name}^ long_name: #{long_name}^ value: #{actual_value}^ lookup_name: #{lookup_name}^ composed_lu_final_result: #{composed_lu_final_result}"
                sheet1.row(ix).push system_name.dup.blank? ? "null" : system_name.dup 
                sheet1.row(ix).push long_name.dup.blank? ? "null" : long_name.dup 
                sheet1.row(ix).push actual_value.dup.blank? ? "null" : actual_value.dup 
                sheet1.row(ix).push lookup_name.dup.blank? ? "null" : lookup_name.dup 
                sheet1.row(ix).push composed_lu_final_result.dup.blank? ? "null" : composed_lu_final_result.dup 
                ix = ix + 1
                

              end
              i += 1
              Delayed::Worker.logger.debug "##row: #{row}"
              # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          # begin
            # Delayed::Worker.logger.debug "--------- pulling CND ---------"
            # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # Delayed::Worker.logger.debug "--------- importing CND ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # begin
            # Delayed::Worker.logger.debug "--------- pulling LND ---------"
            # results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # Delayed::Worker.logger.debug "--------- importing LND ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # begin
            # Delayed::Worker.logger.debug "--------- pulling LOT ---------"
            # results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # Delayed::Worker.logger.debug "--------- importing LOT ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # begin
            # Delayed::Worker.logger.debug "--------- pulling RNT ---------"
#             
            # # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5347139),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(STATUS=CLOSD,LEASE),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # Delayed::Worker.logger.debug "--------- importing RNT ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
          

          new_offset = new_offset - divisor

        end
        
        book.write export_file 

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
    def parse_rets4_by_interval(t=nil,minutes=nil)
      
      export_file = 'graw.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "raw"
      
      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        timenow = t if !t.blank?
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,S,L),(PropertyType=DET),(MatrixModifiedDT=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
            # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
            
            res =   client.http_post(client.capability_url("GetMetadata"),
              { "Format" => "STANDARD-XML",
                "Type"   => "METADATA-TABLE",
              "ID"     => "0" })
            str_metadata = res.body.squish
            
            doc = Nokogiri::XML(str_metadata)
            
            # Delayed::Worker.logger.debug "##str_metadata: #{str_metadata}"

            # res2 =  client.http_post(client.capability_url("GetMetadata"),
            # { "Format" => "STANDARD-XML",
            #   "Type"   => "METADATA-LOOKUP_TYPE",
            #   "ID"     => "0" })
            # str_metadata2 = res2.body.squish

            # doc2 = Nokogiri::XML(str_metadata2)
            
            # Delayed::Worker.logger.debug "##str_metadata2: #{str_metadata2}"
            
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            

            list = ['Complex/Building Access','Acres','Annual Master Assoc. Fee','Basement Desc','Total Full Baths','Total Half Baths','Total Bedrooms','Block','Main Dwelling Stories','Property Category','Total Days on Market','City','Closing Date','Cooling Desc','County','Days On Market','Appliance Desc','Expiration Date','Exterior','Fireplaces #','Floor Description','Parking Desc','# Parking Spaces','Heat Type','Interior','Deed Book','Deed Page','List Price','Listing Agent ID','Listing Agent Name','List Agent Direct Work Phone','Listing Office Code','Listing Office Name','List Date','Lot','Lot Dimensions','Lot Desc','Improvements','Monthly Assoc Fee','Search Area','Search Area','Elementary School','High School','Middle School','FMLS#','Gross Expenses $','Gross Income $','N.O.I. $','Age Desc','# of Units','Original List Price','Owner Name','Owner Phone','Binding Agreement Date','Photo Count','Pool on Property?','Public Remarks-Consumer View','Private Remarks - Members Only','Type','Terms','Type','Road Surface','Roof Type','Sales Price','SP/Sqft','Section/GMD','Selling Agent ID','Selling Agent Full Name','Selling Agent Direct Work Phone','Construction Desc','Type','Square Footage','State','Status','Stories','concatenate (Street Number Disp + Street Name + Street Type)','Street Direction Prefix','concatenate (Street Name + Street Type)','Street Number Disp','Road Type','Style','Subd/Complex','Taxes','Tax ID','Tax Year','Water On Land','Withdrawn Date','Year Built','ZipPlus4','Zip Code']
            ix = 0
            i = 0  
            list.each do |li|
              things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Resource='Property' and @Class='Listing']/Field/LongName[text()='#{li}']")
              if (things && (things.length > 0))
                Delayed::Worker.logger.debug "#{things[0].text}:#{things[0].parent.at("./SystemName").text}"
              else
                Delayed::Worker.logger.debug "#{li}:nil"
              end
            end
            #----------------------------------------------------------------------------
            # ix = 0
            # i = 0  
            # results.each do |row|
            #   puts "MLSNumber: #{row["MLSNumber"]}"
            #   group_name = "Record ##{i+1}, MLSNumber: #{row["MLSNumber"]}"
            #   sheet1.row(ix).push group_name
            #   ix = ix + 1
              
            #   row.each do |x,y|
                
            #     system_name = x.to_s
            #     long_name = ""
            #     lookup_name =  ""
            #     lookup_value = ""
            #     actual_value = ""
            #     composed_lu_final_result = ""
                    
            #     things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Resource='Property' and @Class='Listing']/Field/SystemName[text()='#{x}']")
            #     if (things && (things.length > 0))
            #       long_name = things[0].parent.at("./LongName").text
            #       lookup_name = things[0].parent.at("./LookupName").text
            #       actual_value = y
            #       lookup_value = y

            #       # GFLU20130226165731591186000000
            #       things2 = doc2.xpath("//RETS/METADATA/METADATA-LOOKUP_TYPE[@Lookup='#{lookup_name}']/Lookup")
            #       if (things2 && (things2.length > 0))
            #         multi_items = lookup_value.split(",")
            #         composed_lu_val = multi_items.inject([]) do |memo,e|
            #           lu_lv = things2.at("./Value[text()='#{e}']").parent.at("./LongValue")
            #           memo << lu_lv
            #           memo
            #         end
            #         composed_lu_final_result = composed_lu_val.join(",")
            #       else
            #         composed_lu_final_result = ""

            #       end

                  

            #     end

            #     puts ">>>>system_name: #{system_name}^ long_name: #{long_name}^ value: #{actual_value}^ lookup_name: #{lookup_name}^ composed_lu_final_result: #{composed_lu_final_result}"
            #     sheet1.row(ix).push system_name.dup.blank? ? "null" : system_name.dup 
            #     sheet1.row(ix).push long_name.dup.blank? ? "null" : long_name.dup 
            #     sheet1.row(ix).push actual_value.dup.blank? ? "null" : actual_value.dup 
            #     sheet1.row(ix).push lookup_name.dup.blank? ? "null" : lookup_name.dup 
            #     sheet1.row(ix).push composed_lu_final_result.dup.blank? ? "null" : composed_lu_final_result.dup 
            #     ix = ix + 1
                

            #   end
            #   i += 1
            #   Delayed::Worker.logger.debug "##row: #{row}"
            #   # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
            # end

            #----------------------------------------------------------------------------
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          new_offset = new_offset - divisor

        end
        
        book.write export_file 

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
    def correct_rets_by_specified_period(start_time,end_time,idx)

      begin
        
        divisor = 720
        end_time = Time.parse(end_time)
        start_time = Time.parse(start_time)
        Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
      
        offset = (end_time - start_time).to_i / 60
        time_zone_diff = 840
        
        status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
        # active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD,S,RL'
        active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD'
        sold_list = 'S,RL'
                
        timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
        new_offset = offset
        
        Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"
          
          case idx
          when 1
            begin
              Delayed::Worker.logger.debug "--------- pulling SFA ---------"
              results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=#{active_list}),(RECMODDATE=#{date_range}),(TYP=SFA)",:limit => 60000)
              # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(LN=07575601)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing SFA ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
  
            begin
              Delayed::Worker.logger.debug "--------- pulling SFD ---------"
              results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=#{active_list}),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
               # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07578034),(TYP=SFD)",:limit => 60000)
  
               # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07577020),(TYP=SFD)",:limit => 60000)
              # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing SFD ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            begin
              Delayed::Worker.logger.debug "--------- pulling RR ---------"
              results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=#{active_list}),(RECMODDATE=#{date_range}),(TYP=RR)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing RR ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            begin
              Delayed::Worker.logger.debug "--------- pulling LL ---------"
              results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=#{active_list}),(RECMODDATE=#{date_range}),(TYP=LL)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing LL ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            begin
              Delayed::Worker.logger.debug "--------- pulling AF ---------"
              results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=#{active_list}),(RECMODDATE=#{date_range}),(TYP=AF)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing AF ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
  
            
          when 2
            begin
              Delayed::Worker.logger.debug "--------- pulling SFA ---------"
              results = client.find(:all,:search_type => "Property",:class => "SFA",:query => "(STAT=#{sold_list}),(RECMODDATE=#{date_range}),(TYP=SFA)",:limit => 60000)
              # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(LN=07575601)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing SFA ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
  
            begin
              Delayed::Worker.logger.debug "--------- pulling SFD ---------"
              results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=#{sold_list}),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
               # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07578034),(TYP=SFD)",:limit => 60000)
  
               # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07577020),(TYP=SFD)",:limit => 60000)
              # results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(STAT=A),(RECMODDATE=#{date_range}),(TYP=SFD)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing SFD ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            begin
              Delayed::Worker.logger.debug "--------- pulling RR ---------"
              results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=#{sold_list}),(RECMODDATE=#{date_range}),(TYP=RR)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing RR ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            begin
              Delayed::Worker.logger.debug "--------- pulling LL ---------"
              results = client.find(:all,:search_type => "Property",:class => "LL",:query => "(STAT=#{sold_list}),(RECMODDATE=#{date_range}),(TYP=LL)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing LL ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            begin
              Delayed::Worker.logger.debug "--------- pulling AF ---------"
              results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=#{sold_list}),(RECMODDATE=#{date_range}),(TYP=AF)",:limit => 60000)
              Delayed::Worker.logger.debug "--------- importing AF ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets5(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end
  
            
          end
          
          new_offset = new_offset - divisor
        end
        
      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    def correct_rets2_by_specified_period(start_time,end_time,idx)

      begin
        
        divisor = 720
        end_time = Time.parse(end_time)
        start_time = Time.parse(start_time)
        Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
      
        offset = (end_time - start_time).to_i / 60
        time_zone_diff = 840
        
        status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
                
        timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
        new_offset = offset
        
        Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
        # [1].each do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          Delayed::Worker.logger.debug "idx: #{idx}"
          Delayed::Worker.logger.debug "idx class: #{idx.class}"
          date_range = "#{start_time}-#{end_time}"

          case idx 
          
          when 1 
            begin
              Delayed::Worker.logger.debug "--------- pulling RES ---------"
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
              
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5636714),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT)",:limit => 50000)
              #,(MLSNumber=5320614)
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT),(MLSNumber=5639089)",:limit => 50000)
              results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,CONDD,CONKO,CONTG,P,POA),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=5649986,5648576),(PropertyType=ATT,COM,DET,LND,LOT,MUL,RNT)",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
              
              
              
              Delayed::Worker.logger.debug "--------- importing RES ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets4(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end

            begin
              Delayed::Worker.logger.debug "--------- pulling INCOMP ---------"
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
              results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=I,TERM,W,X),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=I,TERM,W,X),(PropertyType=DET,RNT),(MLSNumber=3981584)",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
              
              
              
              Delayed::Worker.logger.debug "--------- importing INCOMP ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets4(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end

          when 2  
          

            begin
              Delayed::Worker.logger.debug "--------- pulling SOLD ---------"
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
              results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=S,L),(PropertyType=DET,RNT),(MatrixModifiedDT=#{date_range})",:limit => 50000)
              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7343266),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
              
              
              
              Delayed::Worker.logger.debug "--------- importing SOLD ---------"
              results.each do |row|
                Delayed::Worker.logger.debug "##row: #{row}"
                save_rets4(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
              end
            rescue  Exception => e
               Delayed::Worker.logger.debug "No record found #{e.message}"
            end

          end

          new_offset = new_offset - divisor
# 
        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
#     def parse_rets()

#       begin
        
       
#           begin
#             Delayed::Worker.logger.debug "--------- pulling SFD ---------"
#             results = client.find(:all,:search_type => "Property",:class => "SFD",:query => "(LN=07353317,07353359),(STAT=A,B,CK,X,LP,N,PA,I,R,RL,RU,S,U,WD),(TYP=SFD)",:limit => 60000)
#             Delayed::Worker.logger.debug "--------- importing SFD ---------"
#             results.each do |row|
#               Delayed::Worker.logger.debug "##row: #{row}"
#               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
#             end
#           rescue  Exception => e
#              Delayed::Worker.logger.debug "No record found #{e.message}"
#           end

#           # begin
#             # Delayed::Worker.logger.debug "--------- pulling CND ---------"
#              # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(MLNUMBER=7195336,7197394),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
#             # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
#             # Delayed::Worker.logger.debug "--------- importing CND ---------"
#             # results.each do |row|
#               # Delayed::Worker.logger.debug "##row: #{row}"
# #               
#               # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
#             # end
#           # rescue  Exception => e
#              # Delayed::Worker.logger.debug "No record found #{e.message}"
#           # end
          
#           # begin
#             # Delayed::Worker.logger.debug "--------- pulling CND ---------"
#              # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
#             # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
#             # Delayed::Worker.logger.debug "--------- importing CND ---------"
#             # results.each do |row|
#               # Delayed::Worker.logger.debug "##row: #{row}"
# #               
#               # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
#             # end
#           # rescue  Exception => e
#              # Delayed::Worker.logger.debug "No record found #{e.message}"
#           # end
# #           
#           # begin
#             # Delayed::Worker.logger.debug "--------- pulling CND ---------"
#              # results = client.find(:all,:search_type => "Property",:class => "LND",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
#             # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
#             # Delayed::Worker.logger.debug "--------- importing CND ---------"
#             # results.each do |row|
#               # Delayed::Worker.logger.debug "##row: #{row}"
# #               
#               # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
#             # end
#           # rescue  Exception => e
#              # Delayed::Worker.logger.debug "No record found #{e.message}"
#           # end
# #           
#           # begin
#             # Delayed::Worker.logger.debug "--------- pulling CND ---------"
#              # results = client.find(:all,:search_type => "Property",:class => "LOT",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
#             # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
#             # Delayed::Worker.logger.debug "--------- importing CND ---------"
#             # results.each do |row|
#               # Delayed::Worker.logger.debug "##row: #{row}"
# #               
#               # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
#             # end
#           # rescue  Exception => e
#              # Delayed::Worker.logger.debug "No record found #{e.message}"
#           # end
# #           
#           # begin
#             # Delayed::Worker.logger.debug "--------- pulling CND ---------"
#              # results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(MLNUMBER=5345760,5345687,5345806,5345504,5357039,5342567,5349955,5354535,5344753,5344707,5344650,5344566,5343587,5344151,5344125,5351426,5343993,5343807,5343439,5341327,5344711,5342488,5352440,5250778,5097450,5341902,5356711,5341735,5353947,5340803,5339838,5340682,5340646,5340400,5354216,5175995,5340022,5339444,5339887,5338984,5356253,5248368,5338784,5338043,5338439,5338102,5337198,5336681,5356156,5336410,5347487,5332110,5335675,5335474,5335651,5335310,5335501,5333963,5334897,5334328,5334117,5333697,5333006,5332992,5332968,5332920,5332889,5332201,5331959,5352645,5331786,5357012,5357174,5330563,5331043,5330859,5328352,5330313,5330167,5328066,5329840,5329345,5329463,5328968,5355991,5352093,5327885,5347139,5262985,5326678,5325176,5325714,5324088,5323258,5354197,5321498,5321050,5321866,5320625,5320978,5319724,5320292,5320207,5320192,5319996,5319790,5319665,5319635,5356815,5319256,5355314,5318331,5317699,5317556,5268234,5283967,5357169,5348516,5316786,5315479,5352252,5315431,5314768,5314419,5314190,5351443,5319953,5313656,5313691,5313643,5313592,5310071,5357073,5345794,5310472,5296021,5309374,5307816,5307811,5304163,5352313,5355745,5310226,5357159,5312358,5312618,5326175,5356387,5357119,5307600,5333235,5305715,5307021,5305571,5351507,5252604,5306156,5305880,5356429,5306722,5348306,5317484,5319904,5272683,5311799,5351301,5354104,5276183,5282638,5356280,5335255,5234426,5323834,5352338,5239794,5265303,5350620,5356332,5357144,5262510,5356462,5231249,5356296,5357154,5350427,5235609,5357037,5297112,5278033,5278024,5357052,5357113,5224109,5354497,5339706,5309673,5301072,5275317,5283635,5136061,5195173,5267699,5351213,5250545,5357068,5357076,5287059,5283637,5263743,5297064,5345795,5356264,5355586,5351765,5269502,5292554,5346278,5284463,5254651,5277980,4220359,5293988,5278292,5252094,5354411,5212294,5294816,5293822,5287153,5356589,5348244,5230658,5338883,5356588,5354559,5354572,5351377,5278194,5331490,5331118,5302743),(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM,LEASE),(COUNTYID=#{COUNTYLIST})",:limit => 50000)
#             # # results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(STATUS=ACT,CONDD,CONKO,CONTG,PEND,CLOSD,POA,EXP,WITH,TERM),(COUNTYID=#{COUNTYLIST}),(LASTTRANSDATE=#{date_range})",:limit => 50000)
#             # Delayed::Worker.logger.debug "--------- importing CND ---------"
#             # results.each do |row|
#               # Delayed::Worker.logger.debug "##row: #{row}"
# #               
#               # save_rets2(row, DATA_MAPPING2, DATA_ENUMS2, LOOKUP_TYPE_ENUMS2)
#             # end
#           # rescue  Exception => e
#              # Delayed::Worker.logger.debug "No record found #{e.message}"
#           # end

          
          

          

#       rescue Exception => e
#         Delayed::Worker.logger.debug "No record found #{e.message}"
#       end

#     end

    def parse_sale_price()

      begin
        
        
        # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
        # timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
        # timerange = "2014-01-01-2014-06-11"
        sd = Date.parse('2013-09-12')
        ed = Date.parse('2013-12-31')
        Delayed::Worker.logger.debug "--------- pulling single family ---------"
        ed.downto(sd) { |date|
          
          Delayed::Worker.logger.debug "---------  #{date} ---------"
          date1 = date.strftime("%Y-%m-%dT00:00:00")
          date2 = date.strftime("%Y-%m-%dT06:00:00")
          date3 = date.strftime("%Y-%m-%dT12:00:00")
          date4 = date.strftime("%Y-%m-%dT18:00:00")
          tomorrow = date + 1.day
          date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
          dates = []
          dates = [date1,date2,date3,date4,date5]
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
          for i in 0..3
            Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
            begin  
             
               recdate = "#{dates[i]}-#{dates[i+1]}"
               Delayed::Worker.logger.debug "--------- pulling SFD ---------"
                results = client.find(:all,:search_type => "Property",:class => "RR",:query => "(STAT=S,RL),(RECMODDATE=#{recdate}),(TYP=RR)",:limit => 60000)
                Delayed::Worker.logger.debug "--------- importing SFD  ---------"
                results.each do |row|
                  Delayed::Worker.logger.debug "##row: #{row}"
                  correct_sale_price(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
                end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end
          
          end
        }

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
        
    def parse_special_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= 'res'
        correct_property(row, DATA_MAPPING2, DATA_ENUMS2)
      end
    end
    
    def correct_property(original, mapping=nil, enums={}, update_only=false)
      # exectute mappings and retrieve raw data
      
      if mapping
       
        original = original.to_hash.with_case_insensitive_access
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "Attribute #{attr}"  #$$
          result[attr] =
            
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.to_s.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
           
            elsif (string.is_a?(BigDecimal))
              Delayed::Worker.logger.debug "Big Decimal"  #$$
              string
              
              
            else
              if attr == :listing_date
                elems = string.split("/")
                mme = elems[0].to_s.rjust(2, '0')
                dde = elems[1].to_s.rjust(2, '0')
                if elems[2].to_s.include?("14")
                  yye = "2014"
                elsif elems[2].to_s.include?("13")
                  yye = "2013"
                elsif elems[2].to_s.include?("12")
                  yye = "2012"
                elsif elems[2].to_s.include?("11")
                  yye = "2011"
                elsif elems[2].to_s.include?("10")
                  yye = "2010"
                elsif elems[2].to_s.include?("09")
                  yye = "2009"
                elsif elems[2].to_s.include?("08")
                  yye = "2008"
                elsif elems[2].to_s.include?("07")
                  yye = "2007"          
                end
                # yye = elems[2].length == 2 ? "20#{elems[2]}" : elems[2]
                dt_string = "#{mme}/#{dde}/#{yye}"
                Property.parse(attr, dt_string)
              elsif attr == :sub_category
                self.timestamp
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
              
            end
          
          
            # if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              # nil 
            # elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # elsif (string.is_a?(BigDecimal))
              # string
            # else
              # if attr == :listing_date
                # elems = string.split("/")
                # mme = elems[0].to_s.rjust(2, '0')
                # dde = elems[1].to_s.rjust(2, '0')
                # yye = elems[2].length == 2 ? "20#{elems[2]}" : elems[2]
                # dt_string = "#{mme}/#{dde}/#{yye}"
                # Property.parse(attr, dt_string)
              # else
                # Property.parse(attr, string)
              # end
            # end
          result
        end
      else
        raw = {}
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      end
     
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      property = market.import7(self.source, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      
      # self.errors[current_filename] ||= {}
      # if property.nil? || property.is_a?(String) 
        # self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
        # self.results[current_filename] ||= 0
        # self.results[current_filename] += 1
        # self.ids << property.id
        # unless property.errors.empty?
          # self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        # end
      # end
    end
    
    def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          Delayed::Worker.logger.debug "~~self.job_id: #{job_id}"
          result[attr] =

            
            
            
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
             
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp              
              elsif attr == :tax_id
                # string
                # string.gsub(/[^\d]+/, '')
                string.gsub(/\s/, '').gsub(/-/,'')
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
              
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'gamls' } 
      
      property = market.import6(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end

    def correct_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          Delayed::Worker.logger.debug "~~self.job_id: #{job_id}"
          result[attr] =

            
            
            
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
             
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'gamls' } 
      
      property = market.import7(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      # self.errors[current_filename] ||= {}
      # Delayed::Worker.logger.debug "~~ before insert id ~~"  
      # if property.nil? || property.is_a?(String) 
      #   self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
      #   self.results[current_filename] ||= 0
      #   self.results[current_filename] += 1
      #   Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
      #   self.ids << property.id
      #   unless property.errors.empty?
      #     self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
      #   end
      # end
    end


    def save_rets2(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              # Delayed::Worker.logger.debug "$$ raw #{raw}"
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              # case attr
              #   when :single_family_type
              #     Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              # end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp
              elsif attr == :tax_id
                # string
                # string.gsub(/[^\d]+/, '')
                string.gsub(/\s/, '').gsub(/-/,'')
              else
                Delayed::Worker.logger.debug "Attribute property xxx parse #{attr}"
                
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'fmls' } 
      property = market.import4(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end

    def save_rets3(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              # Delayed::Worker.logger.debug "$$ raw #{raw}"
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              # case attr
              #   when :single_family_type
              #     Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              # end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp
              elsif attr == :tax_id
                string
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'fmls' } 
      property = market.import7(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end

      # self.errors[current_filename] ||= {}
      # Delayed::Worker.logger.debug "~~ before insert id ~~"  
      # if property.nil? || property.is_a?(String) 
      #   self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
      #   self.results[current_filename] ||= 0
      #   self.results[current_filename] += 1
      #   Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
      #   self.ids << property.id
      #   unless property.errors.empty?
      #     self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
      #   end
      # end
    end
    
    def save_rets4(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              # Delayed::Worker.logger.debug "$$ raw #{raw}"
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              # case attr
              #   when :single_family_type
              #     Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              # end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp
              elsif attr == :tax_id
                # string
                # string.gsub(/[^\d]+/, '')
                string.gsub(/\s/, '').gsub(/-/,'')
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'fmls' } 
      property = market.import7(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      # self.errors[current_filename] ||= {}
      # Delayed::Worker.logger.debug "~~ before insert id ~~"  
      # if property.nil? || property.is_a?(String) 
        # self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
        # self.results[current_filename] ||= 0
        # self.results[current_filename] += 1
        # Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        # self.ids << property.id
        # unless property.errors.empty?
          # self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        # end
      # end
    end
    
    def save_rets5(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          Delayed::Worker.logger.debug "~~self.job_id: #{job_id}"
          result[attr] =

            
            
            
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
             
              raw[attr] = raw[attr].try(:gsub,/"/,'')
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
              if attr == :sub_category
                self.timestamp              
              elsif attr == :tax_id
                # string
                # string.gsub(/[^\d]+/, '')
                string.gsub(/\s/, '').gsub(/-/,'')
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
              
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      
      source_hash = { :source => self.source, :mls_source => 'gamls' } 
      
      property = market.import9(source_hash, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      # self.errors[current_filename] ||= {}
      # Delayed::Worker.logger.debug "~~ before insert id ~~"  
      # if property.nil? || property.is_a?(String) 
        # self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
        # self.results[current_filename] ||= 0
        # self.results[current_filename] += 1
        # Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        # self.ids << property.id
        # unless property.errors.empty?
          # self.errors[current_filename][info] = property.errors.map { |attr, message| Delayed::Worker.logger.debug "attrib: #{attr} message: #{message}"; "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        # end
      # end
    end
    
    def push_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
               if attr == :sub_category
                nil
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      
      end
      
    end
    
end
