class Zillow

  attr_accessor :agent, :service
  
  
  
  
  PROX_1 = '23.95.193.93'
  PROX_2 = '192.230.49.225'
  PROX_3 = '23.94.87.170'
  PROX_4 = '23.95.193.32'
  PROX_5 = '192.69.254.216'
  PROX_6 = '23.94.87.197'
    
  def initialize()
       
    @agent = Mechanize.new { |ag|
      ag.read_timeout = 20
      ag.open_timeout = 20
      ag.idle_timeout = 0.9
      ag.retry_change_requests = true
    }
    
    
    @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE    
   
  end
  
  def lookup(prop)  #prop_address,zid,zillow_url
    
    attrs = {}
    
    elementary_school = nil
    middle_school= nil
    high_school= nil
    elementary_school_rating= nil
    middle_school_rating= nil
    high_school_rating= nil
    tax_amount = nil
    appreciation = nil
    zillow_sale_estimate = nil
    zillow_rental_estimate = nil
     
    tries = 0
     
    begin
      
      proxy = Proxy.find_available
      
      if proxy 
        ips = GoodIp.find_working_ips
        if (!ips || (ips && (ips.count < 6)))
          ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
        end
        Delayed::Worker.logger.debug "~~ ip ja #{ips}"
        case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
        when 0
          agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
        when 1
          agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
        when 2
          agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
        when 3
          agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
        when 4
          agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
        when 5
          agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
        end
        # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        
        href = prop.zillow_url
      
        if href
          
          zillow_id = nil
          square_feet = nil 
          lot_description = nil
          lot_square_feet = nil
          
          zillow_school_1 = nil
          zillow_school_2 = nil
          zillow_school_3 = nil
          
          Delayed::Worker.logger.debug "~~ href ja #{href}"
          phref = href
          puts ">>> href #{phref}"
          Delayed::Worker.logger.debug "~~ proxy: #{agent.proxy_addr}"
          page = agent.get phref
          # raise Exception
          proxy.increment_calls
          proxy.rem_error_count
          
          Delayed::Worker.logger.debug "~~ href ja #{href}"
          m = href.match(/(\d+)_zpid\/$/).captures rescue nil
          puts "$$% zillow get #{zillow_id}"
          
          if !prop.zillow_id 
            if m && m.length > 0
             zillow_id = m[0] rescue nil
            end
          else
            zillow_id = prop.zillow_id
          end
          
          puts "$$$% xpath #{page.parser.xpath("//a[contains(@href,'-school-')]/following-sibling::span[1]").count}"
          scl_count = 0
          page.parser.xpath("//a[contains(@href,'/schools/') and contains(@class,'school-name')] ").each do |scl|
            scl_item = scl.parent
            puts "scl_item = #{scl_item}"
            scl_desc = scl_item.first_element_child.text
            puts "scl_desc = #{scl_desc}"
            
            case scl_count 
            when 0
              zillow_school_1 = scl.text
            when 1
              zillow_school_2 = scl.text
            when 2
              zillow_school_3 = scl.text 
            end            
            
            if (scl_desc.match(/Elementary/))  
              elementary_school ||= scl_desc.gsub("Elementary","").strip
              puts "elementary_school = #{elementary_school}"
              elementary_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               puts "elementary_school_rating = #{elementary_school_rating}"
            elsif (scl_desc.match(/Middle/) || scl_desc.match(/Junior High/) || scl_desc.match(/J High/) || scl_desc.match(/Intermediate/))
              middle_school ||= scl_desc.gsub(/(Middle|Intermediate|Junior High|J High)\s*\w*/,"").strip
               puts "middle_school = #{middle_school}"
              middle_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               puts "middle_school_rating = #{middle_school_rating}"
            elsif scl_desc.match(/High/) && !(scl_desc.match(/Junior High/) || scl_desc.match(/J High/) )
              high_school ||= scl_desc.gsub(/High/,"").strip
               puts "high_school = #{high_school}"
              high_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
              puts "high_school_rating = #{high_school_rating}"
            end
            scl_count += 1
          end
          
          appre_catch = page.at("//div[@class='track-ga-event']").text.scan(/[0-9\.]+%/) rescue []
          puts "~~appre catch length #{appre_catch.length}"
          if appre_catch.length > 0
            appreciation = appre_catch[0].gsub(/%/,"").to_d / 100.to_d
          end
          
          # Delayed::Worker.logger.debug "~~ Page Ja #{page.body}"
          
          # square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text)
          
          
          
          if ((prop.market_id == 5) || (prop.market_id == 9) || (prop.market_id == 10))
            case prop.category
              when "lots"
                #square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/^\d+/)  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:text) == "Lot:"
                m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text)
                if prop.lot_description.blank? && !m_lot_description.blank?
                  lot_description = m_lot_description
                end
                
                m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                
                if prop.lot_square_feet.blank? && m_lot_square_feet
                  if m_lot_square_feet[2].in?(["sq ft","sqft"])
                    lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                  else
                    lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                  end
                end
              else
                
                if prop.square_feet.blank? || prop.square_feet.to_s.gsub(/\s+/,'') == "0"
                  m_sqft = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                  m_sqft ||= page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                   
                   Delayed::Worker.logger.debug "~~ raw square feet #{page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text)}"
                   Delayed::Worker.logger.debug "~~ m lot description  #{m_sqft}"
                  if m_sqft
                    square_feet = m_sqft[1].try(:gsub,/,/,"")
                  else
                    # x_sqft ||= page.search("//footer[@id='footer']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    # x_sqft ||= page.search("//footer[@class='zsg-subfooter']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of|single family home is|This mobile \/ manufactured home is)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    x_sqft ||= page.search("//footer[@class='zsg-subfooter']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of|single family home is|mobile \/ manufactured home is|mobile \/ manufactured home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    x_sqft ||= page.search("//footer[@class='zsg-subfooter seo-footer']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of|single family home is|mobile \/ manufactured home is|mobile \/ manufactured home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    Delayed::Worker.logger.debug "~~ x square feet gsub #{x_sqft}"
                    square_feet = x_sqft.try(:gsub,/,/,"")
                  end
                  Delayed::Worker.logger.debug "~~ lot description  #{square_feet}"
                  
                end
                
                if prop.lot_description.blank?
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/)
                    m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                    # page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                   
                     Delayed::Worker.logger.debug "~~ m lot description #1  #{m_lot_description}"
                  
                  else
                    
                    m_lot_description = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+\s*sqft|sq ft)/).try(:[],1)
                    
                    Delayed::Worker.logger.debug "~~ m lot description #2  #{m_lot_description}"
                  end
                  
                  lot_description = m_lot_description if !m_lot_description.blank? && m_lot_description != "Contact for details"

                # ---- STILL WRONG ------- #
                end
                
                if prop.lot_square_feet.blank?
                  m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/) 
                    if m_lot_square_feet
                      if m_lot_square_feet[2].in?(["sq ft","sqft"])
                        lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                      else
                        lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                      end
                    end
                  else
                    
                     lot_square_feet = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+)\s*sqft|sq ft/).try(:[],1)
                     lot_square_feet = lot_square_feet.try(:gsub,/,/,"")
                  end
                end
                # ---- STILL WRONG ------- #
                Delayed::Worker.logger.debug "~~ lot_square_feet  #{lot_square_feet}"
              
            end
          elsif (prop.market_id == 3) 
            case prop.category
              when "single_family"
                
                
                if prop.square_feet.blank? || prop.square_feet.to_s.gsub(/\s+/,'') == "0"
                  m_sqft = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                  m_sqft ||= page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                   
                   Delayed::Worker.logger.debug "~~ raw square feet #{page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text)}"
                   Delayed::Worker.logger.debug "~~ m lot description  #{m_sqft}"
                  if m_sqft
                    square_feet = m_sqft[1].try(:gsub,/,/,"")
                  else
                    x_sqft ||= page.search("//footer[@id='footer']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    Delayed::Worker.logger.debug "~~ x square feet gsub #{x_sqft}"
                    square_feet = x_sqft.try(:gsub,/,/,"")
                  end
                  Delayed::Worker.logger.debug "~~ lot description  #{square_feet}"
                  
                end
                
                if prop.lot_square_feet.blank?
                  m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/) 
                    if m_lot_square_feet
                      if m_lot_square_feet[2].in?(["sq ft","sqft"])
                        lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                      else
                        lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                      end
                    end
                  else
                    
                     lot_square_feet = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+)\s*sqft|sq ft/).try(:[],1)
                     lot_square_feet = lot_square_feet.try(:gsub,/,/,"")
                  end
                end
                # ---- STILL WRONG ------- #
                Delayed::Worker.logger.debug "~~ lot_square_feet  #{lot_square_feet}"
              
            end
          end
          # puts " scrape sale est: #{page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text}"
          # page.at("//div[@class='prop-mod           class="prop-mod prop-facts
          zillow_sale_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text).amount.to_i  rescue nil
          zillow_rental_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Rent Zestimate')]").next_element.text).amount.to_i rescue nil
          puts "@@@@ zillow_sale_estimate & zillow_rental_estimate #{zillow_sale_estimate} : #{zillow_rental_estimate}"
          
          m = page.body.match(/(jsModule:"z-hdp-price-history).+?AjaxRender.htm(.+?)",divId:"hdp-tax-history"/)
          
          if m && m.length 
            
            yhref = "http://www.zillow.com/AjaxRender.htm#{m[2]}"
            zhref = "#{yhref}"
            Delayed::Worker.logger.debug "yhref #{zhref}"
            
            proxy = Proxy.find_available
            page = agent.get zhref
            proxy.increment_calls
            proxy.rem_error_count
            html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","'").gsub(/^ \{ 'html': '/,"").gsub(/'\s+}\s+$/,""))
            
            Delayed::Worker.logger.debug "#### tax amount html #{html_doc}"
            
            begin
              # tax_amount = html_doc.at("//tbody/*[1]").first_element_child.next.text rescue nil
              node = html_doc.at("//tbody/*[1]")
              tax_amount = node.first_element_child.next.search('./text()').text rescue nil
              
              if tax_amount
                tax_money =  Money.parse(tax_amount) 
                tax_amount = tax_money.amount.to_i
              end
            
            rescue
              
            end
            
            
          end
          
          puts "###% zillow id #{zillow_id}"
          puts "### walk score http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}"
         
          wshref = "http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}"
          qhref = "#{wshref}"
          
          Delayed::Worker.logger.debug "qhref #{qhref}"
          
          proxy = Proxy.find_available
          page = agent.get qhref
          proxy.increment_calls
          proxy.rem_error_count
          
          html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","").gsub(/^{ text : /,"").gsub(/\s+}$/,""))
          
          Delayed::Worker.logger.debug "#### walk score #{html_doc}"
          
          # walk_score = html_doc.at("//div[@id='walkscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          
          walk_score = html_doc.at("//span[@class='ws-value']").text rescue nil
          Delayed::Worker.logger.debug "-- walk #{walk_score}"
          walk_score = nil if walk_score == 0
          transit_score = html_doc.at("//div[@id='transitscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          transit_score = nil if transit_score == 0
          puts "#%%%%% walk score #{walk_score}"
          puts "#%%%%% transit score #{transit_score}"
          
          
          puts "#%%% out #{elementary_school} #{elementary_school_rating}"
          attrs = { 
                    :walk_score => walk_score,
                    :transit_score => transit_score,
                    :school_updated_at => Time.now
                    
                 }
           attrs = attrs.merge({:elementary_school => elementary_school}) if elementary_school
           attrs = attrs.merge({:middle_school => middle_school}) if middle_school
           attrs = attrs.merge({:high_school => high_school}) if high_school
           attrs = attrs.merge({:elementary_school_rating => elementary_school_rating}) if elementary_school_rating
           attrs = attrs.merge({:middle_school_rating => middle_school_rating}) if middle_school_rating
           attrs = attrs.merge({:high_school_rating => high_school_rating}) if high_school_rating
           attrs = attrs.merge({:zillow_tax_amount => tax_amount})  if tax_amount && tax_amount > 0
           attrs = attrs.merge({:appreciation_rate => appreciation}) if appreciation
           attrs = attrs.merge({:zillow_sale_estimate => zillow_sale_estimate}) if !prop.zillow_sale_estimate
           attrs = attrs.merge({:zillow_rental_estimate => zillow_rental_estimate}) if !prop.zillow_rental_estimate
           attrs = attrs.merge({:square_feet => square_feet}) if square_feet && (square_feet.to_s.gsub(/\s+/,'') != "0")
           attrs = attrs.merge({:lot_description => lot_description}) if lot_description
           attrs = attrs.merge({:lot_square_feet => lot_square_feet}) if lot_square_feet
           attrs = attrs.merge({:zillow_school_1 => zillow_school_1}) if zillow_school_1
           attrs = attrs.merge({:zillow_school_2 => zillow_school_2}) if zillow_school_2
           attrs = attrs.merge({:zillow_school_3 => zillow_school_3}) if zillow_school_3
           
           puts "##** returning attrs #{attrs}"
           
                
        else
          return nil
        end
      else
        return nil
      end
      attrs
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      tries += 1
      Delayed::Worker.logger.debug "Timeout in second zillow call: #{e.message}  #{e.class.to_s}"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)
      if tries > 2
        proxy.try(:mark_error)
        proxy.try(:add_error_count)
      end
      retry if tries <= 3
      return nil
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug  "other error in second zillow call #{e.message}  #{e.class.to_s}"
      Delayed::Worker.logger.debug "backtrace #{error.backtrace}"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)
      if tries > 2
        proxy.try(:mark_error)
        proxy.try(:add_error_count)
      end
      retry if tries <= 3
      return nil
    end
    
  end
  
  def top_up  #prop_address,zid,zillow_url

    attrs = {}
     
    tries = 0
        
    begin
      

      test_items = Proxy.where(["1=1"]).order("id asc")
      p = Property.find(6174248)
      
      i = 0
      GoodIp.remove_all
      test_items.each do |prox|
        
        begin
          agent.set_proxy(prox.ip,prox.port,prox.login,prox.password) if !Rails.env.development?
          
          href = p.zillow_url
          
          begin
            page = agent.get href
          rescue Exception => e 
            case e.message
            when /403/ then
              
              puts "errors 403 #{prox.ip}: #{e.message}"
              next
            when /404/ then
              puts "errors 404 #{prox.ip}: #{e.message}"
              next
            else
              puts "errors else #{prox.ip}: #{e.message}"
              next
            end
          end
  
          nodes = page.parser.xpath("//div[@id='wrapper'][@class='main-wrapper']")
          if nodes && nodes.count > 0
            GoodIp.create(ip: prox.ip)
            i += 1
  
          end
          break if (i == 6)
        rescue  Exception => e 
          puts "errors"
        end
          
      end
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      
      raise e
    rescue Exception => e
      raise e
    end

  end
  
  def lookup2(prop)  #prop_address,zid,zillow_url
    
    attrs = {}
    
    elementary_school = nil
    middle_school= nil
    high_school= nil
    elementary_school_rating= nil
    middle_school_rating= nil
    high_school_rating= nil
    tax_amount = nil
    appreciation = nil
    zillow_sale_estimate = nil
    zillow_rental_estimate = nil
     
    tries = 0
     
    begin
      
      proxy = Proxy.find_available  

      if proxy 
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        
        href = prop.zillow_url
      
        if href
          
          zillow_id = nil
          square_feet = nil 
          lot_description = nil
          lot_square_feet = nil
          
          Delayed::Worker.logger.debug "~~ href ja #{href}"
          phref = href
          puts ">>> href #{phref}"
          page = agent.get phref
          # raise Exception
          proxy.try(:increment_calls)  
          proxy.try(:rem_error_count) 
          
          m = href.match(/(\d+)_zpid\/$/).captures rescue nil
          puts "$$% zillow get #{zillow_id}"
          
          if !prop.zillow_id 
            if m && m.length > 0
             zillow_id = m[0] rescue nil
            end
          else
            zillow_id = prop.zillow_id
          end
          
          puts "$$$% xpath #{page.parser.xpath("//a[contains(@href,'-school-')]/following-sibling::span[1]").count}"
          page.parser.xpath("//a[contains(@href,'-school-')]").each do |scl|
            scl_item = scl.parent
            puts "scl_item = #{scl_item}"
            scl_desc = scl.text
            puts "scl_desc = #{scl_desc}"
            if scl_desc.match(/Elementary/)
              elementary_school ||= scl_desc..gsub("Elementary","").strip
              puts "elementary_school = #{elementary_school}"
              elementary_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               puts "elementary_school_rating = #{elementary_school_rating}"
            elsif scl_desc.match(/Middle/) || scl_desc.match(/Junior High/) || scl_desc.match(/Intermediate/)
              middle_school ||= scl_desc.gsub(/Middle|Intermediate|Junior High/,"").strip
               puts "middle_school = #{middle_school}"
              middle_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               puts "middle_school_rating = #{middle_school_rating}"
            elsif scl_desc.match(/High/) && !scl_desc.match(/Junior High/)
              high_school ||= scl_desc.gsub(/High/,"").strip
               puts "high_school = #{high_school}"
              high_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
              puts "high_school_rating = #{high_school_rating}"
            end
          end
          
          appre_catch = page.at("//div[@class='forecast-text']").text.scan(/[0-9\.]+%/) rescue []
          puts "~~appre catch length #{appre_catch.length}"
          if appre_catch.length > 0
            appreciation = appre_catch[0].gsub(/%/,"").to_d / 100.to_d
          end
          
          if ((prop.market_id == 5) || (prop.market_id == 9) || (prop.market_id == 10))
            case prop.category
              when "lots"
                #square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/^\d+/)  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:text) == "Lot:"
                m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text)
                if prop.lot_description.blank? && !m_lot_description.blank?
                  lot_description = m_lot_description
                end
                
                m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                
                if (prop.lot_square_feet.blank? || prop.square_feet.to_s.gsub(/\s+/,'') == "0") && m_lot_square_feet
                  if m_lot_square_feet[2].in?(["sq ft","sqft"])
                    lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                  else
                    lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                  end
                end
              else
                if prop.square_feet.blank? || prop.square_feet.to_s.gsub(/\s+/,'') == "0"
                  m_sqft = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                  m_sqft ||= page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                   
                   Delayed::Worker.logger.debug "~~ raw square feet #{page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text)}"
                   Delayed::Worker.logger.debug "~~ m lot description  #{m_sqft}"
                  if m_sqft
                    square_feet = m_sqft[1].try(:gsub,/,/,"")
                  else
                    x_sqft ||= page.search("//footer[@id='footer']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    Delayed::Worker.logger.debug "~~ x square feet gsub #{x_sqft}"
                    square_feet = x_sqft.try(:gsub,/,/,"")
                  end
                  Delayed::Worker.logger.debug "~~ lot description  #{square_feet}"
                  
                end
                
                if prop.lot_description.blank?
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/)
                    m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                    # page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                   
                     Delayed::Worker.logger.debug "~~ m lot description #1  #{m_lot_description}"
                  
                  else
                    
                    m_lot_description = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+\s*sqft|sq ft)/).try(:[],1)
                    
                    Delayed::Worker.logger.debug "~~ m lot description #2  #{m_lot_description}"
                  end
                  
                  lot_description = m_lot_description if !m_lot_description.blank? && m_lot_description != "Contact for details"

                # ---- STILL WRONG ------- #
                end
                
                if prop.lot_square_feet.blank?
                  m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/) 
                    if m_lot_square_feet
                      if m_lot_square_feet[2].in?(["sq ft","sqft"])
                        lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                      else
                        lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                      end
                    end
                  else
                    
                     lot_square_feet = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+)\s*sqft|sq ft/).try(:[],1)
                     lot_square_feet = lot_square_feet.try(:gsub,/,/,"")
                  end
                end
                # ---- STILL WRONG ------- #
                Delayed::Worker.logger.debug "~~ lot_square_feet  #{lot_square_feet}"
              
            end
          end
          
          
          zillow_sale_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text).amount.to_i  rescue nil
          zillow_rental_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Rent Zestimate')]").next_element.text).amount.to_i rescue nil
          puts "@@@@ zillow_sale_estimate & zillow_rental_estimate #{zillow_sale_estimate} : #{zillow_rental_estimate}"
          m = page.body.match(/(jsModule:"z-hdp-price-history).+?AjaxRender.htm(.+?)",divId:"hdp-tax-history"/)
          
          if m && m.length 
            
            yhref = "http://www.zillow.com/AjaxRender.htm#{m[2]}"
            zhref = "#{yhref}"
            Delayed::Worker.logger.debug "yhref #{zhref}"
            
            proxy = Proxy.find_available  if !Rails.env.development?
            page = agent.get zhref
            proxy.try(:increment_calls)  
            proxy.try(:rem_error_count)  
            html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","'").gsub(/^ \{ 'html': '/,"").gsub(/'\s+}\s+$/,""))
            
            Delayed::Worker.logger.debug "#### tax amount html #{html_doc}"
            
            begin
              node = html_doc.at("//tbody/*[1]")
              tax_amount = node.first_element_child.next.search('./text()').text rescue nil
              
              if tax_amount
                tax_money =  Money.parse(tax_amount) 
                tax_amount = tax_money.amount.to_i
              end
            
            rescue
              
            end
            
            
          end
          
          puts "###% zillow id #{zillow_id}"
          puts "### walk score 'http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}'}"
         
          wshref = "http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}"
          qhref = "#{wshref}"
          
          Delayed::Worker.logger.debug "qhref #{qhref}"
          
          proxy = Proxy.find_available  
          page = agent.get qhref
          proxy.try(:increment_calls)  
          proxy.try(:rem_error_count)  
          
          html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","").gsub(/^{ text : /,"").gsub(/\s+}$/,""))
          
          Delayed::Worker.logger.debug "#### walk score #{html_doc}"
          
          walk_score = html_doc.at("//div[@id='walkscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          walk_score = nil if walk_score == 0
          transit_score = html_doc.at("//div[@id='transitscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          transit_score = nil if transit_score == 0
          puts "#%%%%% walk score #{walk_score}"
          puts "#%%%%% transit score #{transit_score}"
          
          
          puts "#%%% out #{elementary_school} #{elementary_school_rating}"
          attrs = { 
                    :walk_score => walk_score,
                    :transit_score => transit_score,
                    :school_updated_at => Time.now,
                    
                 }
           attrs = attrs.merge({:elementary_school => elementary_school}) if elementary_school
           attrs = attrs.merge({:middle_school => middle_school}) if middle_school
           attrs = attrs.merge({:high_school => high_school}) if high_school
           attrs = attrs.merge({:elementary_school_rating => elementary_school_rating}) if elementary_school_rating
           attrs = attrs.merge({:middle_school_rating => middle_school_rating}) if middle_school_rating
           attrs = attrs.merge({:high_school_rating => high_school_rating}) if high_school_rating
           attrs = attrs.merge({:zillow_tax_amount => tax_amount})  if tax_amount
           attrs = attrs.merge({:appreciation_rate => appreciation}) if appreciation
           attrs = attrs.merge({:zillow_sale_estimate => zillow_sale_estimate}) if !prop.zillow_sale_estimate
           attrs = attrs.merge({:zillow_rental_estimate => zillow_rental_estimate}) if !prop.zillow_rental_estimate
           attrs = attrs.merge({:square_feet => square_feet}) if square_feet && (square_feet.to_s.gsub(/\s+/,'') != "0")
           attrs = attrs.merge({:lot_description => lot_description}) if lot_description
           attrs = attrs.merge({:lot_square_feet => lot_square_feet}) if lot_square_feet
           
           puts "##** returning attrs #{attrs}"
           
                
        else
          return nil
        end
      else
        return nil
      end
      attrs
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      tries += 1
      Delayed::Worker.logger.debug "Timeout in second zillow call: #{e.message}  #{e.class.to_s}"
      proxy = Proxy.find_available  
      proxy.try(:increment_calls) 
      if tries > 2
        proxy.try(:mark_error)  
        proxy.try(:add_error_count)  
      end
      retry if tries <= 3
      return nil
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug  "other error in second zillow call #{e.message}  #{e.class.to_s}"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)  
      if tries > 2
        proxy.try(:mark_error)  
        proxy.try(:add_error_count)  
      end
      retry if tries <= 3
      return nil
    end
    
  end
  
  def lookup3(prop)  #prop_address,zid,zillow_url
    
    attrs = {}
    
    elementary_school = nil
    middle_school= nil
    high_school= nil
    elementary_school_rating= nil
    middle_school_rating= nil
    high_school_rating= nil
    tax_amount = nil
    appreciation = nil
    zillow_sale_estimate = nil
    zillow_rental_estimate = nil
    city = nil
     
    tries = 0
     
    begin
      
      proxy = Proxy.find_available  

      if proxy 
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        
        href = prop.zillow_url
      
        if href
          
          zillow_id = nil
          square_feet = nil 
          lot_description = nil
          lot_square_feet = nil
          
          Delayed::Worker.logger.debug "~~ href ja #{href}"
          phref = href
          puts ">>> href #{phref}"
          page = agent.get phref
          # raise Exception
          proxy.try(:increment_calls)  
          proxy.try(:rem_error_count) 
          
          m = href.match(/(\d+)_zpid\/$/).captures rescue nil
          puts "$$% zillow get #{zillow_id}"
          
          if !prop.zillow_id 
            if m && m.length > 0
             zillow_id = m[0] rescue nil
            end
          else
            zillow_id = prop.zillow_id
          end
          
          addr_token = page.parser.xpath("//title").text
          puts "$$$% title #{addr_token}"
          if !addr_token.blank?
            addr_elems = addr_token.split(",")
            
            if addr_elems.count > 0
              city = addr_elems[1].gsub(/\s+/,"")
              
            end
          end
          
          
          
          puts "$$$% xpath #{page.parser.xpath("//a[contains(@href,'-school-')]/following-sibling::span[1]").count}"
          page.parser.xpath("//a[contains(@href,'-school-')]").each do |scl|
            scl_item = scl.parent
            puts "scl_item = #{scl_item}"
            scl_desc = scl.text
            puts "scl_desc = #{scl_desc}"
            if scl_desc.match(/Elementary/)
              elementary_school ||= scl_desc.gsub("Elementary","").strip
              puts "elementary_school = #{elementary_school}"
              elementary_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               puts "elementary_school_rating = #{elementary_school_rating}"
            elsif scl_desc.match(/Middle/) || scl_desc.match(/Junior High/) || scl_desc.match(/Intermediate/)
              middle_school ||= scl_desc.gsub(/Middle|Intermediate|Junior High/,"").strip
               puts "middle_school = #{middle_school}"
              middle_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               puts "middle_school_rating = #{middle_school_rating}"
            elsif scl_desc.match(/High/) && !scl_desc.match(/Junior High/)
              high_school ||= scl_desc.gsub(/High/,"").strip
               puts "high_school = #{high_school}"
              high_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
              puts "high_school_rating = #{high_school_rating}"
            end
          end
          
          appre_catch = page.at("//div[@class='forecast-text']").text.scan(/[0-9\.]+%/) rescue []
          puts "~~appre catch length #{appre_catch.length}"
          if appre_catch.length > 0
            appreciation = appre_catch[0].gsub(/%/,"").to_d / 100.to_d
          end
          
          if prop.market_id == 5
            case prop.category
              when "lots"
                #square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/^\d+/)  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:text) == "Lot:"
                m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text)
                if prop.lot_description.blank? && !m_lot_description.blank?
                  lot_description = m_lot_description
                end
                
                m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                
                if prop.lot_square_feet.blank? && m_lot_square_feet
                  if m_lot_square_feet[2].in?(["sq ft","sqft"])
                    lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                  else
                    lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                  end
                end
              else
                if prop.square_feet.blank?
                  m_sqft = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                  m_sqft ||= page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                   
                   Delayed::Worker.logger.debug "~~ raw square feet #{page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text)}"
                   Delayed::Worker.logger.debug "~~ m lot description  #{m_sqft}"
                  if m_sqft
                    square_feet = m_sqft[1].try(:gsub,/,/,"")
                  else
                    x_sqft ||= page.search("//footer[@id='footer']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                    Delayed::Worker.logger.debug "~~ x square feet gsub #{x_sqft}"
                    square_feet = x_sqft.try(:gsub,/,/,"")
                  end
                  Delayed::Worker.logger.debug "~~ lot description  #{square_feet}"
                  
                end
                
                if prop.lot_description.blank?
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/)
                    m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                    # page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                   
                     Delayed::Worker.logger.debug "~~ m lot description #1  #{m_lot_description}"
                  
                  else
                    
                    m_lot_description = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+\s*sqft|sq ft)/).try(:[],1)
                    
                    Delayed::Worker.logger.debug "~~ m lot description #2  #{m_lot_description}"
                  end
                  
                  lot_description = m_lot_description if !m_lot_description.blank? && m_lot_description != "Contact for details"

                # ---- STILL WRONG ------- #
                end
                
                if prop.lot_square_feet.blank?
                  m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/) 
                    if m_lot_square_feet
                      if m_lot_square_feet[2].in?(["sq ft","sqft"])
                        lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                      else
                        lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                      end
                    end
                  else
                    
                     lot_square_feet = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+)\s*sqft|sq ft/).try(:[],1)
                     lot_square_feet = lot_square_feet.try(:gsub,/,/,"")
                  end
                end
                # ---- STILL WRONG ------- #
                Delayed::Worker.logger.debug "~~ lot_square_feet  #{lot_square_feet}"
              
            end
          end
          
          
          zillow_sale_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text).amount.to_i  rescue nil
          zillow_rental_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Rent Zestimate')]").next_element.text).amount.to_i rescue nil
          puts "@@@@ zillow_sale_estimate & zillow_rental_estimate #{zillow_sale_estimate} : #{zillow_rental_estimate}"
          m = page.body.match(/(^asyncLoader.load\().+?AjaxRender.htm(.+?)","divId.+:"hdp-tax-history","jsModule":"z-expando-table"}\)/)
          
          if m && m.length 
            
            yhref = "http://www.zillow.com/AjaxRender.htm#{m[2]}"
            zhref = "#{yhref}"
            Delayed::Worker.logger.debug "yhref #{zhref}"
            
            proxy = Proxy.find_available  if !Rails.env.development?
            page = agent.get zhref
            proxy.try(:increment_calls)  
            proxy.try(:rem_error_count)  
            html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","'").gsub(/^ \{ 'html': '/,"").gsub(/'\s+}\s+$/,""))
            
            Delayed::Worker.logger.debug "#### tax amount html #{html_doc}"
            
            begin
              node = html_doc.at("//tbody/*[1]")
              tax_amount = node.first_element_child.next.search('./text()').text rescue nil
              
              if tax_amount
                tax_money =  Money.parse(tax_amount) 
                tax_amount = tax_money.amount.to_i
              end
            
            rescue
              
            end
            
            
          end
          
          puts "###% zillow id #{zillow_id}"
          puts "### walk score 'http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}'}"
         
          wshref = "http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}"
          qhref = "#{wshref}"
          
          Delayed::Worker.logger.debug "qhref #{qhref}"
          
          proxy = Proxy.find_available  
          page = agent.get qhref
          proxy.try(:increment_calls)  
          proxy.try(:rem_error_count)  
          
          html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","").gsub(/^{ text : /,"").gsub(/\s+}$/,""))
          
          Delayed::Worker.logger.debug "#### walk score #{html_doc}"
          
          walk_score = html_doc.at("//div[@id='walkscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          walk_score = nil if walk_score == 0
          transit_score = html_doc.at("//div[@id='transitscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          transit_score = nil if transit_score == 0
          puts "#%%%%% walk score #{walk_score}"
          puts "#%%%%% transit score #{transit_score}"
          
          
          puts "#%%% out #{elementary_school} #{elementary_school_rating}"
          attrs = { 
                    :walk_score => walk_score,
                    :transit_score => transit_score,
                    :school_updated_at => Time.now,
                    
                 }
           attrs = attrs.merge({:elementary_school => elementary_school}) if elementary_school
           attrs = attrs.merge({:middle_school => middle_school}) if middle_school
           attrs = attrs.merge({:high_school => high_school}) if high_school
           attrs = attrs.merge({:elementary_school_rating => elementary_school_rating}) if elementary_school_rating
           attrs = attrs.merge({:middle_school_rating => middle_school_rating}) if middle_school_rating
           attrs = attrs.merge({:high_school_rating => high_school_rating}) if high_school_rating
           attrs = attrs.merge({:zillow_tax_amount => tax_amount})  if tax_amount
           attrs = attrs.merge({:appreciation_rate => appreciation}) if appreciation
           attrs = attrs.merge({:zillow_sale_estimate => zillow_sale_estimate}) if !prop.zillow_sale_estimate
           attrs = attrs.merge({:zillow_rental_estimate => zillow_rental_estimate}) if !prop.zillow_rental_estimate
           attrs = attrs.merge({:square_feet => square_feet}) if square_feet
           attrs = attrs.merge({:lot_description => lot_description}) if lot_description
           attrs = attrs.merge({:lot_square_feet => lot_square_feet}) if lot_square_feet
           attrs = attrs.merge({:city => city}) if city
           
           puts "##** returning attrs #{attrs}"
           
                
        else
          return nil
        end
      else
        return nil
      end
      attrs
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      tries += 1
      Delayed::Worker.logger.debug "Timeout in second zillow call: #{e.message}  #{e.class.to_s}"
      proxy = Proxy.find_available  
      proxy.try(:increment_calls) 
      if tries > 2
        proxy.try(:mark_error)  
        proxy.try(:add_error_count)  
      end
      retry if tries <= 3
      return nil
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug  "other error in second zillow call #{e.message}  #{e.class.to_s}"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)  
      if tries > 2
        proxy.try(:mark_error)  
        proxy.try(:add_error_count)  
      end
      retry if tries <= 3
      return nil
    end
    
  end
  
  
  def lookup5(prop)  #prop_address,zid,zillow_url
    
    attrs = {}
    
    elementary_school = nil
    middle_school= nil
    high_school= nil
    elementary_school_rating= nil
    middle_school_rating= nil
    high_school_rating= nil
    tax_amount = nil
    appreciation = nil
    zillow_sale_estimate = nil
    zillow_rental_estimate = nil
     
    tries = 0
     
    begin
      
      proxy = Proxy.find_available  

      if proxy 
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        
        href = prop.zillow_url
      
        if href
          
          zillow_id = nil
          square_feet = nil 
          lot_description = nil
          lot_square_feet = nil
          
          
          phref = href
          # puts ">>> href #{phref}"
          page = agent.get phref
          # raise Exception
          proxy.try(:increment_calls)  
          proxy.try(:rem_error_count) 
          
          m = href.match(/(\d+)_zpid\/$/).captures rescue nil
          # puts "$$% zillow get #{zillow_id}"
          
          if !prop.zillow_id 
            if m && m.length > 0
             zillow_id = m[0] rescue nil
            end
          else
            zillow_id = prop.zillow_id
          end
          
          # puts "$$$% xpath #{page.parser.xpath("//a[contains(@href,'-school-')]/following-sibling::span[1]").count}"
          page.parser.xpath("//a[contains(@href,'-school-')]").each do |scl|
            scl_item = scl.parent
            # puts "scl_item = #{scl_item}"
            scl_desc = scl.text
            # puts "scl_desc = #{scl_desc}"
            if scl_desc.match(/Elementary/)
              elementary_school ||= scl_desc.gsub("Elementary","").strip
              # puts "elementary_school = #{elementary_school}"
              elementary_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               # puts "elementary_school_rating = #{elementary_school_rating}"
            elsif scl_desc.match(/Middle/) || scl_desc.match(/Junior High/) || scl_desc.match(/Intermediate/)
              middle_school ||= scl_desc.gsub(/Middle|Intermediate|Junior High/,"").strip
               # puts "middle_school = #{middle_school}"
              middle_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
               # puts "middle_school_rating = #{middle_school_rating}"
            elsif scl_desc.match(/High/) && !scl_desc.match(/Junior High/)
              high_school ||= scl_desc.gsub(/High/,"").strip
               # puts "high_school = #{high_school}"
              high_school_rating ||= scl_item.parent.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
              # puts "high_school_rating = #{high_school_rating}"
            end
          end
          
          appre_catch = page.at("//div[@class='forecast-text']").text.scan(/[0-9\.]+%/) rescue []
          
          if appre_catch.length > 0
            appreciation = appre_catch[0].gsub(/%/,"").to_d / 100.to_d
          end
          
          if ((prop.market_id == 5) || (prop.market_id == 9) || (prop.market_id == 10))
            case prop.category
              when "lots"
                #square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/^\d+/)  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:text) == "Lot:"
                m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text)
                if prop.lot_description.blank? && !m_lot_description.blank?
                  lot_description = m_lot_description
                end
                
                m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[0].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                
                if prop.lot_square_feet.blank? && m_lot_square_feet
                  if m_lot_square_feet[2].in?(["sq ft","sqft"])
                    lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                  else
                    lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                  end
                end
              else
                if prop.square_feet.blank?
                  m_sqft = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[2].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                  m_sqft ||= page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(sqft|sq ft)/)
                                  
                  if m_sqft
                    square_feet = m_sqft[1].try(:gsub,/,/,"")
                  else
                    x_sqft ||= page.search("//footer[@id='footer']").try(:text).to_s.match(/(single family home of|single family of|condo of|vacant land of|a townhouse of|a home of|a multi family home of)\s*([0-9,.]+)\s*(sqft|sq ft)/).try(:[],2)
                 
                    square_feet = x_sqft.try(:gsub,/,/,"")
                  end
                 
                  
                end
                
                if prop.lot_description.blank?
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/)
                    m_lot_description = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                    # page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text)
                                     
                  else
                    
                    m_lot_description = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+\s*sqft|sq ft)/).try(:[],1)
                                        
                  end
                  
                  lot_description = m_lot_description if !m_lot_description.blank? && m_lot_description != "Contact for details"

                # ---- STILL WRONG ------- #
                end
                
                if prop.lot_square_feet.blank?
                  m_lot_square_feet = page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:next_element).try(:text).to_s.match(/(^[0-9,.]+)\s*(\D+)/)
                  if page.search("div.prop-mod.prop-facts>ul>li>strong.prop-facts-label")[3].try(:text).to_s.match(/Lot:/) 
                    if m_lot_square_feet
                      if m_lot_square_feet[2].in?(["sq ft","sqft"])
                        lot_square_feet = m_lot_square_feet[1].try(:gsub,/,/,"")
                      else
                        lot_square_feet = (m_lot_square_feet[1].try(:gsub,/,/,"").to_d * 43560).round(2)
                      end
                    end
                  else
                    
                     lot_square_feet = page.search("//footer[@id='footer']").try(:text).to_s.match(/lot of\s*([0-9,.]+)\s*sqft|sq ft/).try(:[],1)
                     lot_square_feet = lot_square_feet.try(:gsub,/,/,"")
                  end
                end
                # ---- STILL WRONG ------- #
                Delayed::Worker.logger.debug "~~ lot_square_feet  #{lot_square_feet}"
              
            end
          end
          
          
          zillow_sale_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text).amount.to_i  rescue nil
          zillow_rental_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Rent Zestimate')]").next_element.text).amount.to_i rescue nil
          
          m = page.body.match(/(^asyncLoader.load\().+?AjaxRender.htm(.+?)","divId.+:"home-tax-history","jsModule":"z-expando-table"}\)/)
          
          if m && m.length 
            
            yhref = "http://www.zillow.com/AjaxRender.htm#{m[2]}"
            zhref = "#{yhref}"
                        
            proxy = Proxy.find_available  if !Rails.env.development?
            page = agent.get zhref
            proxy.try(:increment_calls)  
            proxy.try(:rem_error_count)  
            html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","'").gsub(/^ \{ 'html': '/,"").gsub(/'\s+}\s+$/,""))
            
            begin
              node = html_doc.at("//tbody/*[1]")
              tax_amount = node.first_element_child.next.search('./text()').text rescue nil
              
              if tax_amount
                tax_money =  Money.parse(tax_amount) 
                tax_amount = tax_money.amount.to_i
              end
            
            rescue
              
            end
                      
          end
                             
          wshref = "http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}"
          qhref = "#{wshref}"
          
          proxy = Proxy.find_available  
          page = agent.get qhref
          proxy.try(:increment_calls)  
          proxy.try(:rem_error_count)  
          
          html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","").gsub(/^{ text : /,"").gsub(/\s+}$/,""))
          
          
          
          walk_score = html_doc.at("//div[@id='walkscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          walk_score = nil if walk_score == 0
          transit_score = html_doc.at("//div[@id='transitscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
          transit_score = nil if transit_score == 0
          
          
          
          attrs = { 
                    :walk_score => walk_score,
                    :transit_score => transit_score,
                    :school_updated_at => Time.now,
                    
                 }
           attrs = attrs.merge({:elementary_school => elementary_school}) if elementary_school
           attrs = attrs.merge({:middle_school => middle_school}) if middle_school
           attrs = attrs.merge({:high_school => high_school}) if high_school
           attrs = attrs.merge({:elementary_school_rating => elementary_school_rating}) if elementary_school_rating
           attrs = attrs.merge({:middle_school_rating => middle_school_rating}) if middle_school_rating
           attrs = attrs.merge({:high_school_rating => high_school_rating}) if high_school_rating
           attrs = attrs.merge({:zillow_tax_amount => tax_amount})  if tax_amount
           attrs = attrs.merge({:appreciation_rate => appreciation}) if appreciation
           attrs = attrs.merge({:zillow_sale_estimate => zillow_sale_estimate}) if !prop.zillow_sale_estimate
           attrs = attrs.merge({:zillow_rental_estimate => zillow_rental_estimate}) if !prop.zillow_rental_estimate
           attrs = attrs.merge({:square_feet => square_feet}) if square_feet
           attrs = attrs.merge({:lot_description => lot_description}) if lot_description
           attrs = attrs.merge({:lot_square_feet => lot_square_feet}) if lot_square_feet
           
           
           
                
        else
          return nil
        end
      else
        return nil
      end
      attrs
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      tries += 1
      
      proxy = Proxy.find_available  
      proxy.try(:increment_calls) 
      if tries > 2
        proxy.try(:mark_error)  
        proxy.try(:add_error_count)  
      end
      retry if tries <= 3
      return nil
    rescue Exception => e
      tries += 1
      
      proxy = Proxy.find_available
      proxy.try(:increment_calls)  
      if tries > 2
        proxy.try(:mark_error)  
        proxy.try(:add_error_count)  
      end
      retry if tries <= 3
      return nil
    end
    
  end
  
  def validate_address(prop)
    
    tries = 0
    
    begin
      
      # puts "~~entering zillow"
      proxy = Proxy.find_available  if !Rails.env.development?
      agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
      page = agent.get 'http://www.zillow.com/'
      proxy.try(:increment_calls) if !Rails.env.development?
      proxy.try(:rem_error_count) if !Rails.env.development?
      # puts "~~page.body #{page.body}"
      form = page.form_with(:name => "formSearchBar")
      # puts "~~address #{prop.address}"
      form.citystatezip = prop.address  #street number&street name, TX zip
      page = form.submit
      href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
      # puts "~~href #{href}"
      
      if href
        page = agent.get href
        proxy.try(:increment_calls) if !Rails.env.development?
        proxy.try(:rem_error_count) if !Rails.env.development?
        text = page.at("h1[@class='prop-addr']").try(:text)
        return text
      else
        sa = prop.street_name
        sl = sa.scan(" ").length rescue 0
        
        if sa.blank? || sl == 0
          return nil
        end
        
        sp = sa.split(" ")
        key = sp.last
        new_street_name = sp.delete_if { |e|  e == key}.join(" ")
        
        new_street_address = [prop.street_number,new_street_name].reject(&:blank?).join(' ')
        state_zip = [prop.state,prop.zip].reject(&:blank?).join(" ")
        new_address = [new_street_address,state_zip].reject(&:blank?).join(", ")
        proxy = Proxy.find_available
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        
        page = agent.get 'http://www.zillow.com/'
        proxy.try(:increment_calls) if !Rails.env.development?
        proxy.try(:rem_error_count) if !Rails.env.development?
        # puts "~~page.body #{page.body}"
        form = page.form_with(:name => "formSearchBar")
        # puts "~~address #{prop.address}"
        form.citystatezip = new_address  #street number&street name, TX zip
        page = form.submit
        href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
        # puts "~~href #{href}"
        
        if href
          
          page = agent.get href
          proxy.try(:increment_calls) if !Rails.env.development?
          proxy.try(:rem_error_count) if !Rails.env.development?
          text = page.at("h1[@class='prop-addr']").try(:text)
          return text
        end
        
        return nil
      end
      
    rescue  Exception => e
      tries += 1
      Delayed::Worker.logger.debug  "other error in second zillow call #{e.message}  #{e.class.to_s}"
      proxy = Proxy.find_available  if !Rails.env.development?
      proxy.try(:increment_calls)  if !Rails.env.development?
      if tries > 2
        proxy.try(:mark_error)  if !Rails.env.development?
        proxy.try(:add_error_count)  if !Rails.env.development?
      end
      retry if tries <= 3
      nil
    end
    
    
  end

  
end