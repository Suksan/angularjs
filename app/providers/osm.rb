class Osm < OsmProvider
  
  DATA_ENUMS = {
    category: hash_from_csv('categories')
    
  }
  DATA_CATEGORY = hash_from_csv('land_use_categories')
  
  DATA_MAPPING = {
    acreage: 'Acreage',
    address: 'Addr',
    # proc {|d| [d['StreetNo'].try(:gsub,/\s+/,''),d['StreetSuffix'].try(:gsub,/\s+/,''),d['StreetDir'].try(:gsub,/\s+/,''),d['StreetName'].try(:gsub,/\s+/,''),d['StreetDesc'].try(:gsub,/\s+/,''),d['StreetDir2'].try(:gsub,/\s+/,''),d['AddressUnit'].try(:gsub,/\s+/,'')].compact.reject(&:blank?).join(" ") },
    tax_value: 'AssessedValue',
    bathrooms_full: proc {|d| sd = !d['Baths'].blank? ? d['Baths'].split('/').reject(&:blank?) : []  ;  sd.try(:[],0)  } ,
    bathrooms_half: proc {|d| sd = !d['Baths'].blank? ? d['Baths'].split('/').reject(&:blank?) : []  ;  sd.try(:[],1)  } ,
    beds: 'Bedrooms',
    city: 'City',
    
    deed_date:  proc {|d|  !d['DeedDate'].blank? ?  (Date.strptime(d['DeedDate'], '%Y%m%d') rescue nil) : nil  },
    exemption: 'Exemption',
    square_feet: 'ImprovedSqFt',
    land_use: 'LandUse',
    land_use_code: 'LandUseCode',
    last_ad_price: 'LastPrice',
    last_sale_date:  proc {|d|  !d['LastSale'].blank? ?  (Date.strptime(d['LastSale'], '%Y%m%d') rescue nil) : nil  },
    legal_description: 'LegalDesc',
    key_map_page:    proc {|d| d['MapPage_Coord'].try(:gsub, /['=]/, '') },    #   'MapPage_Coord',
    tax_market_value: 'MarketValue',
    map_facet: 'MapFacet',
    mls_area_key: 'MLSArea',
    # owner_street_address: 'OwnerAddr1',
    owner_name: proc {|d| [d['OwnerFirstName'].try(:gsub,/\s+/,''),d['OwnerMiddleName'].try(:gsub,/\s+/,''),d['OwnerLastName'].try(:gsub,/\s+/,'')].compact.reject(&:blank?).join(" ") },
    owner_street_address: proc {|d| [d['OwnerAddr1'].try(:strip),d['OwnerAddr2'].try(:strip),d['OwnerAddr3'].try(:strip)].compact.reject(&:blank?).join(" ") },
    
    # owner_street_address: 'OwnerAddr3',
    owner_city: 'OwnerCity',
    owner_occupied: proc {|d| d['OwnerOccupied'] ? ( d['OwnerOccupied'] == 'Y' ? true : false) : false }, # 'OwnerOccupied',
    owner_state: 'OwnerState',
    owner_zip_code: 'OwnerZip',
    owner_zip_4: proc {|d| d['OwnerZip4'].try(:gsub, /['=]/, '') }, #'OwnerZip4',
    tax_id:    proc {|d| d['PIN'].try(:gsub, /['=]/, '') },
    state: 'State',
    stories: 'Stories',
    street_direction: proc {|d| [d['StreetDir'].try(:strip),d['StreetDir2'].try(:strip)].compact.reject(&:blank?).join(" ") },
    street_name: proc {|d| [d['StreetName'].try(:strip),d['StreetDesc'].try(:strip),d['StreetSuffix'].try(:strip),d['AddressUnit'].try(:strip)].compact.reject(&:blank?).join(" ") },
    street_number: 'StreetNo',    
    appraise_subdivision: 'Subdivision',
    year_built: 'YearBuilt',
    zip_code: 'zip',
    zip_4: proc {|d| d['Zip4'].try(:gsub, /['=]/, '') }, # 'Zip4'
    category: lookup(DATA_CATEGORY, 'LandUse')
  }
  
  protected

  private
  
    def parse_raw(data, filename)
      
      begin
        #@@ Delayed::Worker.logger.debug "entering parsing hotsheet "
        parser = Nokogiri::HTML(data)
        #@@ Delayed::Worker.logger.debug "##parser  #{parser.class}"
        rows = parser.xpath("//tr")
             
        
        headers = html_strip(parser.xpath("//tr[1]/td"), true)
        #@@ Delayed::Worker.logger.debug "##headers #{headers}"
        
        ita = 0
        
        county = nil
        m = filename.match(/(^[a-zA-Z]+)_/).captures rescue nil
        if m && m.length > 0
          county = m[0]
        end
        
        rows.each_with_index do |row,row_index|
          result = {}
          next if row_index <= 0
          values = Hash[headers.zip(html_strip(row.xpath("./td")))]
          
          #@@ Delayed::Worker.logger.debug "##value #{values}"
          
          ita += 1
          save_property(values, DATA_MAPPING, DATA_ENUMS, county)
        end
        
        m = filename.match(/[a-zA-Z]+_(\d{5})/).captures rescue nil
        Delayed::Worker.logger.debug  "filename: #{m[0]}:  ##row count: #{ita} " if m
        
        new_objects = ObjectSpace.count_objects
        
        new_objects.each do |k,v|
          Delayed::Worker.logger.debug  "#{k} #{v} "
        end
        
        ObjectSpace.garbage_collect
        
       rescue Exception => e
         Delayed::Worker.logger.debug  "^^ got exception #{e.message}"
       end
      
    end
  
end