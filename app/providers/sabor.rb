class Sabor < Provider
  LFD_INTERIOR_5_N = {
    "1FLRL" => "1st Floor Lvl/No Steps",  
    "BEDUP" => "All Bedrooms Upstairs",
    "ATRM" => "Atrium",
    "AXKIT" => "Auxillary Kitchen",
    "BKFST" => "Breakfast Bar",
    "CABLE" => "Cable TV Available",
    "CNVGR" => "Converted Garage",
    "EATIN" => "Eat-In Kitchen",
    "FLARM" => "Florida Room",
    "GAMRM" => "Game Room",
    "HGHCL" => "High Ceilings",
    "INTRN" => "High Speed Internet",
    "ISLKT" => "Island Kitchen",
    "LDCMB" => "Liv/Din Combo",
    "LOFT" => "Loft",
    "MDQTR" => "Maid's Quarters",
    "MEDIA" => "Media Room",
    "MILST" => "Mother-In-law Suite",
    "NA" => "Not Applicable",
    "1LVAR" => "One Living Area",
    "OPEN" => "Open Floor Plan",
    "OTH" => "Other",
    "PLDWN" => "Pull Down Storage",
    "SAUNA" => "Sauna",
    "SCBED" => "Secondary Bedroom Down",
    "SPDIN" => "Separate Dining Room",
    "SHOP" => "Shop",
    "SKYLT" => "Skylights",
    "STUDY" => "Study/Library",
    "3LVAR" => "Three Living Area",
    "2ETAR" => "Two Eating Areas",
    "2LVAR" => "Two Living Area",
    "UTGAR" => "Utility Area in Garage",
    "UTINS" => "Utility Room Inside",
    "WLKIN" => "Walk-In Pantry"
  }
  LOOKUP_TYPE_ENUMS = {
    exterior_description: {
      "ADDDW" => "Additional Dwelling",
      "HANGR" => "Aircraft Hangar",
      "BBQ" => "Bar-B-Que Pit/Grill",
      "CHLNK" => "Chain Link Fence",
      "CVPAT" => "Covered Patio",
      "DK/BL" => "Deck/Balcony",
      "DECBR" => "Decorative Bars",
      "DTQTR" => "Detached Quarters",
      "DGRUN" => "Dog Run Kennel",
      "DBLPN" => "Double Pane Windows",
      "GRILL" => "Gas Grill",
      "GAZE" => "Gazebo",
      "GTTRS" => "Has Gutters",
      "HORSE" => "Horse Stalls/Barn",
      "TREES" => "Mature Trees",
      "NONE" => "None",
      "OTKT" => "Outdoor Kitchen",
      "PRFNC" => "Partial Fence",
      "PRSPR" => "Partial Sprinkler System",
      "PTSLB" => "Patio Slab",
      "PVFNC" => "Privacy Fence",
      "PVTEN" => "Private Tennis",
      "SOLAR" => "Solar Screens",
      "SPCL" => "Special Yard Lighting",
      "SPSYS" => "Sprinkler System",
      "STONE" => "Stone/Masonry Fence",
      "STRG" => "Storage Building/Shed",
      "STRWN" => "Storm Windows",
      "WIRE" => "Wire Fence",
      "WKSHP" => "Workshop",
      "WRGHT" => "Wrought Iron Fence",
      "ABGRD" => "Above Ground Pool",
      "BBQGR" => "Bar-B-Que Pit/Grill",
      "CABLE" => "Cable TV Available",
      "CHNLN" => "Chain Link Fence",
      "CLBHS" => "Clubhouse",
      "CMNUT" => "Common Utility Room",
      "CNTRL" => "Controlled Access",
      "DECK" => "Deck/Balcony",
      "DVBRD" => "Diving Board",
      "GAZEB" => "Gazebo",
      "HOTTB" => "Hot Tub",
      "INGRD" => "In-Ground Pool",
      "NBRPL" => "Neighborhood Pool",
      "NBRTN" => "Neighborhood Tennis",
      "NA" => "Not Applicable/None",
      "PKPLG" => "Park/Playground",
      "PRTFN" => "Partial Fence",
      "PLHTD" => "Pool is Heated",
      "PRVFN" => "Privacy Fence",
      "PRVTN" => "Private Tennis",
      "SPAAD" => "Spa Adjoining Pool",
      "SPCL" => "Special Yard Lights",
      "SPRKL" => "Sprinkler System",
      "STRGE" => "Storage Building Shed",
      "STORM" => "Storm Windows",
      "TNNIS" => "Tennis Court",
      "CVPAT" => "Covered Patio",
      "PRCFN" => "Partial Fence"
    },

    exterior_type: {
      "3SDMS" => "3 Sides Masonry",
      "4SDMS" => "4 Sides Masonry",
      "ALUMN" => "Aluminum",
      "ASBSH" => "Asbestos Shingle",
      "BRICK" => "Brick",
      "CMTFB" => "Cement Fiber",
      "OTHER" => "Other",
      "SDING" => "Siding",
      "STONE" => "Stone/Rock",
      "STCCO" => "Stucco",
      "VINYL" => "Vinyl",
      "WOOD" => "Wood",
      "ASBSD" => "Asbestos Siding",
      "FRAME" => "Frame"
    },

    financing_available: {
      "100FN" => "100% Financing",
      "1ST" => "1st Seller Carry",
      "2ND" => "2nd Seller Carry",
      "NONQU" => "Assumption non Qualifying",
      "W/QUA" => "Assumption w/Qualifying",
      "BYDWN" => "Buydown",
      "CASH" => "Cash",
      "CONV" => "Conventional",
      "FHA" => "FHA",
      "INVOK" => "Investors OK",
      "LSOPT" => "Lease Option",
      "OTHER" => "Other",
      "RELRE" => "Release Req",
      "SLREQ" => "Seller Req/Qualify",
      "TXVET" => "TX Vet",
      "TRADE" => "Trade",
      "USDA" => "USDA",
      "VA" => "VA",
      "VASUB" => "VA Substitution",
      "WRAP" => "Wraparound",
      "1ASM" => "1st Loan Assumable",
      "2ASM" => "2nd Loan Assumable",
      "CNTRC" => "Contract for Deed",
      "LSPUR" => "Lease Purchase",
      "EXCHG" => "Will Exchange"
    },

    floor_type: {
      "CRPT" => "Carpeting",
      "CTILE" => "Ceramic Tile",
      "LMNAT" => "Laminate",
      "LNLEM" => "Linoleum",
      "MRBLE" => "Marble",
      "OTHER" => "Other",
      "PRQUT" => "Parquet",
      "STILE" => "Saltillo Tile",
      "SLATE" => "Slate",
      "STDCT" => "Stained Concrete",
      "VINYL" => "Vinyl",
      "WOOD" => "Wood"
    },

    foundation_type: {
      "BSMNT" => "Basement",
      "CEDAR" => "Cedar Post",
      "OTHER" => "Other",
      "SLAB" => "Slab"
    },

    heating_system: {
      "CNTRL" => "Central",
      "FLRFR" => "Floor Furnace",
      "HTPMP" => "Heat Pump",
      "JET" => "Jet",
      "NONE" => "None",
      "OTHER" => "Other",
      "PANEL" => "Panel",
      "WNDUN" => "Window Unit"
    },

    hoa_membership: {
      "MAND" => "Mandatory",
      "NONE" => "None",
      "VOLNT" => "Voluntary"
    },

    lot_description: {
      "1-2A" => "1 - 2 Acres",
      "-1AC" => "1/2-1 Acre",
      "-.5AC" => "1/4 - 1/2 Acre",
      "15+A" => "15 Acres Plus",
      "2-5AC" => "2 - 5 Acres",
      "5-14A" => "5 - 14 Acres",
      "BLUFF" => "Bluff View",
      "CITY" => "City View",
      "CRNR" => "Corner",
      "CNTY" => "County VIew",
      "CLDSC" => "Cul-de-Sac/Dead End",
      "HORSE" => "Horses Allowed",
      "IRR" => "Irregular",
      "LAKFR" => "Lakefront",
      "GLFCR" => "On Golf Course",
      "GRNBL" => "On Greenbelt",
      "WTRFR" => "On Waterfront",
      "RVRFR" => "Riverfront",
      "WATER" => "Water View",
      "ZLL" => "Zero Lot Line",
      "MTPLE" => "Multiple Parcels",
      "5-10A" => "Over 5 - 10 Acres",
      "STRGT" => "Street Gutters",
      "ZLL" => "Zero Lot Line"
    },

    lot_improvements: {
      "ALLEY" => "Alley",
      "CURBS" => "Curbs",
      "FRHYD" => "Fire Hydrant w/in 500'",
      "SDWLK" => "Sidewalks",
      "STGTR" => "Street Gutters",
      "PAVED" => "Street Paved",
      "STLGT" => "Streetlights"
    },

    rental_terms: {
      "100FN" => "100% Financing",
      "1ST" => "1st Seller Carry",
      "2ND" => "2nd Seller Carry",
      "NONQU" => "Assumption non Qualifying",
      "W/QUA" => "Assumption w/Qualifying",
      "BYDWN" => "Buydown",
      "CASH" => "Cash",
      "CONV" => "Conventional",
      "FHA" => "FHA",
      "INVOK" => "Investors OK",
      "LSOPT" => "Lease Option",
      "OTHER" => "Other",
      "RELRE" => "Release Req",
      "SLREQ" => "Seller Req/Qualify",
      "TXVET" => "TX Vet",
      "TRADE" => "Trade",
      "USDA" => "USDA",
      "VA" => "VA",
      "VASUB" => "VA Substitution",
      "WRAP" => "Wraparound",
      "1ASM" => "1st Loan Assumable",
      "2ASM" => "2nd Loan Assumable",
      "CNTRC" => "Contract for Deed",
      "LSPUR" => "Lease Purchase",
      "EXCHG" => "Will Exchange"
    },

    roof_type: {
      "BLTUP" => "Built-Up/Gravel",
      "CLAY" => "Clay",
      "COMP" => "Composition",
      "CONCR" => "Concrete",
      "HVCMP" => "Heavy Composition",
      "METAL" => "Metal",
      "OTHER" => "Other",
      "SLATE" => "Slate",
      "TILE" => "Tile",
      "WDSHN" => "Wood Shingle/Shake"
    },

    single_family_type: {

      "GPHD" => "patio",
      "MANU" => "manufactured",
      "MDHM" => "manufactured",
      "SFD" => "freestanding",
      "SFDET" => "freestanding",
      "TWNHM" => "townhome",
      "HIRIS" => nil,
      "LWRIS" => nil,
      "MDRIS" => nil,
      "GRAZE" => nil,
      "OTHER" => nil,
      "RELOT" => nil,
      "LTACR" => nil,
      "RSTPR" => nil,
      "RURAL" => nil,
      "URBAN" => nil,
      "APT" => nil,
      "CONDO" => nil,
      "DUPLX" => nil
    },
    style_of_building: {
      "3+" => "3 or More",
      "A-FRM" => "A-Frame",
      "COLNL" => "Colonial",
      "CONT" => "Contemporary",
      "HSTRC" => "Historic/Older",
      "CABIN" => "Log Cabin",
      "MDTRN" => "Mediterranean",
      "1STRY" => "One Story",
      "OTHER" => "Other",
      "RANCH" => "Ranch",
      "SPNSH" => "Spanish",
      "SPLIT" => "Split Level",
      "TEXAS" => "Texas Hill Country",
      "TRDNL" => "Traditional",
      "TUDOR" => "Tudor",
      "2STRY" => "Two Story",
      "VCTRN" => "Victorian"
    },
    rental_class: {
      "HIRIS" => "multi_family",
      "LWRIS" => "multi_family",
      "MDRIS" => "multi_family",
      "TWNHM" => "townhouse_condo"
    }

  }

  DATA_ENUMS = {
    # category: hash_from_csv('categories'),
    # rental_class: hash_from_csv('rental_classes'),
    # single_family_type: hash_from_csv('single_family_types'),
    status: hash_from_csv('statuses'),
    state: hash_from_csv('states'),
    hoa_bill_freq: hash_from_csv('hoa_bill_freqs'),
    mls_area_name: hash_from_csv('mls_areas'),
    rental_type: hash_from_csv('rental_types'),
    school_district_name: hash_from_csv('new_school_districts'),
    mls_elementary_school: hash_from_csv('mls_elementary_schools'),
    mls_middle_school: hash_from_csv('mls_middle_schools'),
    mls_high_school: hash_from_csv('mls_high_schools'),
    city: hash_from_csv("cities")
  }

  DATA_MAPPING = {
    annual_maintenance_fee: proc { |d| payment = nil; freq = nil; if (d['HOAFee'] && d['HOAFreq']) then payment = d['HOAFee'].to_d ; freq =  d['HOAFreq'] end; if freq == 'A' then (payment*1).to_s elsif freq == 'M' then (payment*12).to_s elsif freq == 'Q' then (payment*4).to_s elsif freq == 'S' then (payment*2).to_s  else nil end },
    bathrooms: 'TotalBath',#
    bathrooms_full: 'FullBaths',#
    bathrooms_half: 'HalfBaths',#
    beds: 'Bedrooms',#
    # block: 'Block',#
    builder_name: 'LM_Char25_10',#
    building_stories: 'Stories',#
    category: proc { |d|
      if d['L_Type_RE_N']
        'single_family'
      elsif d['L_Type_RR_N']
        'rental'
      elsif d['L_Type_CO_N']
        'townhouse_condo'
      elsif d['L_Type_LA_N']
        'lots'
      elsif d['L_Type_FR_N']
        'ranch_acerage'
      elsif d['L_Type_MF_N']
        'multi_family'  
      end

    },
    cdom: 'L_CDOM',#
    city: 'L_City',#
    closed_date: proc { |d| to_date_sql(d['L_ClosingDate']) },#
    cooling_system_type: 'LFD_AIRCONDITIONING_25_N',#
    county: 'LM_Char10_1',#
    disclosures: proc { |d|
      if d['POTSSYN'] == 'Y'
        'Short'
      elsif d['POTSSYN'] == 'N'
        ''
      else
        nil
      end
    },#
    dom: proc {|d| d['L_DOM']||0 },#
    exterior_description: 'LFD_EXTERIORFEATURES_3_N',#
    exterior_type: 'LFD_EXTERIOR_14_N',#
    financing_available: 'LFD_PROPOSEDTERMS_28_N',#
    fireplace_number: 'NoFireplace',
    floor_type: 'LFD_FLOOR_18_N',
    foundation_type: proc { |d| d['LFD_FOUNDATION_16_N']||d['LFD_FOUNDATION_224_N'] },
    garage_capacity: 'NoGarSpc',
    heating_system: proc { |d| d['LFD_HEATING_24_N']||d['LFD_HEATING_230_N'] },
    hoa_bill_freq: 'HOAFreq',
    hoa_membership: 'LM_Char10_13',
    interior_description: proc { |d|
      Delayed::Worker.logger.debug "LFD INTERIOR 5 N #{d['LFD_INTERIOR_5_N']}"
       i5n = if !d['LFD_INTERIOR_5_N'].blank?
         lookup_source(LFD_INTERIOR_5_N,d['LFD_INTERIOR_5_N'])
       end
       Delayed::Worker.logger.debug "LFD INTERIOR 215 N #{d['LFD_INTERIOR_215_N']}"
       i215n = if !d['LFD_INTERIOR_215_N'].blank?
         lookup_source(LFD_INTERIOR_5_N,d['LFD_INTERIOR_215_N'])
       end
       i5n || i215n
    },
    key_map_page: 'FERGUSON',
    land_size: 'LANDSQFT',
    latitude:  proc { |d| d['LAT'] ? d['LAT'].to_d : nil },
    legal_description: 'LM_char30_2',
    legal_subdivision: 'Subdivision_Lgl',
    level_of_unit: 'UNITLEVELS',
    list_price: 'L_AskingPrice',
    list_price_psf: 'PrceOrSQFT',
    listing_agent_email: 'LO1Email',
    listing_agent_id: 'LA1_LoginName',
    listing_agent_name: proc { |d| [
                                      d['LA1_UserFirstName'],
                                      d['LA1_UserLastName']
                                   ].compact.join(' ') },
    listing_agent_phone: 'LA1_CellPhone',
    listing_broker_id: 'L_ListOffice1',
    listing_broker_name: 'LO1_OrganizationName',
    listing_date: proc { |d| to_date_sql(d['L_ListingDate']) },
    longitude:  proc { |d|  d['Longitude'] ? d['Longitude'].to_d : nil },
    lot: 'LglDsc_Lot',
    lot_back_dimensions: 'LotSizeDim',
    lot_description: proc { |d| d['LFD_LOTDESCRIPTION_6_N']||d['LFD_LOTDESCRIPTION_216_N'] },
    lot_improvements: 'LFD_LOTIMPROVEMENTS_22_N',
    lot_square_feet: proc { |d| (d['LotSize'].to_s.to_d * 43560).round.to_s },
    maintenance_fee: "HOAFee",
    mls_area_key: 'L_Area_N',
    mls_area_name: 'L_Area_N',
    mls_elementary_school: 'ElemSchl',
    mls_high_school: 'HighSchl',
    mls_middle_school: 'MidSchl',
    mls_elementary_school_number: 'ElemSchl',
    mls_high_school_number: 'HighSchl',
    mls_middle_school_number: 'MidSchl',
    mls_number: proc { |d| d['L_ListingID'] },
    mls_record_creation_date: proc {|d| to_date_sql(d['L_UpdateDate']) }, #not sure
    new_construction: proc { |d|
      if d['LM_Char10_19_N'] == 'NEW'
        'Y'
      elsif d['LM_Char10_19_N'] == 'PRE-OWNED'
        'N'
      else
        nil
      end
    },
    number_of_photos: 'L_PictureCount',
    number_of_units: 'UnitNo',
    office_phone: 'LA1_OfficePhone',
    original_list_price: 'L_OriginalPrice',
    pending_date: proc { |d| to_date_sql(d['L_ContractDate']) }, #not sure
    pool_area: 'LFD_POOLSPA_8_N',
    pool_private: 'Pool',
    previous_list_price: 'LM_Int4_9',
    public_remarks: 'L_Remarks',
    realtor_remarks: 'L_Remarks',
    rental_class: 'L_Type_RR_N',
    rental_terms: proc {|d| d['LFD_PROPOSEDTERMS_28_N']||d['LFD_PROPOSEDTERMS_167_N']},
    rental_type: 'L_Type_RR_N',
    roof_type: proc { |d| d['LFD_ROOF_15_N']||d['LFD_ROOF_223_N']},
    sold_lease_price:   ['SOLDLEASE', 'L_SoldPrice'], 
    sold_price: 'L_SoldPrice',
    sale_price_psf: 'LM_Dec_19',
    school_district_name: 'LM_Char10_8',
    # selling_agent_name: 'L_SellingAgent1',
    selling_agent_phone: 'LA1_CellPhone',
    single_family_type: proc { |d| d['L_Type_RE_N'] || d['L_Type_RR_N'] || d['L_Type_LA_N'] || d['L_Type_CO_N']},
    square_feet: 'L_SquareFeet',
    state: 'L_State',
    status: 'L_Status_N',
    stories: 'NOSTRY',
    street_name: 'L_AddressStreet',
    street_direction: 'L_AddressDirection',
    street_number: 'L_AddressNumber',
    style_of_building: 'STYLE',
    subdivision: 'Subdivision',
    tax_amount: 'TotalTax',
    tax_id: 'T_list_tax_property_id',
    tax_year: 'TaxYear',
    unit_number: 'UnitNo',
    withdrawn_date: proc { |d| to_date_sql(d['L_OffMarketDate']) },
    year_built: 'YEARBUILT',
    # zip: 'L_Zip',
    zip_4: 'L_Zip6',
    zip_code: 'L_Zip',
    exemption: 'Over65',
    showing_instructions: 'LM_Char10_5',
  }

  def rets_login
    @client = Rets::Client.new(:login_url => "http://sabor-rets.connectmls.com/rets/server/login",:username => market.mls_login,:password => market.mls_password)
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "San Antonio RETS logged in"
    true
  end

  def rets_logout
    @client.logout
    Delayed::Worker.logger.debug "logged out"
    true
  end

  def mls_url_for(property)
    "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
  end

  def grab(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_rets()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

   def self.to_date_sql(dt)
    #"2014-05-09T15:40:14"
    field = dt.to_s
    pattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil
    subpattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil
    ma =  field.match(pattern).captures.try(:[],0) rescue nil
    ta =  field.match(subpattern).captures.try(:[],0) rescue nil
    if field && field.match(/(\d+\/\d+\/\d+)/)
      strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil
    else
      strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil
    end
  rescue
    nil
  end
  
  protected

  private

    def parse_raw(data, filename)

      begin
        CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

          # row['PropType'] ||= filename.try(:slice, -7, 3)
          save_property(row, DATA_MAPPING, DATA_ENUMS)
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "error #{e.message}"
      end
    end

    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end
    
    
    
    
    # def parse_rets()
#         
      # # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
      # timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
      # timerange = "2014-01-01-2014-06-11"
      # sd = Date.parse('2014-07-25')
      # ed = Date.parse('2014-08-14')
      # Delayed::Worker.logger.debug "--------- pulling single family ---------"
      # ed.downto(sd) { |date|
#         
        # Delayed::Worker.logger.debug "---------  #{date} ---------"
        # date1 = date.strftime("%Y-%m-%dT00:00:00")
        # date2 = date.strftime("%Y-%m-%dT06:00:00")
        # date3 = date.strftime("%Y-%m-%dT12:00:00")
        # date4 = date.strftime("%Y-%m-%dT18:00:00")
        # tomorrow = date + 1.day
        # date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
        # dates = []
        # dates = [date1,date2,date3,date4,date5]
        # # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
#         
        # # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
#         
#         
        # for i in 0..3
          # puts "---------  #{dates[i]} - #{dates[i+1]} ---------"  
#           
          # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # puts "--------- pulling ResidentialProperty ---------"
# #             
            # results = client.find(:all,
                      # :search_type => "Property",
                      # :class => "RE",
                      # :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{recdate})",
                      # :limit => 60000)
# #             
           # puts "--------- importing ResidentialProperty ---------"
            # results.each do |row|
              # puts "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # puts "No record found #{e.message}"
          # end
#           
          # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # puts "--------- pulling ResidentialProperty ---------"
# #             
            # results = client.find(:all,
                      # :search_type => "Property",
                      # :class => "RR",
                      # :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{recdate})",
                      # :limit => 60000)
# #             
            # puts "--------- importing ResidentialProperty ---------"
            # results.each do |row|
              # puts  "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # puts "No record found #{e.message}"
          # end
#           
            # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # puts "--------- pulling Land ---------"
# #             
            # results = client.find(:all,
                      # :search_type => "Property",
                      # :class => "LA",
                      # :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{recdate})",
                      # :limit => 60000)
# #             
            # puts "--------- importing Land ---------"
            # results.each do |row|
              # puts "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # puts "No record found #{e.message}"
          # end
#           
           # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # puts "--------- pulling Townhouse Condo ---------"
# #             
            # results = client.find(:all,
                      # :search_type => "Property",
                      # :class => "CO",
                      # :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{recdate})",
                      # :limit => 60000)
# #             
            # puts "--------- importing Townhouse Condo ---------"
            # results.each do |row|
              # puts "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # puts "No record found #{e.message}"
          # end
#       
           # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # puts "--------- pulling Ranch Acerage ---------"
# #             
            # results = client.find(:all,
                      # :search_type => "Property",
                      # :class => "FR",
                      # :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{recdate})",
                      # :limit => 60000)
# #             
            # puts "--------- importing Ranch Acerage ---------"
            # results.each do |row|
              # puts "##row: #{row}"
              # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # puts "No record found #{e.message}"
          # end
          # # begin  
# #            
            # # recdate = "#{dates[i]}-#{dates[i+1]}"
            # # Delayed::Worker.logger.debug "--------- pulling single family ---------"
            # # # results = client.find(:all,:search_type => "Property",:class => "AF",:query => "(STAT=A,B,CK,LP,N,PA,I,R,U),(RECMODDATE=#{recdate}),(TYP=AF)",:limit => 60000)
            # # # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(WTH,EXP,CAN,TOM,WS),(MODIFIED=#{recdate}),(PROPSUBTYPE=LF,R,F)",:limit => 60000)
            # # # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=KO,ACT,PND,CON,OPT),(MODIFIED=#{recdate}),(PROPSUBTYPE=LF,R,F)",:limit => 60000)
            # # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=LSE,SLD),(MODIFIED=#{recdate}),(PROPSUBTYPE=LF,R,F)",:limit => 60000)
            # # Delayed::Worker.logger.debug "--------- importing single family ---------"
    # # #
# #     
            # # results.each do |row|
               # # Delayed::Worker.logger.debug "##row: #{row}"
               # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
             # # end
# #           
          # # rescue Exception => e
            # # Delayed::Worker.logger.debug "No record found #{e.message}"
          # # end
#           
#           
#         
        # end
#         
#        
      # }
# 
#          
# 
    # end

    def parse_rets()

      begin
        
        divisor = 720
        offset = 840
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling ResidentialProperty ---------"
            # results = client.find(:all,
                                  # :search_type => "Property",
                                  # :class => "RR",
                                  # :query => "(L_ListingID=1034674)",
                                  # :limit => 60000)
            results = client.find(:all,
                                  :search_type => "Property",
                                  :class => "RE",
                                  :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{date_range})",
                                  :limit => 60000)
            Delayed::Worker.logger.debug "--------- importing ResidentialProperty ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
        
          begin
            Delayed::Worker.logger.debug "--------- pulling Rental ---------"
            results = client.find(:all,
                                  :search_type => "Property",
                                  :class => "RR",
                                  :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{date_range})",
                                  :limit => 60000)
            Delayed::Worker.logger.debug "--------- importing Rental ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling Land ---------"
            results = client.find(:all,
                                  :search_type => "Property",
                                  :class => "LA",
                                  :query => "(L_Status_N=ACT,ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{date_range})",
                                  :limit => 60000)
            Delayed::Worker.logger.debug "--------- importing Land ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling Condo ---------"
            results = client.find(:all,
                                  :search_type => "Property",
                                  :class => "CO",
                                  :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{date_range})",
                                  :limit => 60000)
            Delayed::Worker.logger.debug "--------- importing Condo ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling Ranch Acerage ---------"
            results = client.find(:all,
                                  :search_type => "Property",
                                  :class => "FR",
                                  :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{date_range})",
                                  :limit => 60000)
            Delayed::Worker.logger.debug "--------- importing Ranch Acerage ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
           begin
            Delayed::Worker.logger.debug "--------- pulling Multi Family ---------"
            results = client.find(:all,
                                  :search_type => "Property",
                                  :class => "MF",
                                  :query => "(L_Status_N=ACT,AO,RFR,AR,BOM,BOMS,EXP,CAN,EXT,NEW,PND,PDB,PCH,RNTD,SLD,WDN),(L_UpdateDate=#{date_range})",
                                  :limit => 60000)
            Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS )
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
    # def parse_rets()
# 
      # begin
#         
#        
        # begin
          # Delayed::Worker.logger.debug "--------- pulling ResidentialProperty ---------"
          # # results = client.find(:all,
                                # # :search_type => "Property",
                                # # :class => "RR",
                                # # :query => "(L_ListingID=1034674)",
                                # # :limit => 60000)
          # results = client.find(:all,
                                # :search_type => "Property",
                                # :class => "CO",
                                # :query => "(L_ListingID=1077354)",
                                # :limit => 60000)
          # Delayed::Worker.logger.debug "--------- importing ResidentialProperty ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
          # end
        # rescue  Exception => e
           # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
#         
#           
# 
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end

    def parse_special_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    end
end