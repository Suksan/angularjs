class Provider
  require 'csv'

  attr_accessor :market, :agent, :results, :errors, :current_filename, :listeners, :ids, :source, :rows_affected, :client, :job_id, :timestamp, :mls_source

  def initialize(market)
    self.market = market
    @client = Rets::Client.new(:login_url => "http://ntreisrets.mls.ntreis.net/rets/login")
    @agent = Mechanize.new { |ag|
      ag.read_timeout = 20
      ag.open_timeout = 20
      ag.idle_timeout = 0.9
      ag.retry_change_requests = true
    }
    
    @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    self.results = {}
    self.errors = {}
    self.listeners = []
    self.ids = []
    self.source = 'MLS'
    self.mls_source = nil
    self.job_id = nil
    self.timestamp = Time.new.strftime "%Y%m%d%H%M%S"

  end

  def upload(source, files, overrides)
    self.source = source
    self.import(files)
    self.current_filename = 'overrides'
    for override in overrides
      save_property(override)
    end
    notify_listeners
  end

  def import(files)
    [*files].each do |file|
      if file.is_a?(Hash)
        Delayed::Worker.logger.debug "In case it's a hash"
        filename = file[:name]
        content = File.open(file[:path]).read
      elsif file.is_a?(Mechanize::File) 
        Delayed::Worker.logger.debug "In case it's a Mechanize file"
        Delayed::Worker.logger.debug "> Saving file name #{file.filename}"
        file.save_as(Rails.root.join('tmp', file.filename))
        filename = file.filename
        content = file.content
      end
      
      Delayed::Worker.logger.debug "saving filename #{filename}"
      self.current_filename = filename

      # ensure content encoding is UTF-8 and replace double quotes
      content.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
      content.encode!('UTF-8', 'UTF-16')
      content.gsub!('"', "'")

      # process file
      puts "- parsing '#{filename}' with #{content.lines.count} lines"
      Delayed::Worker.logger.debug "#passing content #{content}"
      result = parse(content, filename)
      
      
      Delayed::Worker.logger.debug " ~~ file: #{current_filename} => #{self.results[current_filename]}"
      # execute listeners to notify of progress
      notify_listeners
    end
  end
  
  def repair(files)
    [*files].each do |file|
      if file.is_a?(Hash)
        filename = file[:name]
        content = File.open(file[:path]).read
      elsif file.is_a?(Mechanize::File) 
        file.save_as(Rails.root.join('tmp', file.filename))
        filename = file.filename
        content = file.content
      end
     
      self.current_filename = filename

      # ensure content encoding is UTF-8 and replace double quotes
      content.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
      content.encode!('UTF-8', 'UTF-16')
      content.gsub!('"', "'")

      # process file
      Delayed::Worker.logger.debug "- parsing '#{filename}' with #{content.lines.count} lines"
      result = parse(content, filename, true)
        
      # execute listeners to notify of progress
      notify_listeners
    end
  end
  
  def repair_geometry(files)
    [*files].each do |file|
      if file.is_a?(Hash)
        filename = file[:name]
        content = File.open(file[:path]).read
      elsif file.is_a?(Mechanize::File) 
        file.save_as(Rails.root.join('tmp', file.filename))
        filename = file.filename
        content = file.content
      end
     
      self.current_filename = filename

      # ensure content encoding is UTF-8 and replace double quotes
      content.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
      content.encode!('UTF-8', 'UTF-16')
      content.gsub!('"', "'")

      # process file
      Delayed::Worker.logger.debug "- parsing '#{filename}' with #{content.lines.count} lines"
      result = parse(content, filename, true)
        
      # execute listeners to notify of progress
      notify_listeners
    end
  end
  
  def add_dom(days)
    n = 0
    
    Delayed::Worker.logger.debug "reconcile update ##{market.id}"
    n = self.market.properties.on_market.update_all ["cdom = cdom+ DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL dom - 1 DAY)) ) 
                          , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          ["status in ('active','pending') and ((listing_date is not null) and ((cdom is not null) and (dom is not null)))"]
    
    Delayed::Worker.logger.debug "rows affected: #{n}"
    self.rows_affected = n
    
    notify_listeners
  end
  
  def grab(days)
    if login
      Delayed::Worker.logger.debug "~~~~~~~ Fetching data for grab ~~~~~~~~"
      fetch(days)
      logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

  def grab_by_interval(minutes=nil)
    if login
      Delayed::Worker.logger.debug "~~~~~~~ Fetching data for grab ~~~~~~~~"
      fetch(days)
      logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def sync_download(days)
    if login
      Delayed::Worker.logger.debug "~~~~~~~ Fetching data for download ~~~~~~~~"
      download(days)
      logout
    else
      self.errors[:login] = "could not login"
    end
  end
  
  def sync_rets(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_rets()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

  def retrieve_rets(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Retrieving RETS ~~~~~~~~"
      parse_retrieve()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def append_rets(property)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      pull_rets(property)
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def sync_sale_price(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_sale_price()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

  # methods to be implemented by all market mls providers

  def login
    not_implemented
  end

  def logout
    not_implemented
  end

  def fetch_raw(days)
    not_implemented
  end

  def fetch_hotsheet(days)
    not_implemented
  end

  def parse_special_raw(data, filename)
    not_implemented
  end

  def parse_raw(data, filename)
    not_implemented
  end

  def parse_hotsheet(data, filename)
    not_implemented
  end

  def mls_url_for(attrs)
    not_implemented
  end

  def tax_url_for(attrs)
    not_implemented 
  end

  def deed_url_for(attrs)
    not_implemented
  end

  protected

    def parse(data, filename, opt = false)
      
      if opt
        parse_special_raw(data, filename)
      elsif filename.match /.tsv|txt$/i
        parse_raw(data, filename)
      elsif filename.match /.(jsp|html|asp)$/i
        Delayed::Worker.logger.debug "asp"
        parse_hotsheet(data, filename)
      elsif filename.match /.csv$/i
        parse_csv(data, filename)
      else 
        self.errors[filename] = "Unknown file extension"
      end
    end

    def parse_csv(data, filename)
      CSV.parse(data, headers: true) do |row|
        puts "zZZZZzzzzzzZZZ Look up to this #{row}"
        row = row.to_hash.with_indifferent_access
        save_property(row.to_hash.symbolize_keys)
      end
    end

    def fetch(days)
      p "-- fetching raw data"
      fetch_raw(days)
      p "-- fetching hotsheet"
      fetch_hotsheet(days-1)
    end
    
    def download(days)
      p "-- fetching download data"
      fetch_download(days)
    end

    def current_url
      @agent.page.uri.try(:to_s)
    end

    def missing_values
      %w{none n/a 0 unk unknown}
    end

    def self.lookup(mapping, property)
      proc {|d| value = d[property]; mapping[value] || value }
    end

    def self.lookup_type(mapping, property)
      
      proc {|d| 
        strs = []
        value = d[property]
        Delayed::Worker.logger.debug "~~ lookup type vaue: #{value}"
        if !value.nil?
          spls = value.split(",")
          
          # Delayed::Worker.logger.debug "~~ mapping view: #{mapping}"
          spls.each do |s|
            mapped = mapping.try(:[],s) rescue nil
            Delayed::Worker.logger.debug "~~ mapping try: #{mapped}"
            strs << mapped
          end
          Delayed::Worker.logger.debug "~~ string compact: #{strs}"
          strs.compact.join(", ")
        else
          nil
        end
      }
      
    end
    
    def self.lookup_source(mapping, values)
      
      
        strs = []
        value = values
        Delayed::Worker.logger.debug "~~ lookup type vaue: #{value}"
        if !value.nil?
          spls = value.split(",")
          
          # Delayed::Worker.logger.debug "~~ mapping view: #{mapping}"
          spls.each do |s|
            mapped = mapping.try(:[],s) rescue nil
            Delayed::Worker.logger.debug "~~ mapping try: #{mapped}"
            strs << mapped
          end
          Delayed::Worker.logger.debug "~~ string compact: #{strs}"
          strs.compact.join(", ")
        else
          nil
        end
      
      
    end
    
    def self.lookup_composite(mapping, property, subproperty)
      proc {|d| Delayed::Worker.logger.debug "property #{property} subproperty #{subproperty}" ; value = d.try(:[],property); Delayed::Worker.logger.debug "value #{value}"  ; real_value = if !d.try(:[],subproperty).nil? then mapping.try(:[],value.try(:downcase)).try(:[],d.try(:[],subproperty).try(:downcase)) else nil  end  ; real_value }
    end
    
    def self.lookup_category(mapping, property, subproperty)
       value = property.downcase
       Delayed::Worker.logger.debug "category mapping #{mapping} property #{property}  subproperty #{subproperty}" 
       real_value = if !subproperty.blank? then mapping.try(:[],value).try(:[],subproperty.downcase) else nil  end  
       real_value 
    end

    def self.lookup_or(*properties)
      proc {|d| properties.map{|property| d[property] unless d[property].blank? }.compact.first }
    end

    def self.lookup_percent(property)
      proc {|d| d[property] if d[property].try(:match, /%/) }
    end

    def self.lookup_nonpercent(property)
      proc {|d| d[property] unless d[property].try(:match, /%/) }
    end

    def self.hash_from_csv(id)
      result = HashWithCaseInsensitiveAccess.new
      CSV.foreach(Rails.root.join('app', 'providers', self.name.underscore, "#{id}.csv")) do |row|
        next if row.length != 2 
        for key in row.first.split('|')
          result[key] = row.last 
        end
      end
      result
    end
    def self.hash_composite(id)
      result = HashWithCaseInsensitiveAccess.new
      CSV.foreach(Rails.root.join('app', 'providers', self.name.underscore, "#{id}.csv")) do |row|
        
        sub_result = HashWithCaseInsensitiveAccess.new
        sub_result[row[1]] = row[2]
        result[row[0]] =  (result[row[0]].is_a?(Hash) ? result[row[0]] : {}).merge(sub_result)
        
      end
      result
    end


    def html_strip(object, header=false)
      if object.is_a?(Enumerable)
        object.map { |t| html_strip(t.text, header) }
      elsif object.is_a?(String)
        # trim both ends
        object.gsub!(/^[[:space:]|\s|'|"]*|[[:space:]|\s|'|"]*$/, '')
        object.gsub!(/[[:space:]|\s]+/, ' ') if header
        object
      end
    end

    def update_property(original, mapping, enums={})
      save_property(original, mapping, enums, true)
    end

    def save_property(original, mapping=nil, enums={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing #{attr} : #{new_value}"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "## class name #{attr} : #{string.class.name}"
          result[attr] =
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              nil 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (string.is_a?(BigDecimal))
              string
            else
              Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              Property.parse(attr, string)
            end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import(self.source,attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end

    def correct_geometry(original, mapping=nil, enums={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        original = original.to_hash.with_case_insensitive_access
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        attrs = raw.inject({}) do |result, (attr, string)|
          puts "~~~attr #{attr} string type #{string.class.name}"
          result[attr] =
            if  (string.class.name == "BigDecimal")  
              string
            elsif !(string.class.name == "BigDecimal") &&  string.try(:downcase).in?(missing_values)
              nil 
            elsif !(string.class.name == "BigDecimal") &&  enums.has_key?(attr)
              Provider.lookup(enums[attr], attr).call(raw)
            else
              Property.parse(attr, string)
            end
          result
        end
      else
        raw = {}
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      property = market.import_geometry(self.source, attrs, raw, update_only)
    
    end
    
     def correct_sale_price(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        original = original.to_hash.with_case_insensitive_access
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        attrs = raw.inject({}) do |result, (attr, string)|
          # puts "~~~attr #{attr} string type #{string.class.name}"
          result[attr] =
             if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :single_family_type
                  Provider.lookup_composite(categories[attr], attr, :category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              
              string
              
              
            else
               if attr == :sub_category
                nil
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
              
            end
          result
        end
      else
        raw = {}
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      property = market.import_sale_price(self.source, attrs, raw, update_only)
    
    end

    def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              string
            else
              Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              Property.parse(attr, string)
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import3(self.source, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end
    
    def correct_property(original, mapping=nil, enums={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        original = original.to_hash.with_case_insensitive_access
        
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        attrs = raw.inject({}) do |result, (attr, string)|
          result[attr] =
            if string.try(:downcase).in?(missing_values)
              nil 
            elsif enums.has_key?(attr)
              Provider.lookup(enums[attr], attr).call(raw)
            else
              if attr == :closed_date
                elems = string.split("/")
                mme = elems[0].to_s.rjust(2, '0')
                dde = elems[1].to_s.rjust(2, '0')
                yye = elems[2].length == 2 ? "20#{elems[2]}" : elems[2]
                dt_string = "#{mme}/#{dde}/#{yye}"
                Property.parse(attr, dt_string)
              else
                Property.parse(attr, string)
              end
            end
          result
        end
      else
        raw = {}
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      end
     
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      property = market.import2(self.source, attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      
      # self.errors[current_filename] ||= {}
      # if property.nil? || property.is_a?(String) 
        # self.errors[current_filename][info] = property || 'No match' unless update_only
      # else
        # self.results[current_filename] ||= 0
        # self.results[current_filename] += 1
        # self.ids << property.id
        # unless property.errors.empty?
          # self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        # end
      # end
    end

    def notify_listeners
      self.listeners.each {|listener| listener.call() }
    end

    def not_implemented
      raise NotImplementedError.new("You must implement this")
    end

end
