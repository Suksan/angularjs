class Mfrmls < Provider
  LONG_NAMES = ['HOA Info: HOA Amt','HOA Info: HOA Frequency','Cooling','Equipment/Appliances','Exterior Features','Floor',
              'Interior Features','Legal/Misc: Plat Book #','Legal/Misc: Plat Book Page','Lot Description',
              'Schools: Elementary School','Schools: High School','Schools: Middle School','Lease Terms','Roof','Style',
              'Construction','Legal/Misc: Gated Community','Parking: Total Parking Spaces','Road Surface','Parking']

  LOOKUP_NAMES = [['Cooling','cooling_system_type'],['City','city'],['County','county'],
                ['Equipment/Appliances','equipment'],
                ['Exterior Features','exterior_description'],
                ['Floor','floor_type'],
                ['HOA Info: HOA Frequency','hoa_bill_freq_options'],
                ['HOA Home Owners','hoa_membership'],
                ['Interior Features','interior_description'],
                ['Lot Description','lot_description'],
                ['Schools: Elementary School','mls_elementary_school'],
                ['Schools: High School','mls_high_school'],
                ['Schools: Middle School','mls_middle_school'],
                ['To Be Constructed','new_construction'],
                ['Area','mls_area_options'],
                ['Pool - Private','pool_private'],
                ['Prop Type','prop_type_options'],
                ['Res Sub-Type','res_sub_type_options'],
                ['Lease Terms','rental_terms'],
                ['Roof','roof_type'],
                ['Appt','showing_instructions'],
                ['State/Province','state'],
                ['Street Dir','street_direction'],
                ['Street Suffix','street_suffix_options'],
                ['Style','street_surface_type'],
                ['Construction','new_style_of_building'],
                ['Status','status_options'],
                ['Subdivision/Condo Name','subdivision'],
                ['Waterfront','waterfront'],
                ['Zip Code','zip_code'],
                ['Legal/Misc: Gated Community','gated_community'],
                ['Road Surface','street_surface_type'],
                ['Parking','garage_attach_options'],
                ['Parking','garage_capacity_options']
              ]
  KLASSES = ['A','B','C','D','F','G']

  FLEX_MLS_AREA = {
    "153Y10TSDCOG" => {"key" => "381", "name" => "Merritt Island S of Pineda"},
    "153Y10SQF0DI" => {"key" => "355", "name" => "Barefoot Bay/Micco"},
    "153Y10S3WIWX" => {"key" => "350", "name" => "Southern Palm Bay"},
    "153Y10R1OXNJ" => {"key" => "347", "name" => "SW Palm Bay"},
    "153Y10QBYB43" => {"key" => "345", "name" => "NW Palm Bay"},
    "153Y10P7EITR" => {"key" => "344", "name" => "SE Palm Bay"},
    "153Y0ZZX2YD5" => {"key" => "343", "name" => "Malabar/Grant-Valkaria"},
    "153Y0ZZ293DV" => {"key" => "342", "name" => "NE Palm Bay"},
    "153Y0ZWZ7DRX" => {"key" => "341", "name" => "NE Palm Bay"},
    "153Y0ZWR32QN" => {"key" => "340", "name" => "West Melbourne"},
    "153Y0ZWKVD70" => {"key" => "331", "name" => "Melbourne"},
    "153Y0ZVOSK17" => {"key" => "330", "name" => "Eau Gallie"},
    "153Y0ZVH2DR9" => {"key" => "323", "name" => "Palm Shores"},
    "153Y0ZV8EUUK" => {"key" => "322", "name" => "Lake Washington/S of Post"},
    "153Y0ZTM7ZRU" => {"key" => "321", "name" => "Pineda/Lake Washington"},
    "153Y0ZT6RRZU" => {"key" => "320", "name" => "Cocoa Beach"},
    "153Y0ZRQPGQ1" => {"key" => "272", "name" => "Cape Canaveral"},
    "153Y0ZQQ47T1" => {"key" => "271", "name" => "Port Canaveral"},
    "153Y0ZQCLW6N" => {"key" => "270", "name" => "Newfound Harbor"},
    "153Y0ZQ4A0IL" => {"key" => "254", "name" => "S Merritt Island"},
    "153Y0ZP5VQDJ" => {"key" => "253", "name" => "N Banana River Dr."},
    "153Y0Z59T1G9" => {"key" => "252", "name" => "Central Merritt Island"},
    "153Y0Z4UQWNX" => {"key" => "251", "name" => "N Merritt Island"},
    "153Y0Z3M7E2U" => {"key" => "250", "name" => "Suntree S of Wickham"},
    "153Y0Z3CRCNH" => {"key" => "218", "name" => "Viera West of I 95"},
    "153Y0Z339GZZ" => {"key" => "217", "name" => "Viera/Suntree N of Wickham"},
    "153Y0X7TIRR5" => {"key" => "216", "name" => "West Cocoa"},
    "153Y0X783W2O" => {"key" => "215", "name" => "Rockledge"},
    "153Y0X705PTD" => {"key" => "214", "name" => "Mainland E of US 1"},
    "153Y0X63N9M6" => {"key" => "213", "name" => "Cocoa - West of US 1"},
    "153Y0X5N08PE" => {"key" => "212", "name" => "Canaveral Groves"},
    "1AMW3CDDDQIV" => {"key" => "211", "name" => "Cocoa West of I 95"},
    "153Y0X4BS6N5" => {"key" => "210", "name" => "Port St. John"},
    "153Y0X3ZO3O5" => {"key" => "107", "name" => "Titusville W I95 S 46 to Fox Lake"},
    "1ARXWE2YUWOF" => {"key" => "105", "name" => "Titusville SR50 - Kings H"},
    "153Y0X3QIBRI" => {"key" => "104", "name" => "Titusville Garden - SR50"},
    "153Y0X2J8IXZ" => {"key" => "103", "name" => "Mims/Tville SR46 - Garden"},
    "153Y0X23YBQN" => {"key" => "102", "name" => "Mims/Scottsmoor"},
    "153Y0X0Y49N9" => {"key" => "101", "name" => "Out of Area"},
    "153Y10X0ZDBN" => {"key" => "999", "name" => "Miami-Dade"},
    "1592C5K1AG8P" => {"key" => "907", "name" => "St Lucie County"},
    "158XDN5JSBZG" => {"key" => "906", "name" => "Seminole"},
    "153Y10W6SFS7" => {"key" => "905", "name" => "Indian River"},
    "153Y10VYX9I8" => {"key" => "904", "name" => "Osceola"},
    "153Y10VTP6S9" => {"key" => "903", "name" => "Orange"},
    "153Y10VM88OI" => {"key" => "902", "name" => "Volusia"},
    "153Y10VGXINT" => {"key" => "901", "name" => "South Beaches"},
    "153Y10V9NDUU" => {"key" => "385", "name" => "Indialantic/Melbourne Beach"},
    "157X8AMG4MP1" => {"key" => "384", "name" => "N Indialantic"},
    "153Y10UD3MQM" => {"key" => "383", "name" => "Satellite Bch/Indian Harbour Bch"},
    "157X8AL9RDWB" => {"key" => "382", "name" => ""}
  }
  INTF = {
    "Attic" => "Attic",
    "AtticFan" => "Attic Fan",
    "AtticVentilator" => "Attic Ventilator",
    "BlindsSh" => "Blinds/Shades",
    "BreakfastBar" => "Breakfast Bar",
    "BuiltinFeatures" => "Built in Features",
    "CathedralVaultedCeiling" => "Cathedral/Vaulted Ceiling",
    "CeilingFans" => "Ceiling Fan(S)",
    "CentralVacuum" => "Central Vacuum",
    "CorridorAccess" => "Corridor Access",
    "CrownMolding" => "Crown Molding",
    "DryBar" => "Dry Bar",
    "DumbWaiter" => "Dumb Waiter",
    "Elevator" => "Elevator",
    "FireSprinkler" => "Fire Sprinkler",
    "Furnished" => "Furnished",
    "FurnishingOpt" => "Furnishing-Optional",
    "HandicappedModified" => "Handicapped Modified",
    "HotTubSpa" => "Hot Tub/Spa",
    "InsideUtility" => "Inside Utility",
    "IntercomSystem" => "Intercom System",
    "InWallPestSystem" => "In Wall Pest System",
    "MedicalAlarm" => "Medical Alarm",
    "Other" => "Other",
    "PestGuardSys" => "Pest Guard System",
    "RadiantBarrier" => "Radiant Barrier",
    "Rods" => "Rods",
    "Sauna" => "Sauna",
    "Skylights" => "Skylights",
    "SmokeAlarms" => "Smoke Alarm(S)",
    "SolidStoneCounters" => "Solid Stone Counters",
    "SolidSurfaceCounters" => "Solid Surface Counters",
    "SolidWoodCabinets" => "Solid Wood Cabinets",
    "StorageRoom" => "Storage Room",
    "ThermalWindows" => "Thermal Windows",
    "ThermoAttcFan" => "Thermostat Attic Fan",
    "Thermostat" => "Thermostat",
    "TrayCeiling" => "Tray Ceiling",
    "TVAntenna" => "Tv Antenna",
    "Unfurnished" => "Unfurnished",
    "VideoMonitoring" => "VideoMonitoring",
    "VolumeCeilings" => "Volume Ceilings",
    "WalkInCloset" => "Walk In Closet",
    "WasherDryerHookup" => "Washer/Dryer Hookup",
    "WaterSoftener" => "Water Softener",
    "WetBar" => "Wet Bar",
    "WetBarWhole" => "Wet Bar Whole",
    "WheelchairAccessible" => "Wheelchair Accessible",
    "WholeHouseROSystem" => "Whole House R.O. System",
    "WndwTreatment" => "Window Treatment"
  } 

  INTL = {
    "BonusRoom" => "Bonus Room",
    "BreakfastRoomSeparate" => "Breakfast Room Separate",
    "EatingSpaceinKitchen" => "Eating Space In Kitchen",
    "FamilyRoom" => "Family Room",
    "FloridaRoom" => "Florida Room",
    "FormalDiningRoomSeparate" => "Formal Dining Room Separate",
    "FormalLivingRoomSeparate" => "Formal Living Room Separate",
    "GreatRoom" => "Great Room",
    "KitchenFamilyRoomCombo" => "Kitchen/Family Room Combo",
    "LDining" => "L Dining",
    "LivingRoomDiningRoomCombo" => "Living Room/Dining Room Combo",
    "LivingRoomGreatRoom" => "Living Room/Great Room",
    "MasterBedroomDownstairs" => "Master Bedroom Downstairs",
    "OfficeDenLibrary" => "Office Den Library",
    "OpenFloorPlan" => "Open Floor Plan",
    "SplitBedroom" => "Split Bedroom",
    "VolumeCeilings" => "Volume Ceilings",
    "WindowTreatment" => "Window Treatment"
  }

  STOR = {
      "1Level" => "1",
    "1stFloorMultiStory" => "",
    "2Levels" => "2",
    "2ndFloorMultiStory" => "",
    "3Levels" => "3",
    "3rdFloorPlusaboveMultiStory" => "",
    "4PlusLevels" => "4",
    "Attached" => "",
    "Detached" => "",
    "Efficiency" => "",
    "Elevated" => "",
    "EndUnit" => "",
    "FourStory" => "4",
    "GroundFloorUnit" => "",
    "GroundLevel" => "",
    "HighRise" => "",
    "InMHCommunity" => "",
    "MidRise" => "",
    "OneStory" => "1",
    "OutofMHCommunity" => "",
    "Penthouse" => "",
    "SplitLevel" => "2",
    "ThreeStory" => "3",
    "Townhouse23Floors" => "3",
    "TriLevel" => "3",
    "TwoStory" => "2"
  },

  SFT = {
    "12Duplex" => "duplex",
    "ManufacturedMobileHome" => "manufactured",
    "SingleFamilyHome" => "freestanding",
    "Villa" => "patio"
  }

  RNTC = {
    "12Duplex" => "single_family",
    "1stFloorMultiStory" => "multi_family",
    "2ndFloorMultiStory" => "multi_family",
    "3rdFlPlusaboveMultiStory" => "multi_family",
    "Apartment" => "multi_family",
    "Condo"  =>  "townhouse_condo",
    "CondoHotel" => "multi_family",
    "Coop" => "townhouse_condo",
    "Efficiency" => "single_family",
    "Fourplex" => "multi_family",
    "GarageApt" => "single_family",
    "ManufacturedMobileHome" => "mobile_home",
    "Modular" => "manufactured",
    "SingleFamilyHome" => "single_family",
    "StiltHome" => "single_family",
    "Townhouse" => "townhouse_condo",
    "TriLevel" => "townhouse_condo",
    "Triplex" => "multi_family",
    "Villa" => "single_family"
  }

RNTT = {
  "12Duplex" => "duplex",
  "1stFloorMultiStory" => "apartment",
  "2ndFloorMultiStory" => "apartment",
  "3rdFlPlusaboveMultiStory" => "apartment",
  "Apartment" => "apartment",
  "Condo"     =>  "condo",
  "CondoHotel" => "condo",
  "Coop" => "condo",
  "Efficiency" => "other",
  "Fourplex" => "fourplex",
  "GarageApt" => "apartment",
  "ManufacturedMobileHome" => "",
  "Modular" => "",
  "SingleFamilyHome" => "freestanding",
  "StiltHome" => "freestanding",
  "Townhouse" => "town_house",
  "TriLevel" => "town_house",
  "Triplex" => "triplex",
  "Villa" => "patio_home",
}

StreetSuffix = {
  "ALY" => "ALLEY",
  "ANX" => "ANNEX",
  "ARC" => "ARCADE",
  "AVE" => "AVENUE",
  "BCH" => "BEACH",
  "BG" => "BURG",
  "BGS" => "BURGS",
  "BLF" => "BLUFF",
  "BLFS" => "BLUFFS",
  "BLVD" => "BOULEVARD",
  "BND" => "BEND",
  "BR" => "BRANCH",
  "BRG" => "BRIDGE",
  "BRK" => "BROOK",
  "BRKS" => "BROOKS",
  "BTM" => "BOTTOM",
  "BYP" => "BYPASS",
  "BYU" => "BAYOO",
  "CIR" => "CIRCLE",
  "CIRS" => "CIRCLES",
  "CLB" => "CLUB",
  "CLF" => "CLIFF",
  "CLFS" => "CLIFFS",
  "CMN" => "COMMON",
  "COR" => "CORNER",
  "CORS" => "CORNERS",
  "CP" => "CAMP",
  "CPE" => "CAPE",
  "CRES" => "CRESCENT",
  "CRK" => "CREEK",
  "CRSE" => "COURSE",
  "CRST" => "CREST",
  "CSWY" => "CAUSEWAY",
  "CT" => "COURT",
  "CTR" => "CENTER",
  "CTRS" => "CENTERS",
  "CTS" => "COURTS",
  "CURV" => "CURVE",
  "CV" => "COVE",
  "CVS" => "COVES",
  "CYN" => "CANYON",
  "DL" => "DALE",
  "DM" => "DAM",
  "DR" => "DRIVE",
  "DRS" => "DRIVES",
  "DV" => "DIVIDE",
  "EST" => "ESTATE",
  "ESTS" => "ESTATES",
  "EXPY" => "EXPRESSWAY",
  "EXT" => "EXTENSION",
  "EXTS" => "EXTENSIONS",
  "FALL" => "FALL",
  "FLD" => "FIELD",
  "FLDS" => "FIELDS",
  "FLS" => "FALLS",
  "FLT" => "FLAT",
  "FLTS" => "FLATS",
  "FRD" => "FORD",
  "FRDS" => "FORDS",
  "FRG" => "FORGE",
  "FRGS" => "FORGES",
  "FRK" => "FORK",
  "FRKS" => "FORKS",
  "FRST" => "FOREST",
  "FRY" => "FERRY",
  "FT" => "FORT",
  "FWY" => "FREEWAY",
  "GDN" => "GARDEN",
  "GDNS" => "GARDENS",
  "GLN" => "GLEN",
  "GLNS" => "GLENS",
  "GRN" => "GREEN",
  "GRNS" => "GREENS",
  "GRV" => "GROVE",
  "GRVS" => "GROVES",
  "GTWY" => "GATEWAY",
  "HBR" => "HARBOR",
  "HBRS" => "HARBORS",
  "HL" => "HILL",
  "HLS" => "HILLS",
  "HOLW" => "HOLLOW",
  "HTS" => "HEIGHTS",
  "HVN" => "HAVEN",
  "HWY" => "HIGHWAY",
  "INLT" => "INLET",
  "IS" => "ISLAND",
  "ISLE" => "ISLE",
  "ISS" => "ISLANDS",
  "JCT" => "JUNCTION",
  "JCTS" => "JUNCTIONS",
  "KNL" => "KNOLL",
  "KNLS" => "KNOLLS",
  "KY" => "KEY",
  "KYS" => "KEYS",
  "LAND" => "LAND",
  "LCK" => "LOCK",
  "LCKS" => "LOCKS",
  "LDG" => "LODGE",
  "LF" => "LOAF",
  "LGT" => "LIGHT",
  "LGTS" => "LIGHTS",
  "LK" => "LAKE",
  "LKS" => "LAKES",
  "LN" => "LANE",
  "LNDG" => "LANDING",
  "LOOP" => "LOOP",
  "MALL" => "MALL",
  "MDW" => "MEADOW",
  "MDWS" => "MEADOWS",
  "MEWS" => "MEWS",
  "ML" => "MILL",
  "MLS" => "MILLS",
  "MNR" => "MANOR",
  "MNRS" => "MANORS",
  "MSN" => "MISSION",
  "MT" => "MOUNT",
  "MTN" => "MOUNTAIN",
  "MTNS" => "MOUNTAINS",
  "MTWY" => "MOTORWAY",
  "NCK" => "NECK",
  "OPAS" => "OVERPASS",
  "ORCH" => "ORCHARD",
  "OVAL" => "OVAL",
  "PARK" => "PARK",
  "PARKS" => "PARKS",
  "PASS" => "PASS",
  "PATH" => "PATH",
  "PIKE" => "PIKE",
  "PKWY" => "PARKWAY",
  "PKWYS" => "PARKWAYS",
  "PL" => "PLACE",
  "PLN" => "PLAIN",
  "PLNS" => "PLAINS",
  "PLZ" => "PLAZA",
  "PNE" => "PINE",
  "PNES" => "PINES",
  "PR" => "PRAIRIE",
  "PRT" => "PORT",
  "PRTS" => "PORTS",
  "PSGE" => "PASSAGE",
  "PT" => "POINT",
  "PTS" => "POINTS",
  "RADL" => "RADIAL",
  "RAMP" => "RAMP",
  "RD" => "ROAD",
  "RDG" => "RIDGE",
  "RDGS" => "RIDGES",
  "RDS" => "ROADS",
  "RIV" => "RIVER",
  "RNCH" => "RANCH",
  "ROW" => "ROW",
  "RPD" => "RAPID",
  "RPDS" => "RAPIDS",
  "RST" => "REST",
  "RTE" => "ROUTE",
  "RUE" => "RUE",
  "RUN" => "RUN",
  "SHL" => "SHOAL",
  "SHLS" => "SHOALS",
  "SHR" => "SHORE",
  "SHRS" => "SHORES",
  "SKWY" => "SKYWAY",
  "SMT" => "SUMMIT",
  "SPG" => "SPRING",
  "SPGS" => "SPRINGS",
  "SPUR" => "SPUR",
  "SPURS" => "SPURS",
  "SQ" => "SQUARE",
  "SQS" => "SQUARES",
  "ST" => "STREET",
  "STA" => "STATION",
  "STRA" => "STRAVENUE",
  "STRM" => "STREAM",
  "STS" => "STREETS",
  "TER" => "TERRACE",
  "TPKE" => "TURNPIKE",
  "TRAK" => "TRACK",
  "TRCE" => "TRACE",
  "TRFY" => "TRAFFICWAY",
  "TRL" => "TRAIL",
  "TRWY" => "THROUGHWAY",
  "TUNL" => "TUNNEL",
  "UN" => "UNION",
  "UNS" => "UNIONS",
  "UPAS" => "UNDERPASS",
  "VIA" => "VIADUCT",
  "VIS" => "VISTA",
  "VL" => "VILLE",
  "VLG" => "VILLAGE",
  "VLGS" => "VILLAGES",
  "VLY" => "VALLEY",
  "VLYS" => "VALLEYS",
  "VW" => "VIEW",
  "VWS" => "VIEWS",
  "WALK" => "WALK",
  "WALKS" => "WALKS",
  "WALL" => "WALL",
  "WAY" => "WAY",
  "WAYS" => "WAYS",
  "WL" => "WELL",
  "WLS" => "WELLS",
  "XING" => "CROSSING",
  "XRD" => "CROSSROAD"
}

LOOKUP_TYPE_ENUMS = {



cooling_system_type: {
    "ACOfficeOnly" => "A/C - Office Only",
    "Central" => "Central",
    "Humidistat" => "Humidistat",
    "NoAir" => "No Air",
    "WallUnitsWindow" => "Wall Units/Window",
    "ZonedMultiple" => "Zoned/Multiple"
  },

county: {
  "Alachua" => "Alachua",
  "Baker" => "Baker",
  "Bay" => "Bay",
  "Bradford" => "Bradford",
  "Brevard" => "Brevard",
  "Broward" => "Broward",
  "Calhoun" => "Calhoun",
  "Charlotte" => "Charlotte",
  "Citrus" => "Citrus",
  "Clay" => "Clay",
  "Collier" => "Collier",
  "Columbia" => "Columbia",
  "Dade" => "Dade",
  "DeSoto" => "DeSoto",
  "Dixie" => "Dixie",
  "Duval" => "Duval",
  "Escambia" => "Escambia",
  "Flagler" => "Flagler",
  "Franklin" => "Franklin",
  "Gadsen" => "Gadsden",
  "Gilchrist" => "Gilchrist",
  "Glades" => "Glades",
  "Gulf" => "Gulf",
  "Hamilton" => "Hamilton",
  "Hardee" => "Hardee",
  "Hendry" => "Hendry",
  "Hernando" => "Hernando",
  "Highlands" => "Highlands",
  "Hillsborough" => "Hillsborough",
  "Holmes" => "Holmes",
  "IndianRiver" => "Indian River",
  "Jackson" => "Jackson",
  "Jefferson" => "Jefferson",
  "Lafayette" => "Lafayette",
  "Lake" => "Lake",
  "Lee" => "Lee",
  "Leon" => "Leon",
  "Levy" => "Levy",
  "Liberty" => "Liberty",
  "Madison" => "Madison",
  "Manatee" => "Manatee",
  "Marion" => "Marion",
  "Monroe" => "Monroe",
  "Nassau" => "Nassau",
  "Okaloosa" => "Okaloosa",
  "Okeechobee" => "Okeechobee",
  "Orange" => "Orange",
  "Osceola" => "Osceola",
  "Other" => "Other County",
  "OutofArea" => "Out of Area",
  "PalmBeach" => "Palm Beach",
  "Pasco" => "Pasco",
  "Pinellas" => "Pinellas",
  "Polk" => "Polk",
  "Putnam" => "Putnam",
  "Rosa" => "Santa Rosa",
  "Sarasota" => "Sarasota",
  "Seminole" => "Seminole",
  "StJohns" => "St Johns",
  "StLucie" => "St Lucie",
  "Sumter" => "Sumter",
  "Suwannee" => "Suwannee",
  "Taylor" => "Taylor",
  "Union" => "Union",
  "Volusia" => "Volusia",
  "Wakulla" => "Wakulla",
  "Walton" => "Walton",
  "Washington" => "Washington"
},

disclosures: {
  "Auction" => "Auction",
  "AuctionREO" =>  "AuctionREO",
  "AuctionShortSale" =>  "AuctionShortSale",
  "None" =>  "None",
  "REOBankOwned" =>  "REOBankOwned",
  "ShortSale" => "ShortSale"
},

equipment: {
    "BuiltInOven" => "Built In Oven",
    "Compactor" => "Compactor",
    "ConvectionOven" => "Convection Oven",
    "CookTopOnly" => "Cook Top Only",
    "Dishwasher" => "Dishwasher",
    "Disposal" => "Disposal",
    "Dryer" => "Dryer",
    "ExhaustFan" => "Exhaust Fan",
    "Freezer" => "Freezer",
    "GasAppliances" => "Gas Appliances",
    "HotWaterElectric" => "Hot Water Electric",
    "HotWaterGas" => "Hot Water Gas",
    "IceMaker" => "Ice Maker",
    "IndoorGrill" => "Indoor Grill",
    "KitchenReverseOsmos" => "Kitchen Reverse Osmosis Sys",
    "Microwave" => "Microwave",
    "MicrowaveHood" => "Microwave Hood",
    "None" => "None",
    "Other" => "Other",
    "Oven" => "Oven",
    "OvenBuiltIn" => "Oven - Built In",
    "OvenDouble" => "Oven - Double",
    "Range" => "Range",
    "RangeHood" => "Range Hood",
    "Refrigerator" => "Refrigerator",
    "SolarHotWater" => "Solar Hot Water",
    "SolarHotWaterOwned" => "Solar Hot Water Owned",
    "SolarHotWaterRented" => "Solar Hot Water Rented",
    "TanklessWater" => "Tankless Water Heater",
    "TanklessWaterHeater" => "Tankless Water Heater",
    "Washer" => "Washer",
    "WaterAeratorOwned" => "Water Aerator Owned",
    "WaterAeratorRented" => "Water Aerator Rented",
    "WaterFIlterOwned" => "Water FIlter Owned",
    "WaterFilterRented" => "Water Filter Rented",
    "WaterSoftener" => "Water Softener",
    "WaterSoftenerOwned" => "Water Softener Owned",
    "WaterSoftenerRented" => "Water Softener Rented",
    "WaterTreatment" => "Water Treatment System",
    "WatrFiltration" => "Water Filtration System",
    "WineBarRefrigerator" => "Wine/Bar Refrigerator"
  },

  exterior_description: {
    "BalconySunDeck" => "Balcony/Sun Deck",
    "Barn" => "Barn",
    "CoveredParking" => "Covered Parking",
    "DetachedInLawApt" => "Detached In-Law Apt",
    "DetachedWorkshop" => "Detached Workshop",
    "DogRun" => "Dog Run",
    "Fenced" => "Fenced",
    "FrenchDoors" => "French Doors",
    "FruitTrees" => "Fruit Tree",
    "Gazebo" => "Gazeb",
    "Greenhouse" => "Greenhous",
    "GuttersDownspouts" => "Gutters / Downspout",
    "HandicapModified" => "Handicap Modifie",
    "HotTubSpa" => "Hot Tub/Spa",
    "HurricaneShutters" => "Hurricane Shutters",
    "InLawApt" => "In-Law Apt",
    "IrrigationSystem" => "Irrigation System",
    "MatureLandscaping" => "Mature Landscaping",
    "OakTrees" => "Oak Trees",
    "OpenPatio" => "Open Patio",
    "Other" => "Other",
    "OutdoorGrill" => "Outdoor Grill",
    "OutdoorKitchen" => "Outdoor Kitchen",
    "OutdoorLights" => "Outdoor Lights",
    "OutdoorShower" => "Outdoor Shower",
    "ParkingRVBoat" => "Parking - Rv/Boat",
    "PatioDeckCovered" => "Patio/Deck - Covered",
    "PatioDeckOpen" => "Patio/Deck - Open",
    "PatioDeckScreened" => "Patio/Deck - Screened",
    "PatioPorchDeckCov" => "Patio/Porch/Deck Covered",
    "PatioPorchDeckOpen" => "Patio/Porch/Deck Open",
    "PatioPorchDeckScr" => "Patio/Porch/Deck Screened",
    "PorchPatio" => "Porch/Patio",
    "RentalApartment" => "Rental Apartment",
    "SatelliteDish" => "Satellite Dish",
    "Sauna" => "Sauna",
    "ScreenCoveredEncl" => "Screen/Covered Enclosure",
    "Sidewalk" => "Sidewalk",
    "SlidingDoors" => "Sliding Doors",
    "SprinklerMetered" => "Sprinkler Metered",
    "Stables" => "Stables",
    "Storage" => "Storage",
    "TennisCourtsPrivate" => "Tennis Courts - Private",
    "TreesLandscaped" => "Trees/Landscaped",
    "UtilityShed" => "Utility Shed",
    "WDHookup" => "Washer/Dryer Hookup",
    "WheelchairAccessible" => "Wheelchair Accessible",
    "Xeriscape" => "Xeriscape"
  },

  fence_type: {
    "ChainLink" => "Chain Link",
    "Cross" => "Cross",
    "Hedge" => "Hedge"
  },

  financing_available: {
    "AssumableMustQualify" => "Assumable - Must Qualify",
  "AssumableNonQualify" => "Assumable - Non Qualify",
  "Cash" => "Cash",
  "Conventional" => " Conventional",
  "EnergyEfficientMortg" => "Energy Efficient Mortgage",
  "ExchangeTrade" => "Exchange/Trade",
  "FHA" => "FHA",
  "FloodInsuranceRequired" => "Flood Insurance Required",
  "GreenFinancingAvail" => "Green Financing Available",
  "LeaseOption" => "Lease Option",
  "LeasePurchase" => "Lease Purchase",
  "Other" => "Other",
  "OwnerPMM" => "Owner/PMM",
  "SellerFinancing" => "Seller Financing",
  "SpecialFunding" => "Special Funding",
  "USDA" => "USDA",
  "VA" => "VA"
  },

  floor_type: {
    "Bamboo" => "Bamboo",
  "BrickStone" => "Brick/Stone",
  "Carpet" => "Carpet",
  "Carpeted" => "Carpeted",
  "CeramicTile" => "Ceramic Tile",
  "Concrete" => "Concrete",
  "Cork" => "Cork",
  "Epoxy" => "Epoxy",
  "FSC" => "Forestry Stewardship Certified",
  "Laminate" => "Laminate",
  "Marble" => "Marble",
  "Marmoleum" => "Marmoleum",
  "Other" => "Other",
  "Parquet" => "Parquet",
  "PorcelainTile" => "Porcelain Tile",
  "QuarryTile" => "Quarry Tile",
  "ReclaimedWood" => "Reclaimed Wood",
  "RecycleComp" => "Recycled/Composite Flooring",
  "SalvagedFlooring" => "Salvaging Flooring",
  "Slate" => "Slate",
  "Terrazzo" => "Terrazzo",
  "Tile" => "Tile",
  "Travertine" => "Travertine",
  "Vinyl" => "Vinyl",
  "Wood" => "Wood",
  "WoodLaminate" => "Wood Laminate"
  },

  foundation_type: {
    "Basement" => "Basement",
  "Crawlspace" => "Crawlspace",
  "Slab" => "Slab",
  "StemWall" => "Stem Wall",
  "StiltWood" => "Stilt / Wood Piling"
  },

  heating_system: {
    "Central" => "Central",
  "CentralBuilding" => "Central Building",
  "CentralIndividual" => "Central Individual",
  "ExhaustFans" => "Exhaust Fans",
  "FuelElectric" => "Fuel - Electric",
  "FuelGasBottled" => "Fuel - Gas Bottled",
  "FuelGasNatural" => "Fuel - GasNatural",
  "FuelOil" => "Fuel - Oil",
  "HeatPump" => "Heat Pump",
  "HeatRecoveryUnit" => "Heat Recovery Unit",
  "NoHeat" => "No Heat",
  "Other" => "Other",
  "Partial" => "Partial",
  "RadiantBaseboards" => "Radiant / Baseboards",
  "RadiantCeiling" => "Radiant / Ceiling",
  "ReverseCycle" => "Reverse Cycle",
  "Solar" => "Solar",
  "SpaceHeater" => "Space Heater",
  "WallFurnace" => "Wall Furnace",
  "WallUnitsWindowUnits" => "Wall Units / Window Units",
  "ZonedMultiple" => "Zoned/Multiple"
  },

  hoa_membership:{
    "None" => 'None',
    "Optional" => 'Optional',
    "Required" => 'Required'
  },

  lot_description: {
    "ActivityCoreCenter" => "Activity Core/Center",
  "Airport" => "Airport",
  "Balcony" => "Balcony",
  "BeachArea" => "Beach Area",
  "BillboardLease" => "Billboard Lease",
  "Brownfield" => "Brownfield",
  "CCCL" => "Coastal Construction Control Line",
  "CDD" => "CDD",
  "CentralBusinessDistrict" => "Central Business District",
  "ClosetoBusLine" => "Close to Bus Line",
  "CoastalConstrCtrlLine" => "Coastal Construction Control Line",
  "CommercePark" => "Commerce Park",
  "ConservationArea" => "Conservation Area",
  "CornerLot" => "Corner Lot",
  "CornerLotUnit" => "Corner Lot/Unit",
  "CornerUnit" => "Corner Unit",
  "Culdesac" => "Cul-de-sac",
  "DrainageCanal" => "Drainage Canal",
  "EndUnit" => "End Unit",
  "EnvRestrArea" => "Env Restr Area",
  "Expressway" => "Expressway",
  "FloodInsuranceRequired" => "Flood Insurance Required",
  "FloodPlain" => "Flood Plain",
  "FloodZone" => "Flood Zone",
  "GardenView" => "Garden View",
  "GatedCommunity" => "Gated Community",
  "GolfCourseFrontage" => "Golf Course Frontage",
  "GolfCourseView" => "Golf Course View",
  "Greenbelt" => "Greenbelt",
  "GreenbeltView" => "Greenbelt View",
  "HighwayFrontage" => "Highway Frontage",
  "Hilly" => "Hilly",
  "HistoricDistrict" => "Historic District",
  "HuntingLease" => "Hunting Lease",
  "HwyFrontage" => "Highway Frontage",
  "InCityLimits" => "In City Limits",
  "InCounty" => "In County",
  "IndustrialCondo" => "Industrial Condo",
  "IndustrialPark" => "Industrial Park",
  "InsideLot" => "Inside Lot",
  "IrregularLot" => "Irregular Lot",
  "Level" => "Level",
  "LotFlag" => "Lot - Flag",
  "LotIrregular" => "Lot - Irregular",
  "LotKey" => " Lot - Key",
  "LotOversized" => "Lot - Oversized",
  "LotTip" => "Lot - Tip",
  "NearAirport" => "Near Airport",
  "NearGolfCourse" => "Near Golf Course",
  "NearRailroadSiding" => "Near Railroad Siding",
  "Neighborhood" => "Neighborhood",
  "OceanView" => "Ocean View",
  "OnBusLine" => "On Bus Line",
  "Other" => "Other",
  "OutParcel" => "Out Parcel",
  "OversizedLot" => "Oversized Lot",
  "ParkView" => "Park View",
  "PartiallyWooded" => "Partially Wooded",
  "PastureAgricultural" => "Pasture / Agricultural",
  "PoolView" => "Pool View",
  "PreserveArea" => "Preserve Area",
  "Railroad" => "Railroad",
  "ReclaimedLand" => "Reclaimed Land",
  "RetailCondo" => "Retail Condo",
  "RiparianRights" => "Riparian Rights",
  "RoomForPool" => "Room For Pool",
  "Rural" => "Rural",
  "RuralServiceArea" => "Rural Service Area",
  "Seaport" => "Seaport",
  "ShoppingCenter" => "Shopping Center",
  "Sidewalk" => "Sidewalk",
  "SpecialTaxingDistrict" => "Special Taxing District",
  "StreetBrick" => "Street Brick",
  "StreetDeadEnd" => "Street Dead-End",
  "StreetDeadEnds" => "Street Dead-Ends",
  "StreetOneWay" => "Street One Way",
  "StreetPaved" => "Street Paved",
  "StreetPrivate" => "Street Private",
  "StreetUnpaved" => "Street Unpaved",
  "StripCenter" => "Strip Center",
  "Suburb" => "Suburb",
  "TennisCtView" => "Tennis Court View",
  "TransCellTowerLse" => "Trans/Cell Tower Lse",
  "TurnAround" => "Turn Around",
  "Undeveloped" => "Undeveloped",
  "Unincorporated" => "Unincorporated",
  "UrbanServiceArea" => "Urban Service Area",
  "Waterfront" => "Waterfront",
  "Wetlands" => "Wetlands",
  "WildlifeSanctuary" => "Wildlife Sanctuary",
  "Wooded" => "Wooded",
  "WoodedView" => "Wooded View",
  "ZonedforHorses" => "Zoned for Horses",
  "ZoningPermitsHorses" => "Zoning Permits Horses"
  },

  lot_use: {
    "INDUSTRIAL" => "INDUSTRIAL",
    "MULTIFAMILY" => "MULTIFAMILY",
    "OFFICE" => "OFFICE",
    "RETAILCOMMERCIAL" => "RETAIL COMMERCIAL",
    "VACANTLAND" => "VACANT LAND"
  },

  mls_area_name: {
    "Alachua" => "32615",
  "Alachua" => "32616",
  "Alford" => "32420",
  "Altamonte Springs" => "32715",
  "Altamonte Springs" => "32716",
  "Altamonte Springs East" => "32701",
  "Altamonte Springs West/Forest City" => "32714",
  "Altha" => "32421",
  "Altoona" => "32702",
  "Alturas" => "33820",
  "Alva" => "33920",
  "Anna Maria" => "34216",
  "Anthony" => "32617",
  "Apalachicola" => "32320",
  "APO, AA (Military)" => "34020",
  "APO, AA (Military)" => "34024",
  "APO, AA (Military)" => "34035",
  "Apollo Beach / Ruskin" => "33572",
  "Apopka" => "32703",
  "Apopka" => "32704",
  "Apopka" => "32712",
  "Arcadia" => "34265",
  "Arcadia" => "34266",
  "Arcadia" => "34269",
  "Archer" => "32618",
  "Aripeka" => "34679",
  "Astatula" => "34705",
  "Astor" => "32102",
  "Atlantic Beach" => "32233",
  "Auburndale" => "33823",
  "Avon Park" => "33826",
  "Avon Park/Avon Park AFB" => "33825",
  "Babson Park" => "33827",
  "Baker" => "32531",
  "Balm" => "33503",
  "Barberville" => "32105",
  "Bartow" => "33830",
  "Bartow" => "33831",
  "Bascom" => "32423",
  "Bay Pines" => "33744",
  "Bell" => "32619",
  "Belle Glade" => "33430",
  "Belleair Beach/Belleair Shores/Indian Rocks Beach" => "33786",
  "Belleview" => "34420",
  "Belleview" => "34421",
  "Beverly Hills" => "34464",
  "Beverly Hills" => "34465",
  "Big Pine Key" => "33043",
  "Blountstown" => "32424",
  "Boca Grande (PO BOX)" => "33921",
  "Boca Raton" => "33427",
  "Boca Raton" => "33428",
  "Boca Raton" => "33431",
  "Boca Raton" => "33432",
  "Boca Raton" => "33433",
  "Boca Raton" => "33434",
  "Boca Raton" => "33464",
  "Boca Raton" => "33481",
  "Boca Raton" => "33486",
  "Boca Raton" => "33487",
  "Boca Raton" => "33496",
  "Boca Raton" => "33498",
  "Boca Raton (PO Box)" => "33429",
  "Boca Raton (PO Box)" => "33497",
  "Bokeelia" => "33922",
  "Bonifay" => "32425",
  "Bonita Springs" => "34133",
  "Bonita Springs" => "34134",
  "Bonita Springs" => "34135",
  "Bowling Green" => "33834",
  "Boynton Beach" => "33425",
  "Boynton Beach" => "33426",
  "Boynton Beach" => "33435",
  "Boynton Beach" => "33436",
  "Boynton Beach" => "33437",
  "Boynton Beach" => "33472",
  "Boynton Beach" => "33473",
  "Boynton Beach (PO Box)" => "33424",
  "Bradenton" => "34204",
  "Bradenton" => "34205",
  "Bradenton" => "34206",
  "Bradenton" => "34210",
  "Bradenton" => "34211",
  "Bradenton" => "34212",
  "Bradenton/Braden River" => "34208",
  "Bradenton/Braden River/Lakewood Rch" => "34203",
  "Bradenton/Braden River/University Park" => "34201",
  "Bradenton/Fifty Seventh Avenue" => "34207",
  "Bradenton/Fifty Seventh Avenue" => "34282",
  "Bradenton/Lakewood Ranch/Lakewood Rch" => "34202",
  "Bradenton/Palma Sola" => "34209",
  "Bradenton/Palma Sola" => "34280",
  "Bradenton/Trailer Estates" => "34281",
  "Bradley" => "33835",
  "Brandon" => "33508",
  "Brandon" => "33509",
  "Brandon" => "33510",
  "Brandon" => "33511",
  "Branford" => "32008",
  "Bronson" => "32621",
  "Brooksville" => "34601",
  "Brooksville" => "34602",
  "Brooksville" => "34603",
  "Brooksville" => "34605",
  "Brooksville/Masaryktown/Spring Hill" => "34604",
  "Brooksville/Spring Hill/Weeki Wachee" => "34613",
  "Brooksville/Weeki Wachee" => "34614",
  "Buenaventura Lakes/Kissimmee" => "34743",
  "Bunnell" => "32110",
  "Bushnell" => "33513",
  "Campbellton" => "32426",
  "Canal Point" => "33438",
  "Candler" => "32111",
  "Cantonment" => "32533",
  "Cape Canaveral/Port Canaveral" => "32920",
  "Cape Coral" => "33904",
  "Cape Coral" => "33909",
  "Cape Coral" => "33910",
  "Cape Coral" => "33914",
  "Cape Coral" => "33915",
  "Cape Coral" => "33990",
  "Cape Coral" => "33991",
  "Cape Coral" => "33993",
  "Captiva" => "33924",
  "Carrabelle" => "32322",
  "Cassadaga" => "32706",
  "Casselberrry/Winter Springs / Tuscawilla" => "32708",
  "Casselberry" => "32707",
  "Casselberry" => "32718",
  "Casselberry/Fern Park" => "32730",
  "Cedar Key" => "32625",
  "Center Hill" => "33514",
  "Century" => "32535",
  "Chiefland" => "32626",
  "Chiefland (PO Box)" => "32644",
  "Chipley" => "32428",
  "Chokoloskee" => "34138",
  "Christmas" => "32709",
  "Citra" => "32113",
  "Clarcona" => "32710",
  "Clarksville" => "32430",
  "Clearwater" => "33755",
  "Clearwater" => "33757",
  "Clearwater" => "33758",
  "Clearwater" => "33759",
  "Clearwater" => "33760",
  "Clearwater" => "33761",
  "Clearwater" => "33762",
  "Clearwater" => "33763",
  "Clearwater" => "33764",
  "Clearwater" => "33766",
  "Clearwater" => "33769",
  "Clearwater/Belleair" => "33756",
  "Clearwater/Clearwater Beach" => "33767",
  "Clearwater/Sunset Point" => "33765",
  "Clermont" => "34711",
  "Clermont" => "34712",
  "Clermont" => "34713",
  "Clermont" => "34714",
  "Clewiston" => "33440",
  "Cocoa" => "32922",
  "Cocoa" => "32923",
  "Cocoa" => "32924",
  "Cocoa Beach" => "32931",
  "Cocoa Beach" => "32932",
  "Cocoa/Port St John" => "32927",
  "Cocoa/W Cocoa" => "32926",
  "Coleman" => "33521",
  "Cortez" => "34215",
  "Cottondale" => "32431",
  "Crawfordville" => "32326",
  "Crawfordville" => "32327",
  "Crescent City" => "32112",
  "Crestview" => "32539",
  "Cross City" => "32628",
  "Crystal Beach" => "34681",
  "Crystal River" => "34423",
  "Crystal River" => "34428",
  "Crystal River" => "34429",
  "Crystal Springs" => "33524",
  "Cypress" => "32432",
  "Dade City" => "33526",
  "Dade City/Richland" => "33525",
  "Dade City/Ridge Manor" => "33523",
  "Dania" => "33004",
  "Davenport" => "33836",
  "Davenport" => "33837",
  "Davenport" => "33897",
  "Davenport / Champions Gate" => "33896",
  "Daytona Beach" => "32114",
  "Daytona Beach" => "32115",
  "Daytona Beach" => "32117",
  "Daytona Beach" => "32120",
  "Daytona Beach" => "32122",
  "Daytona Beach" => "32124",
  "Daytona Beach" => "32125",
  "Daytona Beach" => "32126",
  "Daytona Beach" => "32198",
  "Daytona Beach/Daytona Beach Shores" => "32116",
  "Daytona Beach/Holly Hill" => "32118",
  "Daytona Beach/S Daytona Beach" => "32119",
  "Daytona Beach/South Daytona" => "32121",
  "Debary" => "32713",
  "Debary" => "32753",
  "Deerfield Beach" => "33441",
  "Deerfield Beach" => "33442",
  "Deerfield Beach" => "33443",
  "Defuniak Springs" => "32433",
  "Deland" => "32720",
  "Deland" => "32721",
  "Deland" => "32723",
  "Deland" => "32724",
  "Deland / Glenwood" => "32722",
  "Deleon Springs" => "32130",
  "Delray Beach" => "33444",
  "Delray Beach" => "33445",
  "Delray Beach" => "33446",
  "Delray Beach" => "33447",
  "Delray Beach" => "33448",
  "Delray Beach" => "33482",
  "Delray Beach" => "33483",
  "Delray Beach" => "33484",
  "Deltona" => "32728",
  "Deltona" => "32739",
  "Deltona / Deltona Pines" => "32738",
  "Deltona / Enterprise" => "32725",
  "Destin" => "32541",
  "Dover" => "33527",
  "DPO,AA" => "34002",
  "DPO,AA" => "34037",
  "Dundee" => "33838",
  "Dunedin" => "34697",
  "Dunedin" => "34698",
  "Dunnellon" => "34430",
  "Dunnellon" => "34431",
  "Dunnellon" => "34432",
  "Dunnellon/Citrus Springs" => "34433",
  "Dunnellon/Citrus Springs" => "34434",
  "Durant" => "33530",
  "Eagle Lake" => "33839",
  "Earleton" => "32631",
  "East Palatka" => "32131",
  "Eastlake Weir" => "32133",
  "Eastpoint" => "32328",
  "Eaton Park" => "33840",
  "Edgewater" => "32132",
  "Edgewater" => "32141",
  "El Jobean" => "33927",
  "Elfers" => "34680",
  "Elkton" => "32033",
  "Ellenton" => "34222",
  "Englewood" => "34223",
  "Englewood" => "34224",
  "Englewood" => "34295",
  "Englewood (PO BOX)" => "34294",
  "Estero" => "33928",
  "Eustis" => "32726",
  "Eustis" => "32727",
  "Eustis" => "32736",
  "Everglades City" => "34139",
  "Fairfield" => "32634",
  "Fedhaven/Lakeland/Lake Wales" => "33854",
  "Felda" => "33930",
  "Fellsmere" => "32948",
  "Fernandina Beach" => "32034",
  "Ferndale" => "34729",
  "Flagler Beach" => "32136",
  "Fleming Island" => "32003",
  "Florahome" => "32140",
  "Floral City" => "34436",
  "Fort Lauderdale" => "33301",
  "Fort Lauderdale" => "33304",
  "Fort Lauderdale" => "33305",
  "Fort Lauderdale" => "33306",
  "Fort Lauderdale" => "33307",
  "Fort Lauderdale" => "33308",
  "Fort Lauderdale" => "33309",
  "Fort Lauderdale" => "33311",
  "Fort Lauderdale" => "33312",
  "Fort Lauderdale" => "33315",
  "Fort Lauderdale" => "33316",
  "Fort Lauderdale" => "33317",
  "Fort Lauderdale" => "33319",
  "Fort Lauderdale" => "33321",
  "Fort Lauderdale" => "33322",
  "Fort Lauderdale" => "33323",
  "Fort Lauderdale" => "33324",
  "Fort Lauderdale" => "33325",
  "Fort Lauderdale" => "33326",
  "Fort Lauderdale" => "33327",
  "Fort Lauderdale" => "33328",
  "Fort Lauderdale" => "33329",
  "Fort Lauderdale" => "33330",
  "Fort Lauderdale" => "33331",
  "Fort Lauderdale" => "33334",
  "Fort Lauderdale" => "33337",
  "Fort Lauderdale" => "33338",
  "Fort Lauderdale" => "33339",
  "Fort Lauderdale" => "33346",
  "Fort Lauderdale" => "33351",
  "Fort Lauderdale" => "33388",
  "Fort Mc Coy/Salt Springs" => "32134",
  "Fort Meade" => "33841",
  "fort Myers" => "33994",
  "Fort Myers" => "33900",
  "Fort Myers" => "33901",
  "Fort Myers" => "33902",
  "Fort Myers" => "33905",
  "Fort Myers" => "33906",
  "Fort Myers" => "33907",
  "Fort Myers" => "33908",
  "Fort Myers" => "33912",
  "Fort Myers" => "33913",
  "Fort Myers" => "33916",
  "Fort Myers" => "33919",
  "Fort Myers" => "33965",
  "Fort Myers" => "33966",
  "Fort Myers" => "33967",
  "Fort Myers (PO BOX)" => "33911",
  "Fort Myers Beach" => "33931",
  "Fort Myers Beach" => "33932",
  "Fort Ogden (PO Box)" => "34267",
  "Fort Pierce" => "34945",
  "Fort Pierce" => "34946",
  "Fort Pierce" => "34947",
  "Fort Pierce" => "34949",
  "Fort Pierce" => "34950",
  "Fort Pierce" => "34951",
  "Fort Pierce" => "34954",
  "Fort Pierce" => "34979",
  "Fort Pierce" => "34981",
  "Fort Pierce" => "34982",
  "Fort Pierce (PO Box)" => "34948",
  "Fort Walton Beach" => "32547",
  "Fort Walton Beach" => "32548",
  "Fort White" => "32038",
  "Fountain" => "32438",
  "FPO, AA (Military)" => "34093",
  "FPO,AA" => "34092",
  "Freeport" => "32439",
  "Frostproof" => "33843",
  "Fruitland Park" => "34731",
  "Gainesville" => "32601",
  "Gainesville" => "32602",
  "Gainesville" => "32603",
  "Gainesville" => "32605",
  "Gainesville" => "32606",
  "Gainesville" => "32607",
  "Gainesville" => "32608",
  "Gainesville" => "32609",
  "Gainesville" => "32610",
  "Gainesville" => "32612",
  "Gainesville" => "32614",
  "Gainesville" => "32627",
  "Gainesville" => "32641",
  "Gainesville" => "32653",
  "Geneva" => "32732",
  "Georgetown" => "32139",
  "Gibsonton" => "33534",
  "Glen Saint Mary" => "32040",
  "Goldenrod" => "32733",
  "Gotha" => "34734",
  "Graceville" => "32440",
  "Grand Island" => "32735",
  "Grand Ridge" => "32442",
  "Grandin" => "32138",
  "Grant" => "32949",
  "Green Cove Springs" => "32043",
  "Greenville" => "32331",
  "Greenwood" => "32443",
  "Groveland" => "34736",
  "Gulf Breeze" => "32561",
  "Gulf Breeze" => "32563",
  "Gulf Hammock" => "32639",
  "Haines City/Grenelefe" => "33844",
  "Haines City/Grenelefe" => "33845",
  "Hallandale" => "33009",
  "Hastings" => "32145",
  "Havana" => "32333",
  "Hawthorne" => "32640",
  "Hernando" => "34442",
  "Hialeah" => "33012",
  "Hialeah" => "33013",
  "Hialeah" => "33014",
  "Hialeah" => "33015",
  "Hialeah" => "33016",
  "High Springs" => "32643",
  "Highland City" => "33846",
  "Hobe Sound" => "33455",
  "Hobe Sound" => "33475",
  "Holder" => "34445",
  "Holiday" => "34692",
  "Holiday/Tarpon Springs" => "34690",
  "Holiday/Tarpon Springs" => "34691",
  "Hollywood" => "33019",
  "Hollywood" => "33020",
  "Hollywood" => "33021",
  "Hollywood" => "33023",
  "Hollywood" => "33024",
  "Hollywood" => "33025",
  "Hollywood" => "33026",
  "Hollywood" => "33027",
  "Hollywood" => "33028",
  "Hollywood" => "33029",
  "Hollywood" => "33081",
  "Hollywood" => "33083",
  "Holmes Beach/Bradenton Beach" => "34218",
  "Holmes Beach/Bradenton Beach" => "34217",
  "Homeland" => "33847",
  "Homestead" => "33030",
  "Homestead" => "33031",
  "Homestead" => "33032",
  "Homestead" => "33033",
  "Homestead" => "33034",
  "Homestead" => "33035",
  "Homestead" => "33090",
  "Homosassa" => "34446",
  "Homosassa" => "34448",
  "Homosassa" => "34487",
  "Homosassa Springs" => "34447",
  "Horseshoe Beach" => "32648",
  "Howey In The Hills" => "34737",
  "Hudson/Bayonet Point/Port Richey" => "34667",
  "Hudson/Port Richey" => "34669",
  "Hudson/Port Richey" => "34674",
  "Immokalee" => "34142",
  "Immokalee" => "34143",
  "Indialantic/Melbourne" => "32903",
  "Indian Lake Estates/Lake Wales" => "33855",
  "Indian Rocks Beach/Belleair Beach/Indian Shores" => "33785",
  "Indiantown" => "34956",
  "Inglis" => "34449",
  "Intercession City" => "33848",
  "Interlachen" => "32148",
  "Inverness" => "34450",
  "Inverness" => "34451",
  "Inverness" => "34452",
  "Inverness" => "34453",
  "Islamorada" => "33036",
  "Island Grove" => "32654",
  "Istachatta" => "34636",
  "Jacksonville" => "32201",
  "Jacksonville" => "32202",
  "Jacksonville" => "32203",
  "Jacksonville" => "32204",
  "Jacksonville" => "32205",
  "Jacksonville" => "32206",
  "Jacksonville" => "32207",
  "Jacksonville" => "32208",
  "Jacksonville" => "32209",
  "Jacksonville" => "32210",
  "Jacksonville" => "32211",
  "Jacksonville" => "32212",
  "Jacksonville" => "32216",
  "Jacksonville" => "32217",
  "Jacksonville" => "32218",
  "Jacksonville" => "32219",
  "Jacksonville" => "32220",
  "Jacksonville" => "32221",
  "Jacksonville" => "32222",
  "Jacksonville" => "32223",
  "Jacksonville" => "32224",
  "Jacksonville" => "32225",
  "Jacksonville" => "32226",
  "Jacksonville" => "32228",
  "Jacksonville" => "32231",
  "Jacksonville" => "32232",
  "Jacksonville" => "32234",
  "Jacksonville" => "32236",
  "Jacksonville" => "32238",
  "Jacksonville" => "32244",
  "Jacksonville" => "32246",
  "Jacksonville" => "32247",
  "Jacksonville" => "32254",
  "Jacksonville" => "32256",
  "Jacksonville" => "32257",
  "Jacksonville" => "32258",
  "Jacksonville" => "32277",
  "Jacksonville Beach" => "32240",
  "Jacksonville Beach" => "32250",
  "Jasper" => "32052",
  "Jay" => "32565",
  "Jennings" => "32053",
  "Jupiter" => "33458",
  "Jupiter" => "33468",
  "Jupiter" => "33469",
  "Jupiter" => "33477",
  "Jupiter" => "33478",
  "Kathleen" => "33849",
  "Kenansville" => "34739",
  "Key Colony Beach" => "33051",
  "Key Largo" => "33037",
  "Key West" => "33040",
  "Keystone Heights" => "32656",
  "Killarney" => "34740",
  "Kissimmee" => "34742",
  "Kissimmee" => "34744",
  "Kissimmee" => "34745",
  "Kissimmee (Downtown East)" => "34741",
  "Kissimmee (West of Town)" => "34746",
  "Kissimmee / Poinciana" => "34758",
  "Kissimmee / Poinciana" => "34759",
  "Kissimmee/Celebration" => "34747",
  "Labelle" => "33935",
  "Labelle" => "33975",
  "Lacoochee" => "33537",
  "Lady Lake" => "32158",
  "Lady Lake (The Villages)" => "32159",
  "Lady Lake/The Villages" => "32162",
  "Lake Alfred" => "33850",
  "Lake Butler" => "32054",
  "Lake City" => "32024",
  "Lake City" => "32025",
  "Lake City" => "32055",
  "Lake City" => "32056",
  "Lake Como" => "32157",
  "Lake Geneva" => "32160",
  "Lake Hamilton" => "33851",
  "Lake Harbor" => "33459",
  "Lake Helen" => "32744",
  "Lake Mary / Heathrow" => "32746",
  "Lake Mary / Heathrow" => "32795",
  "Lake Monroe" => "32747",
  "Lake Panasoffkee" => "33538",
  "Lake Placid" => "33852",
  "Lake Placid" => "33862",
  "Lake Wales" => "33859",
  "Lake Wales" => "33898",
  "Lake Wales South" => "33853",
  "Lake Worth" => "33449",
  "Lake Worth" => "33454",
  "Lake Worth" => "33460",
  "Lake Worth" => "33461",
  "Lake Worth" => "33462",
  "Lake Worth" => "33463",
  "Lake Worth" => "33465",
  "Lake Worth" => "33466",
  "Lake Worth" => "33467",
  "Lakeland" => "33801",
  "Lakeland" => "33802",
  "Lakeland" => "33803",
  "Lakeland" => "33804",
  "Lakeland" => "33806",
  "Lakeland" => "33807",
  "Lakeland" => "33810",
  "Lakeland" => "33811",
  "Lakeland" => "33812",
  "Lakeland" => "33813",
  "Lakeland" => "33815",
  "Lakeland / Gibsonia" => "33805",
  "Lakeland / Polk City" => "33809",
  "Lamont" => "32336",
  "Lanark Village" => "32323",
  "Land O Lakes" => "34637",
  "Land O Lakes" => "34638",
  "Land O Lakes" => "34639",
  "Largo" => "33771",
  "Largo" => "33773",
  "Largo" => "33774",
  "Largo" => "33779",
  "Largo/Belleair Bluffs" => "33770",
  "Largo/Seminole" => "33778",
  "Laurel" => "34272",
  "Lawtey" => "32058",
  "Lecanto" => "34460",
  "Lecanto" => "34461",
  "Lee" => "32059",
  "Leesburg" => "34748",
  "Leesburg" => "34789",
  "Leesburg / Haines Creek" => "34788",
  "Leesburg POB" => "34749",
  "Lehigh Acres" => "33936",
  "Lehigh Acres" => "33970",
  "Lehigh Acres" => "33971",
  "Lehigh Acres" => "33972",
  "Lehigh Acres" => "33973",
  "Lehigh Acres" => "33974",
  "Lehigh Acres" => "33976",
  "Lithia" => "33547",
  "Live Oak" => "32060",
  "Live Oak" => "32064",
  "Lochloosa" => "32662",
  "Longboat Key" => "34228",
  "Longwood" => "32752",
  "Longwood East" => "32750",
  "Longwood/Wekiva Springs" => "32779",
  "Longwood/Wekiva Springs" => "32791",
  "Lorida" => "33857",
  "Loughman" => "33858",
  "Lowell" => "32663",
  "Loxahatchee" => "33470",
  "Lutz" => "33548",
  "Lutz" => "33549",
  "Lutz" => "33558",
  "Lutz" => "33559",
  "Lynn Haven" => "32444",
  "Macclenny" => "32063",
  "Madeira Beach" => "33738",
  "Madison" => "32340",
  "Madison" => "32341",
  "Maitland" => "32794",
  "Maitland / Eatonville" => "32751",
  "Malabar" => "32950",
  "Mango" => "33550",
  "Marathon" => "33050",
  "Marathon Shores" => "33052",
  "Marco Island" => "34145",
  "Marianna" => "32446",
  "Marianna" => "32447",
  "Marianna" => "32448",
  "Mary Esther" => "32569",
  "Mascotte" => "34753",
  "Mayo" => "32066",
  "Mc Alpin" => "32062",
  "Mc Intosh" => "32664",
  "Melbourne" => "32901",
  "Melbourne" => "32902",
  "Melbourne" => "32919",
  "Melbourne" => "32935",
  "Melbourne" => "32936",
  "Melbourne" => "32941",
  "Melbourne Beach/Melbourne" => "32951",
  "Melbourne/Eau Gallie" => "32934",
  "Melbourne/Melbourne Village/West Melbourne" => "32904",
  "Melbourne/Viera" => "32940",
  "Melbourne/West Melbourne" => "32912",
  "Melrose" => "32666",
  "Merritt Island" => "32952",
  "Merritt Island" => "32953",
  "Merritt Island" => "32954",
  "Mexico Beach" => "32410",
  "Miami" => "33110",
  "Miami" => "33122",
  "Miami" => "33124",
  "Miami" => "33125",
  "Miami" => "33127",
  "Miami" => "33129",
  "Miami" => "33130",
  "Miami" => "33131",
  "Miami" => "33133",
  "Miami" => "33134",
  "Miami" => "33135",
  "Miami" => "33136",
  "Miami" => "33138",
  "Miami" => "33142",
  "Miami" => "33143",
  "Miami" => "33144",
  "Miami" => "33145",
  "Miami" => "33146",
  "Miami" => "33147",
  "Miami" => "33155",
  "Miami" => "33156",
  "Miami" => "33157",
  "Miami" => "33158",
  "Miami" => "33161",
  "Miami" => "33162",
  "Miami" => "33164",
  "Miami" => "33165",
  "Miami" => "33166",
  "Miami" => "33167",
  "Miami" => "33168",
  "Miami" => "33169",
  "Miami" => "33170",
  "Miami" => "33172",
  "Miami" => "33173",
  "Miami" => "33174",
  "Miami" => "33175",
  "Miami" => "33176",
  "Miami" => "33177",
  "Miami" => "33178",
  "Miami" => "33179",
  "Miami" => "33180",
  "Miami" => "33181",
  "Miami" => "33182",
  "Miami" => "33183",
  "Miami" => "33186",
  "Miami" => "33187",
  "Miami" => "33189",
  "Miami" => "33190",
  "Miami" => "33193",
  "Miami" => "33194",
  "Miami" => "33195",
  "Miami" => "33196",
  "Miami" => "33197",
  "Miami" => "33238",
  "Miami" => "33242",
  "Miami" => "33243",
  "Miami" => "33247",
  "Miami" => "33255",
  "Miami" => "33265",
  "Miami" => "33269",
  "Miami Beach" => "33109",
  "Miami Beach" => "33139",
  "Miami Beach" => "33140",
  "Miami Beach" => "33141",
  "Miami Beach" => "33154",
  "Micanopy" => "32667",
  "Mid Florida" => "32745",
  "Mid Florida" => "32799",
  "Middleburg" => "32068",
  "Milligan" => "32537",
  "Milton" => "32570",
  "Milton" => "32571",
  "Milton" => "32572",
  "Milton" => "32583",
  "Mims" => "32754",
  "Minneola" => "34715",
  "Minneola" => "34755",
  "Miramar Beach" => "32550",
  "Monticello" => "32344",
  "Monticello" => "32345",
  "Montverde" => "34756",
  "Moore Haven" => "33471",
  "Morriston" => "32668",
  "Mossy Head" => "32434",
  "Mount Dora" => "32756",
  "Mount Dora" => "32757",
  "Mulberry" => "33860",
  "Murdock" => "33938",
  "Myakka City" => "34251",
  "Nalcrest / Lake Wales" => "33856",
  "Naples" => "34102",
  "Naples" => "34103",
  "Naples" => "34104",
  "Naples" => "34105",
  "Naples" => "34106",
  "Naples" => "34108",
  "Naples" => "34109",
  "Naples" => "34110",
  "Naples" => "34112",
  "Naples" => "34113",
  "Naples" => "34114",
  "Naples" => "34116",
  "Naples" => "34117",
  "Naples" => "34119",
  "Naples" => "34120",
  "Navarre" => "32566",
  "New Port Richey" => "34652",
  "New Port Richey" => "34653",
  "New Port Richey" => "34654",
  "New Port Richey" => "34656",
  "New Port Richey/Seven Springs/Trinity" => "34655",
  "New Smyrna Beach" => "32168",
  "New Smyrna Beach" => "32169",
  "New Smyrna Beach" => "32170",
  "Newberry" => "32669",
  "Niceville" => "32578",
  "Niceville" => "32588",
  "Nichols" => "33863",
  "Nobleton" => "34661",
  "Nocatee (PO Box)" => "34268",
  "Nokomis" => "34274",
  "Nokomis/North Venice" => "34275",
  "North Fort Myers" => "33903",
  "North Fort Myers" => "33917",
  "North Fort Myers" => "33918",
  "North Miami Beach" => "33160",
  "North Palm Beach" => "33408",
  "North Port" => "34288",
  "North Port" => "34289",
  "North Port" => "34291",
  "North Port (PO BOX)" => "34290",
  "North Port/Venice" => "34286",
  "North Port/Venice" => "34287",
  "O Brien" => "32071",
  "Oak Hill" => "32759",
  "Oakland" => "34760",
  "Ocala" => "34470",
  "Ocala" => "34471",
  "Ocala" => "34472",
  "Ocala" => "34473",
  "Ocala" => "34474",
  "Ocala" => "34475",
  "Ocala" => "34476",
  "Ocala" => "34477",
  "Ocala" => "34478",
  "Ocala" => "34479",
  "Ocala" => "34480",
  "Ocala" => "34481",
  "Ocala" => "34482",
  "Ocala" => "34483",
  "Ochopee" => "34141",
  "Ocklawaha" => "32179",
  "Ocklawaha" => "32183",
  "Ocoee" => "34761",
  "Odessa" => "33556",
  "Okahumpka" => "34762",
  "Okeechobee" => "34972",
  "Okeechobee" => "34973",
  "Okeechobee" => "34974",
  "Old Town" => "32680",
  "Oldsmar" => "34677",
  "Ona" => "33865",
  "Oneco" => "34264",
  "Opa Locka" => "33054",
  "Opa Locka" => "33055",
  "Opa Locka" => "33056",
  "Orange City" => "32763",
  "Orange City" => "32774",
  "Orange Lake" => "32681",
  "Orange Park" => "32065",
  "Orange Park" => "32073",
  "Orange Springs" => "32182",
  "Orlando" => "32801",
  "Orlando" => "32802",
  "Orlando" => "32814",
  "Orlando" => "32854",
  "Orlando" => "32855",
  "Orlando" => "32856",
  "Orlando" => "32857",
  "Orlando" => "32858",
  "Orlando" => "32859",
  "Orlando" => "32860",
  "Orlando" => "32861",
  "Orlando" => "32862",
  "Orlando" => "32867",
  "Orlando" => "32868",
  "Orlando" => "32869",
  "Orlando" => "32896",
  "Orlando/Airport/Alafaya/Lake Nona" => "32827",
  "Orlando/Alafaya" => "32826",
  "Orlando/Alafaya" => "32831",
  "Orlando/Alafaya" => "32834",
  "Orlando/Alafaya" => "32878",
  "Orlando/Alafaya/Waterford Lakes" => "32828",
  "Orlando/AmSouth" => "32885",
  "Orlando/Azalea Park/Park Manor" => "32807",
  "Orlando/Bay Hill/Sand Lake" => "32819",
  "Orlando/Bithlo" => "32820",
  "Orlando/Chickasaw" => "32829",
  "Orlando/College Park" => "32804",
  "Orlando/Colonial Town" => "32803",
  "Orlando/Conway / Belle Isle" => "32812",
  "Orlando/Defense Finance Accounting" => "32893",
  "Orlando/Delaney Park/Crystal Lake" => "32806",
  "Orlando/Dr. Phillips/Bay Vista" => "32836",
  "Orlando/Edgewood/Pinecastle" => "32839",
  "Orlando/Edgewood/Pinecastle" => "32853",
  "Orlando/First Union Bank" => "32886",
  "Orlando/Harcourt Brace" => "32887",
  "Orlando/Hiawassee/Pine Hills" => "32818",
  "Orlando/Hunters Creek/Southchase" => "32837",
  "Orlando/International Drive" => "32821",
  "Orlando/J C Penney Company" => "32890",
  "Orlando/Kennedy Space Center" => "32815",
  "Orlando/Kennedy Space Center" => "32899",
  "Orlando/Lake Buena Vista" => "32830",
  "Orlando/Lockhart" => "32810",
  "Orlando/MasterCard" => "32898",
  "Orlando/Metrowest/Orlo Vista" => "32835",
  "Orlando/Moss Park/Lake Mary Jane" => "32832",
  "Orlando/Orlo Vista/Richmond Heights" => "32811",
  "Orlando/Pine Hills" => "32808",
  "Orlando/Pinecastle/Oakridge/ Edgewood" => "32809",
  "Orlando/Rio Pinar / Union Park" => "32825",
  "Orlando/South Creek" => "32877",
  "Orlando/Sun Mortgage" => "32891",
  "Orlando/Sun Trust National Bank" => "32897",
  "Orlando/Taft / Meadow woods" => "32824",
  "Orlando/UCF" => "32816",
  "Orlando/Union Park" => "32872",
  "Orlando/Union Park/University Area" => "32817",
  "Orlando/Ventura" => "32822",
  "Orlando/Washington Shores" => "32805",
  "Orlando/Wedgefield/Rocket City/Cape Orlando" => "32833",
  "Ormond Beach" => "32173",
  "Ormond Beach" => "32174",
  "Ormond Beach" => "32175",
  "Ormond Beach" => "32176",
  "Osprey" => "34229",
  "Osteen" => "32764",
  "Otter Creek" => "32683",
  "Oviedo" => "32762",
  "Oviedo" => "32765",
  "Oviedo/Chuluota" => "32766",
  "Oxford" => "34484",
  "Ozona" => "34660",
  "Pahokee" => "33476",
  "Paisley" => "32767",
  "Palatka" => "32177",
  "Palatka" => "32178",
  "Palm Bay/Melbourne" => "32905",
  "Palm Bay/Melbourne" => "32906",
  "Palm Bay/Melbourne" => "32907",
  "Palm Bay/Melbourne" => "32908",
  "Palm Bay/Melbourne" => "32909",
  "Palm Bay/Melbourne" => "32910",
  "Palm Bay/Melbourne" => "32911",
  "Palm Beach" => "33480",
  "Palm Beach Gardens" => "33410",
  "Palm Beach Gardens" => "33418",
  "Palm City" => "34990",
  "Palm City" => "34991",
  "Palm Coast" => "32135",
  "Palm Coast" => "32137",
  "Palm Coast" => "32164",
  "Palm Harbor" => "34682",
  "Palm Harbor" => "34683",
  "Palm Harbor" => "34684",
  "Palm Harbor" => "34685",
  "Palmdale" => "33944",
  "Palmetto" => "34220",
  "Palmetto/Rubonia" => "34221",
  "Panacea" => "32346",
  "Panama City" => "32402",
  "Panama City" => "32403",
  "Panama City" => "32404",
  "Panama City" => "32405",
  "Panama City" => "32406",
  "Panama City" => "32408",
  "Panama City" => "32409",
  "Panama City" => "32412",
  "Panama City Beach" => "32407",
  "Panama City Beach" => "32413",
  "Parrish" => "34219",
  "Patrick AFB/Canaveral Air Station" => "32925",
  "Pembroke Pines" => "33082",
  "Pensacola" => "32507",
  "Pensacola" => "32508",
  "Pensacola" => "32513",
  "Pensacola" => "32514",
  "Pensacola" => "32521",
  "Pensacola" => "32523",
  "Pensacola" => "32526",
  "Pensacola" => "32534",
  "Perry" => "32347",
  "Perry" => "32348",
  "Pierson" => "32180",
  "Pineland" => "33945",
  "Pinellas Park" => "33780",
  "Pinellas Park" => "33781",
  "Pinellas Park" => "33782",
  "Pinetta" => "32350",
  "Placida" => "33946",
  "Plant City" => "33563",
  "Plant City" => "33564",
  "Plant City" => "33565",
  "Plant City" => "33566",
  "Plant City" => "33567",
  "Plymouth" => "32768",
  "Polk City" => "33868",
  "Pomona Park" => "32181",
  "Pompano Beach" => "33060",
  "Pompano Beach" => "33062",
  "Pompano Beach" => "33063",
  "Pompano Beach" => "33064",
  "Pompano Beach" => "33065",
  "Pompano Beach" => "33066",
  "Pompano Beach" => "33067",
  "Pompano Beach" => "33068",
  "Pompano Beach" => "33069",
  "Pompano Beach" => "33071",
  "Pompano Beach" => "33073",
  "Pompano Beach" => "33075",
  "Pompano Beach" => "33076",
  "Pompano Beach" => "33093",
  "Pompano Beach" => "33097",
  "Ponce de Leon" => "32455",
  "Ponte Vedra Beach" => "32082",
  "Port Charlotte" => "33948",
  "Port Charlotte" => "33949",
  "Port Charlotte" => "33952",
  "Port Charlotte" => "33953",
  "Port Charlotte" => "33954",
  "Port Charlotte" => "33980",
  "Port Charlotte" => "33981",
  "Port Orange" => "32129",
  "Port Orange/Allandale/Daytona Beach" => "32123",
  "Port Orange/Daytona Beach" => "32128",
  "Port Orange/Ponce Inlet/Daytona Beach" => "32127",
  "Port Richey" => "34668",
  "Port Richey" => "34673",
  "Port Saint Joe" => "32456",
  "Port Saint Lucie" => "34952",
  "Port Saint Lucie" => "34953",
  "Port Saint Lucie" => "34983",
  "Port Saint Lucie" => "34984",
  "Port Saint Lucie" => "34985",
  "Port Saint Lucie" => "34986",
  "Port Saint Lucie" => "34987",
  "Port Saint Lucie" => "34988",
  "Punta Gorda" => "33950",
  "Punta Gorda" => "33951",
  "Punta Gorda" => "33955",
  "Punta Gorda" => "33982",
  "Punta Gorda" => "33983",
  "Quincy" => "32351",
  "Quincy" => "32352",
  "Quincy" => "32353",
  "Reddick" => "32686",
  "River Ranch / Lake Wales" => "33867",
  "Riverview" => "33568",
  "Riverview" => "33569",
  "Riverview" => "33578",
  "Riverview" => "33579",
  "Rockledge" => "32956",
  "Rockledge/Viera" => "32955",
  "Roseland" => "32957",
  "Rosemary Beach" => "32461",
  "Rotonda West" => "33947",
  "Ruskin" => "33575",
  "Ruskin/Apollo Beach" => "33570",
  "Safety Harbor" => "34695",
  "Saint Augustine" => "32080",
  "Saint Augustine" => "32084",
  "Saint Augustine" => "32085",
  "Saint Augustine" => "32086",
  "Saint Augustine" => "32092",
  "Saint Augustine" => "32095",
  "Saint James City" => "33956",
  "Saint Johns" => "32259",
  "Saint Leo" => "33574",
  "Salem" => "32356",
  "San Antonio" => "33576",
  "San Mateo" => "32187",
  "Sanderson" => "32087",
  "Sanford" => "32772",
  "Sanford" => "32773",
  "Sanford/Lake Forest" => "32771",
  "Sanibel" => "33957",
  "Santa Rosa Beach" => "32459",
  "Sarasota" => "34230",
  "Sarasota" => "34233",
  "Sarasota" => "34234",
  "Sarasota" => "34235",
  "Sarasota" => "34236",
  "Sarasota" => "34237",
  "Sarasota" => "34240",
  "Sarasota" => "34241",
  "Sarasota" => "34243",
  "Sarasota" => "34276",
  "Sarasota" => "34277",
  "Sarasota/Crescent Beach/Siesta Key" => "34242",
  "Sarasota/Fruitville" => "34232",
  "Sarasota/Gulf Gate Branch" => "34231",
  "Sarasota/Manasota" => "34260",
  "Sarasota/Pinecraft" => "34239",
  "Sarasota/Pinecraft" => "34278",
  "Sarasota/Sarasota Square" => "34238",
  "Satellite Beach/Indian Harbor Beach/Melbourne" => "32937",
  "Satsuma" => "32189",
  "Scottsmoor" => "32775",
  "Sebastian" => "32958",
  "Sebastian/Barefoot Bay/Micco" => "32976",
  "Sebring" => "33870",
  "Sebring" => "33871",
  "Sebring" => "33872",
  "Sebring" => "33875",
  "Sebring" => "33876",
  "Seffner" => "33583",
  "Seffner" => "33584",
  "Seminole" => "33772",
  "Seminole" => "33775",
  "Seminole/Largo" => "33776",
  "Seminole/Largo" => "33777",
  "Seville" => "32190",
  "Shady Grove" => "32357",
  "Shalimar" => "32579",
  "Sharpes" => "32959",
  "Silver Springs" => "34488",
  "Silver Springs" => "34489",
  "Sneads" => "32460",
  "Sorrento / Mount Plymouth" => "32776",
  "Sparr" => "32192",
  "Spring Hill/Brooksville" => "34608",
  "Spring Hill/Brooksville" => "34609",
  "Spring Hill/Brooksville" => "34611",
  "Spring Hill/Brooksville/Shady Hills/Weeki Wachee" => "34610",
  "Spring Hill/Brooksville/Weeki Wachee" => "34606",
  "Spring Hill/Brooksville/Weeki Wachee/Hernando Beach" => "34607",
  "St Cloud" => "34770",
  "St Cloud (City of St Cloud)" => "34769",
  "St Cloud (Harmony)" => "34773",
  "St Cloud (Magnolia Square)" => "34771",
  "St Cloud (Narcoossee Road)" => "34772",
  "St Pete" => "33701",
  "St Pete" => "33702",
  "St Pete" => "33703",
  "St Pete" => "33705",
  "St Pete" => "33712",
  "St Pete" => "33713",
  "St Pete" => "33714",
  "St Pete" => "33716",
  "St Pete" => "33728",
  "St Pete" => "33730",
  "St Pete" => "33731",
  "St Pete" => "33732",
  "St Pete" => "33733",
  "St Pete" => "33734",
  "St Pete" => "33742",
  "St Pete" => "33743",
  "St Pete" => "33747",
  "St Pete" => "33784",
  "St Pete/Crossroads" => "33710",
  "St Pete/Euclid" => "33704",
  "St Pete/Gulfport" => "33711",
  "St Pete/Gulfport" => "33737",
  "St Pete/Home Shopping" => "33729",
  "St Pete/Kenneth City" => "33709",
  "St Pete/Madeira Beach/N Redington Beach/Shores" => "33708",
  "St Pete/Pass a Grille" => "33741",
  "St Pete/Pass a Grille Beach/St Pete Beach/Treasure Island" => "33706",
  "St Pete/South Pasadena/Gulfport/St Pete Beach" => "33707",
  "St Pete/St Pete Beach" => "33736",
  "St Pete/Tierra Verde" => "33715",
  "St Pete/Treasure Island" => "33740",
  "Starke" => "32091",
  "Steinhatchee" => "32359",
  "Stuart" => "34994",
  "Stuart" => "34995",
  "Stuart" => "34996",
  "Stuart" => "34997",
  "Summerfield" => "34491",
  "Summerfield" => "34492",
  "Summerland Key" => "33042",
  "Sumterville" => "33585",
  "Sun City" => "33586",
  "Sun City Center / Ruskin" => "33571",
  "Sun City Center / Ruskin" => "33573",
  "Suwannee" => "32692",
  "Sydney" => "33587",
  "Tallahassee" => "32301",
  "Tallahassee" => "32303",
  "Tallahassee" => "32304",
  "Tallahassee" => "32305",
  "Tallahassee" => "32308",
  "Tallahassee" => "32309",
  "Tallahassee" => "32310",
  "Tallahassee" => "32311",
  "Tallahassee" => "32312",
  "Tallahassee" => "32314",
  "Tallahassee" => "32317",
  "Tallahassee" => "32399",
  "Tallevast" => "34270",
  "Tampa" => "33601",
  "Tampa" => "33602",
  "Tampa" => "33607",
  "Tampa" => "33611",
  "Tampa" => "33613",
  "Tampa" => "33614",
  "Tampa" => "33616",
  "Tampa" => "33621",
  "Tampa" => "33622",
  "Tampa" => "33623",
  "Tampa" => "33630",
  "Tampa" => "33631",
  "Tampa" => "33633",
  "Tampa" => "33634",
  "Tampa" => "33635",
  "Tampa" => "33646",
  "Tampa" => "33650",
  "Tampa" => "33651",
  "Tampa" => "33655",
  "Tampa" => "33663",
  "Tampa" => "33672",
  "Tampa" => "33673",
  "Tampa" => "33674",
  "Tampa" => "33675",
  "Tampa" => "33677",
  "Tampa" => "33679",
  "Tampa" => "33680",
  "Tampa" => "33681",
  "Tampa" => "33682",
  "Tampa" => "33684",
  "Tampa" => "33685",
  "Tampa" => "33686",
  "Tampa" => "33690",
  "Tampa" => "33694",
  "Tampa" => "33697",
  "Tampa / Carrollwood" => "33625",
  "Tampa / Carrollwood / Lake Carroll" => "33618",
  "Tampa / Davis Island/University of Tampa" => "33606",
  "Tampa / East Lake" => "33610",
  "Tampa / Forest Hills" => "33612",
  "Tampa / Northdale" => "33624",
  "Tampa / Palm River / Progress Village" => "33619",
  "Tampa / Palma Ceia" => "33609",
  "Tampa / Palma Ceia" => "33629",
  "Tampa / Seminole Heights" => "33603",
  "Tampa / Sulphur Springs" => "33604",
  "Tampa / Tampa Palms" => "33647",
  "Tampa / Temple Terrace" => "33617",
  "Tampa / Temple Terrace" => "33637",
  "Tampa / Time; INC" => "33660",
  "Tampa / Time; INC" => "33661",
  "Tampa / Time; INC" => "33662",
  "Tampa / Town and Country" => "33615",
  "Tampa / University of So. Fla" => "33620",
  "Tampa / Ybor City" => "33605",
  "Tampa/Carrollwood" => "33688",
  "Tampa/Falkenburg Branch" => "33689",
  "Tampa/MacDill AFB" => "33608",
  "Tampa/Northdale/Westchase" => "33626",
  "Tampa/Temple Terrace" => "33687",
  "Tampa/Time Customer Service; Inc" => "33664",
  "Tangerine" => "32777",
  "Tarpon Springs" => "34688",
  "Tarpon Springs" => "34689",
  "Tavares / Deer Island" => "32778",
  "Tavernier" => "33070",
  "Terra Ceia/Terra Ceia Island" => "34250",
  "The Villages" => "32163",
  "Thonotosassa" => "33592",
  "Titusville" => "32780",
  "Titusville" => "32781",
  "Titusville" => "32782",
  "Titusville" => "32783",
  "Titusville" => "32796",
  "Trenton" => "32693",
  "Trilby" => "33593",
  "Umatilla / Donna Vista" => "32784",
  "Unknown" => "99999",
  "Valrico" => "33594",
  "Valrico" => "33595",
  "Valrico" => "33596",
  "Vanderbilt Beach" => "34107",
  "Venice" => "34284",
  "Venice" => "34285",
  "Venice" => "34292",
  "Venice" => "34293",
  "Venus" => "33960",
  "Vero Beach" => "32960",
  "Vero Beach" => "32962",
  "Vero Beach" => "32963",
  "Vero Beach" => "32966",
  "Vero Beach" => "32967",
  "Vero Beach" => "32968",
  "Wacissa" => "32361",
  "Wauchula" => "33873",
  "Waverly" => "33877",
  "Webster/Ridge Manor Estates" => "33597",
  "Weirsdale" => "32195",
  "Welaka" => "32193",
  "Wellborn" => "32094",
  "Wesley Chapel" => "33545",
  "West Palm Beach" => "33401",
  "West Palm Beach" => "33402",
  "West Palm Beach" => "33403",
  "West Palm Beach" => "33404",
  "West Palm Beach" => "33405",
  "West Palm Beach" => "33406",
  "West Palm Beach" => "33407",
  "West Palm Beach" => "33409",
  "West Palm Beach" => "33411",
  "West Palm Beach" => "33412",
  "West Palm Beach" => "33413",
  "West Palm Beach" => "33414",
  "West Palm Beach" => "33415",
  "West Palm Beach" => "33416",
  "West Palm Beach" => "33417",
  "West Palm Beach" => "33420",
  "West Palm Beach" => "33421",
  "West Palm Beach" => "33422",
  "Westville" => "32464",
  "Wewahitchka" => "32465",
  "Wildwood" => "34785",
  "Williston" => "32696",
  "Wimauma" => "33598",
  "Windermere" => "34786",
  "Winter Garden" => "34777",
  "Winter Garden" => "34778",
  "Winter Garden/Oakland" => "34787",
  "Winter Haven" => "33882",
  "Winter Haven" => "33883",
  "Winter Haven / Cypress Gardens" => "33884",
  "Winter Haven / Florence Villa" => "33881",
  "Winter Haven / Florence Villa" => "33885",
  "Winter Haven / State Farm Insurance" => "33888",
  "Winter Haven/Eloise/JPV/Wahnetta" => "33880",
  "Winter Park" => "32789",
  "Winter Park" => "32790",
  "Winter Park" => "32793",
  "Winter Park/Aloma" => "32792",
  "Winter Springs/Casselberry" => "32719",
  "Woodville" => "32362",
  "Worthington Springs" => "32697",
  "Yalaha" => "34797",
  "Yankeetown" => "34498",
  "Youngstown" => "32466",
  "Yulee" => "32097",
  "Zellwood" => "32798",
  "Zephyrhills" => "33539",
  "Zephyrhills" => "33540",
  "Zephyrhills" => "33541",
  "Zephyrhills" => "33542",
  "Zephyrhills/Wesley Chapel" => "33543",
  "Zephyrhills/Wesley Chapel" => "33544",
  "Zolfo Springs" => "33890"
  },

  mls_elementary_school: {
    "AcadamieDavinci" => "Acadamie Davinci",
  "AddedSchoolA" => "Added School - A",
  "AddedSchoolB" => "Added School - B",
  "AddedSchoolC" => "Added School - C",
  "AddedSchoolD" => "Added School - D",
  "AddedSchoolE" => "Added School - E",
  "AddedSchoolF" => "Added School - F",
  "AlafiaElem" => "Alafia Elem",
  "AlafiaHB" => "Alafia-HB",
  "AlexanderElem" => "Alexander Elem",
  "AlexanderHB" => "Alexander-HB",
  "AllenParkElementary" => "Allen Park Elementary",
  "AlomaElem" => "Aloma Elem",
  "AltamonteElementary" => "Altamonte Elementary",
  "AltamonteElementary" => "Altamonte Elementary",
  "AltaVistaElem" => "Alta Vista Elem",
  "AltaVistaElementary" => "Alta Vista Elementary",
  "AlternativeSchools" => "Alternative Schools",
  "AlturasElem" => "Alturas Elem",
  "AlvaElementary" => "Alva Elementary",
  "AncloteElem" => "Anclote Elem",
  "AncloteElementaryPO" => "Anclote Elementary-PO",
  "AndersonElem" => "Anderson Elem",
  "AndersonHB" => "Anderson-HB",
  "AndoverElem" => "Andover Elem",
  "AnnaMariaElementary" => "Anna Maria Elementary",
  "AnnaWoodberryElem" => "Anna Woodberry Elem",
  "AnonaElem" => "Anona Elem",
  "AnonaElementaryPN" => "Anona Elementary-PN",
  "ApolloBeachElem" => "Apollo Beach Elem",
  "ApolloBeachHB" => "Apollo Beach-HB",
  "ApopkaElem" => "Apopka Elem",
  "AppleSchool" => "Apple School",
  "ArborRidgeElem" => "Arbor Ridge Elem",
  "AshtonElementary" => "Ashton Elementary",
  "AstatulaElem" => "Astatula Elem",
  "AthenianAcademy" => "Athenian Academy",
  "AtwaterElementary" => "Atwater Elementary",
  "AuburndaleCentralElem" => "Auburndale Central Elem",
  "AudubonParkElem" => "Audubon Park Elem",
  "AvalonElem" => "Avalon Elem",
  "AzaleaElem" => "Azalea Elem",
  "AzaleaElementaryPN" => "Azalea Elementary-PN",
  "AzaleaParkElem" => "Azalea Park Elem",
  "BabsonParkElem" => "Babson Park Elem",
  "BaileyElementaryHB" => "Bailey Elementary-HB",
  "BallardElementary" => "Ballard Elementary",
  "BallastPointHB" => "Ballast Point-HB",
  "BallastPtElem" => "Ballast Pt Elem",
  "BardmoorElem" => "Bardmoor Elem",
  "BardmoorElementaryPN" => "Bardmoor Elementary-PN",
  "BartowElemAcademy" => "Bartow Elem Academy",
  "BauderElem" => "Bauder Elem",
  "BauderElementaryPN" => "Bauder Elementary-PN",
  "BayCrestElem" => "Bay Crest Elem",
  "BayCrestHB" => "Bay Crest-HB",
  "BayHavenSchofBasicsPlus" => "Bay Haven Sch of Basics Plus Elementary",
  "BayMeadowsElem" => "Bay Meadows Elem",
  "BayPointElem" => "Bay Point Elem",
  "BayPointElementaryPN" => "Bay Point Elementary-PN",
  "BayshoreElementary" => "Bayshore Elementary",
  "BayVistaFundamental" => "Bay Vista Fundamental",
  "BayVistaFundamentalPN" => "Bay Vista Fundamental-PN",
  "BearCreekElem" => "Bear Creek Elem",
  "BearCreekElementaryPN" => "Bear Creek Elementary-PN",
  "BearLakeElementary" => "Bear Lake Elementary",
  "BelcherElem" => "Belcher Elem",
  "BelcherElementaryPN" => "Belcher Elementary-PN",
  "BellamyElem" => "Bellamy Elem",
  "BellamyHB" => "Bellamy-HB",
  "BelleairElem" => "Belleair Elem",
  "BelleairElementaryPN" => "Belleair Elementary-PN",
  "BenHillGriffinElem" => "Ben Hill Griffin Elem",
  "BentleyElementary" => "Bentley Elementary",
  "BerkleyElem" => "Berkley Elem",
  "BethuneAcademy" => "Bethune Academy",
  "BeverlyShoresElem" => "Beverly Shores Elem",
  "BevisElem" => "Bevis Elem",
  "BevisHB" => "Bevis-HB",
  "BingElem" => "Bing Elem",
  "BingHB" => "Bing-HB",
  "BlackburnElementary" => "Blackburn Elementary",
  "BlakeAcademy" => "Blake Academy",
  "BlancheHDaughtreyElem" => "Blanche H. Daughtrey Elementary",
  "BlanknerElem" => "Blankner Elem",
  "BlantonElem" => "Blanton Elem",
  "BlantonElementaryPN" => "Blanton Elementary-PN",
  "BlueLakeElem" => "Blue Lake Elem",
  "BoggyCreekElemK5" => "Boggy Creek Elem (K 5)",
  "BonitaSpringsElementary" => "Bonita Springs Elementary",
  "BonnerElem" => "Bonner Elem",
  "BonnevilleElem" => "Bonneville Elem",
  "BookerTWashingtonHB" => "Booker T. Washington-HB",
  "BoswellElem" => "Boswell Elem",
  "BoyetteSpringsElem" => "Boyette Springs Elem",
  "BoyetteSpringsHB" => "Boyette Springs-HB",
  "BradenRiverElementary" => "Braden River Elementary",
  "BrentwoodElementary" => "Brentwood Elementary",
  "BrighamAdademy" => "Brigham Adademy",
  "BrookerCreekElem" => "Brooker Creek Elem",
  "BrookerCreekElementaryPN" => "Brooker Creek Elementary-PN",
  "BrookerElem" => "Brooker Elem",
  "BrookerHB" => "Brooker-HB",
  "BrookshireElem" => "Brookshire Elem",
  "BrowardElem" => "Broward Elem",
  "BrowardHB" => "Broward-HB",
  "BryanElem" => "Bryan Elem",
  "BryanPlantCityHB" => "Bryan Plant City-HB",
  "BryantHB" => "Bryant-HB",
  "BuckhornElem" => "Buckhorn Elem",
  "BuckhornHB" => "Buckhorn-HB",
  "BurneyHB" => "Burney-HB",
  "BushnellElementarySchool" => "Bushnell Elementary School",
  "CahoonElem" => "Cahoon Elem",
  "CahoonHB" => "Cahoon-HB",
  "CaldwellElem" => "Caldwell Elem",
  "CaloosaElementary" => "Caloosa Elementary",
  "CalusaElem" => "Calusa Elem",
  "CalusaElementaryPO" => "Calusa Elementary-PO",
  "CalvinAHunsingerL" => "Calvin A. Hunsinger L",
  "CamelotElem" => "Camelot Elem",
  "CaminitiExceptional" => "Caminiti Exceptional",
  "CampbellParkElem" => "Campbell Park Elem",
  "CampbellParkElementaryPN" => "Campbell Park Elementary-PN",
  "CannellaElem" => "Cannella Elem",
  "CannellaHB" => "Cannella-HB",
  "CapeElementary" => "Cape Elementary",
  "CarillonElementary" => "Carillon Elementary",
  "CarltonPalmoreElem" => "Carlton Palmore Elem",
  "CarrieDRobinsonLittleton" => "Dr. Carrie D. Robinson Littleton",
  "CarrollwoodElem" => "Carrollwood Elem",
  "CarrollwoodHB" => "Carrollwood-HB",
  "CarverExceptional" => "Carver Exceptional",
  "CasselberryElementary" => "Casselberry Elementary",
  "CastleCreekElem" => "Castle Creek Elem",
  "CatalinaElem" => "Catalina Elem",
  "CelebrationK12" => "Celebration (K12)",
  "CentennialElem" => "Centennial Elem",
  "CentennialElementaryPO" => "Centennial Elementary-PO",
  "CentralAvenueElem" => "Central Avenue Elem",
  "CentralCityCharter" => "Central City Charter",
  "ChainOLakesElem" => "Chain O Lakes Elem",
  "ChascoElem" => "Chasco Elem",
  "ChascoElementaryPO" => "Chasco Elementary-PO",
  "CheneyElem" => "Cheney Elem",
  "ChesterTaylorElemo" => "Chester Taylor Elemo",
  "ChesterWTaylorElemenPO" => "Chester W Taylor Elemen-PO",
  "ChestnutElem" => "Chestnut Elem",
  "ChiaramonteElem" => "Chiaramonte Elem",
  "ChiaramonteHB" => "Chiaramonte-HB",
  "ChickasawElem" => "Chickasaw Elem",
  "ChilesElem" => "Chiles Elem",
  "ChilesHB" => "Chiles-HB",
  "ChisholmElem" => "Chisholm Elem",
  "ChurchwellElem" => "Churchwell Elem",
  "CiminoHB" => "Cimino-HB",
  "CitrusElem" => "Citrus Elem",
  "CitrusGroveElem" => "Citrus Grove Elementary",
  "CitrusParkElem" => "Citrus Park Elem",
  "CitrusParkHB" => "Citrus Park-HB",
  "ClairMelElem" => "Clair-Mel Elem",
  "ClairMelHB" => "Clair Mel-HB",
  "ClarconaElem" => "Clarcona Elem",
  "ClarenceBoswellElem" => "Clarence Boswell Elem",
  "ClarkElem" => "Clark Elem",
  "ClarkHB" => "Clark-HB",
  "ClaySpringsElem" => "Clay Springs Elem",
  "ClaywellElem" => "Claywell Elem",
  "ClaywellHB" => "Claywell-HB",
  "ClearviewAvenueElem" => "Clearview Avenue Elem",
  "ClearviewAvenueElemPN" => "Clearview Avenue Elem-PN",
  "ClermontElem" => "Clermont Elem",
  "ClevelandCourtElem" => "Cleveland Court Elem",
  "ClevelandElem" => "Cleveland Elem",
  "ClevelandHB" => "Cleveland-HB",
  "CollinsHB" => "Collins-HB",
  "ColonialElementary" => "Colonial Elementary",
  "ColsonElem" => "Colson Elem",
  "ColsonHB" => "Colson-HB",
  "ColumbiaElem" => "Columbia Elem",
  "CombeeElem" => "Combee Elem",
  "ConnertonElem" => "Connerton Elem",
  "ConwayElem" => "Conway Elem",
  "CorkElem" => "Cork Elem",
  "CorkHB" => "Cork-HB",
  "CoronadoBeachElem" => "Coronado Beach Elem",
  "CorrHB" => "Corr-HB",
  "CoteeRiverElem" => "Cotee River Elem",
  "CoteeRiverElementaryPO" => "Cotee River Elementary-PO",
  "CranberryElementary" => "Cranberry Elementary",
  "CrestwoodElem" => "Crestwood Elem",
  "CrestwoodHB" => "Crestwood-HB",
  "CrossBayouElem" => "Cross Bayou Elem",
  "CrossBayouElementaryPN" => "Cross Bayou Elementary-PN",
  "CrystalLakeElem" => "Crystal Lake Elem",
  "CrystalLakeElementary" => "Crystal Lake Elementary",
  "CurlewCreekElem" => "Curlew Creek Elem",
  "CurlewCreekElementaryPN" => "Curlew Creek Elementary-PN",
  "CurtisFundamentalElem" => "Curtis Fundamental Elem",
  "CurtisFundamentalPN" => "Curtis Fundamental-PN",
  "CypressCreekElem" => "Cypress Creek Elem",
  "CypressCreekElementary" => "Cypress Creek Elementary",
  "CypressCreekHB" => "Cypress Creek-HB",
  "CypressElem" => "Cypress Elem",
  "CypressElementaryPO" => "Cypress Elementary-PO",
  "CypressElemK6" => "Cypress Elem (K 6)",
  "CypressParkElem" => "Cypress Park Elem",
  "CypressRidgeElem" => "Cypress Ridge Elem",
  "CypressSpringsElem" => "Cypress Springs Elem",
  "CypressWoodsElem" => "Cypress Woods Elem",
  "CypressWoodsElementaryPN" => "Cypress Woods Elementary-PN",
  "DabneyElem" => "Dabney Elem",
  "DaleMabryElementaryHB" => "Dale Mabry Elementary-HB",
  "DavenportElem" => "Davenport Elem",
  "DavidEBaileyJrElemHB" => "David E. Bailey Jr. Elem-HB",
  "DavisHB" => "Davis-HB",
  "DebaryElem" => "Debary Elem",
  "DeepCreekElementary" => "Deep Creek Elementary",
  "DeerParkElem" => "Deer Park Elem",
  "DeerParkElementaryPO" => "Deer Park Elementary-PO",
  "DeerParkElemHB" => "Deer Park Elem-HB",
  "DeerwoodElemK5" => "Deerwood Elem (K 5)",
  "DeerwoodElemOrangeCty" => "Deerwood Elem (Orange Cty)",
  "DeerwoodElemOsceolaCty" => "Deerwood Elem (Osceola Cty)",
  "DeesPtecClearwater" => "Dees - Ptec Clearwater",
  "DeesPtecStPete" => "Dees - Ptec St. Pete",
  "DeltonaLakesElem" => "Deltona Lakes Elem",
  "DenhamOaksElem" => "Denham Oaks Elem",
  "DenhamOaksElementaryPO" => "Denham Oaks Elementary-PO",
  "DesotoElem" => "Desoto Elem",
  "DeSotoHB" => "DeSoto-HB",
  "DickensonElem" => "Dickenson Elem",
  "DickensonHB" => "Dickenson-HB",
  "DillardStreetElem" => "Dillard Street Elem",
  "DiplomatElementary" => "Diplomat Elementary",
  "DiscoveryElem" => "Discovery Elem",
  "DiscoveryIntermedia48" => "Discovery Intermedia (4-8)",
  "DixielandElem" => "Dixieland Elem",
  "DobyElementaryHB" => "Doby Elementary-HB",
  "DommerichElem" => "Dommerich Elem",
  "DorothyThomas" => "Dorothy Thomas",
  "DoubleBranchElementary" => "Double Branch Elementary",
  "DouglasLJamersonElemPN" => "Douglas L. Jamerson Elem-PN",
  "DoverElem" => "Dover Elem",
  "DoverHB" => "Dover-HB",
  "DoverShoresElem" => "Dover Shores Elem",
  "DreamLakeElem" => "Dream Lake Elem",
  "DrNERobertsElem" => "Dr. N. E Roberts Elem",
  "DropoutPrevention" => "Dropout Prevention",
  "DrPhillipsElem" => "Dr. Phillips Elem",
  "DuetteElementary" => "Duette Elementary",
  "DunbarHB" => "Dunbar-HB",
  "DunbarMagnet" => "Dunbar Magnet",
  "DundeeElem" => "Dundee Elem",
  "DunedinElem" => "Dunedin Elem",
  "DunedinElementaryPN" => "Dunedin Elementary-PN",
  "DurranceElem" => "Durrance Elem",
  "EagleLakeElem" => "Eagle Lake Elem",
  "EaglesNestElem" => "Eagles Nest Elem",
  "EastbrookElementary" => "Eastbrook Elementary",
  "EastElementary" => "East Elementary",
  "EastLakeElem" => "East Lake Elem",
  "EastsideElem" => "Eastside Elem",
  "EastsideMultiCult" => "Eastside Multi-Cult",
  "EcclestonElem" => "Eccleston Elem",
  "EdgarLPadgettElem" => "Edgar L. Padgett Elem",
  "EdgewaterPublicSchool" => "Edgewater Public School",
  "EdgewoodAcademySchool" => "Edgewood Academy School",
  "EdisonElem" => "Edison Elem",
  "EdisonHB" => "Edison-HB",
  "EdisonParkCreativeArts" => "Edison Park Creative and Expressive Arts School",
  "EdithIStarkeElem" => "Edith I Starke Elem",
  "EgyptLakeElem" => "Egypt Lake Elem",
  "EgyptLakeHB" => "Egypt Lake-HB",
  "EisenhowerElem" => "Eisenhower Elem",
  "EisenhowerElementaryPN" => "Eisenhower Elementary-PN",
  "EisenhowerMiddle" => "Eisenhower Middle",
  "ElbertElem" => "Elbert Elem",
  "ElemSchool4" => "Elem School 4",
  "EmmaEBookerElementary" => "Emma E. Booker Elementary",
  "EndeavorElem" => "Endeavor Elem",
  "EnglewoodElem" => "Englewood Elem",
  "EnglewoodElementary" => "Englewood Elementary",
  "EnglishEstatesElementary" => "English Estates Elementary",
  "EnterpriseElem" => "Enterprise Elem",
  "EssrigElem" => "Essrig Elem",
  "EssrigHB" => "Essrig-HB",
  "EuclidAvenueLearningCenter" => "Euclid Avenue Learni",
  "EustisElem" => "Eustis Elem",
  "EustisHeightsElem" => "Eustis Heights Elem",
  "EvansElementary" => "Evans Elementary",
  "FairmountParkElem" => "Fairmount Park Elem",
  "FairmountParkElementaryPN" => "Fairmount Park Elementary-PN",
  "FernCreekElem" => "Fern Creek Elem",
  "FishhawkCreekHB" => "Fishhawk Creek-HB",
  "FloralAvenueElem" => "Floral Avenue Elem",
  "FloralRidgeElementary" => "Floral Ridge Elementary",
  "FlorineJAbelElementary" => "Florine J. Abel Elementary",
  "FolsomElem" => "Folsom Elem",
  "FolsomHB" => "Folsom-HB",
  "ForestCityElementary" => "Forest City Elementary",
  "ForestHillsElem" => "Forest Hills Elem",
  "ForestHillsHB" => "Forest Hills-HB",
  "ForestLakeElem" => "Forest Lake Elem",
  "ForestLakesElem" => "Forest Lakes Elem",
  "ForestLakesElementaryPN" => "Forest Lakes Elementary-PN",
  "FortMyersbeachElementary" => "Fort Myers beach Elementary",
  "FosterElem" => "Foster Elem",
  "FosterHB" => "Foster-HB",
  "FourCornerElem" => "Four Corner Elem",
  "FoxHollowElem" => "Fox Hollow Elem",
  "FoxHollowElementaryPO" => "Fox Hollow Elementary-PO",
  "FrancesWakelandElementary" => "Frances Wakeland Elementary",
  "FrangusElem" => "Frangus Elem",
  "FrankEBrighamAcademy" => "Frank E. Brigham Academy",
  "FranklinParkMagnet" => "Franklin Park Magnet",
  "FredGGarnierElem" => "Fred G. Garnier Elem",
  "FreedomElem" => "Freedom Elem",
  "FreedomElementary" => "Freedom Elementary",
  "FriendshipElem" => "Friendship Elem",
  "FrontierElem" => "Frontier Elem",
  "FrontierElementaryPN" => "Frontier Elementary-PN",
  "FrostElementarySchool" => "Frost Elementary School",
  "FrostproofElem" => "Frostproof Elem",
  "FruitlandParkElem" => "Fruitland Park Elem",
  "FruitvilleElementary" => "Fruitville Elementary",
  "FuguittElem" => "Fuguitt Elem",
  "FuguittElementaryPN" => "Fuguitt Elementary-PN",
  "GardenElementary" => "Garden Elementary",
  "GardenGroveElem" => "Garden Grove Elem",
  "GarnerElem" => "Garner Elem",
  "GarrisonJonesElem" => "Garrison-Jones Elem",
  "GarrisonJonesElementaryPN" => "Garrison-Jones Elementary-PN",
  "GatewayElementary" => "Gateway Elementary",
  "GauseRiversideAcademy" => "Gause Riverside Academy",
  "GenevaElementary" => "Geneva Elementary",
  "GeneWittElementary" => "Gene Witt Elementary",
  "GeorgeMarksElem" => "George Marks Elem",
  "GibbonsStreetElem" => "Gibbons Street Elem",
  "GibsontonElem" => "Gibsonton Elem",
  "GibsontonHB" => "Gibsonton-HB",
  "GlenallenElementary" => "Glenallen Elementary",
  "GocioElementary" => "Gocio Elementary",
  "GoldsboroElemMagnet" => "Goldsboro Elem Magnet",
  "GorrieElem" => "Gorrie Elem",
  "GorrieHB" => "Gorrie-HB",
  "GradyElem" => "Grady Elem",
  "GradyHB" => "Grady-HB",
  "GrahamElem" => "Graham Elem",
  "GrahamHB" => "Graham-HB",
  "GrandAveElem" => "Grand Ave. Elem",
  "GrandAvenueElem" => "Grand Avenue Elem",
  "GrassyLakeElementary" => "Grassy Lake Elementary",
  "GriffinElem" => "Griffin Elem",
  "GrovelandElem" => "Groveland Elem",
  "GulfBeachesElem" => "Gulf Beaches Elem",
  "GulfBeachesElementaryPN" => "Gulf Beaches Elementary-PN",
  "GulfElementary" => "Gulf Elementary",
  "GulfGateElementary" => "Gulf Gate Elementary",
  "GulfHighlandElementary" => "Gulf Highland Elementary",
  "GulfportElem" => "Gulfport Elem",
  "GulfportElementaryPN" => "Gulfport Elementary-PN",
  "GulfsideElem" => "Gulfside Elem",
  "GulfsideElementaryPO" => "Gulfside Elementary-PO",
  "GulfTraceElementary" => "Gulf Trace Elementary",
  "GullettElementary" => "Gullett Elementary",
  "GWeaverHipps" => "G. Weaver Hipps",
  "HamiltonDisston" => "Hamilton Disston",
  "HamiltonElementary" => "Hamilton Elementary",
  "HammondElementarySchool" => "Hammond Elementary School",
  "HancockCreekElementary" => "Hancock Creek Elementary",
  "HarmonyCommunityK8" => "Harmony Community School (K-8)",
  "HarnsMarshElementary" => "Harns Marsh Elementary",
  "HartridgeAcademy" => "Hartridge Academy",
  "Headstart" => "Headstart",
  "HeathrowElementary" => "Heathrow Elementary",
  "HectorACafferataJrElem" => "Hector A. Cafferata, Jr. Elementary",
  "HeightsElementary" => "Heights Elementary",
  "HeritageHB" => "Heritage-HB",
  "HiawasseeElem" => "Hiawassee Elem",
  "HickoryTreeElem" => "Hickory Tree Elem",
  "HiddenOaksElem" => "Hidden Oaks Elem",
  "HighlandCityElem" => "Highland City Elem",
  "HighlandGrove" => "Highland Grove",
  "HighlandGroveElem" => "Highland Grove Elem",
  "HighlandLakesElem" => "Highland Lakes Elem",
  "HighlandLakesElementaryPN" => "Highland Lakes Elementary-PN",
  "HighlandsElem" => "Highlands Elem",
  "HighlandsElementary" => "Highlands Elementary",
  "HighPointElem" => "High Point Elem",
  "HighPointElementaryPN" => "High Point Elementary-PN",
  "HillcrestElem" => "Hillcrest Elem",
  "HollyHillElem" => "Holly Hill Elem",
  "HomerKAddairCareerAcademy" => "Homer K Addair Career Academy",
  "HorizonElem" => "Horizon Elem",
  "HorizonsElementary" => "Horizons Elementary",
  "HospitalHomebound" => "Hospital/Homebound",
  "HudsonElem" => "Hudson Elem",
  "HudsonElementaryPO" => "Hudson Elementary-PO",
  "HungerfordElem" => "Hungerford Elem",
  "HuntersCreekElem" => "Hunter&apos;s Creek Elem",
  "HuntersGreenElem" => "Hunters Green Elem",
  "HuntersGreenHB" => "Hunter&apos;s Green-HB",
  "IdaMStewartElementary" => "Ida M. Stewart Elementary",
  "IdyllwildeElementary" => "Idyllwilde Elementary",
  "IndianRiverElem" => "Indian River Elem",
  "InwoodElem" => "Inwood Elem",
  "IppolitoHB" => "Ippolito-HB",
  "IveyLaneElem" => "Ivey Lane Elem",
  "JacksonElem" => "Jackson Elem",
  "JacksonHB" => "Jackson-HB",
  "JamesEStephensElem" => "James E. Stephens Elem",
  "JamesExceptional" => "James Exceptional",
  "JamesK8HB" => "James K-8-HB",
  "JamesMMarloweElem" => "James M. Marlowe Elem",
  "JamesMMarlowElementaryPO" => "James M Marlow Elementary-PO",
  "JamesSanderlinElementaryPN" => "James Sanderlin Elementary-PN",
  "JamesStephensInternational" => "James Stephens International",
  "JamesTillmanElementary" => "James Tillman Elementary",
  "JamesWSikesElem" => "James W Sikes Elem",
  "JanieHowardWilsonElem" => "Janie Howard Wilson Elem",
  "JCollinEnglishElementary" => "J. Collin English Elementary",
  "JesseKeenElem" => "Jesse Keen Elem",
  "JewettSchooloftheArts" => "Jewett School of the Arts",
  "JohnMSextonElem" => "John M. Sexton Elem",
  "JohnMSextonElementaryPN" => "John M Sexton Elementary-PN",
  "JohnSnivelyElem" => "John Snively Elem",
  "JohnYoungElem" => "John Young Elem",
  "JustEarlyChildhood" => "Just Early Childhood",
  "JustHB" => "Just-HB",
  "JuvenileJustice" => "Juvenile Justice",
  "KaleyElem" => "Kaley Elem",
  "KathleenElem" => "Kathleen Elem",
  "KeeneCrossingElementary" => "Keene Crossing Elementary",
  "KeethElementary" => "Keeth Elementary",
  "KenlyElem" => "Kenly Elem",
  "KenlyHB" => "Kenly-HB",
  "KillarneyElem" => "Killarney Elem",
  "KimbellElementarySchool" => "Kimbell Elementary School",
  "KingsfordElem" => "Kingsford Elem",
  "KingsHighwayElem" => "Kings Highway Elem",
  "KingsHighwayElementaryPN" => "Kings Highway Elementary-PN",
  "Kingsway" => "Kingsway",
  "KingswoodElementary" => "Kingswood Elementary",
  "KingswoodHB" => "Kingswood-HB",
  "KinnanElementary" => "Kinnan Elementary",
  "KissimmeeElem" => "Kissimmee Elem",
  "KnightsElem" => "Knights Elem",
  "KnightsHB" => "Knights-HB",
  "KoaElementary" => "Koa Elementary",
  "LacoocheeElem" => "Lacoochee Elem",
  "LacoocheeElementaryPO" => "Lacoochee Elementary-PO",
  "LakeAlfredElem" => "Lake Alfred Elem",
  "LakeComoElem" => "Lake Como Elem",
  "LakeGemElem" => "Lake Gem Elem",
  "LakeGeorgeElem" => "Lake George Elem",
  "LakeGibsonSenior" => "Lake Gibson Senior",
  "LakeMagadleneHB" => "Lake Magadlene-HB",
  "LakeMagdaleneElem" => "Lake Magdalene Elem",
  "LakeMarionCreekElementary" => "Lake Marion Creek Elementary",
  "LakeMaryElementary" => "Lake Mary Elementary",
  "LakemontElem" => "Lakemont Elem",
  "LakeMyrtleElem" => "Lake Myrtle Elem",
  "LakeMyrtleElementaryPO" => "Lake Myrtle Elementary-PO",
  "LakeOrientaElementary" => "Lake Orienta Elementary",
  "LakePanasoffkeeElementary" => "Lake Panasoffkee Elementary",
  "LakeShippElem" => "Lake Shipp Elem",
  "LakeSilverElem" => "Lake Silver Elem",
  "LakeStGeorgeElem" => "Lake St. George Elem",
  "LakeStGeorgeElementaryPN" => "Lake St George Elementary-PN",
  "LakeSybeliaElem" => "Lake Sybelia Elem",
  "LakeviewElementary" => "Lakeview Elementary",
  "LakeviewElemK5" => "Lakeview Elem (K 5)",
  "LakeviewFundamental" => "Lakeview Fundamental",
  "LakeviewFundamentalPN" => "Lakeview Fundamental-PN",
  "LakevilleElem" => "Lakeville Elem",
  "LakeWestonElem" => "Lake Weston Elem",
  "LakeWhitneyElem" => "Lake Whitney Elem",
  "LakewoodElem" => "Lakewood Elem",
  "LakewoodElementaryPN" => "Lakewood Elementary-PN",
  "LamarqueElementary" => "Lamarque Elementary",
  "LancasterElem" => "Lancaster Elem",
  "LanierElem" => "Lanier Elem",
  "LanierHB" => "Lanier-HB",
  "LargoCentralElem" => "Largo Central Elem",
  "LargoCentralElementaryPN" => "Largo Central Elementary-PN",
  "LaurelElementary" => "Laurel Elementary",
  "LaurelNokomisElementary" => "Laurel Nokomis Elementary",
  "LavoyExceptional" => "Lavoy Exceptional",
  "LawtonChilesElem" => "Lawton Chiles Elem",
  "LawtonElementary" => "Lawton Elementary",
  "LayerElementary" => "Layer Elementary",
  "LealmanAvenueElem" => "Lealman Avenue Elem",
  "LealmanAvenueElementaryPN" => "Lealman Avenue Elementary-PN",
  "LearningGate" => "Learning Gate",
  "LeeElem" => "Lee Elem",
  "LeeHB" => "Lee-HB",
  "LeesburgElementary" => "Leesburg Elementary",
  "LehighAcresElementary" => "Lehigh Acres Elementary",
  "LeilaDavisElem" => "Leila Davis Elem",
  "LeilaGDavisElementaryPN" => "Leila G Davis Elementary-PN",
  "LenaVistaElem" => "Lena Vista Elem",
  "LewisElem" => "Lewis Elem",
  "LewisHB" => "Lewis-HB",
  "LibertyElementary" => "Liberty Elementary",
  "LicolnAvenueAcademy" => "Licoln Avenue Academy",
  "LimeStreetElem" => "Lime Street Elem",
  "LimonaElem" => "Limona Elem",
  "LimonaHB" => "Limona-HB",
  "LincolnHB" => "Lincoln-HB",
  "LincolnMagnet" => "Lincoln Magnet",
  "LithiaSpringsElem" => "Lithia Springs Elem",
  "LithiaSpringsHB" => "Lithia Springs-HB",
  "LittleRiverElem" => "Little River Elem",
  "LockhartElem" => "Lockhart Elem",
  "LockhartHB" => "Lockhart-HB",
  "LomaxElem" => "Lomax Elem",
  "LomaxElementarySchool" => "Lomax Elementary School",
  "LongleafElementaryPO" => "Longleaf Elementary-PO",
  "LongwoodElementary" => "Longwood Elementary",
  "LopezElem" => "Lopez Elem",
  "LopezHB" => "Lopez-HB",
  "LostLakeElem" => "Lost Lake Elem",
  "LoughmanOaksElem" => "Loughman Oaks Elem",
  "LouiseSMcInnisElem" => "Louise S. McInnis Elem",
  "LovellElem" => "Lovell Elem",
  "LowryElem" => "Lowry Elem",
  "LowryHB" => "Lowry-HB",
  "LutzElem" => "Lutz Elem",
  "LutzHB" => "Lutz-HB",
  "LynchElem" => "Lynch Elem",
  "LynchElementaryPN" => "Lynch Elementary-PN",
  "MabryElem" => "Mabry Elem",
  "MadeiraBeachElem" => "Madeira Beach Elem",
  "MadeiraBeachElementaryPN" => "Madeira Beach Elementary-PN",
  "ManateeCoveElem" => "Manatee Cove Elem",
  "ManateeElementary" => "Manatee Elementary",
  "MangoElem" => "Mango Elem",
  "MangoHB" => "Mango-HB",
  "ManhattanElem" => "Manhattan Elem",
  "ManiscalcoElem" => "Maniscalco Elem",
  "ManiscalcoHB" => "Maniscalco-HB",
  "MarjorieKinnanRawlings" => "Marjorie Kinnan Rawlings",
  "MaryGiellaElem" => "Mary Giella Elem",
  "MaryGiellaElementaryPO" => "Mary Giella Elementary-PO",
  "MascotteElem" => "Mascotte Elem",
  "MaxeyElem" => "Maxey Elem",
  "MaximoElem" => "Maximo Elem",
  "MaximoElementaryPN" => "Maximo Elementary-PN",
  "McCoyElem" => "Mc Coy Elem",
  "McdonaldElem" => "Mcdonald Elem",
  "McDonaldHB" => "McDonald-HB",
  "McinnisElem" => "Mcinnis Elem",
  "McKeelElementary" => "McKeel Elementary",
  "MckitrickElem" => "Mckitrick Elem",
  "McKitrickHB" => "McKitrick-HB",
  "McmullenBoothElem" => "Mcmullen-Booth Elem",
  "McMullenBoothElementaryPN" => "McMullen-Booth Elementary-PN",
  "McNealElementary" => "McNeal Elementary",
  "MeachamEarlyChild" => "Meacham Early Child",
  "MeadowParkElementary" => "Meadow Park Elementary",
  "MeadowWoodsElem" => "Meadow Woods Elem",
  "MedullaElem" => "Medulla Elem",
  "MelroseElem" => "Melrose Elem",
  "MelroseElementaryPN" => "Melrose Elementary-PN",
  "MemorialElementarySchool" => "Memorial Elementary School",
  "MendenhallElem" => "Mendenhall Elem",
  "MendenhallHB" => "Mendenhall-HB",
  "MendezSedCtr" => "Mendez Sed Ctr",
  "MetropolitanMin" => "Metropolitan Min",
  "MetroWestElem" => "Metro West Elem",
  "MichiganAvenueElemK5" => "Michigan Avenue Elem (K 5)",
  "MichiganInternational" => "Michigan International",
  "MidwayElementary" => "Midway Elementary",
  "MildredHelmsElem" => "Mildred Helms Elem",
  "MildredHelmsElementaryPN" => "Mildred Helms Elementary-PN",
  "MilesElem" => "Miles Elem",
  "MilesHB" => "Miles-HB",
  "MillCreekElemK5" => "Mill Creek Elem (K 5)",
  "MillenniaElementary" => "Millennia Elementary",
  "MillerElementary" => "Miller Elementary",
  "MillsElementary" => "Virgil Mills Elementary",
  "MinneolaElem" => "Minneola Elem",
  "MintzElem" => "Mintz Elem",
  "MintzHB" => "Mintz-HB",
  "MirrorLakesElementary" => "Mirror Lakes Elementary",
  "MitchellElem" => "Mitchell Elem",
  "MitchellHB" => "Mitchell-HB",
  "MittyePLockeElem" => "Mittye P. Locke Elem",
  "MittyePLockePO" => "Mittye P. Locke-PO",
  "MollieRayElem" => "Mollie Ray Elem",
  "MoodyElementary" => "Moody Elementary",
  "MoonLakeElem" => "Moon Lake Elem",
  "MoonLakePO" => "Moon Lake-PO",
  "MooreMickensEduc" => "Moore-Mickens Educ",
  "MorganWoodsElem" => "Morgan Woods Elem",
  "MorganWoodsHB" => "Morgan Woods-HB",
  "MortElem" => "Mort Elem",
  "MortHB" => "Mort-HB",
  "MosiElementaryHB" => "Mosi Elementary-HB",
  "MossParkElementary" => "Moss Park Elementary",
  "MountVernonElementaryPN" => "Mount Vernon Elementary-PN",
  "MtVernonElem" => "Mt. Vernon Elem",
  "MullerHB" => "Muller-HB",
  "MyakkaCityElementary" => "Myakka City Elementary",
  "MyakkaRiverElementary" => "Myakka River Elementary",
  "NarcooseeCommunity" => "Narcoosee Community",
  "NeilArmstrongElementary" => "Neil Armstrong Elementary",
  "NelsonHB" => "Nelson-HB",
  "NeptuneElementary" => "Neptune Elementary",
  "NewHeightsElementaryPN" => "New Heights Elementary-PN",
  "NewRiverElementary" => "New River Elementary",
  "NFtMyersAcadforArts" => "North Fort Myers Academy for the Arts",
  "NinaHarrisExceptional" => "Nina Harris Exceptional",
  "NocateeElementarySchool" => "Nocatee Elementary School",
  "NorthLakelandElem" => "North Lakeland Elem",
  "NorthlakeParkCommunity" => "Northlake Park Community",
  "NorthlakeParkElem" => "Northlake Park Elem",
  "NorthPortElemGlenAllen" => "North Port Elem Glen Allen",
  "NorthPortElemToledoBlade" => "North Port Elem Toledo Blade",
  "NorthShoreElem" => "North Shore Elem",
  "NorthShoreElementaryPN" => "North Shore Elementary-PN",
  "NorthWardElem" => "North Ward Elem",
  "NorthWardElementaryPN" => "North Ward Elementary-PN",
  "NorthwestElem" => "Northwest Elem",
  "NorthwestElementaryPN" => "Northwest Elementary-PN",
  "NorthwestElementaryPO" => "Northwest Elementary-PO",
  "NorthwestHB" => "Northwest-HB",
  "OakGroveElem" => "Oak Grove Elem",
  "OakHillElem" => "Oak Hill Elem",
  "OakhurstElem" => "Oakhurst Elem",
  "OakhurstElementaryPN" => "Oakhurst Elementary-PN",
  "OakParkElem" => "Oak Park Elem",
  "OakParkHB" => "Oak Park-HB",
  "OakshireElem" => "Oakshire Elem",
  "OaksteadElementaryPO" => "Oakstead Elementary-PO",
  "OcoeeElem" => "Ocoee Elem",
  "OdessaElementary" => "Odessa Elementary",
  "OldsmarElem" => "Oldsmar Elem",
  "OldsmarElementaryPN" => "Oldsmar Elementary-PN",
  "OnecoElementary" => "Oneco Elementary",
  "OrangeCenterElem" => "Orange Center Elem",
  "OrangeCityElem" => "Orange City Elem",
  "OrangeGroveElem" => "Orange Grove Elem",
  "OrangeGroveElementaryPN" => "Orange Grove Elementary-PN",
  "OrangeGroveHB" => "Orange Grove-HB",
  "OrangeRidgeBullockElem" => "Orange Ridge Bullock Elementary",
  "OrangeRiverElementary" => "Orange River Elementary",
  "OrangewoodElementary" => "Orangewood Elementary",
  "OrloVistaElem" => "Orlo Vista Elem",
  "OrmondBeachElem" => "Ormond Beach Elem",
  "OrtonaElem" => "Ortona Elem",
  "OscarJPopeElem" => "Oscar J Pope Elem",
  "OsceolaElem" => "Osceola Elem",
  "OsteenElem" => "Osteen Elem",
  "Other" => "Other",
  "OzonaElem" => "Ozona Elem",
  "OzonaElementaryPN" => "Ozona Elementary-PN",
  "PadgettElem" => "Padgett Elem",
  "PalmaSolaElementary" => "Palma Sola Elementary",
  "PalmettoElem" => "Palmetto Elem",
  "PalmettoElementary" => "Palmetto Elementary",
  "PalmHarborElem" => "Palm Harbor Elem",
  "PalmHarborElementaryPN" => "Palm Harbor Elementary-PN",
  "PalmLakeElem" => "Palm Lake Elem",
  "PalmRiverElem" => "Palm River Elem",
  "PalmRiverHB" => "Palm River-HB",
  "PalmTerraceElem" => "Palm Terrace Elem",
  "PalmViewElementary" => "Palm View Elementary",
  "ParkhillExceptional" => "Parkhill Exceptional",
  "PartinElementary" => "Partin Elementary",
  "PartinSettlementElem" => "Partin Settlement Elem",
  "PasadenaFundamental" => "Pasadena Fundamental",
  "PasadenaFundamentalPN" => "Pasadena Fundamental-PN",
  "PascoElem" => "Pasco Elem",
  "PascoElementarySchoolPO" => "Pasco Elementary School-PO",
  "PathwaysElem" => "Pathways Elem",
  "PatriotElementary" => "Patriot Elementary",
  "PaulBStephensExcep" => "Paul B. Stephens Excep",
  "PeaceRiverElementary" => "Peace River Elementary",
  "PelicanElementary" => "Pelican Elementary",
  "PerkinsElem" => "Perkins Elem",
  "PerkinsElementaryPN" => "Perkins Elementary-PN",
  "PershingElem" => "Pershing Elem",
  "PhilipOBrienElementary" => "Philip OBrien Elementary",
  "PhilipsShoreArtsElem" => "Philips Shore Arts Elementary School",
  "PhillipiShoresElementary" => "Phillippi Shores Elementary",
  "PiersonElem" => "Pierson Elem",
  "PinarElem" => "Pinar Elem",
  "PineCastleElem" => "Pine Castle Elem",
  "PinecrestElem" => "Pinecrest Elem",
  "PineCrestElementary" => "Pine Crest Elementary",
  "PinecrestHB" => "Pinecrest-HB",
  "PineHillsElem" => "Pine Hills Elem",
  "PineIslandElementary" => "Pine Island Elementary",
  "PinellasCentralElem" => "Pinellas Central Elem",
  "PinellasCentralElemPN" => "Pinellas Central Elem-PN",
  "PinellasJuvenile" => "Pinellas Juvenile",
  "PinellasParkElem" => "Pinellas Park Elem",
  "PinellasParkElementaryPN" => "Pinellas Park Elementary-PN",
  "PinelochElem" => "Pineloch Elem",
  "PineRidgeElem" => "Pine Ridge Elem",
  "PineTrailElem" => "Pine Trail Elem",
  "PineviewElementary" => "Pineview Elementary",
  "PineViewElementaryPO" => "Pine View Elementary-PO",
  "PinewoodElem" => "Pinewood Elem",
  "PinewoodsElementary" => "Pinewoods Elementary",
  "PizzoElem" => "Pizzo Elem",
  "PizzoHB" => "Pizzo-HB",
  "PlantCityExceptional" => "Plant City Exceptional",
  "PleasantHillElem" => "Pleasant Hill Elem",
  "PlumbElem" => "Plumb Elem",
  "PlumbElementaryPN" => "Plumb Elementary-PN",
  "PoincianaElem" => "Poinciana Elem",
  "PolkAvenueElem" => "Polk Avenue Elem",
  "PolkCityElem" => "Polk City Elem",
  "PonceDeLeonElem" => "Ponce De Leon Elem",
  "PoncedeLeonElementaryPN" => "Ponce de Leon Elementary-PN",
  "PortOrangeElem" => "Port Orange Elem",
  "PotterEarlyChildhood" => "Potter Early Childhood",
  "PotterHB" => "Potter-HB",
  "PrideElem" => "Pride Elem",
  "PrideElementary" => "Pride Elementary",
  "PrideHB" => "Pride-HB",
  "PrincetonElem" => "Princeton Elem",
  "ProgressVillageElemHB" => "Progress Village Elem-HB",
  "PurcellElem" => "Purcell Elem",
  "QuailHollowElem" => "Quail Hollow Elem",
  "QuailHollowElementaryPO" => "Quail Hollow Elementary-PO",
  "RainbowElementary" => "Rainbow Elementary",
  "RawlingsElementaryPN" => "Rawlings Elementary-PN",
  "RaymaCPageElementary" => "Rayma C. Page Elementary",
  "RayVPottorfElementary" => "Ray V. Pottorf Elementary",
  "RBCoxElem" => "R. B. Cox Elem",
  "RBruceWagnerElem" => "R. Bruce Wagner Elem",
  "RClemChurchwellElem" => "R. Clem Churchwell Elem",
  "ReadingEdgeAcademyK4" => "Reading Edge Academy (K-4)",
  "ReadPattilloElem" => "Read Pattillo Elem",
  "RebirthAcademy" => "Rebirth Academy",
  "RedBugElementary" => "Red Bug Elementary",
  "ReddickElementarySchool" => "Reddick Elementary School",
  "RedlandsChristian" => "Redlands Christian",
  "ReedyCreekElemK5" => "Reedy Creek Elem (K 5)",
  "RenaissanceProgressive" => "Renaissance Progressive",
  "RichardLSanders" => "Richard L. Sanders",
  "RichardsonAcademy" => "Richardson Academy",
  "RicheyElem" => "Richey Elem",
  "RicheyElementaryPO" => "Richey Elementary-PO",
  "RichmondHeightsElem" => "Richmond Heights Elem",
  "RidgecrestElem" => "Ridgecrest Elem",
  "RidgecrestElementaryPN" => "Ridgecrest Elementary-PN",
  "RidgeviewElem" => "Ridgeview Elem",
  "RidgewoodParkElem" => "Ridgewood Park Elem",
  "RimesElem" => "Rimes Elem",
  "RioVistaElem" => "Rio Vista Elem",
  "RioVistaElementaryPN" => "Rio Vista Elementary-PN",
  "RiverdaleElem" => "Riverdale Elem",
  "RiverHallElementary" => "River Hall Elementary",
  "RiverhillsElem" => "Riverhills Elem",
  "RiverhillsHB" => "Riverhills-HB",
  "RiversideElem" => "Riverside Elem",
  "RiverviewElem" => "Riverview Elem",
  "RiverviewElemSchoolHB" => "Riverview Elem School-HB",
  "RJLongstreetElem" => "R.J. Longstreet Elem",
  "RobertEWillisElementary" => "Robert E Willis Elementary",
  "RobertHPrineElementary" => "Robert H. Prine Elementary",
  "RobinsonElem" => "Robinson Elem",
  "RobinsonElementarySchoolHB" => "Robinson Elementary School-HB",
  "RoblesElem" => "Robles Elem",
  "RoblesHB" => "Robles-HB",
  "RochelleSchoolofArts" => "Rochelle School of Arts",
  "RockLakeElem" => "Rock Lake Elem",
  "RockSpringsElem" => "Rock Springs Elem",
  "RodneyBCoxElementaryPO" => "Rodney B. Cox Elementary-PO",
  "RolandParkHB" => "Roland Park-HB",
  "RollingHillsElem" => "Rolling Hills Elem",
  "RooseveltElem" => "Roosevelt Elem",
  "RooseveltHB" => "Roosevelt-HB",
  "RosabelleElem" => "Rosabelle Elem",
  "RosemontElem" => "Rosemont Elem",
  "RossEJeffriesElemK5" => "Ross E. Jeffries Elem",
  "RoundLakeElem" => "Round Lake Elem",
  "RowlettElementary" => "Rowlett Elementary",
  "RuskinElem" => "Ruskin Elem",
  "RuskinHB" => "Ruskin-HB",
  "SabalPointElementary" => "Sabal Point Elementary",
  "SadlerElem" => "Sadler Elem",
  "SafetyHarborElem" => "Safety Harbor Elem",
  "SafetyHarborElementaryPN" => "Safety Harbor Elementary-PN",
  "SallieJonesElementary" => "Sallie Jones Elementary",
  "SamosetElementary" => "Samoset Elementary",
  "SamsulaElem" => "Samsula Elem",
  "SanAntonioElem" => "San Antonio Elem",
  "SanAntonioPO" => "San Antonio-PO",
  "SanCarlosParkElementary" => "San Carlos Park Elementary",
  "SandersElem" => "Sanders Elem",
  "SandersMemorialElemen" => "Sanders Memorial Elemen",
  "SandhillElem" => "Sandhill Elem",
  "SandLakeElem" => "Sand Lake Elem",
  "SandPineElem" => "Sand Pine Elem",
  "SandPineElementaryPO" => "Sand Pine Elementary-PO",
  "SandyLaneElem" => "Sandy Lane Elem",
  "SandyLaneElementaryPN" => "Sandy Lane Elementary-PN",
  "SanJoseElem" => "San Jose Elem",
  "SanJoseElementaryPN" => "San Jose Elementary-PN",
  "SawgrassbayElementary" => "Sawgrass bay Elementary",
  "SawgrassLakeElem" => "Sawgrass Lake Elem",
  "SawgrassLakeElementaryPN" => "Sawgrass Lake Elementary-PN",
  "SchmidtHB" => "Schmidt-HB",
  "SchraderElem" => "Schrader Elem",
  "SchraderElementaryPO" => "Schrader Elementary-PO",
  "SchwarzkopfElem" => "Schwarzkopf Elem",
  "SchwarzkopfHB" => "Schwarzkopf-HB",
  "ScottLakeElem" => "Scott Lake Elem",
  "SeaBreezeElementary" => "Sea Breeze Elementary",
  "SeffnerElem" => "Seffner Elem",
  "SeffnerHB" => "Seffner-HB",
  "SeminoleElem" => "Seminole Elem",
  "SeminoleElementaryPN" => "Seminole Elementary-PN",
  "SeminoleHB" => "Seminole-HB",
  "SeminoleSpringsElem" => "Seminole Springs. Elem",
  "SessumsHB" => "Sessums-HB",
  "SevenOaksElementaryPO" => "Seven Oaks Elementary-PO",
  "SevenSpringsElem" => "Seven Springs Elem",
  "SevenSpringsElementaryPO" => "Seven Springs Elementary-PO",
  "SeventyFourthStElemPN" => "Seventy-Fourth St. Elem-PN",
  "SeventyFourthStreet" => "Seventy-Fourth Street",
  "SevillePublicSchool" => "Seville Public School",
  "ShadyHillsElem" => "Shady Hills Elem",
  "ShadyHillsElementaryPO" => "Shady Hills Elementary-PO",
  "ShawElem" => "Shaw Elem",
  "ShawHB" => "Shaw-HB",
  "SheehyHB" => "Sheehy-HB",
  "ShenandoahElem" => "Shenandoah Elem",
  "ShingleCreekElem" => "Shingle Creek Elem",
  "ShoreAcresElem" => "Shore Acres Elem",
  "ShoreAcresElementaryPN" => "Shore Acres Elementary-PN",
  "ShoreElem" => "Shore Elem",
  "SikesElem" => "Sikes Elem",
  "SkeenElem" => "Skeen Elem",
  "SkycrestElem" => "Skycrest Elem",
  "SkycrestElementaryPN" => "Skycrest Elementary-PN",
  "SkylineElementary" => "Skyline Elementary",
  "SkyviewElem" => "Skyview Elem",
  "SkyviewElementaryPN" => "Skyview Elementary-PN",
  "SleepyHillElementary" => "Sleepy Hill Elementary",
  "SnivelyElemSchoolofChoice" => "Snively Elem School of Choice",
  "SocrumElem" => "Socrum Elem",
  "SorrentoElementary" => "Sorrento Elementary",
  "SouthDaytonaElem" => "South Daytona Elem",
  "SouthernOakElem" => "Southern Oak Elem",
  "SouthernOakElementaryPN" => "Southern Oak Elementary-PN",
  "SouthsideElementary" => "Southside Elementary",
  "SouthWardElem" => "South Ward Elem",
  "SouthWardElementaryPN" => "South Ward Elementary-PN",
  "SouthwestElem" => "Southwest Elem",
  "SouthwoodElem" => "Southwood Elem",
  "SpessardLHollandElem" => "Spessard L. Holland Elementary",
  "SpiritElem" => "Spirit Elem",
  "SpookHillElem" => "Spook Hill Elem",
  "SpringCreekElem" => "Spring Creek Elem",
  "SpringCreekElementary" => "Spring Creek Elementary",
  "SpringheadElem" => "Springhead Elem",
  "SpringHeadHB" => "SpringHead-HB",
  "SpringLakeElementary" => "Spring Lake Elementary",
  "SpringLakeElemOrangeCty" => "Spring Lake Elem (Orange Cty)",
  "SpruceCreekElem" => "Spruce Creek Elem",
  "StarkeyElem" => "Starkey Elem",
  "StarkeyElementaryPN" => "Starkey Elementary-PN",
  "StCloudElem" => "St Cloud Elem",
  "StenstromElementary" => "Stenstrom Elementary",
  "StephensElem" => "Stephens Elem",
  "SterlingParkElementary" => "Sterling Park Elementary",
  "StoneLakeElem" => "Stone Lake Elem",
  "StowersElementary" => "Stowers Elementary",
  "SugarMillElem" => "Sugar Mill Elem",
  "SulphurSpringsElem" => "Sulphur Springs Elem",
  "SulphurSpringsHB" => "Sulphur Springs-HB",
  "SummerfieldCrossingElem" => "Summerfield Crossing Elementary",
  "SummerfieldElem" => "Summerfield Elem",
  "SummerfieldHB" => "Summerfield-HB",
  "SunrayElem" => "Sunray Elem",
  "SunrayElementaryPO" => "Sunray Elementary-PO",
  "SunRidgeElementary" => "SunRidge Elementary",
  "SunriseElem" => "Sunrise Elem",
  "SunriseElementary" => "Sunrise Elementary",
  "SunsetHillsElem" => "Sunset Hills Elem",
  "SunsetHillsElementaryPN" => "Sunset Hills Elementary-PN",
  "SunsetParkElem" => "Sunset Park Elem",
  "SunshineElementary" => "Sunshine Elementary",
  "SutherlandElem" => "Sutherland Elem",
  "SutherlandElementaryPN" => "Sutherland Elementary-PN",
  "SweetwaterElem" => "Sweetwater Elem",
  "SymmesElem" => "Symmes Elem",
  "SymmesHB" => "Symmes-HB",
  "TampaBayAcademy" => "Tampa Bay Academy",
  "TampaBayBlvdElem" => "Tampa Bay Blvd Elem",
  "TampaBayBoluevardHB" => "Tampa Bay Boluevard-HB",
  "TampaDowntown" => "Tampa Downtown",
  "TampaPalmsElem" => "Tampa Palms Elem",
  "TampaPalmsHB" => "Tampa Palms-HB",
  "TampaUnited" => "Tampa United",
  "TangeloParkElem" => "Tangelo Park Elem",
  "TanglewoodElementary" => "Tanglewood Elementary",
  "TaraElementary" => "Tara Elementary",
  "TarponSpringsElem" => "Tarpon Springs Elem",
  "TarponSpringsElementaryPN" => "Tarpon Springs Elementary-PN",
  "TarponSpringsFundamental" => "Tarpon Springs Fundamental",
  "TarponSpringsFundamentalPN" => "Tarpon Springs Fundamental-PN",
  "TatumRidgeElementary" => "Tatum Ridge Elementary",
  "TavaresElem" => "Tavares Elem",
  "TaylorRanchElementary" => "Taylor Ranch Elementary",
  "TempleTerraceElem" => "Temple Terrace Elem",
  "TempleTerraceHB" => "Temple Terrace-HB",
  "TerraceCommunity" => "Terrace Community",
  "ThackerAvenueElemK5" => "Thacker Avenue Elem (K 5)",
  "TheIslandSchool" => "The Island School",
  "TheSanibelSchool" => "The Sanibel School",
  "ThonotosassaElem" => "Thonotosassa Elem",
  "ThonotosassaHB" => "Thonotosassa-HB",
  "ThornebrookeElem" => "Thornebrooke Elem",
  "ThreeOaksElementary" => "Three Oaks Elementary",
  "ThreePointsElem" => "Three Points Elem",
  "TiceElementary" => "Tice Elementary",
  "TildenvilleElem" => "Tildenville Elem",
  "TimbercrestElem" => "Timbercrest Elem",
  "TimberLakesElementary" => "Timber Lakes Elementary",
  "TinkerElem" => "Tinker Elem",
  "TinkerHB" => "Tinker-HB",
  "ToledoBladeElementary" => "Toledo Blade Elementary",
  "TomokaElem" => "Tomoka Elem",
  "TownAndCountryElem" => "Town &amp; Country Elem",
  "TownandCountryHB" => "Town and Country-HB",
  "TrafalgarElementary" => "Trafalgar Elementary",
  "TrapnellElem" => "Trapnell Elem",
  "TrapnellHB" => "Trapnell-HB",
  "TreadwayElem" => "Treadway Elem",
  "TreelineElementary" => "Treeline Elementary",
  "TriangleElem" => "Triangle Elem",
  "TrinityCharter" => "Trinity Charter",
  "TrinityElementaryPO" => "Trinity Elementary-PO",
  "TrinityOaksElementary" => "Trinity Oaks Elementary",
  "TropicIslesElementary" => "Tropic Isles Elementary",
  "TurieTSmallElem" => "Turie T. Small Elem",
  "TurnerElemHB" => "Turner Elem-HB",
  "TuttleElementary" => "Tuttle Elementary",
  "TwinLakesElem" => "Twin Lakes Elem",
  "TwinLakesHB" => "Twin Lakes-HB",
  "TyroneElem" => "Tyrone Elem",
  "TyroneElementaryPN" => "Tyrone Elementary-PN",
  "UmatillaElem" => "Umatilla Elem",
  "UnionParkElem" => "Union Park Elem",
  "UsfCharter" => "Usf Charter",
  "ValleyviewElem" => "Valleyview Elem",
  "ValricoElem" => "Valrico Elem",
  "ValricoHB" => "Valrico-HB",
  "VelascoExceptional" => "Velasco Exceptional",
  "VeniceElementary" => "Venice Elementary",
  "VenturaElem" => "Ventura Elem",
  "VeteransElementarySchool" => "Veterans Elementary School",
  "VeteransParkAcadForArts" => "Verterans Park Academy for the Arts",
  "VillageOfExcell" => "Village Of Excell",
  "VillagesElemofLadyLake" => "Villages Elem of Lady Lake",
  "VillasElementary" => "Villas Elementary",
  "VinelandElementary" => "Vineland Elementary",
  "VistaLakesElem" => "Vista Lakes Elem",
  "VolusiaPinesElem" => "Volusia Pines Elem",
  "WahnetaElem" => "Wahneta Elem",
  "WaldenLakeElem" => "Walden Lake Elem",
  "WaldenLakeHB" => "Walden Lake-HB",
  "WalkerElementary" => "Walker Elementary",
  "WalsinghamElem" => "Walsingham Elem",
  "WalsinghamElementaryPN" => "Walsingham Elementary-PN",
  "WalterAHurstElem" => "Walter A. Hurst Elem",
  "WalterCaldwellElem" => "Walter Caldwell Elem",
  "WashingtonShoresElem" => "Washington Shores Elem",
  "WaterbridgeElem" => "Waterbridge Elem",
  "WaterfordElem" => "Waterford Elem",
  "WatergrassElementaryPO" => "Watergrass Elementary-PO",
  "WebsterElementary" => "Webster Elementary",
  "WekivaElementary" => "Wekiva Elementary",
  "WendellWatsonElem" => "Wendell Watson Elem",
  "WesleyChapelElementaryPO" => "Wesley Chapel Elementary-PO",
  "WestbrookeElementary" => "Westbrooke Elementary",
  "WestchaseElem" => "Westchase Elem",
  "WestchaseHB" => "Westchase-HB",
  "WestCreekElem" => "West Creek Elem",
  "WestElementarySchool" => "West Elementary School",
  "WestgateElem" => "Westgate Elem",
  "WestgateElementaryPN" => "Westgate Elementary-PN",
  "WestOaksElem" => "West Oaks Elem",
  "WestShoreElem" => "West Shore Elem",
  "WestShoreHB" => "West Shore-HB",
  "WestsideElem" => "Westside Elem",
  "WestTampaElem" => "West Tampa Elem",
  "WestTampaHB" => "West Tampa-HB",
  "WestZephyrhillsElem" => "West Zephyrhills Elem",
  "WestZephyrhillsElemenPO" => "West Zephyrhills Elemen-PO",
  "WFBurnsOakHillElem" => "W.F. Burns-Oak Hill Elem",
  "WheatleyElem" => "Wheatley Elem",
  "WhisperingOakElem" => "Whispering Oak Elem",
  "WicklowElementary" => "Wicklow Elementary",
  "WildwoodElementary" => "Wildwood Elementary",
  "WilkensonElementary" => "Wilkenson Elementary",
  "WilkinsonElementary" => "Wilkinson Elementary",
  "WilliamFrangusElem" => "William Frangus Elem",
  "WilliamHBashawElementary" => "William H. Bashaw Elementary",
  "WilliamsElementary" => "Williams Elementary",
  "WilliamsElemHB" => "Williams Elem-HB",
  "WilsonElem" => "Wilson Elem",
  "WilsonElementary" => "Wilson Elementary",
  "WilsonElementarySchoolHB" => "Wilson Elementary School-HB",
  "WimaumaElem" => "Wimauma Elem",
  "WimaumaHB" => "Wimauma-HB",
  "WindermereElem" => "Windermere Elem",
  "WindyRidgeElem" => "Windy Ridge Elem",
  "WinegardElem" => "Winegard Elem",
  "WinstonElem" => "Winston Elem",
  "WinterSpringsElementary" => "Winter Springs Elementary",
  "WitterElem" => "Witter Elem",
  "WitterHB" => "Witter-HB",
  "WolfLakeElem" => "Wolf Lake Elem",
  "WoodbridgeElem" => "Woodbridge Elem",
  "WoodbridgeHB" => "Woodbridge-HB",
  "WoodlandElem" => "Woodland Elem",
  "WoodlandElementaryPO" => "Woodland Elementary-PO",
  "WoodlandsElementary" => "Woodlands Elementary",
  "WoodlawnElementaryPN" => "Woodlawn Elementary-PN",
  "WoodwardAvenueElem" => "Woodward Avenue Elem",
  "WyndhamLakesElementary" => "Wyndham Lakes Elementary",
  "YatesElem" => "Yates Elem",
  "YatesHB" => "Yates-HB",
  "ZellwoodElem" => "Zellwood Elem"
  },

  mls_high_school: {
    "AddedSchoolA" => "Added School - A",
  "AddedSchoolB" => "Added School - B",
  "AddedSchoolC" => "Added School - C",
  "AddedSchoolD" => "Added School - D",
  "AddedSchoolE" => "Added School - E",
  "AddedSchoolF" => "Added School - F",
  "AlonsoHB" => "Alonso-HB",
  "AlonsoSenior" => "Alonso Senior",
  "AlternativeSchools" => "Alternative Schools",
  "AncloteHighPO" => "Anclote High-PO",
  "Apopka9thGradeCenter" => "Apopka 9th Grade Center",
  "ApopkaHigh" => "Apopka High",
  "ArmwoodHB" => "Armwood-HB",
  "ArmwoodHigh" => "Armwood High",
  "AtlanticHigh" => "Atlantic High",
  "AuburndaleHigh" => "Auburndale High",
  "BartowHigh" => "Bartow High",
  "BartowSeniorHigh" => "Bartow Senior High",
  "BayshoreHigh" => "Bayshore High",
  "BaysideHighPN" => "Bayside High-PN",
  "BlakeHB" => "Blake-HB",
  "BlakeHigh" => "Blake High",
  "BloomingdaleHB" => "Bloomingdale-HB",
  "BloomingdaleHigh" => "Bloomingdale High",
  "BocaCiegaHigh" => "Boca Ciega High",
  "BocaCiegaHighPN" => "Boca Ciega High-PN",
  "BoleyJuvenileJustice" => "Boley Juvenile Justice",
  "BookerHigh" => "Booker High",
  "BooneHigh" => "Boone High",
  "BowersWhitleyCareerCtrHB" => "Bowers-Whitley Career Ctr-HB",
  "BradenRiverHigh" => "Braden River High",
  "BrandonHB" => "Brandon-HB",
  "BrandonHigh" => "Brandon High",
  "BrittHalfwayHouse" => "Britt Halfway House",
  "BrooksDeBartoloCollegiateH" => "Brooks- DeBartolo Collegiate High School",
  "CalvinAHunsinger" => "Calvin A. Hunsinger",
  "CaminitiExceptional" => "Caminiti Exceptional",
  "CapeCoralHighSchool" => "Cape Coral High School",
  "CarverExceptional" => "Carver Exceptional",
  "CelebrationK12" => "Celebration (K12)",
  "ChainofLakesCollegiateHigh" => "Chain of Lakes Collegiate High",
  "ChamberlainHB" => "Chamberlain-HB",
  "ChamberlainSenior" => "Chamberlain Senior",
  "CharlotteHigh" => "Charlotte High",
  "ClearwaterHigh" => "Clearwater High",
  "ClearwaterHighPN" => "Clearwater High-PN",
  "ColonialHigh" => "Colonial High",
  "CountrysideHigh" => "Countryside High",
  "CountrysideHighPN" => "Countryside High-PN",
  "CroomsAcademyofInfoTech" => "Crooms Academy of Info Tech",
  "CypressCreekHigh" => "Cypress Creek High",
  "CypressCreekReliefHigh" => "Cypress Creek Relief High",
  "CypressLakeHighSchool" => "Cypress Lake High School",
  "DeesPtecClearwater" => "Dees - Ptec Clearwater",
  "DeesPtecStPete" => "Dees - Ptec St. Pete",
  "DelandHigh" => "Deland High",
  "DeltonaHigh" => "Deltona High",
  "DeSotoCountyHighSchool" => "DeSoto County High School",
  "DixieHollinsHighPN" => "Dixie Hollins High-PN",
  "DixieMHollinsHigh" => "Dixie M. Hollins High",
  "DorothyThomas" => "Dorothy Thomas",
  "DoverElem" => "Dover Elem",
  "DropoutPrevention" => "Dropout Prevention",
  "DrPhillipsHigh" => "Dr. Phillips High",
  "DunbarHighSchool" => "Dunbar High School",
  "DunedinHigh" => "Dunedin High",
  "DunedinHighPN" => "Dunedin High-PN",
  "DurantHB" => "Durant-HB",
  "DurantHigh" => "Durant High",
  "DWWatersCareerCenterHB" => "D. W. Waters Career Center-HB",
  "EastBayHB" => "East Bay-HB",
  "EastBaySenior" => "East Bay Senior",
  "EastLakeHigh" => "East Lake High",
  "EastLakeHighPN" => "East Lake High-PN",
  "EastLeeCountyHighSchool" => "East Lee County High School",
  "EastRidgeHigh" => "East Ridge High",
  "EastRiverHigh" => "East River High",
  "EdgewaterHigh" => "Edgewater High",
  "EisenhowerMiddle" => "Eisenhower Middle",
  "EsteroHighSchool" => "Estero High School",
  "EustisHighSchool" => "Eustis High School",
  "EvansHigh" => "Evans High",
  "EwesEckerdChallenge" => "Ewes - Eckerd Challenge",
  "EwesEckerdConserv" => "Ewes - Eckerd Conserv",
  "EwesEckerdIntensive" => "Ewes - Eckerd Intensive",
  "EwesEckerdLeader" => "Ewes - Eckerd Leader",
  "EwesWildernessCamps" => "Ewes - Wilderness Camps",
  "FamilyResourcesShelter" => "Family Resources Shelter",
  "FivayHighPO" => "Fivay High-PO",
  "FKMarchmanTech" => "F. K. Marchman Tech",
  "FKMarchmanTechCenterPO" => "F.K. Marchman Tech Center-PO",
  "FloridaVirtualSchool" => "Florida Virtual School",
  "FloridaYouthAcademy" => "Florida Youth Academy",
  "FloridaYouthHigh" => "Florida Youth High",
  "FloridaYouthModera" => "Florida Youth Modera",
  "FortMeadeJuniorSeniorHigh" => "Fort Meade Junior/Senior High",
  "FortMyersHighSchool" => "Fort Myers High School",
  "FreedomHB" => "Freedom-HB",
  "FreedomHighSchool" => "Freedom High School",
  "FrostproofMiddle" => "Frostproof Middle - Senior High",
  "GaitherHB" => "Gaither-HB",
  "GaitherHigh" => "Gaither High",
  "GatewayHighSchool912" => "Gateway High School (9 12)",
  "GeorgeJenkinsHigh" => "George Jenkins High",
  "GibbsHigh" => "Gibbs High",
  "GibbsHighPN" => "Gibbs High-PN",
  "GulfHigh" => "Gulf High",
  "GulfHighPO" => "Gulf High-PO",
  "HagertyHigh" => "Hagerty High",
  "HainesCitySeniorHigh" => "Haines City Senior High",
  "HamiltonDisston" => "Hamilton Disston",
  "HarmonyHigh" => "Harmony High",
  "HarrisonSchoolfortheArts" => "Harrison School for the Arts",
  "HarrySchwettmanEducCtrPO" => "Harry Schwettman Educ Ctr-PO",
  "Headstart" => "Headstart",
  "HillsboroughHB" => "Hillsborough-HB",
  "HillsboroughSenior" => "Hillsborough Senior",
  "HospitalHomebound" => "Hospital/Homebound",
  "HudsonHigh" => "Hudson High",
  "HudsonHighPO" => "Hudson High-PO",
  "IdaSBakerHighSchool" => "Ida S. Baker High School",
  "IslandCoastHighSchool" => "Island Coast High School",
  "JamesExceptional" => "James Exceptional",
  "JamesIrvinEduc" => "James Irvin Educ",
  "JamesIrvinEducationCtrPO" => "James Irvin Education Ctr-PO",
  "JamesWMitchellHigh" => "James W. Mitchell High",
  "JeffersonCtrforIntlStudHB" => "Jefferson Ctr for Intl Stud-HB",
  "JeffersonSenior" => "Jefferson Senior",
  "JonesHigh" => "Jones High",
  "JuvenileDetention" => "Juvenile Detention",
  "JWMitchellHighPO" => "J.W. Mitchell High-PO",
  "KathleenHigh" => "Kathleen High",
  "KingHB" => "King-HB",
  "KingSenior" => "King Senior",
  "LakeBrantleyHigh" => "Lake Brantley High",
  "LakeGibsonHigh" => "Lake Gibson High",
  "LakeHowellHigh" => "Lake Howell High",
  "LakelandSeniorHigh" => "Lakeland Senior High",
  "LakeMaryHigh" => "Lake Mary High",
  "LakeMinnelolaHigh" => "Lake Minnelola High",
  "LakeNonaHigh" => "Lake Nona High",
  "LakeRegionHigh" => "Lake Region High",
  "LakeTechHigh" => "Lake Tech High",
  "LakeWalesSeniorHigh" => "Lake Wales Senior High",
  "LakewoodHigh" => "Lakewood High",
  "LakewoodHighPN" => "Lakewood High-PN",
  "LakewoodRanchHigh" => "Lakewood Ranch High",
  "LandOlakesHigh" => "Land Olakes High",
  "LandOLakesHighPO" => "Land O&apos; Lakes High-PO",
  "LargoHigh" => "Largo High",
  "LargoHighPN" => "Largo High-PN",
  "LavoyExceptional" => "Lavoy Exceptional",
  "LeafPemhs" => "Leaf Pemhs",
  "LeafRecovery" => "Leaf Recovery",
  "LealmanIntermediate" => "Lealman Intermediate",
  "LeesburgHigh" => "Leesburg High",
  "LehighAcresHighSchool" => "Lehigh Acres High School",
  "LemonBayHigh" => "Lemon Bay High",
  "LennardHB" => "Lennard-HB",
  "LetoHB" => "Leto-HB",
  "LetoSenior" => "Leto Senior",
  "LibertyHigh" => "Liberty High",
  "LopezElem" => "Lopez Elem",
  "LymanHigh" => "Lyman High",
  "MainlandHighSchool" => "Mainland High School",
  "ManateeHigh" => "Manatee High",
  "MandalaAdolescent" => "Mandala Adolescent",
  "MarinerHighSchool" => "Mariner High School",
  "MendezSedCtr" => "Mendez Sed Ctr",
  "MiddletonHB" => "Middleton-HB",
  "ModestaRobbins" => "Modesta Robbins",
  "MooreMickensEduc" => "Moore-Mickens Educ",
  "MooreMickensEducationCtrPO" => "Moore Mickens Education Ctr-PO",
  "MountDoraHigh" => "Mount Dora High",
  "MulberryHigh" => "Mulberry High",
  "NewPortRicheyMarine" => "New Port Richey Marine",
  "NewSmyrnaBeachHighSchool" => "New Smyrna Beach High",
  "NewsomeHB" => "Newsome-HB",
  "NinaHarrisExceptional" => "Nina Harris Exceptional",
  "NortheastHigh" => "Northeast High",
  "NortheastHighPN" => "Northeast High-PN",
  "NorthFortMyersHighSchool" => "North Fort Myers High School",
  "NorthPortHigh" => "North Port High",
  "NorthWardSecondary" => "North Ward Secondary",
  "NorwoodSecondary" => "Norwood Secondary",
  "OakRidgeHigh" => "Oak Ridge High",
  "OcoeeHigh" => "Ocoee High",
  "OlympiaHigh" => "Olympia High",
  "OsceolaCountySchoolforArts" => "Osceola County School for Arts",
  "OsceolaHigh" => "Osceola High",
  "OsceolaHighPN" => "Osceola High-PN",
  "OsceolaHighSchool" => "Osceola High School",
  "Other" => "Other",
  "OviedoHigh" => "Oviedo High",
  "PaceCenterForGirls" => "Pace Center For Girls",
  "PaceForGirlsPasco" => "Pace For Girls Pasco",
  "PalmettoHigh" => "Palmetto High",
  "PalmHarborUniver" => "Palm Harbor Univer",
  "PalmHarborUnivHighPN" => "Palm Harbor Univ High-PN",
  "PanamaKeyIsland" => "Panama Key Island",
  "ParkhillExceptional" => "Parkhill Exceptional",
  "PascoHigh" => "Pasco High",
  "PascoHighPO" => "Pasco High-PO",
  "PaulBStephensExcep" => "Paul B. Stephens Excep",
  "PinellasBootCamp" => "Pinellas Boot Camp",
  "PinellasBootCampTrans" => "Pinellas Boot Camp Trans",
  "PinellasMarineInst" => "Pinellas Marine Inst",
  "PinellasParkHigh" => "Pinellas Park High",
  "PinellasParkHighPN" => "Pinellas Park High-PN",
  "PineRidgeHighSchool" => "Pine Ridge High School",
  "PineviewHigh" => "Pineview High",
  "PlantCity" => "Plant City",
  "PlantCityExceptional" => "Plant City Exceptional",
  "PlantCityHB" => "Plant City-HB",
  "PlantHB" => "Plant-HB",
  "PlantSenior" => "Plant Senior",
  "PoincianaHighSchool" => "Poinciana High School",
  "PortCharlotteHigh" => "Port Charlotte High",
  "PtecClearwater" => "Ptec - Clearwater",
  "PtecSouthSecondary" => "Ptec South Secondary",
  "PtecStPetersburg" => "Ptec - St Petersburg",
  "QuestAcademy" => "Quest Academy",
  "RichardLSanders" => "Richard L. Sanders",
  "RidgeCommunitySeniorHigh" => "Ridge Community Senior High",
  "RidgewoodHigh" => "Ridgewood High",
  "RidgewoodHighSchoolPO" => "Ridgewood High School-PO",
  "RiverdaleHighSchool" => "Riverdale High School",
  "RiverRidgeHighPO" => "River Ridge High-PO",
  "RiverRidgeMiddle" => "River Ridge Middle",
  "RiverviewHB" => "Riverview-HB",
  "RiverviewHigh" => "Riverview High",
  "RobertHungerfordPrep" => "Robert Hungerford Prep",
  "RobinsonHB" => "Robinson-HB",
  "RobinsonSenior" => "Robinson Senior",
  "SafetyHarborSecondary" => "Safety Harbor Secondary",
  "SamuelLRobinson" => "Samuel L. Robinson",
  "SanAntonioBoys" => "San Antonio Boys",
  "SarasotaHigh" => "Sarasota High",
  "SchwettmanEducation" => "Schwettman Education",
  "SeabreezeHighSchool" => "Seabreeze High School",
  "SeminoleHigh" => "Seminole High",
  "SeminoleHighPN" => "Seminole High-PN",
  "SeminoleVocational" => "Seminole Vocational",
  "SheriffsDetention" => "Sheriffs Detention",
  "SicklesHB" => "Sickles-HB",
  "SicklesHigh" => "Sickles High",
  "SimmonsCareerCenterHB" => "Simmons Career Center-HB",
  "SouthCountyCareerCenterHB" => "South County Career Center-HB",
  "SoutheastHigh" => "Southeast High",
  "SouthFortMyersHighSchool" => "South Fort Myers High School",
  "SouthLakeHigh" => "South Lake High",
  "SouthSumterHigh" => "South Sumter High",
  "SpotoHighHB" => "Spoto High-HB",
  "SpruceCreekHighSchool" => "Spruce Creek High School",
  "StCloudHighSchool912" => "St. Cloud High School",
  "SteinbrennerHigh" => "Steinbrenner High School",
  "StPetersburgHigh" => "St. Petersburg High",
  "StPetersburgHighPN" => "St. Petersburg High-PN",
  "StrawberryCrestHighSchool" => "Strawberry Crest High School",
  "SunlakeHighSchoolPO" => "Sunlake High School-PO",
  "SunshineYouth" => "Sunshine Youth",
  "TampaBayAcademy" => "Tampa Bay Academy",
  "TampaBayTechnical" => "Tampa Bay Technical",
  "TampaBayTechnicalHB" => "Tampa Bay Technical-HB",
  "TampaMarine" => "Tampa Marine",
  "TarponSpringsHigh" => "Tarpon Springs High",
  "TarponSpringsHighPN" => "Tarpon Springs High-PN",
  "TavaresHigh" => "Tavares High",
  "TaylorHigh" => "Taylor High",
  "TDewittTaylorMiddleHigh" => "T. Dewitt Taylor Middle-High",
  "TenorocSenior" => "Tenoroc Senior",
  "TheAcademy" => "The Academy",
  "TimberCreekHigh" => "Timber Creek High",
  "UmatillaHigh" => "Umatilla High",
  "UniversityHigh" => "University High",
  "VelascoExceptional" => "Velasco Exceptional",
  "VeniceSeniorHigh" => "Venice Senior High",
  "WekivaHigh" => "Wekiva High",
  "WesleyChapelHigh" => "Wesley Chapel High",
  "WesleyChapelHighPO" => "Wesley Chapel High-PO",
  "WestOrangeHigh" => "West Orange High",
  "WhartonHB" => "Wharton-HB",
  "WhartonHigh" => "Wharton High",
  "WildwoodHigh" => "Wildwood High",
  "WilsonAcademy" => "Wilson Academy",
  "WinterHavenSenior" => "Winter Haven Senior",
  "WinterParkHigh" => "Winter Park High",
  "WinterSpringsHigh" => "Winter Springs High",
  "WiregrassRanchHighPO" => "Wiregrass Ranch High-PO",
  "ZephryhillsHighSchoolPO" => "Zephryhills High School-PO",
  "ZephyrhillsHigh" => "Zephyrhills High"
  },

  mls_middle_school: {
    "AdamsHB" => "Adams-HB",
  "AdamsMiddle" => "Adams Middle",
  "AddedSchoolA" => "Added School - A",
  "AddedSchoolB" => "Added School - B",
  "AddedSchoolC" => "Added School - C",
  "AddedSchoolD" => "Added School - D",
  "AddedSchoolE" => "Added School - E",
  "AddedSchoolF" => "Added School - F",
  "AlternativesNonDjj" => "Alternative s Non-Djj",
  "AlvaMiddleSchool" => "Alva Middle School",
  "ApopkaMiddle" => "Apopka Middle",
  "ArborRidgeMiddle" => "Arbor Ridge Middle",
  "AvalonMiddle" => "Avalon Middle",
  "AzaleaMiddle" => "Azalea Middle",
  "AzaleaMiddlePN" => "Azalea Middle-PN",
  "BarringtonMiddle" => "Barrington Middle",
  "BartelsMiddle" => "Bartels Middle",
  "BartowMiddle" => "Bartow Middle",
  "BayonetPointMiddle" => "Bayonet Point Middle",
  "BayonetPointMiddlePO" => "Bayonet Point Middle-PO",
  "BayPointMiddle" => "Bay Point Middle",
  "BayPointMiddlePN" => "Bay Point Middle-PN",
  "BellalagoCharterAcademyK8" => "Bellalago Charter Academy (K-8)",
  "BenitoHB" => "Benito-HB",
  "BenitoMiddle" => "Benito Middle",
  "BlanknerSchoolK8" => "Blankner School (K-8)",
  "BonitaSpringsMiddleSchool" => "Bonita Springs Middle School",
  "BookerMiddle" => "Booker Middle",
  "BookerTWashington" => "Booker T. Washington",
  "BooneMiddle" => "Boone Middle",
  "BradenRiverMiddle" => "Braden River Middle",
  "BridgewaterMiddle" => "Bridgewater Middle",
  "BrittHalfwayHouse" => "Britt Halfway House",
  "BrooksideMiddle" => "Brookside Middle",
  "BuchananHB" => "Buchanan-HB",
  "BuchananMiddle" => "Buchanan Middle",
  "BuffaloCreekMiddle" => "Buffalo Creek Middle",
  "BurnettHB" => "Burnett-HB",
  "BurnsHB" => "Burns-HB",
  "BurnsMiddle" => "Burns Middle",
  "CaloosaMiddleSchool" => "Caloosa Middle School",
  "CalvinAHunsinger" => "Calvin A. Hunsinger",
  "CaminitiExceptional" => "Caminiti Exceptional",
  "CampbellMiddle" => "Campbell Middle",
  "CarlosEHaileMiddle" => "Carlos E. Haile Middle",
  "CarverExceptional" => "Carver Exceptional",
  "CarverMiddle" => "Carver Middle",
  "CarwiseMiddlePN" => "Carwise Middle-PN",
  "CecilGrayMiddle" => "Cecil Gray Middle",
  "CelebrationK12" => "Celebration (K12)",
  "CentennialMiddle" => "Centennial Middle",
  "CentennialMiddlePO" => "Centennial Middle-PO",
  "ChainofLakesMiddle" => "Chain of Lakes Middle",
  "ChallengerMiddleSchool" => "Challenger Middle School",
  "CharlesSRusheMiddlePO" => "Charles S. Rushe Middle-PO",
  "ChascoMiddle" => "Chasco Middle",
  "ChascoMiddlePO" => "Chasco Middle-PO",
  "ChilesMiddle" => "Chiles Middle",
  "ClearwaterIntermediate" => "Clearwater Intermediate",
  "ClearwaterIntermediatePN" => "Clearwater Intermediate-PN",
  "ClermontMiddle" => "Clermont Middle",
  "CoachmanFundamental" => "Coachman Fundamental",
  "CoachmanFundamentalPN" => "Coachman Fundamental-PN",
  "ColemanHB" => "Coleman-HB",
  "ColemanMiddle" => "Coleman Middle",
  "ConwayMiddle" => "Conway Middle",
  "CornerLakeMiddle" => "Corner Lake Middle",
  "CreeksideMiddle" => "Creekside Middle",
  "CrewsLakeMiddlePO" => "Crews Lake Middle-PO",
  "CrystalLakeMiddleJunior" => "Crystal Lake Middle/Jun",
  "CypressLakeMiddleSchool" => "Cypress Lake Middle School",
  "DavidCHinsonSrMiddle" => "David C Hinson Sr Middle",
  "DavidsenHB" => "Davidsen-HB",
  "DavidsenMiddle" => "Davidsen Middle",
  "DayspringAcademy" => "Dayspring Academy",
  "DeerwoodAcademy" => "Deerwood Academy",
  "DeesPtecClearwater" => "Dees - Ptec Clearwater",
  "DeesPtecStPete" => "Dees - Ptec St. Pete",
  "DelandMiddle" => "Deland Middle",
  "DeltonaMiddle" => "Deltona Middle",
  "DenisonMiddle" => "Denison Middle",
  "DennJohnMiddle" => "Denn John Middle",
  "DeSotoMiddleSchool" => "DeSoto Middle School",
  "DetentionEast" => "Detention East",
  "DetentionWest" => "Detention West",
  "DiplomatMiddleSchool" => "Diplomat Middle School",
  "DiscoverIntermedia48" => "Discover Intermedia (4-8)",
  "DiscoveryAcademyLakeAlfred" => "Discovery Academy of Lake Alfred",
  "DiscoveryIntermediate" => "Discovery Intermediate",
  "DiscoveryMiddle" => "Discovery Middle",
  "DorothyThomas" => "Dorothy Thomas",
  "DoverElementary" => "Dover Elementary",
  "DowdellHB" => "Dowdell-HB",
  "DowdellMiddle" => "Dowdell Middle",
  "DropoutPrevention" => "Dropout Prevention",
  "DundeeRidgeMiddle" => "Dundee Ridge Middle",
  "DunedinHighlandMiddle" => "Dunedin Highland Middle",
  "DunedinHighlandMiddlePN" => "Dunedin Highland Middle-PN",
  "EastCountyCareer" => "East County Career",
  "EastRidgeMiddle" => "East Ridge Middle",
  "EisenhowerHB" => "Eisenhower-HB",
  "EisenhowerMiddle" => "Eisenhower Middle",
  "EustisMiddle" => "Eustis Middle",
  "EwesEckerdIntensive" => "Ewes - Eckerd Intensive",
  "EwesEckerdLeadership" => "Ewes - Eckerd Leadership",
  "EwesEckerdYouthChall" => "Ewes - Eckerd Youth Chall",
  "EwesEckerdYouthCons" => "Ewes - Eckerd Youth Cons",
  "EwesWilderness" => "Ewes - Wilderness",
  "FalkenburgAcademy" => "Falkenburg Academy",
  "FamilyResources" => "Family Resources",
  "FarnellHB" => "Farnell-HB",
  "FerrellHB" => "Ferrell-HB",
  "FerrellMiddle" => "Ferrell Middle",
  "FitzgeraldMiddle" => "Fitzgerald Middle",
  "FitzgeraldMiddlePN" => "Fitzgerald Middle-PN",
  "FKMarchmanTech" => "F. K. Marchman Tech",
  "FloridaYouthAcademy" => "Florida Youth Academy",
  "FloridaYouthHigh" => "Florida Youth High",
  "FloridaYouthModera" => "Florida Youth Modera",
  "FortMeadeMiddle" => "Fort Meade Middle",
  "FortMyersMiddleAcademy" => "Fort Myers Middle Academy",
  "FranklinMagnetHB" => "Franklin Magnet-HB",
  "FranklinMiddle" => "Franklin Middle",
  "FreedomMiddle" => "Freedom Middle",
  "FrostproofMiddleSeniorHigh" => "Frostproof Middle Se",
  "FruitlandParkLearningCen" => "Fruitland Park Learnin",
  "GalaxyMiddle" => "Galaxy Middle",
  "GirlsProgramNorth" => "Girls Program-North",
  "GiuntaMiddleHB" => "Giunta Middle-HB",
  "GlenridgeMiddle" => "Glenridge Middle",
  "GordonBurnettMiddle" => "Gordon Burnett Middle",
  "GothaMiddle" => "Gotha Middle",
  "GrayMiddle" => "Gray Middle",
  "GrecoHB" => "Greco-HB",
  "GrecoMiddle" => "Greco Middle",
  "GreenwoodLakesMiddle" => "Greenwood Lakes Middle",
  "GriffinMiddle" => "Griffin Middle",
  "GulfMiddle" => "Gulf Middle",
  "GulfMiddlePO" => "Gulf Middle-PO",
  "GulfMiddleSchool" => "Gulf Middle School",
  "HamiltonDisston" => "Hamilton Disston",
  "HavenPoeRunaway" => "Haven Poe Runaway",
  "Headstart" => "Headstart",
  "HeritageMiddle" => "Heritage Middle",
  "HeronCreekMiddle" => "Heron Creek Middle",
  "HillHB" => "Hill-HB",
  "HillMiddle" => "Hill Middle",
  "HillsboroughAcademy" => "Hillsborough Academy",
  "HillsboroughGroup" => "Hillsborough Group",
  "HollyHillMiddle" => "Holly Hill Middle",
  "HorizonMiddle" => "Horizon Middle",
  "HospitalHomebound" => "Hospital/Homebound",
  "HowardMiddle" => "Howard Middle",
  "HudsonHigh" => "Hudson High",
  "HudsonMiddle" => "Hudson Middle",
  "HudsonMiddlePO" => "Hudson Middle-PO",
  "HuntersCreekMiddle" => "Hunter&apos;s Creek Middle",
  "IndianTrailsMiddle" => "Indian Trails Middle",
  "JacksonHeightsMiddle" => "Jackson Heights Middle",
  "JamesExceptional" => "James Exceptional",
  "JamesIrvinEduc" => "James Irvin Educ",
  "JamesK8HB" => "James K-8-HB",
  "JamesStephensInternationalA" => "James Stephens International Academy",
  "JenningsHB" => "Jennings-HB",
  "JohnHopkinsMiddle" => "John Hopkins Middle",
  "JohnHopkinsMiddlePN" => "John Hopkins Middle-PN",
  "JohnLongMiddlePO" => "John Long Middle-PO",
  "JosephLCarwiseMiddle" => "Joseph L. Carwise Middle",
  "JuvenileDetention" => "Juvenile Detention",
  "KathleenMiddle" => "Kathleen Middle",
  "KennedyMiddle" => "Kennedy Middle",
  "KennedyMiddlePN" => "Kennedy Middle-PN",
  "KissimmeeMiddle" => "Kissimmee Middle",
  "LAAingerMiddle" => "L.A. Ainger Middle",
  "LakeAlfredAddairMiddle" => "Lake Alfred-Addair Middle",
  "LakeAlfredCareerDevel" => "Lake Alfred Career Devel",
  "LakeGibsonMiddleJunior" => "Lake Gibson Middle/Junio",
  "LakelandHighlandsMiddle" => "Lakeland Highlands Middl",
  "LakeNonaMiddleSchool" => "Lake Nona Middle School",
  "LakeviewMiddleOrangeCty" => "Lakeview Middle",
  "LandOlakesHigh" => "Land Olakes High",
  "LargoMiddle" => "Largo Middle",
  "LargoMiddlePN" => "Largo Middle-PN",
  "LaurelNokomisMiddle" => "Laurel Nokomis Middle",
  "LavoyExceptional" => "Lavoy Exceptional",
  "LawtonChilesMiddle" => "Lawton Chiles Middle",
  "LeafPemhs" => "Leaf Pemhs",
  "LeafRecovery" => "Leaf Recovery",
  "LealmanIntermediate" => "Lealman Intermediate",
  "LealmanIntermediatePN" => "Lealman Intermediate-PN",
  "LeeMiddle" => "Lee Middle",
  "LeeMiddleSchool" => "Lee Middle School",
  "LegacyMiddle" => "Legacy Middle",
  "LehighAcresMiddleSchool" => "Lehigh Acres Middle School",
  "LesliePetersHalfway" => "Leslie Peters Halfway",
  "LexingtonMiddleSchool" => "Lexington Middle School",
  "LibertyHB" => "Liberty-HB",
  "LibertyMiddle" => "Liberty Middle",
  "LincolnMiddle" => "Lincoln Middle",
  "LockhartMiddle" => "Lockhart Middle",
  "LopezElementary" => "Lopez Elementary",
  "LouiseRJohnsonMiddle" => "Louise R. Johnson Middle",
  "MadeiraBeachMiddle" => "Madeira Beach Middle",
  "MadeiraBeachMiddlePN" => "Madeira Beach Middle-PN",
  "MadisonHB" => "Madison-HB",
  "MadisonMiddle" => "Madison Middle",
  "MaitlandMiddle" => "Maitland Middle",
  "MandalaAdolescent" => "Mandala Adolescent",
  "MannHB" => "Mann-HB",
  "MannMiddle" => "Mann Middle",
  "MarinerMiddleSchool" => "Mariner Middle School",
  "MarkhamWoodsMiddle" => "Markham Woods Middle",
  "MarshallHB" => "Marshall-HB",
  "MarshallMiddle" => "Marshall Middle",
  "MarthaBKingMiddle" => "Martha B. King Middle",
  "MartinezHB" => "Martinez-HB",
  "McIntoshMiddle" => "McIntosh Middle",
  "McLaneHB" => "McLane-HB",
  "MclaneMiddle" => "Mclane Middle",
  "MclaughlinMiddle" => "Mclaughlin Middle",
  "MeadowbrookMiddle" => "Meadowbrook Middle",
  "MeadowlawnMiddle" => "Meadowlawn Middle",
  "MeadowlawnMiddlePN" => "Meadowlawn Middle-PN",
  "MeadowWoodMiddle" => "Meadow Wood Middle",
  "MemorialHB" => "Memorial-HB",
  "MemorialMiddle" => "Memorial Middle",
  "MendezSed" => "Mendez Sed",
  "MichiganInternationalAcademy" => "Michigan International Academy",
  "MiddleSchoolCC" => "Middle School &apos;Cc&apos;",
  "MiddleSchoolEE" => "Middle School &apos;Ee&apos;",
  "MiddletonMiddleTechHB" => "Middleton Middle Tech-HB",
  "MidFloridaTechnicalIns" => "Mid Florida Technical In",
  "MillenniumMiddle" => "Millennium Middle",
  "MilweeMiddle" => "Milwee Middle",
  "ModestaRobbins" => "Modesta Robbins",
  "MonroeHB" => "Monroe-HB",
  "MonroeMiddle" => "Monroe Middle",
  "MooreMickensEduc" => "Moore-Mickens Educ",
  "MountDoraMiddle" => "Mount Dora Middle",
  "MulberryMiddle" => "Mulberry Middle",
  "MulrennanHB" => "Mulrennan-HB",
  "MurdockMiddle" => "Murdock Middle",
  "NarcoosseeCommunityK8" => "Narcoossee Community (K-8)",
  "NaturesClassroomHB" => "Natures Classroom-HB",
  "NeptuneMiddle68" => "Neptune Middle  (6-8)",
  "NewPortRicheyMarine" => "New Port Richey Marine",
  "NewSmyrnaBeachMiddleSch" => "New Smyrna Beach Middl",
  "NFtMyersAcadforArts" => "North Fort Myers Academy for the Arts",
  "NinaHarrisExceptional" => "Nina Harris Exceptional",
  "NolanMiddle" => "Nolan Middle",
  "NorthTampaAlternative" => "North Tampa Alternative",
  "NorthWardSecondary" => "North Ward Secondary",
  "NorwoodSecondary" => "Norwood Secondary",
  "OakGroveMiddle" => "Oak Grove Middle",
  "OakGroveMiddlePN" => "Oak Grove Middle-PN",
  "OakHammockMiddleSchool" => "Oak Hammock Middle School",
  "OakParkMiddle" => "Oak Park Middle",
  "OakParkSchoolSouth" => "Oak Park School South",
  "OcoeeMiddle" => "Ocoee Middle",
  "OdysseyMiddle" => "Odyssey Middle",
  "OrangeGroveHB" => "Orange Grove-HB",
  "OrangeGroveMiddle" => "Orange Grove Middle",
  "OrientRoadJail" => "Orient Road Jail",
  "OrmondBeachMiddle" => "Ormond Beach Middle",
  "OsceolaMiddle" => "Osceola Middle",
  "OsceolaMiddlePN" => "Osceola Middle-PN",
  "Other" => "Other",
  "Pace" => "Pace",
  "PaceCenterForGirls" => "Pace Center For Girls",
  "PaceForGirls" => "Pace For Girls",
  "PalmHarborMiddle" => "Palm Harbor Middle",
  "PalmHarborMiddlePN" => "Palm Harbor Middle-PN",
  "PanamaKeyIsland" => "Panama Key Island",
  "ParkhillExceptional" => "Parkhill - Exceptional",
  "ParkwayMiddle" => "Parkway Middle",
  "PascoMiddle" => "Pasco Middle",
  "PascoMiddlePO" => "Pasco Middle-PO",
  "PaulBStephensExcep" => "Paul B. Stephens Excep",
  "PaulRSmithMiddlePO" => "Paul R. Smith Middle-PO",
  "PepinAcademy" => "Pepin Academy",
  "PiedmontLakesMiddle" => "Piedmont Lakes Middle",
  "PierceHB" => "Pierce-HB",
  "PierceMiddle" => "Pierce Middle",
  "PinellasBootCamp" => "Pinellas Boot Camp",
  "PinellasBootCampTrans" => "Pinellas Boot Camp Trans",
  "PinellasMarine" => "Pinellas Marine",
  "PinellasParkMiddle" => "Pinellas Park Middle",
  "PinellasParkMiddlePN" => "Pinellas Park Middle-PN",
  "PineViewMiddle" => "Pine View Middle",
  "PineViewMiddlePO" => "Pine View Middle-PO",
  "PineViewsMiddle" => "PineView Middle"
  },

  new_construction_description: {
    "Completed" => "Completed",
    "PreConstruction" => "Pre-Construction",
    "UnderConstruction" => "Under Construction"
  },

  pool_area: {
    "AboveGround" => "Above Ground",
  "AutoCleaner" => "Auto Cleaner",
  "ChildSafetyFence" => "Child Safety Fence",
  "DivingBoard" => "Diving Board",
  "FiberGlass" => "Fiberglass",
  "GuniteConcrete" => "Gunite/Concrete",
  "HeatedPool" => "Heated Pool",
  "HeatedSpa" => "Heated Spa",
  "InfinityEdge" => "Infinity Edge",
  "InGround" => "In Ground",
  "Inside" => "Inside",
  "Other" => "Other",
  "OtherWaterFeatur" => "Other Water Feature",
  "OutsideBathAcces" => "Outside Bath Access",
  "PoolSweep" => "Pool Sweep",
  "SaltWater" => "Salt Water",
  "ScreenEnclosure" => "Screen Enclosure",
  "SolarCover" => "Solar Cover for Pool",
  "SolarHeatedPool" => "Solar Heated Pool",
  "SolarPoweredPump" => "Solar Powered Pool Pump",
  "Spa" => "Spa",
  "Tile" => "Tile",
  "Vinyl" => "Vinyl"
  },

  road_surface: {
    "CountyRoad" => "County Road",
    "DividedHighways" => "Divided Highways",
    "Interchange" => "Interchange",
    "Interstate" => "Interstate",
    "MainThoroughfare" => "Main Thoroughfare",
    "Other" => "Other",
    "Private" => "Private",
    "StateRoad" => "State Road",
    "TurnLanes" => "Turn Lanes",
    "USHighway" => "US Highway"
  },

  roof_type: {
    "Bltupfoamother" => "Blt up-foam/other",
  "BuiltUp" => "Built Up",
  "BuiltUpTAndG" => "Built Up - T&amp;G",
  "Concrete" => "Concrete",
  "CurveArcRound" => "Curve/Arc/Round",
  "Flat" => "Flat",
  "GableHip" => "Gable/Hip",
  "Massard" => "Massard",
  "Membrane" => "Membrane",
  "Metal" => "Metal",
  "Other" => "Other",
  "Rolled" => "Rolled",
  "RoofOver" => "Roof Over",
  "Shake" => "Shake",
  "Shed" => "Shed",
  "Shingle" => "Shingle",
  "Slate" => "Slate",
  "SteelBarJoist" => "Steel/Bar/Joist",
  "Tile" => "Tile",
  "WoodFrame" => "Wood Frame"
  },

  siding_type: {
    "Asbestos" => "Asbestos",
    "Block" => "Block",
    "Brick" => "Brick",
    "Combination" => "Combination",
    "ConcreteBlock" => "Concrete Block",
    "CurtainWall" => "Curtain Wall",
    "Frame" => "Frame",
    "ICF" => "ICF Insulated Concrete Forms",
    "Log" => "Log",
    "Metal" => "Metal",
    "MetalFrame" => "Metal Frame",
    "Modular" => "Modular",
    "OnPiling" => "On Piling",
    "Other" => "Other",
    "PrecastConcrete" => "Precast Concrete",
    "Siding" => "Siding",
    "SIP" => "SIP (Structurally Insulated Panel)",
    "StemWall" => "Stem Wall",
    "Stone" => "Stone",
    "Stucco" => "Stucco",
    "TiltUp" => "Tilt Up",
    "TiltUpWal" => "Tilt Up Walls",
    "WoodFrame" => "Wood Frame",
    "WoodFSC" => "Wood Frame (FSC Certified)"
  },

status: {
  "ACT" => "active",
  "AWC" => "pending",
  "EXP" => "expired",
  "I" => "active",
  "LSE" => "sold",
  "LSO" => "pending",
  "PNC" => "pending",
  "RNT" => "sold",
  "SLD" => "sold",
  "TOM" => "active",
  "WDN" => "withdrawn"
},



  street_direction: {
    "E" => "E",
    "N" => "N",
    "NE" => "NE",
    "NW" => "NW",
    "S" => "S",
    "SE" => "SE",
    "SW" => "SW",
    "W" => "W"
  },

  style_of_building: {
    "Bungalow" => "Bungalow",
    "CapeCod" => "Cape Cod",
    "Colonial" => "Colonial",
    "Contemporary" => "Contemporary",
    "Courtyard" => "Courtyard",
    "Custom" => "Custom",
    "DutchProvincial" => "Dutch Provincial",
    "Elevated" => "Elevated",
    "Florida" => "Florida",
    "FrenchProvincial" => "French Provincial",
    "Historical" => "Historical",
    "KeyWest" => "Key West",
    "Other" => "Other",
    "Patio" => "Patio",
    "Ranch" => "Ranch",
    "Spanish" => "Spanish/Mediterranean",
    "Townhouse" => "Townhouse",
    "Traditional" => "Traditional",
    "Tudor" => "Tudor",
    "Victorian" => "Victorian",
    "Villa" => "Villa"
  }
}

@lookup_type_enum2 = {

}

LOOKUP_TYPE_ENUMS2 = {
city: {
  "153YBM87FVH2" => "Winter Garden",
  "153YBM87FQKD" => "Harmony",
  "153YBM87C4Q4" => "Grant Valkaria",
  "153YBM87DECR" => "Grant",
  "153YBM87ER7Z" => "Gotha",
  "153YBM87B8RM" => "Golden Oak",
  "1AQX2NRUR7GT" => "Glen Saint Mary",
  "153YBM87EFAE" => "Gifford",
  "15A256UB8TUS" => "Ft. Pierce",
  "1ANWMBQ80DPP" => "Fort Lauderdale",
  "153YBM87DBYI" => "Fellsmere",
  "153YBM877WBE" => "Enterprise",
  "153YBM879EKM" => "Edgewood",
  "153YBM876U2M" => "Edgewater",
  "153YBM87CV0X" => "Eau Gallie",
  "153YBM87858Q" => "Eatonville",
  "1ARYAAOQ1YVB" => "Dunnellon",
  "1AKYM28ORH8X" => "Doral",
  "153YBM877QCY" => "DeLand",
  "153YBM877U0G" => "Deltona",
  "1ATTAHQOOQ1Y" => "Deerfield Beach",
  "153YBM877OAN" => "Debary",
  "153YBM876RC7" => "De Leon Springs",
  "153YBM875U4M" => "Daytona Beach Shores",
  "153YBM875846" => "Daytona Beach",
  "1AQTVXUAUM77" => "Coral Gables",
  "153YBM87CR3N" => "Cocoa Beach",
  "153YBM87CBYJ" => "Cocoa",
  "1AL38P2ORL2T" => "Clermont",
  "153YBM87EBGA" => "Citrus Ridge",
  "158YSUX6N5OS" => "Chuluota",
  "153YBM877KCL" => "Christmas",
  "153YBM87F6KK" => "Celebration",
  "1AM3E7EV0SSD" => "Casselberry",
  "153YBM87C758" => "Cape Canaveral",
  "153YBM87CH0W" => "Canaveral Air Station",
  "153YBM87EZQ6" => "Buena Ventura Lakes",
  "1ARURHM65O4H" => "Belleview",
  "153YBM879C4Q" => "Belle Isle",
  "153YBM87ADUV" => "Bay Lake",
  "153YBM87EMF1" => "Barefoot Bay",
  "153YBM8794IG" => "Azalea Park",
  "153YBM877ERI" => "Apopka",
  "153YBM878P22" => "Aloma",
  "153YBM87A95T" => "Alafaya",
  "153YBM87CEJK" => "Patrick AFB",
  "1AMZMWZ87NAV" => "Panama City Beach",
  "1AQ0TGFRBO3N" => "Panama City",
  "153YBM87D9DD" => "Palm Shores",
  "153YBM87BSLQ" => "Palm Bay",
  "153YDMRHIZFK" => "Other",
  "153YBM878FPR" => "Osteen",
  "153YBM87762X" => "Ormond Beach",
  "153YBM879OW4" => "Orlo Vista",
  "153YBM878V4J" => "Orlando",
  "153YBM87E795" => "Orchid",
  "153YBM878D18" => "Orange City",
  "1AL1GOD9VLOJ" => "Okeechobee",
  "153YBM87FEWS" => "Ocoee",
  "153YBM87FY0D" => "Oakland",
  "153YBM878AA1" => "Oak Hill",
  "153YBM876XW7" => "New Smyrna Beach",
  "153YBM8770PQ" => "New Smyrna",
  "1AQWQ1R880TC" => "New Port Richey",
  "153YBM8787V3" => "Mims",
  "153YBM87EOS2" => "Micco",
  "1AM432PCB0FB" => "Miami",
  "153YBM87DOH5" => "Merritt Island",
  "153YBM87BQA4" => "Melbourne Village",
  "153YBM87DKX1" => "Melbourne Beach",
  "153YBM87BGB9" => "Melbourne",
  "153YBM87DGVS" => "Malabar",
  "153YBM8782KG" => "Maitland",
  "153YBM87FLH4" => "Magnolia Square",
  "1AMVQYUVD886" => "Longwood",
  "153YBM879L4Z" => "Lockhart",
  "1ALXDDIQDUS6" => "Lighthouse Point",
  "1AP2357J7UCP" => "Lake Worth",
  "1ALX091GYUP1" => "Lake Wales",
  "153YBM87800O" => "Lake Helen",
  "153YBM87EW2A" => "Kissimmee",
  "153YBM87ETH6" => "Kenansville",
  "153YBM87E4UY" => "Indian River Shores",
  "153YBM87D354" => "Indian Harbour Beach",
  "153YBM87BK1M" => "Indialantic",
  "153YBM877HRJ" => "Hunt Club",
  "153YBM875NDT" => "Holly Hill",
  "153YBM87A0A7" => "Hiawassee",
  "1AQX2NSDB9GD" => "Hialeah",
  "153YBM87FSY0" => "Windermere",
  "1AN37BRRBGT5" => "Wildwood",
  "153YBM876FTJ" => "Wilbur by the Sea",
  "153YBM87BNUP" => "West Melbourne",
  "153YBM87CKTN" => "West Cocoa",
  "1AQWQ1UJQH7F" => "Weeki Wachee",
  "15A2V3YVI5G7" => "Waynesville",
  "153YBM87D70T" => "Viera",
  "153YBM87DXW8" => "Vero Beach",
  "153YBM87AHK9" => "Ventura",
  "153YBM879WAN" => "Union Park",
  "1AO0VL9YKCF6" => "Umatilla",
  "153YBM87E1B8" => "Tropic",
  "153YBM878I43" => "Titusville",
  "1ASQO8GKZ8GY" => "Temple Terrace",
  "1AQX2NTSNBAO" => "Tallahassee",
  "153YBM87AL86" => "Taft",
  "153YBM87FHGX" => "St. Cloud",
  "1AQ0TBXAJ9AD" => "St. Augustine",
  
  "153YBM875ZM8" => "South Daytona Beach",
  "153YBM8765BA" => "South Daytona",
  "1ALWB9MPBLFD" => "Sorrento",
  "153YBM877C30" => "Seville",
  "1AQWPWPSTN85" => "Seffner",
  "1ATS8U7N6EFZ" => "Sebring",
  "153YBM87DVL1" => "Sebastian",
  "153YBM87D0KR" => "Satellite Beach",
  "1ANW8SLOTANM" => "Sanford",
  "153YBM87A5DN" => "Sand Lake",
  "153YBM87DRWC" => "Rockledge",
  "153YBM87F8Y5" => "Reunion",
  "158YSUVUUB4O" => "Port St Lucie",
  "153YBM87COGU" => "Port Saint John",
  "1AQWQ1RHM74Q" => "Port Richey",
  "153YBM875RDY" => "Port Orange",
  "1AK207F9HE4I" => "Port Charlotte",
  "153YBM87C9G5" => "Port Canaveral",
  "153YBM876CXP" => "Ponce Inlet",
  "153YBM8762GN" => "Point Orange",
  "153YBM87FCI7" => "Poinciana",
  "1ANU47IE8G93" => "Plantation",
  "153YBM87987F" => "Pine Hills",
  "153YBM879H5Q" => "Pine Castle",
  "153YBM8779Q2" => "Pierson",
  "1AQWQ1MP6OXP" => "Zephyrhills",
  "153YBM878SVG" => "Zellwood",
  "153YBM878L8N" => "Winter Park",
  "1AOUNAGCSQP7" => "Satsuma",
  
  "154185HLO3WK" => "Kissimmee",
  "154185HLO1HO" => "Kenansville",
  "154185HLNLKA" => "Indian River Shores",
  "154185HLMUBT" => "Indian Harbour Beach",
  "154185HLLWGM" => "Indialantic",
  "154185HLJPNE" => "Hunt Club",
  "154185HLIMQE" => "Holly Hill",
  "154185HLLDSN" => "Hiawassee",
  "154185HLOM9V" => "Harmony",
  "1APXMCA86TB0" => "Haines City",
  "154185HLM5TQ" => "Grant Valkaria",
  "154185HLN3I8" => "Grant",
  "154185HLNZ53" => "Gotha",
  "154185HLLRRX" => "Golden Oak",
  "154185HLNSBO" => "Gifford",
  "15A25G90JQL1" => "Ft. Pierce",
  "154185HLN13V" => "Fellsmere",
  "154185HLK12N" => "Enterprise",
  "154185HLL2FT" => "Edgewood",
  "154185HLJ8NL" => "Edgewater",
  "154185HLMPO2" => "Eau Gallie",
  "154185HLK8E0" => "Eatonville",
  "154185HLJWEF" => "DeLand",
  "154185HLJYWS" => "Deltona",
  "154185HLJUDU" => "Debary",
  "154185HLJ6FT" => "De Leon Springs",
  "154185HLIRVD" => "Daytona Beach Shores",
  "154185HLIK3H" => "Daytona Beach",
  "154185HLMNBE" => "Cocoa Beach",
  "154185HLMCQL" => "Cocoa",
  "154185HLNQ71" => "Citrus Ridge",
  "1591AIHP66IF" => "Chuluota",
  "154185HLJRYT" => "Christmas",
  "154185HLO8C4" => "Celebration",
  "154185HLM88L" => "Cape Canaveral",
  "154185HLMHD3" => "Canaveral Air Station",
  "154185HLO62W" => "Buena Ventura Lakes",
  "154185HLL0N4" => "Belle Isle",
  "154185HLLKQV" => "Bay Lake",
  "154185HLNUOP" => "Barefoot Bay",
  "154185HLKVF4" => "Azalea Park",
  "154185HLJN5C" => "Apopka",
  "154185HLKOSB" => "Aloma",
  "154185HLLIN7" => "Alafaya",
  "154185HLMS07" => "Satellite Beach",
  "154185HLLG8H" => "Sand Lake",
  "154185HLNCN4" => "Rockledge",
  "154185HLOAQO" => "Reunion",
  "1AQ0TG5MACJG" => "Punta Gorda",
  "1591AIHP8IAS" => "Port St Lucie",
  "154185HLMLCS" => "Port Saint John",
  "154185HLIP6Q" => "Port Orange",
  "1AJVN81SYESD" => "Port Charlotte",
  "154185HLMAFG" => "Port Canaveral",
  "154185HLJ1BT" => "Ponce Inlet",
  "154185HLIWUT" => "Point Orange",
  "154185HLOCYD" => "Poinciana",
  "154185HLKXXT" => "Pine Hills",
  "154185HLL4R2" => "Pine Castle",
  "154185HLJIMS" => "Pierson",
  "154185HLMF7Q" => "Patrick AFB",
  "154185HLMZ03" => "Palm Shores",
  "154185HLM3FQ" => "Palm Bay",
  "154185HLIHCN" => "Other",
  "154185HLKHI9" => "Osteen",
  "154185HLJG8S" => "Ormond Beach",
  "154185HLL99Q" => "Orlo Vista",
  "154185HLKTGY" => "Orlando",
  "154185HLNNV6" => "Orchid",
  "154185HLKFEX" => "Orange City",
  "1AO17PPYLC08" => "Oklawaha",
  "154185HLOFB9" => "Ocoee",
  "1ARR7UQYUYHP" => "Ocala",
  "154185HLOT7H" => "Oakland",
  "154185HLKCYV" => "Oak Hill",
  "154185HLJB6J" => "New Smyrna Beach",
  "154185HLJDOP" => "New Smyrna",
  "154185HLKAJF" => "Mims",
  "154185HLNX1H" => "Micco",
  "154185HLNAAX" => "Merritt Island",
  "154185HLM15T" => "Melbourne Village",
  "154185HLN7XM" => "Melbourne Beach",
  "154185HLLU46" => "Melbourne",
  "154185HLN5UF" => "Malabar",
  "154185HLK5WU" => "Maitland",
  "154185HLOJV9" => "Magnolia Square",
  "154185HLL6UI" => "Lockhart",
  "154185HLK3GN" => "Lake Helen",
  "154185HLKQY6" => "Zellwood",
  "154185HLKM9X" => "Winter Park",
  "154185HLOQU1" => "Winter Garden",
  "154185HLOOPX" => "Windermere",
  "1AN37BJ1AKH9" => "Wildwood",
  "154185HLJ3RF" => "Wilbur by the Sea",
  "154185HLLYX5" => "West Melbourne",
  "154185HLMJP6" => "West Cocoa",
  "15AUHRPVUG9Z" => "Waynesville",
  "154185HLMWMR" => "Viera",
  "154185HLNH5K" => "Vero Beach",
  "154185HLLN5M" => "Ventura",
  "154185HLLBNQ" => "Union Park",
  "154185HLNJFT" => "Tropic",
  "154185HLKJT8" => "Titusville",
  "154185HLLPLX" => "Taft",
  "154185HLOHOV" => "St. Cloud",
  "154185HLIU8S" => "South Daytona Beach",
  "154185HLIZ1F" => "South Daytona",
  "154185HLJKU8" => "Seville",
  "1ATS8UB714J7" => "Sebring",
  "154185HLNERY" => "Sebastian",

  "1541KR8BDE3M" => "South Daytona Beach",
  "1541KR8BLM3M" => "Kissimmee",
  "1541KR8BLJTO" => "Kenansville",
  "1541KR8BL2VS" => "Indian River Shores",
  "1541KR8BK9WR" => "Indian Harbour Beach",
  "1541KR8BJ9XJ" => "Indialantic",
  "1541KR8BGYYE" => "Hunt Club",
  "1541KR8BD6NA" => "Holly Hill",
  "1541KR8BIQKQ" => "Hiawassee",
  "1AL0ELXA8ZPT" => "Hendersonville",
  "1541KR8BM6CN" => "Harmony",
  "1541KR8BJJY9" => "Grant Valkaria",
  "1541KR8BKJO5" => "Grant",
  "1541KR8BLHE7" => "Gotha",
  "1541KR8BJ54O" => "Golden Oak",
  "1541KR8BLA67" => "Gifford",
  "1AK20FZQUDOC" => "Ft. Pierce",
  "1541KR8BKHEB" => "Fellsmere",
  "1541KR8BHARM" => "Enterprise",
  "1541KR8BIE1Y" => "Edgewood",
  "1541KR8BGIAL" => "Edgewater",
  "1541KR8BK56J" => "Eau Gallie",
  "1541KR8BHIQS" => "Eatonville",
  "1541KR8BH67J" => "DeLand",
  "1541KR8BH8O5" => "Deltona",
  "1541KR8BH3XQ" => "Debary",
  "1541KR8BGG0U" => "De Leon Springs",
  "1541KR8BDBUS" => "Daytona Beach Shores",
  "1541KR8BD3W1" => "Daytona Beach",
  "1541KR8BK2SS" => "Cocoa Beach",
  "1541KR8BJR2A" => "Cocoa",
  "1541KR8BL7HM" => "Citrus Ridge",
  "1591AIHPB497" => "Chuluota",
  "1541KR8BH1G0" => "Christmas",
  "1541KR8BLRS3" => "Celebration",
  "1541KR8BJM8N" => "Cape Canaveral",
  "1541KR8BJVOP" => "Canaveral Air Station",
  "1541KR8BLOJX" => "Buena Ventura Lakes",
  "1541KR8BIBNU" => "Belle Isle",
  "1541KR8BIXUF" => "Bay Lake",
  "1541KR8BLCP3" => "Barefoot Bay",
  "1541KR8BI6Q3" => "Azalea Park",
  "1541KR8BGWCP" => "Apopka",
  "1541KR8BHZW1" => "Aloma",
  "1541KR8BIVC3" => "Alafaya",
  "1541KR8BDJ4T" => "South Daytona",
  "1541KR8BGU0D" => "Seville",
  "1541KR8BKVT2" => "Sebastian",
  "1541KR8BK7OS" => "Satellite Beach",
  "1541KR8BIST8" => "Sand Lake",
  "1541KR8BKTB5" => "Rockledge",
  "1541KR8BLU1X" => "Reunion",
  "1591AIHPDL30" => "Port St Lucie",
  "1541KR8BK0KD" => "Port Saint John",
  "1541KR8BD96L" => "Port Orange",
  "1AK20FZQWXGP" => "Port Charlotte",
  "1541KR8BJON1" => "Port Canaveral",
  "1541KR8BDUUO" => "Ponce Inlet",
  "1541KR8BDGME" => "Point Orange",
  "1541KR8BLWJB" => "Poinciana",
  "1541KR8BI97V" => "Pine Hills",
  "1541KR8BIGEW" => "Pine Castle",
  "1541KR8BGRHQ" => "Pierson",
  "1541KR8BJTFF" => "Patrick AFB",
  "1541KR8BKEYI" => "Palm Shores",
  "1541KR8BJHIA" => "Palm Bay",
  "1541KR8BD15R" => "Other",
  "1541KR8BHSH5" => "Osteen",
  "1541KR8BGP06" => "Ormond Beach",
  "1541KR8BILJM" => "Orlo Vista",
  "1541KR8BI4J4" => "Orlando",
  "1541KR8BL5BG" => "Orchid",
  "1541KR8BHQ7F" => "Orange City",
  "1541KR8BLZ3C" => "Ocoee",
  "1541KR8BMDGS" => "Oakland",
  "1541KR8BHNLF" => "Oak Hill",
  "1541KR8BGK6R" => "New Smyrna Beach",
  "1541KR8BGMPT" => "New Smyrna",
  "1541KR8BHL2Q" => "Mims",
  "1541KR8BLEXH" => "Micco",
  "1541KR8BKQT5" => "Merritt Island",
  "1541KR8BJF0S" => "Melbourne Village",
  "1541KR8BKOED" => "Melbourne Beach",
  "1541KR8BJ7GR" => "Melbourne",
  "1541KR8BKM5J" => "Malabar",
  "1541KR8BHFZ8" => "Maitland",
  "1541KR8BM3VJ" => "Magnolia Square",
  "1541KR8BIJ04" => "Lockhart",
  "1541KR8BHDC8" => "Lake Helen",
  "1541KR8BI21I" => "Zellwood",
  "1541KR8BHXDM" => "Winter Park",
  "1541KR8BMB3L" => "Winter Garden",
  "1541KR8BM8LI" => "Windermere",
  "1541KR8BFKKR" => "Wilbur by the Sea",
  "1541KR8BJCT2" => "West Melbourne",
  "1541KR8BJY4B" => "West Cocoa",
  "1AK20FZQZ60X" => "Waynesville",
  "1541KR8BKCBY" => "Viera",
  "1541KR8BKY8R" => "Vero Beach",
  "1541KR8BJ04S" => "Ventura",
  "1541KR8BIO0S" => "Union Park",
  "1541KR8BL0EL" => "Tropic",
  "1541KR8BHUWM" => "Titusville",
  "1ASQO8LADWKC" => "Temple Terrace",
  "1541KR8BJ2OL" => "Taft",
  "1541KR8BM1CC" => "St. Cloud",

  "17VIFFTIBGS0" => "South Daytona",
  "1536QBVU50HK" => "Kissimmee",
  "1536QBVU4XWK" => "Kenansville",
  "1536QBVT7U7Q" => "Indian River Shores",
  "17VIFFTHMEPL" => "Indian Harbour Beach",
  "1536QBVU96G3" => "Indialantic",
  "17VIFFTHKX9L" => "Hunt Club",
  "1536QBVU93YY" => "Holly Hill",
  "17VIFFTHIXP5" => "Hiawassee",
  "1536QBVU4CPT" => "Harmony",
  "17VIFFTHF6V0" => "Grant Valkaria",
  "1536QBVU912G" => "Grant",
  "1536QBVTWGGT" => "Gotha",
  "17VIFFTHD3UD" => "Golden Oak",
  "1536QBVT6BTX" => "Gifford",
  "17VIFFTHAVQL" => "Ft. Pierce",
  "1536QBVT5WBU" => "Fellsmere",
  "17VIFFTH98T5" => "Enterprise",
  "1536QBVTW1GM" => "Edgewood",
  "1536QBVU8YKC" => "Edgewater",
  "17VIFFTH750Y" => "Eau Gallie",
  "1536QBVTVYPE" => "Eatonville",
  "1536QBVU8VQ6" => "DeLand",
  "1536QBVU8T3Z" => "Deltona",
  "17VIFFTH5BH0" => "Debary",
  "1536QBVU8NU5" => "De Leon Springs",
  "1536QBVU8L7T" => "Daytona Beach Shores",
  "1536QBVU8ISF" => "Daytona Beach",
  "1536QBVTMI8G" => "Cocoa Beach",
  "1536QBVTMFWO" => "Cocoa",
  "1AQSGIVHGJYC" => "Clermont",
  "17VIFFTH2N4Z" => "Citrus Ridge",
  "1536QBVTLVZS" => "Chuluota",
  "1536QBVTVT46" => "Christmas",
  "1536QBVU14BN" => "Celebration",
  "1536QBVTKOHC" => "Cape Canaveral",
  "17VIFFTH0V94" => "Canaveral Air Station",
  "1536QBVWTOB8" => "Buena Ventura Lakes",
  "1536QBVTHKED" => "Belle Isle",
  "1536QBVTGUNR" => "Bay Lake",
  "1536QBVU8FYY" => "Barefoot Bay",
  "17VIFFTGYRWQ" => "Azalea Park",
  "1536QBVTGC8T" => "Apopka",
  "17VIFFTGWDFQ" => "Aloma",
  "1536QBVTVQ9I" => "Alafaya",
  "1536QBVUD9AT" => "Seville",
  "1536QBVTDARR" => "Sebastian",
  "1536QBVUD47I" => "Satellite Beach",
  "17VIFFTI9L7H" => "Sand Lake",
  "1536QBVUD17W" => "Rockledge",
  "1536QBVU5RV4" => "Reunion",
  "1536QBVTCKLG" => "Port St Lucie",
  "17VIFFTI8513" => "Port Saint John",
  "1536QBVUCYMY" => "Port Orange",
  "1AK20FZR0OY0" => "Port Charlotte",
  "17VIFFTI6O42" => "Port Canaveral",
  "17VIFFTI4ZJJ" => "Ponce Inlet",
  "17VIFFTI3IX2" => "Point Orange",
  "1536QBVU5M8A" => "Poinciana",
  "1536QBVTZX0T" => "Pine Hills",
  "1536QBVTZUGP" => "Pine Castle",
  "1536QBVUCSPQ" => "Pierson",
  "17VIFFTI21C1" => "Patrick AFB",
  "1536QBVUCQ4S" => "Palm Shores",
  "1536QBVUCNAN" => "Palm Bay",
  "17VIFFTI0NRG" => "Other",
  "1536QBVUCK8T" => "Osteen",
  "17VIFFTHZ4Q1" => "Ormond Beach",
  "17VIFFTHX8EH" => "Orlo Vista",
  "1536QBVTZLQK" => "Orlando",
  "1536QBVTB8JG" => "Orchid",
  "17VIFFTHVSTN" => "Orange City",
  "1536QBVTZITG" => "Ocoee",
  "1ARR7U5522R3" => "Ocala",
  "1536QBVTZG0U" => "Oakland",
  "17VIFFTHUDME" => "Oak Hill",
  "1536QBVUA1EO" => "New Smyrna Beach",
  "17VIFFTHS77F" => "New Smyrna",
  "17VIFFTHQ5IO" => "Mims",
  "1536QBVU9Z04" => "Micco",
  "1536QBVU9WGD" => "Merritt Island",
  "1536QBVU9TUR" => "Melbourne Village",
  "1536QBVU9RDB" => "Melbourne Beach",
  "1536QBVU9OPG" => "Melbourne",
  "1536QBVU9M2G" => "Malabar",
  "1536QBVTX3AM" => "Maitland",
  "17VIFFTHO412" => "Magnolia Square",
  "1536QBVTWXM6" => "Lockhart",
  "1536QBVU9BR6" => "Lake Helen",
  "1536QBVU0W7B" => "Zellwood",
  "1536QBVU0QT2" => "Winter Park",
  "1536QBVU0O3E" => "Winter Garden",
  "1536QBVU0LH7" => "Windermere",
  "17VIFFTIMFCY" => "Wilbur by the Sea",
  "1536QBVUDU42" => "West Melbourne",
  "17VIFFTIKMCX" => "West Cocoa",
  "17VIFFTIJ9QS" => "Waynesville",
  "1536QBVUDOXO" => "Viera",
  "1536QBVTEMZQ" => "Vero Beach",
  "17VIFFTIEWIE" => "Ventura",
  "1536QBVU0CW8" => "Union Park",
  "1536QBVTE9JX" => "Tropic",
  "1536QBVUDHA4" => "Titusville",
  "1536QBVU07RZ" => "Taft",
  "1ARTPEK0KFIS" => "Starke",
  "1536QBVU5UHQ" => "St. Cloud",
  "17VIFFTID8ON" => "South Daytona Beach",

  "17VIFMC8JFNU" => "South Daytona Beach",
  "17VIFMC8F8DS" => "Kissimmee",
  "17VIFMC8FBCA" => "Kenansville",
  "17VIFMC8ISCS" => "Indian River Shores",
  "17VIFMC8KP0L" => "Indian Harbour Beach",
  "17VIFMC8D1UF" => "Indialantic",
  "17VIFMC8KRWW" => "Hunt Club",
  "17VIFMC8D4TJ" => "Holly Hill",
  "17VIFMC8KVPN" => "Hiawassee",
  "17VIFMC8FEYC" => "Harmony",
  "17VIFMC8L22B" => "Grant Valkaria",
  "17VIFMC8D978" => "Grant",
  "17VIFMC8H0DX" => "Gotha",
  "17VIFMC8L5HJ" => "Golden Oak",
  "17VIFMC8IW1R" => "Gifford",
  "17VIFMC8L8PU" => "Ft. Pierce",
  "17VIFMC8IYGT" => "Fellsmere",
  "17VIFMC8LD01" => "Enterprise",
  "17VIFMC8H3HU" => "Edgewood",
  "17VIFMC8DCN7" => "Edgewater",
  "17VIFMC8LFIW" => "Eau Gallie",
  "17VIFMC8H6UB" => "Eatonville",
  "17VIFMC8DG6P" => "DeLand",
  "17VIFMC8DJC9" => "Deltona",
  "17VIFMC8LII7" => "Debary",
  "17VIFMC8DME4" => "De Leon Springs",
  "17VIFMC8EL9C" => "Daytona Beach Shores",
  "17VIFMC8EORL" => "Daytona Beach",
  "17VIFMC8HGAD" => "Cocoa Beach",
  "17VIFMC8HJ9A" => "Cocoa",
  "1AQSGJ7R972O" => "Clermont",
  "17VIFMC8LLKU" => "Citrus Ridge",
  "17VIFMC8HMGZ" => "Chuluota",
  "17VIFMC8HA5Q" => "Christmas",
  "17VIFMC8FINN" => "Celebration",
  "17VIFMC8HORD" => "Cape Canaveral",
  "17VIFMC8MMEW" => "Canaveral Air Station",
  "17VIFMC8N8HO" => "Buena Ventura Lakes",
  "17VIFMC8HRU1" => "Belle Isle",
  "17VIFMC8HV8N" => "Bay Lake",
  "17VIFMC8ER86" => "Barefoot Bay",
  "17VIFMC8MR05" => "Azalea Park",
  "17VIFMC8HYNX" => "Apopka",
  "17VIFMC8MVAM" => "Aloma",
  "17VIFMC8HCZ7" => "Alafaya",
  "17VIFMC8JIY9" => "South Daytona",
  "17VIFMC8NOX5" => "Seville",
  "17VIFMC8ID6T" => "Sebastian",
  "17VIFMC8NSKT" => "Satellite Beach",
  "17VIFMC8JM8F" => "Sand Lake",
  "17VIFMC8NW20" => "Rockledge",
  "17VIFMC8F1LF" => "Reunion",
  "17VIFMC8IGFQ" => "Port St Lucie",
  "17VIFMC8JPC5" => "Port Saint John",
  "17VIFMC8NZN5" => "Port Orange",
  "1AK20FZR2P5S" => "Port Charlotte",
  "17VIFMC8JSMI" => "Port Canaveral",
  "17VIFMC8JW6C" => "Ponce Inlet",
  "17VIFMC8JZMD" => "Point Orange",
  "17VIFMC8F534" => "Poinciana",
  "17VIFMC8GA3I" => "Pine Hills",
  "17VIFMC8GDD0" => "Pine Castle",
  "17VIFMC8O328" => "Pierson",
  "17VIFMC8K3OO" => "Patrick AFB",
  "17VIFMC8O6G0" => "Palm Shores",
  "17VIFMC8O9WT" => "Palm Bay",
  "17VIFMC8K7K0" => "Other",
  "17VIFMC8OD47" => "Osteen",
  "17VIFMC8KBMC" => "Ormond Beach",
  "17VIFMC8KF7G" => "Orlo Vista",
  "17VIFMC8GJXW" => "Orlando",
  "17VIFMC8IJR3" => "Orchid",
  "17VIFMC8KJ9K" => "Orange City",
  "17VIFMC8GNJD" => "Ocoee",
  "17VIFMC8GQSO" => "Oakland",
  "17VIFMC8MYB4" => "Oak Hill",
  "17VIFMC8OGEL" => "New Smyrna Beach",
  "17VIFMC8N1MM" => "New Smyrna",
  "17VIFMC8N53C" => "Mims",
  "17VIFMC8AMOK" => "Micco",
  "17VIFMC8BLSK" => "Merritt Island",
  "17VIFMC8CLCF" => "Melbourne Village",
  "17VIFMC8COOA" => "Melbourne Beach",
  "17VIFMC8CRNG" => "Melbourne",
  "17VIFMC8CUYD" => "Malabar",
  "17VIFMC8GU02" => "Maitland",
  "17VIFMC8KMFE" => "Magnolia Square",
  "17VIFMC8GX67" => "Lockhart",
  "17VIFMC8CY7U" => "Lake Helen",
  "17VIFMC8FLWS" => "Zellwood",
  "17VIFMC8FPO2" => "Winter Park",
  "17VIFMC8FT03" => "Winter Garden",
  "17VIFMC8FWB3" => "Windermere",
  "17VIFMC8J27I" => "Wilbur by the Sea",
  "17VIFMC8NF9P" => "West Melbourne",
  "17VIFMC8J5DE" => "West Cocoa",
  "17VIFMC8J8JF" => "Waynesville",
  "17VIFMC8NILB" => "Viera",
  "17VIFMC8I5P1" => "Vero Beach",
  "17VIFMC8JC5Q" => "Ventura",
  "17VIFMC8FZF7" => "Union Park",
  "17VIFMC8I9WA" => "Tropic",
  "17VIFMC8NLLU" => "Titusville",
  "17VIFMC8G37U" => "Taft",
  "17VIFMC8EY4L" => "St. Cloud"

  },
cooling_system_type: {
  "1536QBSWK011" => "Central",
  "1536QBSWRRRQ" => "Electric",
  "1536QBSX8L7U" => "Wall - Window",
  "156WQGPG250R" => "Attic Fan",
  "156WQGPS0DK6" => "Air Purifier",
  "156WQGQQ7WFL" => "Whole House Fan",
  "156WQGR32VUO" => "Multi-Zone",
  "156WQGR8UGBZ" => "None",
  "156WQGSO5ICE" => "Other"
},
county: {
  "1AQX2NQ16T6P" => "Baker",
  "153Y0WVBNNTV" => "Volusia",
  "1AN37BR6Y07C" => "Sumter",
  "158XDN502SF9" => "St. Lucie",
  "1AJ4OW82C97T" => "St. Johns",
  "158YSXD5T4ZD" => "Seminole",
  "17UF37UM8Y81" => "Polk",
  "1AQWQ1L3BIE0" => "Pasco",
  "1AP2356AUWJR" => "Palm Beach",
  "153Y0WVL0L83" => "Other",
  "153Y0WUH0VAB" => "Osceola",
  "153Y0WVJ3N8U" => "Orange",
  "1AL1GOER18UC" => "Okeechobee",
  "1592C3HT1CAF" => "Miami-Dade",
  "1ARURHSK18DX" => "Marion",
  "1AQX2NQIRXW2" => "Leon",
  "17ZFIEKMMJL6" => "Lake",
  "153Y0WU2D97V" => "Indian River",
  "17WJB4U1SEAB" => "Hillsborough",
  "1ATS8U8NQLNV" => "Highlands",
  "1AQWQ1TBSTT9" => "Hernando",
  "17UH7JP7AG63" => "Henderson (NC)",
  "15A2V3Y3U9WM" => "Haywood (NC)",
  "17UMJ4YB56HH" => "Collier",
  "1AK207HZPHTR" => "Charlotte",
  "17ZCOHKR2QEX" => "Broward",
  "153Y0WTTXNI5" => "Brevard",
  "1AMZMWXK6FL0" => "Bay"
},
equipment: {
  "1536QBSWY6Q2" => "Compactor",
  "1536QBSX10Z9" => "Dishwasher",
  "1536QBSX149O" => "Disposal",
  "1536QBSX1PPH" => "Dryer",
  "1536QBSX4VHJ" => "None",
  "1536QBSX85Q2" => "Stand Alone Freezer",
  "1536QBSXD4BY" => "Ice Maker Hookup",
  "1536QBSXHGFY" => "Laundry-Hookup",
  "1536QBSXOUBN" => "Microwave-Built-in",
  "1536QBSXVEGT" => "Indoor Grill",
  "1536QBSXVT1B" => "Range - Electric",
  "1536QBSXVVYO" => "Range - Gas",
  "1536QBSXW2AO" => "Refrigerator",
  "15373PGXFQVU" => "Garage Door Opener",
  "15373PHC18WI" => "Oven-Built In",
  "15373PIDMSE5" => "Oven-Double",
  "15373PIN8CTP" => "Rented Equipment",
  "15373PIZSITS" => "Vacuum Built-In",
  "15373PK5YR6L" => "Water Softener-Owned",
  "15373PKG4WCG" => "Microwave",
  "15373PLZGLZL" => "Generator",
  "15373PM98BNM" => "Generator Hookup",
  "15373PMIACUP" => "Other - Call Agent",
  "1536QBSYGB8A" => "Water Softener-Rent",
  "156WQA075NY6" => "Washer",
  "156XDK2A6FJB" => "Oven-Convection",
  "156XDK46R2ET" => "Intercom",
  "156XDK4KOK0K" => "Refrigerator-Second",
  "157099FEV8TW" => "Freezer",
  "157099OT5GJX" => "Water Treatment Sys",
  "15373PFNYRQK" => "Bar Refrigerator",
  "15373PFYBHPC" => "Cook Top",
  "17SPEPR6FXHI" => "Dryer - Gas",
  "153ZGBGC9BOV" => "South Daytona Beach",
  "153ZGBGCERVO" => "Kissimmee",
  "153ZGBGCEPGF" => "Kenansville",
  "153ZGBGCE99Z" => "Indian River Shores",
  "153ZGBGCDH6S" => "Indian Harbour Beach",
  "153ZGBGCCHT0" => "Indialantic",
  "153ZGBGCA7QS" => "Hunt Club",
  "153ZGBGC93TE" => "Holly Hill",
  "153ZGBGCBZ4C" => "Hiawassee",
  "153ZGBGCFB6J" => "Harmony",
  "153ZGBGCCR7F" => "Grant Valkaria",
  "153ZGBGCDQJT" => "Grant",
  "153ZGBGCENBJ" => "Gotha",
  "153ZGBGCCCUJ" => "Golden Oak",
  "153ZGBGCEG6E" => "Gifford",
  "1AK20FZQN710" => "Ft. Pierce",
  "153ZGBGCDO1U" => "Fellsmere",
  "153ZGBGCAJVL" => "Enterprise",
  "153ZGBGCBMDV" => "Edgewood",
  "153ZGBGC9QT1" => "Edgewater",
  "153ZGBGCDCLS" => "Eau Gallie",
  "153ZGBGCAR0T" => "Eatonville",
  "1ARSN0897CXS" => "Dundee",
  "153ZGBGCAETO" => "DeLand",
  "153ZGBGCAHA5" => "Deltona",
  "153ZGBGCACMB" => "Debary",
  "153ZGBGC9OD9" => "De Leon Springs",
  "153ZGBGC990A" => "Daytona Beach Shores",
  "153ZGBGC918E" => "Daytona Beach",
  "153ZGBGCDA88" => "Cocoa Beach",
  "153ZGBGCCYFI" => "Cocoa",
  "153ZGBGCEDW5" => "Citrus Ridge",
  "1591AIHOZUQW" => "Chuluota",
  "153ZGBGCAA6H" => "Christmas",
  "153ZGBGCEX1F" => "Celebration",
  "153ZGBGCCTN4" => "Cape Canaveral",
  "153ZGBGCD323" => "Canaveral Air Station",
  "153ZGBGCEUMP" => "Buena Ventura Lakes",
  "153ZGBGCBJYY" => "Belle Isle",
  "153ZGBGCC5ZR" => "Bay Lake",
  "153ZGBGCEIJ6" => "Barefoot Bay",
  "153ZGBGCBF85" => "Azalea Park",
  "153ZGBGCA555" => "Apopka",
  "153ZGBGCB7Y4" => "Aloma",
  "153ZGBGCC3LM" => "Alafaya",
  "153ZGBGC9GXU" => "South Daytona",
  "153ZGBGCA2PM" => "Seville",
  "153ZGBGCE29E" => "Sebastian",
  "153ZGBGCDEQI" => "Satellite Beach",
  "153ZGBGCC19W" => "Sand Lake",
  "153ZGBGCDZVR" => "Rockledge",
  "153ZGBGCEZEM" => "Reunion",
  "1591AIHP2BME" => "Port St Lucie",
  "153ZGBGCD7SU" => "Port Saint John",
  "153ZGBGC96EW" => "Port Orange",
  "1AK20FZQQCMI" => "Port Charlotte",
  "153ZGBGCCW19" => "Port Canaveral",
  "153ZGBGC9JCO" => "Ponce Inlet",
  "153ZGBGC9E8Y" => "Point Orange",
  "153ZGBGCF1LH" => "Poinciana",
  "153ZGBGCBHK1" => "Pine Hills",
  "153ZGBGCBOPG" => "Pine Castle",
  "153ZGBGCA0A5" => "Pierson",
  "153ZGBGCD0KM" => "Patrick AFB",
  "153ZGBGCDLU7" => "Palm Shores",
  "153ZGBGCCOSG" => "Palm Bay",
  "153ZGBGC8YHE" => "Other",
  "153ZGBGCB0SG" => "Osteen",
  "153ZGBGC9Y43" => "Ormond Beach",
  "153ZGBGCBU7D" => "Orlo Vista",
  "153ZGBGCBCU5" => "Orlando",
  "153ZGBGCEBM2" => "Orchid",
  "153ZGBGCAYBN" => "Orange City",
  "153ZGBGCF41K" => "Ocoee",
  "153ZGBGCFI7S" => "Oakland",
  "153ZGBGCAVW6" => "Oak Hill",
  "153ZGBGC9TBS" => "New Smyrna Beach",
  "153ZGBGC9VPY" => "New Smyrna",
  "153ZGBGCATDO" => "Mims",
  "153ZGBGCEKYD" => "Micco",
  "153ZGBGCDXGJ" => "Merritt Island",
  "153ZGBGCCMDM" => "Melbourne Village",
  "153ZGBGCDVBF" => "Melbourne Beach",
  "153ZGBGCCFAZ" => "Melbourne",
  "153ZGBGCDSY2" => "Malabar",
  "153ZGBGCAOKF" => "Maitland",
  "153ZGBGCF8ZX" => "Magnolia Square",
  "153ZGBGCBRYB" => "Lockhart",
  "153ZGBGCAMAR" => "Lake Helen",
  "153ZGBGCBABS" => "Zellwood",
  "153ZGBGCB5E3" => "Winter Park",
  "153ZGBGCFFW3" => "Winter Garden",
  "153ZGBGCFDJ5" => "Windermere",
  "153ZGBGC9LRZ" => "Wilbur by the Sea",
  "153ZGBGCCK6A" => "West Melbourne",
  "153ZGBGCD5G5" => "West Cocoa",
  "1AK20FZQSGTY" => "Waynesville",
  "153ZGBGCDJHG" => "Viera",
  "153ZGBGCE4NA" => "Vero Beach",
  "153ZGBGCC8DH" => "Ventura",
  "153ZGBGCBWJ8" => "Union Park",
  "153ZGBGCE72E" => "Tropic",
  "153ZGBGCB357" => "Titusville",
  "153ZGBGCCAJF" => "Taft",
  "153ZGBGCF6IQ" => "St. Cloud",
},
exterior_description: {
  "1536QBSX24JH" => "Barbeque",
  "1536QBSX27JQ" => "Balcony - Screened",
  "1536QBSX2APC" => "Barn/Stalls",
  "1536QBSX2DU8" => "Courtyard",
  "1536QBSX2NIX" => "Deck",
  "1536QBSX2R55" => "Dog House/Run",
  "1536QBSX36PC" => "Patio Slab",
  "1536QBSX3AOA" => "Gazebo",
  "1536QBSX3OEO" => "Other - Call Agent",
  "1536QBSX42AM" => "Storage/Shed",
  "1536QBSX46X1" => "Sprinkler - Reclaimd",
  "1536QBSX4D4D" => "Sprinkler - Well",
  "1536QBSX4G8G" => "Storm Shutters",
  "1536QBSX4P7F" => "Well - Private",
  "1536QBSXARQ4" => "Green House",
  "1536QBSY1USC" => "Sprinkler - City",
  "1536QBSYHL0J" => "Well - Shared",
  "157VTMR8YCQK" => "Patio - Screened",
  "157VTMS802I6" => "Patio - Unscreened",
  "156WQ1WTJBVJ" => "Balcony - Unscreened",
  "156WQ1Y1FXQ6" => "Balcony - WrapAround",
  "156WQ1YC16Q6" => "Balcony - Side",
  "156WQ2221945" => "Fire Pit",
  "156WQ2305EK6" => "Pole Barn",
  "156WQ23COAAU" => "Porch - Screened",
  "156WQ23N26IQ" => "Porch - Unscreened",
  "156WQ23XL60J" => "Porch - Enclosed",
  "156WQ24XN7B6" => "Porch - Trussed",
  "156WQ25A53XQ" => "Porch - Wrap Around",
  "156WQ26AFILT" => "Fence - Vinyl",
  "156WQ26KW8VV" => "Fence - Wood",
  "156WQ26UJ953" => "Fence - Metal",
  "156WQ273ZWVR" => "Fence - Chain",
  "156WQ28MIOZR" => "Sprinkler - Ponds",
  "156WQ29S8BED" => "Breezeway",
  "156WQ2A3DF0A" => "Retractable Awning",
  "156WQ2AIYUIJ" => "Custom Extr Lighting",
  "156WQ2BMC23N" => "Ext Wash/Dryer Hkup",
  "156WQ2F5CI85" => "Outdoor Shower",
  "156WQ2G8MIWM" => "Tennis Court (Pvt)",
  "15373CFL5Y2K" => "Summer Kitchen",
  "17UFFRGIF4L8" => "Workshop",
  "1AO1WZQD38ZY" => "Fence - Concrete"
},
floor_type: {
  "1536QBSX6YCU" => "Marble",
  "1536QBSX718A" => "Concrete",
  "1536QBSX745H" => "Brick",
  "1536QBSX77YC" => "Laminate",
  "1536QBSX7AXX" => "Other - Call Agent",
  "1536QBSX6GBE" => "Vinyl",
  "1536QBSX6JAL" => "Wood",
  "1536QBSX6QOT" => "Terrazzo",
  "1536QBSX6TWL" => "Carpet",
  "1536QBSYBG7O" => "Tile",
  "156WQ1N9VJC3" => "Ceramic Tile",
  "156WQ1QHHY1B" => "Slate",
  "156WQ1QM0W31" => "Stone",
  "156WQ1TCD4KB" => "Travertine",
  "156WQ1TO19G9" => "Decorative Concrete"
},
hoa_membership: {
  "157XL1T2PZGR" => "Condo",
  "157XL1T2Q74N" => "HOA Home Owners Voluntary",
  "157XL1T2Q375" => "HOA Home Owners Mandatory",
  "157XL1T2QALJ" => "HOA - None"
},
interior_description: {
  "1536QBSXDC7V" => "Closet - Walk-Ins",
  "1536QBSXDF6B" => "Breakfast Bar",
  "1536QBSXDMG0" => "Ceiling Fan(s)",
  "1536QBSXE2ZK" => "Other - Call Agent",
  "1536QBSXE64V" => "Kitchen - Island",
  "1536QBSXE98M" => "Open Floor Plan",
  "1536QBSXEFBK" => "Window Treatments",
  "1536QBSXFBPZ" => "Living/Dining Combo",
  "1536QBSXFILI" => "Handicap Features",
  "1536QBSXFR8Y" => "Bar - Wet",
  "1536QBSXFVUZ" => "Ceilings-Ctdrl/Vault",
  "1536QBSXG57Y" => "Built-in-Features",
  "1536QBSXG84R" => "Kitchen - Eat In",
  "1536QBSXGBWL" => "Sky Light(s)",
  "156WQ6VWF28K" => "Laundry in Garage",
  "156WQ6WUOQ5Z" => "Bar",
  "156WQ6XCH6SE" => "Breakfast Nook",
  "156WQ6YNERDG" => "Pantry - Butler",
  "156WQ6Z22XK2" => "Pantry - Walk-in",
  "156WQ704KNPZ" => "Cook Island",
  "156WQ70DCNPU" => "Prep Sink",
  "156WQ70PVT0T" => "Elevator Private",
  "156WQ71LI0AT" => "Solar Tubes",
  "156WQ71XF3B3" => "Laundry Chute",
  "156WQ73DWSA3" => "Laundry Tub",
  "156WQ73S1FWP" => "Pull Down Stairs",
  "156WQ73Y75D1" => "Pantry"
},
lot_description: {
  "156WPSX3MLUX" => "Paved Street",
  "156WPSY2PLH2" => "Private Road",
  "156WPSYCL80Q" => "Easement Access",
  "156WPSYNQA0F" => "East of US1",
  "156WPSZUBHNJ" => "West of US1",
  "156WPT0AY1V0" => "Air Strip",
  "156WPT1A1DEC" => "Airport Community",
  "156WPT1KW6E1" => "Private Air Strip",
  "156WPT1US1TR" => "Historic Area",
  "1536QKHI6NLF" => "Corner Lot",
  "1536QKIOJAY0" => "Cul-de-Sac",
  "1536QKIX6UFF" => "Dead End Street",
  "1536QKJAAR4A" => "Drainage Canal",
  "1536QKPZZXFH" => "Fld Ins May be Req",
  "1536QKQVOYA4" => "Fruit Trees",
  "1536QKR4E4GR" => "Irregular",
  "1536QKSVWJQE" => "City",
  "1536QKT2JJK5" => "County",
  "1536QKUHPSHX" => "Flag",
  "1536QKUQ1ZQV" => "Major Roadway",
  "1536QKUZZYAO" => "Golf Course",
  "1536QKW5K8ZC" => "Producing Groves",
  "1536QKXJ480U" => "Sidewalks",
  "1536QKXRSBYZ" => "Unpaved Street",
  "1536QKYBJ363" => "Wooded",
  "1536QKZCHXVZ" => "Zero Lot Line",
  "1536QKZM0RGM" => "Farm (Agricultural)",
  "1536QN5E7F00" => "Sold As-Is",
  "1536QN5L3QMY" => "Other - Call Agent"
},  
mls_elementary_school: {
  "17TMQ86AHJM2" => "Suntree",
  "17TMQ865AVVB" => "Sunrise",
  "17TMQ860ZJDS" => "Sherwood",
  "17TMQ85V9P2M" => "Sea Park",
  "17TMQ85LGXL4" => "Saturn",
  "17TMQ84SC4NQ" => "Sabal",
  "17TMQ84M7UC8" => "Roy Allen",
  "17TMQ84GWMUP" => "Roosevelt",
  "17TMQ84C8OBF" => "Riviera",
  "17TMQ844AND2" => "Quest",
  "17TMQ841HXF7" => "Port Malabar",
  "17TMQ83X85M7" => "Pinewood",
  "17TMQ835OIJW" => "Palm Bay Elem",
  "17TMQ831UAH6" => "Ocean Breeze",
  "17TMQ82XFDX4" => "Oak Park",
  "17TMQM9RUDLQ" => "Not Verified",
  "17TMQ82TJXTN" => "Mims",
  "17TMQ82QANMZ" => "Mila",
  "17TMQ82M4FQ2" => "Meadowlane",
  "17TMQ82I1IED" => "McAuliffe",
  "17TMQ82E5U6R" => "Manatee",
  "17TMQ82A2YD6" => "Longleaf",
  "17TMQ81IIU4P" => "Lockmar",
  "17TMQ81ETNH8" => "Jupiter",
  "17TMQ819WU7E" => "Indialantic",
  "17TMQ8166LB2" => "Imperial Estates",
  "17TMQ812LHRS" => "Holland",
  "17TMQ80YB30F" => "Harbor City",
  "17TMQ80U6B4H" => "Golfview",
  "17TMQ80QJBLH" => "Gemini",
  "17TMQ80MTKSO" => "Fairglen",
  "17TMQ7ZVPNQE" => "Enterprise",
  "17TMQ7ZS58JA" => "Endeavour",
  "17TMQ7ZNMY8O" => "Discovery",
  "17TMQ7ZKFWLD" => "Croton",
  "17TMQ7ZH1JLO" => "Creel",
  "17TMQ7ZDFR6T" => "Coquina",
  "17TMQ7ZA9JXO" => "Columbia",
  "17TMQ7Z6OVW3" => "Challenger 7",
  "17TMQ7Z1K5TJ" => "Carroll",
  "17TMQ7YXOZBB" => "Cape View",
  "17TMQ7Y69Z8R" => "Cambridge",
  "17TMQ7Y2RYVD" => "Audubon",
  "17TMQ7XZ5W66" => "Atlantis",
  "17TMQ7XVF62R" => "Apollo",
  "17TMQ7XHQ1EA" => "Andersen",
  "17TMQ87OUIET" => "Williams",
  "17TMQ87L1C9R" => "Westside",
  "17TMQ87GUTQ8" => "University Park",
  "17TMQ87BZD6Z" => "Turner",
  "17TMQ8774830" => "Tropical",
  "17TMQ86EYYFY" => "Surfside"
},
mls_high_school: {
  "17TMQCHM2QAZ" => "Astronaut",
  "17TMQCJLO93M" => "Viera",
  "17TMQCJHGTN4" => "Titusville",
  "17TMQCJEOLVM" => "Space Coast",
  "17TMQCJAYO40" => "Satellite",
  "17TMQCJ7RC7Z" => "Rockledge",
  "17TMQCJ4IPKY" => "Palm Bay",
  "17TMQMB3M9NG" => "Not Verified",
  "17TMQCIYNG9C" => "Merritt Island",
  "17TMQCIVLOXV" => "Melbourne",
  "17TMQCISRVG2" => "Heritage",
  "17TMQCIQAQWC" => "Eau Gallie",
  "17TMQCHYMH0D" => "Cocoa Beach",
  "17TMQCHUTR15" => "Cocoa",
  "17TMQCHOKOHW" => "Bayside"
},
mls_middle_school: {
  "17TMQCEFPHP4" => "Central",
  "17TMQCG9BEYG" => "Stone",
  "17TMQCG5VK5K" => "Space Coast",
  "17TMQCG2VS1I" => "Southwest",
  "17TMQMA7J4SZ" => "Not Verified",
  "17TMQCFZNSV6" => "McNair",
  "17TMQCFWZ7FH" => "Madison",
  "17TMQCFSSUVT" => "Kennedy",
  "17TMQCFPOOSG" => "Johnson",
  "17TMQCFMVW0H" => "Jefferson",
  "17TMQCFK7M5Z" => "Jackson",
  "17TMQCFH7XDC" => "Hoover",
  "17TMQCFEAJSO" => "DeLaura",
  "17TMQCEMQT1U" => "Cocoa Beach",
  "17TMQCEJG78D" => "Cocoa"
},
new_construction: {
  "156WO1I37IB6" => "No",
  "156WO1I19V00" => "Yes"
},
pool_private: {
  "17UL3FKVOLSB" => "No",
  "17UL3FKVPRNU" => "Yes"
},
rental_terms: {
  "1541KZRYRLM7" => "1 Wk Minimum",
  "1541KZS5RWSW" => "2 Wk Minimum",
  "1541KZSYPTP8" => "3 Wk Minimum",
  "1541KZT82A2W" => "1 Mth Minimum",
  "1541KZTEIBQ5" => "2 Mth Minimum",
  "1541KZTIXBJ7" => "3 Mth Minimum",
  "1541KZTO1NHV" => "4 Mth Minimum",
  "1541KZTUR777" => "5 Mth Minimum",
  "1541L069KNZL" => "6 Mth Minimum",
  "1541L06SRN32" => "1 Yr Minimum",
  "1541L07457I4" => "Property for Sale",
  "1541L084TSSP" => "Lease Purchase Opt",
  "1541L08D41S9" => "Daily",
  "1541L09ML8VE" => "7 Mth Minimum",
  "1541L09S05EO" => "Section 8",
  "1541L0A46V06" => "Other - Call Agent"
},  
roof_type: {
  "153YDN6ROKTE" => "Metal",
  "153YDN6ROO5R" => "Membrane",
  "153YDN6ROR2C" => "Other - Call Agent",
  "153YDN6ROUIE" => "Shingle - Wood",
  "153YDN6ROXFA" => "Tile",
  "153YDN6RP0FK" => "Shingle - Asphalt",
  "156Z7D7JX0JN" => "Concrete",
  "156Z7D7U7VT1" => "Gravel",
  "1540V3NI5S3T" => "Rolled Comp"
},
showing_instructions: {
  "156WDNXB08IG" => "Agent",
  "1541KR8CO25E" => "Tenant",
  "1541KR8CNZM8" => "Showing Service",
  "1541KR8CNX62" => "Owner",
  "1541KR8CNUEK" => "List Office"
},
state: {
  "153Y0WW3W4DL" => "FL",
  "15A2V40BIOUU" => "NC"
},
street_direction: {
  "1536QBU2S55F" => "E",
  "1536QBU2S7XQ" => "W",
  "1536QBU2S2OI" => "SW",
  "1536QBU2S016" => "SE",
  "1536QBU2RXN2" => "S",
  "1536QBU2RUZ0" => "NW",
  "1536QBU2RSLE" => "NE",
  "1536QBU2RPU9" => "N",
  "153ZGBGMO68Z" => "E",
  "153ZGBGMO8OU" => "W",
  "153ZGBGMO3RJ" => "SW",
  "153ZGBGMO1DB" => "SE",
  "153ZGBGMNYXY" => "S",
  "153ZGBGMNWIV" => "NW",
  "153ZGBGMNU01" => "NE",
  "153ZGBGMNRD9" => "N",
  "154185HUTYRT" => "E",
  "154185HUU168" => "W",
  "154185HUTW94" => "SW",
  "154185HUTU86" => "SE",
  "154185HUTSE1" => "S",
  "154185HUTPU6" => "NW",
  "154185HUTNCS" => "NE",
  "154185HUTKUO" => "N",
  "1541KR8L20Q4" => "E",
  "1541KR8L23AS" => "W",
  "1541KR8L1Y5Q" => "SW",
  "1541KR8L1VNL" => "SE",
  "1541KR8L1T6F" => "S",
  "1541KR8L1QOG" => "NW",
  "1541KR8L1OH9" => "NE",
  "1541KR8L1KZN" => "N",
  "1536QBVYDCFX" => "E",
  "1536QBVYDUVH" => "W",
  "1536QBVYDS4F" => "SW",
  "1536QBVYDPPC" => "SE",
  "1536QBVYDN2N" => "S",
  "1536QBVYDKLA" => "NW",
  "1536QBVYDHTD" => "NE",
  "1536QBVYDEYV" => "N",
  "17VIFMCDXWPN" => "E",
  "17VIFMCDYMT0" => "W",
  "17VIFMCDYJBU" => "SW",
  "17VIFMCDYF27" => "SE",
  "17VIFMCDYBKX" => "S",
  "17VIFMCDY6JM" => "NW",
  "17VIFMCDY216" => "NE",
  "17VIFMCDXZ5X" => "N"
},
street_surface_type: {
  "156WPNUU8B6A" => "3+ Stories",
  "156WPNV72X7H" => "Manufactured Triple",
  "156WPNW6GNHI" => "Stilt/Pilings",
  "156WPTUZBNW7" => "Multi-Level",
  "156WPTV7G46R" => "Split Level",
  "1536QK9J650P" => "1 Story",
  "1536QKADGGXF" => "2 Story",
  "1536QKC6L0Y0" => "Multi Homes on Prop",
  "1536QKE596IA" => "Manufactured Single",
  "1536QKEDFBIE" => "Manufactured Double"
},
style_of_building: {
  "1536QBSWVE8U" => "Combination",
  "1536QBSWVH86" => "Frame - Metal",
  "1536QBSWVV3E" => "Concrete Poured",
  "1536QBSWVZN1" => "Concrete Block",
  "1536QBSWW9T7" => "Frame - Wood",
  "1536QBSWX6YF" => "Other - Call Agent",
  "1536QBSWX9XS" => "Log"
},
subdivision: {
  "157V3CZQCVWE" => "YACHT HAVEN SEC 2 FIRST UNIT",
  "157V3CYN1TXE" => "A REPLAT OF TRACT A OF SABAL GROVE UNIT TWO",
  "157V3CYN1LAZ" => "A REPLAT OF TRACT A OF SABAL GROVE UNIT ONE",
  "157V3CYN1HV7" => "A REPLAT OF LOT 12 TIMBERS WEST UNIT TWO",
  "157V3CYN1EQL" => "A REPLAT OF HIDDEN HARBOUR TRACT 1",
  "157V3CYN1BJ8" => "A REPLAT OF A PORTION OF SUNNY PINES SUBDIVISION N",
  "157V3CYN185I" => "A PLAT OF THE WAL-MART AT MELBOURNE",
  "157V3CYN14OO" => "A PLAT OF BEACH HOUSE ESTATES",
  "157V3CYN10WH" => "A PARTIAL REPLAT OF VIERA NORTH PUD PARCEL G-1 PHA",
  "157V3CYMZN6W" => "A PARTIAL REPLAT OF THE PLAT OF MEADOW CREEK",
  "157V3CYMXJA4" => "A A BERRYS",
  "157V3CYMXGQJ" => "845 EXECUTIVE LANE CONDO",
  "157V3CYMXDTM" => "836 EXECUTIVE LANE CONDO",
  "157V3CYMXB33" => "8010 COMMERCIAL CONDO",
  "157V3CYMX6WH" => "800 NORTH FISKE CONDO APTS",
  "157V3CYMX41T" => "5TH HOLE CONDO PH II",
  "157V3CYMX126" => "5TH HOLE CONDO PH I",
  "157V3CYMWWSV" => "5505 CENTER A CONDOMINIUM",
  "157V3CYMWS4L" => "3800 OCEAN BEACH BOULEVARD CONDO",
  "157V3CYMWJ6P" => "3700 COMMERCE CENTER CONDO",
  "157V3CYMWARM" => "3485 PLACE CONDO",
  "157V3CYMVZ74" => "3365 PLACE CONDO",
  "157V3CYMVQHV" => "333 BY THE SEA CONDO PH I",
  "157V3CYMVND3" => "3071 CONDO",
  "157V3CYMVKAG" => "307 CONDOMINIUM",
  "157V3CYMVDNW" => "3047 CONDO",
  "157V3CYMV7S7" => "2ND STREET NORTH TOWNHOMES CONDO",
  "157V3CYMTX3V" => "245 MAGNOLIA OFFICE CONDO",
  "157V3CYMTTWC" => "2200 WICKHAM BUSINESS PARK",
  "157V3CYMTMQE" => "2100 TOWERS CONDO",
  "157V3CYMTHUS" => "1601 APOLLO CONDO",
  "157V3CYMTEHK" => "115 RIVER DRIVE CONDO PH III",
  "157V3CYMTB7L" => "115 RIVER DRIVE CONDO PH II",
  "157V3CYMT72S" => "115 RIVER DRIVE CONDO PH I",
  "157V3CYMT40G" => "1037 PATHFINDER WAY CONDOMINIUM",
  "157V3CYMRX31" => "1 AC AS DES IN DB 74 PG 73",
  "157V3CYN4XUN" => "ALL E OF ST JOHNS RIVER EX N",
  "157V3CYN4UGT" => "ALL E OF ST JOHNS RIVER EX AS DES",
  "157V3CYN55SY" => "ALL E OF ST JOHNS RIVER EX",
  "157V3CYN4R5O" => "ALL E OF ST JOHNS RIVER AND N OF",
  "157V3CYN58XR" => "ALL E OF ST JOHNS RIVER",
  "157V3CYN4NX1" => "ALL E OF RIVER EX ORB 862 PG 159",
  "157V3CYN4KAS" => "ALL E OF RIVER EX E 1/2 OF SW 1/4",
  "157V3CYN4GXK" => "ALL E OF RIVER AND N OF GRANT",
  "157V3CYN4E17" => "ALL E OF LEVEE",
  "157V3CYN4ANW" => "ALL AS PER PB 2 PG 102",
  "157V3CYNKE19" => "ALL",
  "157V3CYN466F" => "ALHAMBRA VILLAS REPLAT OF PT OF",
  "157V3CYN42T4" => "ALHAMBRA VILLAGE SUBD",
  "157V3CYN3ZFX" => "ALAMANDA KEY PHASE I",
  "157V3CYN3W1V" => "ALAMANDA KEY PHASE 2",
  "157V3CYN3QTS" => "ALACHUA COURT APTS",
  "157V3CYN3NOO" => "AIRPORT CORPORATE CENTER",
  "157V3CYN3KEU" => "AGNES M KRAKER S/D AS DES IN DB 360 PG 159",
  "157V3CYN3H70" => "AEAEA",
  "157V3CYN3DW3" => "ADMIRALTY LAKES TOWNHOMES PHASE 1",
  "157V3CYN3AJZ" => "ADMIRALTY LAKES PATIO HOMES PHASE 3",
  "157V3CYN37H4" => "ADMIRALTY LAKES PATIO HOMES PHASE 2",
  "157V3CYN344W" => "ADMIRALTY LAKES LAKE PATIO HOMES 2ND REPLAT OF PHA",
  "157V3CYN30UF" => "ADAMSON CREEK PHASE ONE-A",
  "157V3CYN2XMZ" => "ADAMSON COUNTRY ESTATES",
  "157V3CYN2UD2" => "ADAMS VILLAS REPLAT",
  "157V3CYN2QWC" => "ADAMS VILLAS PHASE II",
  "157V3CYN2NMO" => "ADAMS TOWNHOMES",
  "157V3CYN2IEO" => "ADAMS SUBDIVISION",
  "157V3CYN2F2V" => "ACRES OF DIAMONDS",
  "157V3CYN28G0" => "ACKLEY SUBD NO 3",
  "157V3CYN24X1" => "ACKLEY SUBD NO 2",
  "157V3CYN2BV7" => "ACKLEY SUBD",
  "157V3CYN21AV" => "ABBOTT TOWNSHOMES A PARTIAL REPLAT OF",
  "157V3CYN1X1Z" => "ABBOTT TOWNHOUSES",
  "15B1YOI7UE8R" => "A1A CONDO PARK CONDO",
  "157V3CYN89DS" => "ALL EX ORB 2487 PG 2239",
  "157V3CYN86BU" => "ALL EX ORB 2477 PG 934",
  "157V3CYN82XT" => "ALL EX ORB 1936 PG 53",
  "157V3CYN7XGU" => "ALL EX ORB 1808 PG 345 1909 PG",
  "157V3CYN7U68" => "ALL EX ORB 1653 PG 876",
  "157V3CYN7RP1" => "ALL EX ORB 1126 PG 1022",
  "157V3CYN7OC0" => "ALL EX NW 1/4 OF SE 1/4 AND HWY",
  "157V3CYN7LAV" => "ALL EX NE 1/4 OF NW 1/4 OF NW 1/4",
  "157V3CYN7ILX" => "ALL EX NE 1/4 OF NW 1/4 OF NE 1/4",
  "157V3CYN7F7T" => "ALL EX N 50 FT ORB 892 PG 518",
  "157V3CYN7BYT" => "ALL EX N 3250 FT OF E 1/4 AND REC",
  "157V3CYN78UD" => "ALL EX N 111.1 AC AND REC.TAX DIST.",
  "157V3CYN75NM" => "ALL EX N 1/4 OF SW 1/4 OF SW 1/4",
  "157V3CYN72LG" => "ALL EX N 1/4 OF SE 1/4 OF SE 1/4",
  "157V3CYN6YNB" => "ALL EX N 1/4 OF SE 1/4 OF NE 1/4",
  "157V3CYN6VB9" => "ALL EX N 1/4 OF NE 1/4 OF SE 1/4",
  "157V3CYN6RT9" => "ALL EX N 1/2 OF S 1/2 OF SE 1/4",
  "157V3CYN6OMK" => "ALL EX N 1/2 OF NE 1/4 E 1/4 OF",
  "157V3CYN6L79" => "ALL EX MTDD CANAL R/W",
  "157V3CYN6HUO" => "ALL EX I-95 R/W",
  "157V3CYN6F59" => "ALL EX I-95 CO RD R/W AND EX E 1/2",
  "157V3CYN6BYG" => "ALL EX ELY 631 AC AS DES IN ORB",
  "157V3CYN68V0" => "ALL EX E 1/4 OF SE 1/4 OF NE 1/4",
  "157V3CYN65J0" => "ALL EX DB 63 PG 155 AND I-95 R/W",
  "157V3CYN62BH" => "ALL EX CIVIL BOOK 54 PG 1011",
  "157V3CYN5YXG" => "ALL EX CIVIL ACTION NO 54568 TO",
  "157V3CYN5VNT" => "ALL EX 600 AC AS DES IN ORB 1126",
  "157V3CYN5SAL" => "ALL EAST OF ST JOHNS RIVER",
  "157V3CYN5P6B" => "ALL EAST OF RIVER",
  "157V3CYN5LZ2" => "ALL EAST OF NEW LEVEE LINE AS DES",
  "157V3CYN5IJO" => "ALL EAST OF NEW LEVEE AS DES IN",
  "157V3CYN5C4B" => "ALL EAST OF LEVEE LINE AS DES IN",
  "157V3CYN5FAY" => "ALL EAST OF LEVEE",
  "157V3CYN52CC" => "ALL E OF ST JOHNS RIVER EX ORB",
  "157V3CYNBV1M" => "ALL EX SOUTH OF LEVEE LINE AS DES",
  "157V3CYNBRYV" => "ALL EX SE 1/4 E OF I-95 AND I-95",
  "157V3CYNBNJR" => "ALL EX SE 1/2 OF SE 1/4",
  "157V3CYNBJQH" => "ALL EX S 3603 FT AND ORB 799 PG 92",
  "157V3CYNBCS1" => "ALL EX S 1/4 OF SW 1/4 OF NW 1/4",
  "157V3CYNB921" => "ALL EX S 1/4 OF S 1/4 AND ORB 880",
  "157V3CYNBGFC" => "ALL EX S 1/4",
  "157V3CYNB61X" => "ALL EX S 1/2 OF SW 1/4 OF NW 1/4",
  "157V3CYNB23I" => "ALL EX S 1/2 OF N 1/2 OF NE 1/4",
  "157V3CYNAYX4" => "ALL EX RR R/W",
  "157V3CYNAS5M" => "ALL EX REC TAX DIST AND ORB 2787",
  "157V3CYNAVLY" => "ALL EX REC TAX DIST",
  "157V3CYNALRB" => "ALL EX RD R/W PB 24 PG 89 ORB",
  "157V3CYNAORX" => "ALL EX RD R/W",
  "157V3CYNAG7J" => "ALL EX RD AND DRAINAGE R/WS",
  "157V3CYNAD7P" => "ALL EX RD AND DITCH R/WS",
  "157V3CYNA9N9" => "ALL EX R/W",
  "157V3CYNA5YB" => "ALL EX PT OF NW 1/4 LYING N OF",
  "157V3CYNA24R" => "ALL EX PB 21 PG 105 AND PB 21 PG",
  "157V3CYN9YBU" => "ALL EX PB 2 PG 102 AND ORB 1126 PG",
  "157V3CYN9UIR" => "ALL EX PB 17 PG 6 AND PB 17 PG 22",
  "157V3CYN9QQ6" => "ALL EX PB 17 PG 22 AND PB 17 PG 34",
  "157V3CYN9MGP" => "ALL EX PB 16 PGS 9 19 29 68 AND PB",
  "157V3CYN9HHO" => "ALL EX PB 16 PGS 29 68 AND CANAL",
  "157V3CYN9DS1" => "ALL EX PB 15 PG 99 16 PG 9",
  "157V3CYN98IZ" => "ALL EX PART SWLY OF NEW LEVEE",
  "157V3CYN95C9" => "ALL EX ORB 971 PG 399",
  "157V3CYN925B" => "ALL EX ORB 942 PG 313 AND PB 2",
  "157V3CYN8VLM" => "ALL EX ORB 880 PG 739 AND SE 1/4 OF",
  "157V3CYN8Z6E" => "ALL EX ORB 880 PG 739",
  "157V3CYN8MSF" => "ALL EX ORB 713 PG 1048 PB 19 PG",
  "157V3CYN8QQ2" => "ALL EX ORB 713 PG 1048 AND PB 23 PG",
  "157V3CYN8JJT" => "ALL EX ORB 578 PG 546 667 PG 527",
  "157V3CYN8G8N" => "ALL EX ORB 503 PG 513",
  "157V3CYN8BYS" => "ALL EX ORB 335 PG 66 W 70 FT",
  "157V3CYNF00L" => "ALL L CRAYON GRANT",
  "157V3CYNEU10" => "ALL FRACTIONAL SECTION",
  "157V3CYNEQGH" => "ALL FRACTIONAL PAR 500",
  "157V3CYNELTV" => "ALL FRACTIONAL N OF GRANT LINE EX ST",
  "157V3CYNEIGG" => "ALL FRACTIONAL N AND E OF RIVER",
  "157V3CYNEG0C" => "ALL FRACTIONAL EX SW 1/4 OF",
  "157V3CYNECT5" => "ALL FRACTIONAL EX RR R/W",
  "157V3CYNE9WL" => "ALL FRACTIONAL EX REC TAX DIST",
  "157V3CYNE7B0" => "ALL FRACTIONAL EX ORB 942 PG 313",
  "157V3CYNE483" => "ALL FRACTIONAL EX N 200 FT OF E",
  "157V3CYNE14H" => "ALL FRACTIONAL EX LOT 1",
  "157V3CYNDYCH" => "ALL FRACTIONAL EX E OF LEVEE",
  "157V3CYNDU3M" => "ALL FRACTIONAL E OF ST JOHNS RIVER AND N",
  "157V3CYNDN2W" => "ALL FRACTIONAL E OF RIVER EX RD",
  "157V3CYNDQKJ" => "ALL FRACTIONAL E OF RIVER",
  "157V3CYNDJVF" => "ALL FRACTIONAL AS DES IN ORB 618",
  "157V3CYNDH8K" => "ALL FRACTIONAL 5 AND ALL FRACTIONAL SEC 6",
  "157V3CYNEXDM" => "ALL FRACTIONAL",
  "157V3CYNDDQ2" => "ALL EXCEPT SE 1/4 OF SE 1/4 OF",
  "157V3CYND7A4" => "ALL EXC W 1/4 REC TAX DIST AND RD",
  "157V3CYND2D0" => "ALL EXC W 1/2 OF NW 1/4 S 1/2",
  "157V3CYNCZS0" => "ALL EXC SW 1/4",
  "157V3CYNCWGM" => "ALL EXC N 3250 FT OF W 1/2 LYING",
  "157V3CYNCSWN" => "ALL EXC E 1/2 OF NE 1/4 REC TAX",
  "157V3CYNDAIF" => "ALL EXC",
  "157V3CYNCPBK" => "ALL EX WEST OF NEW LEVEE AS DES",
  "157V3CYNCL67" => "ALL EX W 1/2 OF SW 1/4 OF SW 1/4",
  "157V3CYNCIMQ" => "ALL EX W 1/2 OF SE 1/4 OF NE 1/4",
  "157V3CYNCFGX" => "ALL EX W 1/2 OF NE 1/4 OF SW 1/4",
  "157V3CYNCBRV" => "ALL EX W 1/2 OF E 1/2 OF NW 1/4",
  "157V3CYNC83J" => "ALL EX UNION CYPRESS RR R/W",
  "157V3CYNC4IC" => "ALL EX THAT PT E OF EXISTING",
  "157V3CYNC1O9" => "ALL EX SW OF NEW LEVEE LINE AS",
  "157V3CYNBYIJ" => "ALL EX SW 1/4 OF SW 1/4 OF SE 1/4",
  "157V3CYNI056" => "ALL PLAT OF GEORGE W HOPKINS",
  "157V3CYNHWWV" => "ALL PAR 250 500 750",
  "157V3CYNHU5B" => "ALL OFFSHORE ISLANDS",
  "157V3CYNHQOA" => "ALL OF W 1/2 N AND E OF ST JOHNS",
  "157V3CYNHNEU" => "ALL OF SECTION EX LANDS DEEDED",
  "157V3CYNHKW5" => "ALL OF SEC N OF CANAL EX PT OF",
  "157V3CYNHHLB" => "ALL OF SEC E OF I-95 R/W EX",
  "157V3CYNHE72" => "ALL OF LOT 1 EX DB 418 PG 510",
  "157V3CYNHB1S" => "ALL OF GOVT LOT 7 LYING W OF S",
  "157V3CYNH6U7" => "ALL OF FRACTIONAL SEC EX ST HWY",
  "157V3CYNH42G" => "ALL NELY OF NEW LEVEE LINE AS DES",
  "157V3CYNH0PF" => "ALL NE OF RIVER EX CIVIL BOOK 54",
  "157V3CYNGX91" => "ALL N OF ST RD 528 AND W OF I-95 EX",
  "157V3CYNGUGU" => "ALL N OF ST RD 407 AND E OF ORB 862",
  "157V3CYNGR7T" => "ALL N OF GRANT LINE EX SR 524 R/W",
  "157V3CYNGO65" => "ALL N OF GRANT LINE EX ORB 1012",
  "157V3CYNGLEG" => "ALL N OF GRANT LINE EX I-95 R/W",
  "157V3CYNGHUF" => "ALL N OF GRANT LINE EX DRAINAGE",
  "157V3CYNGF6Q" => "ALL N OF GRANT LINE EX DB 394 PG",
  "157V3CYNG8Z8" => "ALL N OF DELESPINE GRANT PAR 250",
  "157V3CYNG677" => "ALL N OF DELESPINE GRANT EX CANAL",
  "157V3CYNGC5Q" => "ALL N OF DELESPINE GRANT",
  "157V3CYNG3QN" => "ALL N AND W OF DELESPINE GRANT",
  "157V3CYNG0CJ" => "ALL N AND S OF GRANT LINE EXC LANDS",
  "157V3CYNFX87" => "ALL N AND E OF NEW LEVEE LINE AS",
  "157V3CYNFUHR" => "ALL N AND E OF LEVEE",
  "157V3CYNFREB" => "ALL MC INTOSH GRANT EX LANDS",
  "157V3CYNFNYW" => "ALL LYING WEST OF NEW LEVEE AS",
  "157V3CYNFKSK" => "ALL LYING N OF THE N LINE OF",
  "157V3CYNFH1G" => "ALL LYING E OF LEVEE",
  "157V3CYNFAWV" => "ALL LOTS 1 2 3",
  "157V3CYNFEIV" => "ALL LOTS 1 2",
  "157V3CYNF7K8" => "ALL LOT 1 W OF HWY BETWEEN COCOA",
  "157V3CYNF33V" => "ALL LAND LYING E OF US 1 HWY R/W",
  "157V3CYNKW59" => "ALLEN ET AL 21 1/2 AC TR PART OF",
  "157V3CYNKSRK" => "ALLEN CONDO APTS",
  "157V3CYNKPRL" => "ALLEN AND JEROMES SUBD OF 280 AC TR GRANT SECS 12",
  "157V3CYNKMRV" => "ALLANS SUBD",
  "157V3CYNKKDU" => "ALL-SHORES TOWNHOMES PHASE II A REPLAT OF A PORTIO",
  "157V3CYNKH8N" => "ALL-SHORES TOWNHOMES A REPLAT OF A PORTION OF BLOC",
  "157V3CYNKBHR" => "ALL WEST OF LEVEE",
  "157V3CYNK818" => "ALL W OF LEVEE",
  "157V3CYNK4VJ" => "ALL W OF I-95 R/W",
  "157V3CYNK1Z2" => "ALL THAT PART OF TWP 28 RGE 35",
  "157V3CYNJYSO" => "ALL THAT PART OF SECTION 35 AND W 1/2 OF",
  "157V3CYNJVW0" => "ALL THAT PART OF SECTION 34 NOT",
  "157V3CYNJTB6" => "ALL THAT PART OF SECTION 31 NOT INCL IN",
  "157V3CYNJQ7R" => "ALL THAT PART OF LOT 1 S AND W OF",
  "157V3CYNJMVX" => "ALL THAT PART OF E 1/2 OF SECTION 36",
  "157V3CYNJGVP" => "ALL SECTION PAR 500",
  "157V3CYNJDTC" => "ALL SECTION N AND E OF ST JOHNS",
  "157V3CYNJB2L" => "ALL SECTION LYING W OF LEVEE",
  "157V3CYNJ828" => "ALL SECTION LYING S OF LEVEE",
  "157V3CYNJ4VD" => "ALL SECTION LYING NORTH OF CANAL EX",
  "157V3CYNJ26C" => "ALL SECTION LYING E OF I-95 R/W",
  "157V3CYNIYU9" => "ALL SECTION EX PB 13 PG 15 14 PG 136",
  "157V3CYNIVVU" => "ALL SECTION EX ORB 687 PG 827 AND STATE",
  "157V3CYNISY5" => "ALL SECTION EX ORB 444 PG 308 775 PG",
  "157V3CYNIPJP" => "ALL SECTION EX CIVIL BOOK 54 PG 838",
  "157V3CYNIMC2" => "ALL SECTION AS DESC IN ORB 1068",
  "157V3CYNIJRM" => "ALL SECTION AND ALL FRACTIONAL",
  "157V3CYNIGDT" => "ALL SECTION 32 AS DESC IN ORB 1068",
  "157V3CYNICPQ" => "ALL SECTION 1 AND ALSO ALL SECTION SEC 2-24-37",
  "157V3CYNI9DF" => "ALL SECTION 1 12 13 14 23 24 25 26",
  "157V3CYNJK3N" => "ALL SECTION",
  "157V3CYNI63C" => "ALL S OF STATE RD 520 EX",
  "157V3CYNI3G0" => "ALL S OF N TOE OF LEVEE",
  "157V3CYNO61M" => "ANGEL FALLS VILLAS CONDO",
  "157V3CYNO2ZS" => "ANGEL CITY",
  "157V3CYNO0H3" => "ANDY ESTATES",
  "157V3CYNNWAD" => "ANCHORAGE CONDO",
  "157V3CYNNT0B" => "AMONIATE PRODUCTS CORP SUBD",
  "157V3CYNNPV3" => "AMHERST GARDENS SEC 5 UNIT 2",
  "157V3CYNNMNN" => "AMHERST GARDENS SEC 5 UNIT 1",
  "157V3CYNNJQ8" => "AMHERST GARDENS SEC 4",
  "157V3CYNNGJH" => "AMHERST GARDENS SEC 3",
  "157V3CYNND6V" => "AMHERST GARDENS SEC 2",
  "157V3CYNN9WM" => "AMHERST GARDENS SEC 1",
  "157V3CYNN7D6" => "AMERICAN VILLAGE PUD STAGE 8",
  "157V3CYNN3T7" => "AMERICAN VILLAGE PUD STAGE 7",
  "157V3CYNN0CG" => "AMERICAN VILLAGE PUD STAGE 6",
  "157V3CYNMXHG" => "AMERICAN VILLAGE PUD STAGE 5",
  "157V3CYNMU7O" => "AMERICAN VILLAGE PUD STAGE 4",
  "157V3CYNMQX7" => "AMERICAN VILLAGE PUD STAGE 3",
  "157V3CYNMNXN" => "AMERICAN VILLAGE PUD STAGE 2",
  "157V3CYNMKG9" => "AMERICAN VILLAGE PUD STAGE 1",
  "157V3CYNMHYO" => "AMERI-CANA RESORTS CO-OP",
  "157V3CYNMEKG" => "AMBERWOOD AT BAYSIDE LAKES",
  "157V3CYNMB1R" => "AMBASSADOR SHORES CONDO",
  "157V3CYNM8J9" => "ALTON THOMAS ACRES SUBD",
  "157V3CYNM537" => "ALSO ALL SECS 11 14 15",
  "157V3CYNM1ME" => "ALPINECREST SUBD",
  "157V3CYNLUA4" => "ALMAR SUBD SEC B 1ST ADDN",
  "157V3CYNLY2Q" => "ALMAR SUBD SEC B",
  "157V3CYNLPUN" => "ALMAR SUBD SEC A",
  "157V3CYNLM0Z" => "ALMAR CONDOMINIUM",
  "157V3CYNLH1T" => "ALLENS ET AL 500 AC TRACT IN SECS 10 11 12 OF FLEM",
  "157V3CYNLDE4" => "ALLEN ET AL SUBD OF S 136 ACRE TRACT GRANT SECS",
  "157V3CYNL93Q" => "ALLEN ET AL SUBD OF 230 AC TRACT GRANT SECS 11 19",
  "157V3CYNL660" => "ALLEN ET AL SUBD OF 21 1/2 AC TR",
  "157V3CYNL2RX" => "ALLEN ET AL 500 ACRE TRACT SECS 10 11 12 19",
  "157V3CYNKZGE" => "ALLEN ET AL 230 ACRE TRACT GRANT SECS 11 19 22",
  "157V3CYNR4YE" => "ASHWOOD LAKES PHASE THREE",
  "157V3CYNR1UA" => "ASHWOOD LAKES PHASE ONE",
  "157V3CYNQYJI" => "ASHWOOD LAKES PHASE FOUR",
  "157V3CYNQVEW" => "ASHWOOD LAKES PHASE FIVE",
  "157V3CYNQSG1" => "ASHTON - VIERA NORTH PUD TRACT B-1",
  "1AIYB3P30WGF" => "ASHLEY OAKS",
  "157V3CYNQPDD" => "ASHLEY MANOR",
  "157V3CYNQM1V" => "ASHLEY ESTATES",
  "157V3CYNQHBJ" => "ASHFORD HARBOUR",
  "157V3CYNQE3U" => "ARUNDEL - BAYTREE P.U.D. PHASE 2 STAGE 2",
  "157V3CYNQAXB" => "ARTHUR F CREEDMANS PLAT OF PT OF LOT 1 OF HOUSTONS",
  "157V3CYNQ7TP" => "ARTESIA TOWNHOMES CONDO",
  "157V3CYNQ51J" => "ARTESIA CONDO",
  "17YKB3DI34R2" => "ARRIVAS VILLAGE",
  "157V3CYNPYJ8" => "ARNOLDS PLAT OF MALABAR AS DES IN UNREC RESUBD OF",
  "157V3CYNQ1TE" => "ARNOLDS PLAT OF MALABAR",
  "157V3CYNPU6A" => "ARMSTRONGS RESUBD OF LOT 14 OF JOHNSON AND RHODES",
  "157V3CYNPR6E" => "ARLINGTON PINES TOWNHOUSES AS DESC IN ORB 3489 PG",
  "157V3CYNPO4M" => "ARIC BY THE SEA",
  "157V3CYNPLJ5" => "ARENS WILD ACRES SUBD",
  "157V3CYNPIDZ" => "ARBOR WOODS",
  "157V3CYNPFE3" => "ARBOR RIDGE - SECTION TWO",
  "157V3CYNPC4Y" => "ARBOR RIDGE - SECTION ONE",
  "157V3CYNP8P4" => "AQUARINA P.U.D. STAGE 4 TRACTS VI AND VII",
  "157V3CYNP5K7" => "AQUARINA P.U.D. STAGE 2",
  "157V3CYNOZNN" => "AQUARINA P.U.D. STAGE 1 TRACTS C AND D STAGE 2",
  "157V3CYNP2TS" => "AQUARINA P.U.D. STAGE 1",
  "157V3CYNOWE8" => "AQUARINA II P.U.D. STAGE 3 TRACT IV",
  "157V3CYNOSRQ" => "AQUARINA II P.U.D. STAGE 1 STAGE 2 AND STAGE 3 T",
  "157V3CYNOORE" => "AQUA SEA CONDO APTS",
  "157V3CYNOMDQ" => "ANSON SUBDS",
  "157V3CYNOCEK" => "ANNIE LAURIE GARDENS REPLAT OF",
  "157V3CYNOFCJ" => "ANNIE LAURIE GARDENS PLAT NO 1",
  "157V3CYNOIXO" => "ANNIE LAURIE GARDENS",
  "157V3CYNO96T" => "ANGELS ISLE",
  "157V3CYNU917" => "AVON TERRACE CONDO",
  "157V3CYNU5MP" => "AVON COURT SUBD",
  "157V3CYNTZGC" => "AVON BY THE SEA REPLAT OF BLKS 104 AND 105",
  "157V3CYNTW9Q" => "AVON BY THE SEA REPLAT OF BLK 93",
  "157V3CYNU297" => "AVON BY THE SEA",
  "157V3CYNTT09" => "AVERILL FARMS SUBD",
  "157V3CYNTQ7A" => "AVENUE VIERA",
  "157V3CYNTN0H" => "AVALON",
  "157V3CYNTK2E" => "AUTUMN WOODS",
  "157V3CYNTHHG" => "AUTUMN PINES",
  "157V3CYNTE0C" => "AUSTINS SUBD OF LOT S OF GRAYS SUBD",
  "157V3CYNTAZO" => "AURORA WOODS",
  "157V3CYNT8AM" => "AURORA VILLAGE CONDO",
  "157V3CYNT57K" => "AURORA OAKS",
  "157V3CYNT28Y" => "AURORA HEIGHTS SUBD",
  "157V3CYNSZR4" => "AURANTIA",
  "157V3CYNSWJQ" => "AUGUSTA VILLAGE REPLAT OF",
  "157V3CYNSTA2" => "AUGUSTA TRACE AT TURTLE CREEK",
  "157V3CYNSNAH" => "AUBURN LAKES SUBDIVISION PHASE THREE VIERA NORTH P",
  "157V3CYNSK5V" => "AUBURN LAKES SUBDIVISION PHASE ONE VIERA NORTH P.",
  "157V3CYNSEPA" => "AUBURN LAKES SUBDIVISION PHASE FOUR VIERA NORTH P.",
  "157V3CYNSQL2" => "AUBURN LAKES SUBDIVISION",
  "157V3CYNSBLB" => "ATWATERS SUBDIVISION",
  "157V3CYNS93Z" => "ATLANTIS SUBDIVISION",
  "157V3CYNS5R0" => "ATLANTIS PROFESSIONAL CONDO",
  "157V3CYNS2MS" => "ATLANTIQUE CONDO APTS",
  "157V3CYNS074" => "ATLANTICA CONDOMINIUM",
  "157V3CYNRX5J" => "ATLANTIC SHORES SUBD",
  "157V3CYNRU20" => "ATLANTIC GARDENS RESUBD",
  "157V3CYNRRAA" => "ATLANTIC GARDENS CONDO",
  "157V3CYNRO0T" => "ATLANTIC COAST REALTY CO SUBD",
  "157V3CYNRKQ9" => "ATKINSONS SUB REPLAT OF LOTS 9 10",
  "157V3CYNRHE7" => "ATKINSONS PLAT NO 2",
  "157V3CYNRDAE" => "ATKINSON SUBD",
  "157V3CYNRAN0" => "ASPINWALL",
  "157V3CYNR81N" => "ASHWOOD LAKES PHASE TWO",
  "157V3CYNXH89" => "BANANA RIVER DRIVE SUBD",
  "157V3CYNXDN6" => "BANANA BAY CONDO PH XXIV",
  "157V3CYNX9Z8" => "BANANA BAY CONDO PH XXIII",
  "157V3CYNX775" => "BANANA BAY CONDO PH XXII",
  "157V3CYNX42J" => "BANANA BAY CONDO PH XXI",
  "157V3CYNX14F" => "BANANA BAY CONDO PH XX",
  "157V3CYNWYDM" => "BANANA BAY CONDO PH XVIII",
  "157V3CYNWUY1" => "BANANA BAY CONDO PH XVII",
  "157V3CYNWRS6" => "BANANA BAY CONDO PH XVI",
  "157V3CYNWOYZ" => "BANANA BAY CONDO PH XV",
  "157V3CYNWM3X" => "BANANA BAY CONDO PH XIX",
  "157V3CYNWIR9" => "BANANA BAY CONDO PH XIV",
  "157V3CYNWFJE" => "BANANA BAY CONDO PH XIII",
  "157V3CYNWCCB" => "BANANA BAY CONDO PH XII",
  "157V3CYNW95A" => "BANANA BAY CONDO PH XI",
  "157V3CYNW5LP" => "BANANA BAY CONDO PH X",
  "157V3CYNW2GH" => "BANANA BAY CONDO PH VIII",
  "157V3CYNVZRC" => "BANANA BAY CONDO PH IX",
  "157V3CYNVW86" => "BANANA BAY CONDO PH IV V VI VII",
  "157V3CYNVT1O" => "BANANA BAY CONDO PH III",
  "157V3CYNVQ5D" => "BANANA BAY CONDO PH II",
  "157V3CYNVN06" => "BANANA BAY CONDO PH I",
  "157V3CYNVJYZ" => "BALMORAL BAYTREE PLANNED UNIT DEVELOPMENT PHASE 3",
  "157V3CYNVHD3" => "BAKERSFIELD",
  "157V3CYNVE5H" => "BAKERS UNIT 2",
  "157V3CYNVAJC" => "BAKERS UNIT 1 RESUBD OF",
  "157V3CYNV1RS" => "BAHNSENS 1ST ADDN SUBD",
  "157V3CYNUZDN" => "B J R ACRES",
  "157V3CYNUW7R" => "B F TRIMBLE SUBD",
  "157V3CYNUSZM" => "B C SNYDERS FORREST PK ALLOTMENT TO COCOA",
  "157V3CYNUQ07" => "B B NELSON COMMERCIAL PLAT",
  "157V3CYNUN27" => "AZALEA PLACE CONDO PHASE II",
  "157V3CYNUKAM" => "AZALEA PLACE CONDO PHASE I",
  "157V3CYNUHEL" => "AZALEA PARK ESTATES UNIT NO 1",
  "157V3CYNUEAX" => "AZALEA PARK DEV CORP",
  "157V3CYNUBC3" => "AZALEA PALMS CONDO",
  "157V3CYO0LX9" => "BARTONS PLAT OF ROCKLEDGE RESUBD OF LOTS 2 AND 4",
  "157V3CYO0IJF" => "BARTON PARK MANOR UNIT 2",
  "157V3CYO0FD6" => "BARTON PARK MANOR UNIT 1",
  "157V3CYO0CHR" => "BARRS SUBDIVISION",
  "157V3CYO097A" => "BARRINGTON PHASE TWO",
  "157V3CYO05WO" => "BARRINGTON PHASE ONE",
  "157V3CYO02ZX" => "BARRINGTON BLUFFS A CONDOMINIUM",
  "157V3CYNZZHU" => "BARONY ESTATES UNIT 2 SEC 2",
  "157V3CYNZVWV" => "BARONY ESTATES UNIT 2 SEC 1",
  "157V3CYNZSB4" => "BARONY ESTATES UNIT 1",
  "157V3CYNZOF8" => "BARNES AND HAYNES RESUBDIVISION",
  "157V3CYNZKTE" => "BARNACLE BAY",
  "157V3CYNZHIX" => "BARNA PLACE",
  "157V3CYNZEH3" => "BARMOR ALLIANCE CONDOMINIUM",
  "157V3CYNZB28" => "BARLOWS SUBD",
  "157V3CYNZ7MS" => "BAREFOOT BAY UNIT 2 PART 13",
  "157V3CYNZ4BY" => "BAREFOOT BAY UNIT 2 PART 12",
  "157V3CYNZ0SD" => "BAREFOOT BAY UNIT 2 PART 11",
  "157V3CYNYU9F" => "BAREFOOT BAY UNIT 2 PART 10 REPLAT OF PARK AREAS",
  "157V3CYNYXQH" => "BAREFOOT BAY UNIT 2 PART 10",
  "157V3CYNYR4Y" => "BAREFOOT BAY UNIT 1",
  "157V3CYNYO20" => "BAREFOOT BAY MOBILE HOME SUBD MICCO FLORIDA UNIT O",
  "157V3CYNYKZ6" => "BARCLAY SQUARE CONDOS",
  "157V3CYNYHT8" => "BARBIZON CONDO",
  "157V3CYNYBZX" => "BAR HARBOR COLONY REPLAT",
  "157V3CYNYF5R" => "BAR HARBOR COLONY",
  "157V3CYNY92Q" => "BAR C RANCHETTES",
  "157V3CYNY2SF" => "BANYAN COVE PHASES ONE TWO AND THREE",
  "157V3CYNY6E0" => "BANYAN COVE",
  "157V3CYNXZSJ" => "BANKERS RESERVE PROP INC 1ST ADDN TO MICCO SUBD OF",
  "157V3CYNXXBD" => "BANANA RIVER PARK",
  "157V3CYNXU5R" => "BANANA RIVER HGTS",
  "157V3CYNXNW0" => "BANANA RIVER ESTATES REPLAT OF PT OF",
  "157V3CYNXKQ9" => "BANANA RIVER ESTATES REPLAT OF PORTION OF",
  "157V3CYNXQTI" => "BANANA RIVER ESTATES",
  "157V3CYO46Y0" => "BAYTREE P.U.D. PHASE 2 STAGE 1A THE HAMLET",
  "157V3CYO4301" => "BAYTREE P.U.D. PHASE 1 STAGE 1-5",
  "157V3CYO3Z3G" => "BAYTREE CORPORATE PARK",
  "157V3CYO3UQR" => "BAYSIDE SUBD",
  "157V3CYO3QCF" => "BAYSIDE LAKES OFFICE PARK",
  "157V3CYO3MJ9" => "BAYSIDE LAKES OFFICE CENTER",
  "157V3CYO3IGV" => "BAYSIDE LAKES COMMERCIAL CENTER PHASE I",
  "157V3CYO3EH6" => "BAYSIDE LAKES COMMERCIAL CENTER PHASE 4",
  "157V3CYO3ANF" => "BAYSIDE LAKES COMMERCIAL CENTER PHASE 3",
  "157V3CYO36WF" => "BAYSIDE LAKES COMMERCIAL CENTER PHASE 2",
  "157V3CYO3325" => "BAYSIDE CONDO PH IV",
  "157V3CYO2ZOV" => "BAYSIDE CONDO PH III",
  "157V3CYO2VJ6" => "BAYSIDE CONDO PH II",
  "157V3CYO2RJZ" => "BAYSIDE CONDO PH I",
  "157V3CYO2MYV" => "BAYPORT CONDO PH VI",
  "157V3CYO2J21" => "BAYPORT CONDO PH V",
  "157V3CYO2F9C" => "BAYPORT CONDO PH IV",
  "157V3CYO2CES" => "BAYPORT CONDO PH III",
  "157V3CYO289R" => "BAYPORT CONDO PH II",
  "157V3CYO24PO" => "BAYPORT CONDO PH I",
  "157V3CYO1Y92" => "BAYMEADOWS OF MELBOURNE PHASE TWO A REPLAT OF TRA",
  "157V3CYO1VET" => "BAYMEADOWS OF MELBOURNE PHASE ONE",
  "157V3CYO21E3" => "BAYMEADOWS",
  "157V3CYO1RU4" => "BAYHILL AT VIERA EAST PHASE TWO",
  "157V3CYO1OES" => "BAYHILL AT VIERA EAST PHASE THREE",
  "157V3CYO1LRB" => "BAYHILL AT VIERA EAST PHASE ONE",
  "157V3CYO1II1" => "BAYBERRY ESTATES UNIT 2",
  "157V3CYO1FLK" => "BAYBERRY ESTATES UNIT 1",
  "157V3CYO1CWY" => "BAY VILLAGE",
  "157V3CYO199Q" => "BAY TOWERS CONDOMINIUM",
  "157V3CYO15OV" => "BAY PROFESSIONAL BUILDING CONDO",
  "157V3CYO12BB" => "BAY POINTE CONDO",
  "157V3CYO0YOF" => "BAY MARE FARMS",
  "157V3CYO0VMO" => "BAY CREST VILLA",
  "157V3CYO0RIH" => "BAY CLUB CONDO",
  "157V3CYO0OIY" => "BAUGHER CONDO",
  "157V3CYO7WSE" => "BEACH WOODS STAGE 4",
  "157V3CYO7TD3" => "BEACH WOODS STAGE 3 PHASE 3",
  "157V3CYO7Q29" => "BEACH WOODS STAGE 3 PHASE 2",
  "157V3CYO7JXP" => "BEACH WOODS STAGE 3 PHASE 1 REPLAT",
  "157V3CYO7NET" => "BEACH WOODS STAGE 3 PHASE 1",
  "157V3CYO7GLB" => "BEACH WOODS STAGE 2 PHASE 2",
  "157V3CYO7DDT" => "BEACH WOODS STAGE 2 PHASE 1",
  "157V3CYO7A79" => "BEACH WOODS STAGE 1 PHASE 1",
  "157V3CYO76HA" => "BEACH WINDS CONDO PH III",
  "157V3CYO729V" => "BEACH WINDS CONDO PH II",
  "157V3CYO6XRT" => "BEACH WINDS CONDO PH I",
  "157V3CYO6URR" => "BEACH VILLAS CONDO",
  "157V3CYO6RZE" => "BEACH VIEW CONDO",
  "157V3CYO6P7M" => "BEACH TOWNHOUSES",
  "157V3CYO6LOQ" => "BEACH PROFESSIONAL CONDO",
  "157V3CYO6F87" => "BEACH PARK VILLAGE FIRST ADDITION TO",
  "157V3CYO6BNG" => "BEACH PARK VILLAGE A REPLAT OF A PORTION OF",
  "157V3CYO6IAW" => "BEACH PARK VILLAGE",
  "157V3CYO68UV" => "BEACH ISLAND RESORT TIME SHARE",
  "157V3CYO65FM" => "BEACH HAVEN CONDO",
  "157V3CYO61XQ" => "BEACH ESTATES",
  "157V3CYO5X1S" => "BEACH CLUB CONDOMINIUM",
  "157V3CYO5TF5" => "BEACH CLUB CONDO",
  "157V3CYO5QEC" => "BEACH CHATEAU A CONDO",
  "157V3CYO5IZF" => "BEACH BREEZE",
  "157V3CYO5F26" => "BAYVUE CONDOMINIUMS OF COCOA BEACH PH III",
  "157V3CYO5BEF" => "BAYVUE CONDOMINIUMS OF COCOA BEACH PH II",
  "157V3CYO57LR" => "BAYVUE CONDOMINIUMS OF COCOA BEACH PH I",
  "157V3CYO50AE" => "BAYVIEW PROFESSIONAL CENTER CONDO",
  "157V3CYO4W2X" => "BAYVIEW PARK SEC 2",
  "157V3CYO4SIC" => "BAYVIEW PARK REPLAT",
  "157V3CYO4N6D" => "BAYVIEW OF TITUSVILLE CONDO APTS",
  "157V3CYO4JH2" => "BAYVIEW MANOR",
  "157V3CYO4FAD" => "BAYVIEW ESTATES",
  "157V3CYO5437" => "BAYVIEW",
  "157V3CYO4B8T" => "BAYTREE PROFESSIONAL CENTER",
  "157V3CYOAW5M" => "BEL AIRE II",
  "157V3CYOATO8" => "BEDFORD OFFICE CENTER CONDO",
  "157V3CYOAQES" => "BECKERS SUBDIVISION",
  "157V3CYOANFL" => "BECKERS RESUBDIVISION",
  "157V3CYOABZ0" => "BEAUJEANS REPLAT OF BLKS 1 3 PLAT OF MELBOURNE",
  "158ZUVUZ8LZM" => "BEAUJEANS PLAT OF MELBOURNE BEACH REPLAT OF LOTS 1",
  "157V3CYOAHEA" => "BEAUJEANS PLAT OF MELBOURNE BEACH REPLAT OF LOTS 1",
  "157V3CYOAEEX" => "BEAUJEANS PLAT OF MELBOURNE BEACH REPLAT OF BLKS 1",
  "157V3CYOAKYV" => "BEAUJEANS PLAT OF MELBOURNE BEACH",
  "157V3CYOA8WH" => "BEAR CREEK",
  "157V3CYOA34S" => "BEACON HILLS SUBD 2ND ADDN TO",
  "157V3CYOA0JE" => "BEACON HILLS SUBD 1ST ADDN",
  "157V3CYOA5TW" => "BEACON HILLS SUBD",
  "157V3CYO9XG4" => "BEACHWALK CONDO PH IV",
  "157V3CYO9UTN" => "BEACHWALK CONDO PH III",
  "157V3CYO9RK0" => "BEACHWALK CONDO PH II",
  "157V3CYO9OEE" => "BEACHWALK CONDO PH I",
  "157V3CYO9LNX" => "BEACHSIDE VILLAS",
  "157V3CYO9ED0" => "BEACHSIDE TOWNHOMES SECOND ADDITION",
  "157V3CYO9AV2" => "BEACHSIDE TOWNHOMES FIRST ADDITION",
  "157V3CYO9I47" => "BEACHSIDE TOWNHOMES",
  "157V3CYO980D" => "BEACHCOURT SOUTH CONDO",
  "157V3CYO94ZA" => "BEACHCOMBER CONDO OF MELB",
  "157V3CYO91BZ" => "BEACH WOODS STAGE 9 PHASE 1",
  "157V3CYO8XZK" => "BEACH WOODS STAGE 8",
  "157V3CYO8UAK" => "BEACH WOODS STAGE 7 PHASE 4",
  "157V3CYO8N6V" => "BEACH WOODS STAGE 7 PHASE 3-B REPLAT OF A PORTION",
  "157V3CYO8QV5" => "BEACH WOODS STAGE 7 PHASE 3-B",
  "157V3CYO8JMO" => "BEACH WOODS STAGE 7 PHASE 3-A",
  "157V3CYO8DMI" => "BEACH WOODS STAGE 7 PHASE 2 REPLAT OF A PORTION OF",
  "157V3CYO8GHP" => "BEACH WOODS STAGE 7 PHASE 2",
  "157V3CYO89Z2" => "BEACH WOODS STAGE 7 PHASE 1",
  "157V3CYO86JE" => "BEACH WOODS STAGE 6",
  "157V3CYO82WJ" => "BEACH WOODS STAGE 5 PHASE 2",
  "157V3CYO7ZG8" => "BEACH WOODS STAGE 5 PHASE 1",
  "157V3CYOEO3X" => "BERKELEY PLAZA NORTH",
  "157V3CYOERDK" => "BERKELEY PLAZA",
  "157V3CYOEHWC" => "BER JAN GARDENS PART OF",
  "157V3CYOELLC" => "BER JAN GARDENS",
  "157V3CYOEBGI" => "BENT OAK AT MEADOWRIDGE PHASE III",
  "157V3CYOE4P3" => "BENT OAK AT MEADOWRIDGE PHASE II",
  "157V3CYOEELH" => "BENT OAK AT MEADOWRIDGE",
  "157V3CYOE14V" => "BENNINGTON PHASE 2 PLAT OF VIERA NORTH PUD TRACT E",
  "157V3CYODXDD" => "BENNINGTON PHASE 1 PLAT OF VIERA NORTH P.U.D. TRAC",
  "157V3CYODTQJ" => "BENNETT STORAGE CONDO",
  "157V3CYODQ5I" => "BENCHMARK INDUSTRIAL CONDO",
  "157V3CYODEXJ" => "BELMAR PARK SUBD",
  "157V3CYODB5K" => "BELLO RIO CONDO",
  "157V3CYOD5IU" => "BELLEWOOD PLAT NO 2 DEL GRANT",
  "157V3CYOD81Y" => "BELLEWOOD PLAT NO 2",
  "157V3CYOD2CB" => "BELLEWOOD ESTATES RESUBD OF",
  "157V3CYOCZ9G" => "BELLEWOOD ESTATE SHEET NO 5",
  "157V3CYOCWLW" => "BELLE TERRACE",
  "157V3CYOCTEO" => "BELLA VITA P.U.D.",
  "157V3CYOCQ0J" => "BELLA VITA CONDO PH I",
  "157V3CYOCB42" => "BELLA VISTA OCEAN CLUB CONDO",
  "157V3CYOC7Q6" => "BELLA VISTA LA CITA PHASE 1",
  "157V3CYOC4LS" => "BELLA VISTA CONDO PHASES VIII AND IX",
  "157V3CYOC1LQ" => "BELLA VISTA CONDO PHASES V VI VII",
  "157V3CYOBY6G" => "BELLA VISTA CONDO PHASES II III AND IV",
  "157V3CYOBV60" => "BELLA VISTA CONDO PHASE I",
  "157V3CYOBSMO" => "BELLA VISTA AT LA CITA PHASE 2",
  "157V3CYOBP1D" => "BELLA LUNA",
  "157V3CYOBLXO" => "BELLA CAPRI",
  "157V3CYOBJDY" => "BELAIRE UNIT NO 3",
  "157V3CYOBFXE" => "BELAIRE UNIT NO 2",
  "157V3CYOBD1L" => "BELAIRE SUBD",
  "157V3CYOBAI5" => "BELAIRE ESTS SEC 1",
  "157V3CYOB721" => "BEL VISTA",
  "157V3CYOB3QV" => "BEL AIRE PALMS",
  "157V3CYOB071" => "BEL AIRE OFFICE CONDO",
  "157V3CYOIMTL" => "BOUGANVILLA SUBD",
  "157V3CYOID1F" => "BOUGAINVILLEA RESUBD OF",
  "157V3CYOIA1P" => "BOTTS UNREC SUBD",
  "157V3CYOI4AT" => "BOOKER T WASHINGTON PARK 1ST ADDN",
  "157V3CYOI6YO" => "BOOKER T WASHINGTON PARK",
  "157V3CYOHY4L" => "BOOKER HEIGHTS ADD 1",
  "157V3CYOI0ZJ" => "BOOKER HEIGHTS",
  "157V3CYOHVM2" => "BONNYMEDE ESTATES UNIT NO 1",
  "157V3CYOHS7D" => "BONNER HEIGHTS",
  "157V3CYOHOWT" => "BONITA BEACH CONDO",
  "157V3CYOHM33" => "BONAVENTURE SUBD",
  "157V3CYOHG61" => "BON AIR SUBD 1ST ADDN",
  "157V3CYOHJ2B" => "BON AIR SUBD",
  "157V3CYOHAWJ" => "BOARDWALK CONDO",
  "157V3CYOH7Q3" => "BOARDWALK AT SUNTREE CONDOMINIUM NO. 3",
  "157V3CYOH4X7" => "BOARDWALK AT SUNTREE CONDOMINIUM NO. 2",
  "157V3CYOH1FM" => "BOARDWALK AT SUNTREE CONDOMINIUM NO. 1",
  "157V3CYOGYBG" => "BLUE WAVE CONDO",
  "157V3CYOGVTI" => "BLUE HERON CONDO",
  "157V3CYOGS8J" => "BLUE HERON AT AQUARINA REVISED PLAT OF",
  "157V3CYOGP7U" => "BLKS 37 38 OF PB 1 PG 17 RESUBD",
  "157V3CYOGMKZ" => "BLAKES PLAT NO 6",
  "157V3CYOGJI8" => "BLAKES PLAT NO 5",
  "157V3CYOGGEY" => "BLAKES PLAT NO 4",
  "157V3CYOGDJ0" => "BLAKES PLAT NO 3",
  "157V3CYOGA98" => "BLAKES PLAT NO 2",
  "157V3CYOG6WA" => "BLAKES PLAT NO 1",
  "157V3CYOG27E" => "BLAIRS SUBD",
  "157V3CYOFQVC" => "BKS CONDO",
  "157V3CYOFNSA" => "BISHOPS SUBD",
  "157V3CYOFK8U" => "BIRCHWOOD FOREST",
  "157V3CYOFH1X" => "BIMINI BAY",
  "157V3CYOFBXW" => "BIG PINE",
  "157V3CYOF9DX" => "BEVIS SUBD",
  "157V3CYOF634" => "BEROJA PALMS",
  "157V3CYOF2Y6" => "BERMUDA HOUSE CONDO PH II",
  "157V3CYOEUOG" => "BERMUDA HOUSE CONDO PH I",
  "157V3CYOLLMG" => "BRENTWOOD LAKES P.U.D. PHASE II",
  "157V3CYOLI4N" => "BRENTWOOD LAKES P.U.D. PHASE I",
  "157V3CYOLF52" => "BRENTWOOD HEIGHTS",
  "157V3CYOLCPJ" => "BRENTWOOD ESTATES UNIT 2",
  "157V3CYOL9J0" => "BRENTWOOD ESTATES UNIT 1",
  "157V3CYOL6BA" => "BREMERS SUBD",
  "157V3CYOL3GK" => "BREEZEWOOD SUBD",
  "157V3CYOL05P" => "BREEZEWAY TOWNHOUSES",
  "157V3CYOKUR7" => "BREEZE RIDGE SUBD 1ST ADDN",
  "157V3CYOKX2Y" => "BREEZE RIDGE SUBD",
  "157V3CYOKRIT" => "BRECHTS SUBD",
  "157V3CYOKOHL" => "BREAKERS WEST CONDO",
  "157V3CYOKLKS" => "BRANDYWINE NORTH",
  "157V3CYOKHZZ" => "BRANDYWINE ESTATES PUD",
  "157V3CYOKBTZ" => "BRADY GROVE PARK SECOND ADDITION",
  "157V3CYOK8QN" => "BRADY GROVE PARK FIRST ADDITION",
  "157V3CYOKEUX" => "BRADY GROVE PARK",
  "157V3CYOK5MD" => "BRADLAND MANOR UNIT 2",
  "157V3CYOK31M" => "BRADLAND MANOR UNIT 1",
  "157V3CYOJZWS" => "BRACKENWOOD SUBDIVISION",
  "157V3CYOJWUY" => "BRABROOKS INDIAN RIVER SUBD",
  "157V3CYOJSGE" => "BOYES SUB PLAT #2",
  "157V3CYOJP37" => "BOWE GARDENS SEC M 1",
  "157V3CYOJM17" => "BOWE GARDENS SEC K 2",
  "157V3CYOJIXC" => "BOWE GARDENS SEC K 1",
  "157V3CYOJG59" => "BOWE GARDENS SEC J 4 A",
  "157V3CYOJDB8" => "BOWE GARDENS SEC J 3",
  "157V3CYOJAB8" => "BOWE GARDENS SEC J 2",
  "157V3CYOJ7A6" => "BOWE GARDENS SEC J 1",
  "157V3CYOJ4IO" => "BOWE GARDENS SEC C 3",
  "157V3CYOJ10V" => "BOWE GARDENS SEC C 1",
  "157V3CYOIY7N" => "BOWE GARDENS SEC B",
  "157V3CYOIVUG" => "BOWE GARDENS SEC A",
  "157V3CYOISRE" => "BOUNTY BAY S/D",
  "157V3CYOIPP6" => "BOULEVARDS THE",
  "157V3CYOIFXI" => "BOUGANVILLA SUBD RESUBD OF",
  "157V3CYOIJIJ" => "BOUGANVILLA SUBD RESUBD",
  "157V3CYOORY3" => "BROOKS HUMPHREYS BUSINESS ADDN",
  "157V3CYOOOKZ" => "BROOKHILL SUBD",
  "157V3CYOOLE3" => "BROOKHAVEN",
  "157V3CYOOIL2" => "BROOK HOLLOW PHASE ONE",
  "157V3CYOOFNS" => "BROOK HILL MANOR",
  "157V3CYOOCHL" => "BROMLEY DRIVE CENTER",
  "157V3CYOO9D0" => "BROADVIEW MANOR",
  "157V3CYOO4WG" => "BROADMOOR ACRES SUBD",
  "157V3CYOO2BD" => "BROAD ACRES",
  "157V3CYONYXV" => "BRITT ESTATES",
  "157V3CYONVBO" => "BRISBANE POINT",
  "157V3CYONSK9" => "BRISBANE ISLE PHASE ONE",
  "157V3CYONPG5" => "BRISAS DEL MAR",
  "157V3CYONMD9" => "BRISA DEL MAR CONDO",
  "157V3CYONGP8" => "BRIKHAVEN REPLAT",
  "157V3CYONJV4" => "BRIKHAVEN",
  "157V3CYONDCE" => "BRIERE SUBDIVISION",
  "157V3CYONA9A" => "BRIDGEWATER PHASE TWO",
  "157V3CYON6U7" => "BRIDGEWATER PHASE ONE",
  "157V3CYON1JV" => "BRIDGEWATER AT BAYSIDE LAKES PH 2",
  "157V3CYON46Z" => "BRIDGEWATER AT BAYSIDE LAKES",
  "157V3CYOMYC0" => "BRIARWOOD SUBD",
  "157V3CYOMV6G" => "BRIARWOOD PARK SEC 4 PART 4",
  "157V3CYOMSEW" => "BRIARWOOD PARK SEC 4 PART 3",
  "157V3CYOMPAA" => "BRIARWOOD PARK SEC 4 PART 2",
  "157V3CYOMLSS" => "BRIARWOOD PARK SEC 4 PART 1",
  "157V3CYOMHUL" => "BRIARWOOD PARK SEC 3",
  "157V3CYOMDZG" => "BRIARWOOD PARK SEC 2",
  "157V3CYOMAJP" => "BRIARWOOD PARK SEC 1",
  "157V3CYOM7HN" => "BRIARWOOD PARK PLAZA",
  "157V3CYOM42R" => "BRIARWOOD MANOR",
  "157V3CYOM0U2" => "BRIARWOOD ESTATES",
  "157V3CYOLX6K" => "BRIARWOOD AT SUNTREE SUNTREE PUD STAGE 5 TRACT 44",
  "1ALX08ZKMU5X" => "BRIARCLIFF",
  "157V3CYOLUB5" => "BREVARD PLACE CONDO",
  "157V3CYOLRFR" => "BRESLAY COMMONS",
  "157V3CYOLO66" => "BRENTWOOD SUBD",
  "157V3CYORSAV" => "BUCKINGHAM AT LEVITT PARK SEC 6-B",
  "157V3CYORP81" => "BUCKINGHAM AT LEVITT PARK SEC 6-A",
  "157V3CYORLUF" => "BUCKINGHAM AT LEVITT PARK SEC 6 PART 1",
  "157V3CYORIJB" => "BUCKINGHAM AT LEVITT PARK SEC 5-A HICKORY",
  "157V3CYORE6G" => "BUCKINGHAM AT LEVITT PARK SEC 4-B",
  "157V3CYORAO3" => "BUCKINGHAM AT LEVITT PARK SEC 4-A",
  "157V3CYOR801" => "BUCKINGHAM AT LEVITT PARK SEC 3-D",
  "157V3CYOR4ID" => "BUCKINGHAM AT LEVITT PARK SEC 3-C",
  "157V3CYOR1AU" => "BUCKINGHAM AT LEVITT PARK SEC 3-B",
  "157V3CYOQWVM" => "BUCKINGHAM AT LEVITT PARK SEC 3-A",
  "157V3CYOQTUZ" => "BUCKINGHAM AT LEVITT PARK SEC 2",
  "157V3CYOQR10" => "BUCKINGHAM AT LEVITT PARK SEC 1",
  "157V3CYOS1N6" => "BUCKINGHAM AT LEVITT PARK",
  "157V3CYOQO0Z" => "BUCHANAN UNREC SUBD",
  "157V3CYOQKQ6" => "BUCHANAN TOWNHOUSES",
  "157V3CYOQF48" => "BUCCANEER CONDO APTS",
  "157V3CYOQBIX" => "BUCCANEER BEACH CLUB CONDO PH II",
  "157V3CYOQ83J" => "BUCCANEER BEACH CLUB CONDO PH I",
  "157V3CYOQ4SD" => "BRYSONS SUBD",
  "157V3CYOQ1P5" => "BRYAN HELLER ESTATES SUBD",
  "157V3CYOPYCK" => "BRYAN AND CARLILE RESUBD",
  "157V3CYOPSIO" => "BRY LYNN ESTATES 3RD ADDN TO",
  "157V3CYOPP71" => "BRY LYNN ESTATES 2ND ADDN TO",
  "157V3CYOPM0O" => "BRY LYNN ESTATES 1ST ADDN TO",
  "157V3CYOPV5J" => "BRY LYNN ESTATES",
  "157V3CYOPJM7" => "BROZMAN ESTATES",
  "157V3CYOPCYA" => "BROWNS SUBDIVISION UNIT TWO",
  "157V3CYOPG34" => "BROWNS SUBDIVISION",
  "157V3CYOPA0P" => "BROWNINGS SUBDIVISION",
  "157V3CYOP6OQ" => "BROOKSIDE AT BAYSIDE LAKES",
  "157V3CYOP3UH" => "BROOKSHIRE AT HERITAGE OAKS PHASE 4",
  "157V3CYOOUVC" => "BROOKSHIRE AT HERITAGE OAKS PHASE 3",
  "157V3CYOP1E9" => "BROOKSHIRE AT HERITAGE OAKS PHASE 1",
  "157V3CYOOY5S" => "BROOKSHIRE AT HERITAGE OAKS PHASE 02",
  "157V3CYOV9T5" => "CAMELOT ESTATES CONDO PH XIV",
  "157V3CYOV6BV" => "CAMELOT ESTATES CONDO PH XIII",
  "157V3CYOV3W3" => "CAMELOT ESTATES CONDO PH XII",
  "157V3CYOV13B" => "CAMELOT ESTATES CONDO PH XI",
  "157V3CYOUY18" => "CAMELOT ESTATES CONDO PH X",
  "157V3CYOUUVY" => "CAMELOT ESTATES CONDO PH VIII",
  "157V3CYOUS7A" => "CAMELOT ESTATES CONDO PH VII",
  "157V3CYOUP2U" => "CAMELOT ESTATES CONDO PH VI",
  "157V3CYOULRL" => "CAMELOT ESTATES CONDO PH V",
  "157V3CYOUIDD" => "CAMELOT ESTATES CONDO PH IX",
  "157V3CYOUEKY" => "CAMELOT ESTATES CONDO PH IV",
  "157V3CYOU6FT" => "CAMELOT ESTATES CONDO PH II",
  "157V3CYOTXWJ" => "CAMELOT ESTATES CONDO PH I",
  "157V3CYOVPRT" => "CAMELOT ESTATES CONDO",
  "157V3CYOTSAC" => "CAMBRIDGE PARK PHASE 3",
  "157V3CYOTP0W" => "CAMBRIDGE PARK PHASE 2",
  "157V3CYOTVHG" => "CAMBRIDGE PARK",
  "157V3CYOTGAL" => "CALICO HEIGHTS",
  "157V3CYOTD16" => "C T BODIFIELDS SUBD",
  "157V3CYOT9YO" => "C M WESTON SUBD",
  "157V3CYOT6CE" => "C L HUGHLETTS WASHINGTON HEIGHTS ADDN TO COCOA",
  "157V3CYOT35N" => "BUTTONWOOD MANOR SUBD",
  "157V3CYOT069" => "BUTLER SUBD",
  "157V3CYOSXRE" => "BURTON MANOR SUBD",
  "157V3CYOSUN3" => "BURTON MANOR 1ST ADD",
  "157V3CYOSR0I" => "BURNS VILLAGE SEC 1",
  "157V3CYOSO49" => "BURNS VILLAGE PARTIAL REPLAT",
  "157V3CYOSKVD" => "BURNETT ACRES",
  "157V3CYOSHSI" => "BURCHFIELD SURVEY BK 1 PG 115",
  "157V3CYOSENQ" => "BURCHFIEL AND BRUNERS ADDN TO CRESCENT BEACH RESU",
  "157V3CYOSBA0" => "BUENA VISTA OCEANFRONT CONDO APTS",
  "157V3CYOS83W" => "BUENA VISTA AT LA CITA",
  "157V3CYOS4NU" => "BUCKINGHAM OF LEVITT PARK SECTION SEVEN PHASE 4",
  "157V3CYORYRC" => "BUCKINGHAM AT LEVITT PARK SEC 7 PHASE 3",
  "157V3CYORVDF" => "BUCKINGHAM AT LEVITT PARK SEC 7 PHASE 1",
  "157V3CYOYCEH" => "CANAVERAL GROVES SUBD PER SB 2 PG 62",
  "157V3CYOY941" => "CANAVERAL GROVES SUBD PER SB 2 PG 57",
  "157V3CYOY64F" => "CANAVERAL GROVES SUBD PER SB 2 PG 53",
  "157V3CYOY3JR" => "CANAVERAL GROVES SUBD PER SB 2 PAGE 60",
  "157V3CYOZ7B7" => "CANAVERAL GROVES SUBD",
  "157V3CYOY090" => "CANAVERAL GROVES S/D",
  "157V3CYOXWR0" => "CANAVERAL GROVES REPLAT UNIT 3 SHEET 4",
  "157V3CYOXTQD" => "CANAVERAL GROVES REPLAT UNIT 3 SHEET 2",
  "157V3CYOXQFL" => "CANAVERAL GROVES REPLAT SEC D UNIT 1 SHEET 4",
  "157V3CYOXMZQ" => "CANAVERAL GROVES REPLAT SEC C UNIT 1 SHEET 1",
  "157V3CYOXJD4" => "CANAVERAL GROVES REPLAT SEC B UNIT 1 SHEET 3",
  "157V3CYOXFYF" => "CANAVERAL GROVES REPLAT SEC A UNIT 1 SHEET 2",
  "157V3CYOXCBT" => "CANAVERAL CIRCLE CONDO",
  "157V3CYOX8RT" => "CANAVERAL BREAKERS CONDO",
  "157V3CYOX5HF" => "CANAVERAL BREAKERS ANNEX APTS",
  "157V3CYOX2F3" => "CANAVERAL BEACH VILLAS",
  "157V3CYOWZ3U" => "CANAVERAL BAY CONDO PH VIII",
  "157V3CYOWWHB" => "CANAVERAL BAY CONDO PH VII",
  "157V3CYOWT3O" => "CANAVERAL BAY CONDO PH VI",
  "157V3CYOWPY2" => "CANAVERAL BAY CONDO PH V",
  "157V3CYOWMKB" => "CANAVERAL BAY CONDO PH IX",
  "157V3CYOWJC4" => "CANAVERAL BAY CONDO PH IV",
  "157V3CYOWFZV" => "CANAVERAL BAY CONDO PH III",
  "157V3CYOWCLN" => "CANAVERAL BAY CONDO PH II",
  "157V3CYOW9GE" => "CANAVERAL BAY CONDO PH I",
  "157V3CYOW685" => "CANAVERAL 1ST ADDN TO PLAT OF",
  "157V3CYOW22F" => "CANARY ISLES A REPLAT OF PART OF CLUB MANOR NO 1 P",
  "157V3CYOVZ5X" => "CAMPS PLAT OF MELBOURNE",
  "157V3CYOVVV4" => "CAMPBELL GARDENS",
  "157V3CYOVSL4" => "CAMERONS 500 A TRACT",
  "157V3CYOVMMW" => "CAMELOT ESTATES CONDO PH XVIII",
  "157V3CYOVJ96" => "CAMELOT ESTATES CONDO PH XVII",
  "157V3CYOVG4G" => "CAMELOT ESTATES CONDO PH XVI",
  "157V3CYOVCXM" => "CAMELOT ESTATES CONDO PH XV",
  "157V3CYP4944" => "CANTERBURY VILLAS",
  "157V3CYP46FE" => "CANOVA BEACH VACATION HOMESITES",
  "157V3CYP402R" => "CANOVA BEACH SEC B REPLAT BLK 2",
  "157V3CYP4352" => "CANOVA BEACH SEC B",
  "157V3CYP3XOB" => "CANOVA BEACH SEC A",
  "157V3CYP3U29" => "CANOVA BEACH REPLAT OF BLK 3",
  "157V3CYP3R73" => "CANOVA BEACH OLSONS ADD TO",
  "157V3CYP3OIA" => "CANNOVA PARK SUBD",
  "157V3CYP3L55" => "CANNOVA PARK SEC C",
  "157V3CYP3HY7" => "CANNOVA PARK SEC B",
  "157V3CYP3EEU" => "CANNOVA PARK SEC A",
  "157V3CYP3BAG" => "CANEBREAKERS CONDOMINIUM",
  "157V3CYP37YQ" => "CANDLEWOOD PINES",
  "157V3CYP345K" => "CANAVERAL WOODS CONDO",
  "157V3CYP30RB" => "CANAVERAL TOWERS",
  "157V3CYP2XAO" => "CANAVERAL SUNRISE TOWNHOMES",
  "157V3CYP2U5C" => "CANAVERAL SUBD",
  "157V3CYP2RLN" => "CANAVERAL STORAGE CONDO PH III",
  "157V3CYP2O7B" => "CANAVERAL STORAGE CONDO PH II",
  "157V3CYP2L55" => "CANAVERAL STORAGE CONDO PH I",
  "157V3CYP2IK9" => "CANAVERAL SANDS CONDO PH IV",
  "157V3CYP2FDN" => "CANAVERAL SANDS CONDO PH III",
  "157V3CYP2BWC" => "CANAVERAL SANDS CONDO PH II",
  "157V3CYP1GMT" => "CANAVERAL SANDS CONDO PH I",
  "157V3CYOZQUV" => "CANAVERAL REALTY UNREC SUBD",
  "157V3CYOZAGE" => "CANAVERAL GROVES SUBDIVISION PER SB 2 PG 54",
  "157V3CYOZ487" => "CANAVERAL GROVES SUBD UNIT 2 SHEET 1",
  "157V3CYOZ0YX" => "CANAVERAL GROVES SUBD REPLAT UNIT 3 SHEET 3",
  "157V3CYOYXP2" => "CANAVERAL GROVES SUBD REPLAT UNIT 3 SHEET 1",
  "157V3CYOYUWZ" => "CANAVERAL GROVES SUBD REPLAT UNIT 2 SHEET 4",
  "157V3CYOYRNJ" => "CANAVERAL GROVES SUBD REPLAT UNIT 2 SHEET 3",
  "157V3CYOYOH5" => "CANAVERAL GROVES SUBD REPLAT UNIT 2 SHEET 2",
  "157V3CYOYLSV" => "CANAVERAL GROVES SUBD PER SB 2 PG 65",
  "157V3CYOYIDJ" => "CANAVERAL GROVES SUBD PER SB 2 PG 64",
  "157V3CYOYF9O" => "CANAVERAL GROVES SUBD PER SB 2 PG 63",
  "157V3CYP7AJQ" => "CAPE VIEW TOWNHOMES",
  "157V3CYP77FJ" => "CAPE SHORES APT BLDG NO 20 CONDO",
  "157V3CYP746G" => "CAPE SHORES APT BLDG NO 19 CONDO",
  "157V3CYP714T" => "CAPE SHORES APT BLDG NO 18 CONDO",
  "157V3CYP6XUG" => "CAPE SHORES APT BLDG NO 17 CONDO",
  "157V3CYP6V87" => "CAPE SHORES APT BLDG NO 16 CONDO",
  "157V3CYP6RMY" => "CAPE SHORES APT BLDG NO 15 CONDO",
  "157V3CYP6OL7" => "CAPE SHORES APT BLDG NO 14 CONDO",
  "157V3CYP6LSK" => "CAPE SHORES APT BLDG NO 13 CONDO",
  "157V3CYP6IDN" => "CAPE SHORES APT BLDG NO 12 CONDO",
  "157V3CYP6FB9" => "CAPE SHORES APT BLDG NO 11 CONDO",
  "157V3CYP6CS3" => "CAPE SHORES APT BLDG NO 10 CONDO",
  "157V3CYP69KU" => "CAPE SHORES APT BLDG NO 09 CONDO",
  "157V3CYP66IA" => "CAPE SHORES APT BLDG NO 08 CONDO",
  "157V3CYP641X" => "CAPE SHORES APT BLDG NO 07 CONDO",
  "157V3CYP60V7" => "CAPE SHORES APT BLDG NO 06 CONDO",
  "157V3CYP5XS1" => "CAPE SHORES APT BLDG NO 05 CONDO",
  "157V3CYP5V61" => "CAPE SHORES APT BLDG NO 04 CONDO",
  "157V3CYP5RXX" => "CAPE SHORES APT BLDG NO 03 CONDO",
  "157V3CYP5OXP" => "CAPE SHORES APT BLDG NO 02 CONDO",
  "157V3CYP5M51" => "CAPE SHORES APT BLDG NO 01 CONDO",
  "157V3CYP5J3Z" => "CAPE ROYAL OFFICE CONDO",
  "157V3CYP5G3O" => "CAPE KENNEDY GROVES UNIT 9",
  "157V3CYP5DHU" => "CAPE KENNEDY GROVES UNIT 11",
  "157V3CYP5A5M" => "CAPE HACIENDA CONDO",
  "157V3CYP577X" => "CAPE GARDENS",
  "157V3CYP54M6" => "CAPE ESCAPE",
  "157V3CYP51JT" => "CAPE CLUB CONDO",
  "157V3CYP4YIX" => "CAPE CANAVERAL SUBD UNRECORDED",
  "157V3CYP4W7F" => "CAPE CANAVERAL RIVER ESTATES UNREC SUBD",
  "157V3CYP4T0Q" => "CAPE CANAVERAL REALTY UNREC SUBD",
  "157V3CYP4PGD" => "CAPE CANAVERAL BEACH GDNS UNIT 2",
  "157V3CYP4FY0" => "CAPE CANAVERAL BEACH GDNS UNIT 1",
  "157V3CYP4C8B" => "CAPE CANAV PROF BLDG A CONDO",
  "157V3CYPAB04" => "CARLTON TERRACE AMENDED PLAT OF",
  "157V3CYPADVI" => "CARLTON TERRACE ADDN",
  "157V3CYPA7SO" => "CARLTON TERR RESUBD OF BLKS A B C",
  "157V3CYPA40E" => "CARLTON STEWART GDS REPLAT BLK A",
  "157V3CYPA0LS" => "CARLTON STEWART GARDENS PLAT NO 2",
  "157V3CYP9XFG" => "CARLTON STEWART GARDENS PLAT NO 1 REPLAT OF PT OF",
  "157V3CYP9QH8" => "CARLTON STEWART GARDENS PLAT NO 1 REPLAT OF LOT 3",
  "157V3CYP9NI6" => "CARLTON GROVES SUBD UNIT 2 SEC 2",
  "157V3CYP9KO1" => "CARLTON GROVES SUBD UNIT 2 SEC 1",
  "157V3CYP9GZT" => "CARLTON GROVES SUBD NO 3",
  "157V3CYP9E26" => "CARLTON GROVES SUBD NO 2",
  "157V3CYP9ABF" => "CARLTON GROVES SUBD NO 1",
  "157V3CYP977I" => "CARLTON GROVES SOUTH UNIT 1",
  "157V3CYP9480" => "CARLILE WARD PLAT OF MIMS",
  "157V3CYP91QF" => "CARIBBEAN ISLES UNIT 3",
  "157V3CYP8YRJ" => "CARIBBEAN ISLES UNIT 2",
  "157V3CYP8VUB" => "CARIBBEAN ISLES UNIT 1",
  "157V3CYP8SR5" => "CARDINAL TERRACE",
  "157V3CYP8PIK" => "CARBONI PARK SUBD",
  "157V3CYP8MA5" => "CAPTAINS COVE OF MELBOURNE CONDO",
  "157V3CYP8JJU" => "CAPTAINS COVE A CONDO PH III",
  "157V3CYP8G4N" => "CAPTAINS COVE A CONDO PH II",
  "157V3CYP8D8F" => "CAPTAINS COVE A CONDO PH I",
  "157V3CYP8AMG" => "CAPRON TRACE - PHASE 3",
  "157V3CYP879Y" => "CAPRON TRACE - PHASE 2",
  "157V3CYP84EA" => "CAPRON TRACE - PHASE 1 VIERA CENTRAL PUD - A PORTI",
  "157V3CYP81N4" => "CAPRON RIDGE PHASE TWO",
  "157V3CYP7YLH" => "CAPRON RIDGE PHASE THREE",
  "157V3CYP7VD3" => "CAPRON RIDGE PHASE SEVEN",
  "157V3CYP7SMA" => "CAPRON RIDGE PHASE ONE",
  "157V3CYP7PD8" => "CAPRON RIDGE PHASE FOUR",
  "157V3CYP7MBT" => "CAPRON RIDGE PHASE FIVE",
  "157V3CYP7JFE" => "CAPRON OFFICE CENTER CONDO",
  "157V3CYP7G3W" => "CAPE WINDS CONDO",
  "157V3CYP7D2N" => "CAPE VILLAS",
  "157V3CYPDJSP" => "CASA PLAYA CONDO",
  "157V3CYPDGFS" => "CASA LOMA SUBD",
  "157V3CYPDDTY" => "CASA LOMA CONDO",
  "157V3CYPDAFE" => "CASA LAGUNA CONDO",
  "157V3CYPD7KZ" => "CASA FIESTA CONDO",
  "157V3CYPD4TQ" => "CASA ESPERANZA",
  "157V3CYPD1NL" => "CASA DEL SOL BY THE SEA",
  "157V3CYPCYPX" => "CASA DEL RIO CONDO",
  "157V3CYPCT0R" => "CASA DEL MAR CONDO APTS",
  "157V3CYPCWA4" => "CASA DEL MAR CONDO",
  "157V3CYPCPY6" => "CASA CANAVERAL CONDO",
  "157V3CYPCK5A" => "CARVER VILLAGE UNRECORDED",
  "157V3CYPCND7" => "CARVER VILLAGE",
  "157V3CYPCGMH" => "CARVER PARK",
  "157V3CYPCDJ6" => "CARRIAGE PARK PHASE 3",
  "157V3CYPCA47" => "CARRIAGE PARK PHASE 2",
  "157V3CYPC717" => "CARRIAGE PARK PHASE 1",
  "157V3CYPC3HF" => "CARRIAGE PARK CONDO PH XII",
  "157V3CYPC028" => "CARRIAGE PARK CONDO PH XI",
  "157V3CYPBVZH" => "CARRIAGE PARK CONDO PH X",
  "157V3CYPBSNJ" => "CARRIAGE PARK CONDO PH VIII",
  "157V3CYPBQ0T" => "CARRIAGE PARK CONDO PH VII",
  "157V3CYPBMSU" => "CARRIAGE PARK CONDO PH VI",
  "157V3CYPBIRP" => "CARRIAGE PARK CONDO PH V",
  "157V3CYPBEI6" => "CARRIAGE PARK CONDO PH IX",
  "157V3CYPBBOZ" => "CARRIAGE PARK CONDO PH IV",
  "157V3CYPB9AB" => "CARRIAGE PARK CONDO PH III",
  "157V3CYPB5WP" => "CARRIAGE PARK CONDO PH II",
  "157V3CYPB2HS" => "CARRIAGE PARK CONDO PH I",
  "157V3CYPAZMP" => "CARRIAGE GATE SUBDIVISION",
  "157V3CYPAWH9" => "CARPENTER HILLS UNIT 1",
  "157V3CYPAT7N" => "CAROLWOOD ESTATES NO 6 REPLAT",
  "157V3CYPAP2L" => "CAROLINA TRACT ADD TO MELBOURNE THE",
  "157V3CYPAMA9" => "CAROLINA COURT RESUBD",
  "157V3CYPAJCV" => "CARMEL RETIREMENT COMMUNITY CONDO PHASE 1",
  "157V3CYPAGUV" => "CARLYLE CONDOMINIUM OF CRESCENT BEACH",
  "157V3CYPGRYX" => "CATALINA VILLAGE 4TH ADDN",
  "157V3CYPGOEB" => "CATALINA VILLAGE 3RD ADDN",
  "157V3CYPGL8L" => "CATALINA VILLAGE 2ND ADDN",
  "157V3CYPGIMO" => "CATALINA VILLAGE 1ST ADDN",
  "157V3CYPH0VQ" => "CATALINA VILLAGE",
  "157V3CYPGFAK" => "CATALINA SEC 2",
  "157V3CYPGCE0" => "CATALINA ISLE ESTATES UNIT 6",
  "157V3CYPG9R1" => "CATALINA ISLE ESTATES UNIT 5",
  "157V3CYPG6JR" => "CATALINA ISLE ESTATES UNIT 4",
  "157V3CYPG39A" => "CATALINA ISLE ESTATES UNIT 3",
  "157V3CYPG0HG" => "CATALINA ISLE ESTATES UNIT 2A",
  "157V3CYPFXHW" => "CATALINA ISLE ESTATES UNIT 2",
  "157V3CYPFUG4" => "CATALINA ISLE ESTATES UNIT 1",
  "157V3CYPFRMI" => "CATALINA EAST CONDO PH I",
  "157V3CYPH3DM" => "CATALINA",
  "157V3CYPFLC0" => "CASTLE OAKS REPLAT",
  "157V3CYPFOII" => "CASTLE OAKS",
  "157V3CYPFIS9" => "CASTAWAY VILLAS CONDO PH IV",
  "157V3CYPFFLO" => "CASTAWAY VILLAS CONDO PH III",
  "157V3CYPFCDE" => "CASTAWAY VILLAS CONDO PH II",
  "157V3CYPF9S7" => "CASTAWAY VILLAS CONDO PH I",
  "157V3CYPF6I6" => "CASTAWAY SHORES CONDO PH II",
  "157V3CYPF3PV" => "CASTAWAY SHORES CONDO PH I",
  "157V3CYPF1B4" => "CASSEEKEE TRAILS CONDO",
  "157V3CYPEV0X" => "CASCADE PARK RESUBD BLK 3",
  "157V3CYPEXWE" => "CASCADE PARK RESUBD",
  "157V3CYPESLH" => "CASAS DEL RIO",
  "157V3CYPEPD2" => "CASABELLA PHASE TWO",
  "157V3CYPELUP" => "CASABELLA PHASE ONE",
  "157V3CYPEDFY" => "CASABELLA OFFICE PARK CONDO",
  "157V3CYPE9IU" => "CASA VERDE CLUB CONDO PH V",
  "157V3CYPE5XW" => "CASA VERDE CLUB CONDO PH IV",
  "157V3CYPE37L" => "CASA VERDE CLUB CONDO PH III",
  "157V3CYPDZSU" => "CASA VERDE CLUB CONDO PH II",
  "157V3CYPDWBG" => "CASA VERDE CLUB CONDO PH I",
  "157V3CYPDT97" => "CASA SERENA CONDO PH I",
  "157V3CYPK5MO" => "CHELFORD SUBDIVISION",
  "157V3CYPK2JY" => "CHATEAU BY THE SEA CONDO",
  "157V3CYPJZGF" => "CHASE HAMMOCK LAKES",
  "157V3CYPJWAQ" => "CHAS W SKELLY SUBD",
  "157V3CYPJT6S" => "CHAROLAIS ESTATES PHASE 2",
  "157V3CYPJQ67" => "CHAROLAIS ESTATES PHASE 1",
  "157V3CYPJNIB" => "CHARLES MC CRAY",
  "157V3CYPJKFC" => "CHARLES COURT",
  "157V3CYPJHMH" => "CHANDLER PARK REPLAT OF",
  "157V3CYPJF9U" => "CHANDLER BEND CONDO",
  "157V3CYPJC6H" => "CHAMPENAE CONDOMINIUM",
  "157V3CYPJ6Z0" => "CHADWICK TOWNHOMES",
  "157V3CYPJ4C5" => "CENTURY OAK SUBD",
  "157V3CYPJ1H7" => "CENTURY MEDICAL PLAZA",
  "157V3CYPIVF6" => "CENTRAL PARK ESTATES",
  "157V3CYPIYJJ" => "CENTRAL PARK",
  "157V3CYPISIN" => "CENTER ONE CONDOMINIUM",
  "157V3CYPIPW7" => "CEDAR SIDE II CONDO",
  "157V3CYPIMME" => "CEDAR SIDE CONDO PH I",
  "157V3CYPIJ4E" => "CEDAR RUN",
  "157V3CYPIGFT" => "CEDAR LAKES MOBILE HOME PARK CO-OP",
  "157V3CYPID6T" => "CEDAR KEY CONDO",
  "157V3CYPI9G1" => "CEDAR AVENUE CONDO",
  "157V3CYPI5UT" => "CBMC CONDO",
  "157V3CYPI2MQ" => "CAV LANDING",
  "157V3CYPHZWK" => "CATHEDRAL VILLAS SECTION 3",
  "157V3CYPHWCO" => "CATHEDRAL VILLAS SECTION 2",
  "157V3CYPHSSQ" => "CATHEDRAL VILLAS SECTION 1",
  "157V3CYPHM02" => "CATHEDRAL PINES II SECTION TWO",
  "157V3CYPHJ6W" => "CATHEDRAL PINES II SECTION THREE-B",
  "157V3CYPHFO7" => "CATHEDRAL PINES II SECTION THREE-A",
  "157V3CYPHCAX" => "CATHEDRAL PINES II SECTION ONE",
  "157V3CYPHPD2" => "CATHEDRAL PINES",
  "157V3CYPH9NW" => "CATHEDRAL OAKS",
  "157V3CYPH6JS" => "CATHEDRAL ESTATES",
  "157V3CYPGXS8" => "CATALINA VILLAGE 6TH ADDN",
  "157V3CYPGUN4" => "CATALINA VILLAGE 5TH ADDITION",
  "157V3CYPNBYG" => "CITRUS RIVER GROVES UNIT 2",
  "157V3CYPN9PY" => "CITRUS RIVER GROVES UNIT 1",
  "157V3CYPN6YY" => "CITRUS POINTE",
  "157V3CYPN4JC" => "CITRUS ISLE",
  "157V3CYPN216" => "CITRUS HEIGHTS",
  "157V3CYPMVWV" => "CITRUS ESTATES UNIT 2",
  "157V3CYPMTKJ" => "CITRUS ESTATES UNIT 1",
  "157V3CYPMYYH" => "CITRUS ESTATES",
  "157V3CYPMQK1" => "CITRUS CLUB",
  "157V3CYPMMLP" => "CISNA PARK",
  "157V3CYPMD46" => "CIRCLE PARK REPLAT OF",
  "157V3CYPMGMR" => "CIRCLE PARK PHASE IV REPLAT OF",
  "157V3CYPMJRS" => "CIRCLE PARK",
  "157V3CYPM9W9" => "CINNAMON COVE",
  "157V3CYPM5OU" => "CIDCO PARK",
  "157V3CYPLZNR" => "CIANCI ESTATES",
  "15C240NRYFEL" => "CHRISTMAS PARK 1ST ADDITION",
  "157V3CYPLWHX" => "CHRISTIAN CHURCH AT VIERA",
  "157V3CYPLTVN" => "CHITTENDON AND MITCHELL ADDN TO COCOA",
  "157V3CYPLQPD" => "CHINA WARD PLAT OF MIMS",
  "157V3CYPLNG1" => "CHILDRES ADDN TO GROVE PARK",
  "157V3CYPLJT1" => "CHILDRES 4TH ADDN TO GROVE PARK",
  "157V3CYPLFWT" => "CHILDRES 3RD ADDN TO GROVE PARK",
  "157V3CYPLCIE" => "CHILDRES 2ND ADDN TO GROVE PARK",
  "157V3CYPL95E" => "CHEVY CHASE GARDENS",
  "157V3CYPL2HX" => "CHESTNUT RUN 1ST ADDITION",
  "157V3CYPL69P" => "CHESTNUT RUN",
  "157V3CYPKZHK" => "CHELSEA PARK UNIT 9",
  "157V3CYPKWN0" => "CHELSEA PARK UNIT 8",
  "157V3CYPKTEO" => "CHELSEA PARK UNIT 7 A S/D OF A PORT OF SEC 22 AND",
  "157V3CYPKQ7X" => "CHELSEA PARK UNIT 6",
  "157V3CYPKN58" => "CHELSEA PARK UNIT 5",
  "157V3CYPKK1O" => "CHELSEA PARK UNIT 4",
  "157V3CYPKH0H" => "CHELSEA PARK UNIT 3",
  "157V3CYPKECC" => "CHELSEA PARK UNIT 2A",
  "157V3CYPK8B9" => "CHELSEA PARK UNIT 1 REPLAT",
  "157V3CYPKBBG" => "CHELSEA PARK UNIT 1",
  "157V3CYPQKP6" => "CLUB HACIENDAS CONDO PH II",
  "157V3CYPQHAR" => "CLUB HACIENDAS CONDO PH I",
  "157V3CYPQ3Q1" => "CLOISTERS REPLAT #2 OF THE",
  "157V3CYPQ0CO" => "CLOISTERS REPLAT #1 OF THE",
  "157V3CYPQDSC" => "CLOISTERS PHASE IIIC",
  "157V3CYPQADW" => "CLOISTERS PHASE II THE",
  "157V3CYPQ70T" => "CLOISTERS PHASE 1 THE",
  "157V3CYPPWYI" => "CLOISTER TERRACE",
  "157V3CYPPOCO" => "CLINES CORNER REPLAT OF",
  "157V3CYPPSFY" => "CLINES CORNER",
  "157V3CYPPLGT" => "CLIFTON EDGE",
  "157V3CYPPI91" => "CLIFTON ACRES",
  "157V3CYPPEWS" => "CLIFF CREEK CONDO",
  "157V3CYPPBR0" => "CLEVELAND GARDEN CONDO",
  "157V3CYPP54W" => "CLEMENTS WOOD PHASE IV C",
  "157V3CYPP2O7" => "CLEMENTS WOOD PHASE IV B",
  "157V3CYPOZHX" => "CLEMENTS WOOD PHASE 4A",
  "157V3CYPOWBJ" => "CLEMENTS WOOD PHASE 3",
  "157V3CYPOSRE" => "CLEMENTS WOOD PHASE 2",
  "157V3CYPP8EI" => "CLEMENTS WOOD",
  "157V3CYPOPHZ" => "CLEGGMOOR SUBD",
  "157V3CYPOMNC" => "CLEARVIEW TERRACE",
  "157V3CYPOJON" => "CLEARVIEW HGTS SUBD",
  "157V3CYPOG73" => "CLEARLAKE VILLAGE-UNIT 2",
  "157V3CYPOD6U" => "CLEARLAKE VILLAGE UNIT 1",
  "157V3CYPOA5P" => "CLEAR LAKE VILLAGE UNREC",
  "157V3CYPO6GL" => "CLEAR LAKE TERRACE",
  "157V3CYPO35F" => "CLEAR LAKE PLAZA SUBD",
  "157V3CYPO0MC" => "CLAYTONS SUBD NO 1",
  "157V3CYPNXE2" => "CLAUDIA ACRES",
  "157V3CYPNU37" => "CLARIDGE OCEANFRONT CONDOMINIUM",
  "157V3CYPNOS4" => "CITY ACRES ADDN NO 2",
  "157V3CYPNMDC" => "CITY ACRES ADDN NO 1",
  "157V3CYPNR5C" => "CITY ACRES",
  "157V3CYPNJXO" => "CITRUS RIVER RANCHETTES",
  "157V3CYPNHN7" => "CITRUS RIVER GROVES UNIT 4",
  "157V3CYPNF54" => "CITRUS RIVER GROVES UNIT 3",
  "157V3CYPTWAE" => "COCOA INDUSTRIAL PARK",
  "157V3CYPTQCQ" => "COCOA INDIAN RIVER PROPERTIES MAP NO 2",
  "157V3CYPTMQQ" => "COCOA INDIAN RIVER PROPERTIES MAP 1 SUBD OF SECS 1",
  "157V3CYPTJJM" => "COCOA INDIAN RIVER PROPERTIES INC",
  "157V3CYPTT2M" => "COCOA INDIAN RIVER PROPERTIES",
  "157V3CYPTA5I" => "COCOA HILLS SUBD REPLAT OF PT OF TRACT E",
  "157V3CYPTD8X" => "COCOA HILLS SUBD REPLAT OF PT OF",
  "157V3CYPTGL6" => "COCOA HILLS SUBD",
  "157V3CYPT7EJ" => "COCOA HEIGHTS",
  "157V3CYPT43Q" => "COCOA COMMONS",
  "157V3CYPT0O9" => "COCOA COMMERCIAL CENTER CONDO",
  "157V3CYPSXCQ" => "COCOA CEMETERY",
  "157V3CYPSQWW" => "COCOA BEACH TOWERS CONDO",
  "157V3CYPSD7X" => "COCOA BEACH REVISED PLAT OF 4TH ADDN TO",
  "157V3CYPS9L5" => "COCOA BEACH RESUBD OF BLKS 17 TO 22 INC",
  "157V3CYPSNJA" => "COCOA BEACH PLAT OF BLKS 37 AND 28A",
  "157V3CYPSKFN" => "COCOA BEACH OFFICE CONDO",
  "157V3CYPSGSL" => "COCOA BEACH CLUB CONDO",
  "157V3CYPS699" => "COCOA BEACH 8TH ADDN TO",
  "157V3CYPS2OA" => "COCOA BEACH 7TH ADDN TO",
  "157V3CYPRZT3" => "COCOA BEACH 6TH ADDN TO",
  "157V3CYPRW7Q" => "COCOA BEACH 5TH ADDN TO AMENDED PLAT OF",
  "157V3CYPRSN3" => "COCOA BEACH 3RD ADDN TO",
  "157V3CYPRIYH" => "COCOA BEACH 2ND ADDN TO AMENDED PLAT OF BLKS 102",
  "157V3CYPRLQU" => "COCOA BEACH 2ND ADDN TO AMENDED",
  "157V3CYPRP7B" => "COCOA BEACH 2ND ADDN TO",
  "157V3CYPRFIT" => "COCOA BEACH 1ST ADDN TO",
  "157V3CYPSU2O" => "COCOA BEACH",
  "157V3CYPRC0O" => "COCOA BAY PHASE 1",
  "157V3CYPR58K" => "COCOA ANNEX REPLAT NO 2",
  "157V3CYPR8JT" => "COCOA ANNEX",
  "157V3CYPQVXC" => "COASTAL COMMERCE CENTER TRACT G 1ST REPLAT",
  "157V3CYPQZ1F" => "COASTAL COMMERCE CENTER",
  "157V3CYPQSD8" => "COASTAL CAY CONDO",
  "157V3CYPQNS0" => "CLUB MANOR NO 1",
  "157V3CYPX1ME" => "COCOA RICO RANCHETTES",
  "157V3CYPWYR5" => "COCOA PINES",
  "157V3CYPWW67" => "COCOA PALMS SUBD",
  "157V3CYPWN2V" => "COCOA OCEAN BEACH SUBD REPLAT NO 1",
  "157V3CYPWPQY" => "COCOA OCEAN BEACH SUBD",
  "157V3CYPWSX1" => "COCOA OCEAN BEACH",
  "157V3CYPWJY4" => "COCOA NORTH VILLAS NO 2",
  "157V3CYPWH1Z" => "COCOA NORTH UNIT 8",
  "157V3CYPWEGC" => "COCOA NORTH UNIT 7",
  "157V3CYPWAS8" => "COCOA NORTH UNIT 6",
  "157V3CYPW7M0" => "COCOA NORTH UNIT 5",
  "157V3CYPW56O" => "COCOA NORTH UNIT 4",
  "157V3CYPW1ON" => "COCOA NORTH UNIT 3",
  "157V3CYPVXHD" => "COCOA NORTH UNIT 2",
  "157V3CYPVREX" => "COCOA NORTH UNIT 1 PARTIAL REPLAT OF",
  "157V3CYPVUFC" => "COCOA NORTH UNIT 1",
  "157V3CYPVOQS" => "COCOA MODERN MANORS",
  "157V3CYPVM7R" => "COCOA ISLES COUNTRY CLUB SECTION PHASE 1",
  "157V3CYPVITI" => "COCOA ISLES COUNTRY CLUB SEC PH 1 REPT OF PT SEC",
  "157V3CYPVFN6" => "COCOA ISLES APTS CONDO",
  "157V3CYPVCB7" => "COCOA ISLES AMENDED PLAT OF",
  "157V3CYPV90G" => "COCOA ISLES 9TH ADDN SEC B",
  "157V3CYPV6AU" => "COCOA ISLES 9TH ADDN SEC A",
  "157V3CYPV2N3" => "COCOA ISLES 8TH ADDN",
  "157V3CYPUZHP" => "COCOA ISLES 7TH ADDN",
  "157V3CYPUWYU" => "COCOA ISLES 6TH ADDN",
  "157V3CYPUTPU" => "COCOA ISLES 5TH ADDN REPLAT",
  "157V3CYPUQBK" => "COCOA ISLES 4TH ADDN",
  "157V3CYPUMWL" => "COCOA ISLES 3RD ADDN SEC C",
  "157V3CYPUI8L" => "COCOA ISLES 3RD ADDN SEC B",
  "157V3CYPUFN3" => "COCOA ISLES 3RD ADDN SEC A",
  "157V3CYPUCFT" => "COCOA ISLES 2ND ADDN",
  "157V3CYPU91Z" => "COCOA ISLES 12TH ADDN",
  "157V3CYPU5IW" => "COCOA ISLES 11TH CAPE ROYAL ADDN PHASE 3",
  "157V3CYPU2EK" => "COCOA ISLES 11TH CAPE ROYAL ADDN PHASE 2",
  "157V3CYPTZQK" => "COCOA ISLES 10TH ADDN",
  "157V3CYPZUUO" => "COLLEGE PINES CONDO PH III",
  "157V3CYPZRLF" => "COLLEGE PINES CONDO PH II",
  "157V3CYPZY3E" => "COLLEGE PINES CONDO",
  "157V3CYPZLAJ" => "COLLEGE PARK RE AMENDED MAP OF",
  "157V3CYPZOHT" => "COLLEGE PARK AMENDED MAP",
  "157V3CYPZI7H" => "COLLEGE PARK 1ST ADDN TO",
  "157V3CYPZFR0" => "COLLEGE OAKS CONDO PH II",
  "157V3CYPZDIY" => "COLLEGE OAKS CONDO PH I",
  "157V3CYPZA5I" => "COLLEGE MANOR UNIT 3",
  "157V3CYPZ71D" => "COLLEGE MANOR UNIT 2",
  "157V3CYPZ47S" => "COLLEGE MANOR NO 1A",
  "157V3CYPYY1V" => "COLLEGE MANOR NO 1 REPLAT OF",
  "157V3CYPZ15R" => "COLLEGE MANOR NO 1",
  "157V3CYPYV6H" => "COLLEGE GREEN ESTATES UNIT 6",
  "157V3CYPYS10" => "COLLEGE GREEN ESTATES UNIT 5",
  "157V3CYPYOU1" => "COLLEGE GREEN ESTATES UNIT 4",
  "157V3CYPYLHW" => "COLLEGE GREEN ESTATES UNIT 3",
  "157V3CYPYHCD" => "COLLEGE GREEN ESTATES UNIT 2",
  "157V3CYPYESP" => "COLLEGE GREEN ESTATES UNIT 1",
  "157V3CYPYBEK" => "COLLEGE CLUB CONDO PH IV",
  "157V3CYPY7YS" => "COLLEGE CLUB CONDO PH III",
  "157V3CYPY58T" => "COLLEGE CLUB CONDO PH II",
  "157V3CYPY23J" => "COLLEGE CLUB CONDO PH I",
  "157V3CYPXYTB" => "COLFAX LANDING PHASE 2",
  "157V3CYPXW6Z" => "COLFAX LANDING PHASE 1",
  "157V3CYPXT0K" => "COLEMANS ADD TO TITUSVILLE",
  "157V3CYPXQ3T" => "COCONUT CAY",
  "157V3CYPXNHK" => "COCOA WOODS REPLAT OF",
  "157V3CYPXKAD" => "COCOA VILLAGE SUBD",
  "157V3CYPXGUD" => "COCOA VILLAGE SQUARE CONDO",
  "157V3CYPR2JH" => "COCOA UNREC SD OF BLKS J AND K OF",
  "157V3CYPX7SL" => "COCOA ROCKLEDGE RESUBD OF LAND COMPANYS 1ST ADDN",
  "157V3CYPXAUY" => "COCOA ROCKLEDGE LAND COS 1ST ADDN RESUBD OF",
  "157V3CYPXE3D" => "COCOA ROCKLEDGE LAND COS 1ST ADDN",
  "157V3CYPX4P6" => "COCOA RIVER DEVELOPMENT",
  "157V3CYQ2YSJ" => "CONRADS UNREC SUBD",
  "157V3CYQ2W8L" => "CONQUISTADOR CONDO",
  "157V3CYQ2PT5" => "CONNERS CASTLE DARE SUBD REPLAT OF PT OF",
  "157V3CYQ2MZR" => "CONNERS CASTLE DARE SUBD REPLAT OF BLKS E AND F",
  "157V3CYQ2SXR" => "CONNERS CASTLE DARE SUBD",
  "157V3CYQ2JQ1" => "CONNERS CASTLE DARE 1ST ADDN",
  "157V3CYQ2GUZ" => "CONE MEDICAL PARK",
  "157V3CYQ2E0L" => "CONDOMINIUMS OF NORTH INDIALANTIC",
  "157V3CYQ2ASC" => "COMPASS POINTE",
  "157V3CYQ27KE" => "COMMERCE LIMITED LIABILITY COMPANY SUBDIVISION",
  "157V3CYQ24OM" => "COMBS REPT OF PT OF SPILLERS ADD",
  "157V3CYQ21J3" => "COLUMBIAD PLAZA SUBD",
  "157V3CYQ1YFT" => "COLUMBIA COMMERCIAL CONDO",
  "157V3CYQ1VRE" => "COLONY WEST SUBD SEC 3",
  "157V3CYQ1SF2" => "COLONY WEST SUBD SEC 2",
  "157V3CYQ1PAY" => "COLONY WEST SUBD SEC 1",
  "157V3CYQ1JCQ" => "COLONY PARK SEC 3",
  "157V3CYQ1G8D" => "COLONY PARK SEC 2",
  "157V3CYQ1D97" => "COLONY PARK NORTH UNIT 3",
  "157V3CYQ1A5M" => "COLONY PARK NORTH UNIT 2",
  "157V3CYQ17GR" => "COLONY PARK NORTH UNIT 1",
  "157V3CYQ1MG2" => "COLONY PARK",
  "157V3CYQ1198" => "COLONIAL VILLAS NO 3",
  "157V3CYQ152S" => "COLONIAL VILLAS",
  "157V3CYQ0YAL" => "COLONIAL SQUARE COMMERCIAL CONDOMINIUM",
  "157V3CYQ0UPO" => "COLONIAL SQUARE COMMERCIAL CENTER",
  "157V3CYQ0RMK" => "COLONIAL MANOR",
  "157V3CYQ0OKF" => "COLONIAL HOUSE CONDO",
  "157V3CYQ0IIV" => "COLONIAL HEIGHTS ADDN NO 4",
  "157V3CYQ0FF0" => "COLONIAL HEIGHTS ADDN NO 2",
  "157V3CYQ0CIO" => "COLONIAL HEIGHTS ADDN NO 1",
  "157V3CYQ0LL5" => "COLONIAL HEIGHTS",
  "157V3CYQ09EC" => "COLONIAL ESTATES",
  "157V3CYQ06NP" => "COLONIAL ARMS CONDO",
  "157V3CYQ00VX" => "COLLEGE VIEW ESTATES REPLAT",
  "157V3CYQ0429" => "COLLEGE VIEW ESTATES",
  "157V3CZDZEPQ" => "COSTA DEL SOL CONDO PH IX",
  "157V3CZDZBOB" => "COSTA DEL SOL CONDO PH IV",
  "157V3CZDZ8E0" => "COSTA DEL SOL CONDO PH III",
  "157V3CZDZ5MK" => "COSTA DEL SOL CONDO PH II",
  "157V3CZDZ2PE" => "COSTA DEL SOL CONDO PH I",
  "157V3CZDYZHY" => "CORTONA SUBDIVISION",
  "157V3CZDYT7Z" => "CORPORATE PARK AT VIERA OFFICE CONDO NO.2",
  "157V3CZDYQ94" => "CORPORATE PARK AT VIERA OFFICE CONDO NO 3",
  "157V3CZDYWDP" => "CORPORATE PARK AT VIERA OFFICE CONDO",
  "157V3CZDYN6B" => "CORPORATE INDUSTRIAL CENTER CONDO",
  "157V3CZDYJZ5" => "CORPLEX",
  "157V3CZDYGL0" => "CORDIAL MANOR SUBD",
  "157V3CZDYDQE" => "CORAL WAY CONDO",
  "157V3CZDYAW0" => "CORAL SPRINGS",
  "157V3CZDY8DP" => "CORAL SEAS CONDOMINIUM",
  "157V3CZDY553" => "CORAL SEA VILLAS",
  "157V3CZDY0WI" => "CORAL SANDS CONDO PH II",
  "157V3CZDXY19" => "CORAL SANDS CONDO PH I",
  "157V3CZDXUVN" => "CORAL REEF",
  "157V3CYQ4FP1" => "CORAL PALMS CONDO",
  "157V3CYQ4D36" => "CORAL PALM CONDO",
  "157V3CYQ49XQ" => "CORAL KEY",
  "157V3CYQ471B" => "CORAL GARDENS CONDO",
  "157V3CYQ449R" => "CORAL BAY CONDO",
  "157V3CYQ417U" => "CORAL APTS CONDO",
  "157V3CYQ3XRV" => "CORA B GRAYS RESUBD",
  "157V3CYQ3RVL" => "COQUINA RIDGE MOBILE HOMES SEC 3",
  "157V3CYQ3OIK" => "COQUINA RIDGE MOBILE HOMES SEC 2",
  "157V3CYQ3LVC" => "COQUINA RIDGE MOBILE HOMES SEC 1",
  "157V3CYQ3UYW" => "COQUINA RIDGE",
  "157V3CYQ3J4W" => "COQUINA POINT",
  "157V3CYQ3GU0" => "COQUINA PALMS",
  "157V3CYQ3AMB" => "COQUINA HILLS 1ST ADDN",
  "157V3CYQ3DSS" => "COQUINA HILLS",
  "157V3CYQ37ZJ" => "COQUINA BAY",
  "157V3CYQ35OO" => "COPPERFIELD",
  "157V3CYQ32EV" => "CONVAIR COVE",
  "157V3CZE29FZ" => "COUNTRY WOODS VILLAGE CONDO PH I",
  "157V3CZE2LOD" => "COUNTRY WOODS",
  "157V3CZE26AD" => "COUNTRY WALK AT SUNTREE STAGE 8 TRACT 64 PUD",
  "157V3CZE23NZ" => "COUNTRY WALK AT SUNTREE STAGE 8 TR 64 PUD REPLAT",
  "157V3CZE01NY" => "COUNTRY THE",
  "157V3CZE20KP" => "COUNTRY RIVER ESTATES UNIT NO 1",
  "157V3CZE1XH7" => "COUNTRY RIVER ESTATES REPLAT OF",
  "157V3CZE1TZ5" => "COUNTRY PLACE THE",
  "157V3CZE1QXF" => "COUNTRY OAKS",
  "157V3CZE1NQK" => "COUNTRY LAKE ESTS",
  "157V3CZE1KQ7" => "COUNTRY GARDENS II CONDO",
  "157V3CZE1HRI" => "COUNTRY GARDENS I CONDO",
  "157V3CZE1EPP" => "COUNTRY ESTATES UNIT 2",
  "157V3CZE1CD5" => "COUNTRY ESTATES UNIT 1",
  "157V3CZE19HU" => "COUNTRY COVE",
  "157V3CZE16KZ" => "COUNTRY CLUB TERRACE",
  "157V3CZE13KN" => "COUNTRY CLUB SHOPPES",
  "157V3CZE10KJ" => "COUNTRY CLUB POINT CONDO",
  "157V3CZE0XN6" => "COUNTRY CLUB MANOR",
  "157V3CZE0SWP" => "COUNTRY CLUB HILLS RESUBD",
  "157V3CZE0UWG" => "COUNTRY CLUB HILLS",
  "157V3CZE0MUC" => "COUNTRY CLUB HEIGHTS 4TH ADDN",
  "157V3CZE0JOX" => "COUNTRY CLUB HEIGHTS 3RD ADDN",
  "157V3CZE0GOL" => "COUNTRY CLUB HEIGHTS 2ND ADDN",
  "157V3CZE0DN0" => "COUNTRY CLUB HEIGHTS 1ST ADDN",
  "157V3CZE0PRD" => "COUNTRY CLUB HEIGHTS",
  "157V3CZE0AEF" => "COUNTRY CLUB GARDENS SUBD",
  "157V3CZE07H4" => "COUNTRY CLUB ESTATES SUBD",
  "157V3CZE04MK" => "COUNTRY CLUB COLONY",
  "157V3CZDZYRE" => "COTTAGES OF MEADOWRIDGE",
  "157V3CZDZVW3" => "COSTA NORTE",
  "157V3CZDZT3R" => "COSTA DEL SOL CONDO PH XII",
  "157V3CZDZQ9D" => "COSTA DEL SOL CONDO PH XI",
  "157V3CZDZNLD" => "COSTA DEL SOL CONDO PH X",
  "157V3CZDZKIU" => "COSTA DEL SOL CONDO PH VIII",
  "157V3CZDZHFI" => "COSTA DEL SOL CONDO PH V VI VII",
  "157V3CZE56SP" => "CRESTHAVEN SATELLITE BEACH UNIT 2",
  "157V3CZE53TL" => "CRESTHAVEN SATELLITE BEACH UNIT 1",
  "157V3CZE50WJ" => "CRESTHAVEN HOMES SEC 2",
  "157V3CZE4XUT" => "CRESTHAVEN HOMES SEC 1",
  "157V3CZE4UMM" => "CRESCENT PALMS CONDO",
  "157V3CZE4OLH" => "CRESCENT BEACH CLUB CONDO",
  "157V3CZE4RL2" => "CRESCENT BEACH",
  "157V3CZE4M1S" => "CREELS S/D OF LOT 26 HOUSTONS ADD TO EAU GALLIE",
  "157V3CZE4IU8" => "CREEKWOOD",
  "157V3CZE4D3E" => "CRANES POINT AT AQUARINA PH 2",
  "157V3CZE4FPN" => "CRANES POINT AT AQUARINA",
  "157V3CZE4A2K" => "CRANE CREEK UNIT 2 PHASES 1 AND 2",
  "157V3CZE46L2" => "CRANE CREEK UNIT 2 PHASE 5",
  "157V3CZE442L" => "CRANE CREEK UNIT 2 PHASE 4",
  "157V3CZE40UI" => "CRANE CREEK UNIT 2 PHASE 3",
  "157V3CZE3XVE" => "CRANE CREEK UNIT 1",
  "157V3CZE3V5P" => "CRANE CREEK HGTS UNREC SUBD",
  "157V3CZE3S28" => "CRANE CLIFF",
  "157V3CZE3OQN" => "COVENTRY SUBDIVISION PHASE 3",
  "157V3CZE3M1N" => "COVENTRY SUBDIVISION PHASE 2",
  "157V3CZE3ISN" => "COVENTRY SUBDIVISION PHASE 1",
  "157V3CZE3FJN" => "COVENTRY OF COCOA A REPLAT OF",
  "157V3CZE39JX" => "COURTYARDS SUNTREE P.U.D. STAGE 5 TRACT 62 UNIT 2",
  "157V3CZE33DI" => "COURTYARDS SUNTREE P.U.D. STAGE 5 TR 62 UNIT 3 TH",
  "157V3CZE36HT" => "COURTYARDS SUNTREE P.U.D. STAGE 5 TR 62 UNIT 3",
  "157V3CZE3CM4" => "COURTYARDS SUNTREE P.U.D. STAGE 5",
  "157V3CZE306R" => "COURTYARDS REPLAT SUNTREE P.U.D. STAGE 5 TRACT 62",
  "157V3CZE2UO3" => "COURTYARDS AT SANDY PINES PRESERVE PHASE 2",
  "157V3CZE2XAO" => "COURTYARDS AT SANDY PINES PRESERVE PHASE 1",
  "157V3CZE2R58" => "COURTENAY SPRINGS P.U.D. STAGE TWO",
  "157V3CZE2O6Z" => "COURTENAY SPRINGS P.U.D",
  "157V3CZE2IIV" => "COUNTRY WOODS VILLAGE CONDO PH IV",
  "157V3CZE2FGU" => "COUNTRY WOODS VILLAGE CONDO PH III",
  "157V3CZE2CKP" => "COUNTRY WOODS VILLAGE CONDO PH II",
  "157V3CZE8GB2" => "CYPRESS TRACE SUNTREE PUD STAGE 4",
  "157V3CZE8D4R" => "CYPRESS SPRINGS CONDO",
  "157V3CZE87BQ" => "CYPRESS LAKE ESTATES UNIT 1",
  "157V3CZE841K" => "CYPRESS LAKE ESTATES ADDITION",
  "157V3CZE8ACF" => "CYPRESS LAKE ESTATES",
  "157V3CZE8159" => "CYPRESS CREEK SUBDIVISION PHASE TWO",
  "157V3CZE7Y2F" => "CYPRESS CREEK SUBDIVISION PHASE 1",
  "157V3CZE7RU2" => "CYPRESS COVE AT SUNTREE A CONDO",
  "157V3CZE7V27" => "CYPRESS COVE",
  "157V3CZE7MRC" => "CYPRESS BEND",
  "157V3CZE7GX0" => "CUYLERS SUBD",
  "157V3CZE7E2R" => "CRYSTAL OAKS SUBDIVISION",
  "157V3CZE7B5Y" => "CRYSTAL LAKES WEST",
  "157V3CZE77VU" => "CRYSTAL LAKES SUBD",
  "157V3CZE74PN" => "CRYSTAL LAKES REPLAT OF A PORTION OF",
  "157V3CZE71UB" => "CRYSTAL LAKE 2ND ADDN",
  "157V3CZE6YTZ" => "CRYSTAL LAKE 1ST ADD",
  "157V3CZE6SX4" => "CROWN HEIGHTS 2ND ADDN",
  "157V3CZE6PTE" => "CROWN HEIGHTS 1ST ADDN TO",
  "157V3CZE6W0A" => "CROWN HEIGHTS",
  "157V3CZE6JAA" => "CROTON WOODS PHASE TWO",
  "157V3CZE6MEM" => "CROTON WOODS",
  "157V3CZE6GKK" => "CROTON RIVER ESTATES",
  "157V3CZE6DN2" => "CROTON PINES",
  "157V3CZE6AJI" => "CROTON PARK UNIT 2",
  "157V3CZE67KM" => "CROTON PARK UNIT 1",
  "157V3CZE648Q" => "CROTON MEADOWS",
  "157V3CZE618T" => "CROTON MANOR",
  "157V3CZE5YBL" => "CROSSWAY BEACH APTS CONDO",
  "157V3CZE5VYE" => "CROSSROADS VILLAGE",
  "157V3CZE5SWR" => "CROSS CREEK PHASE TWO",
  "157V3CZE5P9S" => "CROSS CREEK PHASE ONE",
  "157V3CZE5LT9" => "CRISTAL CONDO PHASE 2",
  "157V3CZE5IQN" => "CRISTAL CONDO PHASE 1",
  "157V3CZE5CWP" => "CRESTVIEW HEIGHTS",
  "157V3CZE59YS" => "CRESTVIEW ACRES",
  "157V3CZE5FPO" => "CRESTVIEW",
  "157V3CZEBKPN" => "DE SOTO PARK UNIT 2",
  "157V3CZEBN9L" => "DE SOTO PARK",
  "157V3CZEBHN0" => "DE ANGELIS PARK UNIT 3",
  "157V3CZEBEII" => "DE ANGELIS PARK UNIT 2 SEC 2",
  "157V3CZEBBDT" => "DE ANGELIS PARK UNIT 2 SEC 1",
  "157V3CZEB5K8" => "DE ANGELIS PARK UNIT 1",
  "157V3CZEB32X" => "DAYSPRING CONDO PH II",
  "157V3CZEAZWW" => "DAYSPRING CONDO PH I",
  "157V3CZEAWP1" => "DAVIS SUBD OF HARDEES PLAT",
  "157V3CZEAR04" => "DAVIS COURT REPLAT",
  "157V3CZEAO0N" => "DAVIS COURT 2ND ADDN",
  "157V3CZEAKTK" => "DAVIS COURT 1ST ADDN",
  "157V3CZEATX7" => "DAVIS COURT",
  "157V3CZEAHQP" => "DAVIS ADDN TO MIMS SUBD OF",
  "157V3CZEAELV" => "DAVIDSONS UNRECORDED SUBD",
  "157V3CZEABLV" => "DAVIDSONS UNRECORDED RESUBD",
  "157V3CZEA8OW" => "DAVIDSON-BENEDICT CO",
  "157V3CZEA2SM" => "DAVIDSON AND HICKS SUBD OF LOT 2",
  "157V3CZEA5R3" => "DAVIDSON AND HICKS SUBD",
  "157V3CZE9ZWX" => "DAVIDSON AND HICKS RESUBD OF LOTS K L M N O P AND",
  "157V3CZE9XP3" => "DAVID SUBD",
  "157V3CZE9V62" => "DARTMOUTH TRAIL",
  "157V3CZE9S3S" => "DARDONVILLE SUBD",
  "157V3CZE9OQQ" => "DALTON SUBDIVISION",
  "157V3CZE9LX2" => "DALES ADDN TO GARDENDALE",
  "157V3CZE9FI1" => "DALEHURST RANCHES UNIT 3",
  "157V3CZE9CLF" => "DALEHURST RANCHES UNIT 2",
  "157V3CZE9ILJ" => "DALEHURST RANCHES",
  "157V3CZE99MQ" => "DALEHURST PINES",
  "157V3CZE93NR" => "DAINTREE SUBDIVISION",
  "157V3CZE914C" => "D N SEELYS REPLAT OF HARDEEVILLE",
  "157V3CZE8Y4X" => "D J C SUBDIVISION",
  "157V3CZE8S97" => "CYPRESS WOODS PHASE 5",
  "157V3CZE8P6V" => "CYPRESS WOODS PHASE 4",
  "157V3CZE8MGH" => "CYPRESS WOODS PHASE 3",
  "157V3CZE8JED" => "CYPRESS WOODS PHASE 2",
  "157V3CZE8V8U" => "CYPRESS WOODS",
  "157V3CZEEO3E" => "DIANA SHORES UNIT NO 4",
  "157V3CZEEL2J" => "DIANA SHORES UNIT NO 3B",
  "157V3CZEEIDL" => "DIANA SHORES UNIT NO 3A",
  "157V3CZEEFL8" => "DIANA SHORES UNIT NO 3",
  "157V3CZEED12" => "DIANA SHORES UNIT NO 2",
  "157V3CZEEAR1" => "DIANA SHORES UNIT NO 10",
  "157V3CZEE7NW" => "DIANA SHORES UNIT NO 1",
  "157V3CZEE2SU" => "DIAMOND BAY CONDO",
  "157V3CZEDYP0" => "DEWIE ACRES SUBD",
  "157V3CZEDVJX" => "DEVONS GLEN UNIT TWO",
  "157V3CZEDSN4" => "DEVONS GLEN UNIT THREE",
  "157V3CZEDPGS" => "DEVONS GLEN UNIT ONE",
  "157V3CZEDMO7" => "DESOTO CONDO",
  "157V3CZEDKG5" => "DERBYS ADDN TO COCOA",
  "157V3CZEDHD7" => "DEPOT PLAZA",
  "157V3CZEDEEC" => "DELL BROOK",
  "157V3CZED8EO" => "DELESPINE ON INDIAN RIVER RESUBD OF PART OF",
  "157V3CZEDBA0" => "DELESPINE ON INDIAN RIVER",
  "157V3CZED2OB" => "DELESPINE GRANT BOYES SUBD",
  "157V3CZED5P5" => "DELESPINE GRANT",
  "157V3CZECZT8" => "DELESPINE GARDEN REPLAT",
  "157V3CZECTXU" => "DELESPINE COURTS SEC 2",
  "157V3CZECQY9" => "DELESPINE COURTS REPLAT",
  "157V3CZECX2V" => "DELESPINE COURTS",
  "157V3CZECNV4" => "DELEON VILLAGE CONDOMINIUM",
  "157V3CZECKSS" => "DELANOYS ADDN TO COCOA",
  "157V3CZECENT" => "DEL REAL TOWN HOUSE VILLAS 1ST ADDN TO",
  "157V3CZECBCH" => "DEL REAL TOWN HOUSE VILLAS 1ST ADDN REPLAT OF PO",
  "157V3CZECHQK" => "DEL REAL TOWN HOUSE VILLAS",
  "157V3CZEC8J7" => "DEHART CONDO",
  "157V3CZEC5NM" => "DEER RUN",
  "157V3CZEC2S1" => "DEER LAKES PHASE 3",
  "157V3CZEBZN4" => "DEER LAKES PHASE 2",
  "157V3CZEBWWG" => "DEER LAKES PHASE 1-A A REPLAT OF TRACT 115/116 OF",
  "157V3CZEBTEY" => "DEER LAKES PHASE 1",
  "157V3CZEBQ7U" => "DEATHERAGE SUBD",
  "157V3CZEHZA6" => "E 1/2 AND E 1/2 OF NW 1/4 EX",
  "157V3CZEKKYV" => "E 1/2",
  "157V3CZEHSYY" => "DURAN GOLF CLUB A PORTION OF PARCEL 4 - VIERA CENT",
  "157V3CZEHPRS" => "DURAN EAST",
  "157V3CZEHMSE" => "DUNLAP ACRES",
  "157V3CZEHJWH" => "DUNE CREST SUBD",
  "157V3CZEHHF0" => "DRUID HILLS SUBD",
  "157V3CZEHE9F" => "DRIFTWOOD VILLAS OF COCOA BEACH CONDO",
  "157V3CZEHB0O" => "DRIFTWOOD PLAZA PHASE II",
  "157V3CZEH8GE" => "DRIFTWOOD BEACH CONDO",
  "157V3CZEH5E2" => "DRAZICH SUBDIVISION",
  "157V3CZEH1WW" => "DRAZICH ANNEX",
  "157V3CZEGYXI" => "DRAKES ADDN TO MALABAR",
  "157V3CZEGVUP" => "DR I M HAY RESUBD",
  "157V3CZEGO2Z" => "DOWNTOWN TRAILER HAVEN REPLAT",
  "157V3CZEGSQY" => "DOWNTOWN TRAILER HAVEN",
  "157V3CZEGL0N" => "DOW ROAD INDUSTRIAL PARK CONDO PH 1",
  "157V3CZEGIBH" => "DOW CENTRAL PARK",
  "157V3CZEGF9H" => "DOUGLAS SUBD OF PT OF LOT 2",
  "157V3CZEGBVW" => "DOLPHINS COVE",
  "157V3CZEG924" => "DOLPHIN CLUB CONDO",
  "157V3CZEG5U2" => "DOLPHIN BEACH APTS CONDO",
  "157V3CZEG0VP" => "DOCK SIDE 31 CONDO",
  "157V3CZEFXS9" => "DIXONS SUBD",
  "157V3CZEFUN4" => "DIXONS ADDN TO COCOA",
  "157V3CZEFOB8" => "DIXIE VILLAGE NO 2",
  "157V3CZEFRE0" => "DIXIE VILLAGE",
  "157V3CZEFLB2" => "DIXIE HEIGHTS",
  "157V3CZEFIRL" => "DISCOVERY BEACH RESORT AND TENNIS CLUB TIME SHARE",
  "157V3CZEFC5R" => "DISCOVERY BEACH RESORT AND TENNIS CLUB CONDO",
  "157V3CZEF98H" => "DISCOVERY BAY",
  "157V3CZEF69T" => "DICKSONS SUBD",
  "157V3CZEF35J" => "DIANA SHORES UNIT NO 9",
  "157V3CZEF06Z" => "DIANA SHORES UNIT NO 8",
  "157V3CZEEX2Z" => "DIANA SHORES UNIT NO 7",
  "157V3CZEEU4F" => "DIANA SHORES UNIT NO 6",
  "157V3CZEER4B" => "DIANA SHORES UNIT NO 5",
  "157V3CZEKXQR" => "E 1/4 OF NE 1/4 PAR 5 13",
  "157V3CZEKUHI" => "E 1/4 OF NE 1/4 OF NE 1/4 EX N",
  "157V3CZEKR71" => "E 1/4 OF NE 1/4 N OF CO RD AND W OF",
  "157V3CZEKNZ9" => "E 1/4 OF NE 1/4 AND E 296.43 FT OF",
  "157V3CZEHW8Y" => "E 1/2 SE 1/4 OF NW 1/4",
  "157V3CZEKHXV" => "E 1/2 PAR 750",
  "157V3CZEKF7S" => "E 1/2 OF SW 1/4 OF SW 1/4 EX ORB",
  "157V3CZEKC1E" => "E 1/2 OF NW 1/4 OF NE 1/4 EX",
  "157V3CZEK92Y" => "E 1/2 OF NW 1/4 LYING W OF F E C",
  "157V3CZEJXJ3" => "E 1/2 OF NE 1/4 OF NE 1/4 OF NE",
  "157V3CZEK01D" => "E 1/2 OF NE 1/4 OF NE 1/4 OF",
  "157V3CZEJU80" => "E 1/2 OF NE 1/4 OF NE 1/4 EX W",
  "157V3CZEK363" => "E 1/2 OF NE 1/4 OF NE 1/4",
  "157V3CZEJQYQ" => "E 1/2 OF NE 1/4 EX RD R/W",
  "157V3CZEJOIO" => "E 1/2 OF NE 1/4 EX CANAL AND ROAD",
  "157V3CZEJI5A" => "E 1/2 OF NE 1/4 E 1/2 OF SW 1/4",
  "157V3CZEJLJZ" => "E 1/2 OF NE 1/4 AND NW 1/4 OF",
  "157V3CZEK6BM" => "E 1/2 OF NE 1/4",
  "157V3CZEJF3N" => "E 1/2 OF N 1/4 OF SE 1/4 OF NE",
  "157V3CZEJBZX" => "E 1/2 OF LOT 32 SUBD",
  "157V3CZEJ6LP" => "E 1/2 OF LOT 31 ALL LOT 32 SUBD",
  "157V3CZEJ935" => "E 1/2 OF LOT 31 ALL LOT 32",
  "157V3CZEJ3MF" => "E 1/2 OF E 1/2 OF NE 1/4 OF",
  "157V3CZEJ0NV" => "E 1/2 N OF STATE RD 520",
  "157V3CZEIY6Y" => "E 1/2 N OF ST RD 520",
  "157V3CZEIUP9" => "E 1/2 N OF GRANT LINE",
  "157V3CZEIRAA" => "E 1/2 LYING N OF CHENEY HWY",
  "157V3CZEIOAD" => "E 1/2 EXC LANDS DEEDED THROUGH",
  "157V3CZEIL5E" => "E 1/2 EX S 1/2 OF SE 1/4 OF",
  "157V3CZEIDGU" => "E 1/2 EX REC TAX DIST R/W ORB",
  "157V3CZEII22" => "E 1/2 EX REC TAX DIST",
  "157V3CZEIAFC" => "E 1/2 EX RD AND CANAL R/W",
  "157V3CZEI7WJ" => "E 1/2 EX N 1/2 OF S 1/2 OF",
  "157V3CZEI4WZ" => "E 1/2 AND SW 1/4 PAR 500 750",
  "157V3CZEI209" => "E 1/2 AND NW 1/4 PAR 250 750",
  "157V3CZEOIUD" => "EAST GREEN CONDO",
  "157V3CZEOFBT" => "EAST BAY PLANTATION PHASE II",
  "157V3CZEOBWZ" => "EAST BAY PLANTATION PHASE 1 REPLAT",
  "157V3CZEO7ST" => "EASON LANDING SUBDIVISION",
  "157V3CZEO4OL" => "EAGLES NEST PHASE II",
  "157V3CZEO1V5" => "EAGLES NEST PHASE I",
  "157V3CZENYPK" => "EAGLES NEST CONDO",
  "157V3CZENDJ7" => "EAGLES LANDING REPLAT OF",
  "157V3CZENSZZ" => "EAGLES LANDING AT SUNTREE 3RD ADDN TO",
  "157V3CZENPFL" => "EAGLES LANDING AT SUNTREE 2ND ADDN TO",
  "157V3CZENIEL" => "EAGLES LANDING AT SUNTREE 1ST ADDN TO",
  "157V3CZENVPH" => "EAGLES LANDING AT SUNTREE",
  "157V3CZEN9I4" => "EAGLE POINTE SUBDIVISION",
  "157V3CZEMYXN" => "EAGLE LAKE TWO PHASE TWO",
  "157V3CZEN2AS" => "EAGLE LAKE TWO",
  "157V3CZEMWGT" => "EAGLE LAKE NORTH",
  "157V3CZEMSZH" => "EAGLE LAKE EAST PHASE 2",
  "157V3CZEMPEQ" => "EAGLE LAKE EAST PHASE 1",
  "157V3CZEN5WO" => "EAGLE LAKE",
  "157V3CZEMJ3S" => "EAGLE HARBOR",
  "157V3CZEMFDN" => "E H RICES ADDN TO TITUSVILLE",
  "157V3CZEMBSJ" => "E E RODES SUBD OF LOTS 1 2 BLK 11",
  "157V3CZEM86V" => "E DAY COS SUBD",
  "157V3CZEM39A" => "E C JOHNSONS SUBD",
  "157V3CZELZWY" => "E 594 FT OF E 1/2 OF NE 1/4 EX",
  "157V3CZELWKC" => "E 50 FT OF MAIN ST S OF HESSEY",
  "157V3CZELTAL" => "E 50 ACRES OF NE 1/4",
  "157V3CZELPXG" => "E 340 FT OF N 3/4 OF NE 1/4 OF",
  "157V3CZELMN7" => "E 30 FT OF S 1/4 OF NE 1/4 OF",
  "157V3CZELJC2" => "E 3/4 OF E 1/2 N OF GRANT LINE",
  "157V3CZELGH1" => "E 3/4 OF E 1/2 EX I-95 R/W",
  "157V3CZELCWW" => "E 1980 FT OF N 1980 FT",
  "157V3CZEL9F3" => "E 150 FT OF N 170 FT OF NE 1/4",
  "157V3CZEL6DD" => "E 1328.08 FT OF N 1/2 OF NE 1/4",
  "157V3CZEL2YA" => "E 123 AC OF NE 1/4 EX ROAD R/W",
  "157V3CZEL049" => "E 10 AC OF LOT 1",
  "157V3CZERIHG" => "EGRET TRACE CONDO",
  "157V3CZERF6K" => "EDWARDS RESUBD OF HENDRYS ADDN TO COCOA",
  "157V3CZERC17" => "EDWARDS FORREST PARK",
  "157V3CZER9N4" => "EDWARDS AND CANFIELD HIGHLAND PK SD",
  "157V3CZER6YY" => "EDGEWOOD MANOR",
  "157V3CZER3VH" => "EDGEWOOD GLEN",
  "157V3CZER1DS" => "EDGEWATER LANDINGS",
  "157V3CZEQXZX" => "EDEN ESTATES",
  "157V3CZEQURI" => "EBB TIDE CONDO APTS",
  "157V3CZEQS21" => "EBB TIDE AT MELBOURNE BCH CONDO",
  "157V3CZEQOL3" => "EAU GALLIE-CROTON PROFESSIONAL CENTER CONDO",
  "157V3CZEPOCE" => "EAU GALLIE VILLAGE PLAT OF",
  "157V3CZEPL6N" => "EAU GALLIE SUBD OF OUT LOTS 21 22 23",
  "157V3CZEQLHZ" => "EAU GALLIE SHORES",
  "157V3CZEPIGI" => "EAU GALLIE REPT OF PT OF PLAT OF",
  "157V3CZEQIT1" => "EAU GALLIE OFFICE CONDO",
  "157V3CZEQFKG" => "EAU GALLIE MOSSWOOD PROFESSIONAL PARK CONDO",
  "157V3CZEQ6JT" => "EAU GALLIE HARBOUR CLUB CONDO PH II A",
  "157V3CZEQ9HE" => "EAU GALLIE HARBOUR CLUB CONDO PH II",
  "157V3CZEQCCR" => "EAU GALLIE HARBOUR CLUB CONDO",
  "157V3CZEQ0LS" => "EAU GALLIE GOLF MANORS REPLAT OF",
  "157V3CZEQ3AV" => "EAU GALLIE GOLF MANORS",
  "157V3CZEPUC6" => "EAU GALLIE GARDENS PHASE 1 REPLAT OF PART OF",
  "157V3CZEPXK0" => "EAU GALLIE GARDENS",
  "157V3CZEPRE7" => "EAU GALLIE BY THE SEA",
  "157V3CZEPF8S" => "EASTWOOD PARK",
  "157V3CZEPC5U" => "EASTWOOD ONE AT HERITAGE OAKS",
  "157V3CZEP9M1" => "EASTWOOD ESTATES",
  "157V3CZEP66A" => "EASTWIND CONDO PH II",
  "157V3CZEP37O" => "EASTWIND CONDO PH I",
  "157V3CZEOXFM" => "EAST WINDS 3",
  "157V3CZEOULC" => "EAST WINDS 2",
  "157V3CZEP0JN" => "EAST WINDS",
  "157V3CZEOS82" => "EAST MAGNOLIA PARK",
  "157V3CZEOMD0" => "EAST HILLS UNIT 2",
  "157V3CZEOP3N" => "EAST HILLS",
  "157V3CZEUOF7" => "EXECUTIVE WEST OFFICE CONDO",
  "157V3CZEULFK" => "EXECUTIVE PARK A REPLAT OF TRACT K PORT MALABAR UN",
  "157V3CZEUGJU" => "EVERGREEN II",
  "157V3CZEUDBS" => "EVERGREEN ESTATES",
  "157V3CZEUIUT" => "EVERGREEN",
  "157V3CZEUA65" => "ESSEX HOUSE CONDO",
  "157V3CZEU6UC" => "ESSENTIALS SPA PARK CONDO",
  "157V3CZEU3ZS" => "ENTERPRISE PARK",
  "157V3CZEU14F" => "ENGLISH PARK CONDO",
  "157V3CZETX44" => "ENCLAVE SUBD",
  "157V3CZETSUT" => "ENCLAVE AT THREE HUNDRED",
  "157V3CZETQCS" => "EMMAUS HEIGHTS",
  "157V3CZETNB9" => "EMMAS COVE",
  "157V3CZETK4P" => "EMERALD SHORES A CONDO",
  "157V3CZETH9N" => "EMERALD SEAS",
  "157V3CZETEAS" => "EMERALD POINTE",
  "157V3CZETBCO" => "EMERALD PARK CONDO",
  "157V3CZET906" => "EMERALD LAKES P.U.D.",
  "157V3CZET5YV" => "EMERALD ISLES PHASE 2",
  "157V3CZET251" => "EMERALD ISLES PHASE 1",
  "157V3CZESZYC" => "EMERALD HILLS UNIT TWO",
  "157V3CZESWPI" => "EMERALD HILLS UNIT THREE",
  "157V3CZESTAC" => "EMERALD HILLS UNIT ONE",
  "157V3CZESQOW" => "EMERALD HILLS UNIT FOUR",
  "157V3CZESNEK" => "EMERALD HILLS UNIT FIVE",
  "157V3CZESHQD" => "EMERALD HARBOR CONDO PH II",
  "157V3CZESEH4" => "EMERALD HARBOR CONDO PH I",
  "157V3CZESKHQ" => "EMERALD HARBOR",
  "157V3CZESBKL" => "EMBASSY ARMS COURTS SUBD",
  "157V3CZES98Q" => "ELTON MANOR CONDO",
  "157V3CZES5X8" => "ELTON HALL GDNS RESUBD OF PT OF",
  "157V3CZES2E6" => "ELLMORES SUBD",
  "157V3CZERZU3" => "ELLIS ROAD INDUSTRIAL PARK",
  "157V3CZERWPW" => "ELCON ESTATES",
  "157V3CZERTL0" => "EL PUEBLO BONITO",
  "157V3CZEROG0" => "EL DORADO CONDO",
  "157V3CZERL3A" => "EGRETS COVE",
  "157V3CZEY5Y0" => "FAITH LANDINGS",
  "157V3CZEY2N1" => "FAIRWAYS OF BREV. CONDO PLAN NO 1",
  "157V3CZEXZEV" => "FAIRWAYS FOUR CONDO PH III",
  "157V3CZEXWMG" => "FAIRWAYS FOUR CONDO PH II",
  "157V3CZEXTGR" => "FAIRWAYS FOUR CONDO PH I",
  "157V3CZEXQIV" => "FAIRWAYS EDGE P.U.D.",
  "157V3CZEXNTR" => "FAIRWAYS AT ROYAL OAK",
  "157V3CZEXKKO" => "FAIRWAY WOODS I CONDO PH I",
  "157V3CZEXHMD" => "FAIRWAY TOWNHOMES",
  "157V3CZEXETI" => "FAIRWAY PARK SUBD",
  "157V3CZEXB2E" => "FAIRWAY OAKS CONDO",
  "157V3CZEX894" => "FAIRWAY LAKES AT VIERA PHASE I",
  "157V3CZEX5HN" => "FAIRWAY LAKES AT VIERA PHASE 3",
  "157V3CZEX2JN" => "FAIRWAY LAKES AT VIERA PHASE 2",
  "157V3CZEWZDT" => "FAIRWAY ISLES AT BAYSIDE LAKES PHASE I",
  "157V3CZEWW8B" => "FAIRWAY ISLES AT BAYSIDE LAKES PHASE 3",
  "157V3CZEWOBF" => "FAIRWAY ISLES AT BAYSIDE LAKES PHASE 2",
  "157V3CZEWH3S" => "FAIRWAY ESTATES 3RD ADDN",
  "157V3CZEWCHV" => "FAIRWAY ESTATES 2ND ADDN",
  "157V3CZEW9FE" => "FAIRWAY ESTATES 1ST ADDN",
  "157V3CZEWK4E" => "FAIRWAY ESTATES",
  "157V3CZEW6MO" => "FAIRWAY CROSSINGS TOWNHOMES AT BAYSIDE LAKES",
  "157V3CZEW3NJ" => "FAIRWAY CROSSINGS AT BAYSIDE LAKES",
  "157V3CZEW0G6" => "FAIRVIEW RESUBD",
  "157V3CZEVX51" => "FAIRVIEW LAWN",
  "157V3CZEVU8D" => "FAIRVIEW ESTATES PHASE 2",
  "157V3CZEVQI2" => "FAIRVIEW ESTATES PHASE 1",
  "157V3CZEVL5O" => "FAIRMONT",
  "157V3CZEVDUC" => "FAIRHAVEN SUBD SECOND ADD TO",
  "157V3CZEV8CE" => "FAIRHAVEN SUBD FIRST ADD TO",
  "157V3CZEVGT1" => "FAIRHAVEN SUBD",
  "157V3CZEV5H4" => "FAIRGLEN SUBD",
  "157V3CZEV2IN" => "FAIRFIELD SUBD",
  "157V3CZEUXCL" => "FAIRFAX SUBD",
  "157V3CZEURC3" => "F C POWELLS SUBD OF ALL THAT PT OF LOT 4 W OF RR",
  "157V3CZEUU96" => "F C POWELLS SUBD",
  "157V3CZF17MK" => "FISKE TERRACE UNIT 6",
  "157V3CZF14UB" => "FISKE TERRACE UNIT 5",
  "157V3CZF11WE" => "FISKE TERRACE UNIT 4",
  "157V3CZF0YSP" => "FISKE TERRACE UNIT 3",
  "157V3CZF0VX9" => "FISKE TERRACE UNIT 2",
  "157V3CZF0SMD" => "FISKE TERRACE UNIT 1",
  "157V3CZF0PML" => "FISKE LAKE ESTATES UNIT NO 1",
  "157V3CZF0MHN" => "FISHERS ADDN TO TITUSVILLE",
  "157V3CZF0JOO" => "FISCHERS HAMMOCK PHASE V",
  "157V3CZF0GU0" => "FISCHERS HAMMOCK PHASE IV SUBDIVISION",
  "157V3CZF0E4H" => "FISCHERS HAMMOCK PHASE II SUBD",
  "157V3CZF0ASG" => "FISCHERS HAMMOCK PHASE 1 SUBDIVISION",
  "157V3CZF07S2" => "FIRST STREET CENTER CONDO",
  "157V3CZF04W1" => "FIRST AND BEST PROPERTIES CONDO",
  "157V3CZF01ZQ" => "FINAN SUBD",
  "157V3CZEZYUD" => "FIFTH AVENUE PLAZA CONDO",
  "157V3CZEZT0X" => "FIELDSTONE SUNTREE PUD ST 4 TR 41 UNIT 1",
  "157V3CZEZPTS" => "FERNWOOD ESTATES",
  "157V3CZEZK9M" => "FERN MEADOWS PHASE TWO",
  "157V3CZEZN8T" => "FERN MEADOWS",
  "157V3CZEZH0D" => "FENNER WOODS",
  "157V3CZEZE4K" => "FELLWOOD ESTATES",
  "157V3CZEZBAG" => "FEES UNREC SUBD",
  "157V3CZEZ8DJ" => "FEATHER LAKES",
  "157V3CZEZ4TX" => "FEAST GROVES",
  "157V3CZEZ1VH" => "FAYS RESUBD OF LOTS 49 TO 52 OF CASEYS SUNNYSIDE A",
  "157V3CZEYZ0K" => "FAYS FIRST ADDN TO COCOA",
  "157V3CZEYWCT" => "FAWN LAKE PUD PHASE I UNIT ONE",
  "157V3CZEYT72" => "FAWN LAKE PUD PHASE 2 UNIT THREE",
  "157V3CZEYQGK" => "FAWN LAKE PUD PHASE 1 UNIT TWO",
  "157V3CZEYO6M" => "FAWN COVE",
  "157V3CZEYH9Q" => "FARVIEW SUBD REPLAT OF PT OF",
  "157V3CZEYKDR" => "FARVIEW",
  "157V3CZEYEJ4" => "FANNINGS SUBD OF S 1/2 OF SW 1/4 SEC 30 AND N 1/2",
  "157V3CZEYBBJ" => "FALCON RIDGE",
  "157V3CZEY8GM" => "FAITH LUTHERAN CHURCH",
  "157V3CZF4ALO" => "FLUER DE LIS CONDO SEC I",
  "157V3CZF44DC" => "FLOYDS SUBD 1ST ADDN",
  "157V3CZF473Z" => "FLOYDS SUBD",
  "157V3CZF3YZ2" => "FLORIDISE RESUBD",
  "157V3CZF422U" => "FLORIDISE",
  "157V3CZF3W2F" => "FLORIDANA BEACH SUBD",
  "157V3CZF3TFP" => "FLORIDANA BEACH CONDO",
  "157V3CZF3PZZ" => "FLORIDANA BEACH 5TH ADDN",
  "157V3CZF3N5J" => "FLORIDANA BEACH 4TH ADD",
  "157V3CZF3K4F" => "FLORIDANA BEACH 3RD ADDN",
  "157V3CZF3H91" => "FLORIDANA BEACH 2ND ADD",
  "157V3CZF3E8Q" => "FLORIDANA BEACH 1ST ADDN",
  "157V3CZF3BWU" => "FLORIDA PALMS",
  "157V3CZF32DG" => "FLORIDA INDIAN RIVER LAND CO SUBD PER",
  "157V3CZF355X" => "FLORIDA INDIAN RIVER LAND CO SUBD",
  "157V3CZF38LY" => "FLORIDA INDIAN RIVER LAND CO",
  "157V3CZF2ZF0" => "FLORIDA GARDEN CONDOMINIUMS",
  "157V3CZF2W52" => "FLORES OCEAN SUITES CONDO",
  "157V3CZF2T5K" => "FLORES DEL MAR CONDO",
  "157V3CZF2Q5B" => "FLORES DE LA PLAYA CONDO",
  "157V3CZF2N7S" => "FLORAL PARK SEC 3",
  "157V3CZF2K7B" => "FLORAL PARK SEC 2",
  "157V3CZF2G6T" => "FLORAL PARK SEC 1",
  "157V3CZF2DMJ" => "FLORADORA PINES RESUBD OF",
  "157V3CZF2ABH" => "FLORA VISTA A REPLAT OF PART OF TRACT A PORT ST",
  "157V3CZF26YX" => "FLORA BEACH SUBD SEC A",
  "157V3CZF248L" => "FLORA AND FAUNA ESTATES",
  "157V3CZF1XWN" => "FLEMING GRANT SD OF D CAINS HOMESTEAD GRANT SEC",
  "157V3CZF211W" => "FLEMING GRANT",
  "157V3CZF1V3H" => "FLAMINGO HOMES SEC D",
  "157V3CZF1RVS" => "FLAMINGO HOMES SEC C",
  "157V3CZF1OVX" => "FLAMINGO HOMES SEC B",
  "157V3CZF1M1T" => "FLAMINGO HOMES SEC A",
  "157V3CZF1J2O" => "FLAMINGO HOMES REPLAT OF PT OF",
  "157V3CZF1DXN" => "FLAMINGO HEIGHTS",
  "157V3CZF1ALX" => "FITZROY REEF AT WALKABOUT",
  "157V3CZF7MEK" => "FOUNTAINHEAD UNIT 5",
  "157V3CZF7JBK" => "FOUNTAINHEAD UNIT 4",
  "157V3CZF7GI8" => "FOUNTAINHEAD UNIT 3",
  "157V3CZF7DCG" => "FOUNTAINHEAD UNIT 2",
  "157V3CZF7ACG" => "FOUNTAINHEAD UNIT 1",
  "157V3CZF770G" => "FOUNTAIN COVE CONDO PH III",
  "157V3CZF7431" => "FOUNTAIN COVE CONDO PH II",
  "157V3CZF71SE" => "FOUNTAIN COVE CONDO PH I",
  "157V3CZF6Z2P" => "FOUNDATION COMMERCE CENTER",
  "157V3CZF6W1Q" => "FORTUNE COOKIE PARK",
  "157V3CZF6T2G" => "FORTENBERRY PROPERTIES PER SURVEY BOOK 1 PG 90",
  "157V3CZF6QLD" => "FORTENBERRY PROFESSIONAL BLDG CONDO",
  "157V3CZF6NED" => "FORTENBERRY PLACE",
  "157V3CZF6JXK" => "FORREST AVE CONDO",
  "157V3CZF6H9P" => "FOREST VILLAS TOWNHOMES",
  "157V3CZF6ED5" => "FOREST RIDGE",
  "15A2INRCF2UJ" => "FOREST LAKES OF COCOA CONDO PH I",
  "157V3CZF6B5F" => "FOREST LAKE VILLAGE UNIT TWO",
  "157V3CZF6897" => "FOREST LAKE VILLAGE UNIT THREE",
  "157V3CZF655N" => "FOREST LAKE VILLAGE UNIT ONE",
  "157V3CZF5YZI" => "FOREST ISLES SOUTH",
  "157V3CZF6204" => "FOREST ISLES",
  "157V3CZF5T1K" => "FOREST HILLS ESTATES 1ST ADDN",
  "157V3CZF5VW7" => "FOREST HILLS ESTATES",
  "157V3CZF5Q4G" => "FOREST HILL SUBD",
  "157V3CZF5K37" => "FOREST GLEN SUBD 1ST ADDN",
  "157V3CZF5N6E" => "FOREST GLEN SUBD",
  "157V3CZF5E7I" => "FOREST GLEN AT BAYSIDE LAKES PHASE 2",
  "157V3CZF5H5P" => "FOREST GLEN AT BAYSIDE LAKES",
  "157V3CZF5BAX" => "FOREST CREEK SUBDIVISION PHASE I",
  "157V3CZF4UVY" => "FOREST CREEK SUBDIVISION PHASE 2",
  "157V3CZF4SCX" => "FOREST CREEK PROFESSIONAL CENTER",
  "157V3CZF4P07" => "FOREST AT LA CITA THE",
  "157V3CZF4LZK" => "FOOTMAN LANDING",
  "157V3CZF4G68" => "FONTAINE GRANT RESUBD OF",
  "157V3CZF4D65" => "FLUER DE LIS CONDO SEC II",
  "157V3CZFB59K" => "GARDEN BEACH",
  "157V3CZFB2G1" => "GARDEN BAY CONDOMINIUM",
  "157V3CZFAZBR" => "GARDEN BAY COLONY",
  "157V3CZFAVWF" => "GANESH SUBDIVISION",
  "157V3CZFATD2" => "GALZERANO TOWNHOMES A REPLAT OF LOT 13 BLK 91 AVON",
  "157V3CZFAOSK" => "GAE RETREAT SUBD",
  "157V3CZFAGZ5" => "GABRIEL BY THE SEA",
  "157V3CZFA8IQ" => "G H FLEISCHUTS UNREC SUBD",
  "157V3CZFA4Q9" => "FRIDAY ACRES",
  "157V3CZFA1J9" => "FRANKLAND PARK",
  "157V3CZF9XTH" => "FRANK J KRAJIC SUBD",
  "157V3CZF9TH6" => "FRACTIONAL SECTIONS 34 AND 35",
  "157V3CZF9Q6I" => "FRACTIONAL SECTION",
  "157V3CZF9K8F" => "FOXHALL PHASE TWO",
  "157V3CZF9GG6" => "FOXHALL PHASE THREE",
  "157V3CZF9NAU" => "FOXHALL",
  "157V3CZF9DA3" => "FOXGREEN MANOR CONDO",
  "157V3CZF9AH3" => "FOX TRAIL RANCHES",
  "157V3CZF971X" => "FOX LAKE MANOR UNIT NO 1",
  "157V3CZF93XP" => "FOX LAKE MANOR SEC 5",
  "157V3CZF9126" => "FOX LAKE MANOR SEC 4",
  "157V3CZF8XZE" => "FOX LAKE MANOR SEC 3",
  "157V3CZF8V04" => "FOX LAKE MANOR SEC 2",
  "157V3CZF8SNG" => "FOX HOLLOW",
  "157V3CZF8PJY" => "FOX BAY PHASE ONE",
  "157V3CZF8M0V" => "FOX BAY FIRST ADDITION",
  "157V3CZF8IY3" => "FOWLERS PLAT",
  "157V3CZF8FSA" => "FOWLERS HORSE HAVEN",
  "157V3CZF8CU2" => "FOUR WINDS OF FLORIDA A CONDOMINIUM",
  "157V3CZF8A4P" => "FOUR OAKS ESTATES",
  "157V3CZF86YG" => "FOUR ACRES SUBD",
  "157V3CZF83KA" => "FOUNTAINS UNIT 4 THE",
  "157V3CZF80ZI" => "FOUNTAINS UNIT 3 THE",
  "157V3CZF7UUO" => "FOUNTAINS UNIT 2 THE",
  "157V3CZF7XSI" => "FOUNTAINS UNIT 2 REPLAT THE",
  "157V3CZF7SCN" => "FOUNTAINS UNIT 1 THE",
  "157V3CZF7PC9" => "FOUNTAINHEAD UNIT 6",
  "157V3CZFE8D2" => "GARDENDALE UNIT 6",
  "157V3CZFE50V" => "GARDENDALE UNIT 5A",
  "157V3CZFE1LC" => "GARDENDALE UNIT 5",
  "157V3CZFDY7W" => "GARDENDALE UNIT 4",
  "157V3CZFDUU8" => "GARDENDALE UNIT 3",
  "157V3CZFDS2P" => "GARDENDALE UNIT 2",
  "157V3CZFDOUX" => "GARDENDALE UNIT 10",
  "157V3CZFDLS2" => "GARDENDALE UNIT 1",
  "157V3CZFDJ0C" => "GARDENDALE SUBD",
  "157V3CZFDFEA" => "GARDEN TERRACE",
  "157V3CZFDCLY" => "GARDEN STREET OFFICE CONDO",
  "157V3CZFD9TX" => "GARDEN STREET ASSOC SUBDIVISION REPLAT",
  "157V3CZFD3BF" => "GARDEN SQUARE REPLAT II",
  "157V3CZFD6FT" => "GARDEN SQUARE REPLAT",
  "157V3CZFD098" => "GARDEN SQUARE CONDO PH XVII",
  "157V3CZFCX6U" => "GARDEN SQUARE CONDO PH XVI",
  "157V3CZFCU6I" => "GARDEN SQUARE CONDO PH XV",
  "157V3CZFCQYX" => "GARDEN SQUARE CONDO PH XIV",
  "157V3CZFCO31" => "GARDEN SQUARE CONDO PH XIII",
  "157V3CZFCL7W" => "GARDEN SQUARE CONDO PH XI",
  "157V3CZFCIAE" => "GARDEN SQUARE CONDO PH X",
  "157V3CZFCFEY" => "GARDEN SQUARE CONDO PH VIII",
  "157V3CZFCC7Q" => "GARDEN SQUARE CONDO PH VII",
  "157V3CZFC96W" => "GARDEN SQUARE CONDO PH VI",
  "157V3CZFC6R8" => "GARDEN SQUARE CONDO PH V",
  "157V3CZFC3EL" => "GARDEN SQUARE CONDO PH IX",
  "157V3CZFC06T" => "GARDEN SQUARE CONDO PH IV",
  "157V3CZFBX9U" => "GARDEN SQUARE CONDO PH III",
  "157V3CZFBUIC" => "GARDEN SQUARE CONDO PH II",
  "157V3CZFBS8O" => "GARDEN SQUARE CONDO PH I",
  "157V3CZFBP4Y" => "GARDEN SQUARE CONDO PH 12-B",
  "157V3CZFBM10" => "GARDEN SQUARE CONDO PH 12-A",
  "157V3CZFBJ2F" => "GARDEN PARK REPLAT",
  "157V3CZFBFYW" => "GARDEN HEIGHTS",
  "157V3CZFBC9R" => "GARDEN ESTATES REPLAT",
  "157V3CZFB8FZ" => "GARDEN BY THE SEA CONDOMINIUM",
  "157V3CZFHXH8" => "GLEN ISLES SEC 1",
  "157V3CZFHUSY" => "GLEN HAVEN SUBD SEC 2C",
  "157V3CZFHRAK" => "GLEN HAVEN SUBD SEC 2B",
  "157V3CZFHO4N" => "GLEN HAVEN SUBD SEC 2A",
  "157V3CZFHL88" => "GLEN HAVEN SUBD SEC 2",
  "157V3CZFHHQY" => "GLEN HAVEN SUBD SEC 1A",
  "157V3CZFHE2U" => "GLEN HAVEN SUBD SEC 1",
  "157V3CZFHAA2" => "GLEN HAVEN PARTIAL REPLAT OF SEC 2-C",
  "157V3CZFH6QZ" => "GLEASONS REPLAT OF A PORTION OF THE TOWN OF EAU GA",
  "157V3CZFGUU4" => "GLEASONS ADDN TO TITUSVILLE RESUBD",
  "157V3CZFH23D" => "GLEASONS ADDN TO TITUSVILLE",
  "157V3CZFGRJQ" => "GIZELLA TOWNHOMES PHASE 4",
  "157V3CZFGOD0" => "GIZELLA TOWNHOMES PHASE 3",
  "157V3CZFGLL0" => "GIZELLA TOWNHOMES PHASE 2",
  "157V3CZFGIA1" => "GIZELLA TOWNHOMES PHASE 1",
  "157V3CZFGEKJ" => "GIROUX SUBD",
  "157V3CZFGB4N" => "GINGRAS MANOR",
  "157V3CZFG7WY" => "GINA MARIE CONDO",
  "157V3CZFG56W" => "GIBSON PARK SEC C",
  "157V3CZFG1OP" => "GIBSON PARK SEC B",
  "157V3CZFFY4O" => "GIBSON PARK SEC A",
  "157V3CZFFU9J" => "GIBSON HEIGHTS",
  "157V3CZFFQY1" => "GEORGIANA WOODS",
  "157V3CZFFNZU" => "GEORGIANA SETTLEMENT",
  "157V3CZFFKI0" => "GEORGIANA GROVES",
  "157V3CZFFH1L" => "GEORGIA TERRACE",
  "157V3CZFF98U" => "GEMS WEST CONDO",
  "157V3CZFF5HM" => "GATWICK MANOR SUBDIVISION",
  "157V3CZFF20N" => "GATEWAY UNIT NO 1",
  "157V3CZFEZBN" => "GATEWAY UNIT 3",
  "157V3CZFEVPD" => "GATEWAY UNIT 2 REVISED",
  "157V3CZFESEZ" => "GARVEYS SUBD",
  "157V3CZFEPTW" => "GARRETTS RUN CONDO",
  "157V3CZFEMI3" => "GARNER ACRES II",
  "157V3CZFEIZD" => "GARLAND REYNOLDS SUBD",
  "157V3CZFEFBD" => "GARDENIA OCEANFRONT",
  "157V3CZFEC2U" => "GARDENDALE UNIT 6A",
  "157V3CZFLEDP" => "GOVT LOT 2 W OF US HWY NO 1 EX",
  "157V3CZFLBAN" => "GOVT LOT 2 AND GOVT LOT 1 EX",
  "157V3CZFL51O" => "GOVT LOT 1 W OF PB 4 PG 21 EX",
  "157V3CZFL20X" => "GOVT LOT 1 LYING W OF HWY A1A",
  "157V3CZFKZ85" => "GOVT LOT 1 EX S 990 FT AND RD R/W",
  "157V3CZFKWOR" => "GOVT LOT 1 EX PB 25 PG 92",
  "157V3CZFKTIX" => "GOVT LOT 1 EX HWY I-95 R/W",
  "157V3CZFKPNF" => "GOVT LOT 1 E OF RR AND W OF US HWY",
  "157V3CZFKLU4" => "GOVT LOT 1 AND UNSURVEYED LAND ON",
  "157V3CZFL7ZG" => "GOVT LOT 1",
  "157V3CZFKHZG" => "GOODE PARK TERRACE",
  "157V3CZFKE3J" => "GOLFVIEW SUBD REPLAT",
  "157V3CZFK9UG" => "GOLFVIEW SUBD ADDN 1",
  "157V3CZFK5XH" => "GOLFVIEW PARK SEC 2",
  "157V3CZFK1QA" => "GOLFVIEW PARK SEC 1",
  "157V3CZFJXBJ" => "GOLFVIEW ESTATES",
  "157V3CZFJTCV" => "GOLF VIEW TERRACES CONDO",
  "157V3CZFJPSY" => "GOLF VIEW CONDO PH II",
  "157V3CZFJLVF" => "GOLF VIEW CONDO PH I",
  "157V3CZFJI7M" => "GOLF PARK SUBD",
  "157V3CZFJEQI" => "GOLF COTTAGES OF COCOA BEACH",
  "157V3CZFJBTN" => "GOLF CLUB ESTATES",
  "157V3CZFJ8RI" => "GOLDEN PALM CONDO",
  "157V3CZFJ5JV" => "GOLDEN GATE MANOR CONDO",
  "157V3CZFJ2X9" => "GOLDEN DUNES BEACH HOMES",
  "157V3CZFIWA7" => "GOLDEN BEACH ESTATES 3RD ADD 2ND REPLAT",
  "157V3CZFIT4N" => "GOLDEN BEACH ESTATES 2ND ADDN",
  "157V3CZFIMXC" => "GOLDEN BEACH ESTATES 1ST ADDN",
  "157V3CZFIZDN" => "GOLDEN BEACH ESTATES",
  "157V3CZFIJS8" => "GLOVER PLACE",
  "157V3CZFIGO9" => "GLENWOOD SUBD",
  "157V3CZFID59" => "GLENEAGLES TOWNHOMES PHASE 1 SUNTREE PUD STAGE 8 T",
  "157V3CZFI9UG" => "GLENEAGLES PHASE II",
  "157V3CZFI6LW" => "GLENEAGLE PHASE 1",
  "157V3CZFI3YK" => "GLEN ISLES SEC 3",
  "157V3CZFI0O0" => "GLEN ISLES SEC 2",
  "157V3CZFOAX6" => "GRAY PORTER SUBD",
  "157V3CZFO5IC" => "GRAVES PLAT REVISED PLAT OF BLKS A F G",
  "157V3CZFO844" => "GRAVES PLAT OF MELBOURNE BEACH",
  "157V3CZFO2B3" => "GRANT PLACE CONDO",
  "157V3CZFNYTD" => "GRANT ISLAND ESTATES",
  "157V3CZFNW7Y" => "GRANT COURT VILLAS",
  "157V3CZFNT02" => "GRANS SUBD OF CAPE MALABAR",
  "157V3CZFNMMS" => "GRANDVIEW SHORES REPLAT OF BLKS 3 TO 10",
  "157V3CZFNJL7" => "GRANDVIEW SHORES 2ND REPLAT",
  "157V3CZFNPDU" => "GRANDVIEW SHORES",
  "157V3CZFNGCW" => "GRANDVIEW PARK",
  "157V3CZFNA4S" => "GRANDVIEW HGTS RESUBD OF PT OF",
  "157V3CZFNDD2" => "GRANDVIEW HGTS",
  "157V3CZFN7BH" => "GRAND LAKE ESTATES",
  "157V3CZFN4SG" => "GRAND ISLE PHASE 3",
  "157V3CZFN1FS" => "GRAND ISLE PHASE 2 VIERA NORTH PUD PARCEL U X",
  "157V3CZFMY9X" => "GRAND ISLE PHASE 1 VIERA NORTH PUD PARCEL U X",
  "157V3CZFMV76" => "GRAND HAVEN PHASE TWO A REPLAT OF TRACT 5 AND A PO",
  "157V3CZFMSXS" => "GRAND HAVEN PHASE THREE A REPLAT OF TRACT 2 PB 46",
  "157V3CZFMQ9X" => "GRAND HAVEN PHASE SIX A REPLAT OF TRACT 1 GRAND HA",
  "157V3CZFMNOQ" => "GRAND HAVEN PHASE SEVEN A REPLAT OF A PORTION OF T",
  "157V3CZFMK9E" => "GRAND HAVEN PHASE ONE",
  "157V3CZFMH79" => "GRAND HAVEN PHASE NINE",
  "157V3CZFMEMD" => "GRAND HAVEN PHASE FOUR REPLAT OF A PORTION OF TR 3",
  "157V3CZFMB6Z" => "GRAND HAVEN PHASE FIVE A REPLAT OF A PORTION OF TR",
  "157V3CZFM7WB" => "GRAND HAVEN PHASE EIGHT A REPLAT OF A PORT OF TR 3",
  "157V3CZFM29J" => "GRAMLING PARK SUBD",
  "157V3CZFLZ60" => "GOVT LOTS 6 7 8 9 10 AND N 1/2 OF",
  "157V3CZFLWCP" => "GOVT LOTS 3 4 EX ST RD 405 ORB",
  "157V3CZFLT4F" => "GOVT LOTS 3 4 EX ORB 771 PG 258",
  "157V3CZFLQ6J" => "GOVT LOTS 2 AND 3 EX LANDS DEEDED",
  "157V3CZFLNDB" => "GOVT LOTS 1 2 AND NE 1/4 EX S",
  "157V3CZFLK6S" => "GOVT LOTS 1 2 5 10 AND EX W 225 FT",
  "157V3CZFLH3C" => "GOVT LOT 5",
  "157V3CZFRGY1" => "GREENWOOD VILLAGE CONDO NO 5",
  "157V3CZFRDOG" => "GREENWOOD VILLAGE CONDO NO 4",
  "157V3CZFRATK" => "GREENWOOD VILLAGE CONDO NO 3",
  "157V3CZFR7HH" => "GREENWOOD VILLAGE CONDO NO 2",
  "157V3CZFR4J3" => "GREENWOOD VILLAGE CONDO NO 1",
  "157V3CZFR1NU" => "GREENWOOD SUBDIVISION",
  "157V3CZFQYG6" => "GREENWOOD PLANTATION",
  "157V3CZFQVJS" => "GREENWOOD PARK UNIT NO 1",
  "157V3CZFQSZB" => "GREENWOOD MANOR CONDO NO 8",
  "157V3CZFQPMC" => "GREENWOOD MANOR CONDO NO 5",
  "157V3CZFQMSJ" => "GREENWOOD MANOR CONDO NO 4",
  "157V3CZFQK3I" => "GREENWOOD MANOR CONDO NO 3",
  "157V3CZFQGTI" => "GREENWOOD MANOR CONDO NO 17",
  "157V3CZFQDDE" => "GREENWOOD MANOR CONDO NO 16",
  "157V3CZFQ9TH" => "GREENWOOD MANOR CONDO NO 15",
  "157V3CZFQ6JI" => "GREENWOOD MANOR CONDO NO 13",
  "157V3CZFQ356" => "GREENWOOD MANOR CONDO NO 1",
  "157V3CZFPWOZ" => "GREENTREE PARK SUBD SECOND ADDN",
  "157V3CZFQ016" => "GREENTREE PARK SUBD",
  "157V3CZFPRPY" => "GREENTREE PARK 1ST ADDN TO",
  "157V3CZFPNE8" => "GREENS AT VIERA EAST CONDO",
  "157V3CZFPHCX" => "GREENBRIAR VILLAGE REPLAT NO 2",
  "157V3CZFPEBT" => "GREENBRIAR VILLAGE REPLAT NO 1",
  "157V3CZFPKL7" => "GREENBRIAR VILLAGE",
  "157V3CZFPBPU" => "GREENBRIAR UNIT 3",
  "157V3CZFP8AQ" => "GREENBRIAR UNIT 2",
  "157V3CZFP5AP" => "GREENBRIAR UNIT 1",
  "157V3CZFP2RK" => "GREENBRIAR SUBDIVISION",
  "157V3CZFOZET" => "GREENBRIAR PLACE",
  "157V3CZFOWS8" => "GREENBORO ACRES",
  "157V3CZFOUH7" => "GREEN PINES",
  "157V3CZFOR42" => "GREEN FIELD SUBD",
  "157V3CZFOHAC" => "GREEN ACRES ESTATES #2",
  "157V3CZFOKBA" => "GREEN ACRES ESTATES",
  "157V3CZFONPR" => "GREEN ACRES",
  "157V3CZFOE1T" => "GRECIAN ESTATES",
  "157V3CZFUZS8" => "HAMMOCK LAKES EAST PHASE TWO",
  "157V3CZFUWQC" => "HAMMOCK LAKES EAST PHASE ONE",
  "157V3CZFUQUH" => "HAMMOCK ESTATES PHASE 1",
  "157V3CZFUTJ6" => "HAMMOCK ESTATES",
  "157V3CZFUNRF" => "HAMLET SECTION 2",
  "157V3CZFUKOY" => "HAMLET SECTION 1",
  "157V3CZFUHYI" => "HAMLET PHASE TWO THE PHASE 2",
  "157V3CZFUEPA" => "HAMLET PHASE ONE THE",
  "157V3CZFUBFQ" => "HAMILTON PARK",
  "157V3CZFU88S" => "HALLWOOD PLACE",
  "157V3CZFU4ZU" => "HACIENDA ESTATES",
  "157V3CZFU1SI" => "HACIENDA DEL SOL ESTATES",
  "157V3CZFTTLZ" => "HACIENDA DEL MAR CONDO",
  "157V3CZFTPNS" => "H L GREEK UNRECORDED SUBD PT OF TRACT 6 ATKINSON",
  "157V3CZFTMEM" => "GULLHOUSE III CONDO",
  "157V3CZFTIQS" => "GULLHOUSE I CONDO PH I",
  "157V3CZFTAK1" => "GULFSTREAM BEACH",
  "157V3CZFT4GR" => "GROVES THE",
  "157V3CZFT7FM" => "GROVES PHASE II THE",
  "157V3CZFT1X6" => "GROVELAND ESTATES",
  "157V3CZFSV9G" => "GROVE PARK PLAT OF 1ST ADDN TO",
  "157V3CZFSYI3" => "GROVE PARK",
  "157V3CZFSS1P" => "GROVE ACRES SUBD",
  "157V3CZFSP6X" => "GRISSOM RIDGE",
  "157V3CZFSM7Z" => "GRIFFITHS OCEAN VILLAGE SUBD",
  "157V3CZFSGMF" => "GRIFFIES SUBDIVISION NO 2",
  "157V3CZFSJS4" => "GRIFFIES SUBDIVISION",
  "157V3CZFSA2X" => "GREYSTONE PHASE III",
  "157V3CZFS707" => "GREYSTONE PHASE II",
  "157V3CZFSD9Q" => "GREYSTONE",
  "157V3CZFS4MV" => "GREENWOOD VILLAGE CONDO NO 8",
  "157V3CZFS1CQ" => "GREENWOOD VILLAGE CONDO NO 7",
  "157V3CZFRY2V" => "GREENWOOD VILLAGE CONDO NO 6E",
  "157V3CZFRTU6" => "GREENWOOD VILLAGE CONDO NO 6D",
  "157V3CZFRQA6" => "GREENWOOD VILLAGE CONDO NO 6C",
  "157V3CZFRN6D" => "GREENWOOD VILLAGE CONDO NO 6B",
  "157V3CZFRKLT" => "GREENWOOD VILLAGE CONDO NO 6A",
  "157V3CZFY0ZW" => "HARBOR ESTATES SEC 3",
  "157V3CZFXV3X" => "HARBOR ESTATES SEC 2 REPLAT",
  "157V3CZFXYFP" => "HARBOR ESTATES SEC 2",
  "157V3CZFXS0A" => "HARBOR ESTATES SEC 1",
  "157V3CZFXORL" => "HARBOR EDGE CONDOMINIUM",
  "157V3CZFXLQ2" => "HARBOR EAST SEC 3 AMENDED",
  "157V3CZFXIWI" => "HARBOR EAST SEC 2",
  "157V3CZFXG5M" => "HARBOR EAST SEC 1",
  "157V3CZFXCZF" => "HARBOR DEL RIO CONDO PH IV",
  "157V3CZFX9UM" => "HARBOR DEL RIO CONDO PH III",
  "157V3CZFX75G" => "HARBOR DEL RIO CONDO PH II",
  "157V3CZFX3XM" => "HARBOR DEL RIO CONDO PH I",
  "157V3CZFWYLC" => "HARBOR COLONY UNIT 2",
  "157V3CZFX10K" => "HARBOR COLONY",
  "1AO0I6V6SNZG" => "HARBOR CLUB CONDOMINIUM",
  "157V3CZFWV4X" => "HARBOR CITY COUNTRY CLUB ESTATES SEC 1",
  "157V3CZFWRRD" => "HARBOR CENTER ONE CONDO",
  "157V3CZFWOY5" => "HARBOR BEACH CLUB A CONDOMINIUM",
  "157V3CZFWLGU" => "HANSELLS SUBD",
  "157V3CZFWIDQ" => "HANDLEN SUBD",
  "157V3CZFWF5V" => "HAMPTON PARK PHASE 3",
  "157V3CZFWC9I" => "HAMPTON PARK PHASE 2",
  "157V3CZFW9MJ" => "HAMPTON PARK PHASE 1",
  "157V3CZFW6MZ" => "HAMPTON HOMES UNIT 8",
  "157V3CZFW3PI" => "HAMPTON HOMES UNIT 7",
  "157V3CZFW155" => "HAMPTON HOMES UNIT 6",
  "157V3CZFVWVI" => "HAMPTON HOMES UNIT 5",
  "157V3CZFVU0G" => "HAMPTON HOMES UNIT 4",
  "157V3CZFVRH3" => "HAMPTON HOMES UNIT 3",
  "157V3CZFVO9N" => "HAMPTON HOMES UNIT 2",
  "157V3CZFVL6R" => "HAMPTON HOMES UNIT 1",
  "157V3CZFVI9H" => "HAMPTON COURT CONDO",
  "157V3CZFVEYR" => "HAMMOCK TRACE PRESERVE PHASE TWO",
  "157V3CZFVBLC" => "HAMMOCK TRACE PRESERVE PHASE ONE",
  "157V3CZFV96U" => "HAMMOCK LANDING",
  "157V3CZFV5PF" => "HAMMOCK LAKES WEST PHASE TWO",
  "157V3CZFV2JL" => "HAMMOCK LAKES WEST PHASE ONE",
  "157V3CZG1C4U" => "HARBORVIEW SEC 1",
  "157V3CZG18KG" => "HARBORVIEW ESTATES",
  "157V3CZG154G" => "HARBORVIEW COVE",
  "157V3CZG11NH" => "HARBOR WOODS CONDO SEC 1 PH III",
  "157V3CZG0WX7" => "HARBOR WOODS CONDO SEC 1 PH II",
  "157V3CZG0TJW" => "HARBOR WOODS CONDO SEC 1 PH I",
  "157V3CZG0PPF" => "HARBOR WALK ONE CONDO",
  "157V3CZG0M6N" => "HARBOR POINTE CONDO PH IV",
  "157V3CZG0IOT" => "HARBOR POINTE CONDO PH III",
  "157V3CZG0FIC" => "HARBOR POINTE CONDO PH II",
  "157V3CZG0CC8" => "HARBOR POINTE CONDO PH I",
  "157V3CZG09FC" => "HARBOR POINT PHASE ONE",
  "157V3CZG03KJ" => "HARBOR PINES OFFICE CONDO",
  "157V3CZG06K1" => "HARBOR PINES",
  "157V3CZG00RZ" => "HARBOR OAKS",
  "157V3CZFZG0O" => "HARBOR ISLES II PHASE V",
  "157V3CZFZCVT" => "HARBOR ISLES II PHASE IV",
  "157V3CZFZWGP" => "HARBOR ISLES II PH VII",
  "157V3CZFZRPF" => "HARBOR ISLES II PH VI",
  "157V3CZFZP5P" => "HARBOR ISLES II PH III",
  "157V3CZFZM78" => "HARBOR ISLES II PH II",
  "157V3CZFZIT8" => "HARBOR ISLES II PH I",
  "157V3CZFZ9UK" => "HARBOR ISLES CONDO PH XI",
  "157V3CZFZ6LF" => "HARBOR ISLES CONDO PH VIII",
  "157V3CZFZ3NR" => "HARBOR ISLES CONDO PH VII",
  "157V3CZFZ0LS" => "HARBOR ISLES CONDO PH VI",
  "157V3CZFYXSL" => "HARBOR ISLES CONDO PH IV",
  "157V3CZFYUNY" => "HARBOR ISLES CONDO PH III",
  "157V3CZFYRR2" => "HARBOR ISLES CONDO PH I",
  "157V3CZFYKHW" => "HARBOR HEIGHTS WEST",
  "157V3CZFYI2P" => "HARBOR HEIGHTS TOWNHOUSES",
  "157V3CZFYEMB" => "HARBOR HEIGHTS 3RD ADDN",
  "157V3CZFYB5D" => "HARBOR HEIGHTS 2ND ADDN",
  "157V3CZFY7AY" => "HARBOR HEIGHTS 1ST ADDN",
  "157V3CZFYOJ2" => "HARBOR HEIGHTS",
  "157V3CZFY48A" => "HARBOR GREEN CONDO",
  "157V3CZG4L4X" => "HAZELWOOD VILLAS CONDO PH II",
  "157V3CZG4HTB" => "HAZELWOOD VILLAS CONDO PH I",
  "157V3CZG4EU9" => "HAZELWOOD SUBD",
  "157V3CZG48NZ" => "HAYWARDS PROSPERITY FARMS 1ST ADD",
  "157V3CZG4CHE" => "HAYWARDS PROSPERITY FARMS",
  "157V3CZG458N" => "HAWTHORNES SUBD",
  "157V3CZG4253" => "HAWTHORNES RESUBD AS DESC IN DB EE PG 597",
  "157V3CZG3Z2K" => "HAWTHORNE POINT",
  "157V3CZG3VRD" => "HAWKS NEST AT AQUARINA",
  "157V3CZG3SME" => "HAVEN GREEN PINEWOOD SEC REPLAT",
  "157V3CZG3O8I" => "HATOUM ESTATES",
  "157V3CZG3L3H" => "HARVIN WAY CONDOMINIUM",
  "157V3CZG3HPF" => "HARVARD PHASE II",
  "157V3CZG3EQQ" => "HARVARD APARTMENTS",
  "157V3CZG3ANY" => "HARRISON TERRACE TOWNHOUSES",
  "157V3CZG37V1" => "HARLANDS SUBD",
  "157V3CZG31YX" => "HARDEEVILLE REPLAT OF",
  "157V3CZG35DQ" => "HARDEEVILLE",
  "157V3CZG2YLY" => "HARDEES PLAT OF INDIAN RIVER CITY",
  "157V3CZG2VM7" => "HARDEE AND RONALDS ADDN TO COCOA",
  "157V3CZG2SBT" => "HARDAWAY ESTATES SUBD",
  "157V3CZG2P6J" => "HARBOUR VILLA SECTION 2",
  "157V3CZG2MN2" => "HARBOUR VILLA SECTION 1",
  "157V3CZG2JK9" => "HARBOUR ROYALE WEST CONDO PH I",
  "157V3CZG2G6N" => "HARBOUR ROYALE SOUTH CONDO PH III",
  "157V3CZG2BQR" => "HARBOUR ROYALE NORTH CONDO PH II",
  "157V3CZG28EE" => "HARBOUR LIGHTS PHASE III",
  "157V3CZG25KB" => "HARBOUR LIGHTS PHASE II",
  "157V3CZG22A4" => "HARBOUR LIGHTS PHASE 1",
  "157V3CZG1WJ3" => "HARBOUR ISLES 3RD ADDN",
  "157V3CZG1T4U" => "HARBOUR ISLES 2ND ADDN",
  "157V3CZG1Q0R" => "HARBOUR ISLES 1ST ADDN",
  "157V3CZG1Z43" => "HARBOUR ISLES",
  "157V3CZG1MNU" => "HARBOUR COURT",
  "157V3CZG1I7H" => "HARBORVIEW SEC 3",
  "157V3CZG1F3V" => "HARBORVIEW SEC 2",
  "157V3CZG7R6F" => "HICKORY HILL UNIT NO 5",
  "157V3CZG7O2E" => "HICKORY HILL UNIT NO 4",
  "157V3CZG7L1K" => "HICKORY HILL UNIT NO 3",
  "157V3CZG7I91" => "HICKORY HILL UNIT NO 2",
  "157V3CZG7F2I" => "HICKORY HILL UNIT NO 1",
  "157V3CZG7BZ3" => "HICKORY GREEN UNIT TWO",
  "157V3CZG79M7" => "HICKORY GREEN UNIT THREE",
  "157V3CZG76E3" => "HICKORY GREEN UNIT ONE",
  "157V3CZG734H" => "HICKORY FOREST",
  "157V3CZG70HL" => "HICKORY CREEK SUBDIVISION",
  "157V3CZG6XC1" => "HIBISCUS PARK",
  "157V3CZG6UA8" => "HIAWATHA GDNS REPLAT BLKS I AND J",
  "157V3CZG6FM4" => "HIAWATHA GARDENS SEC 2",
  "157V3CZG6S08" => "HIAWATHA GARDENS",
  "157V3CZG6C88" => "HERONS LANDING-PHASE 2 UNIT 7 VIERA NORTH P.U.D.",
  "157V3CZG69ME" => "HERONS LANDING PHASE 2 UNIT 6B",
  "157V3CZG66KX" => "HERONS LANDING PHASE 2 UNIT 6 VIERA NORTH PUD T",
  "157V3CZG638Q" => "HERNANDO HIDEAWAY CONDO",
  "1594H1D2EDIH" => "HERITAGE ISLE PUD PHASE 7A",
  "1594H1D7U0R7" => "HERITAGE ISLE P.U.D. PHASE 6 1ST REPLAT",
  "157V3CZG60MS" => "HERITAGE ISLE P.U.D. PHASE 6",
  "157V3CZG5WXN" => "HERITAGE ISLE P.U.D. PHASE 5",
  "157V3CZG5TJF" => "HERITAGE ISLE P.U.D. PHASE 4",
  "157V3CZG5QV7" => "HERITAGE ISLE P.U.D. PHASE 3",
  "157V3CZG5O0A" => "HERITAGE ISLE P.U.D. PHASE 2 PLAT OF A PORTION OF",
  "157V3CZG5KUG" => "HERITAGE ISLE P.U.D. PHASE 1",
  "1AQSGSY5V4H5" => "HERITAGE ISLE P.U.D. Phase 8",
  "157V3CZG5EYS" => "HERITAGE ACRES PHASE TWO",
  "157V3CZG5I99" => "HERITAGE ACRES",
  "157V3CZG5BUI" => "HENRY WHITINGS MELBOURNE BCH",
  "157V3CZG59AA" => "HENDRYS RESUBD",
  "157V3CZG563O" => "HENDRYS ADDITION TO COCOA",
  "157V3CZG51N5" => "HENDRY GARDENS",
  "157V3CZG4XB9" => "HEDGES SUBD",
  "157V3CZG4QSN" => "HEART OF MERRITT CONDO",
  "157V3CZG4OD0" => "HAZELWOOD VILLAS CONDO PH III",
  "157V3CZGAVIB" => "HIGHLAND GROVES",
  "157V3CZGASNR" => "HIGHLAND ESTATES",
  "157V3CZGAQ5Z" => "HIGH POINT UNREC SUBD",
  "157V3CZGAMUC" => "HIGH POINT TOWER CONDO",
  "157V3CZGAJJF" => "HIGH POINT SUBD UNIT 3",
  "157V3CZGAGR3" => "HIGH POINT SUBD SEC 2",
  "157V3CZGADIZ" => "HIGH POINT SUBD SEC 1",
  "157V3CZGAAAY" => "HIGH POINT REPLAT OF LOT 2 BLK C",
  "157V3CZGA6S7" => "HIGH ACRES ESTATES UNIT 4",
  "157V3CZGA3V5" => "HIGH ACRES ESTATES UNIT 3",
  "157V3CZGA1MK" => "HIGH ACRES ESTATES UNIT 2",
  "157V3CZG9YZ3" => "HIGH ACRES ESTATES UNIT 1",
  "157V3CZG9SPL" => "HIDDEN PINES RANCHETTES UNIT 2",
  "157V3CZG9VQ4" => "HIDDEN PINES RANCHETTES",
  "157V3CZG9QBI" => "HIDDEN OAKS OF TITUSVILLE",
  "157V3CZG9N3W" => "HIDDEN LAKES PHASE ONE UNIT ONE REPLAT",
  "157V3CZG9K8I" => "HIDDEN HOLLOW",
  "157V3CZG9HA2" => "HIDDEN HILLS",
  "157V3CZG9DS1" => "HIDDEN HEIGHTS",
  "157V3CZG9AAA" => "HIDDEN HARBOUR",
  "157V3CZG96LH" => "HIDDEN HARBOR CONDO",
  "157V3CZG93ET" => "HIDDEN HAMMOCK ESTATES",
  "157V3CZG90PN" => "HIDDEN GROVE CONDO",
  "157V3CZG8X29" => "HIDDEN GLENN CONDO",
  "157V3CZG8TXX" => "HIDDEN CREEK",
  "157V3CZG8O5O" => "HIDDEN COVE TOWNHOUSES",
  "157V3CZG8QV3" => "HIDDEN COVE",
  "157V3CZG8KYZ" => "HIDDEN ACRES",
  "157V3CZG8I4A" => "HICKORY WOODS SEC 2",
  "157V3CZG8F6K" => "HICKORY WOODS SEC 1",
  "157V3CZG8CH1" => "HICKORY TRAILS SEC 2",
  "157V3CZG89US" => "HICKORY TRAILS SEC 1",
  "157V3CZG86Q6" => "HICKORY STREET MEDICAL CENTER CONDO",
  "157V3CZG83RR" => "HICKORY PARK PHASE ONE",
  "157V3CZG81AF" => "HICKORY LAKE",
  "157V3CZG7XWA" => "HICKORY HILLS VILLAS PHASE 1",
  "157V3CZG7UJB" => "HICKORY HILLS ESTS SUBD",
  "157V3CZGE89B" => "HOMBURG SUBD",
  "157V3CZGE1ZI" => "HOLMES SUBD SEC 1",
  "157V3CZGE550" => "HOLMES SUBD",
  "157V3CZGDZ9J" => "HOLLYWOOD ESTATES",
  "157V3CZGDW3Q" => "HOLLADAY MANOR CONDO",
  "157V3CZGDT1S" => "HOLIDAY SPRINGS AT SUNTREE",
  "157V3CZGDQNE" => "HOLIDAY ESTATES",
  "157V3CZGDKC4" => "HOLIDAY COVE UNIT 5",
  "157V3CZGDHI2" => "HOLIDAY COVE UNIT 4",
  "157V3CZGDEE0" => "HOLIDAY COVE UNIT 3-A",
  "157V3CZGDBK1" => "HOLIDAY COVE UNIT 3",
  "157V3CZGD8QY" => "HOLIDAY COVE UNIT 2A",
  "157V3CZGD545" => "HOLIDAY COVE UNIT 2",
  "157V3CZGD1ZC" => "HOLIDAY COVE UNIT 1A",
  "157V3CZGDNF7" => "HOLIDAY COVE",
  "157V3CZGCWMX" => "HOG POINT PUD",
  "157V3CZGCT7R" => "HILLTOP MANOR",
  "157V3CZGCJVP" => "HILLCREST SUBD",
  "157V3CZGCGV6" => "HILL CREST",
  "157V3CZGCDYV" => "HIGHLANDS UNIT 3",
  "157V3CZGCB1V" => "HIGHLANDS UNIT 2",
  "157V3CZGC8E6" => "HIGHLANDS UNIT 1 REPLAT",
  "157V3CZGC53Z" => "HIGHLANDS OF VIERA EAST",
  "157V3CZGC1RI" => "HIGHLAND TERRACE NO 1",
  "157V3CZGBZ8W" => "HIGHLAND SUBD",
  "157V3CZGBQ7J" => "HIGHLAND SHORES REPLAT OF PT OF",
  "157V3CZGBMXB" => "HIGHLAND SHORES REPLAT OF BLK E",
  "157V3CZGBT34" => "HIGHLAND SHORES ESTATES",
  "157V3CZGBW1G" => "HIGHLAND SHORES",
  "157V3CZGBH2V" => "HIGHLAND PARK SUBD SEC 2",
  "157V3CZGBJRK" => "HIGHLAND PARK",
  "157V3CZGBDVO" => "HIGHLAND HOUSE CONDO APTS PH IV",
  "157V3CZGBAWD" => "HIGHLAND HOUSE CONDO APTS PH III",
  "157V3CZGB88F" => "HIGHLAND HOUSE CONDO APTS PH II",
  "157V3CZGB55G" => "HIGHLAND HOUSE CONDO APTS PH I",
  "157V3CZGAYW0" => "HIGHLAND HOMES 1ST ADDN",
  "157V3CZGB1KM" => "HIGHLAND HOMES",
  "157V3CZGHLLL" => "IMPERIAL ESTATES UNIT 7",
  "157V3CZGHI7W" => "IMPERIAL ESTATES UNIT 6",
  "157V3CZGHDPZ" => "IMPERIAL ESTATES UNIT 5",
  "157V3CZGHAQN" => "IMPERIAL ESTATES UNIT 4",
  "157V3CZGH89K" => "IMPERIAL ESTATES UNIT 3",
  "157V3CZGH4UB" => "IMPERIAL ESTATES UNIT 2",
  "157V3CZGH23F" => "IMPERIAL ESTATES UNIT 1",
  "157V3CZGGZMU" => "IMPERIAL BUSINESS CENTER PH 2",
  "157V3CZGGVG6" => "IMPERIAL BUSINESS CENTER CONDO",
  "157V3CZGGQX4" => "IDLEWYLDE ESTATES SUBD",
  "157V3CZGGMY5" => "HUTCHINSONS SUBD OF BLK D",
  "157V3CZGGK4T" => "HUNTINGTON TERRACE",
  "157V3CZGGHRY" => "HUNTINGTON LAKES",
  "157V3CZGGEIZ" => "HUNTERS RUN",
  "157V3CZGG905" => "HUNTERS RIDGE II",
  "157V3CZGGBMD" => "HUNTERS RIDGE",
  "157V3CZGG5M4" => "HUNTERS GREEN",
  "157V3CZGG2G4" => "HUNTERS CREEK PHASE II",
  "157V3CZGG049" => "HUNTERS CREEK PHASE I",
  "157V3CZGFWZW" => "HUNDRED ACRE WOODS UNIT TWO",
  "157V3CZGFT80" => "HUNDRED ACRE WOODS UNIT THREE",
  "157V3CZGFPBH" => "HUNDRED ACRE WOODS UNIT ONE",
  "157V3CZGFI90" => "HUNDRED ACRE WOODS UNIT FOUR",
  "157V3CZGFETY" => "HOUSTONS ADD TO EAU GALLIE",
  "157V3CZGFBCJ" => "HORTI",
  "157V3CZGF7XF" => "HORNES REPLAT",
  "157V3CZGF4V9" => "HORIZON PALMS CONDO",
  "157V3CZGF1ZV" => "HORIZON COMMONS SHOPPING CENTER",
  "157V3CZGEZD9" => "HORIZON BUSINESS PARK CONDO",
  "157V3CZGEW6B" => "HORACE E PRICE SUBD AS DES IN DB 427 PG 149",
  "157V3CZGET20" => "HOPSONS SUBD",
  "157V3CZGEQ7P" => "HOPKINS PLAT",
  "157V3CZGEN2T" => "HONEYMOON HILL",
  "157V3CZGEJWY" => "HONEYBROOK PLANTATION",
  "157V3CZGEHEV" => "HOMER RODEHEAVER RESUBD",
  "157V3CZGEECB" => "HOME DEPOT PALM BAY OUTPARCEL A",
  "157V3CZGEARL" => "HOME DEPOT COCOA",
  "157V3CZGNDNX" => "INDIAN HARBOUR BEACH SEC 5",
  "157V3CZGNAL0" => "INDIAN HARBOUR BEACH SEC 4",
  "157V3CZGN7WX" => "INDIAN HARBOUR BEACH SEC 3",
  "157V3CZGN4UF" => "INDIAN HARBOUR BEACH SEC 13",
  "157V3CZGN1LE" => "INDIAN HARBOUR BEACH SEC 12",
  "157V3CZGMYJY" => "INDIAN HARBOUR BEACH SEC 11",
  "157V3CZGMVMM" => "INDIAN HARBOUR BEACH SEC 10",
  "157V3CZGMSIZ" => "INDIAN HARBOUR BEACH CLUB CONDO",
  "157V3CZGMQ3D" => "INDIAN HARBOR BEACH PROFESSIONAL PLAZA III",
  "157V3CZGMN7A" => "INDIAN HARBOR BEACH PROFESSIONAL PLAZA II",
  "157V3CZGMK9N" => "INDIAN HARBOR BEACH PROFESSIONAL PLAZA I",
  "157V3CZGMHRI" => "INDIAN CREEK ESTATES",
  "157V3CZGM7OO" => "INDIAN BAY ESTATES PHASE II STAGE ONE",
  "157V3CZGMEBN" => "INDIAN BAY ESTATES PHASE 1",
  "157V3CZGMAVE" => "INDIAN BAY ESTATES PH II STAGE 2",
  "157V3CZGM4CM" => "INDIALANTIC VILLAS CONDO",
  "157V3CZGLYBM" => "INDIALANTIC ONE CONDOMINIUM",
  "157V3CZGLUMH" => "INDIALANTIC OAKS",
  "157V3CZGK4ZW" => "INDIALANTIC HEIGHTS 2ND SEC",
  "157V3CZGJ0J4" => "INDIALANTIC HEIGHTS 2 A",
  "157V3CZGIXNG" => "INDIALANTIC HEIGHTS 1ST SEC",
  "157V3CZGIR5K" => "INDIALANTIC BY THE SEA SEC G",
  "157V3CZGINY9" => "INDIALANTIC BY THE SEA SEC E",
  "157V3CZGIKL4" => "INDIALANTIC BY THE SEA SEC D",
  "157V3CZGIHA4" => "INDIALANTIC BY THE SEA SEC A",
  "157V3CZGI9Z8" => "INDIALANTIC BY THE SEA REPLAT OF PT OF N",
  "157V3CZGIUKW" => "INDIALANTIC BY THE SEA",
  "157V3CZGI74W" => "IMPERIAL WAREHOUSE CONDO PHASE III",
  "157V3CZGI3UX" => "IMPERIAL WAREHOUSE CONDO PHASE II",
  "157V3CZGI0IU" => "IMPERIAL WAREHOUSE CONDO PHASE I",
  "157V3CZGHY70" => "IMPERIAL PLAZA CONDO",
  "157V3CZGHV1L" => "IMPERIAL ESTATES UNIT 9-A",
  "157V3CZGHRSA" => "IMPERIAL ESTATES UNIT 9",
  "157V3CZGHOQC" => "IMPERIAL ESTATES UNIT 8",
  "157V3CZGQ2HC" => "INDIAN RIVER ESTATES UNIT 2",
  "157V3CZGPZVK" => "INDIAN RIVER COLONY CLUB PUD PHASE 2 UNIT 4",
  "157V3CZGPW9D" => "INDIAN RIVER COLONY CLUB PUD PHASE 2 UNIT 3",
  "158ZIUC4I4UW" => "INDIAN RIVER COLONY CLUB PUD PHASE 2 UNIT 2",
  "157V3CZGPSXC" => "INDIAN RIVER COLONY CLUB PUD PHASE 2 UNIT 1",
  "157V3CZGPQ63" => "INDIAN RIVER COLONY CLUB PUD PHASE 1 UNIT 2",
  "157V3CZGPJWX" => "INDIAN RIVER COLONY CLUB PUD PHASE 1 UNIT 1 RESUB",
  "157V3CZGPMYN" => "INDIAN RIVER COLONY CLUB PUD PHASE 1 UNIT 1",
  "157V3CZGPH7P" => "INDIAN RIVER COLONY CLUB P.U.D PHASE 1 UNIT 3",
  "157V3CZGPDQ6" => "INDIAN RIVER CLUB CONDO",
  "157V3CZGP571" => "INDIAN RIVER CITY REVISED PLAT OF 1ST ADDN",
  "157V3CZGP8BW" => "INDIAN RIVER CITY REVISED PLAT OF",
  "157V3CZGPAT4" => "INDIAN RIVER CITY DELESPINE GRANT",
  "157V3CZGP1XQ" => "INDIAN RIVER CITY 3RD ADDN TO",
  "157V3CZGOYY9" => "INDIAN RIVER CITY 2ND ADDN TO",
  "157V3CZGOSI2" => "INDIAN RIVER BLUFF NO 3",
  "157V3CZGOQ4T" => "INDIAN RIVER BLUFF AMENDED PLAT NO 2",
  "157V3CZGOVPC" => "INDIAN RIVER BLUFF",
  "157V3CZGOJ8X" => "INDIAN RIVER ACRES UNIT 2",
  "157V3CZGOMOW" => "INDIAN RIVER ACRES",
  "157V3CZGOGT9" => "INDIAN OAKS",
  "157V3CZGODGW" => "INDIAN LANDING RIVERSIDE",
  "1583YZKGONOB" => "INDIAN LANDING PHASE IV",
  "157V3CZGOAEN" => "INDIAN LANDING PHASE III",
  "157V3CZGO7A1" => "INDIAN LANDING PHASE II",
  "157V3CZGO4A3" => "INDIAN LANDING PHASE 1",
  "157V3CZGO1EA" => "INDIAN HILL SUBD",
  "157V3CZGNYQ3" => "INDIAN HEAD UNIT ONE",
  "157V3CZGNVJ4" => "INDIAN HEAD ACRES SEC 3",
  "157V3CZGNSGE" => "INDIAN HEAD ACRES SEC 2",
  "157V3CZGNPT1" => "INDIAN HARBOUR BEACH SEC 9",
  "157V3CZGNMQE" => "INDIAN HARBOUR BEACH SEC 8",
  "157V3CZGNJKY" => "INDIAN HARBOUR BEACH SEC 7",
  "157V3CZGNH15" => "INDIAN HARBOUR BEACH SEC 6",
  "157V3CZGT7OV" => "INDIAN RIVER ISLES 3RD ADDN",
  "157V3CZGT53X" => "INDIAN RIVER ISLES 2ND ADDN",
  "157V3CZGT1TW" => "INDIAN RIVER ISLES 1ST ADDITION",
  "157V3CZGTAEX" => "INDIAN RIVER ISLES",
  "157V3CZGSYL3" => "INDIAN RIVER HOMESITES S/D",
  "157V3CZGSQAH" => "INDIAN RIVER HIGHLANDS SEC 2",
  "157V3CZGSUDP" => "INDIAN RIVER HIGHLANDS",
  "157V3CZGSICR" => "INDIAN RIVER HEIGHTS UNIT 9",
  "157V3CZGSEQP" => "INDIAN RIVER HEIGHTS UNIT 8",
  "157V3CZGSBBH" => "INDIAN RIVER HEIGHTS UNIT 7A",
  "157V3CZGS8K6" => "INDIAN RIVER HEIGHTS UNIT 7",
  "157V3CZGS5W3" => "INDIAN RIVER HEIGHTS UNIT 6",
  "157V3CZGS2N3" => "INDIAN RIVER HEIGHTS UNIT 5A",
  "157V3CZGRZFA" => "INDIAN RIVER HEIGHTS UNIT 5",
  "157V3CZGRWZH" => "INDIAN RIVER HEIGHTS UNIT 4",
  "157V3CZGRSN3" => "INDIAN RIVER HEIGHTS UNIT 3",
  "157V3CZGROG9" => "INDIAN RIVER HEIGHTS UNIT 22",
  "157V3CZGRL7L" => "INDIAN RIVER HEIGHTS UNIT 21",
  "157V3CZGRI29" => "INDIAN RIVER HEIGHTS UNIT 20",
  "157V3CZGRFHG" => "INDIAN RIVER HEIGHTS UNIT 2",
  "157V3CZGRC7C" => "INDIAN RIVER HEIGHTS UNIT 19",
  "157V3CZGR8L8" => "INDIAN RIVER HEIGHTS UNIT 18",
  "157V3CZGR5SV" => "INDIAN RIVER HEIGHTS UNIT 17",
  "157V3CZGR29T" => "INDIAN RIVER HEIGHTS UNIT 16",
  "157V3CZGQZ0H" => "INDIAN RIVER HEIGHTS UNIT 15",
  "157V3CZGQUJI" => "INDIAN RIVER HEIGHTS UNIT 14",
  "157V3CZGQRL2" => "INDIAN RIVER HEIGHTS UNIT 13",
  "157V3CZGQP1I" => "INDIAN RIVER HEIGHTS UNIT 12",
  "157V3CZGQLQ7" => "INDIAN RIVER HEIGHTS UNIT 11",
  "157V3CZGQICQ" => "INDIAN RIVER HEIGHTS UNIT 10",
  "157V3CZGQFBB" => "INDIAN RIVER HEIGHTS UNIT 1",
  "157V3CZGSMAB" => "INDIAN RIVER HEIGHTS",
  "157V3CZGQC45" => "INDIAN RIVER GROVES AND GARDENS",
  "157V3CZGQ8S4" => "INDIAN RIVER ESTATES VILLAS UNREC",
  "157V3CZGQ5FY" => "INDIAN RIVER ESTATES UNIT NO 1",
  "157V3CZGWH3X" => "INWOOD ISLES SUBD",
  "157V3CZGWEAU" => "INTRACOASTAL ESTATES",
  "157V3CZGWBS6" => "INDUSTRIAL PLAZA UNIT 2",
  "157V3CZGW8HA" => "INDUSTRIAL PLAZA UNIT 1",
  "157V3CZGW5A8" => "INDUSTRIAL CENTRE EAST",
  "157V3CZGW2JP" => "INDRIO ISLES",
  "157V3CZGVYYB" => "INDIGO CROSSING PHASE 4",
  "157V3CZGVV4N" => "INDIGO CROSSING PHASE 3",
  "157V3CZGVRLL" => "INDIGO CROSSING PHASE 2",
  "157V3CZGVO5U" => "INDIGO CROSSING PHASE 1 VIERA CENTRAL PUD- A PORTI",
  "157V3CZGVL97" => "INDIGO COVE",
  "157V3CZGVHST" => "INDIANOLA ESTATES",
  "157V3CZGVEL2" => "INDIANHEAD UNIT 2",
  "157V3CZGVC6O" => "INDIAN RIVER WALK SUBDIVISION",
  "157V3CZGV8VW" => "INDIAN RIVER VILLAGE",
  "157V3CZGV5QJ" => "INDIAN RIVER VILLA REC IN DB 318 PG 182",
  "157V3CZGV2BD" => "INDIAN RIVER TOWNHOMES",
  "157V3CZGUZ31" => "INDIAN RIVER SHORES SEC B",
  "157V3CZGUVTF" => "INDIAN RIVER SHORES SEC A",
  "157V3CZGUSNE" => "INDIAN RIVER SHORES ANNEX",
  "157V3CZGUOR0" => "INDIAN RIVER RESIDENCE LOTS",
  "157V3CZGUIOB" => "INDIAN RIVER PLANTATION ESTATES PLAT 5",
  "157V3CZGUETX" => "INDIAN RIVER PLANTATION ESTATES PLAT 4",
  "157V3CZGUAI6" => "INDIAN RIVER PLANTATION ESTATES PLAT 3",
  "157V3CZGU6VE" => "INDIAN RIVER PLANTATION ESTATES PLAT 2",
  "157V3CZGUM29" => "INDIAN RIVER PLANTATION ESTATES",
  "157V3CZGU3LM" => "INDIAN RIVER PINES TOWNHOUSES",
  "157V3CZGTX3U" => "INDIAN RIVER PARK SEC 18",
  "157V3CZGTK1Q" => "INDIAN RIVER PARK RESUBD OF SEC 17",
  "157V3CZGTSRV" => "INDIAN RIVER PARK RESUBD",
  "157V3CZGTPIW" => "INDIAN RIVER PARK REPLAT SEC 18",
  "157V3CZGTN6E" => "INDIAN RIVER PARK REPLAT SEC 17",
  "157V3CZGU0OT" => "INDIAN RIVER PARK",
  "157V3CZGTH0X" => "INDIAN RIVER OAKS SUBD",
  "157V3CZGTDGJ" => "INDIAN RIVER MANOR CONDO",
  "157V3CZGZOA2" => "ISLANDS AND MARSHLAND IN N 1/2 OF",
  "157V3CZGZLB0" => "ISLAND WALK TOWNHOMES CONDO PH I",
  "17SH9GAVX3O9" => "ISLAND VILLAS",
  "157V3CZGZINJ" => "ISLAND VILLAGE CONDO NO F",
  "157V3CZGZFKN" => "ISLAND VILLAGE CONDO NO E",
  "157V3CZGZCCH" => "ISLAND VILLAGE CONDO NO D",
  "157V3CZGZ9U5" => "ISLAND VILLAGE CONDO NO C",
  "157V3CZGZ6DE" => "ISLAND VILLAGE CONDO NO B",
  "157V3CZGZ37E" => "ISLAND VILLAGE CONDO NO A",
  "157V3CZGZ0DD" => "ISLAND SOUTH",
  "157V3CZGYX37" => "ISLAND SHORES OF MELBOURNE BEACH",
  "157V3CZGYU22" => "ISLAND PROFESSIONAL COMPLEX CONDO",
  "157V3CZGYRLW" => "ISLAND POINTE P.U.D.",
  "157V3CZGYOBG" => "ISLAND POINTE CONDO PH 2",
  "157V3CZGYKT0" => "ISLAND POINTE CONDO PH 1C",
  "157V3CZGYHXP" => "ISLAND POINTE CONDO PH 1B",
  "157V3CZGYEKL" => "ISLAND POINTE CONDO PH 1A",
  "157V3CZGYAPE" => "ISLAND OAKS SUBDIVISION",
  "157V3CZGY73V" => "ISLAND KNOWN AS GOODRICH ISLAND",
  "157V3CZGY3X9" => "ISLAND GROVE",
  "157V3CZGXXUL" => "ISLAND CROSSINGS PHASE 4B",
  "157V3CZGXUOP" => "ISLAND CROSSINGS PHASE 4A",
  "157V3CZGXRJ0" => "ISLAND CROSSINGS PHASE 3",
  "157V3CZGXO8D" => "ISLAND CROSSINGS PHASE 2",
  "157V3CZGY15V" => "ISLAND CROSSINGS",
  "157V3CZGXL4U" => "ISLAND COVE CONDO PH II",
  "157V3CZGXINU" => "ISLAND COVE CONDO PH 1",
  "157V3CZGXFCL" => "ISLAND BEACH SHEET 2",
  "157V3CZGX7RI" => "ISLAND BEACH SHEET 1 RESUBD OF",
  "157V3CZGXBZ1" => "ISLAND BEACH SHEET 1",
  "157V3CZGX45M" => "ISLAND ACRES SUBD",
  "157V3CZGX1H5" => "ISLAND ACRES BLK B",
  "157V3CZGWXSX" => "ISLAMORADA CONDO",
  "157V3CZGWUO2" => "IRWIN AVENUE BUSINESS CONDO",
  "157V3CZGWRBZ" => "IROQUOIS VILLAGE",
  "157V3CZGWL70" => "IROQUOIS SOUTH",
  "157V3CZH33C9" => "JAMESON PLACE CONDO PH XI",
  "157V3CZH2ZIC" => "JAMESON PLACE CONDO PH X",
  "157V3CZH2PJW" => "JAMESON PLACE CONDO PH VIII",
  "157V3CZH2MG9" => "JAMESON PLACE CONDO PH IX",
  "157V3CZH2JCW" => "JAMESON PLACE CONDO PH III",
  "157V3CZH2GBW" => "JAMESON PLACE CONDO PH II",
  "157V3CZH2DHR" => "JAMESON PLACE CONDO PH I",
  "157V3CZH2B0K" => "JAMES M HEARNDON SUBD",
  "157V3CZH24GY" => "JAMES LANDING P.U.D. TRACT B-2",
  "157V3CZH20P3" => "JAMES LANDING P.U.D. TRACT B-1",
  "157V3CZH27F5" => "JAMES LANDING",
  "157V3CZH1X8T" => "JAMAICA SHORES UNIT NO 2",
  "157V3CZH1TNN" => "JAMAICA SHORES UNIT NO 1",
  "157V3CZH1R1C" => "JAMAICA COVE CONDO",
  "157V3CZH1NJ6" => "JADE TREE",
  "157V3CZH1GRT" => "JADE PALM CONDO",
  "157V3CZH1DTM" => "J W STEWARTS",
  "157V3CZH1AN0" => "J V D ALBORA SUBD",
  "157V3CZH16O5" => "J S STONES SUBD",
  "157V3CZH13J8" => "J M SANDERS SUBD",
  "157V3CZH10OY" => "J M SANDERS PLAT OF COCOA RESUBD OF E PT OF LOT 2",
  "157V3CZH0XDG" => "J M CONDO",
  "157V3CZH0UJH" => "J J COVEYS UNREC SUBD",
  "157V3CZH0RSU" => "J H DERBYS SUBD",
  "157V3CZH0OLH" => "J C GARRETTS UNREC SUBD",
  "157V3CZH0LAF" => "IXORA PARK PLAT NO 6",
  "157V3CZH0IB2" => "IXORA PARK PLAT NO 5",
  "157V3CZH0FBH" => "IXORA PARK PLAT NO 4",
  "157V3CZH0CEY" => "IXORA PARK PLAT NO 3",
  "157V3CZH09QG" => "IXORA PARK PLAT NO 2",
  "157V3CZH06DS" => "IXORA PARK PLAT NO 1",
  "157V3CZH03JB" => "IVANHOE ESTATES UNIT 1",
  "157V3CZH010U" => "ISOMS SUBD",
  "157V3CZGZU38" => "ISLES OF BAYTREE PHASE 2 A REPLAT OF TRACT L ISL",
  "157V3CZGZXPS" => "ISLES OF BAYTREE PHASE 1",
  "157V3CZGZREV" => "ISLE OF MERRITT RESIDENTIAL CONDO PH I",
  "157V3CZH6VGB" => "JOYAL GARDENS",
  "157V3CZH6S3H" => "JORGENSONS PLAT OF GRANT",
  "157V3CZH6O2W" => "JONES WARD PLAT OF MIMS",
  "157V3CZH6G36" => "JONES SUBD M S OF PART OF NW 1/4 OF NE 1/4",
  "157V3CZH6JYZ" => "JONES SUBD",
  "157V3CZH6BT0" => "JONES JR SUBD M S",
  "157V3CZH67Q1" => "JONES ADDITION TO TITUSVILLE",
  "157V3CZH63A6" => "JOHNSONS SUBD",
  "157V3CZH5ZL0" => "JOHNSON ARMS CONDO",
  "157V3CZH5VEN" => "JOHNSON AND RODES SUBDIVISION AS PER PLAT IN DB 58",
  "157V3CZH5RIA" => "JOHNSON AND RHODES RESUBD OF LOT 15",
  "157V3CZH5NU9" => "JOHN RODES COMMERCE PARK CONDO PH IB",
  "157V3CZH5JRD" => "JOHN RODES COMMERCE PARK CONDO PH IA",
  "157V3CZH5FVH" => "JOHN J POLLAKS SUBD",
  "157V3CZH5BUL" => "JOHN F MORGAN SUBD",
  "157V3CZH56U7" => "JOHANSON TOWNHOMES",
  "157V3CZH52FE" => "JEROMES SUBD OF 40.36 ACRE TRACT OF FLEMING GRANT",
  "157V3CZH4Y50" => "JERNIGANS SUBD",
  "157V3CZH4UXY" => "JENKINS ACRES",
  "157V3CZH4MZN" => "JEFFERSON VILLAS PHASE II A REPLAT OF A PORTION OF",
  "157V3CZH4QRN" => "JEFFERSON VILLAS",
  "157V3CZH4IY1" => "JEFFERSON TOWNHOMES",
  "157V3CZH4F7V" => "JEFFERSON ARMS CONDO",
  "157V3CZH4C39" => "JEANIE II BY THE SEA CONDO",
  "157V3CZH46DB" => "JEANIE BY THE SEA CONDO",
  "157V3CZH43CK" => "JASMINE ESTATES",
  "157V3CZH3ZW1" => "JAMESON PLACE CONDO PH XXI",
  "157V3CZH3WEP" => "JAMESON PLACE CONDO PH XX",
  "157V3CZH3SQW" => "JAMESON PLACE CONDO PH XVIII",
  "157V3CZH3PAF" => "JAMESON PLACE CONDO PH XVI",
  "157V3CZH3LGX" => "JAMESON PLACE CONDO PH XV",
  "157V3CZH3I4C" => "JAMESON PLACE CONDO PH XIX",
  "157V3CZH3EY6" => "JAMESON PLACE CONDO PH XIV",
  "157V3CZH3AWE" => "JAMESON PLACE CONDO PH XIII",
  "157V3CZH3781" => "JAMESON PLACE CONDO PH XII",
  "157V3CZHF4R2" => "KIRKTON RICHARDS SUBD",
  "157V3CZHF14L" => "KINGSWOOD UNIT TWO",
  "157V3CZHEXLB" => "KINGSWOOD UNIT ONE",
  "157V3CZHEU4Z" => "KINGSMILL",
  "157V3CZHEQNK" => "KINGS ROW",
  "157V3CZHEN2O" => "KINGS PARK UNIT 4",
  "157V3CZHEJZJ" => "KINGS PARK UNIT 3",
  "157V3CZHEGZB" => "KINGS PARK UNIT 2",
  "157V3CZHEDRH" => "KINGS PARK UNIT 1",
  "157V3CZHEALI" => "KINGS PARK ESTATES",
  "157V3CZHE829" => "KINGS GRANT UNIT 5",
  "157V3CZHE4RB" => "KINGS GRANT UNIT 4",
  "157V3CZHE178" => "KINGS GRANT UNIT 3",
  "157V3CZHDXXM" => "KINGS GRANT UNIT 2",
  "157V3CZHDUW5" => "KINGS GRANT UNIT 1",
  "157V3CZHDO7T" => "KINGS COURT HEIGHTS",
  "157V3CZHDKSF" => "KINGS COURT ADDN",
  "157V3CZHDRJ0" => "KINGS COURT",
  "157V3CZHDFVN" => "KIMBERLY ESTATES",
  "157V3CZHDCCU" => "KEY SHORE",
  "157V3CZHD8P1" => "KEW COMMERCIAL CENTER PHASE I",
  "157V3CZHD55Y" => "KENZEL TERRACE SUBD",
  "157V3CZHD1RR" => "KENNEDY SQUARE A REPLAT OF OUTLOT 19 VILLAGE PLAT",
  "157V3CZHCYDY" => "KENNEDY POINT CONDO PH I",
  "157V3CZHCUIC" => "KELLY PARK",
  "157V3CZHCKXZ" => "KELL-MER HOMES SUBD",
  "157V3CZHCHMT" => "KAYA BEACH TOWNHOMES",
  "157V3CZHCDSR" => "KAY LYNN HGTS",
  "157V3CZHC78O" => "KALINS REPLAT",
  "157V3CZHC3YA" => "K AND K COMMERCIAL CONDO",
  "157V3CZH7LSC" => "JUNE PARK ADDN NO 5",
  "157V3CZH7HSU" => "JUNE PARK ADDN NO 4",
  "157V3CZH7DVW" => "JUNE PARK ADDN NO 3",
  "157V3CZH7AAD" => "JUNE PARK ADDN NO 2",
  "157V3CZH775S" => "JUNE PARK ADDN NO 1 RESUBD",
  "157V3CZH7PLU" => "JUNE PARK",
  "157V3CZH725F" => "JULIAN PLACE OFFICE CONDO",
  "157V3CZH6Z1N" => "JOYNERVILLE ADDN TO TITUSVILLE",
  "157V3CZHIY20" => "LA COSTA VILLAGE",
  "157V3CZHIR53" => "LA COSTA OCEAN AND BATH CLUB II CONDO",
  "157V3CZHIO9W" => "LA COSTA OCEAN AND BATH CLUB I CONDO",
  "157V3CZHIL4F" => "LA COLONNADE CONDO",
  "157V3CZHII0R" => "LA CITA TOWNS - VILLAGE 3",
  "157V3CZHIENI" => "LA CITA TOWNS - VILLAGE 2",
  "157V3CZHIBB0" => "LA CITA TOWNS - VILLAGE 1",
  "157V3CZHI828" => "LA CITA SECTION 8",
  "157V3CZHI4OX" => "LA CITA SECTION 7",
  "157V3CZHI1D1" => "LA CITA SECTION 6",
  "157V3CZHHXYM" => "LA CITA SECTION 5",
  "157V3CZHHV1Y" => "LA CITA SECTION 4",
  "157V3CZHHOPZ" => "LA CITA SECTION 3 AMENDMENT OF",
  "157V3CZHHRQQ" => "LA CITA SECTION 3",
  "157V3CZHHLLX" => "LA CITA SECTION 2-A REPLAT OF",
  "157V3CZHHI9F" => "LA CITA SECTION 2",
  "157V3CZHHEXY" => "LA CITA SECTION 11",
  "157V3CZHHBT9" => "LA CITA SECTION 1",
  "157V3CZHH8J8" => "LA CITA HILLTOP SUBDIVISION",
  "157V3CZHH513" => "LA BERTHA LAWN",
  "157V3CZHGVMP" => "LA BELLA CASA CONDO",
  "157V3CZHGSJA" => "L S T ESTATES",
  "157V3CZHGP2L" => "L S SPENCE UNREC SUBD",
  "157V3CZHGLR7" => "L P ALLENS SUBD",
  "157V3CZHGIOZ" => "L L OWENS SUBD",
  "157V3CZHGFHD" => "L BISHOPS SUBD",
  "157V3CZHGC3T" => "L AND C ARLINGTON PINES CONDO",
  "157V3CZHG8RL" => "KRAGERS RESUBD OF BLK 25 PLAT OF MELBOURNE",
  "157V3CZHG5HI" => "KNOX MC RAE BARNA CONDOMINIUM",
  "157V3CZHG24B" => "KNOLLWOOD GARDENS SEC 2",
  "157V3CZHFYNP" => "KNOLLWOOD GARDENS SEC 1",
  "157V3CZHFVHU" => "KNIGHTS SUBD",
  "157V3CZHFO79" => "KNIGHT OAK A REPLAT OF LOT 12 KNIGHT OAK",
  "157V3CZHFRXH" => "KNIGHT OAK",
  "157V3CZHFEOF" => "KNECHTS SUBD",
  "157V3CZHFBIB" => "KIRKWOOD SECTION ONE - A",
  "157V3CZHF7YR" => "KIRKWOOD SECTION 1-B",
  "157V3CZHLSMC" => "LAKE VIEW ESTATES",
  "157V3CZHLPFL" => "LAKE RIDGE",
  "157V3CZHLKXZ" => "LAKE POINTE SUNTREE PUD STAGE 10 TRACT 6",
  "157V3CZHLHWJ" => "LAKE POINTE SUNTREE PUD STAGE 10 TR 6 UNIT 3 AND T",
  "157V3CZHLEOJ" => "LAKE POINSETT VENETIAN LOTS",
  "157V3CZHLBPY" => "LAKE POINSETT FIRST ADDN TO",
  "157V3CZHL8ZY" => "LAKE IBIS",
  "158ZUWBAXR2E" => "LAKE HARNEY WOODS",
  "157V3CZHL5PV" => "LAKE FOREST AT BAYSIDE LAKES",
  "157V3CZHL2NQ" => "LAKE DRIVE TERRACE",
  "157V3CZHL05C" => "LAKE CREST NO 4",
  "157V3CZHKX2P" => "LAKE CREST NO 3",
  "157V3CZHKU83" => "LAKE CREST NO 2",
  "157V3CZHKR2J" => "LAKE CREST NO 1",
  "157V3CZHKNYQ" => "LAKE AREA IN N 1/2 OF SEC NOT INC",
  "157V3CZHKKNV" => "LAKE ANDREW COMMERCIAL CENTER",
  "157V3CZHKI2C" => "LAILA PARK ESTATES SEC B",
  "157V3CZHKENV" => "LAILA PARK ESTATES SEC A",
  "157V3CZHKB9D" => "LAILA PARK ESTATES 1ST ADD",
  "157V3CZHK8NI" => "LAGUNA VISTA CONDO",
  "157V3CZHK5OU" => "LAGO VISTA",
  "157V3CZHK2LK" => "LA VISTA DEL MAR CONDO",
  "157V3CZHK05E" => "LA VIRA TOWNHOMES",
  "157V3CZHJWK7" => "LA RIVIERE ASSOC NO 1 CONDO",
  "157V3CZHJTUH" => "LA PLAYA EAST CONDO",
  "157V3CZHJRCH" => "LA PALOMA CONDO PH I",
  "157V3CZHJO7J" => "LA MER APTS CONDO",
  "157V3CZHJL95" => "LA MAJA TOWNHOUSE AT LA CITA",
  "157V3CZHJFE9" => "LA GRANGE ACRES AMENDED PLAT OF BLK B",
  "157V3CZHJBSA" => "LA GRANGE ACRES 3RD ADDN",
  "157V3CZHJ8J9" => "LA GRANGE ACRES 2ND ADDN",
  "157V3CZHJ4YS" => "LA GRANGE ACRES 1ST ADDN",
  "157V3CZHJIN6" => "LA GRANGE ACRES",
  "17VISQB33WOB" => "LA ENTRADA DEL MAR",
  "157V3CZHJ1FY" => "LA ENSENADA CONDO",
  "157V3CZHIUGF" => "LA COSTA VILLAGE UNIT 2",
  "157V3CZHOWE3" => "LAKEWOOD ESTATES SEC 1A",
  "157V3CZHOT64" => "LAKEWOOD ESTATES SEC 1",
  "157V3CZHOQ6G" => "LAKEWOOD ESTATES PHASE 2 A REPLAT OF LOTS 5 6 AND",
  "157V3CZHOYWB" => "LAKEWOOD ESTATES",
  "157V3CZHOJK8" => "LAKEVIEW SHORES SEC G",
  "157V3CZHOGAN" => "LAKEVIEW SHORES SEC F",
  "157V3CZHOCWE" => "LAKEVIEW SHORES SEC E",
  "157V3CZHO9MP" => "LAKEVIEW SHORES SEC D",
  "157V3CZHO7HN" => "LAKEVIEW SHORES SEC C",
  "157V3CZHO4HJ" => "LAKEVIEW SHORES REPLAT OF PT OF SEC G",
  "157V3CZHONSJ" => "LAKEVIEW SHORES",
  "157V3CZHNXY0" => "LAKEVIEW PARK UNIT 4",
  "157V3CZHNUP3" => "LAKEVIEW PARK UNIT 2",
  "157V3CZHO16N" => "LAKEVIEW PARK",
  "157V3CZHNRD6" => "LAKEVIEW HILLS UNIT 2 SECTION C",
  "157V3CZHNO7Y" => "LAKEVIEW HILLS UNIT 2 SEC B",
  "157V3CZHNKY6" => "LAKEVIEW HILLS UNIT 2 SEC A",
  "157V3CZHNHNB" => "LAKEVIEW HILLS UNIT 1",
  "157V3CZHNAH9" => "LAKESIDE PHASE 1 REPLAT",
  "157V3CZHNEBB" => "LAKESIDE PHASE 1",
  "157V3CZHN4BF" => "LAKES OF RIDGE MANOR THE PHASE TWO",
  "157V3CZHN7K5" => "LAKES OF RIDGE MANOR THE",
  "157V3CZHN111" => "LAKES AT VIERA EAST CONDO",
  "157V3CZHMR4D" => "LAKEFRONT ESTATES 1ST ADDN REPLAT OF",
  "157V3CZHMTG7" => "LAKEFRONT ESTATES 1ST ADDN",
  "157V3CZHMWDH" => "LAKEFRONT ESTATES",
  "157V3CZHMNY9" => "LAKE WASHINGTON TOWNHOMES",
  "157V3CZHMKOW" => "LAKE WASHINGTON OAKS",
  "157V3CZHMI0T" => "LAKE WASHINGTON ESTATES UNIT 1",
  "157V3CZHMEIY" => "LAKE WASHINGTON BUSINESS CENTER",
  "157V3CZHM4PB" => "LAKE WASHINGTON ACRES SEC 1 REPLAT",
  "157V3CZHM83Y" => "LAKE WASHINGTON ACRES SEC 1",
  "157V3CZHMB5T" => "LAKE WASHINGTON ACRES",
  "157V3CZHM1VA" => "LAKE VIEW SEC 3",
  "157V3CZHLZEW" => "LAKE VIEW SEC 2",
  "157V3CZHLVYW" => "LAKE VIEW SEC 1",
  "157V3CZHS1U7" => "LAS VERDES TOWNHOMES TRACT E",
  "157V3CZHRYTS" => "LAS PALMAS APTS CONDO",
  "157V3CZHRVSL" => "LAS OLAS BEACH CLUB CONDO TIME SHARE",
  "157V3CZHRT0X" => "LAS OLAS BCH CLUB OF COCOA BCH TIME SHARE",
  "157V3CZHRP48" => "LAS DOS CONDO",
  "157V3CZHRKY2" => "LAS BRISAS CONDO PH II",
  "157V3CZHRHO4" => "LAS BRISAS CONDO PH I",
  "157V3CZHRBYP" => "LAPHAMS SUBD OF LOT 4 BLK A ADDN TO COCOA",
  "157V3CZHREGY" => "LAPHAMS SUBD",
  "157V3CZHR8RS" => "LAPHAM PARK",
  "157V3CZHR38C" => "LANTERN PARK UNIT 4",
  "157V3CZHQZVN" => "LANTERN PARK UNIT 3",
  "157V3CZHQW58" => "LANTERN PARK UNIT 2",
  "157V3CZHR5OP" => "LANTERN PARK",
  "157V3CZHQT77" => "LANTANA OCEANFRONT",
  "157V3CZHQQ6Z" => "LANSING RIDGE SUBD PHASE TWO",
  "157V3CZHQN89" => "LANSING RIDGE SUBD PHASE THREE",
  "157V3CZHQKVP" => "LANSING RIDGE SUBD PHASE FOUR B",
  "157V3CZHQIAY" => "LANSING RIDGE SUBD PHASE FOUR A",
  "157V3CZHQFAC" => "LANSING RIDGE PHASE 1B",
  "157V3CZHQCPM" => "LANSING RIDGE PHASE 1A",
  "157V3CZHQ9G1" => "LANSING ISLAND PHASE TWO",
  "157V3CZHQ68L" => "LANSING ISLAND PHASE THREE",
  "157V3CZHQ3H2" => "LANSING ISLAND PHASE ONE",
  "157V3CZHPX28" => "LANSING ISLAND PHASE FOUR TRACT A",
  "157V3CZHQ0DJ" => "LANSING ISLAND PHASE FOUR",
  "157V3CZHPS4A" => "LANSING ISLAND PHASE FIVE",
  "157V3CZHPP1H" => "LANG-RYLAND CONDOMINIUM",
  "157V3CZHPLOR" => "LANDMARK PLAZA OFFICE CONDO",
  "157V3CZHPIJ3" => "LANDFORD COURT RESUB OF",
  "157V3CZHPFGW" => "LAMPLIGHTER VILLAGE",
  "157V3CZHPAZ5" => "LAMP POST APTS CONDO",
  "15BX5V8NKQHI" => "LAKEWOOD PARK CONDOMINIUM",
  "157V3CZHP5EK" => "LAKEWOOD MANOR 2ND ADDITION",
  "157V3CZHP2AX" => "LAKEWOOD MANOR 1ST ADDITION",
  "157V3CZHP7TC" => "LAKEWOOD MANOR",
  "157V3CZHVBGF" => "LIDDYS ADDN TO GOLF PARK",
  "157V3CZHV61P" => "LIBERTY SUBDIVISION",
  "157V3CZHV3FO" => "LEXUS OF MELBOURNE",
  "157V3CZHUZY7" => "LEWIS AND JEFFE SUBD",
  "157V3CZHUW7O" => "LEVANTO SUBDIVISION",
  "157V3CZHUTR4" => "LES VILLAS AT AQUARINA",
  "157V3CZHUQK9" => "LENORE ADDN UNIT NO 5",
  "157V3CZHUNC8" => "LENORE ADDN UNIT NO 4",
  "157V3CZHUK0H" => "LENORE ADDN UNIT NO 3",
  "157V3CZHUH0S" => "LENORE ADDN TO TITUSVILLE",
  "157V3CZHUECR" => "LEMAY PALM BAY ESTATES",
  "17TGPG7HQOMX" => "LEISURE LIVING ESTATES CONDO",
  "157V3CZHUBVZ" => "LEGENDS UNIT TWO THE",
  "157V3CZHU8R5" => "LEGENDS UNIT THREE THE",
  "157V3CZHU5PT" => "LEGENDS UNIT ONE THE",
  "157V3CZHU31G" => "LEEWOOD FOREST SEC 8",
  "157V3CZHTZKB" => "LEEWOOD FOREST SEC 7",
  "157V3CZHTT6S" => "LEEWOOD FOREST SEC 6 REPLAT OF PORTIONS OF",
  "157V3CZHTW4L" => "LEEWOOD FOREST SEC 6",
  "157V3CZHTOT3" => "LEEWOOD FOREST SEC 5",
  "157V3CZHTM03" => "LEEWOOD FOREST SEC 4",
  "157V3CZHTIH2" => "LEEWOOD FOREST SEC 3",
  "157V3CZHTEXS" => "LEEWOOD FOREST SEC 2",
  "157V3CZHT8WZ" => "LEEWOOD FOREST SEC 1",
  "157V3CZHT4S8" => "LEEWOOD FOREST EST",
  "157V3CZHT282" => "LECUME DE MER OF MELBOURNE BCH CONDO",
  "157V3CZHSZ4A" => "LECHNERS SUBD",
  "157V3CZHSW3J" => "LAWS TOWNHOMES",
  "157V3CZHSTN4" => "LAVETA SUBD",
  "157V3CZHSQH5" => "LAURELWOOD AT BAYSIDE LAKES",
  "157V3CZHSNI1" => "LAURELL PARK ESTATES REPLAT",
  "157V3CZHSKY3" => "LAUREL RUN AT MEADOWRIDGE PHASE TWO",
  "157V3CZHSHL8" => "LAUREL RUN AT MEADOWRIDGE PHASE ONE",
  "157V3CZHSEQO" => "LAURA LEE ESTATES",
  "157V3CZHSBG0" => "LATHROPES SUBD",
  "157V3CZHS82D" => "LAS VERDES TOWNHOMES TRACT F PHASE TWO",
  "157V3CZHS4UJ" => "LAS VERDES TOWNHOMES TRACT F PHASE ONE",
  "157V3CZHYFZP" => "LOS VECINOS ESTATES REPLAT OF",
  "157V3CZHYCRO" => "LORINGS UNRECORDED SUBD PT OF TRACT 3 ATKINSON SUB",
  "157V3CZHY9OC" => "LOOKOUT POINT APTS CONDO",
  "157V3CZHY6RQ" => "LONGWOOD PHASE III",
  "157V3CZHY3XT" => "LONGWOOD PHASE II",
  "157V3CZHY10M" => "LONGWOOD PHASE I",
  "157V3CZHXXTE" => "LONGS SUBD",
  "157V3CZHXUUM" => "LONG POINT UNREC SUBD",
  "157V3CZHXRP5" => "LONG POINT TOWNHOUSES",
  "157V3CZHXOI8" => "LON DALE ESTATES",
  "157V3CZHXM2Z" => "LOMA LINDA PLAT OF SURVEY FOR",
  "157V3CZHXEU7" => "LOCKMAR ACRES",
  "15A0E3MJBGU1" => "Lost Lakes Phase I",
  "157V3CZHXBFH" => "LIVING HOMES ESTATES PHASE 5",
  "157V3CZHX5Q7" => "LIVING HOMES ESTATES PHASE 4",
  "157V3CZHX2DZ" => "LIVING HOMES ESTATES PHASE 3",
  "157V3CZHWZAI" => "LIVING HOMES ESTATES PHASE 2",
  "157V3CZHWWD9" => "LIVING HOMES ESTATES PHASE 1",
  "157V3CZHWTJ8" => "LIVE OAK PHASE TWO",
  "157V3CZHWQG0" => "LIVE OAK PHASE THREE",
  "157V3CZHWNFM" => "LIVE OAK PHASE ONE",
  "157V3CZHWKWD" => "LITTLE VILLAGE",
  "157V3CZHWHML" => "LITTLE HOLLYWOOD SUBD",
  "157V3CZHW583" => "LITTLE HOLLYWOOD REPLAT OF PT OF",
  "157V3CZHW8AV" => "LITTLE HOLLYWOOD 1ST ADDN REPLAT OF PT OF REPLAT",
  "157V3CZHWBHP" => "LITTLE HOLLYWOOD 1ST ADDN REPLAT OF",
  "157V3CZHWEIT" => "LITTLE HOLLYWOOD 1ST ADDN",
  "157V3CZHW2RI" => "LINDSAYS SUBD",
  "157V3CZHVZN9" => "LINCOLN PARK SUBD",
  "157V3CZHVWGQ" => "LINCOLN ARMS CONDO",
  "157V3CZHVTZ5" => "LIMAGE VILLAS TOWNHOMES",
  "157V3CZHVR1F" => "LIGHTHOUSE LANDING",
  "157V3CZHVNPX" => "LIGHTHOUSE COVE CONDO PH IV",
  "157V3CZHVL3J" => "LIGHTHOUSE COVE CONDO PH III",
  "157V3CZHVHUC" => "LIGHTHOUSE COVE CONDO PH II",
  "157V3CZHVEBJ" => "LIGHTHOUSE COVE CONDO PH I",
  "157V3CZI1NTW" => "LOTS 17 AND 32 EX ORB 2032 PG 665",
  "157V3CZI1K85" => "LOTS 14 15 18 19 OF PB 1 PG 165",
  "157V3CZI1GJ0" => "LOTS 1 TO 7",
  "157V3CZI1CUE" => "LOTS 1 TO 5 7 TO 22 25 TO 28",
  "157V3CZI12YS" => "LOTS 1 TO 4 13 TO 20 29 TO 32",
  "157V3CZI168Z" => "LOTS 1 TO 4 13 TO 17 19 20 27 29",
  "157V3CZI18R9" => "LOTS 1 TO 4",
  "157V3CZI0ZX7" => "LOTS 1 TO 32 OF PB 1 PG 166 EX",
  "157V3CZI0X4X" => "LOTS 1 TO 17 19 TO 26 29 TO 32",
  "157V3CZI0URY" => "LOTS 1 AND 2 E OF HWY S OF MC IVER",
  "157V3CZI0RN0" => "LOTS 1 4 TO 7 9 11 TO 14 16 18",
  "157V3CZI0LEQ" => "LOTS 1 2 N OF SEBASTIAN INLET EX",
  "157V3CZI0IP7" => "LOTS 1 2 7 TO 12 17 TO 25 W 1/2",
  "157V3CZI0FKC" => "LOTS 1 2 5 TO 32 OF FLA INDIAN",
  "157V3CZI0D5S" => "LOTS 1 2 5 6 EX ORB 694 PG 509",
  "157V3CZI08FB" => "LOTS 1 2 3 EX N 1500 FT AND S 500",
  "157V3CZI05YO" => "LOTS 1 2 3 AND ORB 221 PG 488",
  "157V3CZI039C" => "LOTS 1 2 3 AND E 1/2 OF SE 1/4",
  "157V3CZI00SK" => "LOTS 1 2 3 5 8 11 14 17 TO 23",
  "157V3CZHZUU3" => "LOTS 1 2 3 4 LYING W OF I-95 R/W",
  "157V3CZHZYA1" => "LOTS 1 2 3 4",
  "157V3CZI0ARG" => "LOTS 1 2 3",
  "157V3CZI0O3A" => "LOTS 1 2",
  "157V3CZHZRNE" => "LOT 4 SUBD OF NE 1/4 OF NE 1/4",
  "157V3CZHZPCM" => "LOT 32 SUBD",
  "157V3CZHZMU6" => "LOT 32 EX N 890 FT AND S 100 FT",
  "157V3CZHZK79" => "LOT 32 AS PER PB 1 PG 166",
  "157V3CZHZGO8" => "LOT 31 SUBD",
  "157V3CZHZDMD" => "LOT 30 SUBD",
  "157V3CZHZ4H6" => "LOT 17 OF PB 1 PG 165 AS DES IN",
  "157V3CZHYYZB" => "LOT 1 EX RR AND RD R/WS",
  "157V3CZHYSNI" => "LOT 1 EX RR AND HWY R/W",
  "157V3CZHYPC4" => "LOT 1 EX N 985.92 FT AND ALL LOTS",
  "157V3CZHYL53" => "LOT 1 AND NE 1/4 OF NE 1/4 PAR 2",
  "157V3CZHZ1RE" => "LOT 1",
  "15A2V3I9SWK4" => "LOST LAKES CONDOMINIUM PH I",
  "157V3CZI5ABI" => "MAGNOLIA BAY CONDO PH II",
  "157V3CZI575O" => "MAGNOLIA BAY CONDO PH I",
  "157V3CZI54II" => "MAGGIE JOHNSONS PLAT OF MELBOURNE BEACH RESUBD OF",
  "157V3CZI51GQ" => "MAEMIR ESTATES",
  "157V3CZI4YOZ" => "MADISON TOWNHOMES",
  "157V3CZI4V67" => "MADISON SQUARE CONDOMINIUM",
  "157V3CZI4RRX" => "MADISON RIVERFRONT ESTATES",
  "157V3CZI4OVR" => "MACAW CREST",
  "157V3CZI4I37" => "MAC ARTHUR HEIGHTS 1ST ADDN",
  "157V3CZI4LWW" => "MAC ARTHUR HEIGHTS",
  "157V3CZI3XOL" => "MAC ADAM SUBD",
  "157V3CZI3U41" => "LYTTON ESTATES",
  "157V3CZI3QYL" => "LYNNWOOD SUBDIVISION",
  "157V3CZI3MWW" => "LYME BAY SEC 4",
  "157V3CZI3H9T" => "LYME BAY SEC 3 PART 2",
  "157V3CZI3K3T" => "LYME BAY SEC 3",
  "157V3CZI3C5D" => "LYME BAY SEC 2",
  "157V3CZI374R" => "LYME BAY SEC 1",
  "157V3CZI34O7" => "LUND GARDENS",
  "157V3CZI31UW" => "LUNA HEIGHTS",
  "157V3CZI2YLX" => "LUCILLE GARDENS SUBD",
  "157V3CZI2VTM" => "LUCAS TERRACE SEC 2",
  "157V3CZI2T1B" => "LUCAS TERRACE SEC 1",
  "157V3CZI2QF4" => "LOVERIDGE HEIGHTS SUBD",
  "157V3CZI2NGS" => "LOVERIDGE HEIGHTS ADDN 3",
  "157V3CZI2KF4" => "LOVERIDGE HEIGHTS ADDN 2 AMENDED PLAT OF",
  "157V3CZI2GZ8" => "LOVERIDGE HEIGHTS ADDN 1",
  "157V3CZI2E1B" => "LOTS 31 32 SUBD",
  "157V3CZI2BMN" => "LOTS 31 32 OF PB 1 PG 165 EX ORB",
  "157V3CZI28KS" => "LOTS 3 TO 14 19 TO 24 27 TO 30",
  "157V3CZI258I" => "LOTS 29 30 LYING E OF I-95 R/W",
  "157V3CZI22CD" => "LOTS 19 20 29 30 EX RD AND CANAL",
  "157V3CZI1Z6V" => "LOTS 17 TO 32 OF PB 1 PG 166",
  "157V3CZI1W39" => "LOTS 17 TO 32 OF PB 1 PG 165 EX",
  "157V3CZI1TBM" => "LOTS 17 TO 20 29 TO 32 OF PB 1 PG",
  "157V3CZI1QDY" => "LOTS 17 TO 20 22 TO 32 EX I-95",
  "157V3CZI8W0R" => "MANCHESTER LAKES PHASE ONE",
  "17YIW14IRNDJ" => "MANCHESTER LAKES PHASE FOUR",
  "1AQ2KY7UB1JJ" => "MANCHESTER LAKES PHASE FIVE",
  "157V3CZI8Q5D" => "MANATEE POINTE RESERVE",
  "157V3CZI8MN5" => "MANATEE POINTE CONDO",
  "157V3CZI8J3F" => "MANATEE CONDO",
  "157V3CZI8G1Q" => "MALLARD LAKES SUBD",
  "157V3CZI87I0" => "MALABAR ROCK POINT ESTATES",
  "157V3CZI81DY" => "MALABAR LAKES WEST SUBDIVISION PHASE 2",
  "157V3CZI844V" => "MALABAR LAKES WEST SUBDIVISION PHASE 1A",
  "157V3CZI7MO6" => "MAJORCA SOUTH PHASE ONE",
  "157V3CZI7JO0" => "MAJESTY PALM CONDO",
  "157V3CZI7H0Z" => "MAJESTIC SHORES A CONDO",
  "157V3CZI7DGO" => "MAJESTIC SEAS CONDO",
  "157V3CZI7AA1" => "MAJESTIC BAY TOWNHOMES CONDO",
  "157V3CZI77KB" => "MAJESTIC BAY CONDO",
  "157V3CZI74BB" => "MAGNOLIA VILLAGE III",
  "157V3CZI70W1" => "MAGNOLIA VILLAGE II",
  "157V3CZI6XTB" => "MAGNOLIA VILLAGE I",
  "157V3CZI6UNI" => "MAGNOLIA SPRINGS PHASE TWO",
  "157V3CZI6RB1" => "MAGNOLIA SPRINGS PHASE THREE",
  "157V3CZI6OQ3" => "MAGNOLIA SPRINGS PHASE ONE",
  "157V3CZI6L8M" => "MAGNOLIA POINTE UNIT TWO",
  "157V3CZI6IGR" => "MAGNOLIA POINTE UNIT ONE",
  "157V3CZI6COW" => "MAGNOLIA PARK REPLAT REPLAT OF PT OF",
  "157V3CZI6G2I" => "MAGNOLIA PARK REPLAT",
  "157V3CZI69XA" => "MAGNOLIA PARK PLAT NO 2",
  "157V3CZI66S7" => "MAGNOLIA PARK AT BAYSIDE LAKES",
  "157V3CZI64BA" => "MAGNOLIA MANOR",
  "157V3CZI61LZ" => "MAGNOLIA LAKES PHASE 3",
  "157V3CZI5YPP" => "MAGNOLIA LAKES PHASE 2-B",
  "157V3CZI5VN4" => "MAGNOLIA LAKES PHASE 2-A",
  "157V3CZI5SYD" => "MAGNOLIA LAKES PHASE 1",
  "157V3CZI5JII" => "MAGNOLIA KEY CONDO",
  "157V3CZI5GHM" => "MAGNOLIA HEIGHTS",
  "157V3CZI5DM7" => "MAGNOLIA BAY CONDO PH III",
  "157V3CZIBWVM" => "MARTY MANOR",
  "157V3CZIBTX7" => "MARTESIA",
  "157V3CZIBQRL" => "MARSH HARBOR",
  "157V3CZIBNVN" => "MARLIN SUBD",
  "157V3CZIBKR8" => "MARLIN APT CONDO",
  "157V3CZIBHN0" => "MARKS LANDING",
  "157V3CZIBF70" => "MARKO VILLAS APTS CONDO",
  "157V3CZIBC0U" => "MARKETCENTER VIERA",
  "157V3CZIB8JG" => "MARK TWAIN SUBDIVISION",
  "157V3CZIB4XH" => "MARITIME HAMMOCK",
  "157V3CZIB1II" => "MARINER SQUARE OFFICE CONDO",
  "157V3CZIAYXI" => "MARINA VILLAGE CONDO PH VI",
  "157V3CZIAWAK" => "MARINA VILLAGE CONDO PH V",
  "157V3CZIATE9" => "MARINA VILLAGE CONDO PH III",
  "157V3CZIAQLN" => "MARINA VILLAGE CONDO PH II",
  "157V3CZIANUD" => "MARINA VILLAGE CONDO PH I",
  "157V3CZIAKMB" => "MARINA ISLES CLUB CONDO UNIT 1 BLDG 3",
  "157V3CZIAHKD" => "MARINA ISLES CLUB CONDO UNIT 1 BLDG 2",
  "157V3CZIAEMJ" => "MARINA ISLES CLUB CONDO UNIT 1 BLDG 1",
  "157V3CZIABJE" => "MARIE ANN VILLAS",
  "157V3CZIA924" => "MARIANNA OAKS",
  "157V3CZIA1PH" => "MARBELLA TOWNHOMES PHASE 2",
  "157V3CZIA614" => "MARBELLA TOWNHOMES",
  "157V3CZI9YVM" => "MAR BLEU CONDO",
  "157V3CZI9RR4" => "MAPLEWOOD SUBD 3RD ADDN",
  "157V3CZI9O3M" => "MAPLEWOOD SUBD 2ND ADDN",
  "157V3CZI9LGC" => "MAPLEWOOD SUBD 1ST ADDN",
  "157V3CZI9V30" => "MAPLEWOOD SUBD",
  "157V3CZI9IS9" => "MAPLEWOOD LAKES PHASE 2",
  "157V3CZI9G7G" => "MAPLEWOOD LAKES PHASE 1",
  "157V3CZI9D0H" => "MAPLEWOOD ESTATES",
  "157V3CZI9A11" => "MANTONHURST SUBD",
  "157V3CZI97JF" => "MANDARIN LAKES UNIT TWO",
  "157V3CZI94DG" => "MANDARIN LAKES UNIT ONE",
  "157V3CZI91G0" => "MANDALAY GROVE SUBDIVISION",
  "157V3CZI8Z04" => "MANCHESTER LAKES PHASE TWO",
  "157V3CZI8ST0" => "MANCHESTER LAKES PHASE THREE",
  "157V3CZIFHCQ" => "MELBOGALLIE SUBD",
  "157V3CZIFDZF" => "MEFFORDS SUBD",
  "157V3CZIFALU" => "MEDICAL SPECIALISTS CONDO",
  "157V3CZIF7C8" => "MEDICAL ARTS BLDG OF MELBOURNE CONDO",
  "157V3CZIF36N" => "MEADOWS-SEC 2 THE",
  "157V3CZIF0SL" => "MEADOWS WEST THE",
  "157V3CZIEY54" => "MEADOWS SOUTH-SEC 3 THE",
  "157V3CZIEUV7" => "MEADOWS SOUTH-SEC 2 THE",
  "157V3CZIESJN" => "MEADOWS SOUTH-SEC 1 THE",
  "157V3CZIEOZX" => "MEADOWS SEC 1 THE",
  "157V3CZIEM34" => "MEADOWS EAST THE",
  "157V3CZIEH23" => "MEADOWRIDGE I",
  "157V3CZIEJRD" => "MEADOWRIDGE",
  "157V3CZIEE97" => "MEADOWBROOK SUBDIVISION",
  "157V3CZIEB4T" => "MEADOWBROOK SOUTH CONDO",
  "157V3CZIE88A" => "MEADOW-WOOD",
  "17XMNBBRTYQN" => "MEADOW POINTE CONDO PH VII",
  "157V3CZIE5GV" => "MEADOW POINTE CONDO PH VI",
  "157V3CZIE26A" => "MEADOW POINTE CONDO PH V",
  "157V3CZIDZ9S" => "MEADOW POINTE CONDO PH III",
  "157V3CZIDWGA" => "MEADOW POINTE CONDO PH II",
  "157V3CZIDTFG" => "MEADOW POINTE CONDO PH I",
  "157V3CZIDQNM" => "MEADOW POINT",
  "157V3CZIDNN8" => "MEADOW LAKE SUBDIVISION PHASE TWO",
  "157V3CZIDH21" => "MEADOW LAKE SUBDIVISION PHASE ONE",
  "157V3CZID9PP" => "MEADOW CREEK",
  "157V3CZICX70" => "MEADOW COVE",
  "157V3CZICU7M" => "MCARTHURS UNRECORDED SUBD PT OF TRACT 12 ATKINSON",
  "157V3CZICPJ0" => "MC DONALDS SUBDIVISION",
  "157V3CZICM78" => "MAYFIELD SUBD",
  "157V3CZICJEX" => "MAYFAIR MANOR LITTLE EST",
  "157V3CZICGBB" => "MAY-PALM ESTATES",
  "157V3CZICD69" => "MATTHEW PARK",
  "157V3CZIC6CF" => "MASTERS HEIGHTS ADDN 1",
  "157V3CZIC90H" => "MASTERS HEIGHTS",
  "157V3CZIC34L" => "MASEKS SUBD UNIT TWO",
  "157V3CZIBZPZ" => "MASEKS SUBD UNIT ONE",
  "157V3CZII5QM" => "MELBOURNE PINES",
  "157V3CZII2MC" => "MELBOURNE OCEAN CLUB CONDO",
  "157V3CZIHZZH" => "MELBOURNE MANOR SEC F RESUBD OF OF LOTS 18 19 AND",
  "157V3CZIHX1T" => "MELBOURNE MANOR SEC F RESUBD OF LOTS 18 19 AND 20",
  "157V3CZIHTWL" => "MELBOURNE MANOR SEC F RESUBD OF LOTS 18 19 20 OF P",
  "157V3CZIHQTT" => "MELBOURNE MANOR SEC E RESUBD OF LOTS 14 OF PB 1 PG",
  "157V3CZIHNRN" => "MELBOURNE MANOR SEC E RESUBD OF LOT 16 OF PB 1 PG",
  "157V3CZIHKUD" => "MELBOURNE MANOR SEC E RESUBD OF LOT 15",
  "157V3CZIHI1Y" => "MELBOURNE MANOR SEC E RESUBD OF LOT 13 OF PB 1 PG",
  "157V3CZIHF00" => "MELBOURNE MANOR SEC D RESUBD OF LOT 29 OF PB 1 PG",
  "157V3CZIHCMK" => "MELBOURNE MANOR SEC CRESUBD OF LOTS 9 10 OF PB 1 P",
  "157V3CZIH9R8" => "MELBOURNE MANOR SEC C RESUBD OF LOT 10 OF PB 1 PG",
  "157V3CZIH3C4" => "MELBOURNE MANOR SEC B RESUBD OF LOT 2 OF PB 1 PG 1",
  "157V3CZIH6R9" => "MELBOURNE MANOR SEC B RESUBD OF",
  "157V3CZIH0RC" => "MELBOURNE MANOR SEC A RESUBD OF LOT 27 OF PB 1 PG",
  "157V3CZIGX7V" => "MELBOURNE MANOR SEC A RESUBD OF LOT 26 OF PB 1 PG",
  "157V3CZIGUEE" => "MELBOURNE MANOR SEC A RESUBD OF LOT 25 OF PB 1 PG",
  "157V3CZIGR48" => "MELBOURNE HEIGHTS SEC E",
  "157V3CZIGOAF" => "MELBOURNE HEIGHTS SEC D",
  "157V3CZIGL9E" => "MELBOURNE HEIGHTS SEC C",
  "157V3CZIGI8Z" => "MELBOURNE HEIGHTS SEC B",
  "157V3CZIGFS4" => "MELBOURNE HEIGHTS SEC A",
  "157V3CZIGD99" => "MELBOURNE HARBOR CONDO",
  "157V3CZIG7X7" => "MELBOURNE GARDENS UNIT NO 2",
  "157V3CZIGAI0" => "MELBOURNE GARDENS",
  "157V3CZIG1YL" => "MELBOURNE ESTATES ADDN NO 1",
  "157V3CZIG53I" => "MELBOURNE ESTATES",
  "157V3CZIFZBA" => "MELBOURNE COURT",
  "157V3CZIFTUL" => "MELBOURNE BEACH SOUTH REPLAT OF SHEETS 1 OF 2",
  "157V3CZIFWN4" => "MELBOURNE BEACH SOUTH",
  "157V3CZIFPW9" => "MELBOURNE BEACH OCEAN VIEW TOWNHOMES CONDO",
  "157V3CZIFN61" => "MELBOURNE BEACH CONDO",
  "157V3CZIKYKY" => "MERRITT PARK PLACE",
  "157V3CZIKSI5" => "MERRITT MEDICAL CENTER II CONDO",
  "157V3CZIKVHU" => "MERRITT MEDICAL CENTER",
  "157V3CZIKPF8" => "MERRITT LAKES ESTS REPLAT OF",
  "157V3CZIKMXG" => "MERRITT ISLAND VILLAGE",
  "157V3CZIKKIC" => "MERRITT ISLAND TROPIC PARK S/D",
  "157V3CZIKHFI" => "MERRITT ISLAND PROF CENTER OFFICE CONDO",
  "157V3CZIKE9I" => "MERRITT ISLAND CO-OP APTS INC",
  "157V3CZIKAW1" => "MERRITT COURT TOWNHOMES",
  "157V3CZIK6VD" => "MERRITT CITY RESUBD",
  "157V3CZIK43R" => "MERRITT APARTMENT SITES",
  "181DCHSMCCQ7" => "MERIDIAN RIVERFRONT",
  "1AQZWVAEIU9K" => "MERIDIAN PLACE",
  "157V3CZIK1FQ" => "MERCEDES PLAZA",
  "157V3CZIJYXN" => "MENTZELL SUBD",
  "157V3CZIJW2U" => "MENTZELL RESUBD OF",
  "157V3CZIJTDT" => "MELROSE MANOR UNIT 3",
  "157V3CZIJQDN" => "MELROSE MANOR UNIT 2",
  "157V3CZIJNP1" => "MELROSE MANOR UNIT 1",
  "157V3CZIJKKM" => "MELRIDGE MANOR SUBD",
  "157V3CZIJI1X" => "MELODY ESTATES CONDO PH II",
  "157V3CZIJEZM" => "MELODY ESTATES CONDO PH I",
  "157V3CZIJBXI" => "MELBOURNE VILLAGE GARDENS UNIT 1",
  "157V3CZIJ92H" => "MELBOURNE VILLAGE GARDENS PLAT OF UNIT NO 1",
  "157V3CZIJ5RJ" => "MELBOURNE VILLAGE 6TH SEC",
  "157V3CZIJ237" => "MELBOURNE VILLAGE 5TH SEC",
  "157V3CZIIXGF" => "MELBOURNE VILLAGE 4TH SEC",
  "157V3CZIIUM6" => "MELBOURNE VILLAGE 3RD SEC",
  "157V3CZIIRND" => "MELBOURNE VILLAGE 2ND SEC",
  "157V3CZIIOL3" => "MELBOURNE VILLAGE 1ST SEC REPLAT",
  "157V3CZIIL29" => "MELBOURNE SHORES TOWNHOMES",
  "157V3CZIIHVY" => "MELBOURNE SHORES SUBD",
  "157V3CZIIES4" => "MELBOURNE SHORES 2ND ADDN",
  "157V3CZIIBSY" => "MELBOURNE SHORES 1ST ADDN",
  "157V3CZIFK3X" => "MELBOURNE RESUBD OF PT OF BLK 24",
  "157V3CZII95T" => "MELBOURNE POULTRY COLONY ADD NO 1",
  "157V3CZIO1XE" => "MICHIGAN BEACH 9TH ADDN SEC 3",
  "157V3CZINYUJ" => "MICHIGAN BEACH 9TH ADDN SEC 2",
  "157V3CZIO58L" => "MICHIGAN BEACH 9TH ADDN",
  "157V3CZINVYR" => "MICHIGAN BEACH 8TH ADDN",
  "157V3CZINTJC" => "MICHIGAN BEACH 7TH ADDN TO",
  "157V3CZINQDE" => "MICHIGAN BEACH 6TH ADDN TO",
  "157V3CZINKY6" => "MICHIGAN BEACH 5TH ADDN REPLAT",
  "157V3CZINNR1" => "MICHIGAN BEACH 5TH ADDN",
  "157V3CZINHXP" => "MICHIGAN BEACH 4TH ADDN TO",
  "157V3CZINEP3" => "MICHIGAN BEACH 3RD ADDN TO",
  "157V3CZINBYE" => "MICHIGAN BEACH 2ND ADDN TO",
  "157V3CZIN9HF" => "MICHIGAN BEACH 1ST ADDN TO",
  "157V3CZIN65R" => "MICHIGAN BEACH 12TH ADDN",
  "157V3CZIN2UG" => "MICHIGAN BEACH 11TH ADDN TO",
  "157V3CZIN0AB" => "MICHIGAN BEACH 10TH ADDN TO",
  "157V3CZIMUHL" => "MICHELINA CONDOMINIUM",
  "157V3CZIMRI6" => "MICHAELS SUBDIVISION",
  "157V3CZIMIIR" => "MICCO SUBD",
  "157V3CZIMFLZ" => "MG PROFESSIONAL CENTER CONDO",
  "157V3CZIM9KW" => "MEYERS ADD TO MELBOURNE RESUBD",
  "157V3CZIMCKJ" => "MEYERS ADD TO MELBOURNE",
  "157V3CZIM6H0" => "METCALFS UNRECORDED PLAT OF BALEM ALLEN TRACT",
  "157V3CZIM3CH" => "METCALFS UNREC PLAT OF WRIGHT BROTHERS TRACT",
  "157V3CZIM0TY" => "METCALFS ADD TO MELBOURNE",
  "157V3CZILXJV" => "MERRITT WINTER HOMES DEV SUBD MAP NO 2",
  "157V3CZILPO7" => "MERRITT WINTER HOME DEVELOPMENT SUBD MAP 1",
  "157V3CZILTOT" => "MERRITT WINTER HOME DEVELOPMENT",
  "157V3CZILN0C" => "MERRITT TOWERS CONDO PH III",
  "157V3CZILJ8A" => "MERRITT TOWERS CONDO PH II",
  "157V3CZILG1P" => "MERRITT TOWERS CONDO PH I",
  "157V3CZILCWD" => "MERRITT RIDGE SUBD",
  "157V3CZILADU" => "MERRITT RIDGE SHEET 5",
  "157V3CZIL6OT" => "MERRITT RIDGE SHEET 4",
  "157V3CZIL49T" => "MERRITT RIDGE SHEET 3",
  "157V3CZIL1PD" => "MERRITT RIDGE SHEET 2",
  "157V3CZIR8OJ" => "MONTECITO PHASE 1A",
  "158ZIUH6XNNC" => "MONTECITO",
  "157V3CZIR580" => "MONTCLAIR",
  "157V3CZIR1OB" => "MONACO ESTATES PHASE TWO",
  "157V3CZIQZ54" => "MONACO ESTATES PHASE ONE",
  "157V3CZIQVZN" => "MONACO CONDO PH II AND III",
  "157V3CZIQSUU" => "MONACO CONDO PH 1",
  "157V3CZIQPOS" => "MONA PARK SUBD",
  "157V3CZIQMYA" => "MODEL INDIAN RIVER GARDENS",
  "157V3CZIQKH9" => "MOCKINGBIRD HILL UNIT 5",
  "157V3CZIQHAH" => "MOCKINGBIRD HILL UNIT 4",
  "157V3CZIQDYF" => "MOCKINGBIRD HILL UNIT 3",
  "157V3CZIQ30Z" => "MOCKINGBIRD HILL UNIT 1 1ST REP",
  "157V3CZIQB95" => "MOCKINGBIRD HILL UNIT 1",
  "157V3CZIPZEQ" => "MITCHELL MANOR",
  "157V3CZIPWU1" => "MISTY WAY PHASE II P.U.D.",
  "157V3CZIPTRW" => "MISTY WAY PHASE I P.U.D.",
  "157V3CZIPRAL" => "MISTY SHORES CONDO",
  "157V3CZIPO0A" => "MISTY CREEK UNIT TWO",
  "157V3CZIPL9F" => "MISTY CREEK UNIT ONE",
  "157V3CZIPI3V" => "MISSION LAKE VILLAS UNIT TWO",
  "157V3CZIPF1J" => "MISSION LAKE VILLAS UNIT ONE",
  "157V3CZIPBPG" => "MISSION BAY APARTMENTS PARCEL H VIERA NORTH PUD",
  "157V3CZIP7VH" => "MISSION ACRES",
  "157V3CZIP4VQ" => "MIRA BELLA CONDO",
  "157V3CZIP0BY" => "MINUTEMEN VILLAS CONDO",
  "157V3CZIOWKR" => "MINTONS CORNERS REPLAT OF",
  "157V3CZIOTD2" => "MINTONS CORNER SUBD NO 2",
  "157V3CZIOPSG" => "MINTONS CORNER SUBD NO 1 REPLAT OF",
  "157V3CZION7U" => "MINNIEHAHA GARDENS",
  "157V3CZIOK4C" => "MIMA OF MELBOURNE",
  "157V3CZIOGUN" => "MILFORD POINT BEACH SUBD",
  "157V3CZIOE56" => "MILAN SUBDIVISION",
  "157V3CZIOAV3" => "MICHIGAN BEACH SUBD",
  "157V3CZIMXPO" => "MICHIGAN BEACH REPLAT OF PT OF",
  "157V3CZIO7V9" => "MICHIGAN BEACH REPLAT OF PART OF 3RD ADD TO",
  "157V3CZIUH6P" => "N 1/2 AND SW 1/4 EX LANDS DEEDED",
  "157V3CZIUDRY" => "N 1/2 AND LAND IN SEC 6 AS DES IN",
  "157V3CZIU9HC" => "N 1/10 OF E 1/2 OF E 1/2 OF",
  "157V3CZIU6T0" => "MYRTLE PARK SUBD",
  "157V3CZIU44B" => "MYLES ESTATE AMENDED PLAT",
  "157V3CZITYTP" => "MYERS UNREC SUBD",
  "157V3CZITWBB" => "MURRELL VILLAS CONDO",
  "157V3CZITTLC" => "MURRELL PROFESSIONAL PARK CONDO",
  "157V3CZITQP0" => "MURRELL CROSSING CONDO",
  "157V3CZITNZE" => "MURRELL BARNES CROSSING",
  "157V3CZITKVG" => "MURDOCK SUBD",
  "157V3CZITEJ3" => "MUIRFIELD VILLAS",
  "157V3CZITA9U" => "MRS A L SMITHS SUBD",
  "157V3CZIT72X" => "MOSSWOOD",
  "157V3CZIT0HM" => "MOSLEY GARDENS REPLAT OF BLK B",
  "157V3CZIT3S6" => "MOSLEY GARDENS",
  "157V3CZISXNG" => "MORROWS SUBD OF LOT 2",
  "157V3CZISPMW" => "MORNINGSIDE PARK SEC B",
  "157V3CZISN7B" => "MORNINGSIDE PARK SEC A",
  "157V3CZISHLK" => "MORNINGSIDE MOBILE HOME ESTATES NORTH",
  "157V3CZISKOE" => "MORNINGSIDE MOBILE HOME ESTATES",
  "157V3CZISBEC" => "MORNINGSIDE HEIGHTS RESUBD",
  "157V3CZIS8OT" => "MORNINGSIDE HEIGHTS PART OF ESTATE NO 1 REPLAT OF",
  "157V3CZISEUU" => "MORNINGSIDE HEIGHTS",
  "157V3CZIS5TW" => "MORNINGSIDE BEACH SUBD",
  "157V3CZIS319" => "MORNINGSIDE BEACH RESUBD OF PT OF",
  "157V3CZISTSC" => "MORNINGSIDE",
  "157V3CZIS01B" => "MORGAN MANOR CONDO",
  "157V3CZIRWYV" => "MOORINGS UNIT 2 THE",
  "157V3CZIRTVF" => "MOORINGS SUBD THE",
  "157V3CZIRR62" => "MOORE PARK",
  "157V3CZIROG8" => "MOORE AND CANTINES SUBD OF PT OF LOT 2 AND ALL LOT",
  "157V3CZIRLF6" => "MONTEREY COVE AT BAYSIDE LAKES",
  "157V3CZIRIKI" => "MONTECITO PHASE 2B",
  "157V3CZIRFCF" => "MONTECITO PHASE 2A A REPLAT BEING A REPLAT OF PB",
  "157V3CZIRBPS" => "MONTECITO PHASE 1B",
  "157V3CZIXN1S" => "N 1/2 OF NW 1/4",
  "157V3CZIX5IO" => "N 1/2 OF NE 1/4 OF NE 1/4 OF",
  "157V3CZIWZRI" => "N 1/2 OF NE 1/4 OF NE 1/4 EX RD",
  "157V3CZIWWEX" => "N 1/2 OF NE 1/4 OF NE 1/4 EX N",
  "157V3CZIX2CT" => "N 1/2 OF NE 1/4 OF NE 1/4 EX",
  "157V3CZIX80V" => "N 1/2 OF NE 1/4 OF NE 1/4",
  "157V3CZIWTPZ" => "N 1/2 OF NE 1/4 LYING E OF U.S.",
  "157V3CZIWR63" => "N 1/2 OF NE 1/4 EXC PB 33 PG 63",
  "157V3CZIWOJ4" => "N 1/2 OF NE 1/4 EX E 70 FT",
  "157V3CZIWIJB" => "N 1/2 OF NE 1/4 E 1/2 OF SW 1/4",
  "157V3CZIWLYO" => "N 1/2 OF NE 1/4 AND EX ORB 2392",
  "157V3CZIXB3Y" => "N 1/2 OF NE 1/4",
  "157V3CZIWEUS" => "N 1/2 OF N 176.51 FT OF GOVT LOT",
  "157V3CZIWCGZ" => "N 1/2 OF N 1/2 OF SE 1/4",
  "157V3CZIW93L" => "N 1/2 OF N 1/2 OF S 1/2 OF LOT 32",
  "157V3CZIW5LE" => "N 1/2 OF N 1/2 AND SW 1/4 OF NW 1/4",
  "157V3CZIW2BU" => "N 1/2 OF LOT 32 OF FLORIDA INDIAN",
  "157V3CZIVZRU" => "N 1/2 OF LOT 2 W OF RR AND ALL LOTS",
  "157V3CZIVWXT" => "N 1/2 OF LOT 1 EX RR R/W",
  "157V3CZIVTAW" => "N 1/2 OF GOVT LOT 3 EXC ORB",
  "157V3CZIVOWA" => "N 1/2 OF GOVT LOT 1 EX RR R/W",
  "157V3CZIVLZU" => "N 1/2 OF E 1/2 OF NE 1/4 OF",
  "157V3CZIVISI" => "N 1/2 OF E 1/2 OF LOT 30 EX ST RD",
  "157V3CZIVFLT" => "N 1/2 N OF SEBASTIAN MAIN CANAL",
  "157V3CZIVBXI" => "N 1/2 LYING W OF EAST SHORE OF",
  "157V3CZIV92J" => "N 1/2 EXC ORB 523 PG 343 1260 PG",
  "157V3CZIV5T5" => "N 1/2 EX SOUTH OF NEW LEVEE LINE",
  "157V3CZIV2RQ" => "N 1/2 EX RR R/W AND LANDS DEEDED",
  "157V3CZIUZJK" => "N 1/2 EX ORB 442 PG 224 2607 PG",
  "157V3CZIUW2C" => "N 1/2 EX ORB 2564 PG 2979 AND I-95",
  "157V3CZIUT8J" => "N 1/2 EX I-95 RW AND LANDS DEEDED",
  "157V3CZIUQK8" => "N 1/2 EX I-95 R/W",
  "157V3CZIUNTS" => "N 1/2 EX HWY R/W",
  "157V3CZIUKPS" => "N 1/2 EX DRAINAGE AND HWY R/W",
  "157V3CZJ0HAU" => "N 250 FT OF NE 1/4 OF NE 1/4",
  "157V3CZJ0CKP" => "N 25 FT OF S 1/2 OF GOVT LOT 3",
  "157V3CZJ09XE" => "N 227.29 FT OF E 128 FT OF W 778",
  "157V3CZJ0641" => "N 200 FT E OF ST JOHNS RIVER",
  "157V3CZJ02SC" => "N 2/3 OF NE 1/4 OF NE 1/4 EX S",
  "157V3CZJ009A" => "N 2 AC OF GOVT LOT 1 SEC 33",
  "157V3CZIZWYK" => "N 1816.15 FT EX E 658.91 FT OF N",
  "157V3CZIZTXH" => "N 1680 FT OF SEC 14 LYING E OF",
  "157V3CZIZPSH" => "N 165 FT OF E 325 FT OF GOVT",
  "157V3CZIZMTN" => "N 160 FT OF LOT 1 SEC 27 AND S 40",
  "157V3CZIZJSX" => "N 150 FT OF GOVT LOT 1 LYING W",
  "157V3CZIZGMX" => "N 1440 FT E OF SYKES CREEK AS DES",
  "157V3CZIZDPT" => "N 141.5 FT OF GOVT LOT 1 AS DES",
  "157V3CZIZARV" => "N 12 AC OF FRAC SEC 9",
  "157V3CZIZ789" => "N 11/16 OF SEC 3 W OF I-95",
  "157V3CZIZ3UK" => "N 100 FT OF S 820 FT OF N 2610 FT",
  "157V3CZIZ104" => "N 100 FT OF S 28 1/2 AC OF LOT 1",
  "157V3CZIYXFF" => "N 100 FT OF S 2055 FT OF GOVT LOT",
  "157V3CZIYTZE" => "N 100 FT OF NW 1/4 OF NW 1/4 E",
  "157V3CZIYQT2" => "N 100 FT OF LOT 1 E OF ST RD",
  "157V3CZIYNPA" => "N 100 FT OF GOVT LOT 1 LYING WEST",
  "157V3CZIYKCR" => "N 100 FT OF E 100 FT OF NE 1/4 OF",
  "157V3CZIY2VX" => "N 1/4 OF NE 1/4 S 3/4 OF W 1/4",
  "157V3CZIY6PV" => "N 1/4 OF NE 1/4 OF NE 1/4 EX ORB",
  "157V3CZIYB92" => "N 1/4 OF NE 1/4 OF NE 1/4 EX",
  "157V3CZIYDZW" => "N 1/4 OF NE 1/4 OF NE 1/4",
  "157V3CZIXZUW" => "N 1/4 OF LOT 32 EX RD R/W OF PB 1",
  "157V3CZIXWQT" => "N 1/4 EX CIVIL BOOK 54 PG 838",
  "157V3CZIXTPK" => "N 1/4 E OF CO RD EX ORB 16 PG 420",
  "157V3CZIYHPD" => "N 1/4",
  "157V3CZIXQ8O" => "N 1/2 OF WM GARVIN GRANT EX PB 7",
  "157V3CZIXKC4" => "N 1/2 OF NW 1/4 OF NW 1/4 OF",
  "157V3CZIXH4L" => "N 1/2 OF NW 1/4 OF NE 1/4 EX ORB",
  "157V3CZIXDVS" => "N 1/2 OF NW 1/4 LYING W OF A1A EX",
  "157V3CZJ3W3M" => "NASA CORPORATE CENTRE PHASE II",
  "157V3CZJ3SVU" => "NASA CORPORATE CENTRE PHASE 1",
  "157V3CZJ3P2E" => "NANTUCKET SQUARE COMMERCIAL CONDO",
  "157V3CZJ3MH8" => "N 800 FT OF E 800 FT OF NE 1/4",
  "157V3CZJ3E89" => "N 80 FT OF S 230 FT OF LOT 1",
  "157V3CZJ3AH6" => "N 75 FT OF LOT 1 E OF HWY",
  "157V3CZJ370G" => "N 7.5 FT OF S 632.5 FT OF LOT 2",
  "157V3CZJ33HA" => "N 660 FT OF N 1/4",
  "157V3CZJ307K" => "N 64 FT E OF HWY OF NE 1/4 OF",
  "157V3CZJ2XJR" => "N 501.42 FT OF LOTS 1 2 3 LYING W",
  "157V3CZJ2TN5" => "N 501.42 FT OF LOT 1 E OF MULLET",
  "157V3CZJ2Q4Q" => "N 500 FT OF NE 1/4 LYING E OF RD",
  "157V3CZJ2MC3" => "N 5/8 OF N 1/2 EX W 50 FT E 50",
  "157V3CZJ2IKS" => "N 5 A OF W 10 A OF NW 1/4 OF",
  "157V3CZJ2EX0" => "N 425 FT OF E 318 FT OF NW 1/4 OF",
  "157V3CZJ2BC4" => "N 400 FT OF LOT 1 W OF RD A1A",
  "157V3CZJ28GZ" => "N 33 FT OF VAC JOURET",
  "157V3CZJ24V3" => "N 311.57 FT OF GOVT LOTS 3 AND 4 W",
  "157V3CZJ21HH" => "N 300 FT OF LOT 1 E OF A1A",
  "157V3CZJ0YR6" => "N 3.25 AC OF NE 1/4 OF NE 1/4",
  "157V3CZJ1VGG" => "N 3/4 OF NE 1/4 OF NE 1/4 E OF RR",
  "157V3CZJ1RRW" => "N 3/4 OF NE 1/4 LYING E OF SR",
  "157V3CZJ1NZ4" => "N 3/4 OF E 1/2 EX W 1/2 OF E 1/2",
  "157V3CZJ1KJG" => "N 3/4 EX SOUTH OF NEW LEVEE LINE",
  "157V3CZJ1GS0" => "N 3/4 EX RD R/W",
  "157V3CZJ1C5H" => "N 3/4 EX I-95 R/W ST RD 407 R/W",
  "157V3CZJ18GB" => "N 3/4 E OF I-95 R/W AND THAT PART",
  "157V3CZJ1501" => "N 3/4 E OF HWY R/WS",
  "157V3CZJ12GJ" => "N 3/4 AND N 3/4 OF N 1/2 OF N 1/2",
  "157V3CZJ1YZD" => "N 3/4",
  "157V3CZJ0V6W" => "N 3 A OF LOT 1 EX DB 422 PG 422",
  "157V3CZJ0RMG" => "N 296.88 FT OF GOVT LOT 1 E OF",
  "157V3CZJ0O89" => "N 277.7 FT OF GOVT LOT 1 E OF HWY",
  "157V3CZJ0L7J" => "N 26.9 AC OF LOT 1 EX RD R/W",
  "157V3CZJ74O9" => "NE 1/4 OF NE 1/4 EX AS DES IN DB",
  "157V3CZJ70UU" => "NE 1/4 OF NE 1/4 E OF FECRR EX",
  "157V3CZJ6X9Q" => "NE 1/4 OF NE 1/4 AND SE 1/4 OF",
  "157V3CZJ6U7D" => "NE 1/4 OF NE 1/4 AND S 50 FT OF W",
  "157V3CZJ6REC" => "NE 1/4 OF NE 1/4 AND PT OF SE 1/4",
  "157V3CZJ6NLF" => "NE 1/4 OF NE 1/4 AND ORB 872 PG",
  "157V3CZJ6JKV" => "NE 1/4 OF NE 1/4 AND N 1/2 OF SE",
  "157V3CZJ87HI" => "NE 1/4 OF NE 1/4",
  "157V3CZJ4V1J" => "NE 1/4 N 3/8 OF SE 1/4 AND GOVT",
  "157V3CZJ6D8F" => "NE 1/4 LYING N OF NEW ST RD 500",
  "157V3CZJ69U7" => "NE 1/4 LYING N OF CENTER LINE OF",
  "157V3CZJ669S" => "NE 1/4 EX SE 1/4 OF SE 1/4 OF NE",
  "157V3CZJ62QW" => "NE 1/4 EX ROAD AND CANAL R/W",
  "157V3CZJ5ZPL" => "NE 1/4 EX RD R/WS",
  "157V3CZJ5W5L" => "NE 1/4 EX PB 11 PG 79 12 PGS 43",
  "157V3CZJ5S76" => "NE 1/4 EX PB 10 PGS 2 49 29 PG 69",
  "157V3CZJ5NIW" => "NE 1/4 EX ORB 1117 PG 4",
  "157V3CZJ5JUY" => "NE 1/4 EX E 20 AC",
  "157V3CZJ5GOH" => "NE 1/4 EX CIVIL BOOK 54 PG 1011",
  "157V3CZJ4OS0" => "NE 1/4 E 1/2 OF NW 1/4 SW 1/4",
  "157V3CZJ5DZ3" => "NE 1/4 E 1/2 OF NW 1/4 EX RD R/W",
  "157V3CZJ5ANI" => "NE 1/4 E 1/2 OF NE 1/4 OF NW 1/4",
  "157V3CZJ53Y0" => "NE 1/4 AND SE 1/4 EX LEASEHOLD",
  "157V3CZJ574E" => "NE 1/4 AND SE 1/4",
  "157V3CZJ51G7" => "NE 1/4 AND PART OF N 1/2 AS DESC IN",
  "157V3CZJ4YFA" => "NE 1/4 AND LAND DES IN ORB 2342 PG",
  "157V3CZJ8LP1" => "NE 1/4",
  "157V3CZJ4HEP" => "NATIONAL POLICE HOME FOUNDATION INC SUBD 2ND ADDN",
  "157V3CZJ4KEZ" => "NATIONAL POLICE HOME FOUNDATION INC",
  "157V3CZJ4EFO" => "NATIONAL POLICE HOME FOUNDATION 1ST ADDN",
  "157V3CZJ4BLY" => "NASA PALMS PROFESSIONAL CENTER PH IV",
  "157V3CZJ488E" => "NASA PALMS PROFESSIONAL CENTER PH III",
  "157V3CZJ44H1" => "NASA PALMS PROFESSIONAL CENTER PH II",
  "157V3CZJ406D" => "NASA PALMS PROFESSIONAL CENTER PH I",
  "157V3CZJA5AP" => "NORTH COURTENAY SQUARE CONDO PHASE I",
  "157V3CZJA21P" => "NORTH BREVARD INDUSTRIAL PARK",
  "157V3CZJ9YXB" => "NORTH 929.65 FT OF GOVT LOT 4",
  "157V3CZJ9TJR" => "NO 4 CENTRAL",
  "1582WKI9000H" => "None",
  "157V3CZJ9QNC" => "NIEMAN HEIGHTS",
  "157V3CZJ9NOB" => "NICOLE PARK",
  "157V3CZJ9KKK" => "NICHOLSON GROVES SEC 2",
  "157V3CZJ9I1C" => "NICHOLSON GROVE SEC 1",
  "157V3CZJ9EU6" => "NEWTOWN SUBDIVISION",
  "157V3CZJ9BB2" => "NEWLAND MANOR",
  "157V3CZJ98WW" => "NEW SOUTH WALES AND LORRIKEET AT WALKABOUT",
  "157V3CZJ95PY" => "NEW ROCHELLE PARK",
  "157V3CZJ92BR" => "NEW PROVIDENCE ESTATES",
  "157V3CZJ8ZPI" => "NEW MELBOURNE BEACH",
  "157V3CZJ8WGZ" => "NEW JERSEY COLONY",
  "157V3CZJ8TFS" => "NEW GEORGIANA SETTLEMENT THE",
  "157V3CZJ8R3Z" => "NEW FOUND HARBOR",
  "157V3CZJ8OCC" => "NEVINS SUBD OF PT OF SECS 23 26",
  "157V3CZJ8JJI" => "NE 1/4 OF SW 1/4 LYING N OF",
  "157V3CZJ8H0W" => "NE 1/4 OF SEC 8",
  "157V3CZJ8DWS" => "NE 1/4 OF NW 1/4 OF NW 1/4",
  "157V3CZJ8AOK" => "NE 1/4 OF NW 1/4 EX W 40 FT",
  "157V3CZJ8455" => "NE 1/4 OF NE 1/4 W 1/2 OF NW 1/4",
  "157V3CZJ6GKX" => "NE 1/4 OF NE 1/4 S 1/2 OF NW 1/4",
  "157V3CZJ7YXM" => "NE 1/4 OF NE 1/4 OF NE 1/4 EX W",
  "157V3CZJ818Q" => "NE 1/4 OF NE 1/4 OF NE 1/4",
  "157V3CZJ7VFJ" => "NE 1/4 OF NE 1/4 LYING EAST OF",
  "157V3CZJ7SBO" => "NE 1/4 OF NE 1/4 EXC ORB 577 PG",
  "157V3CZJ7PJH" => "NE 1/4 OF NE 1/4 EX S 930 FT",
  "157V3CZJ7MGC" => "NE 1/4 OF NE 1/4 EX RD R/W",
  "157V3CZJ7JHV" => "NE 1/4 OF NE 1/4 EX RD AND CANAL",
  "157V3CZJ7GP7" => "NE 1/4 OF NE 1/4 EX PB 13 PG 139",
  "157V3CZJ7DRM" => "NE 1/4 OF NE 1/4 EX ORB 1632 PG",
  "157V3CZJ7AIG" => "NE 1/4 OF NE 1/4 EX ORB 1253",
  "157V3CZJ7807" => "NE 1/4 OF NE 1/4 EX E 50 FT AND EX",
  "157V3CZJD1X4" => "NW 1/4 OF NW 1/4 OF NE 1/4 S 1/2",
  "157V3CZJCZ4Q" => "NW 1/4 OF NW 1/4 AND W 1/2 OF",
  "157V3CZJCW25" => "NW 1/4 OF NW 1/4 AND NE 1/4 OF",
  "157V3CZJCQ57" => "NW 1/4 OF NE 1/4 W OF RR AND ALL",
  "157V3CZJCKQG" => "NW 1/4 OF NE 1/4 OF NE 1/4 EX PB",
  "157V3CZJCNQR" => "NW 1/4 OF NE 1/4 OF NE 1/4",
  "157V3CZJCHHC" => "NW 1/4 OF NE 1/4 EX PB 10 PG 40",
  "157V3CZJCDZP" => "NW 1/4 OF NE 1/4 EX ORB 1096 PG",
  "157V3CZJCAIQ" => "NW 1/4 OF NE 1/4 EX DB 203 PG 329",
  "157V3CZJC692" => "NW 1/4 OF NE 1/4 AND S 1/2 OF",
  "157V3CZJCT0W" => "NW 1/4 OF NE 1/4",
  "157V3CZJC2XT" => "NOVA VILLAS",
  "157V3CZJBZAW" => "NOTTINGHAM MANOR CONDO",
  "157V3CZJBVSA" => "NORTHWOOD ESTATES",
  "17THRCIHJYW6" => "NORTHWEST LAKES CONDO PH IV",
  "17THR8R1LTSQ" => "NORTHWEST LAKES CONDO PH III",
  "157V3CZJBS3V" => "NORTHWEST LAKES CONDO PH I AND II",
  "157V3CZJBPDY" => "NORTHVIEW CONDO",
  "157V3CZJBILK" => "NORTHGATE MOBILE RANCH 1ST ADD",
  "157V3CZJBM00" => "NORTHGATE MOBILE RANCH",
  "157V3CZJBF1O" => "NORTH TRITON ARMS CONDO",
  "157V3CZJBBG9" => "NORTH SOLERNO PARK",
  "157V3CZJB8L8" => "NORTH PORT ST JOHN UNIT 4",
  "157V3CZJB4YP" => "NORTH PORT ST JOHN UNIT 3",
  "157V3CZJB1PM" => "NORTH PORT ST JOHN UNIT 2 PART 2",
  "157V3CZJAZAZ" => "NORTH PORT ST JOHN UNIT 2 PART 1",
  "157V3CZJAW4N" => "NORTH PORT ST JOHN UNIT 1",
  "157V3CZJAT0B" => "NORTH POINTE UNIT 3",
  "157V3CZJAQIU" => "NORTH POINTE UNIT 2",
  "157V3CZJAN8I" => "NORTH POINTE UNIT 1",
  "157V3CZJAJSE" => "NORTH INDIALANTIC BY THE SEA REPLAT OF",
  "157V3CZJAHAQ" => "NORTH GROVE",
  "157V3CZJADYU" => "NORTH FORTY THE",
  "157V3CZJA8QK" => "NORTH DRIVE INDUSTRIAL PLAZA PHASE ONE A REPLAT O",
  "157V3CZJAAZS" => "NORTH DRIVE INDUSTRIAL PLAZA",
  "157V3CZJG9Z6" => "OAKTRAILS AT MEADOWRIDGE THE UNIT III",
  "157V3CZJG6IG" => "OAKTRAILS AT MEADOWRIDGE THE UNIT II",
  "157V3CZJG3G4" => "OAKTRAILS AT MEADOWRIDGE THE PHASE V",
  "157V3CZJG05C" => "OAKTRAILS AT MEADOWRIDGE THE PHASE IV-B",
  "157V3CZJFWXA" => "OAKTRAILS AT MEADOWRIDGE THE PHASE IV-A",
  "157V3CZJGD8K" => "OAKTRAILS AT MEADOWRIDGE THE",
  "157V3CZJFTOY" => "OAKS OF EAU GALLIE THE",
  "157V3CZJFPKR" => "OAKS AT MEADOW WOODS",
  "157V3CZJFMJR" => "OAKMONT PRESERVE SUBDIVISION",
  "157V3CZJFGGG" => "OAKLEDGE PARK REPLAT OF PT OF",
  "157V3CZJFJ9B" => "OAKLEDGE PARK",
  "157V3CZJFE2J" => "OAKDALE SEC 3",
  "157V3CZJFAVQ" => "OAKDALE SEC 2",
  "157V3CZJF8CE" => "OAKDALE SEC 1",
  "157V3CZJF5F8" => "OAK TREE GARDENS TOWNHOMES",
  "157V3CZJEZ1T" => "OAK TRACE TOWNHOMES PHASE II",
  "157V3CZJF28E" => "OAK TRACE",
  "157V3CZJEWGA" => "OAK ROYALE CONDO",
  "157V3CZJETFG" => "OAK RIDGE TOWNHOMES",
  "157V3CZJEQ4Q" => "OAK RIDGE ESTATES",
  "157V3CZJEK68" => "OAK PARK VILLAS OF BREVARD CONDO",
  "157V3CZJEH6Z" => "OAK PARK CONDO PH I THRU VI",
  "157V3CZJEEFH" => "OAK PARK AT SUNTREE REPLAT ONE",
  "157V3CZJEBH4" => "OAK PARK AT SUNTREE P.U.D.",
  "157V3CZJEN9C" => "OAK PARK",
  "157V3CZJE8HI" => "OAK HURST",
  "157V3CZJE5ZQ" => "OAK HAMMOCK ESTATES",
  "157V3CZJE2H5" => "OAK GROVE ESTATES UNIT 1",
  "157V3CZJDTVD" => "OAK GROVE AT WEST MELBOURNE PHASE THREE",
  "157V3CZJDQL9" => "OAK GROVE AT WEST MELBOURNE PHASE 2",
  "157V3CZJDNZY" => "OAK GROVE AT WEST MELBOURNE PHASE 1",
  "157V3CZJDKH0" => "OAK GROVE ACRES",
  "157V3CZJDH3K" => "OAK FOREST",
  "157V3CZJDB0J" => "OAK CREEK ESTATES",
  "157V3CZJD80W" => "O H BAGGETTS SUBD",
  "157V3CZJD4VJ" => "NW 1/4 W OF RD EX PB 11 PG 1 17",
  "157V3CZJJQ7H" => "OCEAN LANDINGS RES AND RAC CLB II TIME SHARE",
  "157V3CZJJMUS" => "OCEAN LANDINGS RES AND RAC CLB I TIME SHARE",
  "157V3CZJJJ8K" => "OCEAN LANDINGS OF MELBOURNE BCH CONDO",
  "157V3CZJJG3D" => "OCEAN KEY",
  "157V3CZJJD0B" => "OCEAN GARDENS WAVE TWO",
  "157V3CZJJ9VR" => "OCEAN GARDENS WAVE THREE",
  "157V3CZJJ5FK" => "OCEAN GARDENS WAVE ONE",
  "157V3CZJJ27Z" => "OCEAN GARDENS WAVE FOUR",
  "157V3CZJIZEK" => "OCEAN GARDEN VILLAGE OF SOUTH SHORES PHASE 1 AND P",
  "157V3CZJITVV" => "OCEAN ESTATES CONDO",
  "157V3CZJIWW0" => "OCEAN ESTATES",
  "157V3CZJIRGG" => "OCEAN EDGE COLONY",
  "157V3CZJINX1" => "OCEAN DUNES SUBD",
  "157V3CZJIL0F" => "OCEAN DUNES CONDOMINIUM AT AQUARINA BEACH PH III",
  "157V3CZJIIJT" => "OCEAN DUNES CONDOMINIUM AT AQUARINA BEACH PH II",
  "157V3CZJIF1U" => "OCEAN DUNES CONDOMINIUM AT AQUARINA BEACH PH I",
  "157V3CZJIBD9" => "OCEAN DUNES CONDO",
  "157V3CZJI70V" => "OCEAN CREST CONDO",
  "157V3CZJI3WE" => "OCEAN COURT",
  "157V3CZJI0YJ" => "OCEAN CLUB CONDO",
  "157V3CZJHXDJ" => "OCEAN CLUB A CONDOMINIUM",
  "157V3CZJHU40" => "OCEAN BREEZE TOWNHOUSES",
  "157V3CZJHQTS" => "OCEAN BREEZE HOMES CONDO",
  "157V3CZJHMZO" => "OCEAN BREEZE CONDO",
  "157V3CZJHK2G" => "OCEAN BLUFFS CONDO",
  "157V3CZJHGZN" => "OCEAN BLUE APTS CONDO",
  "157V3CZJHDPE" => "OCEAN BEACH VILLAS CONDO",
  "157V3CZJH5Y5" => "OCEAN BEACH APTS CONDO",
  "157V3CZJH3KV" => "OAKWOOD VILLAGE",
  "157V3CZJH00K" => "OAKWOOD TRAILS",
  "157V3CZJGUA8" => "OAKWOOD SUBD SEC D",
  "157V3CZJGP34" => "OAKWOOD SUBD SEC C",
  "157V3CZJGMJ3" => "OAKWOOD SUBD SEC B",
  "157V3CZJGJCS" => "OAKWOOD SUBD SEC A",
  "157V3CZJGX0D" => "OAKWOOD SUBD",
  "157V3CZJGG33" => "OAKWOOD ESTATES",
  "157V3CZJMPB0" => "OCEAN SPRAY SUBD",
  "157V3CZJMM30" => "OCEAN SPRAY ESTS ADDN NO 1",
  "157V3CZJMIW5" => "OCEAN SPRAY ESTATES",
  "157V3CZJMG5S" => "OCEAN SIDE VILLAGE PHASE THREE-A A REPLAT OF LOTS",
  "157V3CZJM9Q5" => "OCEAN SIDE VILLAGE PHASE 2C",
  "157V3CZJM70A" => "OCEAN SIDE VILLAGE PHASE 2B SURVEY BOOK 8 PAGE 92",
  "157V3CZJMD16" => "OCEAN SIDE VILLAGE PHASE 1C4 SURVEY BOOK 9 PAGE 13",
  "157V3CZJM3V9" => "OCEAN SIDE VILLAGE PHASE 1C3 SURVEY BOOK 9 PAGE 4",
  "157V3CZJM0WB" => "OCEAN SIDE VILLAGE PHASE 1C2 SURVEY BOOK 8 PAGE 7",
  "157V3CZJLYAX" => "OCEAN SIDE VILLAGE PHASE 1C2 SURVEY BOOK 8 PAGE 6",
  "157V3CZJLV58" => "OCEAN SHORES SUBD OF GOVT LOT 7",
  "157V3CZJLRP5" => "OCEAN SHORES RESUBD",
  "157V3CZJLOEO" => "OCEAN SANDS SOUTH CONDO",
  "157V3CZJLLG9" => "OCEAN SANDS NORTH CONDO",
  "157V3CZJLIIY" => "OCEAN SANDS CONDO",
  "157V3CZJLFWZ" => "OCEAN ROYALE CONDO",
  "157V3CZJLD2I" => "OCEAN RIVER ESTS REVISED PLAT OF",
  "157V3CZJL74I" => "OCEAN RIDGE II OF BREVARD",
  "157V3CZJLAEF" => "OCEAN RIDGE",
  "157V3CZJL3M9" => "OCEAN RESIDENCE SOUTH",
  "157V3CZJL0VA" => "OCEAN RESIDENCE NORTH",
  "157V3CZJKXSX" => "OCEAN PINES CONDO",
  "157V3CZJKU7I" => "OCEAN PINE VILLAS CONDO",
  "157V3CZJKNGS" => "OCEAN PARK CONDO SOUTH",
  "157V3CZJKJYD" => "OCEAN PARK CONDO NORTH",
  "157V3CZJKHBY" => "OCEAN PARK 2ND ADDN TO",
  "157V3CZJKE97" => "OCEAN PARK 1ST ADDN TO",
  "157V3CZJKR5P" => "OCEAN PARK",
  "157V3CZJKBMW" => "OCEAN OASIS CONDO",
  "157V3CZJK5YD" => "OCEAN OAKS CONDO PHASE II",
  "157V3CZJK2JR" => "OCEAN OAKS CONDO PHASE I",
  "157V3CZJK8YR" => "OCEAN OAKS",
  "157V3CZJJZQH" => "OCEAN MIST TOWNHOMES",
  "157V3CZJJW9I" => "OCEAN MIST CONDO",
  "157V3CZJJTR4" => "OCEAN LANDINGS RES AND RAC CLB III TIME SHARE",
  "157V3CZJPT0R" => "ODIN",
  "157V3CZJPPNI" => "ODEA AND MC INNIS SUBD",
  "157V3CZJPMB4" => "OCEANVIEW VILLAS CONDO PH III",
  "157V3CZJPJ3U" => "OCEANVIEW VILLAS CONDO PH II",
  "157V3CZJPG5M" => "OCEANVIEW VILLAS CONDO PH I",
  "157V3CZJPCZL" => "OCEANVIEW CONDO",
  "157V3CZJP9CX" => "OCEANUS CONDO",
  "157V3CZJP69B" => "OCEANSIDE VILLAS CONDO",
  "157V3CZJP30S" => "OCEANSIDE PALMS CONDOMINIUM",
  "1AS02WP3MBW6" => "OCEANSIDE ESTATES",
  "157V3CZJP07U" => "OCEANS GATE",
  "157V3CZJOX44" => "OCEANIQUE II CONDO PH A TIME SHARE",
  "157V3CZJOU2U" => "OCEANIQUE I CONDO PH C TIME SHARE",
  "157V3CZJOR1F" => "OCEANFRONT PARTNERSHIP CONDO",
  "157V3CZJOO1X" => "OCEANA OF COCOA BEACH CONDO",
  "157V3CZJOL9I" => "OCEAN WOODS STAGE 9 REPLAT",
  "157V3CZJOIUO" => "OCEAN WOODS STAGE 8 REPLAT",
  "157V3CZJOEEH" => "OCEAN WOODS STAGE 7",
  "157V3CZJOBE5" => "OCEAN WOODS STAGE 6",
  "157V3CZJO8PA" => "OCEAN WOODS STAGE 5",
  "157V3CZJO5J3" => "OCEAN WOODS STAGE 4",
  "157V3CZJO26U" => "OCEAN WOODS STAGE 3-B",
  "157V3CZJNYZ2" => "OCEAN WOODS STAGE 3-A",
  "157V3CZJNVSE" => "OCEAN WOODS STAGE 2",
  "157V3CZJNT7L" => "OCEAN WOODS STAGE 10-C",
  "157V3CZJNQEK" => "OCEAN WOODS STAGE 10-B",
  "157V3CZJNN94" => "OCEAN WOODS STAGE 10-A",
  "157V3CZJNK2E" => "OCEAN WOODS STAGE 1 REPLAT OF",
  "157V3CZJNHJM" => "OCEAN WINDS CONDO",
  "157V3CZJNECZ" => "OCEAN WALK CONDO",
  "157V3CZJNAVY" => "OCEAN VILLAGE PLAT OF EAU GALLIE BEACH",
  "157V3CZJN5D9" => "OCEAN VIEW TOWNHOMES",
  "157V3CZJN2I7" => "OCEAN VIEW OF COCOA BEACH CONDO",
  "157V3CZJMZ5V" => "OCEAN VIEW ESTATES",
  "157V3CZJN877" => "OCEAN VIEW",
  "157V3CZJMW0Z" => "OCEAN TERRACES CONDO",
  "157V3CZJMT0X" => "OCEAN TERRACE",
  "157V3CZJT078" => "OYSTER COVE A REPLAT OF LOT 5 AND LOT 6 OF",
  "157V3CZJT3CC" => "OYSTER COVE",
  "157V3CZJSWUV" => "OXFORD RIDGE",
  "157V3CZJSTQD" => "OUTRIGGER CONDO",
  "17TLOCDEAC6W" => "OUTDOOR RESORTS",
  "157V3CZJSQKH" => "OUTBACK TOWNHOMES",
  "157V3CZJSO1O" => "OUTBACK REPLAT BLOCK B",
  "157V3CZJSKSU" => "OTTER TRACE",
  "157V3CZJSH7H" => "OSPREY VILLAS EAST REPLAT AT AQUARINA II P.U.D.",
  "157V3CZJSBEI" => "OSPREY VILLAS AT AQUARINA P.U.D. PHASE II",
  "157V3CZJSEDS" => "OSPREY VILLAS AT AQUARINA P.U.D.",
  "157V3CZJS86R" => "OSPREY VILLAGE AT CEDAR CREEK PH II",
  "157V3CZJS4TR" => "OSPREY VILLAGE AT CEDAR CREEK PH I",
  "157V3CZJS213" => "OSPREY CONDO",
  "157V3CZJRZK9" => "OSBANS SUBD",
  "157V3CZJRX9Z" => "ORLANDO BEACH",
  "157V3CZJRTZ1" => "ORANGE WOODS 1ST GROVE SECTION",
  "157V3CZJRQZH" => "ORANGE PARK",
  "157V3CZJRO37" => "ORANGE HEIGHTS",
  "157V3CZJRKR9" => "ORANGE HAVEN UNRECORDED SUBD",
  "157V3CZJRHPM" => "ORANGE GROVE PARK",
  "157V3CZJRENP" => "ORANGE GROVE MANOR",
  "157V3CZJRBFW" => "ORANGE GARDENS SUBD OF E 1/2 OF",
  "157V3CZJR8S5" => "ORANGE CREEK SUBD",
  "157V3CZJR2QD" => "ORANGE BLOSSOM HEIGHTS",
  "157V3CZJQZYJ" => "OPUS SOUTH",
  "157V3CZJQXEJ" => "OPUS 21 TOWNHOUSE CONDO",
  "157V3CZJQUKQ" => "OPAL SEAS OCEANFRONT CONDO",
  "157V3CZJQRQD" => "OP-COCOA NO 1",
  "157V3CZJQNVL" => "ON THE BEACH CONDO",
  "157V3CZJQKRM" => "OLSONS RESUBD",
  "157V3CZJQHOZ" => "OLEANDER POINTE YACHT CLUB CONDO",
  "157V3CZJQF4T" => "OLEANDER POINTE PH III",
  "157V3CZJQBRB" => "OLEANDER POINTE PH II",
  "157V3CZJQ8NV" => "OLEANDER POINTE PH I",
  "157V3CZJQ5KE" => "OLD CHURCH MANOR THE",
  "157V3CZJPVL7" => "OLA GRANDE CONDO",
  "157V3CZJW9SR" => "PALMS WEST",
  "157V3CZJW6SY" => "PALMS POINTE OFFICE PARK CONDO PH III IV V",
  "157V3CZJW3JS" => "PALMS POINTE OFFICE PARK CONDO PH II",
  "157V3CZJW0UT" => "PALMS POINTE OFFICE PARK CONDO PH I",
  "157V3CZJVUZN" => "PALMETTO PHASE 3",
  "157V3CZJVRU1" => "PALMETTO PHASE 2",
  "157V3CZJVY7Q" => "PALMETTO",
  "157V3CZJVONC" => "PALMDALE",
  "157V3CZJVLX3" => "PALMAS DE MAJORCA",
  "157V3CZJVIWK" => "PALM SPRINGS CONDO",
  "157V3CZJVFOY" => "PALM SHORE ESTATES",
  "157V3CZJVCTS" => "PALM GARDENS",
  "157V3CZJV9QV" => "PALM ESTATES UNREC",
  "157V3CZJV3W8" => "PALM COURTS 2ND ADDN",
  "157V3CZJV0NM" => "PALM COURTS 1ST ADDN",
  "157V3CZJV6QP" => "PALM COURTS",
  "157V3CZJUXMQ" => "PALM COLONY CLUB CONDO",
  "157V3CZJUUJN" => "PALM BAY VILLAGE",
  "157V3CZJURI5" => "PALM BAY POINT",
  "157V3CZJUOIQ" => "PALM BAY MEDICAL OFFICE CENTER",
  "157V3CZJULVE" => "PALM BAY MAP NUMBER 1",
  "157V3CZJUJ60" => "PALM BAY INDUSTRIAL CENTER CONDO",
  "157V3CZJUFTY" => "PALM BAY HOMES SUBD",
  "157V3CZJUD1T" => "PALM BAY ESTATES MOBILE HOME PARK CO/OP OF TROPIC",
  "157V3CZJU9MC" => "PALM BAY ESTATES 1ST UNIT",
  "157V3CZJU6RF" => "PALM BAY COLONY SEC 4",
  "157V3CZJU4AW" => "PALM BAY COLONY SEC 3",
  "157V3CZJU0VY" => "PALM BAY COLONY SEC 2",
  "157V3CZJTXO7" => "PALM BAY COLONY SEC 1",
  "157V3CZJTU9T" => "PALM BAY COLONY REPLAT OF PORTIONS OF SECTIONS 1 2",
  "157V3CZJTR54" => "PALM BAY CLUB CONDOMINIUM",
  "157V3CZJTOFA" => "PALM AVE GARDENS",
  "157V3CZJTM6U" => "PALADIN ESTATES",
  "157V3CZJTHKI" => "PAINE HARRINGTON ADDITION TO MELBOURNE",
  "157V3CZJTEHE" => "P M FIVE LOTS",
  "157V3CZJTBBL" => "P F FONTAINE GRANT",
  "157V3CZJT6HO" => "P AND M HOUSING DEV",
  "157V3CZJZBCD" => "PARKWAY MEADOWS PHASE 1-A",
  "157V3CZJZ8I2" => "PARKWAY LAKES",
  "157V3CZJZ2LW" => "PARKWAY GARDENS UNIT 1 A REPLAT",
  "157V3CZJZ5JB" => "PARKWAY GARDENS UNIT 1",
  "157V3CZJYZK6" => "PARKWAY BUSINESS COMPLEX NORTH CONDO",
  "157V3CZJYWWU" => "PARKSIDE WEST",
  "1AS02WIAONYC" => "PARKSIDE ESTATES",
  "157V3CZJYU9R" => "PARKSIDE CENTER OFFICE CONDO",
  "157V3CZJYQTZ" => "PARKLAND ESTATES UNIT 1",
  "157V3CZJYNMT" => "PARKER ESTATES",
  "157V3CZJYKB6" => "PARKCHESTER UNIT 2",
  "157V3CZJYH21" => "PARKCHESTER UNIT 1",
  "157V3CZJYAZ4" => "PARK VILLAS PHASE TWO",
  "157V3CZJYDSI" => "PARK VILLAS",
  "157V3CZJY4XY" => "PARK PLACE SUBDIVISION",
  "157V3CZJY2GN" => "PARK PLACE CONDO",
  "157V3CZJY8FU" => "PARK PLACE",
  "157V3CZJXZJA" => "PARK MEADOW WEST AT SUNTREE",
  "157V3CZJXWGJ" => "PARK MEADOW EAST AT SUNTREE",
  "157V3CZJXQ9C" => "PARK HILL PLAT NO 1",
  "157V3CZJXT7Z" => "PARK HILL",
  "157V3CZJXN9K" => "PARK ADDN TO TITUSVILLE",
  "157V3CZJXKC2" => "PARK ACRES UNREC SUBD",
  "157V3CZJXHEG" => "PARADISE POINT NO 5",
  "157V3CZJXEE9" => "PARADISE POINT NO 4",
  "157V3CZJXBBD" => "PARADISE POINT NO 3",
  "157V3CZJX8JP" => "PARADISE POINT NO 2",
  "157V3CZJX5E9" => "PARADISE POINT NO 1",
  "157V3CZJX22U" => "PARADISE COVE",
  "157V3CZJWYVT" => "PARADISE CAY A CONDOMINIUM PH I",
  "157V3CZJWVJY" => "PARADISE BEACH VILLAS CONDO",
  "157V3CZJWSJ8" => "PARADISE BEACH CLUB NO 1 CONDO",
  "157V3CZJWPJV" => "PAR 4 CONDO",
  "157V3CZJWJT4" => "PAMPAS VILLAS PHASE 2",
  "157V3CZJWMX1" => "PAMPAS VILLAS",
  "157V3CZJWGEM" => "PAM-LEM INDUSTRIAL PARK",
  "157V3CZJWCVB" => "PALMWOOD",
  "157V3CZK2OXJ" => "PART OF LOT 1 AS DES IN DB 301",
  "157V3CZK2LCZ" => "PART OF LOT 1 AS DES IN DB 295",
  "157V3CZK2GGD" => "PART OF LOT 1 AND PT OF NE 1/4 OF",
  "157V3CZK2D6I" => "PART OF LOT 1 AND PART OF E 1/2 OF",
  "157V3CZK29U9" => "PART OF GOVT LOTS 1 2 LYING N OF",
  "157V3CZK26L3" => "PART OF GOVT LOTS 1 2 AND UNSURVEYD",
  "157V3CZK23IY" => "PART OF GOVT LOT 5 AS DES IN DB",
  "157V3CZK20GG" => "PART OF GOVT LOT 4 LYING E OF",
  "157V3CZK1XIV" => "PART OF GOVT LOT 2 AND PART OF",
  "157V3CZK1UDL" => "PART OF GOVT LOT 1 W OF ST RD 3",
  "157V3CZK1R5G" => "PART OF GOVT LOT 1 S OF ST RD AS",
  "157V3CZK1NY8" => "PART OF GOVT LOT 1 OF SEC 11",
  "157V3CZK1JEE" => "PART OF GOVT LOT 1 LYING N OF",
  "157V3CZK1G65" => "PART OF GOVT LOT 1 IN SEC 31",
  "157V3CZK1D1B" => "PART OF GOVT LOT 1 E OF A1A AS",
  "157V3CZK16H9" => "PART OF GOVT LOT 1 AS DES IN PAR",
  "157V3CZK134P" => "PART OF GOVT LOT 1 AS DES IN ORB",
  "157V3CZK0ZVL" => "PART OF GOVT LOT 1 AS DES IN DB",
  "157V3CZK19RK" => "PART OF GOVT LOT 1 AS DES IN",
  "157V3CZK0VFD" => "PART OF FRACTIONAL SECTION LYING",
  "157V3CZK0S4Z" => "PART OF E 1/4 OF NE 1/4 OF NE 1/4",
  "157V3CZK0OON" => "PART OF E 1/4 AS DES IN ORB 644",
  "157V3CZK0FQC" => "PART OF E 1/4 AS DES IN ORB 160",
  "157V3CZK0CRQ" => "PART OF E 1/2 OF NW 1/4 OF",
  "157V3CZK099E" => "PART OF E 1/2 OF NE 1/4 OF NE 1/4",
  "157V3CZK05UX" => "PART OF DEL GRANT KNOWN AS SEC 21",
  "157V3CZK025W" => "PART OF ALL FRACTIONAL SECS",
  "157V3CZJZZ2D" => "PARSONS ADD TO COCOA BEACH",
  "157V3CZJZVWR" => "PARRISH PARK AT SCOTTSMOOR",
  "157V3CZJZROK" => "PARKWAY VILLAS CONDO PH III",
  "157V3CZJZOEO" => "PARKWAY VILLAS CONDO PH II",
  "157V3CZJZL59" => "PARKWAY VILLAS CONDO PH I",
  "157V3CZJZHZ4" => "PARKWAY MEADOWS PHASE 2",
  "157V3CZJZEFL" => "PARKWAY MEADOWS PHASE 1-B",
  "157V3CZK7GIR" => "PART OF NE 1/4 OF SW 1/4 AS DES",
  "17VJ599CMKVQ" => "PART OF NE 1/4 OF SE 1/4 AS DES",
  "157V3CZK7D5E" => "PART OF NE 1/4 OF NW 1/4 AND PART",
  "157V3CZK79RX" => "PART OF NE 1/4 OF NE 1/4 E OF",
  "157V3CZK76IT" => "PART OF NE 1/4 OF NE 1/4 AS DES",
  "157V3CZK73A8" => "PART OF NE 1/4 LYING N OF CREEK",
  "157V3CZK6ZXP" => "PART OF NE 1/4 AS DESC IN ORB 387",
  "157V3CZK6WIV" => "PART OF NE 1/4 AS DES IN ORB",
  "157V3CZK6TYP" => "PART OF NE 1/4 AS DES IN DB XX",
  "157V3CZK6QSN" => "PART OF NE 1/4 AS DES IN DB W PG",
  "157V3CZK6NH7" => "PART OF NE 1/4 AS DES IN DB 96",
  "157V3CZK6JBM" => "PART OF NE 1/4 AS DES IN DB 332",
  "157V3CZK6G53" => "PART OF NE 1/4 AND PART OF W 1/2 AS",
  "157V3CZK6CWG" => "PART OF N 717.44 FT AS DES IN ORB",
  "157V3CZK69MH" => "PART OF N 1887.57 FT OF SEC 28 E",
  "157V3CZK66B9" => "PART OF N 1/4 OF NW 1/4 AS DES IN",
  "157V3CZK62NA" => "PART OF N 1/4 AS DES IN ORB 560",
  "157V3CZK5Z8M" => "PART OF N 1/2 OF NE 1/4 AS DES IN",
  "157V3CZK5W5P" => "PART OF N 1/2 AS DES IN DB 227 PG",
  "157V3CZK5TNA" => "PART OF LOT 9 SEC 36 AND PT OF SEC",
  "157V3CZK5EA2" => "PART OF LOT 3 SEC 18 AND PT OF LOT",
  "157V3CZK43PL" => "PART OF LOT 2 SECTION 22 AS DES",
  "157V3CZK3RBG" => "PART OF LOT 2 AS DES IN DB P",
  "157V3CZK3OIP" => "PART OF LOT 2 AS DES IN DB 357",
  "157V3CZK3KP5" => "PART OF LOT 2 AS DES IN DB 319 PG",
  "157V3CZK3G3U" => "PART OF LOT 1 SEC 6 AND PART OF LOT",
  "157V3CZK3D08" => "PART OF LOT 1 SEC 14 AND PT OF LOT",
  "157V3CZK3AO5" => "PART OF LOT 1 AS DES IN ORB 475",
  "157V3CZK367W" => "PART OF LOT 1 AS DES IN DB NN",
  "157V3CZK335Q" => "PART OF LOT 1 AS DES IN DB 73 PG",
  "157V3CZK3009" => "PART OF LOT 1 AS DES IN DB 416",
  "157V3CZK2VP3" => "PART OF LOT 1 AS DES IN DB 389 PG",
  "157V3CZK2SD2" => "PART OF LOT 1 AS DES IN DB 385 PG",
  "157V3CZKAIPJ" => "PART OF W 1/2 AS DES IN ORB 713",
  "157V3CZKAFNL" => "PART OF W 1/2 AS DES IN ORB 1975",
  "157V3CZKACGC" => "PART OF UNSURVEYED SEC 16 AS DES",
  "157V3CZKA8YK" => "PART OF THE NE 1/4 LYING N OF ST",
  "157V3CZKA600" => "PART OF SW 1/4 OF NE 1/4 AS DES",
  "157V3CZKA36C" => "PART OF SUBMERGED LAND IN GOVT",
  "157V3CZK9YZL" => "PART OF SECTIONS 5 8 9 15 16 26",
  "157V3CZK9UOE" => "PART OF SECTION AS DESC IN ORB",
  "157V3CZK9REU" => "PART OF SECS 11 AND 12 AS DES IN",
  "157V3CZK9O9O" => "PART OF SEC 6 AS DES IN ORB 655",
  "157V3CZK9L4H" => "PART OF SEC 5 AS DES IN ORB 655",
  "157V3CZK9HTM" => "PART OF SEC 35 AS DES IN ORB",
  "157V3CZK9EKF" => "PART OF SEC 34 IN PB 16 PG 84 AS",
  "157V3CZK9889" => "PART OF SEC 33 IN PB 16 PG 19",
  "157V3CZK93VI" => "PART OF SEC 32 IN PB 16 PG 19",
  "157V3CZK90N2" => "PART OF SEC 32 AS DES IN ORB 942",
  "157V3CZK8XSM" => "PART OF SEC 30 AS DES IN ORB 276",
  "157V3CZK8UIZ" => "PART OF SEC 20 AS DES IN ORB 1174",
  "157V3CZK8RBI" => "PART OF SEC 13 IN PB 17 PG 34 AS",
  "157V3CZK8NRS" => "PART OF SE 1/4 OF SW 1/4 AS DES",
  "157V3CZK8KDR" => "PART OF SE 1/4 OF SE 1/4 OF",
  "157V3CZK8H5G" => "PART OF SE 1/4 OF NE 1/4 AS DES",
  "157V3CZK8DZ4" => "PART OF S 122 FT OF GOVT LOT 2 AS",
  "157V3CZK8AKG" => "PART OF S 1/4 OF SECS 17 18 AS",
  "157V3CZK87I8" => "PART OF S 1/2 OF NE 1/4 AS DES IN",
  "157V3CZK84DK" => "PART OF S 1/2 OF N 1/2 OF SE 1/4",
  "157V3CZK803C" => "PART OF PETER FOUCHARD GRANT",
  "157V3CZK7X09" => "PART OF OLD GOVT LOT 44 AS DES",
  "157V3CZK7TTY" => "PART OF NW 1/4 OF NW 1/4 SEC 26",
  "158ZIUDTDYO8" => "PART OF NW 1/4 OF NW 1/4 AS DES",
  "157V3CZK7Q8R" => "PART OF NW 1/4 OF NE 1/4 AS DES",
  "157V3CZK7N5M" => "PART OF NW 1/4 AS DES IN ORB 742",
  "157V3CZK7JYN" => "PART OF NW 1/4 AS DES IN ORB 327",
  "157V3CZKDR2Z" => "PERLAS DEL MAR CONDO",
  "157V3CZKDNTA" => "PEPPERTREE EAST CONDO",
  "157V3CZKDKLS" => "PEPPER WOOD ESTATES",
  "157V3CZKDHDL" => "PENTHOUSE RESIDENCES AT MARINA POINTE CONDO",
  "157V3CZKDE4E" => "PENN PLACE CONDO PH VI",
  "157V3CZKDB1L" => "PENN PLACE CONDO PH V",
  "157V3CZKD80A" => "PENN PLACE CONDO PH IV",
  "157V3CZKD4UN" => "PENN PLACE CONDO PH III",
  "157V3CZKD1R6" => "PENN PLACE CONDO PH II",
  "157V3CZKCYLS" => "PENN PLACE CONDO PH I",
  "157V3CZKCVJ5" => "PEMBROOKE",
  "157V3CZKCS64" => "PELLEGRINO OFFICE CONDOMINIUM",
  "157V3CZKCOV8" => "PELICAN POINT CONDO",
  "157V3CZKCLFO" => "PELICAN MANOR SEC 2",
  "157V3CZKCI98" => "PELICAN MANOR SEC 1",
  "157V3CZKCESV" => "PELICAN HARBOUR UNIT TWO",
  "157V3CZKCBNM" => "PELICAN HARBOUR UNIT THREE",
  "157V3CZKC59F" => "PELICAN HARBOUR UNIT ONE A REPLAT OF TRACTS A B",
  "157V3CZKC8E3" => "PELICAN HARBOUR UNIT ONE",
  "157V3CZKC2PX" => "PELICAN HARBOUR UNIT FOUR",
  "157V3CZKBZCC" => "PELICAN CREEK ESTATES ADDN NO 5",
  "157V3CZKBW3E" => "PELICAN CREEK ESTATES ADDN NO 4",
  "157V3CZKBSK0" => "PELICAN CREEK ESTATES ADDN NO 3",
  "157V3CZKBP7Y" => "PELICAN CREEK ESTATES ADDN NO 2",
  "157V3CZKBM0K" => "PELICAN CREEK ESTATES ADDN NO 1",
  "157V3CZKBIXH" => "PELICAN COVE CONDO",
  "157V3CZKBFSD" => "PELICAN BEACH AT AQUARINA P.U.D. STAGE 3 TRACT V",
  "157V3CZKBCPR" => "PEBBLE CREEK",
  "157V3CZKB9MW" => "PEBBLE COVE CONDO",
  "157V3CZKB66O" => "PEARL OF THE SEA CONDOMINIUM",
  "157V3CZKAZMQ" => "PEACHTREE LANDING A REPLAT OF A PORTION OF LTS 41",
  "157V3CZKAW96" => "PAULANA MOBILE HOME SUBD",
  "157V3CZKAT30" => "PAULANA EAST",
  "157V3CZKAPW5" => "PAUL A GEIGERS ADDN TO RIVERVIEW HEIGHTS RESUBD OF",
  "157V3CZKALUX" => "PAT/DEE ESTATES",
  "157V3CZKH5XW" => "PINE ISLAND ESTATES UNIT 1",
  "157V3CZKH2PE" => "PINE HILL SUB",
  "157V3CZKGW03" => "PINE HILL MANOR PHASE II",
  "157V3CZKGZKG" => "PINE HILL MANOR",
  "157V3CZKGGIC" => "PINE GROVE PARK SUBD OF BLK 1",
  "157V3CZKGDBT" => "PINE GROVE PARK REPLAT OF PT BLK 5",
  "157V3CZKGN4X" => "PINE GROVE PARK NO 2 3RD ADDN",
  "157V3CZKGJV7" => "PINE GROVE PARK NO 2 1ST ADDN",
  "157V3CZKGQ5C" => "PINE GROVE PARK",
  "157V3CZKGA54" => "PINE GROVE NO 1",
  "157V3CZKG6XD" => "PINE GROVE ESTATES SEC 2",
  "157V3CZKG0P2" => "PINE GROVE ESTATES SEC 1 REPLAT OF LOTS 22 23",
  "157V3CZKG3R2" => "PINE GROVE ESTATES SEC 1",
  "157V3CZKGT4F" => "PINE GROVE",
  "157V3CZKFXLT" => "PINE GARDENS UNIT 2",
  "157V3CZKFTYT" => "PINE GARDENS UNIT 1",
  "157V3CZKFQNW" => "PINE CREST",
  "157V3CZKFNIN" => "PINE CREEK PHASE 4",
  "157V3CZKFK8O" => "PINE CREEK PHASE 3",
  "157V3CZKFH5I" => "PINE CREEK PHASE 2",
  "157V3CZKFDEQ" => "PINE CREEK PHASE 1",
  "157V3CZKF9ZJ" => "PINE COVE VILLAGE REPLAT",
  "157V3CZKF6SY" => "PINE COVE UNIT 3",
  "157V3CZKF3V1" => "PINE COVE UNIT 2",
  "157V3CZKF1AS" => "PINE COVE UNIT 1",
  "157V3CZKEYED" => "PINE BREEZE ACRES SUBD",
  "157V3CZKEV8Q" => "PINE ACRES",
  "157V3CZKES7H" => "PIERCE PLACE",
  "157V3CZKEPV8" => "PIERCE MANGURIAN SUBDIVISION",
  "157V3CZKEMM3" => "PIERCE CONDOMINIUM",
  "157V3CZKEJAI" => "PIER RESORT CONDO PH I AND II",
  "157V3CZKEG0U" => "PIC ESTATES SEC 3",
  "157V3CZKECSZ" => "PIC ESTATES SEC 2",
  "157V3CZKE491" => "PIC ESTATES SEC 1",
  "157V3CZKDXV2" => "PHILLIPS LANDING PHASE 2",
  "157V3CZKE16Z" => "PHILLIPS LANDING",
  "157V3CZKDUA7" => "PHILLIPS AND CAMPBELLS RESUBD",
  "157V3CZKKGW7" => "PINELAND PARK",
  "157V3CZKK1SM" => "PINEHURST GARDENS",
  "157V3CZKK4UL" => "PINEHURST",
  "157V3CZKJYPD" => "PINEHILL ACRES",
  "157V3CZKJVAU" => "PINEGROVE HEIGHTS SEC 1",
  "181HKZ9IA3AO" => "PINEDA SPRINGS",
  "157V3CZKJPMO" => "PINEDA RIDGE SUBDIVISION PHASE I",
  "157V3CZKJN04" => "PINEDA RIDGE SUBDIVISION PHASE 2",
  "157V3CZKJJKS" => "PINEDA PLAZA AT SUNTREE COMMERCIAL CONDO PH I",
  "157V3CZKJGEA" => "PINEDA PARK",
  "157V3CZKJD7X" => "PINEDA OCEAN CLUB CONDO PH III",
  "157V3CZKJA1L" => "PINEDA OCEAN CLUB CONDO PH II",
  "157V3CZKJ6V8" => "PINEDA OCEAN CLUB CONDO PH I",
  "157V3CZKJ3FC" => "PINEDA CROSSING PHASE V",
  "157V3CZKJ072" => "PINEDA CROSSING PHASE IV",
  "157V3CZKIXG6" => "PINEDA CROSSING PHASE III",
  "157V3CZKIU1E" => "PINEDA CROSSING PHASE II",
  "157V3CZKIQO1" => "PINEDA CROSSING PHASE I",
  "157V3CZKJSLJ" => "PINEDA",
  "157V3CZKIO1D" => "PINECREST SUBD",
  "157V3CZKIKW3" => "PINEAPPLE RIDGE",
  "157V3CZKIHOT" => "PINEAPPLE HOUSE CONDOMINIUM PH I",
  "157V3CZKIEOT" => "PINE-DALE ESTATES",
  "157V3CZKIBHY" => "PINE WALK MANOR",
  "157V3CZKI8H7" => "PINE VILLA UNIT 1",
  "157V3CZKI55B" => "PINE RIDGE ESTATES PHASE 2",
  "157V3CZKI21V" => "PINE RIDGE ESTATES PHASE 1",
  "157V3CZKHYUO" => "PINE PARK SUBD",
  "157V3CZKHW6H" => "PINE MEADOWS CONDO PH I",
  "157V3CZKHSST" => "PINE MEADOW ESTATES",
  "157V3CZKHPGD" => "PINE LANE ESTATES",
  "157V3CZKHM8T" => "PINE LAKE RANCH",
  "157V3CZKHITT" => "PINE LAKE MOBILE HOME ESTATES",
  "157V3CZKHFGI" => "PINE LAKE ESTATES",
  "157V3CZKHCEJ" => "PINE LAKE ESTATE REPLAT OF PT OF",
  "157V3CZKH90Y" => "PINE ISLAND LAKES UNIT 1",
  "157V3CZKNGU2" => "PLANTATION PHASE THREE",
  "157V3CZKNDOF" => "PLANTATION OAKS OF BREVARD PHASE TWO",
  "157V3CZKNAT9" => "PLANTATION OAKS OF BREVARD PHASE THREE",
  "157V3CZKN7ML" => "PLANTATION OAKS OF BREVARD PHASE ONE",
  "157V3CZKN4H7" => "PLANTATION OAKS OF BREVARD PHASE FOUR",
  "157V3CZKN1YA" => "PLANTATION OAKS OF BREVARD PHASE 5",
  "158ZUVRRHV9K" => "PLANTATION OAKS OF BREVARD",
  "158ZIUIWY55L" => "PLANTATION OAKS",
  "157V3CZKMPE6" => "PLANTATION ESTATES",
  "157V3CZKMMC8" => "PIPER PALMS",
  "157V3CZKMJ77" => "PIONEER WEST CONDO",
  "157V3CZKMG39" => "PIONEER GROVE SUBD",
  "157V3CZKMD23" => "PINEY WOODS",
  "157V3CZKM9Q0" => "PINEWOOD VILLAS",
  "157V3CZKM2ZF" => "PINEWOOD VILLAGE OF MELBOURNE CONDO NO 4",
  "157V3CZKLZQC" => "PINEWOOD VILLAGE OF MELBOURNE CONDO NO 3",
  "157V3CZKLWPO" => "PINEWOOD VILLAGE OF MELBOURNE CONDO NO 2",
  "157V3CZKLU4X" => "PINEWOOD VILLAGE OF MELBOURNE CONDO NO 1",
  "157V3CZKM6OJ" => "PINEWOOD VILLAGE",
  "157V3CZKLQOZ" => "PINEWOOD PARK",
  "157V3CZKLNM6" => "PINEWOOD HILLS",
  "157V3CZKLKT2" => "PINEWOOD ESTATES",
  "157V3CZKLHKB" => "PINETREE GARDENS",
  "157V3CZKLEDF" => "PINETREE CONDO PH III",
  "157V3CZKLBO6" => "PINETREE CONDO PH II",
  "157V3CZKL8E6" => "PINETREE CONDO PH I",
  "157V3CZKL4PP" => "PINES THE",
  "157V3CZKL18M" => "PINERIDGE UNIT NO 4",
  "157V3CZKKXES" => "PINERIDGE UNIT NO 3",
  "157V3CZKKNYR" => "PINERIDGE SUBD UNIT 2 TRACT A OF",
  "157V3CZKKQV8" => "PINERIDGE SUBD UNIT 2",
  "157V3CZKKU31" => "PINERIDGE SUBD",
  "157V3CZKKKD7" => "PINERIDGE HEIGHTS",
  "157V3CZKKDJP" => "PINELAND PARK UNIT III PHASE I",
  "157V3CZKKAH1" => "PINELAND PARK UNIT III PHASE 2",
  "157V3CZKK81G" => "PINELAND PARK UNIT II A REPLAT OF",
  "157V3CZKQLNG" => "POINSETT SHORES 3RD ADDN",
  "157V3CZKQI03" => "POINSETT SHORES 2ND ADDN",
  "157V3CZKQDUV" => "POINSETT SHORES 1ST ADDN",
  "157V3CZKQZ79" => "POINSETT SHORES",
  "157V3CZKQAI5" => "POINSETT GROVES",
  "157V3CZKQ4AY" => "POINSETT GARDENS 2ND ADDN",
  "157V3CZKQ15S" => "POINSETT GARDENS 1ST ADDN",
  "157V3CZKQ7NS" => "POINSETT GARDENS",
  "157V3CZKPV8E" => "POINSETT ACRES UNIT 3",
  "157V3CZKPRU9" => "POINSETT ACRES UNIT 2",
  "157V3CZKPYMC" => "POINSETT ACRES",
  "157V3CZKPNT8" => "POINCIANA RIDGE",
  "157V3CZKPKKN" => "PLYMOUTH HOLLOW",
  "157V3CZKPHUX" => "PLUMOSA COMMERICAL PLAZA CONDO",
  "157V3CZKPEG9" => "PLEASANT HILLS SUBD",
  "157V3CZKPASK" => "PLAZA NORTH",
  "157V3CZKP75R" => "PLAZA HEIGHTS SUBD",
  "157V3CZKP3K0" => "PLAYERS RIDGE CONDO",
  "157V3CZKP0TN" => "PLAYERS CLUB AT SUNTREE",
  "157V3CZKOU2C" => "PLAYERS CLUB AT BAYSIDE LAKES PHASE 3",
  "157V3CZKOXFM" => "PLAYERS CLUB AT BAYSIDE LAKES PHASE 2",
  "157V3CZKOREP" => "PLAYERS CLUB AT BAYSIDE LAKES PHASE 1",
  "157V3CZKONUK" => "PLATTS RESUBD",
  "157V3CZKOKA1" => "PLAT OF VIERA PARCELS KK AND LL",
  "157V3CZKOGMQ" => "PLAT OF VIERA PARCEL KK PHASE II",
  "157V3CZKOD53" => "PLAT OF VIERA PARCEL K",
  "157V3CZKOA6J" => "PLAT OF VIERA NORTH P.U.D. PARCEL G-2 PHASE FOUR",
  "157V3CZKO6RN" => "PLAT OF VIERA NORTH P.U.D. PARCEL G-1 PHASE TWO",
  "157V3CZKO39J" => "PLAT OF VIERA - PARCEL TT AND UU",
  "157V3CZKNZQU" => "PLAT OF SURVEY OF COUNTRY ROAD RIGHT OF WAY AND DR",
  "157V3CZKNVVO" => "PLAT OF CHATSWORTH SOUTH - A REPLAT OF LOTS 19 20",
  "157V3CZKNSQN" => "PLAT OF BAYTREE P.U.D. PHASE 2 STAGE 1",
  "157V3CZKNPO3" => "PLANTATION POINT PHASE TWO",
  "157V3CZKNMQP" => "PLANTATION POINT PHASE ONE",
  "157V3CZKNK8V" => "PLANTATION PHASE TWO",
  "157V3CZKTLD3" => "PORT MALABAR INDUSTRIAL PARK SUBD",
  "157V3CZKTI0K" => "PORT MALABAR HOLIDAY PARK UNIT 2",
  "157V3CZKTBKR" => "PORT MALABAR HOLIDAY PARK UNIT 1 1ST REPLAT",
  "157V3CZKTF47" => "PORT MALABAR HOLIDAY PARK UNIT 1",
  "157V3CZKT90X" => "PORT MALABAR COUNTRY CLUB UNIT 9",
  "157V3CZKT6J7" => "PORT MALABAR COUNTRY CLUB UNIT 8",
  "157V3CZKSZZ4" => "PORT MALABAR COUNTRY CLUB UNIT 7 PT OF SECOND REPL",
  "157V3CZKSX9U" => "PORT MALABAR COUNTRY CLUB UNIT 7 FIRST REPLAT OF",
  "157V3CZKT3BA" => "PORT MALABAR COUNTRY CLUB UNIT 7",
  "157V3CZKSU8A" => "PORT MALABAR COUNTRY CLUB UNIT 6",
  "157V3CZKSP72" => "PORT MALABAR COUNTRY CLUB UNIT 5 SECOND REPLAT IN",
  "157V3CZKSLQC" => "PORT MALABAR COUNTRY CLUB UNIT 5 1ST REPLAT",
  "157V3CZKSRSK" => "PORT MALABAR COUNTRY CLUB UNIT 5",
  "157V3CZKSIRQ" => "PORT MALABAR COUNTRY CLUB UNIT 4",
  "157V3CZKSFYC" => "PORT MALABAR COUNTRY CLUB UNIT 3",
  "157V3CZKSCLI" => "PORT MALABAR COUNTRY CLUB UNIT 2",
  "157V3CZKS9BG" => "PORT MALABAR COUNTRY CLUB UNIT 10",
  "157V3CZKS37A" => "PORT MALABAR COUNTRY CLUB UNIT 1 1ST REPLAT IN",
  "157V3CZKS6J2" => "PORT MALABAR COUNTRY CLUB UNIT 1",
  "157V3CZKRX6A" => "PORT CANAVERAL SEC 2",
  "157V3CZKRZZX" => "PORT CANAVERAL",
  "157V3CZKRTVJ" => "POMELLO RANCH UNIT 3",
  "157V3CZKRQN6" => "POMELLO RANCH UNIT 2",
  "157V3CZKRO55" => "POMELLO RANCH UNIT 1",
  "157V3CZKRHFN" => "POINT WEST ESTATES 2ND ADDN",
  "157V3CZKREEB" => "POINT WEST ESTATES 1ST ADDN",
  "157V3CZKRKNV" => "POINT WEST ESTATES",
  "157V3CZKRBV9" => "POINT VIEW PLACE",
  "157V3CZKR947" => "POINT THE",
  "157V3CZKR6ET" => "POINSETTA TOWNHOUSES",
  "157V3CZKR2XE" => "POINSETT VILLAS SEC 2",
  "157V3CZKQUY3" => "POINSETT SHORES 5TH ADDN",
  "157V3CZKQOE1" => "POINSETT SHORES 4TH ADDN REPLAT",
  "157V3CZKQS4O" => "POINSETT SHORES 4TH ADDN",
  "157V3CZKWJVJ" => "PORT MALABAR UNIT 3 2ND REPLAT",
  "157V3CZKWPQO" => "PORT MALABAR UNIT 3",
  "157V3CZKWGIZ" => "PORT MALABAR UNIT 29",
  "157V3CZKWAC7" => "PORT MALABAR UNIT 28 FIRST REPLAT",
  "157V3CZKWDUL" => "PORT MALABAR UNIT 28",
  "157V3CZKW702" => "PORT MALABAR UNIT 27",
  "157V3CZKW3U5" => "PORT MALABAR UNIT 26",
  "157V3CZKW0HD" => "PORT MALABAR UNIT 25",
  "157V3CZKVWP3" => "PORT MALABAR UNIT 24",
  "157V3CZKVTC9" => "PORT MALABAR UNIT 23",
  "157V3CZKVPUB" => "PORT MALABAR UNIT 22",
  "157V3CZKVN67" => "PORT MALABAR UNIT 21 SECOND REPLAT IN",
  "157V3CZKVK4R" => "PORT MALABAR UNIT 21 1ST REPLAT OF",
  "158ZUVTO458J" => "PORT MALABAR UNIT 21",
  "157V3CZKVCYK" => "PORT MALABAR UNIT 20 1ST REPLAT IN TRACT A",
  "157V3CZKVGO8" => "PORT MALABAR UNIT 20",
  "157V3CZKV73L" => "PORT MALABAR UNIT 2 SUBD OF TRACT B BLK 9",
  "157V3CZKV3CY" => "PORT MALABAR UNIT 2 REPLAT OF BLK 11",
  "157V3CZKV9UQ" => "PORT MALABAR UNIT 2",
  "157V3CZKUZTP" => "PORT MALABAR UNIT 19",
  "157V3CZKUWY7" => "PORT MALABAR UNIT 18",
  "157V3CZKUTSD" => "PORT MALABAR UNIT 17",
  "157V3CZKUQNZ" => "PORT MALABAR UNIT 16",
  "157V3CZKUNZI" => "PORT MALABAR UNIT 15",
  "157V3CZKUKKN" => "PORT MALABAR UNIT 14",
  "157V3CZKUHKS" => "PORT MALABAR UNIT 13",
  "157V3CZKUF2M" => "PORT MALABAR UNIT 12",
  "157V3CZKUBMA" => "PORT MALABAR UNIT 11",
  "157V3CZKU8LQ" => "PORT MALABAR UNIT 10",
  "158ZUVTW62H9" => "PORT MALABAR UNIT 1 REPLAT OF PT OF",
  "157V3CZKU2EM" => "PORT MALABAR UNIT 1 REPLAT OF PT OF",
  "157V3CZKTYY7" => "PORT MALABAR UNIT 1 5TH REPLAT",
  "157V3CZKTVK8" => "PORT MALABAR UNIT 1 4TH REPLAT",
  "157V3CZKU5PS" => "PORT MALABAR UNIT 1",
  "157V3CZKTRLU" => "PORT MALABAR PROFESSIONAL CONDO",
  "157V3CZKTOMK" => "PORT MALABAR INDUSTRIAL PARK UNIT 2",
  "157V3CZKZWS9" => "PORT MALABAR UNIT 59",
  "157V3CZKZU7A" => "PORT MALABAR UNIT 58",
  "157V3CZKZQTR" => "PORT MALABAR UNIT 57",
  "157V3CZKZNZF" => "PORT MALABAR UNIT 56",
  "157V3CZKZL82" => "PORT MALABAR UNIT 55",
  "157V3CZKZHN0" => "PORT MALABAR UNIT 54",
  "157V3CZKZEN4" => "PORT MALABAR UNIT 53",
  "157V3CZKZBYM" => "PORT MALABAR UNIT 52",
  "157V3CZKZ955" => "PORT MALABAR UNIT 51",
  "157V3CZKZ5OL" => "PORT MALABAR UNIT 50",
  "157V3CZKYZFZ" => "PORT MALABAR UNIT 5 1ST REPLAT",
  "157V3CZKZ2K8" => "PORT MALABAR UNIT 5",
  "157V3CZKYVYM" => "PORT MALABAR UNIT 49",
  "157V3CZKYT5N" => "PORT MALABAR UNIT 48",
  "157V3CZKYQ3L" => "PORT MALABAR UNIT 47",
  "157V3CZKYMM9" => "PORT MALABAR UNIT 46",
  "157V3CZKYIUT" => "PORT MALABAR UNIT 45",
  "157V3CZKYF4K" => "PORT MALABAR UNIT 44",
  "157V3CZKYC3I" => "PORT MALABAR UNIT 43",
  "157V3CZKY8YK" => "PORT MALABAR UNIT 42",
  "157V3CZKY6EF" => "PORT MALABAR UNIT 41",
  "157V3CZKY3QY" => "PORT MALABAR UNIT 40",
  "157V3CZKXSBC" => "PORT MALABAR UNIT 4 SECOND REPLAT OF",
  "157V3CZKXWZK" => "PORT MALABAR UNIT 4 SECOND REPLAT",
  "157V3CZKXP85" => "PORT MALABAR UNIT 4 REPLAT OF PT OF",
  "157V3CZKY0HK" => "PORT MALABAR UNIT 4",
  "157V3CZKXMHF" => "PORT MALABAR UNIT 39",
  "157V3CZKXILE" => "PORT MALABAR UNIT 38",
  "157V3CZKXF85" => "PORT MALABAR UNIT 37",
  "157V3CZKXAKH" => "PORT MALABAR UNIT 36",
  "157V3CZKX7D3" => "PORT MALABAR UNIT 35",
  "157V3CZKX4NO" => "PORT MALABAR UNIT 34",
  "157V3CZKX1HY" => "PORT MALABAR UNIT 33",
  "157V3CZKWYBA" => "PORT MALABAR UNIT 32",
  "157V3CZKWV90" => "PORT MALABAR UNIT 31",
  "157V3CZKWSG9" => "PORT MALABAR UNIT 30",
  "157V3CZKWN5P" => "PORT MALABAR UNIT 3 REPLAT OF PT OF",
  "157V3CZL31YN" => "PRESERVE AT LAKE WASHINGTON",
  "157V3CZL2VG3" => "PRESERVE AT HERITAGE OAKS",
  "157V3CZL2OWW" => "POWELLS SUBD OF E 5 AC OF SW 1/4 OF SW 1/4 F C",
  "157V3CZL2S0S" => "POWELLS SUBD",
  "157V3CZL2LWM" => "POWELLS RESUBD OF PT OF LOT 14 AND ALL LOTS 15 16",
  "157V3CZL2IU0" => "POWELLS RESUBD OF MELBOURNE",
  "157V3CZL2FTB" => "POWELLS AND HENLEYS RESUBD",
  "157V3CZL2D6K" => "POSTRIDGE",
  "157V3CZL2A4O" => "POST ROAD CASCADES",
  "157V3CZL26W6" => "PORTSIDE VILLAS CONDO",
  "157V3CZL23XR" => "PORTOFINO BAY",
  "157V3CZL20NZ" => "PORTERS SUBD",
  "157V3CZL1XLM" => "PORT VIEW TOWNHOMES",
  "157V3CZL1UZ4" => "PORT ST JOHNS MEDICAL AND PROF PLAZA",
  "157V3CZL1RG4" => "PORT ST JOHN UNIT 8",
  "157V3CZL1LGB" => "PORT ST JOHN UNIT 7 1ST REPLAT",
  "157V3CZL1O9M" => "PORT ST JOHN UNIT 7",
  "157V3CZL1C69" => "PORT ST JOHN UNIT 6 1ST REPLAT IN",
  "157V3CZL1F1H" => "PORT ST JOHN UNIT 6 1ST REPLAT",
  "157V3CZL1I6H" => "PORT ST JOHN UNIT 6",
  "157V3CZL195X" => "PORT ST JOHN UNIT 5",
  "157V3CZL164F" => "PORT ST JOHN UNIT 4",
  "157V3CZL13IH" => "PORT ST JOHN UNIT 3",
  "157V3CZL0XF8" => "PORT ST JOHN UNIT 2 1ST REPLAT IN",
  "157V3CZL10DS" => "PORT ST JOHN UNIT 2",
  "157V3CZL0RDX" => "PORT ST JOHN UNIT 1 REPLAT OF PT OF LOTS 5 6 BLK",
  "157V3CZL0O27" => "PORT ST JOHN UNIT 1 REPLAT OF BLK 17",
  "157V3CZL0UWE" => "PORT ST JOHN UNIT 1",
  "157V3CZL0LI1" => "PORT ST JOHN CENTER",
  "157V3CZL0HW2" => "PORT ROYAL CONDO",
  "157V3CZL0EST" => "PORT MALABAR UNIT 9",
  "157V3CZL0CFS" => "PORT MALABAR UNIT 8",
  "157V3CZL092Y" => "PORT MALABAR UNIT 7",
  "157V3CZL05WZ" => "PORT MALABAR UNIT 60",
  "157V3CZKZZUW" => "PORT MALABAR UNIT 6 1ST REPLAT IN",
  "157V3CZL02ZT" => "PORT MALABAR UNIT 6",
  "157V3CZL699O" => "PUERTO DEL RIO CONDO PH 1E",
  "157V3CZL65X1" => "PUERTO DEL RIO CONDO PH 1D",
  "157V3CZL62RM" => "PUERTO DEL RIO CONDO PH 1C",
  "157V3CZL5ZGE" => "PUERTO DEL RIO CONDO PH 1A",
  "157V3CZL5WTF" => "PT OF W 1/2 AS DES IN ORB 2237",
  "157V3CZL5TEB" => "PT OF SW 1/4 OF NE 1/4 AS DES IN",
  "157V3CZL5QAA" => "PT OF SECTION AS DES IN CIVIL",
  "157V3CZL5NM5" => "PT OF SECS 18 19 AS DES IN ORB",
  "157V3CZL5K8I" => "PT OF SEC AS DES IN CIVIL BOOK",
  "157V3CZL5H4V" => "PT OF SEC 7 AS DES IN ORB",
  "157V3CZL5EJY" => "PT OF SE 1/4 OF SEC 15 LYING E OF",
  "157V3CZL5BCR" => "PT OF NW 1/4 OF NE 1/4 AS DES IN",
  "157V3CZL5834" => "PT OF NE 1/4 OF NE 1/4 E OF RR",
  "157V3CZL55J2" => "PT OF N 205 FT OF S 455.51 FT OF",
  "157V3CZL5285" => "PT OF N 1/2 OF NE 1/4 OF NE 1/4",
  "157V3CZL4Z2U" => "PT OF N 1/2 OF NE 1/4 AS DES IN",
  "157V3CZL4W5N" => "PT OF N 1/2 OF LOT 32 AS DES IN",
  "157V3CZL4SYO" => "PT OF N 1/2 AS DES IN ORB 1147 PG",
  "157V3CZL4PU7" => "PT OF GOVT LOT 1 OF SEC 20 AS DES",
  "157V3CZL4K35" => "PT OF GOVT LOT 1 AS DES IN ORB",
  "157V3CZL4MXJ" => "PT OF GOVT LOT 1 AS DES IN",
  "157V3CZL4GWM" => "PT OF G.L 1 LYING E OF U.S HWY 1",
  "157V3CZL4D56" => "PT OF G L 1 SEC 23 AND PT G L 2",
  "157V3CZL48NU" => "PT OF FRAC SEC LYING N OF GRANT",
  "157V3CZL4545" => "PT OF FRAC SEC LYING E OF US 1 AS",
  "157V3CZL40KP" => "PT OF E 3/4 E OF S PATRICK DRIVE",
  "157V3CZL3WCA" => "PT AS DES IN ORB 655 PG 377",
  "157V3CZL3SNP" => "PROPERTY AS DES IN DB 326 PG 269",
  "157V3CZL3E1D" => "PRITCHARDS UNREC REPLAT OF LOTS 1 AND 2 OF BLAKES",
  "157V3CZL3ARC" => "PRIDE AND JOY ENVIRONMENTAL CONDOS NORTH",
  "157V3CZL37L8" => "PRESERVE POINTE",
  "17WH5OAKVL60" => "PRESERVE OF VERO BEACH",
  "157V3CZL34TQ" => "PRESERVE DRIVE",
  "157V3CZL2YI4" => "PRESERVE AT LAKE WASHINGTON PHASE TWO",
  "157V3CZL9N8K" => "RAILROAD ADDN TO EAU GALLIE",
  "157V3CZL9816" => "RADFORD ESTATES",
  "157V3CZL95C0" => "R H POST SUBD",
  "157V3CZL92GV" => "QUANTUM PLACE AT WALKABOUT POD 9",
  "157V3CZL8ZLQ" => "QUAIL VILLAGE HOMES",
  "157V3CZL8TAX" => "QUAIL RUN ESTATES",
  "157V3CZL8WR3" => "QUAIL RUN",
  "157V3CZL8Q5R" => "QUAIL RIDGE TOWNHOUSES",
  "157V3CZL8NSF" => "QUAIL RIDGE PATIO HOMES",
  "157V3CZL8KRJ" => "QUAIL RIDGE PART 2",
  "157V3CZL8HBC" => "QUAIL RIDGE CONDO PH 4E",
  "157V3CZL8EUZ" => "QUAIL RIDGE CONDO PH 4D",
  "157V3CZL8BC3" => "QUAIL RIDGE CONDO PH 4C",
  "157V3CZL88PR" => "QUAIL RIDGE CONDO PH 4B",
  "157V3CZL85W4" => "QUAIL RIDGE CONDO PH 4A",
  "157V3CZL82EQ" => "QUAIL RIDGE CONDO PH 3B",
  "157V3CZL7Z7T" => "QUAIL RIDGE CONDO PH 3A",
  "157V3CZL7WCU" => "QUAIL RIDGE CONDO PH 2C",
  "157V3CZL7T8Y" => "QUAIL RIDGE CONDO PH 2B",
  "157V3CZL7Q6H" => "QUAIL RIDGE CONDO PH 2A",
  "157V3CZL7NCJ" => "QUAIL RIDGE CONDO PH 1E",
  "157V3CZL7K57" => "QUAIL RIDGE CONDO PH 1D",
  "157V3CZL7GYX" => "QUAIL RIDGE CONDO PH 1C",
  "157V3CZL7EDG" => "QUAIL RIDGE CONDO PH 1B",
  "157V3CZL7B15" => "QUAIL RIDGE CONDO PH 1A",
  "157V3CZL77W6" => "QUAIL RIDGE AT SUNTREE SUNTREE PUD STAGE 5 TRACT 4",
  "157V3CZL758L" => "QUAIL POINT OF SUNTREE",
  "157V3CZL720S" => "QUAIL HOLLOW",
  "157V3CZL6YUO" => "QUAIL HAVEN-SECTION 2",
  "157V3CZL6VNA" => "QUAIL HAVEN SECTION 1",
  "157V3CZL6QRX" => "QUAIL COVE",
  "157V3CZL6OFT" => "PUTNAM PARK SUBD UNIT 1",
  "157V3CZL6L66" => "PURYEAR WARD PLAT OF MIMS",
  "157V3CZL6I2M" => "PUESTA DEL SOL",
  "157V3CZL6FH7" => "PUERTO DEL RIO CONDO PH 3B",
  "157V3CZL6C57" => "PUERTO DEL RIO CONDO PH 3A",
  "157V3CZLCQAP" => "REMBERT-BOLEY ESTATES UNIT 1",
  "157V3CZLCN3Z" => "REILLY SUBDIVISION",
  "157V3CZLCKBF" => "REGINA MYRA SUBDIVISION",
  "157V3CZLCH2U" => "REGENTS WALK",
  "157V3CZLCE0T" => "REGENCY PINES II",
  "157V3CZLCBE9" => "REGENCY PINES CONDO PH IV",
  "157V3CZLC7W8" => "REGENCY PINES CONDO PH III",
  "157V3CZLC4KF" => "REGENCY PINES CONDO PH II",
  "157V3CZLC1OB" => "REGENCY PINES CONDO PH I",
  "157V3CZLBYN6" => "REGENCY MEDICAL OFFICE PARK",
  "157V3CZLBVHC" => "REGENCY II OFFICE CONDO",
  "157V3CZLBSSC" => "REFLECTIONS CONDO",
  "157V3CZLBPDB" => "READ AND ALLENS SUBD",
  "157V3CZLBMFL" => "RAYSTEFF COUNTRY CLUB SUBDIVISION",
  "157V3CZLBJXX" => "RAVENSWOOD UNIT 2",
  "157V3CZLBGPG" => "RAVENSWOOD UNIT 1",
  "157V3CZLB9AD" => "RAVENCREST UNIT 2",
  "157V3CZLBCWJ" => "RAVENCREST",
  "157V3CZLB5HC" => "RAVENCLIFFE PHASE 2",
  "157V3CZLB2PE" => "RAVENCLIFFE PHASE 1",
  "157V3CZLAWII" => "RANSOMS RESUBD OF BLK 12 OF MAP NO 3 WELLS SUBD",
  "157V3CZLAZLC" => "RANSOMS RESUBD",
  "157V3CZLATNY" => "RANSOMS ADDITION",
  "157V3CZLAQEV" => "RANGEWOOD VILLAS PUD STAGE ONE",
  "157V3CZLANBT" => "RANCHWOOD ESTATES",
  "157V3CZLAKD7" => "RANCH UNIT 4 THE",
  "157V3CZLAHIN" => "RANCH UNIT 3 THE",
  "157V3CZLAEE8" => "RANCH UNIT 2 THE",
  "157V3CZLABXQ" => "RANCH UNIT 1 THE",
  "157V3CZLA5E8" => "RAMBLING ACRES WEST",
  "157V3CZLA8VQ" => "RAMBLING ACRES",
  "157V3CZLA2DP" => "RALEY GARDENS SUBD",
  "157V3CZL9WFV" => "RAINTREE-BY-THE-LAKE REPLAT OF LOT 1 BLK B AND PAR",
  "157V3CZL9ZC5" => "RAINTREE-BY-THE-LAKE",
  "157V3CZL9U1B" => "RAINGE AND DAVIS SUBD",
  "157V3CZL9QK8" => "RAILROAD EMPORIUM OFFICE CONDO",
  "157V3CZLFVLJ" => "RIDGEWOOD CONDO OF CAPE CANAVERAL",
  "157V3CZLFSWI" => "RIDGEWAY PARK",
  "157V3CZLFMSW" => "RIDGEVIEW CONDO",
  "157V3CZLFPZH" => "RIDGEVIEW",
  "157V3CZLFK9G" => "RIDGE MANOR ESTATES",
  "157V3CZLFGZ0" => "RIDGE MANOR A PARTIAL REPLAT OF",
  "157V3CZLFDJF" => "RICHWOOD",
  "157V3CZLFAX4" => "RICHARDS SUBD",
  "157V3CZLF7Q7" => "RICHARDIS SUBD",
  "157V3CZLF4W7" => "RICHARD ARMS APTS CONDO",
  "157V3CZLF21P" => "RIBLETS RESUB",
  "157V3CZLEVPV" => "RIALTO PLACE",
  "157V3CZLESMH" => "RHUM COVE",
  "157V3CZLEP7G" => "RHETT S PERRY SUBDIVISION",
  "157V3CZLEMD1" => "REVILLA MANOR",
  "157V3CZLEK15" => "RESURVEY OF PART OF LOT 5 SUBD",
  "157V3CZLEHIP" => "RESUBD OF TRACT 11",
  "157V3CZLEEMH" => "RESUBD OF LOTS 11 THRU 16 BLK E",
  "157V3CZLEBDH" => "RESUBD OF FARM 62 AS IN VACATED",
  "157V3CZLE8V0" => "RESUBD OF BLKS A B C D N O P Q",
  "157V3CZLE5U7" => "RESUBD OF BLKS 1 2 4 5 6 7 8 9 OF",
  "157V3CZLE2T1" => "RESUBD OF BLK 56",
  "157V3CZLE0AF" => "RESUBD OF BLK 44 OF SUBD OF E 1/2",
  "157V3CZLDXJ9" => "RESUBD FARM 63 AS IN VACATED PB 3",
  "157V3CZLDU8F" => "RESERVE THE",
  "157V3CZLDQVB" => "RERDELLS ADDN TO TITUSVILLE",
  "157V3CZLDNBP" => "REPLAT OF PT OF BLK K OF GRANDVIEW SHORES",
  "157V3CZLDHMT" => "REPLAT OF PT OF BLK 6 WICKHAM OAKS",
  "157V3CZLDEAR" => "REPLAT OF OUT LOT 16",
  "157V3CZLDBIW" => "REPLAT OF LOTS 8 TO 17 BLK A",
  "157V3CZLD862" => "REPLAT OF LOTS 15 16 17 18",
  "157V3CZLD506" => "REPLAT OF LOT 29 BLK 2",
  "157V3CZLD2E6" => "REPLAT OF HOLLY TRACE BAYSIDE LAKES",
  "157V3CZLCYSK" => "REPLAT OF FARMS 48 TO 53",
  "157V3CZLCVSK" => "RENAISSANCE POINTE UNIT TWO",
  "157V3CZLCTHU" => "RENAISSANCE POINTE UNIT ONE",
  "157V3CZLJ76J" => "RIVER FALLS ESTATES",
  "157V3CZLJ42Q" => "RIVER EDGE ESTATES UNIT 4",
  "157V3CZLJ1H1" => "RIVER DUNES",
  "157V3CZLIY0K" => "RIVER COVE",
  "157V3CZLIUXG" => "RIVER COLONY WEST SECTION THE",
  "157V3CZLIS1L" => "RIVER COLONY NORTHWEST SECTION",
  "157V3CZLIOOJ" => "RIVER COLONY EAST",
  "157V3CZLILVU" => "RIVER CLUB CONDO",
  "157V3CZLIG3F" => "RIVER CLIFF UNIT 2",
  "157V3CZLIJH2" => "RIVER CLIFF",
  "157V3CZLICMP" => "RIVER BREEZE ACRES UNIT 2",
  "157V3CZLI762" => "RIVER BREEZE ACRES UNIT 1",
  "157V3CZLI1HQ" => "RIVER BLUFF CONDO PH II",
  "157V3CZLI44T" => "RIVER BLUFF CONDO",
  "157V3CZLHY66" => "RIVER BEND CONDO",
  "157V3CZLHUYN" => "RIOMAR CONDO",
  "157V3CZLHRSV" => "RIO-MAR",
  "157V3CZLHOZB" => "RIO VISTA UNIT 1",
  "157V3CZLHKOE" => "RIO VISTA PATIO HOMES",
  "157V3CZLHHHV" => "RIO VISTA CONDO PH VI",
  "157V3CZLHEDJ" => "RIO VISTA CONDO PH V",
  "157V3CZLHBKY" => "RIO VISTA CONDO PH IV",
  "157V3CZLH881" => "RIO VISTA CONDO PH III",
  "157V3CZLH5D5" => "RIO VISTA CONDO PH II",
  "157V3CZLH2VI" => "RIO VISTA CONDO PH I",
  "157V3CZLGZXZ" => "RIO VILLA UNIT V",
  "157V3CZLGWQ0" => "RIO VILLA UNIT IV",
  "157V3CZLGU5R" => "RIO VILLA UNIT III",
  "157V3CZLGQS8" => "RIO VILLA UNIT II",
  "157V3CZLGNGN" => "RIO VILLA UNIT I",
  "157V3CZLGKHT" => "RIO VILLA NORTH PHASE II AND III",
  "157V3CZLGH8A" => "RIO VILLA NORTH PHASE I",
  "157V3CZLGE55" => "RIO MAR FIRST ADDITION",
  "157V3CZLGBGC" => "RIO LINDO SUBD",
  "157V3CZLG89H" => "RIO LINDO 3RD ADDN",
  "157V3CZLG52K" => "RIO LINDO 2ND SEC",
  "157V3CZLG262" => "RIO LINDO 1ST ADDN TO",
  "157V3CZLFYYI" => "RINCONORO",
  "157V3CZLMGJS" => "RIVER PLACE CONDO",
  "157V3CZLMDF3" => "RIVER PALMS CONDO",
  "157V3CZLMA9D" => "RIVER OAKS WEST",
  "157V3CZLM4LQ" => "RIVER OAKS PLANTATION PHASE III",
  "157V3CZLM1AL" => "RIVER OAKS PLANTATION PHASE II",
  "157V3CZLM7PP" => "RIVER OAKS PLANTATION",
  "157V3CZLLXZM" => "RIVER OAKS ESTATES",
  "157V3CZLLUW3" => "RIVER OAKS EAST",
  "157V3CZLLRV7" => "RIVER OAKS CONDO",
  "157V3CZLLOEQ" => "RIVER OAKS AT AQUARINA AQUARINA II P.U.D. STAGE 3",
  "157V3CZLLL8T" => "RIVER MOORINGS",
  "157V3CZLLIP4" => "RIVER LANDINGS OF TITUSVILLE A RESUBDIVISION OF P",
  "157V3CZLLF7D" => "RIVER LAKES CONDO PH III BLDG 800",
  "157V3CZLLBUF" => "RIVER LAKES CONDO PH II BLDG 720",
  "157V3CZLL8W6" => "RIVER LAKES CONDO PH I BLDG 760",
  "157V3CZLL2SN" => "RIVER ISLES ADDN 5",
  "157V3CZLKZML" => "RIVER ISLES ADDN 4 NORTH ISLES 3 AND 4",
  "157V3CZLKWDA" => "RIVER ISLES ADDN 3",
  "157V3CZLKPHG" => "RIVER ISLES ADDN 2 AMENDED",
  "157V3CZLKSSQ" => "RIVER ISLES ADDN 2",
  "157V3CZLKM5Y" => "RIVER ISLES ADDN 1",
  "157V3CZLL5WY" => "RIVER ISLES",
  "157V3CZLKFKZ" => "RIVER ISLAND RAQUETTE CLUB PH 2",
  "157V3CZLKJ12" => "RIVER ISLAND RAQUETTE CLUB",
  "157V3CZLKC4Q" => "RIVER ISLAND PHASE 3",
  "157V3CZLK8LI" => "RIVER ISLAND ESTATES",
  "157V3CZLK55N" => "RIVER HEIGHTS",
  "157V3CZLK28C" => "RIVER HAVEN",
  "157V3CZLJZ0P" => "RIVER GROVES",
  "157V3CZLJUU4" => "RIVER GROVE ON THE TRAIL PHASE 3",
  "157V3CZLJRWZ" => "RIVER GROVE ON THE TRAIL PHASE 2",
  "157V3CZLJOFP" => "RIVER GROVE ON THE TRAIL PHASE 1",
  "157V3CZLJL2X" => "RIVER GARDENS CONDO PH III",
  "157V3CZLJHQS" => "RIVER GARDENS CONDO PH II",
  "157V3CZLJDNW" => "RIVER GARDENS CONDO PH I",
  "157V3CZLJAJT" => "RIVER FRONT ACRES UNIT 1",
  "157V3CZLPGRB" => "RIVERSIDE TOWNHOUSES",
  "157V3CZLORMF" => "RIVERSIDE REPLAT OF",
  "157V3CZLPBHA" => "RIVERSIDE PARK SUBD",
  "157V3CZLP8ZB" => "RIVERSIDE PARK SEC A",
  "157V3CZLPE49" => "RIVERSIDE PARK",
  "157V3CZLP3PB" => "RIVERSIDE LANDING OF SOUTH BREVARD",
  "157V3CZLP17I" => "RIVERSIDE LANDING CONDO",
  "157V3CZLP6AO" => "RIVERSIDE LANDING",
  "157V3CZLOYZV" => "RIVERSIDE DRIVE",
  "157V3CZLOWL4" => "RIVERSIDE CONDOS AT BEACH WOODS PH I",
  "157V3CZLOU9G" => "RIVERSIDE CONDOMINIUM",
  "157V3CZLOP6D" => "RIVERS EDGE SUBD",
  "157V3CZLOMEL" => "RIVERS EDGE ESTATES UNIT 4",
  "157V3CZLOJKN" => "RIVERS EDGE ESTATES UNIT 3",
  "157V3CZLOH0R" => "RIVERS EDGE ESTATES UNIT 2",
  "157V3CZLODP4" => "RIVERS EDGE ESTATES UNIT 1",
  "157V3CZLOA9Z" => "RIVERS EDGE CONDO",
  "157V3CZLO7FU" => "RIVERMONT ESTATES SUBD",
  "157V3CZLO4A6" => "RIVERMONT ESTATES NO 2",
  "157V3CZLO0UZ" => "RIVERIA ESTATES SUBD",
  "157V3CZLNXXK" => "RIVEREDGE CONDO",
  "157V3CZLNUL1" => "RIVERDALE ESTATES SUBD",
  "157V3CZLNOE3" => "RIVER WOOD ESTATES PHASE TWO",
  "157V3CZLNRAY" => "RIVER WOOD ESTATES",
  "157V3CZLNIA4" => "RIVER WAY CONDO PH III",
  "157V3CZLNF66" => "RIVER WAY CONDO PH II",
  "157V3CZLNLEF" => "RIVER WAY CONDO",
  "157V3CZLNBZF" => "RIVER WALK BY THE SEA",
  "157V3CZLN8LS" => "RIVER VILLA CONDO",
  "157V3CZLN5NR" => "RIVER SUNSET",
  "157V3CZLMYFZ" => "RIVER SHORES EAST",
  "157V3CZLMV3P" => "RIVER SHORES 1ST ADDN",
  "157V3CZLN1BD" => "RIVER SHORES",
  "157V3CZLMS49" => "RIVER ROAD ESTATES",
  "157V3CZLMJ8G" => "RIVER RIDGE REPLAT OF LOTS 1 AND2",
  "157V3CZLMMDB" => "RIVER RIDGE ESTATES",
  "157V3CZLMPMP" => "RIVER RIDGE",
  "157V3CZLS96U" => "RIVIERA 1ST ADDN REPLAT OF PT OF BLK 2",
  "157V3CZLSCFV" => "RIVIERA 1ST ADDN",
  "157V3CZLS6OZ" => "RIVERWEST CONDO PH II",
  "157V3CZLS3GB" => "RIVERWEST CONDO PH I",
  "158WOE9W4JA2" => "RIVERWALK OF MELBOURNE",
  "157V3CZLRXP1" => "RIVERVIEW TOWNHOMES",
  "157V3CZLRQUL" => "RIVERVIEW TERRACE SECS Z 1 2 3 4 5",
  "157V3CZLROD5" => "RIVERVIEW TERRACE SECS W X Y 6 7 8",
  "157V3CZLRLB0" => "RIVERVIEW TERRACE SECS O P Q R",
  "157V3CZLRHWN" => "RIVERVIEW TERRACE SECS N S U V",
  "157V3CZLRF9K" => "RIVERVIEW TERRACE SECS M AND T",
  "157V3CZLRBU8" => "RIVERVIEW TERRACE SECS H AND I",
  "157V3CZLR8T5" => "RIVERVIEW TERRACE SECS G AND J",
  "157V3CZLR5QG" => "RIVERVIEW TERRACE SECS F AND K",
  "157V3CZLR1BT" => "RIVERVIEW TERRACE SEC D",
  "157V3CZLQYR7" => "RIVERVIEW TERRACE SEC C",
  "157V3CZLQW8S" => "RIVERVIEW TERRACE ESTATES",
  "157V3CZLQSXD" => "RIVERVIEW TERRACE AMENDED PLAT SEC L",
  "157V3CZLQPS0" => "RIVERVIEW TERRACE AMENDED PLAT SEC B",
  "157V3CZLQMZE" => "RIVERVIEW TERRACE AMENDED PLAT SEC A",
  "157V3CZLQJCH" => "RIVERVIEW TERRACE AMENDED PLAT",
  "157V3CZLRU7C" => "RIVERVIEW TERRACE",
  "157V3CZLQFPB" => "RIVERVIEW SUBD",
  "157V3CZLQD6R" => "RIVERVIEW RESUBD OF",
  "157V3CZLQATU" => "RIVERVIEW MANOR CONDO PH VI",
  "157V3CZLQ8CG" => "RIVERVIEW MANOR CONDO PH V",
  "157V3CZLQ5WR" => "RIVERVIEW MANOR CONDO PH IV",
  "157V3CZLQ3BO" => "RIVERVIEW MANOR CONDO PH III",
  "157V3CZLQ0JJ" => "RIVERVIEW MANOR CONDO PH II",
  "157V3CZLPY2L" => "RIVERVIEW MANOR CONDO PH I",
  "157V3CZLPVGL" => "RIVERVIEW HOMES",
  "157V3CZLPP6H" => "RIVERVIEW HEIGHTS SUBD",
  "157V3CZLPS4H" => "RIVERVIEW HEIGHTS",
  "157V3CZLPM79" => "RIVERVIEW ESTATES",
  "157V3CZLPJNE" => "RIVERVIEW ACRES SUBD",
  "157V3CZLS03R" => "RIVERVIEW",
  "157V3CZLVBFL" => "ROCKLEDGE HEIGHTS RESUBD OF LOTS 25 26 27 DOUGLAS",
  "157V3CZLV8EL" => "ROCKLEDGE GARDENS",
  "157V3CZLV2Q4" => "ROCKLEDGE ESTATES UNIT 1 2ND ADDN",
  "157V3CZLUZ65" => "ROCKLEDGE ESTATES UNIT 1 1ST ADDITION",
  "157V3CZLV5XI" => "ROCKLEDGE ESTATES UNIT 1",
  "157V3CZLUT74" => "ROCKLEDGE COUNTRY CLUB ESTATES TOWNHOUSE SECTION",
  "157V3CZLUNEQ" => "ROCKLEDGE COUNTRY CLUB ESTATES SEC 2 PARTIAL REPL",
  "157V3CZLUPZT" => "ROCKLEDGE COUNTRY CLUB ESTATES SEC 2",
  "157V3CZLUKCS" => "ROCKLEDGE COUNTRY CLUB ESTATES SEC 1 NORTH",
  "157V3CZLUWDU" => "ROCKLEDGE COUNTRY CLUB ESTATES",
  "157V3CZLUH7M" => "ROCKLEDGE CENTRAL INDUSTRIAL PARK PHASE ONE",
  "157V3CZLUECH" => "ROCKLEDGE BUSINESS AND INDUSTRIAL PARK",
  "157V3CZLU5BS" => "ROCKLEDGE BARTONS PLAT OF",
  "157V3CZLUB5W" => "ROCKLEDGE ACRE ESTATES",
  "157V3CZLU25T" => "ROCK POINTE CONDO PH III",
  "157V3CZLTYT9" => "ROCK POINTE CONDO PH II",
  "157V3CZLTW02" => "ROCK POINTE CONDO PH I",
  "157V3CZLTSTJ" => "ROCK POINT SUBD",
  "157V3CZLTPHH" => "ROCK PIT ROAD TOWNHOUSES",
  "157V3CZLTMDW" => "ROBINSWOOD",
  "157V3CZLTJ3D" => "ROBINSON ESTATES",
  "157V3CZLTFRS" => "ROBINS HILL ACRES",
  "157V3CZLTCW7" => "ROBIN LEE SUBD",
  "157V3CZLTAKQ" => "ROBBINS AND GRAHAM ADDITION TO TITUSVILLE",
  "157V3CZLT88O" => "ROB-E-E PLACE",
  "157V3CZLT5C3" => "RKEYS CONDOMINIUM",
  "157V3CZLT2DT" => "RIVIERA SUBDIVISION",
  "157V3CZLSZVF" => "RIVIERA KEY",
  "157V3CZLSUGK" => "RIVIERA ISLES UNIT 1 SEC 2",
  "157V3CZLSRP6" => "RIVIERA ISLES UNIT 1 REPLAT",
  "157V3CZLSWZB" => "RIVIERA ISLES UNIT 1",
  "157V3CZLSP23" => "RIVIERA ISLES SEC 4",
  "157V3CZLSM9B" => "RIVIERA ISLES SEC 2",
  "157V3CZLSJ43" => "RIVIERA GREEN",
  "157V3CZLSFHI" => "RIVIERA COMMERCIAL CENTER",
  "157V3CZLYDF9" => "ROSEDALE SUBD 3RD ADDN",
  "157V3CZLYA33" => "ROSEDALE SUBD 2ND ADDN",
  "157V3CZLY72O" => "ROSEDALE SUBD 1ST ADDN",
  "157V3CZLYG2J" => "ROSEDALE SUBD",
  "157V3CZLY4BZ" => "ROSEDALE MANORS",
  "157V3CZLY1E4" => "ROSE HILL ESTATES UNIT 1 3RD SEC",
  "157V3CZLXY57" => "ROSE HILL ESTATES UNIT 1 2ND SEC",
  "157V3CZLXUZ0" => "ROSALIND CONDOMINIUM",
  "157V3CZLXRWS" => "ROOTS REPLAT",
  "157V3CZLXP0H" => "ROOSEVELT GARDENS APTS CONDO",
  "157V3CZLXMA7" => "RON JON CAPE CARIBE RESORT TIME SHARE",
  "157V3CZLXJNH" => "RON JON CAPE CARIBE RESORT PH I",
  "157V3CZLXG88" => "ROMAC SUBD",
  "157V3CZLXDLX" => "ROLLING HILLS SUBD",
  "157V3CZLX54F" => "ROGERS PLACE",
  "157V3CZLX1WQ" => "ROESCHS ADD TO EAU GALLIE",
  "157V3CZLWYYR" => "ROCKY WATER PARK REPLAT OF OF",
  "157V3CZLWT6K" => "ROCKY POINT GROVES",
  "157V3CZLWWG9" => "ROCKY POINT",
  "157V3CZLWPSI" => "ROCKWOOD",
  "157V3CZLWMSC" => "ROCKWELL SOUTH",
  "157V3CZLWJOO" => "ROCKWELL ESTATES",
  "157V3CZLWDV9" => "ROCKLEDGE VILLAS REPLAT OF BLK D",
  "157V3CZLWGNQ" => "ROCKLEDGE VILLAS",
  "157V3CZLU7UN" => "ROCKLEDGE RESUBD OF LOT NO 1",
  "157V3CZLWAR8" => "ROCKLEDGE PROFESSIONAL CENTER CONDO",
  "157V3CZLW85X" => "ROCKLEDGE PROFESSIONAL BLDG CONDO",
  "157V3CZLW5IE" => "ROCKLEDGE PLAZA",
  "157V3CZLW28A" => "ROCKLEDGE PINES UNIT 2",
  "157V3CZLVZ5W" => "ROCKLEDGE PINES UNIT 1",
  "157V3CZLVSZD" => "ROCKLEDGE PARK NO 2 REPLAT",
  "157V3CZLVWHF" => "ROCKLEDGE PARK NO 2",
  "157V3CZLVPTV" => "ROCKLEDGE PARK NO 1",
  "157V3CZLVNDE" => "ROCKLEDGE MURRELL PROFESSIONAL CONDO",
  "157V3CZLVJU8" => "ROCKLEDGE MOBILE VILLAGE",
  "157V3CZLVH0K" => "ROCKLEDGE III",
  "157V3CZLVEJ4" => "ROCKLEDGE HOMES",
  "157V3CZM1855" => "ROYAL OAKS CONDO",
  "157V3CZM1531" => "ROYAL OAK VIEW CONDO",
  "157V3CZM11YR" => "ROYAL OAK TOWNHOUSE CONDO",
  "157V3CZM0WRW" => "ROYAL OAK PATIO HOMES PHASE TWO",
  "157V3CZM0UCU" => "ROYAL OAK PATIO HOMES - REVISED",
  "157V3CZM0ZAA" => "ROYAL OAK PATIO HOMES",
  "157V3CZM0RZR" => "ROYAL OAK LODGE UNIT ONE CONDO",
  "157V3CZM0PCH" => "ROYAL OAK GOLF HAVEN CONDO",
  "157V3CZM0MFL" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC NO 5",
  "157V3CZM0JFG" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 9",
  "157V3CZM0GER" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 8A",
  "157V3CZM0DVG" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 8",
  "157V3CZM0A48" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 7",
  "157V3CZM07DW" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 6C",
  "157V3CZM04TG" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 6B",
  "157V3CZM020D" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 6",
  "157V3CZLZYVV" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 4",
  "157V3CZLZVWV" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 3",
  "157V3CZLZRJL" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 2",
  "157V3CZLZOVT" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 10",
  "157V3CZLZMAY" => "ROYAL OAK GOLF AND COUNTRY CLUB SEC 1",
  "157V3CZLZJ4P" => "ROYAL OAK GOLF AND COUNTRY CLUB EAST UNIT 3",
  "157V3CZLZGAG" => "ROYAL OAK GOLF AND COUNTRY CLUB EAST UNIT 2",
  "157V3CZLZD7Q" => "ROYAL OAK GOLF AND COUNTRY CLUB EAST UNIT 1",
  "157V3CZLZA7B" => "ROYAL OAK CONDO",
  "157V3CZLZ7J0" => "ROYAL MANSION CONDO",
  "157V3CZLZ1KV" => "ROYAL GARDEN HOMES SEC 2",
  "157V3CZLZ4ZD" => "ROYAL GARDEN HOMES",
  "157V3CZLYY1G" => "ROYAL COLONY CONDO",
  "157V3CZLYV6M" => "ROYAL COLONIAL CONDO",
  "157V3CZLYS44" => "ROSSERS PLAT OF EAU GALLIE BEACH",
  "157V3CZLYP2Q" => "ROSEVERE TERR PART OF RESUBD OF",
  "157V3CZLYMKK" => "ROSEMARIE PINES PLAT OF",
  "157V3CZLYJAV" => "ROSEHAVEN",
  "157V3CZM3ZQ2" => "S 150 FT OF N 560 FT MEASURED ON",
  "157V3CZM3WGO" => "S 11.16 FT OF SE 1/4 OF SE 1/4 OF",
  "157V3CZM3T3N" => "S 1029.02 FT OF E 1/2 OF LOT 28",
  "157V3CZM3Q3P" => "S 100 FT OF N 331.61 FT OF W 99",
  "157V3CZM3MVS" => "S 100 FT OF N 331.2 FT OF SE 1/4",
  "157V3CZM3JQ9" => "S 100 FT OF N 328.21 FT OF W 305",
  "157V3CZM3GL8" => "S 100 FT OF GOVT LOT 2 SEC 33",
  "157V3CZM3D3R" => "S 100 FT OF GOVT LOT 1 IN SEC 27",
  "157V3CZM39OJ" => "S 1/4 OF SE 1/4 OF NW 1/4",
  "157V3CZM372R" => "S 1/2 OF SW 1/4 OF NE 1/4 OF",
  "157V3CZM34BA" => "S 1/2 OF SE 1/4 EX AS DES IN DB",
  "157V3CZM31HC" => "S 1/2 OF NW 1/4 OF SW 1/4 OF",
  "157V3CZM2RWL" => "S 1/2 OF NE 1/4 OF NE 1/4 LYING E",
  "157V3CZM2WHV" => "S 1/2 OF NE 1/4 OF NE 1/4 LYING",
  "157V3CZM2YWP" => "S 1/2 OF NE 1/4 OF NE 1/4",
  "157V3CZM2PBZ" => "S 1/2 OF LOT 1 AND VAC OLD DIXIE",
  "157V3CZM2MIF" => "S 1/2 OF E 1/2 OF LOT 32 PER PLAT",
  "157V3CZM2K0I" => "S 1/2 NE 1/4 E 1/4 OF NW 1/4",
  "157V3CZM2HAT" => "S 1/2 EX RDS AND ORB 770 PG 640",
  "157V3CZM2EMC" => "S 1/2 EX ORB 1147 PG 221",
  "157V3CZM2BB0" => "RUSSELLS SUBD",
  "157V3CZM28CV" => "RUNWAY BAYS CONDOMINIUM",
  "157V3CZM25XR" => "ROYALE TOWERS CONDO PH III",
  "157V3CZM237P" => "ROYALE TOWERS CONDO PH II",
  "157V3CZM20OA" => "ROYALE TOWERS CONDO PH I",
  "157V3CZM1Y4E" => "ROYAL YORK CONDO",
  "157V3CZM1SWU" => "ROYAL VILLAS - FIRST ADDITION",
  "157V3CZM1VKA" => "ROYAL VILLAS",
  "157V3CZM1QB8" => "ROYAL SUBD",
  "157V3CZM1LGY" => "ROYAL PALMS II",
  "157V3CZM1NVK" => "ROYAL PALMS",
  "157V3CZM1IH8" => "ROYAL PALM ISLAND",
  "157V3CZM1G2M" => "ROYAL PALM ESTATES",
  "157V3CZM1DG3" => "ROYAL PALM CONDO",
  "157V3CZM1ARS" => "ROYAL PALACE SUBDIVISION",
  "157V3CZM7E4T" => "SAN MATEO VILLAGE SEC 5",
  "157V3CZM7B68" => "SAN MATEO VILLAGE SEC 4",
  "157V3CZM788K" => "SAN MATEO VILLAGE SEC 3",
  "157V3CZM751B" => "SAN MATEO VILLAGE SEC 2",
  "157V3CZM71S5" => "SAN MATEO VILLAGE SEC 1",
  "157V3CZM6YHX" => "SAN MARINO ESTATES",
  "157V3CZM6V8N" => "SAN LIN MANOR SUBD",
  "157V3CZM6RUB" => "SAN JUAN VILLAGE CONDO",
  "157V3CZM6OHJ" => "SAN FILIPPO COMMERCIAL COMPLEX",
  "157V3CZM6LHX" => "SAMS CLUB SUBDIVISION",
  "157V3CZM6IK9" => "SAMPSON GROVE SUBDIVISION",
  "157V3CZM6FGP" => "SALTAIRE CONDO",
  "157V3CZM6CF1" => "SAGRANS RIVER TOWNHOUSES",
  "157V3CZM65O3" => "SADDLEBROOKE OF WEST MELBOURNE A REPLAT OF SADDLEB",
  "157V3CZM69BQ" => "SADDLEBROOKE",
  "157V3CZM62HE" => "SABEL PALMS UNIT 1",
  "157V3CZM5ZCD" => "SABAL POINT",
  "157V3CZM5W92" => "SABAL PALMS UNIT 3",
  "157V3CZM5T52" => "SABAL PALMS CONDO",
  "157V3CZM5PQL" => "SABAL PALM ESTATES UNIT TWO",
  "157V3CZM5MSD" => "SABAL PALM ESTATES UNIT ONE",
  "157V3CZM5JID" => "SABAL LAKE ESTATES UNIT ONE",
  "157V3CZM5G4D" => "SABAL GROVE UNIT TWO",
  "157V3CZM5D4H" => "SABAL GROVE UNIT THREE",
  "157V3CZM59XO" => "SABAL GROVE UNIT ONE",
  "157V3CZM50U7" => "SABAL CHASE",
  "157V3CZM4WTE" => "S W MILLERS UNREC SUBD",
  "157V3CZM4TTJ" => "S F TRAVIS ADDN TO COCOA",
  "157V3CZM4PET" => "S F GRAYS SUBD",
  "157V3CZM4M8Y" => "S B CARTER SURVEY OF GEORGE FLEMING GRANT",
  "157V3CZM4J3O" => "S AND S DEVELOPMENT",
  "157V3CZM4FZF" => "S 98.6 FT OF N 230 FT OF E 175 FT",
  "157V3CZM4CPW" => "S 300 FT OF N 400 FT OF GOVT LOT",
  "157V3CZM49FZ" => "S 3/4 OF NE 1/4 N OF GRANT LINE",
  "157V3CZM463N" => "S 200 FT OF N 250 FT OF E 330 FT",
  "157V3CZM4320" => "S 153.65 FT OF GOVT LOT 4",
  "157V3CZMATCG" => "SANDY PINES PRESERVE PHASE 3",
  "157V3CZMAPLC" => "SANDY PINES PRESERVE PHASE 2",
  "157V3CZMAM4F" => "SANDY PINES PRESERVE PHASE 1",
  "157V3CZMAIJC" => "SANDY KAYE CONDO",
  "157V3CZMADY6" => "SANDPIPER TOWERS CONDO",
  "157V3CZMAA9B" => "SANDPIPER ESTATES",
  "157V3CZMA6R8" => "SANDPIPER COVE AT AQUARINA",
  "157V3CZMA2TQ" => "SANDPIPER AT AQUARINA AQUARINA P.U.D. ST 4 TR II",
  "157V3CZM9ZDL" => "SANDPINES SEC 3",
  "157V3CZM9WLL" => "SANDPINES SEC 2",
  "157V3CZM9SVZ" => "SANDPINES SEC 1",
  "157V3CZM9PD0" => "SANDHURST",
  "157V3CZM9L0P" => "SANDERS SUBD",
  "157V3CZM9HKR" => "SANDERS 1ST SD BEING A RESD OF",
  "157V3CZM9DI8" => "SANDERLING RUN",
  "157V3CZM9AFE" => "SANDCASTLES CONDO",
  "157V3CZM978S" => "SAND REEF CONDO",
  "157V3CZM93YU" => "SAND PEBBLES CONDO",
  "157V3CZM90RC" => "SAND FIRES CONDO",
  "157V3CZM8XK5" => "SAND DUNES OCEANFRONT CONDO",
  "157V3CZM8U9D" => "SAND DUNES HOMES CONDO",
  "157V3CZM8QUH" => "SAND DOLLAR OFFICE CONDO",
  "157V3CZM8NOA" => "SAND DOLLAR CONDO",
  "157V3CZM8KEX" => "SAND CASTLE CONDO",
  "157V3CZM82EQ" => "SANCTUARY THE",
  "157V3CZM8HEA" => "SANCTUARY PHASE 3 THE",
  "157V3CZM8DZ8" => "SANCTUARY PHASE 2 THE",
  "157V3CZM8AEI" => "SANCTUARY OF TITUSVILLE SECTION ONE",
  "157V3CZM87E0" => "SANCTUARY II PHASE TWO",
  "157V3CZM8513" => "SANCTUARY II PHASE ONE",
  "157V3CZM7WIY" => "SAN TOMAS VILLAS PHASE IV",
  "157V3CZM7TDM" => "SAN TOMAS VILLAS PHASE III",
  "157V3CZM7QVS" => "SAN TOMAS VILLAS PHASE II",
  "157V3CZM7ZQQ" => "SAN TOMAS VILLAS",
  "157V3CZM7NNP" => "SAN SEBASTIAN PLAT NO 1",
  "157V3CZM7KMX" => "SAN SEBASTIAN FARMS",
  "157V3CZM7HJS" => "SAN PAULO VILLAGE CONDO",
  "157V3CZMDU49" => "SCOTTSMOOR RESUBD OF PT",
  "157V3CZMDWP7" => "SCOTTSMOOR RESUBD",
  "157V3CZMDR0L" => "SCOTTSMOOR LITTLE TRACTS",
  "157V3CZMDNZM" => "SCOTTSMOOR LITTLE FARMS",
  "157V3CZMDKHW" => "SCOTTSMOOR E EXTENSION RESUBD OF FARMS 24 25 26",
  "157V3CZMDH9K" => "SCOTTSMOOR ANNEX",
  "157V3CZMDZ90" => "SCOTTSMOOR",
  "157V3CZMDDW7" => "SCOBIES SUBD",
  "157V3CZMDAB7" => "SCHUSTERS UNREC SUBD",
  "157V3CZMD6NQ" => "SAWGRASS TOWNHOMES",
  "1AQZWV7BG71H" => "SAWGRASS LAKES",
  "17TN3F8GMAWW" => "SAWGRASS KEY",
  "157V3CZMD30V" => "SAWGRASS EXECUTIVE CONDO",
  "157V3CZMCZ4Q" => "SAWGRASS CONDO HOMEOWNERS INC",
  "157V3CZMCVMK" => "SAWGRASS AT SUNTREE PHASE TWO",
  "157V3CZMCSJ1" => "SAWGRASS AT SUNTREE PHASE THREE",
  "157V3CZMCPB2" => "SAWGRASS AT SUNTREE PHASE ONE",
  "157V3CZMCLV9" => "SAWGRASS AT SUNTREE PHASE FOUR",
  "157V3CZMCIJ5" => "SAWGRASS AT SUNTREE PHASE FIVE",
  "157V3CZMCF67" => "SAVANNAHS PHASE III P.U.D. THE",
  "157V3CZMCBVA" => "SAVANNAHS PHASE II THE",
  "157V3CZMC8HJ" => "SAVANNAHS P.U.D. THE",
  "157V3CZMC5CJ" => "SAUNDERS ADDN TO MELBOURNE",
  "157V3CZMC0YU" => "SATURN CONDO",
  "157V3CZMBXU9" => "SATELLITE BEACH ISLES",
  "157V3CZMBUJQ" => "SATELLITE BEACH ESTATES SUBD",
  "157V3CZMBNYW" => "SARNO INDUSTRIAL PARK CONDO PH II",
  "157V3CZMBLE3" => "SARNO INDUSTRIAL PARK CONDO PH I",
  "157V3CZMBQXZ" => "SARNO INDUSTRIAL PARK",
  "157V3CZMBHWH" => "SARNO HEIGHTS",
  "157V3CZMBEFE" => "SARA LOU ACRES",
  "157V3CZMBB61" => "SANTA ROSA CONDO",
  "157V3CZMB75O" => "SANDY WAVES CONDO",
  "157V3CZMB3MO" => "SANDY SHOES OF MELBOURNE BEACH CONDOMINIUM",
  "157V3CZMB09G" => "SANDY RUN AT SUNTREE",
  "157V3CZMAWS2" => "SANDY PINES PRESERVE PHASE 4",
  "157V3CZMH3H2" => "SEA PARK HOMES REPLAT",
  "157V3CZMGYAO" => "SEA PARK HOMES 4TH ADD",
  "157V3CZMGSCA" => "SEA PARK HOMES 3RD ADD REPLAT",
  "157V3CZMGVG8" => "SEA PARK HOMES 3RD ADD",
  "157V3CZMGPL8" => "SEA PARK HOMES 2ND ADD",
  "157V3CZMH6J7" => "SEA PARK HOMES",
  "157V3CZMGMYI" => "SEA MOON CONDO",
  "157V3CZMGK8B" => "SEA MONICA III",
  "157V3CZMGH9E" => "SEA MONICA II",
  "157V3CZMGE54" => "SEA MONICA I",
  "157V3CZMGAR9" => "SEA JADE CONDO",
  "157V3CZMG7LV" => "SEA ISLE VILLAGE SUBD",
  "157V3CZMG4R7" => "SEA ISLE VILLAGE 3RD ADDN RESUBD OF PART",
  "157V3CZMG1VS" => "SEA ISLE VILLAGE 3RD ADDN REPLAT",
  "157V3CZMFZ0Q" => "SEA ISLE VILLAGE 3RD ADD",
  "157V3CZMFW92" => "SEA ISLE VILLAGE 2ND ADD",
  "157V3CZMFT4X" => "SEA ISLE VILLAGE 1ST ADD",
  "157V3CZMFQ0U" => "SEA HAWK PLACE AT AQUARINA PHASE I AQUARINA PUD ST",
  "157V3CZMFMYJ" => "SEA HAWK PLACE AT AQUARINA PHASE 2",
  "157V3CZMFGYF" => "SEA GATE WEST UNIT TWO",
  "157V3CZMFDXR" => "SEA GATE WEST UNIT THREE",
  "157V3CZMFB21" => "SEA GATE WEST UNIT ONE",
  "157V3CZMFJWF" => "SEA GATE",
  "157V3CZMF890" => "SEA ESCAPE CONDO",
  "157V3CZMF56H" => "SEA ERA SEGUNDO",
  "157V3CZMF2TQ" => "SEA ERA SANDS CONDO",
  "157V3CZMEZZC" => "SEA ERA PATIO HOMES",
  "157V3CZMEX1C" => "SEA DUNES PARK SUBD",
  "157V3CZMETWQ" => "SEA CREST MANOR",
  "157V3CZMEQJ0" => "SEA CREST BEACH",
  "157V3CZMENOK" => "SEA CLUB CONDOMINIUM",
  "157V3CZMEL7E" => "SEA CLIFF CONDO",
  "157V3CZMEH97" => "SEA BREEZE SUN AND GOLF RESORT CONDO",
  "157V3CZMEEDN" => "SEA BREAKERS CONDO",
  "157V3CZMEBUJ" => "SE 1/4 OF NE 1/4 OF NE 1/4",
  "157V3CZME90B" => "SE 1/4 OF NE 1/4 AND SE 1/4 E OF",
  "157V3CZME1QJ" => "SE 1/4 EX LAKE WINDER AS DES IN",
  "157V3CZMKFRA" => "SEMINOLE HEIGHTS",
  "157V3CZMK8X4" => "SEMINOLE GROVE SECTION D",
  "157V3CZMK5SV" => "SEMINOLE GROVE SEC C",
  "157V3CZMK2O5" => "SEGUI COTHRON HEIGHTS SUBD",
  "157V3CZMJZTB" => "SEBASTIAN RIVER HEIGHTS",
  "157V3CZMJWL7" => "SEBASTIAN RIVER ESTATES",
  "17UH82CLZZUN" => "SEBASTIAN LAKES",
  "157V3CZMJTFS" => "SEBASTAIN PINES",
  "157V3CZMJQWM" => "SEASIDE SUBDIVISION",
  "157V3CZMJNSG" => "SEARSTOWN MALL A CONDO",
  "157V3CZMJKO0" => "SEAMARK CONDO",
  "157V3CZMJHXX" => "SEACREST BEACH REPLAT OF",
  "157V3CZMJEQG" => "SEACOAST SHORES UNIT NO 2",
  "157V3CZMJBLL" => "SEACOAST SHORES UNIT NO 1",
  "157V3CZMJ5JM" => "SEACOAST SHORES UNIT 5 SEC 3",
  "157V3CZMJ2HQ" => "SEACOAST SHORES UNIT 5 SEC 2",
  "157V3CZMJ90F" => "SEACOAST SHORES UNIT 5",
  "157V3CZMJ000" => "SEACOAST SHORES UNIT 4",
  "157V3CZMIWO8" => "SEACOAST SHORES UNIT 3",
  "157V3CZMITK6" => "SEACOAST GARDENS APTS CONDO",
  "157V3CZMIQNB" => "SEABURY POINT TRUST",
  "157V3CZMIO3M" => "SEABREEZE SUBD",
  "157V3CZMIKN0" => "SEA WATCH TOWERS",
  "157V3CZMIHB2" => "SEA VILLA CONDO",
  "157V3CZMIDYJ" => "SEA VIEW TOWNHOMES",
  "157V3CZMIAPQ" => "SEA TURTLE CONDO PH II",
  "157V3CZMI5QS" => "SEA TURTLE CONDO PH I",
  "157V3CZMI2KA" => "SEA SPRAY TOWNHOMES CONDO PH II III AND IV",
  "157V3CZMHZKK" => "SEA SPRAY TOWNHOMES CONDO PH I",
  "157V3CZMHS25" => "SEA SHORE TOWNHOUSES PH II",
  "157V3CZMHVYP" => "SEA SHORE TOWNHOUSES",
  "157V3CZMHOLY" => "SEA SHORE ESTATES CONDO PH II",
  "157V3CZMHLGO" => "SEA SHORE ESTATES CONDO PH I",
  "157V3CZMHINY" => "SEA SHELL CAY TOWNHOMES",
  "157V3CZMHFK6" => "SEA SHELL CAY NORTH",
  "157V3CZMHCNF" => "SEA PEARL CONDO",
  "157V3CZMH9M2" => "SEA PARK PLAZA CONDO",
  "157V3CZMNCH9" => "SHADY VILLAS UNIT 2",
  "157V3CZMN9S4" => "SHADY VILLAS UNIT 1",
  "157V3CZMN3NS" => "SHADY SHORES 3RD ADDN",
  "157V3CZMN1A7" => "SHADY SHORES 2ND ADDN",
  "157V3CZMMY4S" => "SHADY SHORES 1ST ADDN",
  "157V3CZMN6LL" => "SHADY SHORES",
  "157V3CZMMV2C" => "SHADY PINE TOWNHOUSES",
  "157V3CZMMRL6" => "SHADY OAKS SUBD",
  "157V3CZMMOB6" => "SHADY OAKS 4TH ADDN",
  "157V3CZMML5S" => "SHADY OAKS 3RD ADDN",
  "157V3CZMMI9H" => "SHADY OAKS 2ND ADDN",
  "157V3CZMMFDN" => "SHADY OAKS 1ST ADDN",
  "157V3CZMMCBA" => "SHADY DELL RIVERVIEW SOUTH CONDO NO 2",
  "157V3CZMM9Y9" => "SHADY DELL RIVERVIEW SOUTH CONDO NO 1",
  "157V3CZMM7M7" => "SHADY DELL RIVERVIEW CONDO PH II",
  "157V3CZMM4RI" => "SHADY DELL RIVERVIEW CONDO PH I",
  "157V3CZMM2BD" => "SHADY DELL III CONDO",
  "157V3CZMLZTV" => "SHADY DELL II CONDO",
  "157V3CZMLWMR" => "SHADY DELL CONDO",
  "157V3CZMLT98" => "SHADOW WOOD I CONDO PH I",
  "157V3CZMLQE2" => "SHADOW LAWN NO 2",
  "157V3CZMLNGD" => "SHADOW LAWN NO 1",
  "157V3CZMLKC9" => "SHADOW GREEN II CONDO",
  "157V3CZMLGW7" => "SHADOW GREEN I CONDO",
  "157V3CZMLBZJ" => "SHADE TREE ESTATES",
  "157V3CZML95B" => "SEVILLE CONDO RESIDENCES OF COCOA BCH",
  "157V3CZML5ZU" => "SEVEN ACRE SUBDIVISION",
  "157V3CZML2WD" => "SETON BY THE SEA CONDO PH III",
  "157V3CZMKZJI" => "SETON BY THE SEA CONDO PH II",
  "157V3CZMKWJM" => "SETON BY THE SEA CONDO PH I",
  "157V3CZMKTRH" => "SERENO POINTE",
  "157V3CZMKQNJ" => "SERENA SHORES CONDO OF IND HBR BCH PH II",
  "157V3CZMKNNZ" => "SERENA SHORES CONDO OF IND HBR BCH PH I",
  "157V3CZMKKR5" => "SENNE HIGHLANDS NO 1",
  "157V3CZMKIF6" => "SEMINOLE LANDINGS CONDO",
  "157V3CZMKBM0" => "SEMINOLE HEIGHTS 1ST ADDN",
  "157V3CZMQAU2" => "SHERWOOD FOREST P.U.D. II STAGE ONE TRACT B REPLAT",
  "157V3CZMQEZ7" => "SHERWOOD FOREST P.U.D. II STAGE ONE",
  "157V3CZMQ5D2" => "SHERWOOD FOREST P.U.D. II REPLAT OF STAGE ONE TR",
  "157V3CZMQ8GM" => "SHERWOOD FOREST P.U.D. II REPLAT OF",
  "157V3CZMQNU1" => "SHERWOOD FOREST",
  "157V3CZMPXIQ" => "SHERWOOD ESTATES UNIT 9",
  "157V3CZMPT86" => "SHERWOOD ESTATES UNIT 8",
  "157V3CZMPQH9" => "SHERWOOD ESTATES UNIT 7",
  "157V3CZMPNCJ" => "SHERWOOD ESTATES UNIT 6A",
  "157V3CZMPJY8" => "SHERWOOD ESTATES UNIT 6",
  "157V3CZMPH16" => "SHERWOOD ESTATES UNIT 5",
  "157V3CZMPDX6" => "SHERWOOD ESTATES UNIT 4",
  "157V3CZMPALW" => "SHERWOOD ESTATES UNIT 3",
  "157V3CZMP72I" => "SHERWOOD ESTATES UNIT 2",
  "157V3CZMP3YO" => "SHERWOOD ESTATES UNIT 15",
  "157V3CZMP1GC" => "SHERWOOD ESTATES UNIT 12",
  "157V3CZMOY9P" => "SHERWOOD ESTATES UNIT 10",
  "157V3CZMOUEC" => "SHERWOOD ESTATES REPLAT NO 1",
  "157V3CZMQ1WW" => "SHERWOOD ESTATES",
  "157V3CZMORZC" => "SHERIFF PLAZA",
  "157V3CZMOOQD" => "SHERIDAN WOODS",
  "157V3CZMOLB8" => "SHERIDAN MANOR",
  "157V3CZMOIPQ" => "SHERIDAN LAKES PHASE 2",
  "157V3CZMOFBW" => "SHERIDAN LAKES PHASE 1",
  "157V3CZMOC5T" => "SHERIDAN GLENN",
  "157V3CZMO96Y" => "SHERIDAN ESTATES",
  "157V3CZMO614" => "SHERIDAN ACRES",
  "157V3CZMO2XI" => "SHERIANA POINT",
  "157V3CZMO04F" => "SHEPHERDS CASBAH UNREC SUDB",
  "157V3CZMNX4D" => "SHELTON BOOTHS 1ST ADDN",
  "157V3CZMNTVD" => "SHAWS ADDITION TO COCOA J.C.",
  "157V3CZMNRGF" => "SHARPES PINE ACRES ESTATES NO 2",
  "157V3CZMNO5S" => "SHARPES PINE ACRES ESTATES NO 1",
  "157V3CZMNL5T" => "SHANGRI-LA",
  "157V3CZMNIT1" => "SHAKESPEARE PARK SEC 2",
  "157V3CZMNFM8" => "SHAKESPEARE PARK SEC 1",
  "157V3CZMU2RV" => "SHOREWOOD CONDO PH IV",
  "157V3CZMTZML" => "SHOREWOOD CONDO PH III",
  "157V3CZMTW2B" => "SHOREWOOD CONDO PH II",
  "157V3CZMTSZ1" => "SHOREWOOD CONDO PH I",
  "157V3CZMTPKW" => "SHORES OF ARTESIA PHASE 2",
  "157V3CZMTMKS" => "SHORES OF ARTESIA PHASE 1",
  "157V3CZMTJDA" => "SHORES HOLDING CO",
  "157V3CZMTGDR" => "SHOPPES AT MINTON",
  "157V3CZMTDAQ" => "SHIRLEY PARK",
  "157V3CZMT7HD" => "SHIRE THE FIRST ADDITION",
  "157V3CZMTA7J" => "SHIRE THE",
  "157V3CZMT446" => "SHIPS WHEEL CONDO",
  "157V3CZMT0L9" => "SHILOH BEACH SUBD",
  "157V3CZMSHAX" => "SHERWOOD VILLAS UNIT 2",
  "157V3CZMSE3Y" => "SHERWOOD VILLAS UNIT 1",
  "157V3CZMSBP3" => "SHERWOOD VILLAS PH I",
  "157V3CZMSXHZ" => "SHERWOOD VILLAS",
  "157V3CZMS92E" => "SHERWOOD RANCHETTES",
  "157V3CZMS06F" => "SHERWOOD PARK SEC J REPLAT NO 1 OF PART OF",
  "157V3CZMS2M0" => "SHERWOOD PARK SEC J",
  "157V3CZMRWY9" => "SHERWOOD PARK SEC H",
  "157V3CZMRTJ7" => "SHERWOOD PARK SEC G",
  "157V3CZMRQTZ" => "SHERWOOD PARK SEC F",
  "157V3CZMRKE7" => "SHERWOOD PARK SEC E REPLAT",
  "157V3CZMRNLN" => "SHERWOOD PARK SEC E",
  "157V3CZMRE9D" => "SHERWOOD PARK SEC D REPLAT",
  "157V3CZMRHNG" => "SHERWOOD PARK SEC D",
  "157V3CZMRBD7" => "SHERWOOD PARK SEC C",
  "157V3CZMR88X" => "SHERWOOD PARK SEC B",
  "157V3CZMR579" => "SHERWOOD PARK SEC A-1",
  "157V3CZMR1YI" => "SHERWOOD PARK SEC A",
  "157V3CZMQZ8Y" => "SHERWOOD LAKES PHASE TWO",
  "157V3CZMQW0O" => "SHERWOOD LAKES PHASE THREE",
  "157V3CZMQT7V" => "SHERWOOD LAKES PHASE ONE",
  "157V3CZMQQXS" => "SHERWOOD GLEN",
  "157V3CZMQKP7" => "SHERWOOD FOREST SECOND DIV",
  "157V3CZMQI53" => "SHERWOOD FOREST P.U.D. II STAGE TWO PHASE ONE",
  "157V3CZMX7OR" => "SLEEPY HOLLOW CONDO",
  "157V3CZMXDBW" => "SLEEPY HOLLOW",
  "157V3CZMX4AD" => "SKYWAY INDUSTRIAL PARK",
  "157V3CZMX0NO" => "SKYWAY COMMERCIAL CONDO",
  "157V3CZMWXP4" => "SKYLINE SUBD",
  "157V3CZMWUJQ" => "SKYLARK ESTATES",
  "157V3CZMWRU8" => "SIX MILE CREEK PHASE IV",
  "157V3CZMWP3L" => "SIX MILE CREEK PHASE III",
  "157V3CZMWLR3" => "SIX MILE CREEK PHASE II",
  "157V3CZMWICQ" => "SIX MILE CREEK PHASE 1",
  "157V3CZMWFXB" => "SISSON MEADOWS",
  "157V3CZMWCW3" => "SISCO PLAT",
  "157V3CZMW9VL" => "SINGLETONS 2ND ADDN TO COCOA",
  "157V3CZMW7BY" => "SINGLETONS 1ST ADDN TO COCOA",
  "157V3CZMW400" => "SIMONSEN TOWNHOUSES",
  "157V3CZMW0N8" => "SILVERADO SUBDIVISION",
  "157V3CZMVY4B" => "SILVER SANDS SUBD",
  "157V3CZMVV1P" => "SILVER SANDS CONDO PH II",
  "157V3CZMVROE" => "SILVER SANDS CONDO PH I",
  "157V3CZMVL2L" => "SILVER PINES ESTATES SEC 5",
  "157V3CZMVIBE" => "SILVER PINES ESTATES SEC 4A",
  "157V3CZMVFXU" => "SILVER PINES ESTATES NO 4",
  "157V3CZMVCIF" => "SILVER PINES ESTATES NO 3",
  "157V3CZMV9IG" => "SILVER PINES ESTATES NO 2",
  "157V3CZMVOBX" => "SILVER PINES ESTATES",
  "157V3CZMV76C" => "SILVER PALM PROFESSIONAL CONDO",
  "157V3CZMV4BE" => "SILVER PALM CONDO",
  "157V3CZMV0DE" => "SILVER LAKE ESTATES",
  "157V3CZMUXVB" => "SILVER HILL SUBDIVISION",
  "157V3CZMUUR5" => "SILK OAK ESTATES",
  "157V3CZMURG6" => "SIESTA PARK",
  "157V3CZMUOKP" => "SIESTA DEL MAR CONDO",
  "157V3CZMUI7D" => "SIERRA LAKES",
  "157V3CZMUFB0" => "SHREE KRISHNA MEDICAL AND PROFESSIONAL PLAZA CONDO",
  "157V3CZMUC0N" => "SHOREWOOD CONDO PH VII",
  "157V3CZMU91A" => "SHOREWOOD CONDO PH VI",
  "157V3CZMU64L" => "SHOREWOOD CONDO PH V",
  "157V3CZN0MSG" => "SOMERSET OCEANFRONT CONDO",
  "157V3CZN0JP2" => "SOLUTIONS WAY OFFICE CONDO",
  "157V3CZN0GC3" => "SOLERNO BOULEVARD AND TAVISTOCK DRIVE PHASE 1 A",
  "157V3CZN0CZ9" => "SOLANA SHORES CONDO PH II",
  "157V3CZN09SK" => "SOLANA SHORES CONDO PH I",
  "157V3CZN064S" => "SOLANA ON THE RIVER CONDO PH V",
  "157V3CZN02UM" => "SOLANA ON THE RIVER CONDO PH IV",
  "157V3CZMZZAH" => "SOLANA ON THE RIVER CONDO PH III",
  "157V3CZMZVBH" => "SOLANA ON THE RIVER CONDO PH II",
  "157V3CZMZSAH" => "SOLANA ON THE RIVER CONDO PH I",
  "157V3CZMZPF6" => "SOLANA LAKE CONDO PH VIII",
  "157V3CZMZMJD" => "SOLANA LAKE CONDO PH VII",
  "157V3CZMZJK8" => "SOLANA LAKE CONDO PH VI",
  "157V3CZMZGUM" => "SOLANA LAKE CONDO PH V",
  "157V3CZMZDRQ" => "SOLANA LAKE CONDO PH IV",
  "157V3CZMZBED" => "SOLANA LAKE CONDO PH III",
  "157V3CZMZ7Z2" => "SOLANA LAKE CONDO PH II",
  "157V3CZMZ51H" => "SOLANA LAKE CONDO PH I",
  "157V3CZMZ1SB" => "SOL Y MAR CONDO",
  "157V3CZMYTG7" => "SOBRE LAS OLAS OF CAPE CANAVERAL CONDO",
  "157V3CZMYQ52" => "SNYDERS SUBD",
  "157V3CZMYN1J" => "SNUG HARBOR VILLAGE SEC 1",
  "157V3CZMYJZD" => "SNUG HARBOR SUBDIVISION",
  "157V3CZMYH0Z" => "SNUG HARBOR ESTATES TRACT C SEC 5",
  "157V3CZMYEQ5" => "SNUG HARBOR ESTATES TRACT B SEC 5",
  "157V3CZMYBDZ" => "SNUG HARBOR ESTATES TRACT A SEC 5",
  "157V3CZMY4ZG" => "SNUG HARBOR ESTATES SUBD SEC 2",
  "157V3CZMY1YF" => "SNUG HARBOR ESTATES SUBD SEC 1",
  "157V3CZMXYFV" => "SNUG HARBOR ESTATES SUBD REPLAT OF PART OF",
  "157V3CZMY7WU" => "SNUG HARBOR ESTATES SUBD",
  "157V3CZMXV30" => "SNUG HARBOR ESTATES SECTION 5",
  "157V3CZMXRNU" => "SNELLS S/D OF LOT 25 HOUSTONS ADD TO EAU GALLIE",
  "157V3CZMXNMJ" => "SNEDEKERS SUBD OF MALABAR",
  "157V3CZMXKQ8" => "SLONERIDGE",
  "157V3CZMXGYJ" => "SLEEPY LAGOON",
  "157V3CZN5JU2" => "SOUTH LAKES PHASE II",
  "157V3CZN5GHQ" => "SOUTH LAKES PHASE I",
  "157V3CZN5D14" => "SOUTH LAKE VILLAGE SEC 2",
  "157V3CZN59JO" => "SOUTH LAKE VILLAGE SEC 1",
  "157V3CZN565D" => "SOUTH LAKE SHORES UNIT 3",
  "157V3CZN53DM" => "SOUTH LAKE SHORES UNIT 2",
  "157V3CZN4ZYZ" => "SOUTH LAKE SHORES UNIT 1",
  "157V3CZN4U3Q" => "SOUTH INDIAN RIVER ISLES SECOND ADDN",
  "157V3CZN4QP7" => "SOUTH INDIAN RIVER ISLES 3RD ADDN",
  "157V3CZN4WV5" => "SOUTH INDIAN RIVER ISLES",
  "157V3CZN4NEJ" => "SOUTH HARBOR ESTATES",
  "157V3CZN4K2G" => "SOUTH GRANT",
  "157V3CZN4GO4" => "SOUTH FORTY THE",
  "157V3CZN4E4Z" => "SOUTH FAIRWAYS CONDO",
  "157V3CZN4ASF" => "SOUTH BREVARD STORAGE FACILITY CONDO",
  "157V3CZN47GC" => "SOUTH BEACH KEYS CONDOMINIUM",
  "157V3CZN443R" => "SOUTH BEACH CONDO",
  "157V3CZN40S9" => "SOUTH BANYAN ISLES",
  "157V3CZN3XJD" => "SORRENTO VILLAGE",
  "157V3CZN3U9L" => "SONOMA SOUTH PHASE 4 VIERA CENTRAL P.U.D. A PORTI",
  "157V3CZN3QM5" => "SONOMA SOUTH PHASE 3 VIERA CENTRAL P.U.D. A PORTIO",
  "157V3CZN3N6E" => "SONOMA SOUTH PHASE 2 VIERA NORTH P.U.D.- A PORTION",
  "157V3CZN3K4W" => "SONOMA SOUTH PHASE 1 VIERA CENTRAL P.U.D. A PORTI",
  "157V3CZN3H77" => "SONOMA AT VIERA PHASES 1 AND 2 VIERA CENTRAL P.U.D",
  "157V3CZN26OD" => "SONOMA AT VIERA PHASE 6 VIERA CENTRAL P.U.D. A POR",
  "157V3CZN1BHO" => "SONOMA AT VIERA PHASE 5 VIERA CENTRAL P.U.D. A POR",
  "157V3CZN18BY" => "SONOMA AT VIERA PHASE 4 VIERA CENTRAL P.U.D. A POR",
  "157V3CZN1587" => "SONOMA AT VIERA PHASE 3 A REPLAT OF TRACT JJ SONOM",
  "157V3CZN0Z7Y" => "SONESTA WALK PHASE TWO",
  "157V3CZN12PA" => "SONESTA WALK",
  "157V3CZN0VMZ" => "SOMERVILLE SUBDIVISION PHASE ONE VIERA NORTH P.U.D",
  "157V3CZN0SEL" => "SOMERVILLE SUBDIVISION PHASE 2 VIERA NORTH PARCE",
  "157V3CZN0PID" => "SOMERSET RIVERFRONT CONDOMINIUM",
  "157V3CZN8M10" => "SOUTHGATE SEC 1",
  "157V3CZN8IUA" => "SOUTHERN PINES",
  "157V3CZN8FUM" => "SOUTHERN COMFORT ESTS SEC 2",
  "157V3CZN8CLG" => "SOUTHERN COMFORT ESTS SEC 1",
  "157V3CZN89IX" => "SOUTH WOODS",
  "157V3CZN86E4" => "SOUTH TROPICAL ESTATES",
  "157V3CZN83GA" => "SOUTH TROPICAL COVE",
  "157V3CZN808X" => "SOUTH SOLERNO COMMERCIAL CENTER",
  "157V3CZN7X5U" => "SOUTH SHORES RIVERSIDE",
  "157V3CZN7U21" => "SOUTH SHORES PHASE II",
  "157V3CZN7R1U" => "SOUTH SHORES PHASE I",
  "157V3CZN7NU9" => "SOUTH SHORES OCEANSIDE CONDO PH I",
  "157V3CZN7H9G" => "SOUTH SEA VILLAGE 1ST ADDN",
  "157V3CZN7KGK" => "SOUTH SEA VILLAGE",
  "157V3CZN7DVD" => "SOUTH POINTE COMMONS A COMMERCIAL CONDO PH I II",
  "157V3CZN7AE4" => "SOUTH PATRICK SHORES 5TH SEC",
  "157V3CZN773Z" => "SOUTH PATRICK SHORES 4TH SEC",
  "157V3CZN73Q5" => "SOUTH PATRICK SHORES 3RD SEC",
  "157V3CZN70HG" => "SOUTH PATRICK SHORES 2ND SEC",
  "157V3CZN6XA0" => "SOUTH PATRICK SHORES 1ST SEC",
  "157V3CZN6QZC" => "SOUTH PATRICK PARK 1ST ADD",
  "157V3CZN6U53" => "SOUTH PATRICK PARK",
  "157V3CZN6NVI" => "SOUTH PATRICK APTS CONDO",
  "157V3CZN6HOR" => "SOUTH OAKS PHASE TWO",
  "157V3CZN6ENX" => "SOUTH OAKS PHASE ONE",
  "157V3CZN6KT0" => "SOUTH OAKS",
  "157V3CZN6BBC" => "SOUTH MERRITT ESTATES PLANTATION SECTION PLAT 1",
  "157V3CZN682F" => "SOUTH MERRITT ESTATES PLANTATION SEC PLAT NO. 2",
  "157V3CZN64T6" => "SOUTH MERRITT ESTATES PINELAND SEC",
  "157V3CZN61QO" => "SOUTH MERRITT ESTATES PINELAND 2ND SEC",
  "157V3CZN5YFS" => "SOUTH MERRITT ESTATES LAKE SEC",
  "157V3CZN5V4K" => "SOUTH MERRITT ESTATES 1ST GROVE SEC",
  "157V3CZN5P7K" => "SOUTH MELBOURNE RESUBD OF LOT 9 BLK H",
  "157V3CZN5S28" => "SOUTH MELBOURNE",
  "157V3CZN5MO5" => "SOUTH LAKES PHASE III",
  "157V3CZNBR6Y" => "SPRING CREEK CONDO NO 7",
  "157V3CZNBN6W" => "SPRING CREEK CONDO NO 6",
  "157V3CZNBJV3" => "SPRING CREEK CONDO NO 5",
  "157V3CZNBGV5" => "SPRING CREEK CONDO NO 4",
  "157V3CZNBE7P" => "SPRING CREEK CONDO NO 3",
  "157V3CZNBAH4" => "SPRING CREEK CONDO NO 2",
  "157V3CZNB6WU" => "SPRING CREEK CONDO NO 1",
  "157V3CZNB3NC" => "SPRAY BEACH APTS CONDO",
  "157V3CZNB09A" => "SPOTTSWOOD AND SALANO PLAT NO 1",
  "157V3CZNAX5F" => "SPOONBILL VILLAS AT AQUARINA STAGE 3 TRACT III",
  "157V3CZNAU0J" => "SPINNAKER POINT",
  "157V3CZNAQBI" => "SPINDRIFT TOWNHOUSES",
  "157V3CZNAN47" => "SPILLERS ADDN TO EAU GALLIE",
  "157V3CZNAJPE" => "SPEED AND SCOTT SUBD",
  "157V3CZNAGGV" => "SPANISH WELLS AT SUNTREE",
  "157V3CZNADDR" => "SPANISH TRACE VILLAGE CONDO",
  "157V3CZNAA62" => "SPANISH TRACE OFFICE CONDO",
  "157V3CZNA77H" => "SPANISH OAKS GOLF VILLAS CONDO",
  "157V3CZNA42A" => "SPANISH OAKS CONDO SEC 3",
  "157V3CZNA0UB" => "SPANISH OAKS CONDO SEC 2",
  "157V3CZN9XMO" => "SPANISH OAKS CONDO SEC 1",
  "157V3CZN9S6T" => "SPANISH MAIN APTS CONDO ADDN III",
  "157V3CZN9OL8" => "SPANISH MAIN APTS CONDO ADDN II",
  "157V3CZN9M79" => "SPANISH MAIN APTS CONDO ADDN I",
  "157V3CZN9UMV" => "SPANISH MAIN APTS CONDO",
  "157V3CZN9JWW" => "SPANISH COVE SUNTREE PUD STAGE 4 TRACT 35 AND A PO",
  "157V3CZN9HAR" => "SPANGLER LAKE ESTATES",
  "157V3CZN9BRE" => "SPACE COAST GARDENS UNIT 2",
  "157V3CZN9E9N" => "SPACE COAST GARDENS",
  "157V3CZN98S5" => "SOUTHOVER",
  "157V3CZN95I6" => "SOUTHMERE",
  "157V3CZN936H" => "SOUTHGATE SEC 3 UNIT 3",
  "157V3CZN90F0" => "SOUTHGATE SEC 3 UNIT 2",
  "157V3CZN8X1T" => "SOUTHGATE SEC 3 UNIT 1",
  "157V3CZN8SJ1" => "SOUTHGATE SEC 2 UNIT 2",
  "157V3CZN8P8K" => "SOUTHGATE SEC 2 UNIT 1",
  "157V3CZNEZTS" => "ST LUCIE GARDENS CONDO IXORA ADDN",
  "157V3CZNEW3V" => "ST LUCIE GARDENS CONDO HIBISCUS ADDN",
  "157V3CZNESSR" => "ST LUCIE GARDENS CONDO GARDENIA ADDN",
  "157V3CZNEPCR" => "ST LUCIE GARDENS CONDO FELICIA ADDN",
  "157V3CZNEM7K" => "ST LUCIE GARDENS CONDO ERICA ADDN",
  "157V3CZNEIEX" => "ST LUCIE GARDENS CONDO DAHLIA ADDN",
  "157V3CZNEG0Z" => "ST LUCIE GARDENS CONDO CAMELIA ADDN",
  "157V3CZNECR3" => "ST LUCIE GARDENS CONDO BEGONIA ADDN",
  "157V3CZNE8B5" => "ST LUCIE GARDENS CONDO AZALEA ADDN",
  "157V3CZNE4MB" => "ST JOHNS VALLEYCAMP SITES REPLAT OF",
  "157V3CZNE1AD" => "ST CLAIR OFFICE CONDOMINIUM",
  "157V3CZNDY7S" => "ST BENEDICT PARK 6TH ADDN",
  "157V3CZNDRPN" => "ST ANDREWS VILLAGE CONDO",
  "157V3CZNDUWH" => "ST ANDREWS VILLAGE",
  "157V3CZNDO94" => "ST ANDREWS TOWNHOMES PHASE 2",
  "157V3CZNDKW9" => "ST ANDREWS TOWNHOMES PHASE 1",
  "157V3CZNDEBU" => "ST ANDREWS ISLE",
  "1AIXLGFT51KJ" => "ST ANDREW MANOR",
  "157V3CZNDALP" => "SPYGLASS WEST PROFESSIONAL CENTER CONDO",
  "157V3CZND5OJ" => "SPYGLASS SOUTH OFFICE CONDO",
  "157V3CZND2E1" => "SPYGLASS PLAZA CONDO PH I",
  "157V3CZNCYW2" => "SPYGLASS OFFICE PARK CONDOMINIUM",
  "157V3CZNCUFO" => "SPYGLASS COMMERCE PARK VIERA D.R.I.-PARCEL NN",
  "157V3CZNCR60" => "SPURGAS 1ST ADDN TO GROVE PARK",
  "157V3CZNCO0Y" => "SPRUCE HILLS SUBD",
  "157V3CZNCKVB" => "SPRUCE HILLS ESTS PLAT NO 2",
  "157V3CZNCHRI" => "SPRUCE HILLS ESTS PLAT NO 1",
  "157V3CZNCEJZ" => "SPRUCE HILLS 1ST ADDN TO",
  "157V3CZNC8AP" => "SPRINGTREE UNIT 3 A CORRECTIVE PLAT OF BLOCK 4 T",
  "157V3CZNCBIY" => "SPRINGTREE UNIT 3",
  "157V3CZNC4UH" => "SPRINGTREE UNIT 2",
  "157V3CZNC19X" => "SPRINGTREE UNIT 1",
  "157V3CZNBUPD" => "SPRING VALLEY SUBD 1ST ADDN",
  "157V3CZNBXY3" => "SPRING VALLEY SUBD",
  "157V3CZNHXNS" => "STRATFORD POINTE PHASE FOUR",
  "157V3CZNHV4N" => "STRATFORD PLACE PHASE 2 VIERA NORTH PUD - PARCEL C",
  "157V3CZNHRQ6" => "STRATFORD PLACE PHASE 1 VIERA NORTH PLANNED UNIT D",
  "157V3CZNHOUQ" => "STONEWOOD TOWERS CONDO PH I AND PH II",
  "157V3CZNHM8A" => "STONES ADD TO MELBOURNE RESUBD OF LOTS 8 TO 12",
  "157V3CZNHJKN" => "STONES ADD TO MELBOURNE J.S.",
  "157V3CZNHG8E" => "STONERIDGE SUBDIVISION",
  "157V3CZNHD3D" => "STONEHEDGE",
  "157V3CZNHAEE" => "STONEBRIDGE",
  "157V3CZNH779" => "STONEBRIAR AT BAYSIDE LAKES",
  "157V3CZNH3NY" => "STONE LAKE ESTATES",
  "157V3CZNH191" => "STILLWATER PRESERVE SUBDIVISION",
  "157V3CZNGYQG" => "STILLWATER LAKES",
  "157V3CZNGW84" => "STILL WATERS",
  "157V3CZNGT4K" => "STILL POINT PHASE II B",
  "157V3CZNGPPX" => "STILL POINT PHASE 11A",
  "157V3CZNGMF2" => "STILL POINT PHASE 1",
  "157V3CZNGIXS" => "STEWARTS HOMESTEAD",
  "157V3CZNGFQA" => "STEWART PLACE",
  "157V3CZNGCNH" => "STERLING HOUSE OF MELBOURNE BCH PH III",
  "157V3CZNG9QM" => "STERLING HOUSE OF MELBOURNE BCH PH II",
  "157V3CZNG704" => "STERLING HOUSE OF MELBOURNE BCH PH I",
  "157V3CZNG4LK" => "STERLING FOREST",
  "157V3CZNG1BP" => "STATE TREE SHORES",
  "157V3CZNFXZ8" => "STARBEACH CONDO",
  "157V3CZNFTQ2" => "STAR RUSH DRIVE EXTENSION VIERA NORTH PUD REMAIND",
  "157V3CZNFQ7Y" => "STANFIELD TOWNHOMES",
  "157V3CZNFN4I" => "STADUIM EAST",
  "17YIWK81N9U2" => "STADIUM VILLAS",
  "157V3CZNFK64" => "STADIUM PARKWAY PHASE 3",
  "157V3CZNFHMH" => "STADIUM PARKWAY PHASE 2",
  "157V3CZNFEN2" => "STADIUM PARKWAY PHASE 1",
  "157V3CZNFC4R" => "STADIUM EAST",
  "157V3CZNF8OE" => "ST MICHEL VILLAGE",
  "157V3CZNF5QK" => "ST LUCIE VILLAS PHASE II AND III",
  "157V3CZNF2JV" => "ST LUCIE VILLAS PHASE I",
  "157V3CZNL5RE" => "SUMMERWOOD VILLAS",
  "157V3CZNL8NH" => "SUMMERWOOD",
  "157V3CZNKZWO" => "SUMMERWIND CONDO OF COCOA BEACH",
  "157V3CZNL2F8" => "SUMMERWIND CONDO",
  "157V3CZNKWDV" => "SUMMERS CREEK PHASE TWO",
  "157V3CZNKTLA" => "SUMMERS CREEK PHASE ONE",
  "157V3CZNKR69" => "SUMMERFIELD AT BAYSIDE LAKES PHASE 3",
  "157V3CZNKO4M" => "SUMMERFIELD AT BAYSIDE LAKES PHASE 2",
  "157V3CZNKKPA" => "SUMMERFIELD AT BAYSIDE LAKES PHASE 1",
  "157V3CZNKHRE" => "SUMMER PLACE CONDOMINIUM",
  "157V3CZNKEZT" => "SUMMER LAKES PHASE 3",
  "157V3CZNKBHS" => "SUMMER LAKES PHASE 2 VIERA CENTRAL P.U.D. A PORTIO",
  "157V3CZNK8AX" => "SUMMER LAKES PHASE 1 VIERA CENTRAL PUD-A PORTION O",
  "157V3CZNK4GS" => "SUMMER COVE CONDO PH I",
  "157V3CZNK0OL" => "SUMMER BROOK SECTION TWO",
  "157V3CZNJX2P" => "SUMMER BROOK SECTION THREE",
  "157V3CZNJTKR" => "SUMMER BROOK SECTION ONE",
  "157V3CZNJQD4" => "SUMMER BROOK SECTION FOUR",
  "157V3CZNJMX1" => "SUMMER BROOK PRESERVE",
  "157V3CZNJJKI" => "SUGAR PINES",
  "157V3CZNJGBJ" => "SUENO DEL RIO CONDOMINIUM",
  "157V3CZNJD5U" => "SUBURBAN HEIGHTS PLAT NO 1",
  "157V3CZNJ9TW" => "SUBD OF W 270 FT OF N 620 FT OF NW 1/4 OF NW 1/4",
  "157V3CZNJ6JM" => "SUBD OF W 1/2 OF NW 1/4 OF SW 1/4 OF NE 1/4",
  "157V3CZNJ3OJ" => "SUBD OF SW 15 ACRES OF SE 1/4 OF SW 1/4",
  "157V3CZNJ16M" => "SUBD OF N 1/2",
  "157V3CZNINWI" => "SUBD OF E 1/2 OF NE 1/4",
  "157V3CZNIGT0" => "STUART TERRACE 1ST ADDN",
  "157V3CZNIKKS" => "STUART TERRACE",
  "17TKLWJNXOJX" => "STROM PARK",
  "157V3CZNIDR0" => "STRAWBRIDGE SUBD",
  "157V3CZNIB96" => "STRAWBRIDGE CIVIC CENTER CONDO",
  "157V3CZNI7LY" => "STRATFORD POINTE PHASE TWO",
  "157V3CZNI45E" => "STRATFORD POINTE PHASE THREE",
  "157V3CZNI0SD" => "STRATFORD POINTE PHASE ONE",
  "157V3CZNODFN" => "SUNNYLAND BEACH SEC 4",
  "157V3CZNOAI1" => "SUNNYLAND BEACH SEC 3",
  "157V3CZNO7JO" => "SUNNYLAND BEACH 1ST ADD",
  "157V3CZNOQR1" => "SUNNYLAND BEACH",
  "157V3CZNO4D8" => "SUNNYBANK ON THE DIXIE",
  "157V3CZNO16D" => "SUNNY SITES UNIT 2",
  "157V3CZNNXO6" => "SUNNY SITES UNIT 1",
  "157V3CZNNULU" => "SUNNY POINT SUBD",
  "157V3CZNNMTR" => "SUNNY PINES NO 1 PARK",
  "157V3CZNNROS" => "SUNNY PINES",
  "157V3CZNNGP9" => "SUNNY ACRES SUBD SEC 3",
  "157V3CZNND2U" => "SUNNY ACRES SUBD SEC 2",
  "157V3CZNNJX7" => "SUNNY ACRES SUBD",
  "157V3CZNNA30" => "SUN VALLEY SUBD",
  "157V3CZNN7QG" => "SUN POINTE",
  "157V3CZNN4UK" => "SUN PALMS CONDO",
  "157V3CZNN1V4" => "SUN N SEA CONDO",
  "157V3CZNMZHE" => "SUN N GREEN CONDO 3",
  "157V3CZNMW7E" => "SUN N GREEN CONDO 2",
  "157V3CZNMSV7" => "SUN N GREEN CONDO 1",
  "157V3CZNMQ1H" => "SUN LAND ISLES",
  "157V3CZNMMV3" => "SUN LAND HARBOUR",
  "157V3CZNMJSL" => "SUN LAKE ESTATES UNIT 2",
  "157V3CZNMGRC" => "SUN LAKE ESTATES UNIT 1",
  "157V3CZNMDS7" => "SUN ISLE CONDO PH II",
  "157V3CZNMAVL" => "SUN ISLE CONDO PH I",
  "157V3CZNM8EA" => "SUN CREST",
  "157V3CZNM58Z" => "SUN CLUB CONDO",
  "157V3CZNM2BZ" => "SUN AND SEA II CONDO PH I",
  "157V3CZNLZM3" => "SUMMIT PHASE 3 THE",
  "157V3CZNLW8K" => "SUMMIT PHASE 2 THE",
  "157V3CZNLSW1" => "SUMMIT PHASE 1 THE",
  "157V3CZNLPRR" => "SUMMIT COVE CONDO PH VI",
  "157V3CZNLMUN" => "SUMMIT COVE CONDO PH V",
  "157V3CZNLK3Y" => "SUMMIT COVE CONDO PH IV",
  "157V3CZNLHON" => "SUMMIT COVE CONDO PH III",
  "157V3CZNLEA2" => "SUMMIT COVE CONDO PH II",
  "157V3CZNLB4C" => "SUMMIT COVE CONDO PH I",
  "157V3CZNRS4R" => "SUNSET LAKES P.U.D. PHASE ONE-A",
  "157V3CZNROJX" => "SUNSET LAKES P.U.D. PHASE IX",
  "157V3CZNRLF0" => "SUNSET ISLAND",
  "157V3CZNRIG9" => "SUNSET HOMES SUBD",
  "157V3CZNRFL7" => "SUNSET HOMES 1ST ADD",
  "157V3CZNR96O" => "SUNSET HILLS AMENDED",
  "157V3CZNRCCR" => "SUNSET HILLS",
  "157V3CZNR2XH" => "SUNSET HARBOUR 1ST ADD REPLAT OF",
  "157V3CZNR649" => "SUNSET HARBOUR",
  "157V3CZNQWA8" => "SUNSET HARBOR CONDO",
  "157V3CZNQZUV" => "SUNSET HARBOR",
  "157V3CZNQT8G" => "SUNSET GROVES UNIT 2",
  "157V3CZNQPYO" => "SUNSET GROVES UNIT 1",
  "157V3CZNQMEB" => "SUNSET GARDENS SUBD",
  "157V3CZNQJ95" => "SUNSET COVE",
  "157V3CZNQFSD" => "SUNSET BAY SUBD",
  "157V3CZNQCLH" => "SUNSET BAY A CONDOMINIUM",
  "157V3CZNQ9AD" => "SUNSET ARMS",
  "157V3CZNQ1B7" => "SUNRISE VILLAS TOWNHOMES",
  "157V3CZNPY3P" => "SUNRISE VILLAGE CONDO PH III",
  "157V3CZNPU0Q" => "SUNRISE VILLAGE CONDO PH II",
  "157V3CZNPQU6" => "SUNRISE VILLAGE CONDO PH I",
  "157V3CZNPNVU" => "SUNRISE TOWER CONDO",
  "157V3CZNPKZD" => "SUNRISE LANDING III CONDO",
  "157V3CZNPICS" => "SUNRISE LANDING II CONDO",
  "157V3CZNPFN0" => "SUNRISE LANDING I CONDO",
  "157V3CZNPC9X" => "SUNRISE CONDOMINIUM APARTMENTS",
  "157V3CZNQ4VW" => "SUNRISE",
  "157V3CZNP8T4" => "SUNNYSIDE TRACT MAP 2",
  "157V3CZNP5WI" => "SUNNYSIDE TERRACE",
  "157V3CZNP3DA" => "SUNNYSIDE PARK",
  "157V3CZNP0FO" => "SUNNYSIDE ADDN TO COCOA J T HODGES PLAT OF",
  "157V3CZNOX8K" => "SUNNYSIDE ADDN TO COCOA DRAWDYS PLAT OF",
  "157V3CZNOU15" => "SUNNYLAND GROVES SUBD",
  "157V3CZNONA8" => "SUNNYLAND BEACH SEC 7",
  "157V3CZNOKAB" => "SUNNYLAND BEACH SEC 6",
  "157V3CZNOGUY" => "SUNNYLAND BEACH SEC 5",
  "157V3CZNV352" => "SUNTREE LAKES PH I",
  "157V3CZNUYX0" => "SUNTREE GOLF COURSE",
  "157V3CZNUV2B" => "SUNTREE FOREST HOMES UNIT TWO",
  "157V3CZNURXM" => "SUNTREE FOREST HOMES UNIT ONE",
  "157V3CZNUPDT" => "SUNTREE ESTATES PHASE 2",
  "157V3CZNUM5Q" => "SUNTREE ESTATES PHASE 1A",
  "157V3CZNUIYN" => "SUNTREE ESTATES PHASE 1",
  "157V3CZNUGEG" => "SUNTREE CHALLENGE GOLF COURSE",
  "157V3CZNUD23" => "SUNTREE CENTER SUNTREE PUD STAGE 3 TRACTS 25A AND",
  "157V3CZNU9WJ" => "SUNTREE BUSINESS CENTER",
  "157V3CZNU77U" => "SUNTREE BAY",
  "157V3CZNU3V7" => "SUNSTONE SUBDIVISION PHASE TWO",
  "157V3CZNU0YS" => "SUNSTONE SUBDIVISION PHASE I",
  "157V3CZNTYCL" => "SUNSHINE GROVE UNIT 7",
  "157V3CZNTVA4" => "SUNSHINE GROVE UNIT 5",
  "157V3CZNTS4Q" => "SUNSHINE GROVE UNIT 3",
  "157V3CZNTP9R" => "SUNSHINE GROVE UNIT 1",
  "157V3CZNTJ22" => "SUNSET TERRACE PLAT NO 3",
  "157V3CZNTMGP" => "SUNSET TERRACE",
  "157V3CZNTEM7" => "SUNSET SHORES SUBD",
  "157V3CZNTB7V" => "SUNSET RIDGE ESTATES",
  "157V3CZNT882" => "SUNSET PARK UNIT 2",
  "157V3CZNT3SR" => "SUNSET PARK UNIT 1",
  "157V3CZNT0GU" => "SUNSET PARK ADDN TO COCOA",
  "157V3CZNSWUJ" => "SUNSET LAKES WEST-ISLAND ESTATES",
  "157V3CZNSTHW" => "SUNSET LAKES PHASE XI",
  "157V3CZNSPW6" => "SUNSET LAKES PHASE VIII",
  "157V3CZNSMAW" => "SUNSET LAKES PHASE VII",
  "157V3CZNSJ4M" => "SUNSET LAKES PHASE VI",
  "157V3CZNSFZF" => "SUNSET LAKES PHASE V AND A REPLAT OF TRACT B SUNSE",
  "157V3CZNSCUC" => "SUNSET LAKES PHASE ONE-B",
  "157V3CZNS9PK" => "SUNSET LAKES PHASE IV-B",
  "157V3CZNS62U" => "SUNSET LAKES PHASE IV-A",
  "157V3CZNS2SZ" => "SUNSET LAKES PHASE III",
  "157V3CZNRZLN" => "SUNSET LAKES P.U.D. PHASE X",
  "157V3CZNRVIK" => "SUNSET LAKES P.U.D. PHASE TWO",
  "157V3CZNXRIW" => "SUNTREE WOODS",
  "157V3CZNXP06" => "SUNTREE VILLA HOMES",
  "157V3CZNXMAS" => "SUNTREE PROFESSIONAL CENTER",
  "157V3CZNXDHT" => "SUNTREE P.U.D. STAGE TRACT 94 PHASE 5 UNIT 1 GLEA",
  "157V3CZNXABQ" => "SUNTREE P.U.D. STAGE 90 TRACT 99",
  "157V3CZNX6Q0" => "SUNTREE P.U.D. STAGE 5 TRACTS H I J K L AND M",
  "157V3CZNX3OL" => "SUNTREE P.U.D. STAGE 5 TRACT 59",
  "157V3CZNX0DT" => "SUNTREE P.U.D. STAGE 5 TRACT 55",
  "157V3CZNWTMB" => "SUNTREE P.U.D. STAGE 4 TRACT D",
  "157V3CZNWQLV" => "SUNTREE P.U.D. STAGE 4 TRACT 31",
  "157V3CZNWKX0" => "SUNTREE P.U.D. STAGE 4 TRACT 29 29 UNIT 2",
  "157V3CZNWNQJ" => "SUNTREE P.U.D. STAGE 4 TRACT 29",
  "157V3CZNWI2E" => "SUNTREE P.U.D. STAGE 4 TRACT 27-B",
  "157V3CZNWWJX" => "SUNTREE P.U.D. STAGE 4",
  "157V3CZNWFBW" => "SUNTREE P.U.D. STAGE 3 TRACT 26 STAGE 4 TRACT 30",
  "157V3CZNWCJH" => "SUNTREE P.U.D. STAGE 3 TRACT 25",
  "157V3CZNW9V9" => "SUNTREE P.U.D. STAGE 2 TRACT 9",
  "157V3CZNW7AZ" => "SUNTREE P.U.D. STAGE 2 PHASE 1",
  "157V3CZNW4KC" => "SUNTREE P.U.D. STAGE 14 TRACT TEN UNIT FOUR REPLAT",
  "157V3CZNW1WC" => "SUNTREE P.U.D. STAGE 14 TRACT 10",
  "157V3CZNVWG8" => "SUNTREE P.U.D. STAGE 10 TRACT 5",
  "157V3CZNVTO9" => "SUNTREE P.U.D. STAGE 10 TRACT 4",
  "157V3CZNVZ8F" => "SUNTREE P.U.D. STAGE 10",
  "157V3CZNVO43" => "SUNTREE P.U.D. STAGE 1 TRACT E",
  "157V3CZNVLAI" => "SUNTREE P.U.D. STAGE 1 TRACT 72",
  "157V3CZNVIG3" => "SUNTREE P.U.D. STAGE 1 TR C UNIT 2",
  "157V3CZNVFZ1" => "SUNTREE P.U.D. STAGE 1 REPLAT OF TRACT B UNIT 1",
  "157V3CZNVCLY" => "SUNTREE P.U.D. STAGE 1 REPLAT OF TR C UNIT 1",
  "157V3CZNVQWF" => "SUNTREE P.U.D. STAGE 1",
  "157V3CZNXG89" => "SUNTREE P.U.D. STAGE",
  "157V3CZNV986" => "SUNTREE P.U.D. REPLAT",
  "157V3CZNXJGS" => "SUNTREE P.U.D.",
  "157V3CZNV62F" => "SUNTREE LAKES PH II",
  "157V3CZO0YAZ" => "SYKES VIEW ESTATES",
  "157V3CZO0VCI" => "SYKES CREEK PROFESSIONAL BUILDING",
  "157V3CZO0S35" => "SWISS POINTE CONDO PH II",
  "157V3CZO0OXT" => "SWISS POINTE CONDO PH I",
  "157V3CZO0LM0" => "SWEETWOOD ESTATES",
  "157V3CZO0IDX" => "SWEETWATER DOWNS",
  "157V3CZO0F58" => "SWEET MAGNOLIA CONDO",
  "157V3CZO08QS" => "SUTTON PLACE ON THE OCEAN CONDO",
  "157V3CZO0BUM" => "SUTTON PLACE",
  "157V3CZO05JI" => "SUSEDA PARK",
  "157V3CZO02BL" => "SURVEY BOOK 9 PGS 64 AND 65",
  "157V3CZNZZC0" => "SURFSIDE UNIT 2",
  "157V3CZNZW19" => "SURFSIDE UNIT 1",
  "157V3CZNZSQ4" => "SURFSIDE PARK",
  "157V3CZNZPHR" => "SURFSIDE HAVEN CONDO",
  "157V3CZNZM95" => "SURFSIDE ESTATES UNIT 7",
  "157V3CZNZISO" => "SURFSIDE ESTATES UNIT 6",
  "157V3CZNZFIY" => "SURFSIDE ESTATES UNIT 5",
  "157V3CZNZCAT" => "SURFSIDE ESTATES UNIT 4 REPLAT",
  "157V3CZNZ4TX" => "SURFSIDE ESTATES UNIT 3 REPLAT OF PART OF",
  "157V3CZNZ8JB" => "SURFSIDE ESTATES UNIT 3",
  "157V3CZNZ1OM" => "SURFSIDE ESTATES UNIT 2",
  "157V3CZNYYYX" => "SURFSIDE ESTATES UNIT 1",
  "157V3CZNYW0W" => "SURFSIDE APTS CONDO",
  "157V3CZNYSKD" => "SURFSIDE 6 OFFICE CONDO",
  "157V3CZNYPQG" => "SURFLAND PALMS UNIT 1",
  "157V3CZNYMWW" => "SURF SIDE VILLA CONDO",
  "157V3CZNYJR3" => "SURF N SUN CONDO",
  "157V3CZNYGZS" => "SUNWOOD PARK SEC 3",
  "157V3CZNYE8C" => "SUNWOOD PARK NO 7",
  "157V3CZNYBDG" => "SUNWOOD PARK NO 6",
  "157V3CZNY6UM" => "SUNWOOD PARK NO 5",
  "157V3CZNY436" => "SUNWOOD PARK NO 4",
  "157V3CZNY104" => "SUNWOOD PARK NO 2",
  "157V3CZNXXX7" => "SUNWOOD PARK NO 1",
  "157V3CZNXUB0" => "SUNTREE-VIERA PROFESSIONAL CENTER OFFICE CONDO",
  "157V3CZO4YV2" => "TERRACE IV AT HERITAGE ISLE CONDO",
  "157V3CZO4VGV" => "TERRACE III AT HERITAGE ISLE CONDO",
  "157V3CZO4RXR" => "TERRACE II AT HERITAGE ISLE CONDO",
  "157V3CZO4OBL" => "TERRACE I AT HERITAGE ISLE CONDO",
  "157V3CZO4KQ2" => "TERRACE GARDEN",
  "157V3CZO4GSV" => "TERRA VERDE CONDO",
  "157V3CZO4D3C" => "TEQUESTA HARBOR",
  "157V3CZO43S2" => "TENNIS VILLAGE SEC 4",
  "157V3CZO4135" => "TENNIS VILLAGE SEC 3",
  "157V3CZO3XPQ" => "TENNIS VILLAGE SEC 2",
  "157V3CZO3U6O" => "TENNIS VILLAGE SEC 1",
  "157V3CZO3QXG" => "TEMPLETON SUBDIVISION VIERA NORTH P.U.D. - PARCEL",
  "157V3CZO3MAF" => "TEMPLE HEIGHTS",
  "157V3CZO3IV5" => "TAYLOR-PECK-TAYLOR PLAT OF ROCKLEDGE",
  "157V3CZO3F7K" => "TAYLOR TERRACE CONDO",
  "157V3CZO385X" => "TAYLOR SUBDIVISION PHASE 2",
  "157V3CZO3BLH" => "TAYLOR SUBDIVISION",
  "157V3CZO34ED" => "TAX ASSESSORS PLAT OF COCOA",
  "157V3CZO30L1" => "TATE UNRECORDED SUBD PT OF TRACT 13 ATKINSON SUBD",
  "157V3CZO2WQA" => "TANGLEWOOD UNIT 4",
  "157V3CZO2JPN" => "TANGLEWOOD UNIT 3",
  "157V3CZO2GG2" => "TANGLEWOOD UNIT 2",
  "157V3CZO2D0F" => "TANGLEWOOD UNIT 1",
  "157V3CZO29EP" => "TANGLEWOOD AT SUNTREE COUNTRY CLUB CONDO STAGE 88",
  "157V3CZO2686" => "TANGLEWOOD AT SUNTREE CNTRY CLUB CONDO PH I",
  "157V3CZO22ZX" => "TAMARIND ESTATES",
  "157V3CZO1ZOH" => "TALLWOOD NORTH PHASE 2",
  "157V3CZO1VSB" => "TALLWOOD NORTH PHASE 1",
  "157V3CZO1P5F" => "TADLOCKS SUBD",
  "157V3CZO1LUF" => "T O F DEVELOPMENT",
  "157V3CZO1I6N" => "SYLVAN SHORES REPLAT",
  "157V3CZO1EO3" => "SYLVAN SHORES PART OF RESUBD OF",
  "157V3CZO1BI3" => "SYLVAN PINES SECTION 1",
  "157V3CZO17XG" => "SYLVAN PINES 1ST ADDITION",
  "157V3CZO14L8" => "SYLVAN ESTATES SEC B",
  "157V3CZO11BH" => "SYLVAN ESTATES SEC A",
  "157V3CZO8C7R" => "THE BLUFFS CONDO PH I",
  "157V3CZO893J" => "THE BEACH HOUSE CONDOMINIUM",
  "157V3CZO8584" => "THE BARRINGER PHASE II",
  "157V3CZO81WM" => "THE BARRINGER CONDO",
  "157V3CZO7YXH" => "THE ARBORS AT LONGLEAF",
  "157V3CZO7WIC" => "THE ANCHORAGE CONDOMINIUM",
  "157V3CZO7TCQ" => "THE ANCHORAGE A CONDOMINIUM",
  "157V3CZO7QA6" => "THE ALOHA CONDO",
  "157V3CZO7O3N" => "THE 850 CONDO",
  "157V3CZO7HDK" => "THE 19TH HOLE CONDO PH II",
  "157V3CZO7L4N" => "THE 19TH HOLE CONDO",
  "157V3CZO7EVQ" => "THAT PT OF TWPS 22 23 26 27 34",
  "157V3CZO7BU4" => "THAT PT OF SEC 19 LYING ON HORTI",
  "157V3CZO78WT" => "THAT PT OF SE 1/4 OF SW 1/4",
  "157V3CZO75PS" => "THAT PT OF N 660 FT OF GOVT LOT 1",
  "157V3CZO72L6" => "THAT PT OF N 3 1/4 CHS OF LOT 1",
  "157V3CZO6Z4C" => "THAT PT OF GOVT LOT 2 E OF RR S",
  "157V3CZO6VSH" => "THAT PT E OF LEVEE",
  "157V3CZO6QXZ" => "THAT PART OF W 1/2 OF SECTION",
  "157V3CZO6FD9" => "THAT PART OF SEC 9 W OF HOG POINT",
  "157V3CZO6C7Q" => "THAT PART OF SEC 12 LYING IN PB",
  "157V3CZO69VG" => "THAT PART OF SEC 11 IN PB 20 PG 2",
  "157V3CZO66JW" => "THAT PART OF SEC 1 E OF RR",
  "157V3CZO63JV" => "THAT PART OF RESERVED TRACT OF PB",
  "157V3CZO614W" => "THAT PART OF NW 1/4 OF NW 1/4 E",
  "157V3CZO5YQR" => "THAT PART OF NW 1/4 OF NE 1/4",
  "157V3CZO5VIY" => "THAT PART OF GOVT LOT 1 AND E 1/2",
  "157V3CZO5SM2" => "THAT PART N OF GRANT LINE",
  "157V3CZO5MC5" => "THAT PART LYING EAST OF",
  "157V3CZO5JKD" => "TEUTONS SUBD",
  "157V3CZO5GRA" => "TERRAMORE SUBDIVISION",
  "157V3CZO5DBZ" => "TERRACES AT NORTH POINTE SEC 2 THE",
  "157V3CZO59VN" => "TERRACES AT NORTH POINTE SEC 1 THE",
  "157V3CZO565J" => "TERRACE SHORES",
  "157V3CZO52B6" => "TERRACE ON THE GREEN",
  "157V3CZOBHOG" => "THE COVE AT SOUTH BEACHES CONDO PH I",
  "157V3CZOBEHV" => "THE COURTYARDS CONDO PH IIF",
  "157V3CZOBBC6" => "THE COURTYARDS CONDO PH IIE",
  "157V3CZOB7LZ" => "THE COURTYARDS CONDO PH IID",
  "157V3CZOB3ZC" => "THE COURTYARDS CONDO PH IIC",
  "157V3CZOB0NI" => "THE COURTYARDS CONDO PH IIB",
  "157V3CZOAX9W" => "THE COURTYARDS CONDO PH IIA",
  "157V3CZOATX3" => "THE COURTYARDS CONDO PH IA IB IC ID IE IF",
  "157V3CZOAQIS" => "THE CONSTELLATION CONDO",
  "157V3CZOAN7F" => "THE CONDOS OF INDIAN HARBOUR PH III B",
  "157V3CZOAJ37" => "THE CONDOS OF INDIAN HARBOUR PH III A",
  "157V3CZOAGLV" => "THE CONDOS OF INDIAN HARBOUR PH II",
  "157V3CZOACYG" => "THE CONDOS OF INDIAN HARBOUR PH I",
  "157V3CZOA9XX" => "THE COMMODORE CONDO PH II",
  "157V3CZOA6OD" => "THE COMMODORE CONDO PH I",
  "157V3CZOA47B" => "THE COMMODORE CLUB CONDO",
  "157V3CZOA0Y3" => "THE COLONIAL HOUSE CONDO",
  "157V3CZO9XKT" => "THE CLUB RESIDENCES CONDO",
  "157V3CZO9UNE" => "THE CLOISTERS PHASE IIID",
  "157V3CZO9RDR" => "THE CLOISTERS PHASE IIIB",
  "157V3CZO9O6O" => "THE CLOISTERS PHASE IIB",
  "157V3CZO9L1B" => "THE CLARKE CONDO",
  "157V3CZO9HZD" => "THE CENTER AT MURRELL AND VIERA",
  "157V3CZO9ELX" => "THE CEDARS APTS CONDO",
  "157V3CZO9BDN" => "THE CASUARINA CLUB CONDO",
  "157V3CZO9821" => "THE CASTAWAY COVE CONDO",
  "157V3CZO94NF" => "THE BREAKERS CONDO PH III",
  "157V3CZO908P" => "THE BREAKERS CONDO PH II",
  "157V3CZO8WZB" => "THE BREAKERS CONDO PH I",
  "157V3CZO8U7C" => "THE BOARDWALK CONDO PH III",
  "157V3CZO8RBL" => "THE BOARDWALK CONDO PH II",
  "157V3CZO8OFV" => "THE BOARDWALK CONDO PH I",
  "157V3CZO8LX0" => "THE BLUFFS CONDO PH IV",
  "157V3CZO8IS6" => "THE BLUFFS CONDO PH III",
  "157V3CZO8FDT" => "THE BLUFFS CONDO PH II",
  "157V3CZOEIV0" => "THE GABLES II AT LAKE IN THE WOODS PH IV",
  "157V3CZOEG5H" => "THE GABLES II AT LAKE IN THE WOODS PH III",
  "157V3CZOEDGN" => "THE GABLES II AT LAKE IN THE WOODS PH II",
  "157V3CZOEA4N" => "THE GABLES II AT LAKE IN THE WOODS PH I",
  "157V3CZOE7BG" => "THE GABLES AT LAKE IN THE WOODS PH V",
  "157V3CZOE4D0" => "THE GABLES AT LAKE IN THE WOODS PH IV",
  "157V3CZOE1F1" => "THE GABLES AT LAKE IN THE WOODS PH III",
  "157V3CZODYFX" => "THE GABLES AT LAKE IN THE WOODS PH II",
  "157V3CZODV49" => "THE GABLES AT LAKE IN THE WOODS PH I",
  "157V3CZODRTF" => "THE FOUR SEASONS CONDO PH X",
  "157V3CZODOQA" => "THE FOUR SEASONS CONDO PH VIII",
  "157V3CZODLG5" => "THE FOUR SEASONS CONDO PH VII",
  "157V3CZODICR" => "THE FOUR SEASONS CONDO PH VI",
  "157V3CZODDKI" => "THE FOUR SEASONS CONDO PH V",
  "157V3CZODANZ" => "THE FOUR SEASONS CONDO PH IX",
  "157V3CZOD7R0" => "THE FOUR SEASONS CONDO PH IV",
  "157V3CZOD4L7" => "THE FOUR SEASONS CONDO PH III",
  "157V3CZOD1EY" => "THE FOUR SEASONS CONDO PH II",
  "157V3CZOCYAR" => "THE FOUR SEASONS CONDO PH I",
  "157V3CZOCUXI" => "THE FALLS AT SHERIDAN PHASE II",
  "157V3CZOCR4L" => "THE FALLS AT SHERIDAN PHASE 1",
  "157V3CZOCMET" => "THE FAIRWAYS AT THE GREAT OUTDOORS",
  "157V3CZOCJHS" => "THE FAIRWAYS APT CONDO",
  "157V3CZOCGM4" => "THE DUNES OF INDIALANTIC CONDO",
  "157V3CZOCD9S" => "THE DUNES AT OCEANSIDE VILLAGE SHOWN ON S.B. 8 PG",
  "157V3CZOC9NN" => "THE DOMINICA APTS CONDO",
  "157V3CZOC76Y" => "THE DIPLOMAT APTS CONDO PH IV",
  "157V3CZOC425" => "THE DIPLOMAT APTS CONDO PH III",
  "157V3CZOC10O" => "THE DIPLOMAT APTS CONDO PH II",
  "157V3CZOBXQO" => "THE DIPLOMAT APTS CONDO PH I",
  "157V3CZOBUJ9" => "THE DELANO ON THE RIVER",
  "157V3CZOBR4Z" => "THE COVE AT SOUTH BEACHES CONDO PH IV",
  "157V3CZOBO9T" => "THE COVE AT SOUTH BEACHES CONDO PH III",
  "157V3CZOBKYW" => "THE COVE AT SOUTH BEACHES CONDO PH II",
  "157V3CZOHAII" => "THE LANDINGS CONDO",
  "157V3CZOH3QC" => "THE LANDINGS AT MELBOURNE CONDO PH II",
  "157V3CZOH74C" => "THE LANDINGS AT MELBOURNE CONDO",
  "157V3CZOH0M2" => "THE JAMESTOWN CONDO",
  "157V3CZOGXGK" => "THE ISLES OF MERRITT CONDO PH II",
  "157V3CZOGU1A" => "THE ISLES OF MERRITT CONDO PH I",
  "157V3CZOGR09" => "THE HORIZON CONDO PH IV",
  "157V3CZOGO22" => "THE HORIZON CONDO PH III",
  "157V3CZOGL4Q" => "THE HORIZON CONDO PH II",
  "157V3CZOGHC5" => "THE HORIZON CONDO PH I",
  "157V3CZOGEAN" => "THE HOMESTEAD CONDO PH VI",
  "157V3CZOGB2B" => "THE HOMESTEAD CONDO PH V",
  "157V3CZOG7Y7" => "THE HOMESTEAD CONDO PH II III IV",
  "157V3CZOG4IS" => "THE HOMESTEAD CONDO PH I",
  "157V3CZGCQ88" => "THE HILLS 2",
  "157V3CZGCMZF" => "THE HILLS",
  "157V3CZOG1CU" => "THE HERON VILLAGES OF CEDAR CREEK CONDO",
  "157V3CZOFY7R" => "THE HERITAGE CONDO",
  "157V3CZOFVA0" => "THE HAMMOCK CONDO I",
  "157V3CZOFS5C" => "THE HAMLET CONDO",
  "157V3CZOFP0R" => "THE GREAT OUTDOORS PREMIER RV/GOLF RESORT XVI",
  "157V3CZOFLJU" => "THE GREAT OUTDOORS PREMIER RV/GOLF RESORT XV",
  "157V3CZOFI9R" => "THE GREAT OUTDOORS PREMIER RV/GOLF RESORT XII A C",
  "157V3CZOFCFJ" => "THE GREAT OUTDOORS PREMIER RV/GOLF RESORT IX PHAS",
  "157V3CZOFFNJ" => "THE GREAT OUTDOORS PREMIER RV/GOLF RESORT IX",
  "157V3CZOF9BR" => "THE GREAT OUTDOORS PREMIER RV/GOLF RESOIRTS XIV",
  "157V3CZOF60L" => "THE GREAT OUTDOORS PREMIER R.V./ GOLF RESORT XI A",
  "157V3CZOF2N6" => "THE GREAT OUTDOORS PREMIER R.V./ GOLF RESORT X A",
  "157V3CZOEZ4R" => "THE GLASS BUILDING CONDO",
  "157V3CZOEVY0" => "THE GEMS CONDO PH 2B 3 4 5 6 7 8",
  "157V3CZOESPQ" => "THE GEMS CONDO PH 2A",
  "157V3CZOEORR" => "THE GEMS CONDO PH 1",
  "15CXO5FHX6AD" => "THE GARDENS OF INDIAN HARBOUR BEACH",
  "157V3CZOELR6" => "THE GABLES II AT LAKE IN THE WOODS PH V",
  "157V3CZOKG9H" => "THE PRESERVE AT COCONUT POINTE CONDO",
  "157V3CZOKDHE" => "THE POLARIS CONDOMINIUM",
  "157V3CZOKAH9" => "THE PLAZA CONDO",
  "157V3CZOK6XH" => "THE PLANTATION PHASE ONE",
  "157V3CZOK3QZ" => "THE PALMS CONDOMINIUM",
  "157V3CZOK16Z" => "THE OFFICE SPACE CONDO PH IV",
  "157V3CZOJXRE" => "THE OFFICE SPACE CONDO PH III",
  "157V3CZOJUE7" => "THE OFFICE SPACE CONDO PH II",
  "157V3CZOJR2D" => "THE OFFICE SPACE CONDO PH I",
  "157V3CZOJNGL" => "THE OCEANS CONDOMINIUM",
  "157V3CZOJK2S" => "THE OCEAN PARADISE CONDO",
  "157V3CZOJGJ9" => "THE OCEAN COVE CONDO",
  "157V3CZOJDHM" => "THE OAKS OF SUNTREE A CONDO",
  "157V3CZOJA2A" => "THE OAKS OF COUNTRY CLUB CONDO PH I",
  "157V3CZOJ6WY" => "THE OAKS CONDO PH VII",
  "157V3CZOJ3VC" => "THE OAKS CONDO PH VI",
  "157V3CZOJ0VL" => "THE OAKS CONDO PH V",
  "157V3CZOIY2K" => "THE OAKS CONDO PH IV",
  "157V3CZOIV21" => "THE OAKS CONDO PH III",
  "157V3CZOIRXB" => "THE OAKS CONDO PH II",
  "157V3CZOIOF9" => "THE OAKS CONDO PH I",
  "157V3CZOILF0" => "THE OAKS AT ROCKLEDGE COUNTRY CLUB PH I",
  "157V3CZOIICO" => "THE OAKS AT LAKE FRONT",
  "157V3CZOIF2I" => "THE MERIDIAN A CONDOMINIUM PH II",
  "157V3CZOIBHV" => "THE MERIDIAN A CONDOMINIUM PH I",
  "157V3CZOI85V" => "THE MEADOWS PLAZA OFFICE CONDO",
  "157V3CZOI52R" => "THE MASTERS CONDO PH II",
  "157V3CZOI0V4" => "THE MASTERS CONDO PH I",
  "157V3CZOHXFB" => "THE MARLIN AT AQUARINA CONDO PH I",
  "157V3CZOHUBF" => "THE MARK CONDO",
  "157V3CZOHQRH" => "THE MARINER APTS CONDO",
  "17TKM7DF9POT" => "THE MARENDA",
  "157V3CZOHNEY" => "THE MANOR CONDO PH I",
  "157V3CZOHK74" => "THE LANDINGS WEST AT MELBOURNE CONDOMINIUM",
  "157V3CZOHGVC" => "THE LANDINGS OF MELBOURNE BEACH PH II",
  "157V3CZOHDPI" => "THE LANDINGS OF MELBOURNE BCH CONDO PH I",
  "157V3CZONL8J" => "THREE MEADOWS PHASE I",
  "157V3CZONHYH" => "THREE FOUNTAINS OF VIERA CONDO",
  "157V3CZONEWM" => "THOUSAND ISLAND VIEW",
  "157V3CZONBQK" => "THOMAS SUBD",
  "157V3CZON8FC" => "THISTLE CREEK",
  "157V3CZON51X" => "THE WINDJAMMER CONDO",
  "157V3CZOMWRX" => "THE WILLOWS PHASE TWO",
  "157V3CZON1JJ" => "THE WILLOWS",
  "157V3CZOMTAF" => "THE WELLINGTON CONDO",
  "157V3CZOMPE1" => "THE WAVECRESTER APTS CONDO",
  "157V3CZOMLGJ" => "THE WAVECREST CONDO",
  "157V3CZOMHOE" => "THE VILLAS ESCONDIDO CONDO PH I",
  "157V3CZOME31" => "THE VILLAS AT AGORA CIRCLE CONDO",
  "157V3CZOMB8N" => "THE VILLAGES OF SEAPORT CONDO",
  "157V3CZOM7Z9" => "THE VILLAGE AT MELBOURNE A CONDO",
  "157V3CZOM560" => "THE TRAILS OF TITUSVILLE",
  "157V3CZOM2G0" => "THE SURFRIDER CONDO OF INDIALANTIC",
  "157V3CZOLYNN" => "THE SPRINGS OF SUNTREE",
  "157V3CZOLVFM" => "THE SONNYDALE WAREHOUSE CONDO",
  "157V3CZOLSLC" => "THE SIXTEENTH HOLE CONDO",
  "157V3CZOLPE6" => "THE SEAPORT OCEAN FRONT CONDO",
  "157V3CZOLLOK" => "THE SEA PEARL CONDO PH I",
  "157V3CZOLHUM" => "THE SEA OATS CONDO PH II",
  "157V3CZOLEZW" => "THE SEA OATS CONDO PH I",
  "157V3CZOLBQG" => "THE SEA GULL MOTEL TIME SHARE CONDO",
  "157V3CZOL9JL" => "THE SANDS CONDO",
  "157V3CZOL780" => "THE SANDPRINT CONDO",
  "157V3CZOL4UM" => "THE RIDGEWOOD CLUB CONDO PH II",
  "157V3CZOL2AH" => "THE RIDGEWOOD CLUB CONDO PH I",
  "157V3CZOKZOF" => "THE RESORT ON COCOA BEACH TIME-SHARE",
  "157V3CZOKX8V" => "THE RESORT ON COCOA BEACH PH II.",
  "157V3CZOKUR3" => "THE RESORT ON COCOA BEACH PH I",
  "157V3CZOKSEO" => "THE RESERVE AT MEADOWRIDGE",
  "157V3CZOKP00" => "THE REGENCY OF MELBOURNE BCH CONDO",
  "157V3CZOKLYL" => "THE REEF CONDO",
  "157V3CZOKJ0B" => "THE PROFESSIONAL CENTER CONDO",
  "157V3CZOQSJU" => "TITUSVILLE HEIGHTS",
  "157V3CZOQPBA" => "TITUSVILLE FRUIT AND FARMS CO SUBD",
  "157V3CZOQM5Y" => "TITUSVILLE FRUIT AND FARM LAND CO",
  "157V3CZOR83S" => "TITUSVILLE",
  "157V3CZOQJ5D" => "TITUSVILLAGE SEC XII",
  "157V3CZOQG26" => "TITUSVILLAGE SEC XI",
  "157V3CZOQCWD" => "TITUSVILLAGE SEC VIII",
  "157V3CZOQ9FM" => "TITUSVILLAGE SEC VII",
  "157V3CZOQ6D1" => "TITUSVILLAGE SEC VI",
  "157V3CZOQ2X2" => "TITUSVILLAGE SEC V",
  "157V3CZOPZJT" => "TITUSVILLAGE SEC IX",
  "157V3CZOPWF1" => "TITUSVILLAGE SEC IV",
  "157V3CZOPT65" => "TITUSVILLAGE SEC III",
  "157V3CZOPPX9" => "TITUSVILLAGE SEC II",
  "157V3CZOPNOU" => "TITUSVILLAGE SEC I",
  "157V3CZOPFXP" => "TITUS WOODS REPLAT I",
  "157V3CZOPKEF" => "TITUS WOODS",
  "157V3CZOPBNL" => "TITUS ADDN TO TITUSVILLE",
  "157V3CZOP85L" => "TITCOMB RESUBD OF BLKS D E G AND PT OF BLK F OF R",
  "157V3CZOP4X1" => "TIMUQUANA SUBD",
  "157V3CZOP1IQ" => "TIMBERS WEST",
  "157V3CZOOXSG" => "TIMBERLINE ESTATES UNIT 3",
  "157V3CZOOUPD" => "TIMBERLINE ESTATES UNIT 2",
  "157V3CZOORFH" => "TIMBERLINE ESTATES UNIT 1",
  "157V3CZOOO2K" => "TIMBERLAKE",
  "157V3CZOOKLR" => "TIMBER WEST UNIT TWO",
  "157V3CZOOHG3" => "TIMBER RIDGE SUBDIVISION",
  "157V3CZOOEBG" => "TIMBER LANE",
  "157V3CZOO82H" => "TILLMAN PLAT OF",
  "157V3CZOOB09" => "TILLMAN",
  "17TLNPPBP537" => "TIFFANY WOODS",
  "157V3CZOO4E3" => "TIERRA DE MARIA",
  "157V3CZOO1NP" => "TIDEWATER CONDO NO 1",
  "157V3CZONY5R" => "TIDES INN CONDO",
  "157V3CZONUYP" => "THREE MEADOWS PHASE IV",
  "157V3CZONRJR" => "THREE MEADOWS PHASE III",
  "157V3CZONOEH" => "THREE MEADOWS PHASE II",
  "157V3CZOU1PK" => "TRAFFORD WEST TRAFFORD DRIVE WEST OF LAKE ANDREW",
  "157V3CZOTYS5" => "TRAFFORD CORNERS",
  "157V3CZOTVN0" => "TRADEWINDS TERRACE",
  "157V3CZOTPBM" => "TRADEWINDS HOMES SUBD REPLAT OF PT OF",
  "157V3CZOTSDN" => "TRADEWINDS HOMES SUBD",
  "157V3CZOTMB4" => "TRACT OF SUBMERGED LAND N OF 520",
  "157V3CZOTFVP" => "TRACT OF SUBMERGED LAND IN SEC 33",
  "157V3CZOTJ46" => "TRACT OF SUBMERGED LAND IN",
  "157V3CZOTCRB" => "TRACT H AT INDIAN RIVER COLONY CLUB",
  "157V3CZOT4X6" => "TRACT 28 AS DES IN ORB 643 PG 800",
  "157V3CZOT1QA" => "TOWNHOUSE ESTATES NORTH",
  "157V3CZOSYH6" => "TOWNHOMES OF VILLAS BY THE SEA CONDO",
  "17TNG6GV4EPW" => "TOWNHOMES OF SATELLITE BEACH",
  "157V3CZOSVDQ" => "TOWNHOMES AND VILLAS OF DIANA",
  "157V3CZOSS7P" => "TOWN OF EAU GALLIE BEACH",
  "157V3CZOSOR5" => "TOWN HOUSE ESTATES SEC 2",
  "157V3CZOSLLC" => "TOWN HOUSE ESTATES SEC 1",
  "157V3CZOSIF0" => "TOWN HOMES OF COCOA BEACH CONDO",
  "157V3CZOSEZE" => "TORTOISE VIEW VILLAS",
  "157V3CZOSBWB" => "TORTOISE VIEW ESTATES",
  "157V3CZOS8XU" => "TORTOISE ISLAND PH 4 P.U.D.",
  "157V3CZOS5UI" => "TORTOISE ISLAND PH 3 UNIT 2 P.U.D.",
  "157V3CZORZD4" => "TORTOISE ISLAND PH 3 UNIT 1 P.U.D REPLAT OF TR D",
  "157V3CZOS2NT" => "TORTOISE ISLAND PH 3 UNIT 1",
  "157V3CZORW6L" => "TORTOISE ISLAND PH 2 UNIT 2 P.U.D.",
  "157V3CZORSWI" => "TORTOISE ISLAND PH 2 UNIT 1 P.U.D.",
  "157V3CZORPSV" => "TORTOISE ISLAND PH 1 P.U.D.",
  "157V3CZORMKD" => "TOPAZ OCEANFRONT CONDO",
  "157V3CZORJEY" => "TOPANGA VILLAS CONDO",
  "157V3CZORGAE" => "TOMAHAWK CONDO",
  "157V3CZORBY1" => "TOLLEY ESTATES REPLAT",
  "157V3CZOR4TX" => "TITUSVILLE WAL-MART",
  "157V3CZOR1VS" => "TITUSVILLE SECURITIES CO SUBD",
  "157V3CZOQZBD" => "TITUSVILLE SECTION XIII",
  "157V3CZOQVNE" => "TITUSVILLE MEDICAL PLAZA CONDO",
  "157V3CZOX1B5" => "TROPICAL VILLAS UNIT 2",
  "157V3CZOWY8E" => "TROPICAL VILLAS UNIT 1",
  "157V3CZOWV5F" => "TROPICAL TRAILER TOWN",
  "157V3CZOWRYQ" => "TROPICAL TRAIL ESTATES",
  "157V3CZOWOPI" => "TROPICAL PARADISE SUBDIVISION",
  "157V3CZOWL5W" => "TROPICAL ISLAND",
  "157V3CZOWHR0" => "TROPICAL GARDENS",
  "157V3CZOWB9X" => "TROPICAL COVE CONDO PH VII",
  "157V3CZOW7V7" => "TROPICAL COVE CONDO PH VI",
  "157V3CZOW4Y7" => "TROPICAL COVE CONDO PH V",
  "157V3CZOW1JT" => "TROPICAL COVE CONDO PH IV",
  "157V3CZOVY8C" => "TROPICAL COVE CONDO PH III",
  "157V3CZOVV4V" => "TROPICAL COVE CONDO PH II",
  "157V3CZOVRQ9" => "TROPICAL COVE CONDO PH I",
  "157V3CZOWEHZ" => "TROPICAL COVE",
  "157V3CZOVOKC" => "TROPIC PT WATERFRONT EST REPLAT",
  "157V3CZOVL78" => "TROPIC POINT WATERFRONT ESTATES REPLAT OF",
  "157V3CZOVI0S" => "TROPIC HOME PLAT",
  "157V3CZOVEEQ" => "TRITON ARMS APT CONDO",
  "1AL1G6852TXA" => "TRINITY RIVERFRONT CONDOMINIUM",
  "157V3CZOVAYC" => "TREUTLERS SUBD",
  "157V3CZOV7ZM" => "TREETOP VILLAGE CONDO",
  "157V3CZOV5LN" => "TREASURE LAGOON",
  "157V3CZOV28A" => "TREASURE ISLAND CLUB CONDO PH III",
  "157V3CZOUYUK" => "TREASURE ISLAND CLUB CONDO PH II",
  "157V3CZOUVE3" => "TREASURE ISLAND CLUB CONDO PH I",
  "157V3CZOUS21" => "TREASURE COAST HARBOUR VILLAS CONDO",
  "157V3CZOUOU0" => "TREADWELL SUBDIVISION",
  "157V3CZOULHD" => "TRANQUILITY",
  "1ASWBH9QNAJN" => "TRALEE BAY SHORES PHASE TWO",
  "1ASWBH4Q2B79" => "TRALEE BAY SHORES PHASE THREE",
  "157V3CZOUIGR" => "TRALEE BAY SHORES PHASE ONE",
  "157V3CZOU5E6" => "TRAILS THE",
  "157V3CZOUC7P" => "TRAILS END SECTION TWO",
  "157V3CZOU8JW" => "TRAILS END SECTION THREE",
  "157V3CZOUFF2" => "TRAILS END",
  "157V3CZP080Z" => "UNIVERSITY PARK SEC A",
  "157V3CZP02OT" => "UNIVERSITY PARK REPLAT OF PT OF SEC A PB 12 PG 27",
  "157V3CZP05DH" => "UNIVERSITY PARK REPLAT OF PT OF SEC A",
  "157V3CZOZTUF" => "UNIVERSITY ESTATES",
  "157V3CZOZQQE" => "TYLER GARDENS",
  "157V3CZOZO5D" => "TWO RIVERS UNIT NO 1",
  "157V3CZOZKAU" => "TWO RIVERS REPLAT OF",
  "157V3CZOZH2C" => "TWO OAKS",
  "157V3CZOZED4" => "TWIN TOWERS CONDO",
  "157V3CZOZAZ9" => "TWIN RIVERS SUBDIVISION",
  "157V3CZOZ80I" => "TWIN PALM APTS CONDO",
  "157V3CZOZ55X" => "TWIN LAKES OF SUNTREE",
  "17SOCKX1OMTV" => "TWIN LAKES COUNTRY HOMES CONDO PH 1",
  "157V3CZOZ28D" => "TWIN LAKES AT SUNTREE 2ND ADDN",
  "157V3CZOYZ6U" => "TWIN LAKES AT SUNTREE 1ST ADDN",
  "157V3CZOYVU9" => "TWENTY-ONE RIVERSIDE DRIVE CONDO",
  "157V3CZOYSN7" => "TUSCANY ESTATES",
  "157V3CZOYPU3" => "TUSCANY CENTER CONDO",
  "157V3CZOYN78" => "TURTLE MOUND RANCHETTES",
  "157V3CZOYJVF" => "TURTLE KEY OF COCOA BEACH CONDO",
  "157V3CZOYGC5" => "TURTLE CREEK SUBDIVISION",
  "157V3CZOYDCH" => "TURTLE CREEK BUSINESS CONDO",
  "157V3CZOY9TT" => "TURTLE BAY",
  "157V3CZOY6WC" => "TURKEY RIVER ESTATES",
  "157V3CZOY3PI" => "TURKEY CREEK VILLAS CONDO PH II",
  "157V3CZOY0B3" => "TURKEY CREEK VILLAS CONDO PH I",
  "157V3CZOXXFM" => "TURKEY CREEK SUBD",
  "157V3CZOXU3R" => "TULL AND PAXTONS SUBD",
  "157V3CZOXRE0" => "TUFTED OAKS SUBDIVISION",
  "157V3CZOXOO2" => "TUCKAWAY SHORES RESORT CONDO",
  "157V3CZOXLM2" => "TUCKAWAY LAKE ESTATES",
  "157V3CZOXICQ" => "TRUMBULL OFFICE CONDO",
  "157V3CZOXF6V" => "TROPICANA SUBD",
  "157V3CZOX8RF" => "TROPICANA RESUBD OF",
  "157V3CZOX5MY" => "TROPICANA REPLAT OF LOT 11 BLK A",
  "157V3CZOXC3T" => "TROPICANA ESTATES REPLAT",
  "157V3CZP3HQR" => "VENTANA PHASE TWO",
  "157V3CZP3EN5" => "VENTANA PHASE ONE",
  "157V3CZP3B7W" => "VENETIAN WAY SEC 3",
  "157V3CZP388S" => "VENETIAN WAY SEC 2",
  "157V3CZP35JA" => "VENETIAN WAY SEC 1",
  "157V3CZP330C" => "VENETIAN WAY BLK B",
  "157V3CZP303J" => "VENETIAN WAY BLK A",
  "157V3CZP2XKL" => "VENETIAN VILLAGE CONDO PH II THRU XIII",
  "157V3CZP2UT1" => "VENETIAN VILLAGE CONDO PH I",
  "157V3CZP2S0W" => "VENETIAN PLAZA",
  "157V3CZP2P04" => "VENETIAN GARDENS SUBD",
  "157V3CZP2LX6" => "VENETIAN BAY CONDO PH II",
  "157V3CZP2IIG" => "VENETIAN BAY CONDO PH I",
  "157V3CZP2B78" => "VECTORSPACE",
  "157V3CZP27S0" => "VARNUM PARK SUBD",
  "157V3CZP24QE" => "VANGUARD ESTATES UNIT 1",
  "157V3CZP21LK" => "VANGUARD ESTATES PHASE II",
  "157V3CZP1Z0K" => "VALKARIA HEIGHTS UNREC SUBD",
  "157V3CZP1W5G" => "VALENTINE ESTATE SUBD",
  "157V3CZP1PXD" => "VALENCIA ACRES AS PER DB 339 PG 146",
  "157V3CZP1STN" => "VALENCIA",
  "157V3CZP1N1P" => "VADCLAR SUBD",
  "157V3CZP1JW5" => "VACATION VILLAS II CONDO PH IV TIMESHARE",
  "157V3CZP1HFS" => "VACATION VILLAS II CONDO PH III TIMESHARE",
  "157V3CZP1DV4" => "VACATION VILLAS II CONDO PH II TIMESHARE",
  "157V3CZP1AHV" => "VACATION VILLAS II CONDO PH I TIMESHARE",
  "157V3CZP16Y9" => "VACATION VILLAS CONDO PH I TIMESHARE",
  "157V3CZP0VWN" => "V I P VACATION ISLAND PLAYGROUND SEC 1",
  "157V3CZP0T9D" => "UNSURVEYED LAND LYING W OF GOVT",
  "157V3CZP0Q2C" => "UNRECORDED BARBERRY PER SB 7 PGS 91 92 93",
  "157V3CZP0NFP" => "UNREC-SUBD OF PT OF SE 1/4 OF SE 1/4 AS DESC IN DB",
  "157V3CZP0JXT" => "UNREC SUBD AS PLATTED IN DB 353 PG 103",
  "157V3CZP0GPW" => "UNREC PLAT OF S 210 FT OF GOVT LOT 4",
  "157V3CZP0E4A" => "UNREC CANAVERAL GROVES SUBD",
  "157V3CZP0AOW" => "UNIVERSITY PARK SEC B",
  "157V3CZP6HR2" => "VIERA NORTH P.U.D. PARCEL G-1 PHASE THREE B",
  "157V3CZP6ENT" => "VIERA NORTH P.U.D. PARCEL G-1 PHASE 5 OSPREY RID",
  "157V3CZP6BR7" => "VIERA NORTH P.U.D. PARCEL CC",
  "157V3CZP68VX" => "VIERA NORTH P.U.D. G-2 PH 1 OF PB 41 PG 45 AND OF",
  "157V3CZP65R1" => "VIERA HEALTH COMPLEX",
  "157V3CZP62CE" => "VIERA EAST MARKET CENTRE",
  "157V3CZP5ZD7" => "VIERA CENTRAL PUD TRACT 12 UNIT 1 PARCELS 1-3 PHA",
  "157V3CZP5WCN" => "VIERA BOULEVARD EXTENSION",
  "157V3CZP5T4T" => "VIERA BOULEVARD COMMERCE PARK",
  "157V3CZP5PY9" => "VICTORIAN APTS OF CAPE CANAVERAL CONDO",
  "157V3CZP5LVD" => "VICTORIA HOMES",
  "157V3CZP5G1F" => "VICKERS SUBD NO 3",
  "157V3CZP59RK" => "VICKERS SUBD NO 2",
  "157V3CZP5IUD" => "VICKERS SUBD",
  "157V3CZP56V9" => "VETTER ISLES ESTS SEC 4",
  "157V3CZP53O1" => "VETTER ISLES ESTS SEC 3",
  "157V3CZP50K1" => "VETTER ISLES ESTS SEC 2",
  "157V3CZP4UJ5" => "VETTER ISLES ESTS SEC 1 REPLAT",
  "157V3CZP4XJX" => "VETTER ISLES ESTS SEC 1",
  "157V3CZP4R3E" => "VETERANS CITY UNIT 7",
  "157V3CZP4O2J" => "VETERANS CITY UNIT 6",
  "157V3CZP4L39" => "VETERANS CITY UNIT 5",
  "157V3CZP4I8H" => "VETERANS CITY UNIT 4",
  "157V3CZP4EWM" => "VETERANS CITY UNIT 3",
  "157V3CZP4BYI" => "VETERANS CITY UNIT 2",
  "157V3CZP48VP" => "VETERANS CITY REPLAT OF",
  "157V3CZP45IA" => "VERSAILLES SUR LA MER CONDO",
  "157V3CZP42EQ" => "VERONICA ESTATES PHASE ONE",
  "157V3CZP3ZJG" => "VERNON HEIGHTS",
  "157V3CZP3WYR" => "VERANDA PLACE",
  "181HXYI9YAT5" => "VERANDA ESTATES",
  "157V3CZP3U4W" => "VENTURA AT TURTLE CREEK CONDO PH IV",
  "157V3CZP3R6D" => "VENTURA AT TURTLE CREEK CONDO PH III",
  "157V3CZP3NXH" => "VENTURA AT TURTLE CREEK CONDO PH II",
  "157V3CZP3KVY" => "VENTURA AT TURTLE CREEK CONDO PH I",
  "157V3CZP92WX" => "VIERA SOUTH P.U.D. TRACTS MM AND QQ PHASES FOUR-",
  "157V3CZP9014" => "VIERA SOUTH P.U.D. TRACTS MM AND QQ PHASE THREE",
  "157V3CZP8WZN" => "VIERA REGIONAL PARK BEING A PORTION OF PARCEL 2 V",
  "157V3CZP8UY8" => "VIERA PUD TRACT B-3 WOODHALL",
  "157V3CZP8SUD" => "VIERA NORTH PUD-TRACT Z AND A PORTION OF CLUBHOUSE",
  "157V3CZP8QDC" => "VIERA NORTH PUD TRACT L PHASE 2 UNIT 3",
  "157V3CZP8NCK" => "VIERA NORTH PUD TRACT L PHASE 1 UNIT 2",
  "157V3CZP8K3R" => "VIERA NORTH PUD TRACT L PHASE 1 UNIT 1",
  "157V3CZP8H0D" => "VIERA NORTH PUD TRACT C-A1 AND 2 PHASE 4",
  "157V3CZP8E1J" => "VIERA NORTH PUD TRACT C-A1 AND 2 PHASE 3",
  "157V3CZP8B03" => "VIERA NORTH PUD TRACT C-2",
  "157V3CZP87UC" => "VIERA NORTH PUD TRACT B-2",
  "157V3CZP8505" => "VIERA NORTH PUD TRACT AA",
  "157V3CZP81WR" => "VIERA NORTH PUD PARCEL P",
  "157V3CZP7YYG" => "VIERA NORTH PUD PARCEL G-2 PH 3",
  "157V3CZP7VX1" => "VIERA NORTH PUD PARCEL G-2 PH 2 AND VIERA NORTH PU",
  "157V3CZP7SQL" => "VIERA NORTH PUD PARCEL G-1 PHASE THREE A",
  "157V3CZP7N2K" => "VIERA NORTH PUD PARCEL G-1 PHASE ONE",
  "157V3CZP7PY6" => "VIERA NORTH PUD PARCEL G-1 PHASE FOUR",
  "157V3CZP7KII" => "VIERA NORTH PUD PARCEL F-1",
  "157V3CZP7I2V" => "VIERA NORTH P.U.D. TRACT L PHASE 2 UNIT 5",
  "157V3CZP7FE6" => "VIERA NORTH P.U.D. TRACT L PHASE 2 UNIT 4",
  "157V3CZP79YJ" => "VIERA NORTH P.U.D. TRACT D3 PHASES 3 AND 4",
  "157V3CZP7C9A" => "VIERA NORTH P.U.D. TRACT D3",
  "157V3CZP76QZ" => "VIERA NORTH P.U.D. TRACT D-2 PHASES 3 AND 4",
  "157V3CZP73OG" => "VIERA NORTH P.U.D. TRACT D-2 PHASE 2",
  "157V3CZP70RM" => "VIERA NORTH P.U.D. TRACT D-2 PHASE 1",
  "157V3CZP6WG5" => "VIERA NORTH P.U.D. TRACT D-1",
  "157V3CZP6TE8" => "VIERA NORTH P.U.D. TRACT C-A1 AND 2 PHASES 1 AND 2",
  "157V3CZP6QL2" => "VIERA NORTH P.U.D. TRACT C-1",
  "157V3CZP6NVZ" => "VIERA NORTH P.U.D. TRACT B-4",
  "157V3CZP6KT0" => "VIERA NORTH P.U.D. PARCEL K-2 A REPLAT OF A PORT",
  "157V3CZPC536" => "VILLA DE PALMAS SYKES COVE SEC 03",
  "157V3CZPBYD8" => "VILLA DE PALMAS SYKES COVE SEC 02 A REPLATOF LOTS",
  "157V3CZPC1HJ" => "VILLA DE PALMAS SYKES COVE SEC 02",
  "157V3CZPC7OR" => "VILLA DE PALMAS SYKES COVE",
  "157V3CZPBU4V" => "VILLA CIELO",
  "157V3CZPBQYQ" => "VILLA CAMPANIA A CONDOMINIUM",
  "157V3CZPBNL5" => "VIERA-TRACT N-PHASE TWO UNIT SIX-A",
  "157V3CZPBKE6" => "VIERA TRACTS SS AND RR",
  "157V3CZPBHGD" => "VIERA TRACTS BBAND V PHASE 1 UNIT 1",
  "157V3CZPBE1V" => "VIERA TRACTS BB AND V PHASES 4 AND 5 OF VIERA NORT",
  "157V3CZPBAWC" => "VIERA TRACTS BB AND V PHASE 3 OF VIERA NORTH P.U.D",
  "157V3CZPB7TP" => "VIERA TRACTS BB AND V PHASE 2",
  "157V3CZPB4TH" => "VIERA TRACTS BB AND V PH 1 UNIT 2",
  "157V3CZPB1M6" => "VIERA TRACT N PHASE TWO UNIT THREE B",
  "157V3CZPAYJG" => "VIERA TRACT N PHASE TWO UNIT THREE A",
  "157V3CZPAVCI" => "VIERA TRACT N PHASE TWO UNIT FOUR",
  "157V3CZPAS54" => "VIERA TRACT N PHASE TWO UNIT FIVE",
  "157V3CZPALJY" => "VIERA TRACT N PHASE ONE UNIT 2",
  "157V3CZPAOUU" => "VIERA TRACT N PHASE ONE",
  "157V3CZPAGSQ" => "VIERA TRACT JJ PHASE ONE",
  "157V3CZPADHQ" => "VIERA TRACT II PHASE THREE B",
  "157V3CZPAA9X" => "VIERA TRACT II PHASE THREE A",
  "157V3CZPA765" => "VIERA TRACT II PHASE I",
  "157V3CZPA3QQ" => "VIERA TRACT II PHASE 2",
  "157V3CZPA05C" => "VIERA TRACT FF PHASE III",
  "157V3CZP9WPH" => "VIERA TRACT FF PHASE II",
  "157V3CZP9TFQ" => "VIERA TRACT FF PHASE 1",
  "157V3CZP9QBM" => "VIERA TRACT DD",
  "157V3CZP9N44" => "VIERA TRACT D-4 PHASE TWO",
  "157V3CZP9JW8" => "VIERA TRACT D-4 PHASE THREE",
  "157V3CZP9GQL" => "VIERA TRACT D-4 PHASE ONE",
  "157V3CZP9DJP" => "VIERA TRACT D-4 PHASE FOUR",
  "157V3CZP999V" => "VIERA SOUTH PUD TRACTS MM AND QQ PHASES ONE AND",
  "157V3CZP960F" => "VIERA SOUTH P.U.D. TRACTS MM AND QQ PHASES SEVEN",
  "157V3CZPFEPT" => "VILLAGE PROFESSIONAL OFFICE CONDO",
  "157V3CZPFBJK" => "VILLAGE PLAT OF EAU GALLIE",
  "157V3CZPF6Q5" => "VILLAGE OF TRAMORE",
  "157V3CZPF37J" => "VILLAGE OF TITUSVILLE",
  "157V3CZPF08O" => "VILLAGE GROVE OF LA CITA PHASE 1 UNIT 2",
  "157V3CZPEXC7" => "VILLAGE GROVE OF LA CITA PHASE 1 UNIT 1 A PATIO",
  "157V3CZPEU9S" => "VILLAGE GREEN ESTATES",
  "157V3CZPEREN" => "VILLAGE GREEN CONDO OF MELBOURNE",
  "157V3CZPEOHC" => "VILLAGE GARDENS OF MELBOURNE A CONDOMINIUM",
  "157V3CZPELI4" => "VILLAGE DRIVE PROFESSIONAL CENTER",
  "157V3CZPEIQR" => "VILLAGE DRIVE OFFICE CONDO",
  "157V3CZPEF3R" => "VILLA VISTA CONDO",
  "157V3CZPEC0O" => "VILLA VERNE",
  "1AQYIDZ2HQ8D" => "VILLA VERDE",
  "157V3CZPE8U5" => "VILLA TOSCANA CONDO",
  "157V3CZPE5YV" => "VILLA SIENA OF SATELLITE BEACH CONDO",
  "157V3CZPE32H" => "VILLA ROSA ESTATES",
  "157V3CZPDZND" => "VILLA RIVIERA CLUB INC CONDO",
  "157V3CZPDWKZ" => "VILLA DEL MAR SEC 8",
  "157V3CZPDTFH" => "VILLA DEL MAR SEC 7",
  "157V3CZPDQ7I" => "VILLA DEL MAR SEC 6 UNIT 1",
  "157V3CZPDMR5" => "VILLA DEL MAR SEC 6 3RD UNIT",
  "157V3CZPDJEF" => "VILLA DEL MAR SEC 6 2ND UNIT",
  "157V3CZPDGA4" => "VILLA DEL MAR SEC 5",
  "157V3CZPDCM2" => "VILLA DEL MAR SEC 4",
  "157V3CZPD8AZ" => "VILLA DEL MAR SEC 3",
  "157V3CZPD54E" => "VILLA DEL MAR SEC 2",
  "157V3CZPD1M7" => "VILLA DEL MAR SEC 1",
  "157V3CZPCX36" => "VILLA DEL MAR APTS CONDO",
  "157V3CZPCTQW" => "VILLA DE TROVA",
  "157V3CZPCQN6" => "VILLA DE PALMAS UNIT 6",
  "157V3CZPCNHL" => "VILLA DE PALMAS UNIT 5",
  "157V3CZPCKHU" => "VILLA DE PALMAS UNIT 4",
  "157V3CZPCHGE" => "VILLA DE PALMAS UNIT 3",
  "157V3CZPCDYZ" => "VILLA DE PALMAS UNIT 2",
  "157V3CZPCASM" => "VILLA DE PALMAS UNIT 1",
  "157V3CZPI6TL" => "VISTA DEL LAGO UNIT NO 3",
  "157V3CZPI44R" => "VISTA DEL LAGO UNIT NO 2",
  "157V3CZPI0X2" => "VISTA DEL LAGO UNIT NO 1",
  "157V3CZPHW7R" => "VIRGINIA PARK RESUBD",
  "157V3CZPHTP8" => "VIRGINIA PARK REPLAT OF BLK 26",
  "157V3CZPHYMO" => "VIRGINIA PARK",
  "157V3CZPHR55" => "VIRGINIA ESTATES",
  "157V3CZPHOCE" => "VINCENT W KIRBY INDUSTRIAL PARK",
  "157V3CZPHLIM" => "VILLAS PARKING CONDO",
  "157V3CZPHIMZ" => "VILLAS ON THE GREEN",
  "157V3CZPHFVM" => "VILLAS OF WEST MELBOURNE PHASE II III",
  "157V3CZPHCYY" => "VILLAS OF WEST MELBOURNE PHASE I",
  "157V3CZPH8JX" => "VILLAS OF WEST MELBOURNE PH IV",
  "157V3CZPH68P" => "VILLAS AT SUNTREE UNIT TWO REPLAT OF PT OF PARCEL",
  "157V3CZPH2RK" => "VILLAS AT SUNTREE UNIT ONE SUNTREE PUD STAGE 10 T",
  "157V3CZPH0DI" => "VILLAS AT SUNTREE UNIT 3 A RE- PLAT OF A PT OF PA",
  "157V3CZPGXUH" => "VILLAS AT SUNSET POINT",
  "157V3CZPGV1V" => "VILLAS AT SAWGRASS",
  "157V3CZPGS6H" => "VILLAS AT NEWFOUND HARBOR THE",
  "157V3CZPGPQ7" => "VILLAS AT LACITA CONDO",
  "157V3CZPGMSB" => "VILLAS AT INDIAN RIVER THE",
  "157V3CZPGJV5" => "VILLAS AT DUNMORE",
  "157V3CZPGH56" => "VILLAGIO TOWNHOMES",
  "157V3CZPGEJL" => "VILLAGIO DEL MAR",
  "157V3CZPGB9Q" => "VILLAGER CONDO",
  "157V3CZPG7V7" => "VILLAGE WOODS AT LA CITA PHASE TWO",
  "157V3CZPG59N" => "VILLAGE WOODS AT LA CITA PHASE THREE",
  "157V3CZPG21Q" => "VILLAGE WOODS AT LA CITA PHASE ONE",
  "157V3CZPFYZW" => "VILLAGE SQUARE OF TITUSVILLE CONDO PH VII",
  "157V3CZPFW08" => "VILLAGE SQUARE OF TITUSVILLE CONDO PH V",
  "157V3CZPFT0S" => "VILLAGE SQUARE OF TITUSVILLE CONDO PH IV",
  "157V3CZPFPK0" => "VILLAGE SQUARE OF TITUSVILLE CONDO PH III",
  "157V3CZPFLBW" => "VILLAGE SQUARE OF TITUSVILLE CONDO PH II",
  "157V3CZPFI64" => "VILLAGE SQUARE OF TITUSVILLE CONDO PH I",
  "157V3CZPL8O1" => "WALKABOUT P.U.D.",
  "157V3CZPL2O7" => "WALDEN OAKS",
  "157V3CZPKZJQ" => "WAL-MART AT VIERA",
  "157V3CZPKWHD" => "WAL-MART AT PALM BAY",
  "157V3CZPKPO3" => "WAKEFIELD TERR REPLAT OF",
  "157V3CZPKT00" => "WAKEFIELD",
  "157V3CZPKMIN" => "WAGGONER HEIGHTS SUBD",
  "157V3CZPKHM7" => "WAGERS SUBD OF LOTS 3 4 5 6 BLK A",
  "157V3CZPKEDU" => "W T WELLS MAP NO 4",
  "157V3CZPKB98" => "W S MAXWELLS",
  "157V3CZPK858" => "W P ARNOLDS SUBD OF A PORTION OF",
  "157V3CZPK4UT" => "W L WILLIAMS RESUB OF HARDEEVILLE",
  "157V3CZPK1T6" => "W HIGHLAND ADD TO MELBOURNE",
  "157V3CZPJYJZ" => "W 5/8 W OF GRANT LINE EX S 1/4",
  "157V3CZPJVBI" => "W 3/4 EX W 2450 FT AND ORB 2787",
  "157V3CZPJS76" => "W 260 FT OF S 530 FT OF N 790 FT",
  "157V3CZPJOWD" => "W 250 FT OF E 300 FT OF NE 1/4",
  "157V3CZPJLI8" => "W 250 FT E OF OLD US HWY 1 OF N",
  "157V3CZPJIFC" => "W 208.7 FT OF E 384.7 FT OF",
  "157V3CZPJFAW" => "W 139.83 FT OF E 380.80 FT OF N",
  "157V3CZPJC04" => "W 1/4 OF NE 1/4 OF NE 1/4 EX ORB",
  "157V3CZPJ991" => "W 1/2 OF SE 1/4 OF NE 1/4",
  "157V3CZPJ6JW" => "W 1/2 OF NW 1/4",
  "157V3CZPJ47M" => "W 1/2 OF NE 1/4 SE 1/4 OF SE 1/4",
  "157V3CZPJ1A8" => "W 1/2 OF NE 1/4 OF NE 1/4 OF NE",
  "157V3CZPIZ12" => "W 1/2 OF NE 1/4 OF NE 1/4 EX",
  "157V3CZPIWPY" => "W 1/2 OF NE 1/4 AND ALL NW 1/4 EX",
  "157V3CZPIU3Z" => "W 1/2 OF LOT 32 EX RD R/W OF FLA",
  "157V3CZPIQNV" => "W 1/2 EX E 1/2 OF NE 1/4 OF",
  "157V3CZPIN6H" => "VIZCAYA OCEANFRONT A CONDO",
  "157V3CZPIKGW" => "VIZCAYA ESTATES",
  "157V3CZPIHVO" => "VIVA P LAPHAM SUBD",
  "157V3CZPIFC6" => "VISTAGRANDE",
  "157V3CZPICEU" => "VISTA POINTE",
  "157V3CZPI9GF" => "VISTA DEL MAR TOWNHOMES",
  "157V3CZPO8VU" => "WATERWAY ESTATES 2ND ADDN",
  "157V3CZPO5I3" => "WATERWAY ESTATES 1ST ADDN",
  "157V3CZPOR0L" => "WATERWAY ESTATES",
  "157V3CZPO2WT" => "WATERSTONE PLAT TWO P.U.D. HERON BAY MODEL CENTER",
  "157V3CZPNZ18" => "WATERSTONE PLAT THREE P.U.D. HERON BAY PHASE 2",
  "157V3CZPNVOI" => "WATERSTONE PLAT ONE P.U.D.",
  "157V3CZPNSD2" => "WATERSTONE PLAT ONE - REPLAT BEING A REPLAT OF LOT",
  "157V3CZPNOZT" => "WATERSONG SOUTH AT VIERA",
  "157V3CZPNLKW" => "WATERSONG AT VIERA",
  "157V3CZPNEUC" => "WATERSIDE AT SUNTREE PH I",
  "157V3CZPNB8R" => "WATERSIDE AT LA CITA",
  "157V3CZPNI88" => "WATERSIDE",
  "157V3CZPN7H3" => "WATERS MARK PLANTATION",
  "157V3CZPN0D5" => "WATERS EDGE WEST CONDO",
  "157V3CZPMWQM" => "WATERS EDGE APTS CONDO",
  "157V3CZPN3V9" => "WATERS EDGE",
  "157V3CZPMT1W" => "WATERMILL",
  "157V3CZPMPOB" => "WATERFORD POINTE UNIT TWO",
  "157V3CZPMMBX" => "WATERFORD POINTE UNIT ONE",
  "157V3CZPMJIM" => "WATERFORD PLACE UNIT TWO",
  "157V3CZPMGGC" => "WATERFORD PLACE UNIT THREE",
  "157V3CZPMD4W" => "WATERFORD PLACE UNIT ONE",
  "157V3CZPM9Y1" => "WATERFORD PLACE UNIT FOUR",
  "157V3CZPM6ZH" => "WATERFORD BAY CONDO PH II",
  "157V3CZPM3VO" => "WATERFORD BAY CONDO PH I",
  "157V3CZPM0RS" => "WATER WALK CONDO PH III",
  "157V3CZPLXNM" => "WATER WALK CONDO PH II",
  "157V3CZPLUI4" => "WATER WALK CONDO PH I",
  "157V3CZPLQYX" => "WASHINGTONIA BUILDING CONDOMINIUM",
  "157V3CZPLO45" => "WASHINGTON TOWNHOMES",
  "157V3CZPLL4Q" => "WASHINGTON PARK",
  "157V3CZPLHY7" => "WASHINGTON ARMS CLUB CONDO",
  "157V3CZPLESF" => "WALNUT HILLS UNIT 2",
  "157V3CZPLBRQ" => "WALNUT HILLS UNIT 1",
  "157V3CZPL5QF" => "WALKABOUT P.U.D. THAT PART OF TRACT N LYING IN T 2",
  "157V3CZPRTIJ" => "WESTBROOKE PHASE IV",
  "157V3CZPRQ7T" => "WESTBROOKE PHASE III",
  "157V3CZPRMLM" => "WESTBROOKE PHASE II",
  "157V3CZPRITP" => "WESTBROOKE PHASE 1",
  "157V3CZPRFRC" => "WEST WIND",
  "157V3CZPRCE2" => "WEST POINT ADD TO SNUG HARBOR EST",
  "157V3CZPR91A" => "WEST MELBOURNE LANDINGS",
  "157V3CZPR1TL" => "WEST LINK TOWNHOMES II",
  "157V3CZPR5M2" => "WEST LINK TOWNHOMES",
  "157V3CZPQVAE" => "WEST LAKE VILLAGE UNIT TWO",
  "157V3CZPQS2M" => "WEST LAKE VILLAGE UNIT ONE",
  "157V3CZPQYEH" => "WEST LAKE",
  "157V3CZPQOUT" => "WEST HARBOUR VIEW EST",
  "157V3CZPQM73" => "WENDYS BUSINESS PARK",
  "157V3CZPQIE8" => "WELLS SUBD MAP NO 5",
  "157V3CZPQFDZ" => "WELLS MAP NO 3",
  "157V3CZPQBYZ" => "WELLINGTON STATION",
  "157V3CZPQ8I1" => "WELLINGTON AT BAYSIDE LAKES",
  "157V3CZPQ5FM" => "WEINBERG SUBD",
  "157V3CZPQ21I" => "WEHMEYERS ADD TO COCOA BEACH",
  "157V3CZPPY93" => "WEDGE HOLLOW",
  "157V3CZPPVBM" => "WEBER WOODS",
  "157V3CZPPOEB" => "WEBER ROAD ACRES",
  "157V3CZPPHRH" => "WATERWAY TOWNHOUSE CONDO 2 BLDG 14 AND 15",
  "157V3CZPPE9C" => "WATERWAY TOWNHOUSE CONDO 2 BLDG 12A",
  "157V3CZPPARO" => "WATERWAY TOWNHOUSE CONDO 2 BLDG 12",
  "157V3CZPP7IL" => "WATERWAY TOWNHOUSE CONDO 2 BLDG 11",
  "157V3CZPP511" => "WATERWAY TOWNHOUSE CONDO 2 BLDG 10",
  "157V3CZPPL48" => "WATERWAY TOWNHOUSE CONDO 2",
  "157V3CZPP1KW" => "WATERWAY TOWNHOUSE CONDO 1",
  "157V3CZPOY67" => "WATERWAY MANOR UNIT 2",
  "157V3CZPOUOS" => "WATERWAY MANOR UNIT 1",
  "157V3CZPONDW" => "WATERWAY ESTATES REPLAT",
  "157V3CZPOJVU" => "WATERWAY ESTATES 5TH ADDN",
  "157V3CZPOG2O" => "WATERWAY ESTATES 4TH ADDN",
  "157V3CZPOCEH" => "WATERWAY ESTATES 3RD ADDN",
  "157V3CZPV2YD" => "WEXFORD P.U.D. STAGE TWO THE COURTYARD AT WEXFORD",
  "157V3CZPUZRD" => "WEXFORD P.U.D. STAGE ONE",
  "157V3CZPUWLA" => "WEXFORD CONDO PH II",
  "157V3CZPUTTA" => "WEXFORD CONDO PH I",
  "157V3CZPUQL7" => "WETLANDS AT OAK GROVE",
  "157V3CZPUMJ2" => "WESTWOODS",
  "157V3CZPU9I3" => "WESTWOOD VILLAS 8TH ADDN AMENDED PLAT OF BLOCK A",
  "157V3CZPUCVG" => "WESTWOOD VILLAS 8TH ADDN",
  "157V3CZPU6AQ" => "WESTWOOD VILLAS 7TH ADDN",
  "157V3CZPU2JM" => "WESTWOOD VILLAS 6TH ADDN",
  "157V3CZPTZAT" => "WESTWOOD VILLAS 5TH ADDN",
  "157V3CZPTWC1" => "WESTWOOD VILLAS 4TH ADDN",
  "157V3CZPTT66" => "WESTWOOD VILLAS 3RD ADDN",
  "157V3CZPTQ0J" => "WESTWOOD VILLAS 2ND ADDN",
  "157V3CZPTMFO" => "WESTWOOD VILLAS 1ST ADDN",
  "157V3CZPUFYM" => "WESTWOOD VILLAS",
  "157V3CZPTJBJ" => "WESTWOOD MANOR",
  "157V3CZPTG2K" => "WESTWOOD GARDENS SUBD",
  "157V3CZPTD3G" => "WESTWOOD CONDO",
  "157V3CZPUJJY" => "WESTWOOD",
  "157V3CZPT9TH" => "WESTWARD SUBDIVISON",
  "157V3CZPT6JT" => "WESTVIEW ESTATES SEC 1",
  "157V3CZPT37T" => "WESTVIEW CONDO",
  "157V3CZPSWOD" => "WESTOVER HEIGHTS",
  "157V3CZPSZVR" => "WESTOVER",
  "157V3CZPSTP1" => "WESTON VILLAGE",
  "157V3CZPSQER" => "WESTON PARK PHASE V",
  "157V3CZPSNEW" => "WESTON PARK PHASE IV",
  "157V3CZPSKFZ" => "WESTON PARK PHASE III",
  "157V3CZPSHI0" => "WESTON PARK PHASE II B",
  "157V3CZPSEFZ" => "WESTON PARK PHASE I",
  "157V3CZPSBG7" => "WESTLAND HEIGHTS AMENDED PLAT OF",
  "157V3CZPS8DB" => "WESTFIELD ESTATES SUB",
  "157V3CZPS5E2" => "WESTERWOLD",
  "157V3CZPS2F1" => "WESTBROOKE PHASE VII REPLAT OF LOTS 5 6 AND 7 OF F",
  "157V3CZPRZ7Q" => "WESTBROOKE PHASE VI",
  "157V3CZPRW8E" => "WESTBROOKE PHASE V",
  "157V3CZPYB43" => "WICKHAM FOREST PHASE 1",
  "157V3CZPY8HZ" => "WICKHAM EXECUTIVE CENTER CONDO",
  "157V3CZPY58F" => "WICKHAM BUSINESS PARK CONDOMINIUM",
  "157V3CZPXYV4" => "WICHITA ACRES",
  "157V3CZPXUQU" => "WHITLEY BAY WEST CONDO",
  "157V3CZPXRBI" => "WHITLEY BAY CONDO",
  "157V3CZPXO3Q" => "WHITE ACRES SUBD REPLAT",
  "157V3CZPXL2P" => "WHISPERING WOODS PHASE I",
  "157V3CZPXESI" => "WHISPERING WINDS CONDO",
  "157V3CZPXI0D" => "WHISPERING WINDS",
  "157V3CZPX8I7" => "WHISPERING PINES SUBD PLAT NO 2",
  "157V3CZPXBLN" => "WHISPERING PINES SUBD",
  "157V3CZPX59N" => "WHISPERING PINES ACRES",
  "157V3CZPX1QT" => "WHISPERING PINE ESTATES",
  "157V3CZPWUHR" => "WHISPERING OAKS CONDO PH I",
  "157V3CZPWS22" => "WHISPERING OAKS 2ND SEC",
  "157V3CZPWYKM" => "WHISPERING OAKS",
  "157V3CZPW5HJ" => "WHISPERING LAKES REPLAT OF 3RD SEC PART 4",
  "157V3CZPWOWY" => "WHISPERING LAKES 3RD SEC PART 4",
  "157V3CZPWLPO" => "WHISPERING LAKES 3RD SEC PART 3",
  "157V3CZPWIA7" => "WHISPERING LAKES 3RD SEC PART 1",
  "157V3CZPWEZN" => "WHISPERING LAKES 2ND SEC REPLAT OF PT OF BLKS 2 3",
  "157V3CZPWBUV" => "WHISPERING LAKES 2ND SEC AMENDED PLAT OF",
  "157V3CZPW8T9" => "WHISPERING LAKES 1ST SEC",
  "157V3CZPVZLR" => "WHISPERING HILLS GOLF ESTATES ADDN NO 2",
  "157V3CZPVWA9" => "WHISPERING HILLS GOLF ESTATES ADDN NO 1",
  "157V3CZPW2K1" => "WHISPERING HILLS GOLF ESTATES",
  "157V3CZPVT1H" => "WHISPERING HILLS COUNTRY CLUB ESTS SEC 4",
  "157V3CZPVPJS" => "WHISPERING HILLS COUNTRY CLUB ESTS SEC 3",
  "157V3CZPVM4C" => "WHISPERING HILLS COUNTRY CLUB ESTS SEC 2",
  "157V3CZPVJ17" => "WHISPERING HILLS COUNTRY CLUB ESTATES SEC NO 5",
  "157V3CZPVFUY" => "WHISPER WOODS SUBD",
  "157V3CZPVCK3" => "WHISPER LAKE",
  "157V3CZPV6AY" => "WHIPPOORWILL SUBDIVISION",
  "157V3CZQ3PJR" => "WINCHESTER COVE",
  "157V3CZQ3M8C" => "WILSON RAY SUBD",
  "157V3CZQ3G3E" => "WILSON GARDENS REPLAT LOTS 5 TO 10",
  "157V3CZQ3JD7" => "WILSON GARDENS",
  "157V3CZQ3DH8" => "WILLOW RUN",
  "157V3CZQ376B" => "WILLOW LAKES RV RESORT CONDO PHASE IIA",
  "157V3CZQ3ALG" => "WILLOW LAKES RV RESORT",
  "157V3CZQ344N" => "WILLOW GREEN AT LA CITA PHASE TWO",
  "157V3CZQ3153" => "WILLOW GREEN AT LA CITA PHASE THREE",
  "157V3CZQ2Y57" => "WILLOW GREEN AT LA CITA PHASE ONE",
  "157V3CZQ2VLO" => "WILLOW CREEK COMMERCIAL CENTER PHASE 1",
  "157V3CZQ2SJJ" => "WILLOW BEND CONDO",
  "157V3CZQ2PBJ" => "WILLIAMSBURG VILLAGE NORTH",
  "157V3CZQ2M0Y" => "WILLIAMSBURG UNIT 3",
  "157V3CZQ2IV6" => "WILLIAMSBURG UNIT 2",
  "157V3CZQ2FKP" => "WILLIAMSBURG UNIT 1",
  "157V3CZQ2CE2" => "WILLIAMS SUBD",
  "157V3CZQ29CB" => "WILLIAMS POINT ESTATES",
  "157V3CZQ264F" => "WILLIAMS POINT COMPLEX",
  "157V3CZQ22S1" => "WILLIAMS BROS ADDN TO ROCKLEDGE",
  "157V3CZQ1ZJI" => "WILEYS SUBD OF LOTS 6 TO 10 BLK 1",
  "157V3CZQ1W1I" => "WILCOX PLAT OF MELBOURNE BEACH RESUBD OF BLKS 11 2",
  "157V3CZQ1S9L" => "WILCOX MELBOURNE BEACH",
  "157V3CZQ1OTT" => "WILBURS SUBD",
  "157V3CZQ1L31" => "WILBUR SUBD",
  "157V3CZQ0A66" => "WICKHAM ROAD EXTENSION",
  "157V3CZPZ2WY" => "WICKHAM ROAD BUSINESS CENTER SOUTH PHASE 1",
  "157V3CZPYZUB" => "WICKHAM ROAD BUSINESS CENTER NORTH PHASE 1",
  "157V3CZPYWQ3" => "WICKHAM PLAZA WEST",
  "157V3CZPYTRT" => "WICKHAM PLAZA OFFICE CONDO",
  "1AJ51RVZSQN3" => "WICKHAM PLACE",
  "157V3CZPYQTE" => "WICKHAM OAKS SEC 1",
  "157V3CZPYNUI" => "WICKHAM OAKS REPLAT",
  "157V3CZPYKRL" => "WICKHAM FOREST PHASE 4",
  "157V3CZPYH9M" => "WICKHAM FOREST PHASE 3",
  "157V3CZPYE7H" => "WICKHAM FOREST PHASE 2",
  "157V3CZQ6PMD" => "WINDRUSH CONDO",
  "157V3CZQ6JMT" => "WINDOVER WOODS CONDO PH III",
  "157V3CZQ6GHK" => "WINDOVER WOODS CONDO PH II",
  "157V3CZQ6DOP" => "WINDOVER WOODS CONDO PH I",
  "157V3CZQ6B7I" => "WINDOVER OF COCOA APT CONDO",
  "157V3CZQ66VI" => "WINDOVER FARMS UNIT 7",
  "157V3CZQ63M8" => "WINDOVER FARMS UNIT 6",
  "157V3CZQ607N" => "WINDOVER FARMS UNIT 5",
  "157V3CZQ5W9N" => "WINDOVER FARMS UNIT 4",
  "157V3CZQ5T04" => "WINDOVER FARMS UNIT 3-A",
  "157V3CZQ5O8S" => "WINDOVER FARMS UNIT 3",
  "157V3CZQ5LMM" => "WINDOVER FARMS UNIT 2-A",
  "157V3CZQ5IFE" => "WINDOVER FARMS UNIT 2",
  "157V3CZQ5FAS" => "WINDOVER FARMS UNIT 1",
  "157V3CZQ592C" => "WINDOVER FARMS OF MELBOURNE PUD PHASE TWO",
  "157V3CZQ55P9" => "WINDOVER FARMS OF MELBOURNE PUD PHASE THREE UNIT F",
  "157V3CZQ52H5" => "WINDOVER FARMS OF MELBOURNE PUD PHASE ONE",
  "157V3CZQ4VB1" => "WINDOVER FARMS OF MELBOURNE PUD PHASE FIVE UNIT O",
  "157V3CZQ4Y5U" => "WINDOVER FARMS OF MELBOURNE PUD PHASE FIVE UNIT FO",
  "157V3CZQ4S4G" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 5 UNIT 2",
  "157V3CZQ4OW5" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 04 UNIT 03",
  "157V3CZQ4MDW" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 04 UNIT 02",
  "157V3CZQ4JE7" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 04 UNIT 01",
  "157V3CZQ4GEN" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 03 UNIT 03",
  "157V3CZQ4DK8" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 03 UNIT 02",
  "157V3CZQ4AFO" => "WINDOVER FARMS OF MELBOURNE PUD PHASE 03 UNIT 01",
  "157V3CZQ5CGQ" => "WINDOVER FARMS OF MELBOURNE PUD",
  "157V3CZQ478Y" => "WINDOVER FARMS OF MELBOURNE P.U.D. PHASE FIVE UN",
  "157V3CZQ448U" => "WINDJAMMER TOWNHOUSES CONDO PH III",
  "157V3CZQ4162" => "WINDJAMMER TOWNHOUSES CONDO PH II",
  "157V3CZQ3Y1D" => "WINDJAMMER TOWNHOUSES CONDO PH I",
  "157V3CZQ3VOT" => "WINDEMERE SHORES",
  "157V3CZQ3SFR" => "WINCOVE SUBDIVISION",
  "157V3CZQ9I3K" => "WOODFIELD AT HERITAGE OAKS PHASE 1",
  "157V3CZQ9F4A" => "WOODFIELD AT HERITAGE OAKS PH 3 A REPLAT OF A PORT",
  "157V3CZQ9BXC" => "WOODBRIDGE TOWNHOUSES",
  "157V3CZQ996R" => "WOODBRIDGE AT SUNTREE UNIT 2 SUNTREE P.U.D STAGE 8",
  "157V3CZQ9615" => "WOODBRIDGE AT SUNTREE UNIT 111 SUNTREE P.U.D. STAG",
  "157V3CZQ92WO" => "WOODBRIDGE AT SUNTREE UNIT 1 SUNTREE PUD 85 TRACT",
  "157V3CZQ8ZVT" => "WOOD LAKE ESTATES",
  "157V3CZQ8VXV" => "WOOD HAVEN MANOR SUBD",
  "157V3CZQ8S8I" => "WM GIBSONS SUBD OF LOT 29 OF JOYNERVILLE",
  "157V3CZQ8OL8" => "WISCONSIN-FLA FRUIT LAND CO SUBD OF LOTS 2 3 P F F",
  "157V3CZQ8LGD" => "WINSLOW RESERVE SUBD",
  "157V3CZQ8IKP" => "WINSLOW BEACH CONDO",
  "157V3CZQ8FHB" => "WINGATE RESERVE",
  "157V3CZQ8CG2" => "WINGATE ESTATES PHASE TWO VIERA NORTH P.U.D. PARC",
  "157V3CZQ89X7" => "WINGATE ESTATES PHASE THREE VIERA NORTH P.U.D. PA",
  "157V3CZQ86K2" => "WINGATE ESTATES PHASE SIX VIERA NORTH P.U.D. - PAR",
  "157V3CZQ83QI" => "WINGATE ESTATES PHASE ONE VIERA NORTH PUD-PARCELS",
  "157V3CZQ817D" => "WINGATE ESTATES PHASE FOUR VIERA NORTH P.U.D.-PARC",
  "157V3CZQ7Y0I" => "WINGATE ESTATES PHASE FIVE VIERA NORTH P.U.D. PARC",
  "157V3CZQ7UF7" => "WINDWARD PASSAGE CONDO",
  "157V3CZQ7RVJ" => "WINDWARD EAST PROFESSIONAL BLDG CONDO",
  "157V3CZQ7ONS" => "WINDWARD EAST CONDO PH II",
  "157V3CZQ7LGO" => "WINDWARD EAST CONDO PH I",
  "157V3CZQ7G02" => "WINDWARD COVE AT OCEAN SIDE VILLAGE",
  "157V3CZQ7J3P" => "WINDWARD COVE",
  "157V3CZQ7CUB" => "WINDWARD APTS CONDO",
  "157V3CZQ76E7" => "WINDSOR ESTATES PHASE TWO B",
  "157V3CZQ79TG" => "WINDSOR ESTATES PHASE TWO",
  "157V3CZQ72T8" => "WINDSOR ESTATES PHASE THREE",
  "157V3CZQ6ZDQ" => "WINDSOR ESTATES PHASE ONE",
  "157V3CZQ6VNP" => "WINDSONG OF BREVARD CONDO",
  "157V3CZQ6SQF" => "WINDSONG CONDO",
  "157V3CZQ6MHY" => "WINDRUSH CONDO PH II",
  "157V3CZQCRR6" => "YACHT HAVEN SEC 1",
  "157V3CZQCOJ0" => "XANADU CONDO",
  "157V3CZQCL2D" => "WYNDHAM LAKE ESTATES PHASE 2",
  "157V3CZQCHQX" => "WYNDHAM LAKE ESTATES A REPLAT OF LOTS 10 THRU 14",
  "157V3CZQCE52" => "WYNDHAM AT DURAN",
  "157V3CZQCAME" => "WROBEL ESTATES",
  "157V3CZQC7IU" => "WORLEYS SUBD",
  "157V3CZQC02T" => "WOODSMERE SEC 3 REPLAT OF LOTS 10 11 AND 12",
  "157V3CZQC445" => "WOODSMERE SEC 3",
  "157V3CZQBX3W" => "WOODSMERE SEC 2 BLOCK 8",
  "157V3CZQBTRT" => "WOODSMERE SEC 1",
  "157V3CZQBQD6" => "WOODSIDE SIX CONDO",
  "157V3CZQBNJO" => "WOODSIDE AT PORT MALABAR CONDO",
  "157V3CZQBKOE" => "WOODSHIRE PRESERVE PHASE I",
  "157V3CZQBHES" => "WOODSHIRE PRESERVE PHASE 2",
  "157V3CZQBE24" => "WOODS UNIT 2 THE",
  "157V3CZQB9NL" => "WOODS UNIT 1 THE",
  "157V3CZQB6LO" => "WOODS LAKE SUBDIVISION",
  "157V3CZQB3JP" => "WOODS AT IROQUOIS",
  "157V3CZQB0GK" => "WOODS AT CROOKED MILE THE",
  "157V3CZQAUP1" => "WOODLAWN PARK RESUBD OF",
  "157V3CZQAXS4" => "WOODLAWN PARK REPLAT",
  "157V3CZQARGH" => "WOODLAWN FARMS",
  "157V3CZQALDZ" => "WOODLANDS THE",
  "157V3CZQAODB" => "WOODLANDS OF LAKE WASHINGTON THE",
  "157V3CZQAIIF" => "WOODLAND TERRACE CONDO",
  "157V3CZQAFIX" => "WOODLAND TERRACE APTS CONDO",
  "157V3CZQACD0" => "WOODLAND OAKS",
  "157V3CZQA9N4" => "WOODLAND MANOR",
  "157V3CZQA6XB" => "WOODLAND HILLS",
  "157V3CZQA1TR" => "WOODLAND ESTATES SEC II",
  "157V3CZQA4GO" => "WOODLAND ESTATES",
  "157V3CZQ9YRU" => "WOODLAND CONDO",
  "157V3CZQ9TXM" => "WOODLAND AVENUE CONDOMINIUM",
  "157V3CZQ9RCE" => "WOODLAKE SOUTHWEST PARK CONDO",
  "157V3CZQ9O06" => "WOODFIELD PATIO HOMES AS PER ORB 2005 PG 815",
  "157V3CZQ9KR7" => "WOODFIELD AT HERITAGE OAKS PHASE 2",
  "157V3CZQD394" => "YOUNG AND METZNER SUBD PART OF",
  "157V3CZQD6MK" => "YOUNG AND METZNER SUBD",
  "157V3CZQCYGW" => "YOUNG AND METZNER REALTY CO RESUBD"
},
waterfront: {
  "1536QBU32LBK" => "No",
  "1536QBU32GXE" => "Yes"
},
zip_code: {
  "1536QBU3KARH" => "34985",
  "153Y1R4DLIGN" => "32132",
  "153Y1R4DLGHH" => "32130",
  "1536QBU4MWXS" => "32129",
  "1536QBU4MUC9" => "32128",
  "1536QBU4MRXY" => "32127",
  "1536QBU3PY1L" => "32125",
  "153Y1R4DL18U" => "32124",
  "153Y1R4DKT8U" => "32119",
  "153Y1R4DKOJK" => "32118",
  "1536QBU3PVBN" => "32117",
  "153Y1R4DKK4N" => "32114",
  "1536QBU3ZODR" => "32097",
  "1536QBU3YB7W" => "32096",
  "1536QBU46KM6" => "32095",
  "1536QBU41SJF" => "32094",
  "1536QBU46I9O" => "32092",
  "1536QBU47SNR" => "32091",
  "1536QBU439U4" => "32087",
  "1536QBU46FU1" => "32086",
  "1536QBU46DIH" => "32085",
  "1536QBU46AOZ" => "32084",
  "1536QBU4M7YE" => "32082",
  "1536QBU4M5OD" => "32081",
  "1536QBU4689A" => "32080",
  "1536QBU4K783" => "32079",
  "1536QBU4GKJW" => "32073",
  "1536QBU4F3IT" => "32071",
  "1536QBU4UUZB" => "32068",
  "1536QBU4TUSL" => "32066",
  "1536QBU4GHXP" => "32065",
  "1536QBU4SHEU" => "32064",
  "1536QBU4SYAN" => "32063",
  "1536QBU4U5XH" => "32062",
  "1536QBU4STGH" => "32061",
  "1536QBU4SEY6" => "32060",
  "1536QBU4RR0N" => "32059",
  "1536QBU4RITK" => "32058",
  "1536QBU4P110" => "32055",
  "1536QBU4OSR2" => "32054",
  "1536QBU3Q84O" => "32053",
  "1536QBU3Q5FF" => "32052",
  "1AQX2NV4N7OE" => "32040",
  "1536QBU4OY82" => "32025",
  "1536QBU4OVM2" => "32024",
  "1536QBU3O3O4" => "32008",
  "1536QBU4GF32" => "32003",
  "1536QBU3T0ZH" => "30335",
  "17UH7JQHKCCE" => "28791",
  "15A2V41ZHGI9" => "28786",
  "1536QBU49E4O" => "32318",
  "1536QBU49LPY" => "32317",
  "1536QBU49BJ5" => "32316",
  "1536QBU498U2" => "32315",
  "1536QBU49J4A" => "32314",
  "1536QBU496I2" => "32313",
  "1536QBU493NO" => "32312",
  "1536QBU4917W" => "32311",
  "1536QBU48Y9N" => "32310",
  "1536QBU48VEY" => "32309",
  "1536QBU48SZ1" => "32308",
  "1536QBU48Q21" => "32307",
  "1536QBU48NIX" => "32306",
  "1536QBU48KPE" => "32305",
  "1536QBU48I8Q" => "32304",
  "1536QBU49GQT" => "32303",
  "1536QBU48FLA" => "32302",
  "1536QBU48D87" => "32301",
  "1536QBU4E4TC" => "32266",
  "1536QBU48AMM" => "32259",
  "1536QBU4U3F6" => "32233",
  "1536QBU4U13T" => "32228",
  "1536QBU4TXQ4" => "32227",
  "1536QBU41NF1" => "32195",
  "1536QBU41Q2S" => "32193",
  "1536QBU457U0" => "32190",
  "1536QBU4C195" => "32189",
  "1536QBU437B5" => "32187",
  "1536QBU4NLBN" => "32185",
  "1536QBU4GN6W" => "32182",
  "1536QBU4M33X" => "32181",
  "1536QBU4NTI7" => "32180",
  "1536QBU4FZ32" => "32179",
  "1536QBU4J3F4" => "32177",
  "153Y1R4DLT9E" => "32176",
  "153Y1R4DLR04" => "32174",
  "1536QBU3RBOF" => "32170",
  "1536QBU3R91J" => "32169",
  "1536QBU3R6G2" => "32168",
  "1536QBU4JBHV" => "32164",
  "1536QBU4AGNF" => "32163",
  "1536QBU4AE9E" => "32162",
  "1536QBU4ABKE" => "32159",
  "1536QBU4P3KY" => "32157",
  "1536QBU4J93P" => "32143",
  "153Y1R4DLK6A" => "32141",
  "182FZRRZEH1D" => "32139",
  "1536QBU4J6FZ" => "32137",
  "1536QBU42VYD" => "32134",
  "1536QBU420SX" => "32462",
  "1536QBU42QMK" => "32461",
  "1536QBU45FDD" => "32460",
  "1536QBU3SOCY" => "32459",
  "1536QBU3STCV" => "32457",
  "1536QBU3SQT4" => "32456",
  "1536QBU3SYDC" => "32455",
  "1536QBU3REGB" => "32452",
  "1536QBU3Y691" => "32449",
  "1536QBU4TFXD" => "32448",
  "1536QBU4TDL6" => "32446",
  "1536QBU4T60M" => "32445",
  "1536QBU4SVYB" => "32444",
  "1536QBU3PF6G" => "32443",
  "1536QBU3P9P9" => "32439",
  "1536QBU3P4K0" => "32437",
  "1536QBU3PNN5" => "32435",
  "1536QBU3PKMJ" => "32434",
  "1536QBU3PHQA" => "32433",
  "1536QBU3ORFU" => "32428",
  "1536QBU3OORF" => "32427",
  "1536QBU3O6CT" => "32426",
  "1536QBU3OM3H" => "32425",
  "1536QBU41Y3W" => "32413",
  "1536QBU45O8K" => "32409",
  "1536QBU4K4MY" => "32408",
  "1536QBU4K26D" => "32407",
  "1536QBU4JZKX" => "32405",
  "1536QBU4JX7Z" => "32404",
  "1536QBU4JUH5" => "32403",
  "1536QBU4JML0" => "32401",
  "1536QBU49R9Q" => "32399",
  "1536QBU49OLK" => "32395",
  "1536QBU3ZZ10" => "32362",
  "1536QBU47VC5" => "32359",
  "1536QBU45I3V" => "32358",
  "1536QBU46Z9W" => "32355",
  "1536QBU428KW" => "32353",
  "1536QBU426A7" => "32352",
  "1536QBU423L6" => "32351",
  "1536QBU4LNOU" => "32350",
  "1536QBU4L76F" => "32348",
  "1536QBU4L4H9" => "32347",
  "1536QBU4JRV8" => "32346",
  "1536QBU4VBH5" => "32344",
  "1536QBU4UXS1" => "32343",
  "1536QBU4T11R" => "32340",
  "1536QBU4SJRL" => "32337",
  "1536QBU4QUZ1" => "32336",
  "153Y1R4DM3HZ" => "32712",
  "153Y1R4DM1HI" => "32709",
  "1536QBU40232" => "32708",
  "183BIWTLJOCV" => "32707",
  "1536QBU4AVU7" => "32706",
  "153Y1R4DLXZM" => "32703",
  "15BX5V6VL64E" => "32701",
  "1536QBU3ZWEE" => "32697",
  "1536QBU40UTM" => "32696",
  "1536QBU41DCM" => "32694",
  "1536QBU4CMTR" => "32693",
  "1536QBU42B6V" => "32686",
  "1536QBU4ONFL" => "32685",
  "1536QBU4G752" => "32680",
  "1536QBU4EF4V" => "32669",
  "1536QBU4VGDW" => "32668",
  "1536QBU4USHL" => "32667",
  "1536QBU4UKWW" => "32666",
  "1536QBU4TKX5" => "32658",
  "1536QBU4O770" => "32656",
  "1536QBU3MI6R" => "32619",
  "1536QBU4V35R" => "32583",
  "1536QBU45D1Y" => "32579",
  "1536QBU4EHS2" => "32578",
  "1536QBU4V8S6" => "32577",
  "1536QBU4IXWG" => "32571",
  "1536QBU4V0BW" => "32570",
  "1536QBU4TIH8" => "32569",
  "1536QBU4U8C0" => "32568",
  "1536QBU4RGC8" => "32567",
  "1536QBU4E1YA" => "32566",
  "1536QBU4L1QZ" => "32561",
  "1536QBU3QYUI" => "32550",
  "1536QBU3S67G" => "32538",
  "1536QBU4KZAW" => "32534",
  "1536QBU4KWJF" => "32526",
  "1536QBU4KU12" => "32514",
  "1536QBU4KRDH" => "32508",
  "1536QBU4KOTB" => "32507",
  "1536QBU4KMFZ" => "32506",
  "1536QBU4KJU7" => "32505",
  "1536QBU4KHB5" => "32504",
  "1536QBU4KEQY" => "32503",
  "1536QBU4KC5P" => "32502",
  "1536QBU4K9M4" => "32501",
  "1536QBU3ZR1I" => "32466",
  "1536QBU3Y8JY" => "32465",
  "1536QBU3XY00" => "32464",
  "1536QBU3Y11J" => "32463",
  "1536QBU4HIU8" => "32815",
  "153Y1R4DNNDZ" => "32814",
  "1536QBU4HGBB" => "32812",
  "1536QBU4HDVP" => "32811",
  "1536QBU4HB7U" => "32810",
  "1536QBU4H8L1" => "32809",
  "1536QBU4H659" => "32808",
  "1536QBU4H3AS" => "32807",
  "1536QBU4H0OY" => "32806",
  "1536QBU4GXJC" => "32805",
  "1536QBU4GUX8" => "32804",
  "1536QBU4GSHI" => "32803",
  "1536QBU4GPU2" => "32801",
  "1536QBU3ZLVF" => "32798",
  "1536QBU4ATKT" => "32796",
  "1536QBU40FCK" => "32793",
  "1536QBU40CD7" => "32792",
  "1536QBU409ZW" => "32790",
  "1536QBU407F6" => "32789",
  "1536QBU4B169" => "32784",
  "1536QBU4AQZ9" => "32783",
  "1536QBU4AOHU" => "32781",
  "1536QBU4ALVS" => "32780",
  "1536QBU4SOMQ" => "32779",
  "1536QBU4A6HP" => "32778",
  "1536QBU49TVH" => "32777",
  "1ALWB9LF8P5P" => "32776",
  "1536QBU43HJP" => "32773",
  "1536QBU43EVW" => "32772",
  "1536QBU43CII" => "32771",
  "1536QBU4J0XV" => "32767",
  "1536QBU3OU0O" => "32766",
  "1536QBU4IPGJ" => "32765",
  "1536QBU4IMLL" => "32764",
  "153Y1R4DMQB5" => "32763",
  "153Y1R4DMNU5" => "32759",
  "1536QBU4VIRM" => "32757",
  "153Y1R4DMLSH" => "32754",
  "1536QBU4T3ND" => "32751",
  "1536QBU4SMB3" => "32750",
  "1536QBU4PH4K" => "32747",
  "1536QBU4PEDA" => "32746",
  "1536QBU4PBSL" => "32744",
  "153Y1R4DMG5U" => "32738",
  "153Y1R4DMCMO" => "32725",
  "153Y1R4DMAG3" => "32724",
  "153Y1R4DM82I" => "32720",
  "1536QBU404XK" => "32719",
  "153Y1R4DM5VJ" => "32713",
  "1536QBU4UPY4" => "32953",
  "1536QBU4UNKG" => "32952",
  "1536QBU4UI9H" => "32951",
  "1536QBU4B6JW" => "32950",
  "153Y1R4DQEUE" => "32949",
  "1536QBU3A5RI" => "32948",
  "1536QBU4BVY5" => "32940",
  "1536QBU4BYF0" => "32937",
  "1536QBU488B0" => "32935",
  "1536QBU4UFRB" => "32934",
  "153Y1R4DPZ61" => "32931",
  "153Y1R4DPVVU" => "32927",
  "153Y1R4DPSU1" => "32926",
  "153Y1R4DPPC0" => "32925",
  "153Y1R4DPNCE" => "32922",
  "153Y1R4DPJZ7" => "32920",
  "1536QBU41V5I" => "32912",
  "1536QBU3RYFF" => "32911",
  "1536QBU3RW1L" => "32910",
  "1536QBU3RTI0" => "32909",
  "1536QBU3RR4A" => "32908",
  "1536QBU3ROK2" => "32907",
  "1536QBU3RM63" => "32906",
  "1536QBU3RJI1" => "32905",
  "1536QBU3QR8E" => "32904",
  "1536QBU4UD0I" => "32903",
  "1536QBU4UAUC" => "32901",
  "1536QBU4L9UO" => "32839",
  "153Y1R4DOW2G" => "32837",
  "153Y1R4DOSDC" => "32836",
  "153Y1R4DOQE7" => "32835",
  "153Y1R4DON0D" => "32834",
  "153Y1R4DOJLD" => "32833",
  "1536QBU4IJYE" => "32832",
  "1536QBU4IHAL" => "32831",
  "1536QBU4IEOU" => "32830",
  "1536QBU4IBZP" => "32829",
  "1536QBU4I918" => "32828",
  "1536QBU4I6GQ" => "32827",
  "1536QBU4I3XF" => "32826",
  "1536QBU4I19O" => "32825",
  "1536QBU4HYC8" => "32824",
  "1536QBU4HVXO" => "32823",
  "1536QBU4NQNR" => "32822",
  "1536QBU4HTA8" => "32821",
  "1536QBU4HQKP" => "32820",
  "1536QBU4HO6S" => "32819",
  "1536QBU4HLKH" => "32818",
  "1536QBU4B3ZW" => "32817",
  "1536QBU3B244" => "33055",
  "1536QBU3AZBI" => "33054",
  "1536QBU4O1JE" => "33051",
  "1536QBU4T8QJ" => "33050",
  "1536QBU4EKZ8" => "33043",
  "1536QBU4809U" => "33042",
  "1536QBU47XOE" => "33041",
  "1536QBU3AWAU" => "33040",
  "1536QBU3LS5L" => "33039",
  "1536QBU4O4NJ" => "33037",
  "1536QBU3LPL9" => "33035",
  "1536QBU36NXG" => "33034",
  "1536QBU3EQ6R" => "33033",
  "1536QBU3AQZ2" => "33032",
  "1536QBU3ATO5" => "33031",
  "1536QBU36L2W" => "33030",
  "1536QBU3ESNQ" => "33029",
  "1536QBU3I2NT" => "33028",
  "1536QBU3EV6L" => "33027",
  "1536QBU3I0BI" => "33026",
  "1536QBU3F8PL" => "33025",
  "1536QBU3HXU4" => "33024",
  "1536QBU3AOHD" => "33023",
  "1536QBU3ALSH" => "33021",
  "1536QBU36IIO" => "33020",
  "1536QBU3LMTP" => "33019",
  "1536QBU3EY1K" => "33018",
  "1536QBU3JZUU" => "33017",
  "1536QBU36FS0" => "33016",
  "1536QBU3JX0N" => "33015",
  "1536QBU3JUGU" => "33014",
  "1AQX2NWLRPGI" => "33012",
  "1536QBU36DEC" => "33010",
  "1536QBU36AVM" => "33009",
  "1536QBU368IB" => "33004",
  "1536QBU4RLRK" => "33001",
  "1536QBU3AJCA" => "32976",
  "1536QBU3AGOZ" => "32971",
  "1536QBU3AEC1" => "32970",
  "1536QBU3J7FO" => "32968",
  "1536QBU365W8" => "32967",
  "1536QBU3J9ZQ" => "32966",
  "1536QBU363IO" => "32963",
  "1536QBU3JCTR" => "32962",
  "1536QBU3JFGA" => "32960",
  "1536QBU3ABL9" => "32958",
  "1536QBU3A8GL" => "32957",
  "1536QBU42O7K" => "32956",
  "1536QBU42LJS" => "32955",
  "1536QBU37DL9" => "33150",
  "1536QBU37B56" => "33149",
  "1536QBU3H7W7" => "33147",
  "1536QBU3DW7D" => "33146",
  "1536QBU3H59Z" => "33145",
  "1536QBU3H2XF" => "33144",
  "1536QBU3H074" => "33143",
  "1536QBU3GXW3" => "33142",
  "1536QBU3BNPQ" => "33141",
  "1536QBU3GV8R" => "33140",
  "1536QBU378JK" => "33139",
  "1536QBU3F0JO" => "33138",
  "1536QBU3GSL3" => "33137",
  "1536QBU3GQ62" => "33136",
  "1536QBU3GNKO" => "33135",
  "1536QBU375YV" => "33134",
  "1536QBU373BO" => "33133",
  "1536QBU3GL04" => "33132",
  "1536QBU3GI2T" => "33131",
  "1536QBU3GF41" => "33130",
  "1536QBU3GCHS" => "33129",
  "1536QBU3G9Z6" => "33128",
  "1536QBU3G7AK" => "33127",
  "1536QBU3G4HX" => "33126",
  "1536QBU3G24A" => "33125",
  "1536QBU3BKOX" => "33124",
  "1536QBU3KWLH" => "33122",
  "1536QBU370YD" => "33109",
  "1536QBU3Z1G6" => "33097",
  "1536QBU3YYS2" => "33093",
  "1536QBU3R40L" => "33092",
  "1536QBU3YW80" => "33077",
  "1536QBU3BCQI" => "33076",
  "1536QBU3YTHM" => "33075",
  "1536QBU3BA2J" => "33073",
  "1536QBU3YQRC" => "33072",
  "1536QBU3IQ1N" => "33071",
  "1536QBU4A936" => "33070",
  "1536QBU3INL9" => "33069",
  "1536QBU3BFAW" => "33068",
  "1536QBU3B7IK" => "33067",
  "1536QBU3YNUF" => "33066",
  "1536QBU36YHU" => "33065",
  "1536QBU3B4PA" => "33064",
  "1536QBU36W3W" => "33063",
  "1536QBU36T5G" => "33062",
  "1536QBU3U3WB" => "33061",
  "1536QBU3IL1S" => "33060",
  "1536QBU36QL9" => "33056",
  "1536QBU3E1Q6" => "33311",
  "1536QBU3BI4Q" => "33309",
  "1536QBU383NK" => "33308",
  "1536QBU3E54J" => "33306",
  "1536QBU3BYFF" => "33305",
  "1536QBU3EATG" => "33304",
  "1536QBU380GM" => "33301",
  "1536QBU3EDDQ" => "33261",
  "1536QBU3JRJN" => "33257",
  "1536QBU3KDJV" => "33256",
  "1536QBU3O8YF" => "33239",
  "1536QBU3SVW0" => "33197",
  "1536QBU3FZHV" => "33196",
  "1536QBU3FX3G" => "33194",
  "1536QBU3FUJ5" => "33193",
  "1536QBU3FRSD" => "33190",
  "1536QBU37XFH" => "33189",
  "1536QBU3FOZ8" => "33187",
  "1536QBU3FMAE" => "33186",
  "1536QBU3FJ9G" => "33185",
  "1536QBU3FGWE" => "33184",
  "1536QBU3FE4J" => "33183",
  "1536QBU3FBJN" => "33182",
  "1536QBU3EL5E" => "33181",
  "1536QBU35TG5" => "33180",
  "1536QBU3EG3U" => "33179",
  "1536QBU3HSRY" => "33178",
  "1536QBU3HQ37" => "33177",
  "1536QBU3BT4V" => "33176",
  "1536QBU3HNJ6" => "33175",
  "1536QBU3HKZ7" => "33174",
  "1536QBU3HIJZ" => "33173",
  "1536QBU3HFZ1" => "33172",
  "1536QBU37UW5" => "33170",
  "1536QBU37SNI" => "33169",
  "1536QBU3F5Z5" => "33168",
  "1536QBU3ENIY" => "33167",
  "1536QBU37NEE" => "33166",
  "1536QBU3HDNM" => "33165",
  "1536QBU3EIIY" => "33164",
  "1536QBU3BVSJ" => "33162",
  "1536QBU37PSX" => "33161",
  "1536QBU37KXD" => "33160",
  "1536QBU3BQFC" => "33158",
  "1536QBU37IIY" => "33157",
  "1536QBU37FYL" => "33156",
  "1536QBU3HAXM" => "33155",
  "1536QBU35X10" => "33154",
  "1536QBU3F3F3" => "33153",
  "1536QBU3N6C8" => "33433",
  "1536QBU38U23" => "33432",
  "1536QBU3N3IK" => "33431",
  "1536QBU38RFI" => "33430",
  "1536QBU3N0YS" => "33429",
  "1536QBU3MY62" => "33428",
  "1536QBU3MVKR" => "33427",
  "1536QBU3PQDZ" => "33422",
  "1536QBU3TE50" => "33421",
  "1536QBU3HVI0" => "33418",
  "1536QBU38OVE" => "33417",
  "1536QBU3ISLH" => "33415",
  "1536QBU3CPVI" => "33414",
  "1536QBU3IUYS" => "33413",
  "1536QBU3CN97" => "33412",
  "1536QBU3CKG1" => "33411",
  "1536QBU3E7KT" => "33410",
  "1536QBU3IXJC" => "33409",
  "1536QBU38M58" => "33408",
  "1536QBU3CHWA" => "33407",
  "1536QBU38J06" => "33406",
  "1536QBU3IZR5" => "33405",
  "1536QBU3CC7Q" => "33404",
  "1536QBU38GIC" => "33403",
  "1536QBU3C9CI" => "33401",
  "1536QBU3L42T" => "33394",
  "1536QBU3IIPK" => "33388",
  "1536QBU3JHU2" => "33351",
  "1536QBU3JMXI" => "33345",
  "1536QBU3DZ1U" => "33334",
  "1536QBU3C6MC" => "33332",
  "1536QBU3C43B" => "33331",
  "1536QBU3C14N" => "33330",
  "1536QBU38DOH" => "33328",
  "1536QBU3KG9C" => "33327",
  "1536QBU3JPAO" => "33326",
  "1536QBU3IFD1" => "33325",
  "1536QBU3ICXA" => "33324",
  "1536QBU3IAB1" => "33323",
  "1536QBU3I7TE" => "33322",
  "1536QBU3JKFS" => "33321",
  "1536QBU38B39" => "33319",
  "1536QBU3SLPY" => "33318",
  "1536QBU3I58V" => "33317",
  "1536QBU3L1R0" => "33316",
  "1536QBU3KZ33" => "33315",
  "1536QBU388LN" => "33314",
  "1536QBU3868K" => "33313",
  "1536QBU3KJ3K" => "33312",
  "1536QBU3Z6GP" => "33540",
  "1536QBU4PJNH" => "33538",
  "1536QBU4OKNM" => "33537",
  "1536QBU3PCCJ" => "33534",
  "1536QBU3P24E" => "33527",
  "1536QBU3OWK8" => "33524",
  "1536QBU42J8C" => "33523",
  "1536QBU3YIS7" => "33511",
  "1536QBU3O10G" => "33510",
  "1536QBU3NYKO" => "33509",
  "1536QBU3NVOC" => "33508",
  "1536QBU3NT47" => "33499",
  "1536QBU3NQH7" => "33498",
  "1536QBU3NNST" => "33497",
  "1536QBU3NLJE" => "33496",
  "1536QBU3D2X6" => "33493",
  "1536QBU482N2" => "33491",
  "1536QBU3NIZL" => "33488",
  "1536QBU39MHG" => "33487",
  "1536QBU3NGKJ" => "33486",
  "1536QBU3KTPK" => "33484",
  "1536QBU39JNF" => "33483",
  "1536QBU3NDYW" => "33481",
  "1536QBU3D0C2" => "33480",
  "1536QBU3LXON" => "33478",
  "1536QBU3LV31" => "33477",
  "1536QBU3CXLO" => "33476",
  "1536QBU4VDU1" => "33471",
  "1536QBU3CV3T" => "33470",
  "1536QBU39HEO" => "33469",
  "1536QBU3J2C4" => "33467",
  "1536QBU3NBJU" => "33464",
  "1536QBU39EI0" => "33463",
  "1536QBU35R00" => "33462",
  "1536QBU3CF36" => "33461",
  "1536QBU39BTW" => "33460",
  "1536QBU4P8W7" => "33459",
  "1536QBU399IZ" => "33458",
  "1536QBU396WX" => "33455",
  "1536QBU3J4M6" => "33449",
  "1536QBU3KR2B" => "33446",
  "1536QBU3KOB7" => "33445",
  "1536QBU394EC" => "33444",
  "1536QBU3KLOS" => "33442",
  "1536QBU391LH" => "33441",
  "1536QBU38Z16" => "33438",
  "1536QBU3CSJC" => "33436",
  "1536QBU38WES" => "33435",
  "1536QBU3N8T4" => "33434",
  "1536QBU3UWFW" => "33612",
  "1536QBU3UTMF" => "33611",
  "1536QBU3UR1P" => "33610",
  "1536QBU3UO76" => "33609",
  "1536QBU3ULLY" => "33608",
  "1536QBU3UIUS" => "33607",
  "1536QBU3UGFU" => "33606",
  "1536QBU3UDVT" => "33605",
  "1536QBU3UBO9" => "33604",
  "1536QBU3U8TD" => "33603",
  "1536QBU3U6EN" => "33602",
  "1536QBU3Z3Z3" => "33601",
  "1536QBU3YDM5" => "33598",
  "1536QBU41L2J" => "33597",
  "1536QBU3XVGP" => "33596",
  "1536QBU3XSRC" => "33595",
  "1536QBU3XPRF" => "33594",
  "1536QBU3XN7O" => "33592",
  "1536QBU3U21D" => "33586",
  "1536QBU485OQ" => "33585",
  "1536QBU3TZPL" => "33584",
  "1536QBU3TX2G" => "33583",
  "1536QBU3TBJG" => "33579",
  "1536QBU3T8Q8" => "33578",
  "1536QBU42YR5" => "33576",
  "1536QBU3TR7P" => "33575",
  "1536QBU3TO5P" => "33573",
  "1536QBU3TLSL" => "33572",
  "1536QBU3TJ6S" => "33571",
  "1536QBU3TGU5" => "33570",
  "1536QBU3T65H" => "33569",
  "1536QBU3T3GN" => "33568",
  "1536QBU3SJBJ" => "33567",
  "1536QBU3SGKE" => "33566",
  "1536QBU3SDV3" => "33565",
  "1536QBU3SB8D" => "33564",
  "1536QBU3S8LJ" => "33563",
  "1536QBU3QLRL" => "33559",
  "1536QBU3YLAO" => "33558",
  "1536QBU4G49E" => "33556",
  "1536QBU3QOLV" => "33550",
  "1536QBU3QJ8Y" => "33549",
  "1536QBU3QGGD" => "33548",
  "1536QBU3QASD" => "33547",
  "1536QBU3ZITU" => "33545",
  "1536QBU3ZGG9" => "33544",
  "1536QBU3ZDWU" => "33543",
  "1536QBU3ZBGM" => "33542",
  "1536QBU3Z8VL" => "33541",
  "1536QBU4C934" => "33710",
  "1536QBU4C6DH" => "33709",
  "1536QBU42DM1" => "33708",
  "1536QBU45KMO" => "33707",
  "1536QBU472E0" => "33706",
  "1536QBU4C3OA" => "33705",
  "1536QBU47F8N" => "33704",
  "1536QBU47CN9" => "33703",
  "1536QBU47AG8" => "33702",
  "1536QBU477QI" => "33701",
  "1536QBU3OJ5V" => "33688",
  "1536QBU3XKGV" => "33681",
  "1536QBU3XHZM" => "33680",
  "1536QBU3XFD6" => "33679",
  "1536QBU3XCM7" => "33677",
  "1536QBU3XA39" => "33675",
  "1536QBU3X7A6" => "33674",
  "1536QBU3X4FL" => "33673",
  "1536QBU3X222" => "33672",
  "1536QBU3WZJY" => "33664",
  "1536QBU3WX4K" => "33663",
  "1536QBU3WUIA" => "33662",
  "1536QBU3WRPZ" => "33661",
  "1536QBU3WPA4" => "33660",
  "1536QBU3WMEX" => "33655",
  "1536QBU3WJOF" => "33650",
  "1536QBU3WH24" => "33647",
  "1536QBU3WEMU" => "33646",
  "1536QBU3WBUB" => "33637",
  "1536QBU3W8TI" => "33635",
  "1536QBU3W6CH" => "33634",
  "1536QBU3W3M5" => "33633",
  "1536QBU3W0QW" => "33631",
  "1536QBU3VXN4" => "33630",
  "1536QBU3VV20" => "33629",
  "1536QBU3VSCY" => "33626",
  "1536QBU3OGMU" => "33625",
  "1536QBU3VPR9" => "33624",
  "1536QBU3VNCP" => "33623",
  "1536QBU3VKS2" => "33622",
  "1536QBU3VI50" => "33621",
  "1536QBU3VF64" => "33620",
  "1536QBU3VC2W" => "33619",
  "1536QBU3OB9O" => "33618",
  "1536QBU3V9FX" => "33617",
  "1536QBU3V6TN" => "33616",
  "1536QBU3V4GU" => "33615",
  "1536QBU3V1UV" => "33614",
  "1536QBU3UZD1" => "33613",
  "1536QBU44J6D" => "33870",
  "1536QBU4M0GO" => "33868",
  "1536QBU4Q1Y5" => "33867",
  "1536QBU4GCIS" => "33865",
  "1536QBU4VLIO" => "33860",
  "1536QBU4PZNS" => "33859",
  "1536QBU4SR47" => "33857",
  "1536QBU4PX2F" => "33854",
  "1536QBU4PUHM" => "33853",
  "1536QBU4PMED" => "33852",
  "1536QBU4P6EK" => "33851",
  "1536QBU4OPZZ" => "33850",
  "1536QBU4NW45" => "33849",
  "1536QBU3PSW4" => "33846",
  "1536QBU4TNGY" => "33830",
  "1536QBU4QSDT" => "33815",
  "1536QBU4QPF3" => "33813",
  "1536QBU4QN0J" => "33812",
  "1536QBU4QKAL" => "33811",
  "1536QBU4QHMC" => "33810",
  "1536QBU4QEPU" => "33809",
  "1536QBU4QC3T" => "33805",
  "1536QBU4Q9PB" => "33803",
  "1536QBU4Q741" => "33801",
  "1536QBU3MQ8C" => "33786",
  "1536QBU3MN8F" => "33785",
  "1536QBU4LI89" => "33782",
  "1536QBU4LFDX" => "33781",
  "1536QBU455D5" => "33778",
  "1536QBU452VF" => "33777",
  "1536QBU45088" => "33776",
  "1536QBU44XUQ" => "33775",
  "1536QBU4RAQA" => "33774",
  "1536QBU4R81H" => "33773",
  "1536QBU44V9F" => "33772",
  "1536QBU4R5GV" => "33771",
  "1536QBU3MSV8" => "33770",
  "1536QBU3MKM8" => "33756",
  "1536QBU3MFD9" => "33744",
  "1536QBU4AYP0" => "33740",
  "1536QBU474YL" => "33736",
  "1536QBU47Q3I" => "33730",
  "1536QBU47N9P" => "33717",
  "1536QBU47KN1" => "33716",
  "1536QBU47HRX" => "33715",
  "1536QBU4CK57" => "33714",
  "1536QBU4CHDO" => "33713",
  "1536QBU4CEZT" => "33712",
  "1536QBU4CC2O" => "33711",
  "1536QBU4CS89" => "34102",
  "1536QBU4TSH8" => "33993",
  "1536QBU4MPFB" => "33983",
  "1536QBU4NIX9" => "33982",
  "1536QBU4MMXS" => "33981",
  "1536QBU4MKIP" => "33980",
  "1536QBU4SCL6" => "33976",
  "1536QBU4SA1B" => "33974",
  "1536QBU4S765" => "33973",
  "1536QBU4S4MA" => "33972",
  "1536QBU4S1T4" => "33971",
  "1536QBU434HF" => "33967",
  "1536QBU4BTDA" => "33960",
  "1536QBU43JYF" => "33957",
  "1536QBU3D5QP" => "33956",
  "1536QBU4NGAZ" => "33955",
  "1536QBU4MHZV" => "33954",
  "1536QBU4MFLZ" => "33953",
  "1536QBU4MCYX" => "33952",
  "1536QBU4NDQD" => "33951",
  "1536QBU4NAWV" => "33950",
  "1536QBU4MAKU" => "33948",
  "1536QBU4LSVJ" => "33947",
  "1536QBU4LQ9V" => "33946",
  "1536QBU4LKUR" => "33945",
  "1536QBU4JPE6" => "33944",
  "1536QBU4RZAQ" => "33936",
  "1536QBU4OHXT" => "33935",
  "1536QBU4EVGO" => "33917",
  "1536QBU4V5PS" => "33913",
  "1536QBU431AT" => "33912",
  "1536QBU4AJ80" => "33905",
  "1536QBU4ESN4" => "33903",
  "1536QBU4Q4K4" => "33898",
  "1ALX09380W30" => "33895",
  "1536QBU3YG8M" => "33890",
  "1536QBU40I3O" => "33888",
  "1536QBU41AYA" => "33885",
  "1536QBU3OZ5Y" => "33884",
  "1536QBU418FL" => "33883",
  "1536QBU4160E" => "33882",
  "1536QBU413A7" => "33881",
  "1536QBU410PX" => "33880",
  "1536QBU41IG2" => "33877",
  "1536QBU44SSD" => "33876",
  "1536QBU44QII" => "33875",
  "1536QBU41G2L" => "33873",
  "1536QBU44LLG" => "33872",
  "1536QBU44O39" => "33871",
  "1536QBU4B9AC" => "34284",
  "1536QBU4EQ0C" => "34275",
  "1536QBU4RDLU" => "34272",
  "1536QBU4PRR6" => "34269",
  "1536QBU4ENNB" => "34268",
  "1536QBU4POYW" => "34266",
  "1536QBU3RGZJ" => "34264",
  "1536QBU3R16D" => "34251",
  "1536QBU44GLK" => "34243",
  "1536QBU44EBU" => "34242",
  "1536QBU44BPW" => "34241",
  "1536QBU3MA2Y" => "34240",
  "1536QBU449D7" => "34239",
  "1536QBU446W1" => "34238",
  "1536QBU444F0" => "34237",
  "1536QBU441SR" => "34236",
  "1536QBU43Z9Z" => "34235",
  "1536QBU43WEN" => "34234",
  "1536QBU43TW1" => "34233",
  "1536QBU43RAH" => "34232",
  "1536QBU43OIM" => "34231",
  "1536QBU43M8O" => "34230",
  "1536QBU3QDET" => "34228",
  "1536QBU3P75D" => "34222",
  "1536QBU3S3J6" => "34221",
  "1536QBU3S158" => "34220",
  "1536QBU3Q32L" => "34218",
  "1536QBU3Q0I9" => "34217",
  "1536QBU3M7LJ" => "34212",
  "1536QBU3M4ZP" => "34211",
  "1536QBU3TTQT" => "34208",
  "1536QBU3Y3N7" => "34205",
  "1536QBU3M2MQ" => "34203",
  "1536QBU3M024" => "34202",
  "1536QBU4TB18" => "34145",
  "1536QBU39PDX" => "34141",
  "1536QBU4DYX5" => "34120",
  "1536QBU4DWA5" => "34119",
  "1536QBU4DLX3" => "34117",
  "1536QBU4DJ9T" => "34116",
  "1536QBU4DGUL" => "34114",
  "1536QBU4DE0Y" => "34113",
  "1536QBU4DAZ3" => "34112",
  "1536QBU4D8KK" => "34110",
  "1536QBU4D5JY" => "34109",
  "1536QBU4D2ZU" => "34108",
  "1536QBU4D06L" => "34105",
  "1536QBU4CXJP" => "34104",
  "1536QBU4CUVV" => "34103",
  "1536QBU4JJZC" => "34685",
  "1536QBU4JH6N" => "34684",
  "1536QBU4JEAV" => "34683",
  "1536QBU4G9P0" => "34677",
  "1536QBU4N5DB" => "34669",
  "1536QBU4N295" => "34668",
  "1536QBU4MZQU" => "34667",
  "1536QBU4IUTI" => "34660",
  "1536QBU4CPGJ" => "34655",
  "1536QBU4ECQR" => "34654",
  "1536QBU4EA3D" => "34653",
  "1536QBU4E7JX" => "34652",
  "1536QBU4R2R4" => "34639",
  "1536QBU4R03P" => "34638",
  "1536QBU4QXOR" => "34637",
  "1536QBU465ON" => "34613",
  "1536QBU46323" => "34611",
  "1536QBU45AIZ" => "34610",
  "1536QBU460JR" => "34609",
  "1536QBU45XXB" => "34608",
  "1536QBU45VMB" => "34607",
  "1536QBU45T8Y" => "34606",
  "1536QBU45QUP" => "34604",
  "1536QBU3ZTLF" => "34498",
  "1536QBU4IS7A" => "34484",
  "1536QBU4FW8S" => "34482",
  "1536QBU4FTS5" => "34481",
  "1536QBU4FR07" => "34480",
  "1536QBU4FOEG" => "34479",
  "1536QBU4FLMK" => "34476",
  "1536QBU4FJ6T" => "34475",
  "1536QBU4FGJ8" => "34474",
  "1536QBU4FDVT" => "34473",
  "1536QBU4FB4I" => "34472",
  "1536QBU4F8JI" => "34471",
  "1536QBU4F5ZI" => "34470",
  "1536QBU4LCBE" => "34465",
  "1536QBU4ROLQ" => "34461",
  "1ARYAAQLOQMA" => "34432",
  "1ARURHNWP9DV" => "34420",
  "1536QBU4BR0D" => "34293",
  "1536QBU4BOBJ" => "34292",
  "1536QBU4BM2D" => "34291",
  "1536QBU4BJHC" => "34290",
  "1536QBU4F0RQ" => "34289",
  "1536QBU4EXT5" => "34288",
  "1536QBU4BH00" => "34287",
  "1536QBU4BE6W" => "34286",
  "1536QBU4BBMK" => "34285",
  "1536QBU3K8AV" => "34984",
  "1536QBU3DAZ4" => "34983",
  "1536QBU3LK0S" => "34982",
  "1536QBU3LHFD" => "34981",
  "1536QBU3DIV4" => "34974",
  "1536QBU360ZI" => "34972",
  "1536QBU3A0KI" => "34957",
  "1536QBU39Y0B" => "34956",
  "1536QBU3D8BJ" => "34953",
  "1536QBU3K5AD" => "34952",
  "1536QBU39UCV" => "34951",
  "1536QBU39RSJ" => "34950",
  "1536QBU3LF3I" => "34949",
  "1536QBU3LC4Y" => "34947",
  "1536QBU3L96I" => "34946",
  "1536QBU3L6OK" => "34945",
  "1536QBU4NO57" => "34822",
  "1536QBU4RWIF" => "34788",
  "1536QBU40PNZ" => "34787",
  "1536QBU40SAS" => "34786",
  "1536QBU40XQE" => "34785",
  "1536QBU40NE6" => "34778",
  "1536QBU40KT0" => "34777",
  "1536QBU46WWQ" => "34773",
  "1536QBU46UC4" => "34772",
  "1536QBU46RZ1" => "34771",
  "1536QBU46PER" => "34770",
  "1536QBU46N4A" => "34769",
  "1536QBU4G1TP" => "34761",
  "1536QBU4LXWX" => "34759",
  "1536QBU4LV7B" => "34758",
  "1536QBU3QWC9" => "34755",
  "1536QBU4TPUE" => "34753",
  "1536QBU4RTL1" => "34748",
  "1536QBU42GGB" => "34747",
  "1536QBU4OFEU" => "34746",
  "1536QBU4OCM4" => "34744",
  "1536QBU3MCUE" => "34743",
  "1536QBU4OA0S" => "34741",
  "1536QBU4NYX3" => "34739",
  "153Y1R4DRBJ9" => "34734",
  "1536QBU3QTZZ" => "34715",
  "1AL38P5JP2VU" => "34714",
  "1AQSGJ46P2XG" => "34711",
  "1536QBU42TH0" => "34695",
  "1536QBU4A42D" => "34691",
  "1536QBU4A1GQ" => "34690",
  "1536QBU49YV6" => "34689",
  "1536QBU49W8Q" => "34688",
  "153YDMSNMUP8" => "99999",
  "1AQWQ1NQFSMA" => "37022",
  "1536QBU3DTML" => "34997",
  "1536QBU3DQU2" => "34996",
  "1536QBU3DOAW" => "34994",
  "1536QBU3K2GO" => "34992",
  "1536QBU3DLKK" => "34990",
  "1536QBU4N7Z9" => "34988",
  "1536QBU3DG23" => "34987",
  "1536QBU3DDEY" => "34986"
},
gated_community: {
  "153Y19UEJ2GW" => "No",
  "153Y19UBBXHJ" => "Yes"
}


}

DATA_CATEGORIES = {
  # single_family_type: hash_composite('at_single_family_types')
  # single_family_type: hash_composite('at_single_family_types')
}
DATA_ENUMS = {
  # category: hash_from_csv('categories'),
  # lot_use: hash_from_csv('lot_uses'),
  # rental_class: hash_from_csv('rental_classes'),
  # rental_type: hash_from_csv('rental_types'),
  # single_family_type: hash_from_csv('single_family_types'),
  #status: hash_from_csv('statuses'),
  # state: hash_from_csv('states'),
  # hoa_bill_freq: hash_from_csv('hoa_bill_freqs')
  mls_area_name: hash_from_csv('mls_areas'),
  mls_elementary_school: hash_from_csv('mls_elementary_schools'),
  mls_middle_school: hash_from_csv('mls_middle_schools'),
  mls_high_school: hash_from_csv('mls_high_schools')
}

DATA_CATEGORIES2 = {
  
}
DATA_ENUMS2 = {
  
} 

DATA_MAPPING = {
  acreage: proc { |d|
    case d['TotalAcreage']
      when '100tolessthan200'
         '150'
      when '10tolessthan20'
         '15'
      when '12Acreto1Acre'
         '6'
      when '14Acreto21779SqFt'
         '6'
      when '200tolessthan500'
         '350'
      when '20tolessthan50'
         '30'
      when '500PlusAcres'
         '600'
      when '50tolessthan100'
         '75'
      when '5tolessthan10'
         '7'
      when 'NonApplicable'
         '0'
      when 'OnePlustoTwoAcres'
         '1.8'
      when 'TwoPlustoFiveAcres'
         '4'
      when 'Upto10889SqFt'
         '0.2'
      when 'ZeroLotLine'
         '0'
    end

  },
  ### address:  'Address',
  ### adj_sale_price: proc { |d|
  #     d['ClosePrice'].to_s.to_d - d['SellerPaidBuyerCosts'].to_s.to_d
  #   },
  #annual_maintenance_fee:  'AnnualCDDFee+HOAFee+CondoFees+OtherFeesAmount ( DO NOT MULTIPLY BY hoa_bill_freq)',
  annual_maintenance_fee:  proc { |d|
          # Delayed::Worker.logger.debug "~~ HOA all fees: annual CDD #{d['AnnualCDDFee']} HOA Fee #{d['HOAFee']} Condo Fees #{d['CondoFee']} Other Fees #{d['OtherFeesAmount']} "
          #Formula: AnnualCDDFee + HOAFee + CondoFees + OtherFeesAmount ( DO NOT MULTIPLY BY hoa_bill_freq)
          # (d['AnnualCDDFee'].to_s.to_d + d['HOAFee'].to_s.to_d + d['CondoFees'].to_s.to_d + d['OtherFeesAmount'].to_s.to_d).to_s
          freq = (case d['HOAPaymentSchedule']
          when 'Annual'
            "1"
          when 'Monthly'
            "12"
          when'Quarterly'
            "4"
          when 'SemiAnnual'
            "2"
          else
            "1"
          end).to_d
         (( d['AnnualCDDFee'].to_s.to_d + d['CondoFees'].to_s.to_d + d['OtherFeesAmount'].to_s.to_d) + (d['HOAFee'].to_s.to_d * freq)).to_s
  }, #
  back_on_market: proc { |d| to_date_sql(d['BOMDate']) }, #
  # bathrooms: 'BathsTotal',
  bathrooms_full: 'BathsFull',
  bathrooms_half: 'BathsHalf',
  beds: 'BedsTotal',
  ###block: 'BlockParcel',
  building_name: 'BuildingNameNumber',
  building_stories: 'BuildingNumFloors',
  buyer_agent_bonus_amount: 'BonusAmount',
  buyer_agent_compensation: 'SingleAgentComp',
  category: proc { |d|
      if d['PropertyType'] == "RES"
        case d['PropertyStyle']
          when"12Duplex"
            'single_family'
          when"Coop"
            'townhouse_condo'
          when"Condo"
            'townhouse_condo'
          when"CondoHotel"
            'townhouse_condo'
          when"DockRackominium"
            'commercial'
          when"Farm"
            'ranch_acerage'
          when"ManufacturedMobileHome"
            'single_family'
          when"SingleFamilyHome"
            'single_family'
          when"Townhouse"
            'townhouse_condo'
          when"Villa"
            'single_family'
        end
      elsif d['PropertyType'] == "REN"
        case d['PropertyStyle']
          when"12Duplex"
            'rental'
          when"1stFloorMultiStory"
            'rental'
          when"2ndFloorMultiStory"
            'rental'
          when"3rdFlPlusaboveMultiStory"
            'rental'
          when"Apartment"
            'rental'
          when"Condo"
            'rental'
          when"CondoHotel"
            'rental'
          when"Coop"
            'rental'
          when"Efficiency"
            'rental'
          when"Fourplex"
            'rental'
          when"GarageApt"
            'rental'
          when"ManufacturedMobileHome"
            'rental'
          when"Modular"
            'rental'
          when"SingleFamilyHome"
            'rental'
          when"StiltHome"
            'rental'
          when"Townhouse"
            'rental'
          when"TriLevel"
            'rental'
          when"Triplex"
            'rental'
          when"Villa"
            'rental'
        end

      elsif d['PropertyType'] == "INC"
        case d['PropertyStyle']
          when 'DockRackominium'
            'commercial'
          when 'Duplex'
            'single_family'
          when 'FiveorMore'
            'single_family'
          when 'FourUnits'
            'single_family'
          when 'HomewithRentalUnit'
            'single_family'
          when 'SingleFamily'
            'single_family'
          when 'SingleFamilyHome'
            'single_family'
          when 'Townhouse'
            'townhouse_condo'
          when 'Triplex'
            'single_family'
          end
      elsif d['PropertyType'] == "COM"
        case d['PropertyStyle']
          when 'AcreageRanchGrove'
            'ranch_acerage'
          else
            'commercial'
          end
      elsif d['PropertyType'] == "VAC"
        case d['PropertyStyle']
          when 'BillboardSite'
           'commercial'
          when 'Business'
           'commercial'
          when 'Commercial'
           'commercial'
          when 'CropProducingFarm'
           'ranch_acerage'
          when 'DudeRanch'
           'ranch_acerage'
          when 'DuplexUse'
           'lots'
          when 'Farmland'
           'ranch_acerage'
          when 'FishFarm'
           'commercial'
          when 'FourUnitsUse'
           'lots'
          when 'Groves'
           'ranch_acerage'
          when 'HomeandIncomeHousing'
           'lots'
          when 'Industrial'
           'commercial'
          when 'Landfill'
           'commercial'
          when 'MixedUse'
           'lots'
          when 'MobileHomeUse'
           'commercial'
          when 'MultiFamily'
           'lots'
          when 'Other'
           'commercial'
          when 'PlantNursery'
           'commercial'
          when 'PUD'
           'commercial'
          when 'Ranchland'
           'ranch_acerage'
          when 'ResidentialDevelopment'
           'lots'
          when 'SingleFamilyUse'
           'lots'
          when 'Timberland'
           'commercial'
          when 'TransCellTower'
           'commercial'
          when 'TreeFarm'
           'commercial'
          when 'TriplexUse'
           'lots'
          when 'WellField'
           'commercial'
          when 'WorkingRanch'
           'ranch_acerage'
          end
      end
    },
  cdom: 'CDOM', #
  city: 'StreetCity', #
  closed_date: proc { |d| to_date_sql(d['CloseDate']) }, #
  completion_date: proc { |d| to_date_sql(d['ProjectedCompletionDate']) }, #
  cooling_system_type: 'AirConditioning',
  county: 'CountyOrParish',
  disclosures: 'SpecialSaleProvision', #
  dom: 'DOM', #
  equipment: 'AppliancesIncluded',
  expiration_date: proc { |d| to_date_sql(d['ExpirationDate']) }, #
  exterior_description: 'ExteriorFeatures',
  fence_type: 'Fences',
  financing_available: 'FinancingAvailable',
  fireplace_number: proc { |d|
      if d['FireplaceYN'] == 'Y'
        "1"
      elsif d['FireplaceYN'] == 'N'
        "0"
      else
        d['FireplaceYN']
      end
    },
  floor_type: 'FloorCovering',
  foundation_type: 'Foundation',
  gated_community: proc {|d|
        ret = nil
        Delayed::Worker.logger.debug ">>gated_community: }"
        unless d['CommunityFeatures'].blank?
          d['CommunityFeatures'].split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              
              ret = case item
                when 'GatedComm'
                "Y"
                when 'AirptRunwy','AssoRecLse','AssoRecOwn','BoatSlip','ByrApprReq','Cable','CardEntry','Clubhouse','ComHotTubSpa','CommBoatRamp','DeedRestr','DeedRstrctd','Dock','DockBoatSlip','Elevators','FeesReq','FishingPier','Fitness','GolfCartsOK','GolfComm','GolfCourse','HndcpModfd','HorsesAllowed','HorseStables','Irrigation','LaundryFacl','LawnPestFtzr','LawnSvcIncl','LobbyKeyReq','Marina','MtnceFree','NoCommPool','NoDeedRestr','None','NoPool','NoTkRVMoPk','OptnlAdlFees','Park','PestCntlIncl','Playground','Pool','PrvBoatRamp','PubBoatRamp','PUD','RacquetBall','RecBldg','Sauna','Security','ShoppingCtr','Shuffleboard','Sidewalk','SpcCommRestr','Storage','TenantsNoPet','TennisCourts','WaterAccess','WaterfrntUnit','WheelchAccess','WtrfrntCmplex','WtrfrontComm'
                "N"                
                else
                nil
              end
            rescue
              ret = nil
            end
            break if !ret.nil? && ret != "N"

          }
        end
        Delayed::Worker.logger.debug ">>ret: #{ret}"
        # !ret.blank? ? ret : "0"
        !ret.nil? ? ret : nil
    },
  garage_attach: proc { |d|

      if d['GarageCarport'] == '1CarCarport' then "N"
      elsif d['GarageCarport'] == '1CarGarage' then "Y"
      elsif d['GarageCarport'] == '2CarCarport' then "N"
      elsif d['GarageCarport'] == '2CarGarage' then "Y"
      elsif d['GarageCarport'] == '3CarCarport' then "N"
      elsif d['GarageCarport'] == '3CarGarage' then "Y"
      elsif d['GarageCarport'] == '4CarCarport' then "N"
      elsif d['GarageCarport'] == '4CarGarage' then "Y"
      elsif d['GarageCarport'] == '5PlusCarCarport' then "N"
      elsif d['GarageCarport'] == '5PlusCarGarage' then "Y"
      elsif d['GarageCarport'] == 'GolfCartGar' then "N"
      elsif d['GarageCarport'] == 'None' then "N"
      elsif d['GarageCarport'] == 'RVCarport' then "N"
      elsif d['GarageCarport'] == 'RVGarage' then "Y"
      elsif d['GarageCarport'] == 'UnderBuilding' then "Y"
      else "N"
      end
    },
  garage_capacity: proc { |d|
      if d['GarageCarport'] == '1CarCarport' then "0"
      elsif d['GarageCarport'] == '1CarGarage' then "1"
      elsif d['GarageCarport'] == '2CarCarport' then "0"
      elsif d['GarageCarport'] == '2CarGarage' then "2"
      elsif d['GarageCarport'] == '3CarCarport' then "0"
      elsif d['GarageCarport'] == '3CarGarage' then "3"
      elsif d['GarageCarport'] == '4CarCarport' then "0"
      elsif d['GarageCarport'] == '4CarGarage' then "4"
      elsif d['GarageCarport'] == '5PlusCarCarport' then "0"
      elsif d['GarageCarport'] == '5PlusCarGarage' then "5"
      elsif d['GarageCarport'] == 'GolfCartGar' then "0"
      elsif d['GarageCarport'] == 'None' then "0"
      elsif d['GarageCarport'] == 'RVCarport' then "0"
      elsif d['GarageCarport'] == 'RVGarage' then "1"
      elsif d['GarageCarport'] == 'UnderBuilding' then "1"
      else "0"
      end
    },
  golf_course: proc {|d|
        ret = nil
        Delayed::Worker.logger.debug ">>golf_course: }"
        unless d['CommunityFeatures'].blank?
          d['CommunityFeatures'].split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              
              ret = case item
                when 'GolfCourse'
                "Y"
                when 'AirptRunwy','AssoRecLse','AssoRecOwn','BoatSlip','ByrApprReq','Cable','CardEntry','Clubhouse','ComHotTubSpa','CommBoatRamp','DeedRestr','DeedRstrctd','Dock','DockBoatSlip','Elevators','FeesReq','FishingPier','Fitness','GatedComm','GolfCartsOK','GolfComm','HndcpModfd','HorsesAllowed','HorseStables','Irrigation','LaundryFacl','LawnPestFtzr','LawnSvcIncl','LobbyKeyReq','Marina','MtnceFree','NoCommPool','NoDeedRestr','None','NoPool','NoTkRVMoPk','OptnlAdlFees','Park','PestCntlIncl','Playground','Pool','PrvBoatRamp','PubBoatRamp','PUD','RacquetBall','RecBldg','Sauna','Security','ShoppingCtr','Shuffleboard','Sidewalk','SpcCommRestr','Storage','TenantsNoPet','TennisCourts','WaterAccess','WaterfrntUnit','WheelchAccess','WtrfrntCmplex','WtrfrontComm'
                "N"                
                else
                nil
              end
            rescue
              ret = nil
            end
            break if !ret.nil? && ret != "N"

          }
        end
        Delayed::Worker.logger.debug ">>ret: #{ret}"
        # !ret.blank? ? ret : "0"
        !ret.nil? ? ret : nil
    },
  heating_system: 'HeatingandFuel',
  hoa_bill_freq: proc { |d|
      if d['HOAPaymentSchedule'] == 'Annual'
        "1"
      elsif d['HOAPaymentSchedule'] == 'Monthly'
        "12"
      elsif d['HOAPaymentSchedule'] == 'Quarterly'
        "4"
      elsif d['HOAPaymentSchedule'] == 'SemiAnnual'
        "2"
      else
        "1"
      end
    }, #
  hoa_membership: 'HOACommAssn', #
  interior_description: proc{ |d|
      [
        d['InteriorLayout'].to_s.split(",").map{ |u| INTL[u.to_s] }.reject(&:blank?).join(', '),
        d['InteriorFeatures'].to_s.split(",").map{ |u| INTF[u.to_s] }.reject(&:blank?).join(', ')
      ].reject(&:blank?).join(', ')
    },
  key_map_page: 'PlatBookPage',
  land_size: 'LotDimensions',
  land_use_code: proc{ |d|
      [
          d['PropertyType'],
          'State:',
          d['StateLandUseCode'],
          'County:',
          d['CountyLandUseCode']
      ].compact.join(' ')
    },
  previous_list_price: 'LastListPrice',
  legal_description: 'LegalDescription',
  legal_subdivision: 'LegalSubdivisionName',
  level_of_unit: 'FloorNum', #
  list_price: 'ListPrice', #
  # list_price_psf: 'LPSqFt',
  listing_agent_email: 'ListAgentEmail',
  listing_agent_fax: 'AgentFax',
  listing_agent_id: 'ListAgentMLSID',
  listing_agent_name: 'ListAgentFullName',
  listing_agent_phone: 'ListAgentDirectWorkPhone',
  listing_broker_id: 'ListOfficeMLSID',
  listing_broker_name: 'ListOfficeName',
  listing_date: proc { |d| to_date_sql(d['ListingContractDate']) }, #
  lot: 'LotNum',
  lot_back_dimensions: 'LotDimensions',
  lot_description: 'Location',
  lot_number: 'LotNum',
  lot_square_feet: 'LotSizeSqFt', #
  lot_use: 'PropertyUse',
  maintenance_fee: 'HOAFee', #
  millage_rate: 'MillageRate',
  mls_area_key: 'MLSAreaMajor',
  mls_area_name: 'MLSAreaMajor',
  mls_elementary_school: 'ElementarySchool',
  mls_elementary_school_number:'ElementarySchool',
  mls_flood: 'FloodZoneCode',
  mls_high_school: 'HighSchool',
  mls_high_school_number: 'HighSchool',
  mls_middle_school: 'MiddleorJuniorSchool',
  mls_middle_school_number: 'MiddleorJuniorSchool',
  mls_number: 'MLSNumber',
  mls_record_modified: proc { |d| to_date_sql(d['MatrixModifiedDT']) }, #'MatrixModifiedDT',    
  neighborhood_number: 'SubdivisionNum',
  new_construction: proc { |d|
   case d['NewConstructionYN']
    when "0"
      'N'
    when "1"
      'Y'
    else
      ''
   end
  },
  new_construction_description: 'ConstructionStatus',
  number_of_photos: 'PhotoCount',
  office_phone: 'ListOfficePhone',
  original_list_price: 'OriginalListPrice',
  other_fees: 'OtherFees',
  owner_name: 'OwnerName',
  owner_phone: 'OwnerPhone',
  pending_date: proc { |d| to_date_sql(d['PendingDate']) },
  pool_area: proc { |d|
     if !d['PoolType'].blank?  then 'Yes'
      else 'No' end
    },
  pool_private: proc{ |d|
      if !d['Pool'].blank?
        pools = d['Pool'].split(",")
        pools = pools.reject do |x| x.in?(['Community','None']) end

        if pools && (pools.count > 0)
          'Yes'
        else
          'No'
        end
        
      end
    },
  previous_list_price: 'LastListPrice',
  public_remarks: 'PublicRemarksNew',
  range_number: 'Range',
  realtor_remarks: 'RealtorOnlyRemarks',
  rental_class: proc {|d|
      if d['PropertyType'] == 'REN'
        lookup_source(RNTC,d['PropertyStyle'])
      end
    },
    rental_type: proc {|d|
        if d['PropertyType'] == 'REN'
        lookup_source(RNTT,d['PropertyStyle'])
      end
    },
  road_surface: 'RoadFrontage', #
  roof_type: 'Roof', #
  sold_price: 'ClosePrice',
  # sale_price_psf: 'SPSqFt',
  section_number: 'Section',
  seller_paid_to_buyer_closing_costs: 'SellerPaidBuyerCosts',
  selling_agent_email: 'SellingAgentEmail',
  selling_agent_id: 'SellingAgentMLSID',
  selling_agent_name: 'SellingAgentFullName',
  selling_agent_phone: 'SellingAgentDirectWorkPhone',
  showing_instructions: 'ShowingInstructions',
  siding_type: 'ExteriorConstruction',#
  new_siding_type: 'ExteriorConstruction',#
  single_family_type: proc {|d|
      if d['PropertyType'] == 'RES'
        lookup_source(SFT,d['PropertyStyle'])
      elsif d['PropertyType'] == 'INC'
        case d['PropertyStyle']
          when 'Duplex'
            'duplex'
          when 'FiveorMore'
            'multiplex'
          when 'FourUnits'
            'multiplex'
          when 'HomewithRentalUnit'
            'freestanding'
          when 'SingleFamily'
            'manufactured'
          when 'SingleFamilyHome'
            'freestanding'
          when 'Triplex'
            'multiplex'
        end
      end
    },
  sold_lease_price: proc {|d| d['ClosePrice'] || d['LeasePrice'] },   #'LeasePrice',
  square_feet: 'SqFtHeated',
  state: 'StateOrProvince', #
  status: 'Status', #
  stories: proc {|d|
    ret = ""
    Delayed::Worker.logger.debug ">>stories: }"
    unless d['PropertyDescription'].blank?
      d['PropertyDescription'].split(",").each { |item|
        begin
          Delayed::Worker.logger.debug ">>item: #{item}"
          # Delayed::Worker.logger.debug ">>STO[item]: #{STOR[item.to_s]}"
          # ret = !STO[item].blank? ? STOR[item.to_s] : ""
          ret = case item
            when "1Level"
            "1"
            when "1stFloorMultiStory"
            ""
            when "2Levels"
            "2"
            when "2ndFloorMultiStory"
            ""
            when "3Levels"
            "3"
            when "3rdFloorPlusaboveMultiStory"
            ""
            when "4PlusLevels"
            "4"
            when "Attached"
            ""
            when "Detached"
            ""
            when "Efficiency"
            ""
            when "Elevated"
            ""
            when "EndUnit"
            ""
            when "FourStory"
            "4"
            when "GroundFloorUnit"
            ""
            when "GroundLevel"
            ""
            when "HighRise"
            ""
            when "InMHCommunity"
            ""
            when "MidRise"
            ""
            when "OneStory"
            "1"
            when "OutofMHCommunity"
            ""
            when "Penthouse"
            ""
            when "SplitLevel"
            "2"
            when "ThreeStory"
            "3"
            when "Townhouse23Floors"
            "3"
            when "TriLevel"
            "3"
            when "TwoStory"
            "2"
            else
            ""
          end
        rescue
          ret = ""
        end
        break if !ret.blank?

      }
    end
    Delayed::Worker.logger.debug ">>ret: #{ret}"
    !ret.blank? ? ret : "0"
  },
  new_stories: proc {|d|
    ret = ""
    Delayed::Worker.logger.debug ">>stories: }"
    unless d['PropertyDescription'].blank?
      d['PropertyDescription'].split(",").each { |item|
        begin
          Delayed::Worker.logger.debug ">>item: #{item}"
          # Delayed::Worker.logger.debug ">>STO[item]: #{STOR[item.to_s]}"
          # ret = !STO[item].blank? ? STOR[item.to_s] : ""
          ret = case item
            when "1Level"
            "1"
            when "1stFloorMultiStory"
            ""
            when "2Levels"
            "2"
            when "2ndFloorMultiStory"
            ""
            when "3Levels"
            "3"
            when "3rdFloorPlusaboveMultiStory"
            ""
            when "4PlusLevels"
            "4"
            when "Attached"
            ""
            when "Detached"
            ""
            when "Efficiency"
            ""
            when "Elevated"
            ""
            when "EndUnit"
            ""
            when "FourStory"
            "4"
            when "GroundFloorUnit"
            ""
            when "GroundLevel"
            ""
            when "HighRise"
            ""
            when "InMHCommunity"
            ""
            when "MidRise"
            ""
            when "OneStory"
            "1"
            when "OutofMHCommunity"
            ""
            when "Penthouse"
            ""
            when "SplitLevel"
            "2"
            when "ThreeStory"
            "3"
            when "Townhouse23Floors"
            "3"
            when "TriLevel"
            "3"
            when "TwoStory"
            "2"
            else
            ""
          end
        rescue
          ret = ""
        end
        break if !ret.blank?

      }
    end
    Delayed::Worker.logger.debug ">>ret: #{ret}"
    !ret.blank? ? ret : "0"
  },
  # 'PropertyDescription',
  # 'See MRED Tab ( PropertyDescription)',
  street_address: 'Address',
  street_direction: 'StreetDirPrefix',
  street_name: proc { |d|
      [
        d['StreetName'],
        d['StreetSuffix'].to_s.split(",").map{ |u| StreetSuffix[u.to_s] }.reject(&:blank?).join(', ')
      ].compact.join(' ')
    },
  street_number: 'StreetNumber',
  style_of_building: 'ArchitecturalStyle',
  new_style_of_building: 'ArchitecturalStyle',
  subdivision_section_number: 'SubdivisionSectionNumber',
  tax_amount: 'Taxes',
  tax_id: 'ParcelNumber',
  tax_year: 'TaxYear',
  termination_date: proc { |d| to_date_sql(d['offmarketdate']) },
  township: 'Township',
  unit_number: 'UnitNumber',
  waterfront: proc {|d|
      ret = "N"
      Delayed::Worker.logger.debug ">>waterfront: }"
      unless d['WaterFrontage'].blank?
        d['WaterFrontage'].split(",").each { |item|
          begin
            Delayed::Worker.logger.debug ">>item: #{item}"
            
            ret = case item
              when 'BayHarbor','BeachPrvt','BeachPub','CanalFresh','CanalSalt','Creek','GulfOcean','ICW','Lagoon','Lake','LakeChain','Marina','Ocean2Bay','Pond','River'
              "Y"
                            
              else
              "N"
            end
          rescue
            ret = nil
          end
          break if !ret.nil? && ret != "N"

        }
      end
      Delayed::Worker.logger.debug ">>ret: #{ret}"
      # !ret.blank? ? ret : "0"
      !ret.nil? ? ret : nil
  },
  withdrawn_date: proc { |d| to_date_sql(d['WithdrawnDate']) },
  year_built: 'YearBuilt',
  zip_4: 'PostalCodePlus4',
  zip_code: 'PostalCode'
}

@data_mapping2 = {
}

def initialize(market)
  super(market)
  Delayed::Worker.logger.debug ">> initializing metadata"
  set_up_metadata
end

def set_up_metadata
  a = nil
  if rets_login2
    # b = LOOKUP_NAMES
    # set_up_metadata_lookups
    set_up_metadata_lookups_from_json
    
    
    rets_logout2
  else
    self.errors[:login] = "could not login"
  end    
  
end

def set_up_metadata_hash(klass)
  ds = []
  res =  self.client.http_post("http://retsgw.flexmls.com/rets2_1/GetMetadata",
          { "Type"   => "METADATA-TABLE",
            "ID"     => "0" })
  str_metadata = res.body.squish

  doc = Nokogiri::XML(str_metadata)

  # thing = doc.xpath("//METADATA-LOOKUP_TYPE[@Lookup='20130226165751698625000000']/Lookup")
  # things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Class='A']/Field/LongName[text()='Equipment/Appliances']")
  LONG_NAMES.each do |d|
    puts "d@@: #{d}"
    things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Class='#{klass}']/Field/LongName[text()='#{d}']")
    if (things && (things.length > 0))
      ds << things[0].parent.at("./SystemName").text
    else
      ds << ""
    end
  end
  
  # puts "things #{things}"

  # ret = things[0].parent.at("./SystemName")
  # str_metadata

  ds if (ds && (ds.length > 0))

end

def set_up_metadata_lookups()
  hsh = {}
  ds = []
  res =  self.client.http_post("http://retsgw.flexmls.com/rets2_1/GetMetadata",
          { "Type"   => "METADATA-TABLE",
            "ID"     => "0" })
  str_metadata = res.body.squish

  res2 =  self.client.http_post("http://retsgw.flexmls.com/rets2_1/GetMetadata",
          { "Type"   => "METADATA-LOOKUP_TYPE",
            "ID"     => "0" })
  str_metadata2 = res2.body.squish

  doc = Nokogiri::XML(str_metadata)
  doc2 = Nokogiri::XML(str_metadata2)

  LOOKUP_NAMES.each do |d|

    KLASSES.each do |k|
      things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Resource='Property' and @Class='#{k}']/Field/LongName[text()='#{d[0]}']")
      if (things && (things.length > 0))
        ds << things[0].parent.at("./LookupName").text
      end
    end

    if (ds && ds.length > 0)
      symb = d[1].to_sym
      hsh[symb] = {}
      ds.each do |x|
      # things2 = doc2.xpath("//RETS/METADATA/METADATA-LOOKUP_TYPE[@Lookup='#{x}']/Lookup")
        things2 = doc2.xpath("//RETS/METADATA/METADATA-LOOKUP_TYPE[@Lookup='#{x}']/LookupType")

        things2.each do |th|
          s_value = th.at("./Value").text
          s_long_value = th.at("./LongValue").text
          hsh[symb] = hsh[symb].merge({s_value => s_long_value})
        end
      end
    end

    ds = []

  end

  File.open(Rails.root.join('lookup_types.json'),'w') { |file| file.write(hsh.to_json) }

  @lookup_type_enum2  = hsh
  Delayed::Worker.logger.debug "lookup type enum2: #{@lookup_type_enum2}"
  return hsh

end

def set_up_metadata_lookups_from_json()
  file = File.read(Rails.root.join('lookup_types.json'))
  @lookup_type_enum2  = JSON.parse(file)      
  @lookup_type_enum2 = @lookup_type_enum2.with_indifferent_access
end

def set_up_metadata_fields(fields)
  @data_mapping2 = {
    access_lock_box: 'LIST_30',
    acreage: 'LIST_57',
    annual_maintenance_fee:  proc {|d|
      Delayed::Worker.logger.debug "!! fields[1] #{fields[1]} d field: #{d[fields[1]]} freq options: #{@lookup_type_enum2.try(:[],:hoa_bill_freq_options).try(:[],d[fields[1]])}"
      freq = (case @lookup_type_enum2.try(:[],:hoa_bill_freq_options).try(:[],d[fields[1]])
      when "Bi-Annual"
        "2"
      when "Yearly"
        "1"
      when "Voluntary"
        "0"
      when "Quarterly"
        "4"
      when "None"
        "0"
      when "Monthly"
        "12"
      else
        "1"
      end).to_d
      # freq = (case d[fields[1]]
      #       when '153Y18YB5W5S'
      #         "2"
      #       when '153Y18XHFXO7'
      #         "1"
      #       when '153Y18YG8U5U'
      #         "0"
      #       when '153Y18XE2JMG'
      #         "4"
      #       when '15B0JZCFK88M'
      #         "0"
      #       when  '153Y18X9RLA1'
      #         "12"
      #       else
      #         "1"
      #       end).to_d
      ((d[fields[0]].to_s.to_d) * freq).to_s
    }, ##
    appointment_phone_description: 'LIST_84', #
    bathrooms: 'LIST_67', #
    bathrooms_full: 'LIST_68', #
    bathrooms_half: 'LIST_69',  #
    beds: 'LIST_66',
    buyer_agent_bonus: 'LIST_114',
    category:  proc {|d| 
      case d['LIST_8']
      when 'A'
        case  @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9']) 
        when 'Single Family Detached'
        'single_family'
        when 'Townhouse'
        'townhouse_condo'
        when 'Time Share'
        'townhouse_condo'
        when 'Mobile/Manufactured'
        'single_family'
        when 'Half Duplex'
         'single_family'
        when 'Coop'
          'townhouse_condo'
        when 'Condo-Tel'
          'townhouse_condo'
        when 'Condo'
          'townhouse_condo'
        end
      when 'B'
        # case  @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9']) 
        # when 'Income'
        'multi_family'
        # end
      when 'C'
        # case  @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9'])
        # when 'Vacant Land'
          'lots'
        # end
      when 'D'
        # case  @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9'])
        # when 'Rental' 
          'rental'
        # end
      when 'F'
        # case  @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9'])
        # when 'Commercial - Sale'
          'commercial'
        # end
      when 'G'
        # case  @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9'])
        # when 'Commercial - Lease'         
          'commercial'
        # end
      end
    }, ##
    cdom: 'LIST_135',
    city: 'LIST_39',
    closed_date: proc { |d| self.class.to_date_sql(d['LIST_12']) },
    cooling_system_type:  fields[2],
    county: 'LIST_41',
    dom: 'LIST_137',
    equipment: fields[3],
    expiration_date: proc { |d| self.class.to_date_sql(d['LIST_11']) }, #
    exterior_description:  fields[4],
    floor_type:  fields[5],
    garage_attach: proc { |d|
      ret = []
      retu = nil
      Delayed::Worker.logger.debug ">>garage_capacity: #{d[fields[20]]}"
      unless d[fields[20]].blank?
        d[fields[20]].split(",").each { |item|
          begin
            Delayed::Worker.logger.debug ">>item: #{item}"
            ite = nil
            its = {
              '1 Car Attchd Garage' => 'Y',
              '3+ Car Carport' => 'N',
              'No Parking' => 'N',
              '4+ Car Attchd Garage' => 'Y',
              '2 Car Carport' => 'N',
              '1 Car Carport' => 'N',
              '3 Car Attchd Garage' => 'Y',
              'Open Parking' => 'N',
              'Reserved' => 'N',
              'Under Building' => 'Y',
              '2 Car Attchd Garage' => 'Y',
              '4+ Car Dtchd Garage' => 'N',
              '3 Car Dtchd Garage' => 'N',
              '2 Car Dtchd Garage' => 'N',
              '1 Car Dtchd Garage' => 'N',
              'Street' => 'N'
              }
            ite = its[@lookup_type_enum2.try(:[],:garage_capacity_options).try(:[],item)]
            ret << ite if ite
          rescue
            ret = []
          end
          
        }

        Delayed::Worker.logger.debug ">>ret item: #{ret}"
        rsl = 0
        if (ret && (ret.length > 0))
          rs = ret.reject do |x| x.in?(['N']) end
          if (rs && (rs.length > 0))
            retu = 'Y'
          else
            retu = 'N'
          end
        else
          retu = nil
        end
      end
      retu
    },
    garage_capacity: proc { |d|
      ret = []
      Delayed::Worker.logger.debug ">>garage_capacity: }"
      unless d[fields[20]].blank?
        d[fields[20]].split(",").each { |item|
          begin
            Delayed::Worker.logger.debug ">>item: #{item}"
            ite = nil
            its = {
              '1 Car Attchd Garage' => '1',
              '3+ Car Carport' => '3',
              'No Parking' => '0',
              '4+ Car Attchd Garage' => '4',
              '2 Car Carport' => '3',
              '1 Car Carport' => '1',
              '3 Car Attchd Garage' => '3',
              'Under Building' => '1',
              '2 Car Attchd Garage' => '2',
              '4+ Car Dtchd Garage' => '4',
              '3 Car Dtchd Garage' => '3',
              '2 Car Dtchd Garage' => '2',
              '1 Car Dtchd Garage' => '1',
              'Street' => '0'
              }
            ite = its[@lookup_type_enum2.try(:[],:garage_capacity_options).try(:[],item)]
            ret << ite.to_i if (ite && ite.to_s.to_i >= 0)
          rescue
            ret = []
          end
          
        }

        rsl = 0
        if (ret && (ret.length > 0))
          ret.each { |rt|
            rsl = rsl + rt 
          }
          rsl.to_s
        else
          nil
        end
      end

    },
    hoa_bill_freq: proc {|d|
      case @lookup_type_enum2.try(:[],:hoa_bill_freq_options).try(:[],d[fields[1]])
      when "Bi-Annual"
        "2"
      when "Yearly"
        "1"
      when "Voluntary"
        "0"
      when "Quarterly"
        "4"
      when "None"
        "0"
      when "Monthly"
        "12"
      else
        "1"
      end
    }, ##
    hoa_membership: 'LIST_107',
    interior_description: fields[6], #
    key_map_book: fields[7],
    key_map_page: fields[8],
    latitude: 'LIST_46',
    legal_description: 'LIST_81',
    list_price: 'LIST_22',
    listing_agent_email: 'listing_member_email',
    listing_agent_fax: 'listing_member_fax',
    listing_agent_id: 'LIST_167',
    listing_agent_name: 'listing_member_name',
    listing_agent_phone: 'listing_member_phone',
    listing_broker_id: 'LIST_163',
    listing_broker_name: 'listing_office_name',
    listing_date: proc { |d| self.class.to_date_sql(d['LIST_10']) }, #    
    longitude: 'LIST_47',
    lot_description: fields[9],
    lot_square_feet: proc { |d| 
      if (!d['LIST_52'].blank?)
        d['LIST_52']
      else
        (d['LIST_57'].to_d * 43560).to_i.to_s
      end
    },
    maintenance_fee: fields[0],
    mls_area_key: proc { |d|
      # FLEX_MLS_AREA.try(:[],d['LIST_29']).try(:[],"key") 
      tok = (case @lookup_type_enum2.try(:[],:mls_area_options).try(:[],d['LIST_29'])
      when "381 - N Satellite Beach" 
        "381 - N Satellite Beach"
      when "355 - Merritt Island S of Pineda" 
        "355 - Merritt Island S of Pineda"
      when "350 - Barefoot Bay/Micco" 
        "350 - Barefoot Bay/Micco"
      when "347 - Southern Palm Bay" 
        "347 - Southern Palm Bay"
      when "345 - SW Palm Bay" 
        "345 - SW Palm Bay"
      when "344 - NW Palm Bay" 
        "344 - NW Palm Bay"
      when "343 - SE Palm Bay" 
        "343 - SE Palm Bay"
      when "342 - Malabar/Grant-Valkaria" 
        "342 - Malabar/Grant-Valkaria"
      when "341 - NE Palm Bay" 
        "341 - NE Palm Bay"
      when "340 - NE Palm Bay" 
        "340 - NE Palm Bay"
      when "331 - West Melbourne" 
        "331 - West Melbourne"
      when "330 - Melbourne" 
        "330 - Melbourne"
      when "323 - Eau Gallie" 
        "323 - Eau Gallie"
      when "322 - Palm Shores" 
        "322 - Palm Shores"
      when "321 - Lake Washington/S of Post" 
        "321 - Lake Washington/S of Post"
      when "320 - Pineda/Lake Washington" 
        "320 - Pineda/Lake Washington"
      when "272 - Cocoa Beach" 
        "272 - Cocoa Beach"
      when "271 - Cape Canaveral" 
        "271 - Cape Canaveral"
      when "270 - Port Canaveral" 
        "270 - Port Canaveral"
      when "254 - Newfound Harbor" 
        "254 - Newfound Harbor"
      when "253 - S Merritt Island" 
        "253 - S Merritt Island"
      when "252 - N Banana River Dr." 
        "252 - N Banana River Dr."
      when "251 - Central Merritt Island" 
        "251 - Central Merritt Island"
      when "250 - N Merritt Island" 
        "250 - N Merritt Island"
      when "218 - Suntree S of Wickham" 
        "218 - Suntree S of Wickham"
      when "217 - Viera West of I 95" 
        "217 - Viera West of I 95"
      when "216 - Viera/Suntree N of Wickham" 
        "216 - Viera/Suntree N of Wickham"
      when "215 - West Cocoa" 
        "215 - West Cocoa"
      when "214 - Rockledge" 
        "214 - Rockledge"
      when "213 - Mainland E of US 1" 
        "213 - Mainland E of US 1"
      when "212 - Cocoa - West of US 1" 
        "212 - Cocoa - West of US 1"
      when "211 - Canaveral Groves" 
        "211 - Canaveral Groves"
      when "210 - Cocoa West of I 95" 
        "210 - Cocoa West of I 95"
      when "107 - Port St. John" 
        "107 - Port St. John"
      when "105 - Titusville W I95 S 46 to Fox Lake" 
        "105 - Titusville W I95 S 46 to Fox Lake"
      when "104 - Titusville SR50 - Kings H" 
        "104 - Titusville SR50 - Kings H"
      when "103 - Titusville Garden - SR50" 
        "103 - Titusville Garden - SR50"
      when "102 - Mims/Tville SR46 - Garden" 
        "102 - Mims/Tville SR46 - Garden"
      when "101 - Mims/Scottsmoor" 
        "101 - Mims/Scottsmoor"
      when "999 - Out of Area" 
        "999 - Out of Area"
      when "907 - Miami-Dade" 
        "907 - Miami-Dade"
      when "906 - St Lucie County" 
        "906 - St Lucie County"
      when "905 - Seminole" 
        "905 - Seminole"
      when "904 - Indian River" 
        "904 - Indian River"
      when "903 - Osceola" 
        "903 - Osceola"
      when "902 - Orange" 
        "902 - Orange"
      when "901 - Volusia" 
        "901 - Volusia"
      when "385 - South Beaches" 
        "385 - South Beaches"
      when "384-Indialantic/Melbourne Beach" 
        "384-Indialantic/Melbourne Beach"
      when "383 - N Indialantic" 
        "383 - N Indialantic"
      when "382-Satellite Bch/Indian Harbour Bch" 
        "382-Satellite Bch/Indian Harbour Bch"
      end)

      m = tok.match(/^(\d+)\s*-/).captures rescue nil

      # m = tok.match(/^\d+\s*-\s*(\D+)/).captures rescue nil

      if (m && (m.length > 0))
        m[0]
      end

    }, #*   
    mls_area_name: proc { |d|
      # FLEX_MLS_AREA.try(:[],d['LIST_29']).try(:[],"name") 
      tok = (case @lookup_type_enum2.try(:[],:mls_area_options).try(:[],d['LIST_29'])
      when "381 - N Satellite Beach" 
        "381 - N Satellite Beach"
      when "355 - Merritt Island S of Pineda" 
        "355 - Merritt Island S of Pineda"
      when "350 - Barefoot Bay/Micco" 
        "350 - Barefoot Bay/Micco"
      when "347 - Southern Palm Bay" 
        "347 - Southern Palm Bay"
      when "345 - SW Palm Bay" 
        "345 - SW Palm Bay"
      when "344 - NW Palm Bay" 
        "344 - NW Palm Bay"
      when "343 - SE Palm Bay" 
        "343 - SE Palm Bay"
      when "342 - Malabar/Grant-Valkaria" 
        "342 - Malabar/Grant-Valkaria"
      when "341 - NE Palm Bay" 
        "341 - NE Palm Bay"
      when "340 - NE Palm Bay" 
        "340 - NE Palm Bay"
      when "331 - West Melbourne" 
        "331 - West Melbourne"
      when "330 - Melbourne" 
        "330 - Melbourne"
      when "323 - Eau Gallie" 
        "323 - Eau Gallie"
      when "322 - Palm Shores" 
        "322 - Palm Shores"
      when "321 - Lake Washington/S of Post" 
        "321 - Lake Washington/S of Post"
      when "320 - Pineda/Lake Washington" 
        "320 - Pineda/Lake Washington"
      when "272 - Cocoa Beach" 
        "272 - Cocoa Beach"
      when "271 - Cape Canaveral" 
        "271 - Cape Canaveral"
      when "270 - Port Canaveral" 
        "270 - Port Canaveral"
      when "254 - Newfound Harbor" 
        "254 - Newfound Harbor"
      when "253 - S Merritt Island" 
        "253 - S Merritt Island"
      when "252 - N Banana River Dr." 
        "252 - N Banana River Dr."
      when "251 - Central Merritt Island" 
        "251 - Central Merritt Island"
      when "250 - N Merritt Island" 
        "250 - N Merritt Island"
      when "218 - Suntree S of Wickham" 
        "218 - Suntree S of Wickham"
      when "217 - Viera West of I 95" 
        "217 - Viera West of I 95"
      when "216 - Viera/Suntree N of Wickham" 
        "216 - Viera/Suntree N of Wickham"
      when "215 - West Cocoa" 
        "215 - West Cocoa"
      when "214 - Rockledge" 
        "214 - Rockledge"
      when "213 - Mainland E of US 1" 
        "213 - Mainland E of US 1"
      when "212 - Cocoa - West of US 1" 
        "212 - Cocoa - West of US 1"
      when "211 - Canaveral Groves" 
        "211 - Canaveral Groves"
      when "210 - Cocoa West of I 95" 
        "210 - Cocoa West of I 95"
      when "107 - Port St. John" 
        "107 - Port St. John"
      when "105 - Titusville W I95 S 46 to Fox Lake" 
        "105 - Titusville W I95 S 46 to Fox Lake"
      when "104 - Titusville SR50 - Kings H" 
        "104 - Titusville SR50 - Kings H"
      when "103 - Titusville Garden - SR50" 
        "103 - Titusville Garden - SR50"
      when "102 - Mims/Tville SR46 - Garden" 
        "102 - Mims/Tville SR46 - Garden"
      when "101 - Mims/Scottsmoor" 
        "101 - Mims/Scottsmoor"
      when "999 - Out of Area" 
        "999 - Out of Area"
      when "907 - Miami-Dade" 
        "907 - Miami-Dade"
      when "906 - St Lucie County" 
        "906 - St Lucie County"
      when "905 - Seminole" 
        "905 - Seminole"
      when "904 - Indian River" 
        "904 - Indian River"
      when "903 - Osceola" 
        "903 - Osceola"
      when "902 - Orange" 
        "902 - Orange"
      when "901 - Volusia" 
        "901 - Volusia"
      when "385 - South Beaches" 
        "385 - South Beaches"
      when "384-Indialantic/Melbourne Beach" 
        "384-Indialantic/Melbourne Beach"
      when "383 - N Indialantic" 
        "383 - N Indialantic"
      when "382-Satellite Bch/Indian Harbour Bch" 
        "382-Satellite Bch/Indian Harbour Bch"
      end)        

      m = tok.match(/^\d+\s*-\s*([A-Za-z0-9 ,-\/]+)/).captures rescue nil

      if (m && (m.length > 0))
        m[0]
      end
    }, #*
    mls_elementary_school: fields[10],
    mls_high_school: fields[11],
    mls_middle_school: fields[12],
    mls_number: 'LIST_3',
    mls_record_modified: 'LIST_87',
    new_construction: 'LIST_92',
    new_stories: 'LIST_65',
    original_list_price: 'LIST_21',
    owner_name: 'LIST_83',
    number_of_photos: 'LIST_133',
    pool_private: 'LIST_150',
    public_remarks: 'LIST_78',
    realtor_remarks: 'LIST_79',
    rental_class: proc { |d| 
      
      if d['LIST_8'] == 'D'
        case @lookup_type_enum2.try(:[],:prop_type_options).try(:[],d['LIST_9'])
        when "Single Family"
          "single_family"
        when "Townhouse"
          "townhouse_condo"
        when "Timeshare"
          ""
        when "Professional Office"
          "commercial"
        when "Manufactured Home"
          "manufactured"
        when "Efficiency"
          "single_family"
        when "Duplex"
          "single_family"
        when "Condotel"
          "multi_family"
        when "Condo"
          "townhouse_condo"
         when "Apartment"
          "multi_family"
        else
          ""
        end
        # case d['LIST_9']
        # when "1541KRWIT7YQ"
        #   "single_family"
        # when "1541KS9ZKU4W"
        #   "townhouse_condo"
        # when "1541KSA32CTG"
        #   ""
        # when "1541KSABTZEA"
        #   "commercial"
        # when "1541KS9SJF9A"
        #   "manufactured"   #
        # when "1541KSA5G367"
        #   "single_family"
        # when "1541KS8ZHDAI"
        #   "single_family"
        # when "1541KS8WS4YC"
        #   "multi_family"
        # when "1541KS8RVRI7"
        #   "townhouse_condo"
        # when "1541KS8NWU1E"
        #   "multi_family"
        # else
        #   ""
        # end
      else
        ""
      end
    },##
    rental_terms: fields[13],
    rental_type:  proc { |d|
      
      if d['LIST_8'] == 'D'
        case @lookup_type_enum2.try(:[],:prop_type_options).try(:[],d['LIST_9'])
        when "Single Family"
          "freestanding"
        when "Townhouse"
          "town_house"
        when "Timeshare"
          "other"
        when "Professional Office"
          "commercial"
        when "Manufactured Home"
          ""
        when "Efficiency"
          "other"
        when "Duplex"
          "single_family"
        when "duplex"
          "condo"
        when "Condo"
          "condo"
         when "Apartment"
          "apartment"
        else
          ""
        end
        # case d['LIST_9']
        # when "1541KRWIT7YQ"
        #   "freestanding"
        # when "1541KS9ZKU4W"
        #   "town_house"
        # when "1541KSA32CTG"
        #   "other"
        # when "1541KSABTZEA"
        #   "commercial"
        # when "1541KS9SJF9A"
        #   ""
        # when "1541KSA5G367"
        #   "other"
        # when "1541KS8ZHDAI"
        #   "duplex"
        # when "1541KS8WS4YC"
        #   "condo"
        # when "1541KS8RVRI7"
        #   "condo"
        # when "1541KS8NWU1E"
        #   "apartment"
        # else
        #   ""
        # end
      else
        ""
      end
    }, ##
    # 'LIST_9',
    roof_type: fields[14],
    sold_price: 'LIST_23',
    sold_lease_price: 'LIST_23',    
    showing_instructions: 'LIST_86',
    single_family_type: proc { |d|
      # sft = {
      #   "153Y11DH36QL" => "freestanding",
      #   "153Y11FG8FOZ" => "townhome",
      #   "156WO53J030U" => "townhome",
      #   "153Y11CPQASF" => "manufactured",
      #   "153Y11CJAUAL" => "multiplex",
      #   "156WO53F7EY7" => "townhome",
      #   "156WO53Q4I4K" => "multiplex",
      #   "153Y11FD5I6Y" => "townhome"
      # }

      if d['LIST_8'] == 'A'
        case @lookup_type_enum2.try(:[],:res_sub_type_options).try(:[],d['LIST_9']) 
        when 'Mobile/Manufactured'
          'manufactured'
        when 'Mobile/Manufactured'
          'duplex'        
        end
      end
    },
    square_feet: proc { |d|
      case d['LIST_8']
      when 'A'
        d['LIST_48']
      when 'B'
        d['LIST_49']
      when 'D'
        d['LIST_48']
      end
        
    },
    state: 'LIST_40',
    status: proc { |d|
    
      # case d['LIST_15']
      # when "1536QBU2R0NU"
      #   "active"
      # when "1536QBU2RECN"
      #   "withdrawn"
      # when "1536QBU2R99V"
      #   "pending"
      # when "1536QBU2RBV2"
      #   "expired"
      # when "1536QBU2RJIZ"
      #   "terminated"
      # when "1536QBU2R6W9"
      #   "sold"
      # when "1536QBU2RGVC"
      #   "cancelled"
      # when "PWC_1585BVP21LL9"
      #   "pending"
      # end

      case @lookup_type_enum2.try(:[],:status_options).try(:[],d['LIST_15'])
      when "Active"
        "active"
      when "Withdrawn"
        "withdrawn"
      when "Pending"
        "pending"
      when "Expired"
        "expired"
      when "Deleted"
        "terminated"
      when "Closed"
        "sold"
      when "Cancelled"
        "cancelled"
      when "Contingent"
        "pending"
      end

    }, #'LIST_15',
    street_direction: 'LIST_33',
    street_name: proc { |d|
      # suffixes = {
      #   "1536QBU2ZI4O" => "Run",
      #   "1536QBU2ZFIH" => "Row",
      #   "1536QBU2TTCR" => "Road",
      #   "1536QBU2ZCQR" => "River",
      #   "1536QBU2YH9A" => "Ridge",
      #   "1536QBU2WRJB" => "Point",
      #   "1536QBU2U326" => "Place",
      #   "1536QBU2WCTQ" => "Pines",
      #   "1536QBU2WA41" => "Pine",
      #   "1536QBU2W512" => "Pike",
      #   "1536QBU2W2BS" => "Path",
      #   "1536QBU2WF5C" => "Passage",
      #   "1536QBU2W7LU" => "Parkway",
      #   "1536QBU2VWW6" => "Meadow",
      #   "1536QBU2VZQ7" => "Manor",
      #   "1536QBU2TQXS" => "Loop",
      #   "1536QBU2TO9I" => "Lane",
      #   "1536QBU2VUJM" => "Landing(s)",
      #   "1536QBU2VRP6" => "Lakes",
      #   "1536QBU2VPCJ" => "Lake",
      #   "1536QBU2VMO0" => "Isle(s)",
      #   "1536QBU2VIQS" => "Island",
      #   "1536QBU2VGE4" => "Hill",
      #   "1536QBU2UHOF" => "Highway",
      #   "1536QBU2VDSM" => "Grove",
      #   "1536QBU2VB5T" => "Green",
      #   "1536QBU2V8UJ" => "Glen",
      #   "1536QBU2V6DY" => "Gardens",
      #   "1536QBU2V44V" => "Garden",
      #   "1536QBU2V1QO" => "Forest",
      #   "1536QBU2UZ79" => "Estate(s)",
      #   "1536QBU2TLYK" => "Drive",
      #   "1536QBU304TY" => "Crossing",
      #   "1536QBU2URPA" => "Cove",
      #   "1536QBU2TIHK" => "Court",
      #   "1536QBU2UP43" => "Club",
      #   "1536QBU2TFYC" => "Circle",
      #   "1536QBU2UWML" => "Center",
      #   "1536QBU2UU1M" => "Causeway",
      #   "1536QBU2TDJS" => "Boulevard",
      #   "1536QBU2UMFU" => "Bend",
      #   "1536QBU2UK9Q" => "Beach",
      #   "1536QBU2TAZS" => "Avenue",
      #   "1536QBU2SYG9" => "Alley",
      #   "1536QBU2UFET" => "Way",
      #   "1536QBU30292" => "View",
      #   "1536QBU2ZX1Z" => "Turnpike",
      #   "1536QBU2UCRJ" => "Trail",
      #   "1536QBU2ZZHO" => "Trace",
      #   "1536QBU2U0EA" => "Terrace",
      #   "1536QBU2UAAH" => "Street Ocean",
      #   "1536QBU2U7UL" => "Street Gulf",
      #   "1536QBU2TWT9" => "Street",
      #   "1536QBU2ZUMN" => "Square",
      #   "1536QBU2ZS10" => "Springs",
      #   "1536QBU2ZPCG" => "Spring",
      #   "1536QBU2ZMYN" => "Shores",
      #   "1536QBU2ZKJV" => "Shore"       
      # }
      suff = @lookup_type_enum2.try(:[],:street_suffix_options).try(:[],d['LIST_37'])
      [d['LIST_34'],suff].reject(&:blank?).join(" ")
    },

    #'concat(LIST_34,LIST_37)',
    street_number: 'LIST_31',
    street_surface_type: fields[19],
    new_style_of_building: fields[16],
    subdivision: 'LIST_77',
    tax_amount: 'LIST_75',
    tax_id: 'LIST_80',
    tax_year: 'LIST_76',
    unit_number: 'LIST_35',
    waterfront: 'LIST_108',
    withdrawn_date: proc { |d|  self.class.to_date_sql(d['LIST_17'])    },      
    year_built: 'LIST_53',
    zip_code: 'LIST_43',
    gated_community: fields[17]

    
  }

end

def rets_login
  @client = Rets::Client.new(:login_url => "http://rets.mfrmls.com/contact/rets/login",:username => market.mls_login,:password => market.mls_password)
  
  Delayed::Worker.logger.debug "RETS client #{@client}"
  Delayed::Worker.logger.debug "Orlando RETS logged in"
  true
end

def rets_login2
  
  @client =  Rets::Client.new(:login_url => "http://retsgw.flexmls.com:80/rets2_1/Login",:username => "spc.rets.ogreen",:password => "milha-ence98")
  Delayed::Worker.logger.debug "RETS client #{@client}"
  Delayed::Worker.logger.debug "Orlando2 RETS logged in"
  true
end

def rets_logout
  @client.logout
  Delayed::Worker.logger.debug "logged out"
  true
end

def rets_logout2
  @client.logout
  Delayed::Worker.logger.debug "logged out"
  true
end

def mls_url_for(property)
  "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
end

def tax_url_for(property)
  "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
end

def deed_url_for(property)
  "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
end

def grab(days)
  if rets_login
    Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
    parse_rets()
    rets_logout
  else
    self.errors[:login] = "could not login"
  end
  notify_listeners
end

# def grab_by_interval(minutes=nil)
#   if rets_login
#     Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS by interval ~~~~~~~~"
#     parse_rets_by_interval(minutes) 
#     rets_logout
#   else
#     self.errors[:login] = "could not login"
#   end
#   notify_listeners
# end

def grab_by_interval(minutes=nil)
  # t = DateTime.strptime('2015-02-09T08:08:00-06:00', '%Y-%m-%dT%H:%M:%S%z')
  t =  !Rails.env.development? ? Time.now  : Time.now - 840.minutes
  
  Delayed::Worker.logger.debug "~~@~~~ loggin in ~~~~~~"
   
  begin
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
      parse_rets_by_interval(t,minutes)
      # parse_rets4_by_interval(t,minutes)
      rets_logout
      
    else
      self.errors[:login] = "could not login MLS1"
    end
  rescue Exception => e
     Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
  end
  
  begin
    if rets_login2
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
      parse_rets2_by_interval(t,minutes)
      rets_logout2
    else 
      self.errors[:login] = "could not login MLS2"
    end
  rescue Exception => e
     Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
  end
  
  begin
    notify_listeners
  rescue Exception => e
     Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
  end
end



def grab_by_specified_period(start_time,end_time)

  begin
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 1 ~~~~~~~~"
      parse_rets_by_specified_period(start_time,end_time) 
      rets_logout
      
    else
      self.errors[:login] = "could not login MLS1"
    end
  rescue Exception => e
     Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 1 ~~~~~~~~"
  end
  
  begin
    if rets_login2
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS 2 ~~~~~~~~"
      parse_rets2_by_specified_period(start_time,end_time) 
      rets_logout2
    else 
      self.errors[:login] = "could not login MLS2"
    end
  rescue Exception => e
     Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS 2 ~~~~~~~~"
  end
  
  begin
    notify_listeners
  rescue Exception => e
     Delayed::Worker.logger.debug "~~~~~~~ Error Syncing RETS Notification ~~~~~~~~"
  end
end

def sync_rets(days)
  if rets_login
    Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
    parse_rets_test()
    rets_logout
  else
    self.errors[:login] = "could not login"
  end
  notify_listeners
end

def self.to_date_sql(dt)
  #"2014-05-09T15:40:14"
  field = dt.to_s
  pattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil
  subpattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil
  ma =  field.match(pattern).captures.try(:[],0) rescue nil
  ta =  field.match(subpattern).captures.try(:[],0) rescue nil
  if field && field.match(/(\d+\/\d+\/\d+)/)
    strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'
    !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil
  else
    strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'
    !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil
  end
rescue
  nil
end

protected

private

  def parse_raw(data, filename)

    begin
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

        # row['PropType'] ||= filename.try(:slice, -7, 3)
        save_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    rescue Exception => e
      Delayed::Worker.logger.debug "error #{e.message}"
    end
  end

  def parse_geometry(data, filename)
    CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
      # row['PropType'] ||= filename.try(:slice, -7, 3)
      correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
    end
  end


  # def parse_rets()

  #     # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
  #     timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
  #     timerange = "2014-01-01-2014-06-11"
  #     sd = Date.parse('2014-09-21')
  #     ed = Date.parse('2015-03-20')
  #     Delayed::Worker.logger.debug "--------- pulling single family ---------"
  #     ed.downto(sd) { |date|

  #       Delayed::Worker.logger.debug "---------  #{date} ---------"
  #       date1 = date.strftime("%Y-%m-%dT00:00:00")
  #       date2 = date.strftime("%Y-%m-%dT06:00:00")
  #       date3 = date.strftime("%Y-%m-%dT12:00:00")
  #       date4 = date.strftime("%Y-%m-%dT18:00:00")
  #       tomorrow = date + 1.day
  #       date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
  #       dates = []
  #       dates = [date1,date2,date3,date4,date5]
  #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)

  #       # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)


  #       for i in 0..3
  #         Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"

  #         begin
  #           recdate = "#{dates[i]}-#{dates[i+1]}"
  #           Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
  #           results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSO,PNC,TOM,WDN),(MatrixModifiedDT=#{recdate}),(PropertyType=RES)",:limit => 60000)
  #           Delayed::Worker.logger.debug "--------- importing Single Family ---------"
  #           results.each do |row|
  #             Delayed::Worker.logger.debug "##row: #{row}"
  #             save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #           end
  #         rescue Exception => e
  #           Delayed::Worker.logger.debug "No record found #{e.message}"
  #         end

  #         begin
  #           recdate = "#{dates[i]}-#{dates[i+1]}"
  #           Delayed::Worker.logger.debug "--------- pulling Rental ---------"
  #           results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSO,PNC,TOM,WDN),(MatrixModifiedDT=#{recdate}),(PropertyType=REN)",:limit => 60000)
  #           Delayed::Worker.logger.debug "--------- importing Rental ---------"
  #           results.each do |row|
  #             Delayed::Worker.logger.debug "##row: #{row}"
  #             save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #           end
  #         rescue Exception => e
  #           Delayed::Worker.logger.debug "No record found #{e.message}"
  #         end

  #         # begin
  #         #   recdate = "#{dates[i]}-#{dates[i+1]}"
  #         #   Delayed::Worker.logger.debug "--------- pulling INC ---------"
  #         #   results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSO,PNC,TOM,WDN),(MatrixModifiedDT=#{recdate}),(PropertyType=INC)",:limit => 60000)
  #         #   Delayed::Worker.logger.debug "--------- importing INC ---------"
  #         #   results.each do |row|
  #         #     Delayed::Worker.logger.debug "##row: #{row}"
  #         #     save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         #   end
  #         # rescue Exception => e
  #         #   Delayed::Worker.logger.debug "No record found #{e.message}"
  #         # end

  #         # begin
  #         #   recdate = "#{dates[i]}-#{dates[i+1]}"
  #         #   Delayed::Worker.logger.debug "--------- pulling COM ---------"
  #         #   results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSO,PNC,TOM,WDN),(MatrixModifiedDT=#{recdate}),(PropertyType=COM)",:limit => 60000)
  #         #   Delayed::Worker.logger.debug "--------- importing COM ---------"
  #         #   results.each do |row|
  #         #     Delayed::Worker.logger.debug "##row: #{row}"
  #         #     save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         #   end
  #         # rescue Exception => e
  #         #   Delayed::Worker.logger.debug "No record found #{e.message}"
  #         # end

  #         # begin
  #         #   recdate = "#{dates[i]}-#{dates[i+1]}"
  #         #   Delayed::Worker.logger.debug "--------- pulling VAC ---------"
  #         #   results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSO,PNC,TOM,WDN),(MatrixModifiedDT=#{recdate}),(PropertyType=VAC)",:limit => 60000)
  #         #   Delayed::Worker.logger.debug "--------- importing VAC ---------"
  #         #   results.each do |row|
  #         #     Delayed::Worker.logger.debug "##row: #{row}"
  #         #     save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         #   end
  #         # rescue Exception => e
  #         #   Delayed::Worker.logger.debug "No record found #{e.message}"
  #         # end


  #       end


  #     }



  # end

  def parse_rets()

    begin

      divisor = 720
      # how many minute in the past to grab the data
      offset = 720
      time_zone_diff = 840

      timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
      
      new_offset = offset
      step = (offset / divisor) + 1

      step.downto(1) do |increment|

        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S")

        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S")
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S")
        end

        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        begin
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(MatrixModifiedDT=#{date_range}),(PropertyType=REN)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing Rental ---------"
          results.each do |row|
             Delayed::Worker.logger.debug "##row: #{row}"
             save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling INC ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(MatrixModifiedDT=#{date_range}),(PropertyType=INC)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing INC ---------"
          results.each do |row|
             Delayed::Worker.logger.debug "##row: #{row}"
             save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling COM ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(MatrixModifiedDT=#{date_range}),(PropertyType=COM)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing COM ---------"
          results.each do |row|
             Delayed::Worker.logger.debug "##row: #{row}"
             save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling VAC ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(MatrixModifiedDT=#{date_range}),(PropertyType=VAC)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing VAC ---------"
          results.each do |row|
             Delayed::Worker.logger.debug "##row: #{row}"
             save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        new_offset = new_offset - divisor

      end

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end

  def parse_rets_by_interval(t=nil,minutes=nil)

    begin
      
      divisor = 720
      offset = minutes ? minutes : 0
      time_zone_diff = 840

      status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
              
      timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
      timenow = t if !t.blank?
      new_offset = offset
      step = (offset / divisor) + 1

      step.downto(1) do |increment|
        
        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
        end
        
        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        begin
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          # results = client.find(:all,:search_type => "Property",:class => "A",:query => "(ModificationTimestamp=#{date_range})",:limit => 60000,:standard_name => "true")
          
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=REN)",:limit => 60000)
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MLSNumber=T2810125,O5423910,T2802779,O5415414,O5414036,O5414025,O5412001,O5410515,T2810125,O5423910,T2802779,T2802316,O5415414,O5414025,O5412001,O5410515,O5404924,T2802808,O5416365,O5416145,O5412695}),(PropertyType=REN)",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Rental ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling INC ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=INC)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing INC ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling COM ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=COM)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing COM ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling VAC ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=VAC)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing VAC ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        new_offset = new_offset - divisor

      end

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end

  def parse_rets_by_specified_period(start_time,end_time)

    begin
      
      divisor = 720
      end_time = Time.parse(end_time)
      start_time = Time.parse(start_time)
      Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
    
      offset = (end_time - start_time).to_i / 60
      time_zone_diff = 840
      
      status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
      active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD,S,RL'
      # active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD'
      # active_list = 'S,RL'
              
      timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
      new_offset = offset
      
      Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
      step = (offset / divisor) + 1

      step.downto(1) do |increment|
        
        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
        end
        
        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        begin
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          # results = client.find(:all,:search_type => "Property",:class => "A",:query => "(ModificationTimestamp=#{date_range})",:limit => 60000,:standard_name => "true")
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MLSNumber=V4705554),(PropertyType=RES)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=REN)",:limit => 60000)
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MLSNumber=O5414025,O5412001,O5410515,S4823651}),(PropertyType=REN)",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Rental ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling INC ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=INC)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing INC ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling COM ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=COM)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing COM ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          Delayed::Worker.logger.debug "--------- pulling VAC ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=VAC)",:limit => 60000)
          Delayed::Worker.logger.debug "--------- importing VAC ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        new_offset = new_offset - divisor

      end

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end

  def parse_rets2_by_interval(t=nil,minutes=nil)

    begin
      
      divisor = 720
      offset = minutes ? minutes : 0
      time_zone_diff = 840
              
      timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
      timenow = t if !t.blank?
      new_offset = offset
      step = (offset / divisor) + 1
      kls = ""

      step.downto(1) do |increment|
        
        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
        end
        
        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        begin
          kls = "A"
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          
          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)
          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_3=743636)",:limit => 60000)
          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "B"
          Delayed::Worker.logger.debug "--------- pulling Multi Family ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end
# 
        begin
          kls = "C"
          Delayed::Worker.logger.debug "--------- pulling Lot ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          
          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Lot ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "D"
          Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

           a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Rental ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "F"
          Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

           a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "G"
          Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        

        new_offset = new_offset - divisor

      end

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end

  def parse_rets2_by_specified_period(start_time,end_time)

    begin
      
      divisor = 720
      end_time = Time.parse(end_time)
      start_time = Time.parse(start_time)
      Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
    
      offset = (end_time - start_time).to_i / 60
      time_zone_diff = 840
      
      status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
      # active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD,S,RL'
      # active_list = '1536QBU2R6W9,153ZGBGME2VL,154185HUKIZV,1541KR8KSAEF,1536QBVYCWQM,17VIFMCDAOH1'
      active_list = '1536QBU2R0NU,1536QBU2RECN,1536QBU2R99V,1536QBU2RBV2,1536QBU2RJIZ,1536QBU2RGVC,PWC_1585BVP21LL9,153ZGBGME03J,153ZGBGMEA1X,153ZGBGME5B4,153ZGBGME7OR,153ZGBGMEEZX,153ZGBGMECIZ,PWC_1585BVQ3ZK79,154185HUKG4P,154185HUKRJ5,154185HUKLJL,154185HUKO3H,154185HUKW65,154185HUKTQZ,PWC_1585BVQ3ZV4F,1541KR8KS7J3,1541KR8KSHYN,1541KR8KSCW1,1541KR8KSFFT,1541KR8KSMW3,1541KR8KSKAS,PWC_1585BVQ405ER,1536QBVYCRBX,1536QBVYD77N,1536QBVYD4EO,1536QBVYD1XY,1536QBVYCZ9S,1536QBVYCU09,PWC_17VIFQYZN576,17VIFMCDAI8L,17VIFMCDB2R0,17VIFMCDAYNR,17VIFMCDAVGA,17VIFMCDARTE,17VIFMCDAL3R,PWC_17VIFQYZPQLV'
      # active_list = 'A,B,CK,X,LP,N,PA,I,R,RU,U,WD'
      # active_list = 'S,RL'
              
      timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
      new_offset = offset
      
      Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
      step = (offset / divisor) + 1

      step.downto(1) do |increment|
        
        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
        end
        
        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        # @lookup_type_enum2.try(:[],:status_options).each do |j,k|
        #   Delayed::Worker.logger.debug "j,k #{j},#{k}"
        # end

        begin
          kls = "A"
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          
          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)
          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_3=743636)",:limit => 60000)
          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range}),(LIST_15=#{active_list})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "B"
          Delayed::Worker.logger.debug "--------- pulling Multi Family ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range}),(LIST_15=#{active_list})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end
# 
        begin
          kls = "C"
          Delayed::Worker.logger.debug "--------- pulling Lot ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          
          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range}),(LIST_15=#{active_list})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Lot ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "D"
          Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

           a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range}),(LIST_15=#{active_list})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Rental ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "F"
          Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

           a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range}),(LIST_15=#{active_list})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        begin
          kls = "G"
          Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)

          a = set_up_metadata_hash(kls)
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          results = client.find(:all,:search_type => "Property",:class => kls,:query => "(LIST_87=#{date_range}),(LIST_15=#{active_list})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          results.each do |row|
            Delayed::Worker.logger.debug "##row: #{row}"
            save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        new_offset = new_offset - divisor

      end

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end
  
  def parse_rets3_by_interval(t=nil,minutes=nil)

    export_file = 'iguazu.xls'
    File.delete(export_file) if File.exist?(export_file)

    Spreadsheet.client_encoding = 'UTF-8'

    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet :name => "raw"

    begin
      

      divisor = 720
      offset = minutes ? minutes : 0
      time_zone_diff = 3700
              
      timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
      timenow = t if !t.blank?
      new_offset = offset
      step = (offset / divisor) + 1

      step.downto(1) do |increment|
        
        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
        end
        
        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        begin
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          
          a = set_up_metadata_hash("A")
          Delayed::Worker.logger.debug "a~~: #{a}"
          set_up_metadata_fields(a)

          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_3=743613)",:limit => 60000)
          results = client.find(:all,:search_type => "Property",:class => "A",:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"

          res =  self.client.http_post("http://retsgw.flexmls.com/rets2_1/GetMetadata",
            { "Type"   => "METADATA-TABLE",
            "ID"     => "0" })
          str_metadata = res.body.squish
            
          doc = Nokogiri::XML(str_metadata)

          res2 =  self.client.http_post("http://retsgw.flexmls.com/rets2_1/GetMetadata",
          { "Type"   => "METADATA-LOOKUP_TYPE",
            "ID"     => "0" })
          str_metadata2 = res2.body.squish

          doc2 = Nokogiri::XML(str_metadata2)
          
          ix = 0
          i = 0  
          results.each do |row|
            
             
            
            puts "LIST_0: #{row["LIST_0"]}"
            group_name = "Record ##{i+1}, LIST_1: #{row["LIST_1"]}"
            sheet1.row(ix).push group_name
            Delayed::Worker.logger.debug "##row: #{row}"
            ix = ix + 1

            row.each do |x,y|
              
              system_name = x.to_s
              long_name = ""
              lookup_name =  ""
              lookup_value = ""
              actual_value = ""
              composed_lu_final_result = ""
                  
              things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Resource='Property' and @Class='A']/Field/SystemName[text()='#{x}']")
              if (things && (things.length > 0))
                long_name = things[0].parent.at("./LongName").text
                lookup_name = things[0].parent.at("./LookupName").text
                actual_value = y
                lookup_value = y

                # GFLU20130226165731591186000000
                things2 = doc2.xpath("//RETS/METADATA/METADATA-LOOKUP_TYPE[@Lookup='#{lookup_name}']/LookupType")
                if (things2 && (things2.length > 0))
                  multi_items = lookup_value.split(",")
                  composed_lu_val = multi_items.inject([]) do |memo,e|
                    lu_lv = things2.at("./Value[text()='#{e}']").parent.at("./LongValue")
                    memo << lu_lv
                    memo
                  end
                  composed_lu_final_result = composed_lu_val.join(",")
                else
                  composed_lu_final_result = ""

                end

                

              end

              puts ">>>>system_name: #{system_name}^ long_name: #{long_name}^ value: #{actual_value}^ lookup_name: #{lookup_name}^ composed_lu_final_result: #{composed_lu_final_result}"
              sheet1.row(ix).push system_name.dup.blank? ? "null" : system_name.dup 
              sheet1.row(ix).push long_name.dup.blank? ? "null" : long_name.dup 
              sheet1.row(ix).push actual_value.dup.blank? ? "null" : actual_value.dup 
              sheet1.row(ix).push lookup_name.dup.blank? ? "null" : lookup_name.dup 
              sheet1.row(ix).push composed_lu_final_result.dup.blank? ? "null" : composed_lu_final_result.dup 
              ix = ix + 1
              

            end
            i += 1


            
                
            
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        # begin
          # Delayed::Worker.logger.debug "--------- pulling Multi Family ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
          # a = set_up_metadata_hash("B")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "B",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Lot ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
#             
          # a = set_up_metadata_hash("C")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "C",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Lot ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
           # a = set_up_metadata_hash("D")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Rental ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
           # a = set_up_metadata_hash("F")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "F",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
          # a = set_up_metadata_hash("G")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "G",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end

        

        new_offset = new_offset - divisor

      end

      book.write export_file 

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end
  
  
  
  def parse_rets4_by_interval(t=nil,minutes=nil)

    export_file = 'evidence.xls'
    File.delete(export_file) if File.exist?(export_file)

    Spreadsheet.client_encoding = 'UTF-8'

    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet :name => "raw"

    begin
      

      divisor = 720
      offset = minutes ? minutes : 0
      time_zone_diff = 840
              
      timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
      timenow = t if !t.blank?
      new_offset = offset
      step = (offset / divisor) + 1
      status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'

      step.downto(1) do |increment|
        
        start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
        if (new_offset - divisor) >= 0
          end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        else
           end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
        end
        
        Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
        date_range = "#{start_time}-#{end_time}"

        begin
          Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
          # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
          
          # a = set_up_metadata_hash("D")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)

          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_3=743613)",:limit => 60000)
          
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MLSNumber=T2810125,O5423910,T2802779,O5415414,O5414036,O5414025,O5412001,O5410515,T2810125,O5423910,T2802779,T2802316,O5415414,O5414025,O5412001,O5410515,O5404924,T2802808,O5416365,O5416145,O5412695),(PropertyType=REN)",:limit => 60000)
          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_87=#{date_range})",:limit => 60000)
          
          Delayed::Worker.logger.debug "--------- importing Single Family ---------"

          res =  self.client.http_post(self.client.capability_url("GetMetadata"),
            { "Type"   => "METADATA-TABLE",
            "ID"     => "0" })
          str_metadata = res.body.squish
            
          doc = Nokogiri::XML(str_metadata)

          res2 =  self.client.http_post(self.client.capability_url("GetMetadata"),
          { "Type"   => "METADATA-LOOKUP_TYPE",
            "ID"     => "0" })
          str_metadata2 = res2.body.squish

          doc2 = Nokogiri::XML(str_metadata2)
          
          ix = 0
          i = 0  
          results.each do |row|
            
             
            
            # puts "LIST_0: #{row["LIST_0"]}"
            group_name = "Record ##{i+1}, MLSNumber #{row["MLSNumber"]}"
            sheet1.row(ix).push group_name
            Delayed::Worker.logger.debug "##row: #{row}"
            ix = ix + 1

            row.each do |x,y|
              
              system_name = x.to_s
              long_name = ""
              lookup_name =  ""
              lookup_value = ""
              actual_value = ""
              composed_lu_final_result = ""
                  
              things = doc.xpath("//RETS/METADATA/METADATA-TABLE[@Resource='Property' and @Class='Listing']/Field/SystemName[text()='#{x}']")
              if (things && (things.length > 0))
                long_name = things[0].parent.at("./LongName").text
                lookup_name = things[0].parent.at("./LookupName").text
                actual_value = y
                lookup_value = y

                # GFLU20130226165731591186000000
                things2 = doc2.xpath("//RETS/METADATA/METADATA-LOOKUP_TYPE[@Lookup='#{lookup_name}']/LookupType")
                if (things2 && (things2.length > 0))
                  multi_items = lookup_value.split(",")
                  composed_lu_val = multi_items.inject([]) do |memo,e|
                    lu_lv = things2.at("./Value[text()='#{e}']").parent.at("./LongValue")
                    memo << lu_lv
                    memo
                  end
                  composed_lu_final_result = composed_lu_val.join(",")
                else
                  composed_lu_final_result = ""

                end

                

              end

              puts ">>>>system_name: #{system_name}^ long_name: #{long_name}^ value: #{actual_value}^ lookup_name: #{lookup_name}^ composed_lu_final_result: #{composed_lu_final_result}"
              sheet1.row(ix).push system_name.dup.blank? ? "null" : system_name.dup 
              sheet1.row(ix).push long_name.dup.blank? ? "null" : long_name.dup 
              sheet1.row(ix).push actual_value.dup.blank? ? "null" : actual_value.dup 
              sheet1.row(ix).push lookup_name.dup.blank? ? "null" : lookup_name.dup 
              sheet1.row(ix).push composed_lu_final_result.dup.blank? ? "null" : composed_lu_final_result.dup 
              ix = ix + 1
              

            end
            i += 1


            
                
            
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          end
        rescue Exception => e
          Delayed::Worker.logger.debug "No record found #{e.message}"
        end

        # begin
          # Delayed::Worker.logger.debug "--------- pulling Multi Family ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
          # a = set_up_metadata_hash("B")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "B",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Lot ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
#             
          # a = set_up_metadata_hash("C")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "C",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Lot ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Rental ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
           # a = set_up_metadata_hash("D")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "D",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Rental ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
           # a = set_up_metadata_hash("F")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "F",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end
# 
        # begin
          # Delayed::Worker.logger.debug "--------- pulling Commercial ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
# 
          # a = set_up_metadata_hash("G")
          # Delayed::Worker.logger.debug "a~~: #{a}"
          # set_up_metadata_fields(a)
# 
          # results = client.find(:all,:search_type => "Property",:class => "G",:query => "(LIST_87=#{date_range})",:limit => 60000)
#             
          # Delayed::Worker.logger.debug "--------- importing Multi Family ---------"
          # results.each do |row|
            # Delayed::Worker.logger.debug "##row: #{row}"
            # save_rets2(row, @data_mapping2, DATA_ENUMS2, @lookup_type_enum2,DATA_CATEGORIES2 )
          # end
        # rescue Exception => e
          # Delayed::Worker.logger.debug "No record found #{e.message}"
        # end

        

        new_offset = new_offset - divisor

      end

      book.write export_file 

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end
  
  # def parse_rets_by_specified_period(start_time,end_time)

  #   begin
      
  #     divisor = 720
  #     end_time = Time.parse(end_time)
  #     start_time = Time.parse(start_time)
  #     Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
    
  #     offset = (end_time - start_time).to_i / 60
  #     time_zone_diff = 840
      
  #     status_list = 'ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN'
              
  #     timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
  #     new_offset = offset
      
  #     Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
  #     step = (offset / divisor) + 1

  #     step.downto(1) do |increment|
        
  #       start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
        
  #       if (new_offset - divisor) >= 0
  #         end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
  #       else
  #          end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
  #       end
        
  #       Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
  #       date_range = "#{start_time}-#{end_time}"

  #       begin
  #         Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
  #         results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=RES)",:limit => 60000)
  #         Delayed::Worker.logger.debug "--------- importing Single Family ---------"
  #         results.each do |row|
  #           Delayed::Worker.logger.debug "##row: #{row}"
  #           save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         end
  #       rescue Exception => e
  #         Delayed::Worker.logger.debug "No record found #{e.message}"
  #       end

  #       begin
  #         Delayed::Worker.logger.debug "--------- pulling Rental ---------"
  #         results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=REN)",:limit => 60000)
  #         Delayed::Worker.logger.debug "--------- importing Rental ---------"
  #         results.each do |row|
  #           Delayed::Worker.logger.debug "##row: #{row}"
  #           save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         end
  #       rescue Exception => e
  #         Delayed::Worker.logger.debug "No record found #{e.message}"
  #       end

  #       begin
  #         Delayed::Worker.logger.debug "--------- pulling INC ---------"
  #         results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=INC)",:limit => 60000)
  #         Delayed::Worker.logger.debug "--------- importing INC ---------"
  #         results.each do |row|
  #           Delayed::Worker.logger.debug "##row: #{row}"
  #           save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         end
  #       rescue Exception => e
  #         Delayed::Worker.logger.debug "No record found #{e.message}"
  #       end

  #       begin
  #         Delayed::Worker.logger.debug "--------- pulling COM ---------"
  #         results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=COM)",:limit => 60000)
  #         Delayed::Worker.logger.debug "--------- importing COM ---------"
  #         results.each do |row|
  #           Delayed::Worker.logger.debug "##row: #{row}"
  #           save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         end
  #       rescue Exception => e
  #         Delayed::Worker.logger.debug "No record found #{e.message}"
  #       end

  #       begin
  #         Delayed::Worker.logger.debug "--------- pulling VAC ---------"
  #         results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{date_range}),(PropertyType=VAC)",:limit => 60000)
  #         Delayed::Worker.logger.debug "--------- importing VAC ---------"
  #         results.each do |row|
  #           Delayed::Worker.logger.debug "##row: #{row}"
  #           save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
  #         end
  #       rescue Exception => e
  #         Delayed::Worker.logger.debug "No record found #{e.message}"
  #       end

  #       new_offset = new_offset - divisor

  #     end

  #   rescue Exception => e
  #     Delayed::Worker.logger.debug "No record found #{e.message}"
  #   end

  # end


  def parse_rets_test()

    begin

      
      # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
      timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
      timerange = "2014-01-01-2014-06-11"
      sd = Date.parse('2015-09-03')
      ed = Date.parse('2015-09-03')
      Delayed::Worker.logger.debug "--------- pulling single family ---------"
      ed.downto(sd) { |date|

        Delayed::Worker.logger.debug "---------  #{date} ---------"
        date1 = date.strftime("%Y-%m-%dT00:00:00")
        date2 = date.strftime("%Y-%m-%dT06:00:00")
        date3 = date.strftime("%Y-%m-%dT12:00:00")
        date4 = date.strftime("%Y-%m-%dT18:00:00")
        tomorrow = date + 1.day
        date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
        dates = []
        dates = [date1,date2,date3,date4,date5]
        # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)

        # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
        status_list = 'ACT,AWC,EXP,I,LSO,PNC,TOM,WDN,SLD,RNT,LSE'
        # status_list = 'ACT,AWC,EXP,I,LSO,PNC,TOM,WDN'
        # status_list = "SLD,RNT,LSE"

        for i in 0..0
          Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"

          begin
            recdate = "#{dates[i]}-#{dates[i+1]}"
            Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{recdate}),(PropertyType=RES)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MLSNumber=T2293790,O4680342),(PropertyType=RES)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing Single Family ---------"
            results.each do |row|
              Delayed::Worker.logger.debug "##row: #{row}"
              save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
              # import_waterfront(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
            end
          rescue Exception => e
            Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # Delayed::Worker.logger.debug "--------- pulling Rental ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{recdate}),(PropertyType=REN)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing Rental ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
              # import_waterfront(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
            # end
          # rescue Exception => e
            # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # Delayed::Worker.logger.debug "--------- pulling INC ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{recdate}),(PropertyType=INC)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing INC ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
              # import_waterfront(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
            # end
          # rescue Exception => e
            # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # Delayed::Worker.logger.debug "--------- pulling COM ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{recdate}),(PropertyType=COM)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing COM ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
              # import_waterfront(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
            # end
          # rescue Exception => e
            # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
          # begin
            # recdate = "#{dates[i]}-#{dates[i+1]}"
            # Delayed::Worker.logger.debug "--------- pulling VAC ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=#{status_list}),(MatrixModifiedDT=#{recdate}),(PropertyType=VAC)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing VAC ---------"
            # results.each do |row|
              # Delayed::Worker.logger.debug "##row: #{row}"
              # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
              # import_waterfront(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES)
            # end
          # rescue Exception => e
            # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end


        end


      }

    rescue Exception => e
      Delayed::Worker.logger.debug "No record found #{e.message}"
    end

  end

  # def parse_rets()
#
    # begin
       # if Rails.env.development?
        # offset = 1950
      # else
        # offset = 1560
      # end
#
      # timenow = (Time.now - offset.minutes).strftime("%Y-%m-%dT%H:%M:%S")
# #
      # # begin
        # # Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
        # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=O5175461,U7591095)",:limit => 60000)
        # # Delayed::Worker.logger.debug "--------- importing Single Family ---------"
        # # results.each do |row|
          # # Delayed::Worker.logger.debug "##row: #{row}"
          # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
        # # end
      # # rescue Exception => e
        # # Delayed::Worker.logger.debug "No record found #{e.message}"
      # # end
#
      # # ML Number          Address            List Price                 Close Price
# # A3975646             8009 FAIRHAVEN GLN   455000            405000
# # A3974247                  1640 STANFORD LN         1495000           1425000
# # A3975983                   5311 SUNRISE LN A         2199000            2020000
# #
# # ML Number                 Address                List Price    Close Price
# # A3987251            2232 BAHIA VISTA ST A-1     1050       1025
# # A3987182                  1135 LONGFELLOW RD #      1200       1200
# # A3986891                  1648 BONITA BLUFF CT      1350      1300
#
       # begin
         # Delayed::Worker.logger.debug "--------- pulling Single Family ---------"
         # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(StatusChangeTimestamp=#{timenow}+),(PropertyType=RES)",:limit => 60000)
         # Delayed::Worker.logger.debug "--------- importing Single Family ---------"
         # results.each do |row|
           # Delayed::Worker.logger.debug "##row: #{row}"
           # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
         # end
       # rescue Exception => e
         # Delayed::Worker.logger.debug "No record found #{e.message}"
       # end
#
       # begin
         # Delayed::Worker.logger.debug "--------- pulling Rental ---------"
         # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=ACT,AWC,EXP,I,LSE,LSO,PNC,RNT,SLD,TOM,WDN),(StatusChangeTimestamp=#{timenow}+),(PropertyType=REN)",:limit => 60000)
         # Delayed::Worker.logger.debug "--------- importing Rental ---------"
         # results.each do |row|
           # Delayed::Worker.logger.debug "##row: #{row}"
           # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS,DATA_CATEGORIES )
         # end
       # rescue Exception => e
         # Delayed::Worker.logger.debug "No record found #{e.message}"
       # end
#
    # rescue Exception => e
      # Delayed::Worker.logger.debug "No record found #{e.message}"
    # end
#
  # end

  def parse_special_raw(data, filename)
    CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
      row['PropType'] ||= filename.try(:slice, -7, 3)
      correct_property(row, DATA_MAPPING, DATA_ENUMS)
    end
  end

  def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
    # exectute mappings and retrieve raw data
    Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping null?: #{mapping.nil?}" 

    if mapping
      Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
      # original = original.to_hash.with_case_insensitive_access
      Delayed::Worker.logger.debug "MLSNUM: #{original}"
      raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|

        Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
        method = Provider.lookup_or(*method) if method.is_a?(Array)
         Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"

        new_value = method.is_a?(Proc) ? method.call(original) : original[method]

        Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
        result[attr] = new_value.missing? ? nil : new_value
        result
      end
      Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
      attrs = raw.inject({}) do |result, (attr, string)|
        Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
        result[attr] =




          if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
            if attr == :dom || attr == :cdom
              "0"
            else
              nil
            end
          elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
            Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
            Provider.lookup(enums[attr], attr).call(raw)
          elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
            Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
            Provider.lookup_type(lookup_types[attr], attr).call(raw)

          elsif (string.is_a?(BigDecimal))

            string


          else
            if attr == :sub_category
              nil
            elsif attr == :tax_id
              string
            else
              Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              Property.parse(attr, string)
            end

          end
          # if string.try(:downcase).in?(missing_values)
            # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
            # if attr == :dom || attr == :cdom
              # "0"
            # else
              # nil
            # end
          # elsif enums.has_key?(attr)
            # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
            # Provider.lookup(enums[attr], attr).call(raw)
          # else
            # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
            # Property.parse(attr, string)
          # end
        result
      end
    else
      raw = {}
      Delayed::Worker.logger.debug "original: #{original}"
      # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
      attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      Delayed::Worker.logger.debug "attrs: #{attrs}"
    end
    info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
    info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
    info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
    Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
    Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
    source_hash = { :source => self.source, :mls_source => 'mfrmls' } 
    property = market.import8(source_hash, attrs, raw, update_only)
    # allprops = Property.where(["id is not null"])
    # allprops.each do |ap|
      # Delayed::Worker.logger.debug "ap: #{ap.id}"
    # end
    self.errors[current_filename] ||= {}
    Delayed::Worker.logger.debug "~~ before insert id ~~"
    if property.nil? || property.is_a?(String)
      self.errors[current_filename][info] = property || 'No match' unless update_only
    else
      self.results[current_filename] ||= 0
      self.results[current_filename] += 1
      Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
      self.ids << property.id
      unless property.errors.empty?
        self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        Delayed::Worker.logger.debug "~~Property Error: #{property.errors}"
      end
    end
  end

  def save_rets2(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
    # exectute mappings and retrieve raw data
    Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping null?: #{mapping.nil?}" 

    if mapping
      Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
      # original = original.to_hash.with_case_insensitive_access
      Delayed::Worker.logger.debug "MLSNUM: #{original}"
      raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|

        Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
        method = Provider.lookup_or(*method) if method.is_a?(Array)
         Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"

        new_value = method.is_a?(Proc) ? method.call(original) : original[method]

        Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
        result[attr] = new_value.missing? ? nil : new_value
        result
      end
      Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
      attrs = raw.inject({}) do |result, (attr, string)|
        Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
        result[attr] =




          if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
            if attr == :dom || attr == :cdom
              "0"
            else
              nil
            end
          elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
            Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
            Provider.lookup(enums[attr], attr).call(raw)
          elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
            Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
            Provider.lookup_type(lookup_types[attr], attr).call(raw)

          elsif (string.is_a?(BigDecimal))

            string


          else
            if attr == :sub_category
              nil
            elsif attr == :tax_id
              string
            elsif attr == :square_feet
              string.to_i
            elsif attr == :lot_square_feet
              string.to_i
            else
              Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              Property.parse(attr, string)
            end

          end
          # if string.try(:downcase).in?(missing_values)
            # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
            # if attr == :dom || attr == :cdom
              # "0"
            # else
              # nil
            # end
          # elsif enums.has_key?(attr)
            # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
            # Provider.lookup(enums[attr], attr).call(raw)
          # else
            # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
            # Property.parse(attr, string)
          # end
        result
      end
    else
      raw = {}
      Delayed::Worker.logger.debug "original: #{original}"
      # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
      attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      Delayed::Worker.logger.debug "attrs: #{attrs}"
    end
    info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
    info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
    info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
    Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
    Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
    
    source_hash = { :source => self.source, :mls_source => 'flexmls' } 
    
    property = market.import8(source_hash, attrs, raw, update_only)
    # allprops = Property.where(["id is not null"])
    # allprops.each do |ap|
      # Delayed::Worker.logger.debug "ap: #{ap.id}"
    # end
    self.errors[current_filename] ||= {}
    Delayed::Worker.logger.debug "~~ before insert id ~~"
    if property.nil? || property.is_a?(String)
      self.errors[current_filename][info] = property || 'No match' unless update_only
    else
      self.results[current_filename] ||= 0
      self.results[current_filename] += 1
      Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
      self.ids << property.id
      unless property.errors.empty?
        self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        Delayed::Worker.logger.debug "~~Property Error: #{property.errors}"
      end
    end
  end
  
  def import_waterfront(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
    # exectute mappings and retrieve raw data
    if mapping
      Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
      # original = original.to_hash.with_case_insensitive_access
      Delayed::Worker.logger.debug "MLSNUM: #{original}"
      raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|

        Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
        method = Provider.lookup_or(*method) if method.is_a?(Array)
         Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"

        new_value = method.is_a?(Proc) ? method.call(original) : original[method]

        Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
        result[attr] = new_value.missing? ? nil : new_value
        result
      end
      Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
      attrs = raw.inject({}) do |result, (attr, string)|
        Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
        result[attr] =




          if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
            if attr == :dom || attr == :cdom
              "0"
            else
              nil
            end
          elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
            Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
            Provider.lookup(enums[attr], attr).call(raw)
          elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
            Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
            Provider.lookup_type(lookup_types[attr], attr).call(raw)

          elsif (string.is_a?(BigDecimal))

            string


          else
            if attr == :sub_category
              nil
            elsif attr == :tax_id
              string
            else
              Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              Property.parse(attr, string)
            end

          end
          # if string.try(:downcase).in?(missing_values)
            # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
            # if attr == :dom || attr == :cdom
              # "0"
            # else
              # nil
            # end
          # elsif enums.has_key?(attr)
            # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
            # Provider.lookup(enums[attr], attr).call(raw)
          # else
            # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
            # Property.parse(attr, string)
          # end
        result
      end
    else
      raw = {}
      Delayed::Worker.logger.debug "original: #{original}"
      # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
      attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
      Delayed::Worker.logger.debug "attrs: #{attrs}"
    end
    info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
    info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
    info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
    Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
    Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
    property = market.import_waterfront(self.source, attrs, raw, update_only)
    
  end

end
