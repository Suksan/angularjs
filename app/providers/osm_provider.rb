class OsmProvider
  require 'csv'

  attr_accessor :market, :agent, :results, :errors, :current_filename, :listeners, :ids, :source, :rows_affected, :file_names

  def initialize(market)
    self.market = market
        
    @agent = Mechanize.new { |ag|
      ag.read_timeout = 20
      ag.open_timeout = 20
      ag.idle_timeout = 0.9
      ag.retry_change_requests = true
    }
    
    @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    self.results = {}
    self.errors = {}
    self.listeners = []
    self.ids = []
    self.file_names = []
    self.source = 'MLS'
  end

  def upload(source, files, overrides)
    self.source = source
    self.import(files)
    self.current_filename = 'overrides'
    for override in overrides
      save_property(override)
    end
    notify_listeners
  end

  def import(files)
    
    # Delayed::Worker.logger.debug "%%files #{files}"
    [*files].each do |file|
      
      # Delayed::Worker.logger.debug "##file #{file}"
      
      if file.is_a?(Hash)
        # Delayed::Worker.logger.debug "In case it's a hash"
        filename = file[:name]
        file = File.open(file[:path])
        content = file.read
        file.close
        
      elsif file.is_a?(Mechanize::File) 
        # Delayed::Worker.logger.debug "In case it's a Mechanize file"
        # Delayed::Worker.logger.debug "Saving file name #{file.filename}"
        file.save_as(Rails.root.join('tmp', file.filename))
        filename = file.filename
        content = file.content
      end
      
      # Delayed::Worker.logger.debug "## content #{content}"
      # Delayed::Worker.logger.debug "saving filename #{filename}"
      self.current_filename = filename

      # ensure content encoding is UTF-8 and replace double quotes
      content.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
      content.encode!('UTF-8', 'UTF-16')
      content.gsub!('"', "'")

      # process file
      #@@puts "- parsing '#{filename}' with #{content.lines.count} lines"
      result = parse(content, filename)
      
      self.file_names << filename
      # execute listeners to notify of progress
      notify_listeners
    end
  end
  
  def add_dom(days)
    n = 0
    Delayed::Worker.logger.debug "on market count #{self.market.properties.on_market.count}"
    
    
    dud = market.properties.on_market_with_dom_update.order("dom_updated_at desc").select("dom_updated_at").first rescue nil
    
    if dud
     Delayed::Worker.logger.debug "dud dom_updated_at: #{dud.dom_updated_at}"
     Delayed::Worker.logger.debug "date time now: #{DateTime.now}"
     Delayed::Worker.logger.debug "date time now - 1: #{DateTime.now - days.day}"
     Delayed::Worker.logger.debug "date comparison: #{(dud.dom_updated_at.to_date == (DateTime.now - days.day).to_date)}"
    end
    # if dud && (dud.dom_updated_at.to_date == (DateTime.now - days.day).to_date)
      # Delayed::Worker.logger.debug "normal increment"
      # n = self.market.properties.on_market.update_all ["cdom = cdom + ? , dom = dom + ?, dom_updated_at = ?", days , days, DateTime.now], 
                                       # ["((listing_date IS NOT NULL) and ((cdom IS NOT NULL) and (dom  IS NOT NULL)))"]
#     
    # else
      Delayed::Worker.logger.debug "reconcile update"
      n = self.market.properties.on_market.update_all ["cdom = cdom+ DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL dom - 1 DAY)) ) 
                          , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          ["status in ('active','pending') and ((listing_date is not null) and ((cdom is not null) and (dom is not null)))"]
    # end                           
    # Avatar.update_all ['migrated_at = ?', Time.now.utc], ['migrated_at > ?', 1.week.ago]
    # DateTime.now
    # Service.where(["code = ? AND ((daily_limit IS NULL) OR (last_date = ? && daily_limit > daily_calls) OR (last_date IS NULL OR last_date < ?))", code, Date.today, Date.today]).first
    
    # .update_all "cdom = cdom + 1 , dom = dom + 1", 
                                      # "status in ('active','pending')"
    Delayed::Worker.logger.debug "rows affected: #{n}"
    self.rows_affected = n
    
    notify_listeners
  end
  
  def grab(days)
    if login
      Delayed::Worker.logger.debug "~~~~~~~ Fetching data for grab ~~~~~~~~"
      fetch(days)
      logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

  # methods to be implemented by all market mls providers

  def login
    not_implemented
  end

  def logout
    not_implemented
  end

  def fetch_raw(days)
    not_implemented
  end

  def fetch_hotsheet(days)
    not_implemented
  end

  def parse_raw(data, filename)
    not_implemented
  end

  def parse_hotsheet(data, filename)
    not_implemented
  end

  def mls_url_for(attrs)
    not_implemented
  end

  def tax_url_for(attrs)
    not_implemented 
  end

  def deed_url_for(attrs)
    not_implemented
  end

  protected

    def parse(data, filename)
      if filename.match /.xls$/i
        parse_raw(data, filename)
      
      else 
        self.errors[filename] = "Unknown file extension"
      end
    end

    def parse_csv(data, filename)
      CSV.parse(data, headers: true) do |row|
        puts "zZZZZzzzzzzZZZ Look up to this #{row}"
        row = row.to_hash.with_indifferent_access
        save_property(row.to_hash.symbolize_keys)
      end
    end

    def fetch(days)
      p "-- fetching raw data"
      fetch_raw(days)
      p "-- fetching hotsheet"
      fetch_hotsheet(days-1)
    end

    def current_url
      @agent.page.uri.try(:to_s)
    end

    def missing_values
      %w{none n/a 0 unk unknown}
    end

    def self.lookup(mapping, property)
      proc {|d| value = d[property]; mapping[value] || value }
    end

    def self.lookup_or(*properties)
      proc {|d| properties.map{|property| d[property] unless d[property].blank? }.compact.first }
    end

    def self.lookup_percent(property)
      proc {|d| d[property] if d[property].try(:match, /%/) }
    end

    def self.lookup_nonpercent(property)
      proc {|d| d[property] unless d[property].try(:match, /%/) }
    end

    def self.hash_from_csv(id)
      result = HashWithCaseInsensitiveAccess.new
      CSV.foreach(Rails.root.join('app', 'providers', self.name.underscore, "#{id}.csv")) do |row|
        next if row.length != 2 
        for key in row.first.split('|')
          result[key] = row.last 
        end
      end
      result
    end

    def html_strip(object, header=false)
      if object.is_a?(Enumerable)
        object.map { |t| html_strip(t.text, header) }
      elsif object.is_a?(String)
        # trim both ends
        object.gsub!(/^[[:space:]|\s|'|"]*|[[:space:]|\s|'|"]*$/, '')
        object.gsub!(/[[:space:]|\s]+/, ' ') if header
        object
      end
    end

    def update_property(original, mapping, enums={})
      save_property(original, mapping, enums, true)
    end

    def save_property(original, mapping=nil, enums={}, county=nil, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        # Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        original = original.to_hash.with_case_insensitive_access
        # Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          # Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           # Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          # Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        
        # Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          # Delayed::Worker.logger.debug "zZ #{attr} #{attr.class} #{string} #{string.class}"
                    
          result[attr] =
            if (!!string != string) && (! string.is_a?(Date)) && string.try(:downcase).in?(missing_values)
              nil 
            elsif  (!!string != string) && (! string.is_a?(Date))  && enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (! string.is_a?(Date)) && !!string == string
              string
            elsif (string.is_a?(Date))
              string
            else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              Property.parse(attr, string)
            end
          result
        end
        
        attrs[:county] = county if attrs && attrs.length > 0
      else
        raw = {}
        # Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        # Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      # Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      # Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import("Appraisal District", attrs, raw, update_only)
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end

    def notify_listeners
      self.listeners.each {|listener| listener.call() }
    end

    def not_implemented
      raise NotImplementedError.new("You must implement this")
    end

end