class Realtracs < Provider

  LOOKUP_TYPE_ENUMS = {
    style_of_building: {
      "AFRAM" => "A-Frame",
      "CAPE" => "Cape Cod",
      "COLON" => "Colonial",
      "CONTP" => "Contemporary",
      "COTTG" => "Cottage",
      "LOG" => "Log",
      "OTHER" => "Other",
      "RANCH" => "Ranch",
      "RUST" => "Rustic",
      "SPFOY" => "Split Foyer",
      "SPLEV" => "Split Level",
      "TRAD" => "Traditional",
      "TUDOR" => "Tudor",
      "VICT" => "Victorian"
    },
    street_direction: {
      "E" => "E",
      "N" => "N",
      "NE" => "NE",
      "NW" => "NW",
      "S" => "S",
      "SE" => "SE",
      "SW" => "SW",
      "W" => "W"
    },
    county: {
      "1" => "Anderson",
      "10" => "Carter",
      "101" => "Christian, KY",
      "102" => "Todd, KY",
      "103" => "North Alabama",
      "104" => "Logan, KY",
      "106" => "Allen, KY",
      "109" => "Barren, KY",
      "11" => "Cheatham",
      "12" => "Chester",
      "122" => "Calloway, KY",
      "13" => "Claiborne",
      "130" => "Clinton, KY",
      "132" => "Cumberland, KY",
      "14" => "Clay",
      "15" => "Cocke",
      "16" => "Coffee",
      "17" => "Crockett",
      "174" => "Lyon, KY",
      "178" => "Marshall, KY",
      "18" => "Cumberland",
      "188" => "Monroe, KY",
      "19" => "Davidson",
      "2" => "Bedford",
      "20" => "Decatur",
      "209" => "Simpson, KY",
      "21" => "Dekalb",
      "212" => "Trigg, KY",
      "215" => "Warren, KY",
      "22" => "Dickson",
      "23" => "Dyer",
      "231" => "Cherokee, AL",
      "238" => "Colbert, AL",
      "24" => "Fayette",
      "246" => "DeKalb, AL",
      "25" => "Fentress",
      "251" => "Franklin, AL",
      "257" => "Jackson, AL",
      "26" => "Franklin",
      "260" => "Lauderdale, AL",
      "261" => "Lawrence, AL",
      "263" => "Limestone, AL",
      "266" => "Madison, AL",
      "269" => "Marshall, AL",
      "27" => "Gibson",
      "273" => "Morgan, AL",
      "28" => "Giles",
      "29" => "Grainger",
      "3" => "Benton",
      "30" => "Greene",
      "31" => "Grundy",
      "32" => "Hamblen",
      "33" => "Hamilton",
      "34" => "Hancock",
      "35" => "Hardeman",
      "36" => "Hardin",
      "37" => "Hawkins",
      "38" => "Haywood",
      "39" => "Henderson",
      "4" => "Bledsoe",
      "40" => "Henry",
      "41" => "Hickman",
      "42" => "Houston",
      "43" => "Humphreys",
      "44" => "Jackson",
      "45" => "Jefferson",
      "46" => "Johnson",
      "47" => "Knox",
      "48" => "Lake",
      "49" => "Lauderdale",
      "5" => "Blount",
      "50" => "Lawrence",
      "51" => "Lewis",
      "52" => "Lincoln",
      "53" => "Loudon",
      "54" => "Mcminn",
      "55" => "Mcnairy",
      "56" => "Macon",
      "57" => "Madison",
      "58" => "Marion",
      "59" => "Marshall",
      "6" => "Bradley",
      "60" => "Maury",
      "61" => "Meigs",
      "62" => "Monroe",
      "63" => "Montgomery",
      "64" => "Moore",
      "65" => "Morgan",
      "66" => "Obion",
      "67" => "Overton",
      "68" => "Perry",
      "69" => "Pickett",
      "7" => "Campbell",
      "70" => "Polk",
      "71" => "Putnam",
      "72" => "Rhea",
      "73" => "Roane",
      "74" => "Robertson",
      "75" => "Rutherford",
      "76" => "Scott",
      "77" => "Sequatchie",
      "78" => "Sevier",
      "79" => "Shelby",
      "8" => "Cannon",
      "80" => "Smith",
      "81" => "Stewart",
      "82" => "Sullivan",
      "83" => "Sumner",
      "84" => "Tipton",
      "85" => "Trousdale",
      "86" => "Unicoi",
      "87" => "Union",
      "88" => "Van Buren",
      "89" => "Warren",
      "9" => "Carroll",
      "90" => "Washington",
      "91" => "Wayne",
      "92" => "Weakley",
      "93" => "White",
      "94" => "Williamson",
      "95" => "Wilson"
    },
    exterior_type: {
      "BRICK" => "All Brick",
      "EXICS" => "Ext Insulating Coating System",
      "FCMNT" => "Fiber Cement",
      "FRAME" => "Frame",
      "INCFR" => "Insulated Concrete Form",
      "LOG" => "Log",
      "OTHER" => "Other",
      "PTBRK" => "Partial Brick",
      "SIDNG" => "Siding",
      "STINP" => "Struct. Insul. Panels",
      "STONE" => "Stone",
      "STUCO" => "Stucco"
    },
    exterior_description: {
      "BARN" => "Barn",
      "GASGR" => "Gas Grill",
      "GOPNR" => "Garage Door Opener",
      "GUEST" => "Guest House",
      "SPRNK" => "Sprinkler",
      "STABL" => "Stable",
      "STOBG" => "Storage Building",
      "STRMS" => "Storm Shelter",
      "TENNI" => "Tennis",
      "DISH" => "Satellite Dish"
    },
    fence_type: {
      "BACK" => "Back",
      "CHAIN" => "Chain",
      "DOGRN" => "Dog Run",
      "FRONT" => "Front Yard",
      "FULL" => "Full Yard",
      "PARTL" => "Partial",
      "PRVCY" => "Privacy",
      "SPLRL" => "Split Rail"
    },
    financing_available: {
      "ASSUM" => "Assumable",
      "FHALO" => "FHA Loan",
      "LEOPT" => "Owner-Lease Option",
      "LNDCT" => "Land Contract",
      "LPRCH" => "Owner-Lease Purchase",
      "OTHER" => "Other Financing",
      "OWNER" => "Owner Financing",
      "USDAR" => "USDA/Rural",
      "VALOA" => "VA Loan"
    },
    floor_type: {
      "BMCRK" => "Bamboo/Cork",
      "CARPT" => "Carpet",
      "CNCRT" => "Concrete",
      "FINWD" => "Finished Wood",
      "LAMIN" => "Laminate",
      "MARBL" => "Marble",
      "OTHER" => "Other",
      "PARQU" => "Parquet",
      "SLATE" => "Slate",
      "TILE" => "Tile",
      "VINYL" => "Vinyl"
    },
    foundation_type: {
      "BMCRK" => "Bamboo/Cork",
      "CARPT" => "Carpet",
      "CNCRT" => "Concrete",
      "FINWD" => "Finished Wood",
      "LAMIN" => "Laminate",
      "MARBL" => "Marble",
      "OTHER" => "Other",
      "PARQU" => "Parquet",
      "SLATE" => "Slate",
      "TILE" => "Tile",
      "VINYL" => "Vinyl"
    },
    cooling_system_type: {
      "ATFAN" => "Attic Fan",
      "CENTR" => "Central",
      "DUAL" => "Dual",
      "FAN" => "Fan",
      "NONE" => "None",
      "OTHER" => "Other",
      "WINDW" => "Window"
    },
    heating_system: {
      "BASEB" => "Baseboard",
      "CENTR" => "Central",
      "DUAL" => "Dual",
      "ESHVC" => "Energy Star HVAC",
      "FLOOR" => "Floor",
      "FURNC" => "Furnace",
      "HPUMP" => "Heat Pump",
      "NONE" => "None",
      "OTHER" => "Other",
      "RADNT" => "Radiant",
      "STOVE" => "Stove",
      "TWOSY" => "Two Systems",
      "WALL" => "Wall"
    },
    interior_description: {
      "AIRFI" => "Air Filter",
      "CLFAN" => "Ceiling Fan",
      "CNVAC" => "Central Vacuum",
      "DEHUM" => "Dehumidifier",
      "ELVTR" => "Elevator",
      "EXCLO" => "Extra Closets",
      "FIRES" => "Fire Sprinkler System",
      "HOTUB" => "Hot Tub",
      "HUMID" => "Humidifier",
      "INLAW" => "In-Law Quarters",
      "INTCM" => "Intercom",
      "RECRD" => "Recording Studio",
      "REDEC" => "Redecorated",
      "SMOKE" => "Smoke Alarm",
      "STORG" => "Storage",
      "UTLCN" => "Utility Connection",
      "WETBR" => "Wet Bar",
      "WICLO" => "Walk-In Closets",
      "WOODB" => "Wood Burning FP",
      "WTRFI" => "Water Filter"
    },
    # waterfront: {
    #   "CREEK" => "Y",
    #   "LAKE" => "N",
    #   "LAKFR" => "Y",
    #   "POND" => "Y",
    #   "RIVER" => "N",
    #   "RIVFR" => "Y"
    # },
    equipment: {
      "COMPT" => "Compactor",
      "DISHW" => "Dishwasher",
      "DISPL" => "Disposal",
      "DRYER" => "Dryer",
      "ENSAP" => "Energy Star Appliances",
      "FREZR" => "Freezer",
      "GRILL" => "Grill",
      "ICEMK" => "Ice Maker",
      "MICRO" => "Microwave",
      "REFRG" => "Refrigerator",
      "WASHR" => "Washer"
    },
    # pool_private: {
      # "ABGRN" => "Y",
      # "IDOOR" => "Y",
      # "INGRN" => "Y",
      # "NONE" => "N"
    # },
    road_surface: {
      "AGGRE" => "Aggregate",
      "BLTOP" => "Blacktop",
      "CONCR" => "Concrete",
      "GRAVL" => "Gravel",
      "NONE" => "None",
      "OTHER" => "Other",
      "PGRAV" => "Pea Gravel",
      "PNTRD" => "Penetrated",
      "STONE" => "Stone"
    },
    roof_type: {
      "ALUMN" => "Aluminum",
      "ASBES" => "Asbestos",
      "ASPHL" => "Asphalt",
      "BLTUP" => "Built-up",
      "CLTIL" => "Clay Tile",
      "CMTSH" => "Cement Shake",
      "CNCRT" => "Concrete Tile",
      "COMPS" => "Composition Shingle",
      "COPPR" => "Copper",
      "CRMTL" => "Corrugated Metal",
      "GREEN" => "Green - Vegetation",
      "MEMBR" => "Membrane",
      "METAL" => "Metal",
      "OTHER" => "Other",
      "REFLT" => "Reflective Coating",
      "SLATE" => "Slate",
      "SOLPN" => "Solar Panel",
      "STAND" => "Standing Seam Steel",
      "STEEL" => "Steel",
      "TARGR" => "Tar and Gravel",
      "THATC" => "Thatched",
      "TILES" => "Tile",
      "URETH" => "Urethane",
      "WDSHK" => "Wood Shake",
      "WDSHN" => "Wood Shingle"
    },
    showing_instructions: {
      "AGENT" => "Agent Must be Present",
      "ANYTM" => "Show Anytime",
      "CALAG" => "Call Agent",
      "CALOF" => "Call Listing Office",
      "CENTR" => "Call Showing Center",
      "COMBO" => "Combo Lock",
      "LOCK" => "MLS Lock Box",
      "OTHER" => "Other",
      "SENTR" => "SentriLock"
    },  
    disclosures: {
      "AO" => "Owner Agent",
      "EA" => "Exclusive Agency",
      "EASHS" => "Ex Agency-Short Sale",
      "ER" => "Exc. Right to Sell",
      "ERSHS" => "Ex Right-Short Sale",
      "REOFH" => "HUD Home",
      "REOOT" => "Other Foreclosure",
      "REOVA" => "VA Foreclosure"
    },
    basement_desc: {
      "APT" => "Apartment",
      "COMBO" => "Combination",
      "CRAWL" => "Crawl",
      "FINIS" => "Finished",
      "OTHER" => "Other",
      "SLAB" => "Slab",
      "UNFIN" => "Unfinished"
    },
    # garage_attach: {
      # "ASSGN" => "Y",
      # "INCPT" => "Y",
      # "INGAR" => "Y",
      # "OTHER" => "Y",
      # "PRKSP" => "Y",
      # "SHCPT" => "Y",
      # "SHGAR" => "Y",
      # "UNASG" => "Y",
      # "ADTCH" => "Y",
      # "ATACH" => "Y",
      # "ATCHF" => "Y",
      # "ATCHR" => "Y",
      # "ATCHS" => "Y",
      # "BSMT" => "Y",
      # "CPORT" => "Y",
      # "DTACH" => "Y",
      # "NONE" => "N"      
    # }
    # pool_area: {
    #   "ABGRN" => "Y",
    #   "IDOOR" => "Y",
    #   "INGRN" => "Y",
    #   "NONE" => "N"
    # }
  }

  DATA_ENUMS = {
    category: hash_from_csv('categories'),
    mls_area_name: hash_from_csv('mls_areas'),
    # rental_type: hash_from_csv('rental_types'),
    # single_family_type: hash_from_csv('single_family_types'),
    status: hash_from_csv('statuses')
    # state: hash_from_csv('states')
  }

  DATA_MAPPING = {
    style_of_building: "Style",
    annual_maintenance_fee: proc { |d| d["AssociationFee"].blank? ? nil : (d["AssociationFee"].to_d * 12).round.to_s },
    hoa_payment: 'AssociationFee',
    bathrooms_full: 'TotalFullBaths',
    bathrooms_half: 'TotalHalfBaths',
    beds: 'TotalBedrooms',
    city: "City",
    closed_date:  proc {|d| to_date_sql(d['ClosedDate']) },
    sold_price: 'SalesPrice',
    sold_lease_price: 'LeasePerMonth',
    street_address: 'StreetAddressDisplay',
    street_direction: 'StreetDirection',
    lot_description: 'LotDesc',
    county: 'CountyID',
    dom: 'DaysOnMarket',
    mls_elementary_school: 'ElementarySchool',
    expiration_date: proc {|d| to_date_sql(d['ExpireDate']) },
    exterior_type: 'ConstructionType',
    exterior_description: 'ExteriorFeatures',
    fence_type: 'FenceType',
    financing_available: 'BuyerFinancing',
    fireplace_number: 'NumberOfFireplaces',
    floor_type: 'FloorTypes',
    foundation_type: 'FloorTypes',
    cooling_system_type: 'CoolingSystem',
    heating_system:  'HeatingSystem',
    mls_high_school: 'HighSchool',
    interior_description: 'InteriorOther',
    latitude:  proc { |d| d['Latitude'] ? d['Latitude'].to_d : nil },
    listing_agent_phone: 'AgentListPhone',
    listing_agent_name: 'AgentListFullName',
    listing_agent_id: 'AgentListID',
    listing_date:  proc {|d| to_date_sql(d['ListDate']) },
    listing_broker_id: 'OfficeListCompanyID',
    listing_broker_name: 'OfficeListOfficeName',    
    list_price: proc { |d| 
      if d['PropertyClassID'] == "6" 
        d['LeasePerMonth']
      else
        d['ListPrice']
      end  
    },
    longitude:  proc { |d|  d['Longitude'] ? d['Longitude'].to_d : nil },
    waterfront:  proc { |d|
      case d["WaterfrontDesc"]
        when "CREEK" 
         "Y"
        when "LAKE"  
         "N"
        when "LAKFR" 
         "Y"
        when "POND"  
         "Y"
        when "RIVER" 
         "N"
        when "RIVFR" 
         "Y"
        else
         "N"
      end
    },
    acreage: 'Acres',
    mls_middle_school: 'JuniorHighSchool',
    mls_area_name: 'AreaID',
    mls_area_key: 'AreaID',
    mls_number: 'MlsNum',
    # mls_record_creation_date: proc {|d| to_date_sql(d['ListDate']) },
    mls_record_modified: proc {|d| to_date_sql(d['ModDate']) },
    stories: 'NumOfStories',
    termination_date: proc {|d| to_date_sql(d['OffMarketDate']) },
    original_list_price: 'OriginalListPrice',
    equipment: 'AppliancesOther',
    owner_name: 'OwnerName',
    tax_id: 'ParcelIDDisplay',
    garage_capacity: 'GarageCapacity' ,
    pending_date:  proc {|d| to_date_sql(d['PendingDate']) },
    number_of_photos: 'PictureCount',
    # pool_private: 'PoolDesc',
    pool_private: proc {|d| if  d['PoolDesc'].in?(['ABGRN','IDOOR','INGRN'])   then "Y" else "N" end },
    zip_code: 'ZipCode',
    rental_type: proc {|d|
       if d['PropertyClassID'] == "6"
          case d['PropertySubType']
          when 'APRTB' 
            'apartment'
          when 'CONDO' 
            'condo'
          when 'DUPLX' 
            'duplex'
          when 'MOBIL' 
            'other'
          when 'OTHER' 
            'other'
          when 'RETIR' 
            'other'
          when 'SINGL' 
            'freestanding'
          when 'TRPLX' 
            'triplex'
          end
       end
    },  
    
     
    single_family_type: proc {|d|
       if d['PropertyClassID'] == "1"
          Delayed::Worker.logger.debug "entered 1"
          case d['PropertySubType']
          when 'HPR-A'
            'townhome'
          when 'HPR-D'
            'duplex'
          when 'MANUF'
            'manufactured'
          when 'MOBIL'
            'manufactured'
          when 'MODUL'
            'manufactured'
          when 'SINGL'
            'freestanding'
          when 'ZERO'
            'patio'
          end
       end
    },  
    category: 'PropertyClassID',
    public_remarks: 'Remarks',
    road_surface: 'DrivewayType',
    roof_type:  'RoofMaterial',
    seller_paid_to_buyer_closing_costs: 'SellerParticipation',
    selling_agent_phone: 'AgentSellPhone',
    selling_agent_name: 'AgentSellFullName',
    selling_agent_id: 'AgentSellID',
    showing_instructions: 'ShowingInstructions',
    disclosures: 'ListingType',
    square_feet: 'SqFtTotal',
    land_size: 'LotSize',
    lot_square_feet: proc { |d|   d["Acres"].blank? ? nil : (d["Acres"].to_s.to_d  * 43560).round.to_s     },
    state: 'State',
    status: 'ListingStatusID',
    street_name: 'StreetName',
    street_number: 'StreetNumber',
    subdivision: 'Subdivision',
    legal_description: 'ParcelID',
    tax_amount: 'TaxAmount',
    unit_number: 'UnitNumber',
    withdrawn_date: proc {|d| to_date_sql(d['WithdrawnDate']) },
    year_built: 'YearBuilt',
    new_construction: 'IsNewConstruction',
    basement_desc: "BasementDesc",
    garage_attach: proc { |d| 
       case d["GarageDescription"]
        when 'ATACH'
          'Y'
        when 'ATCHF'
          'Y'
        when 'ATCHS'
          'Y'
        when 'ATCHR'
          'Y'
        when 'NONE'
          'N'
        when 'DTACH'
          'N'
        when 'CPORT'
          'N'
        when 'BSMT'
          'N'
        when 'ADTCH'
          'N'
        else
          'N'
      end 
    },
    key_map_book: 'BookPage',
    lot_number: 'LotNum',
    pool_area: proc {|d| if  d['PoolDesc'].in?(['ABGRN','IDOOR','INGRN'])   then "Y" else "N" end },
    rooms: 'TotalRooms'
  }

  def login
    page = agent.post 'https://members.har.com/login/dispLogin.cfm?app=engine', USERNAME: market.mls_login, PWD: market.mls_password, USERTYPE: 'M', SUBMIT: 'Log In'
    page = agent.get 'http://members.har.com/mopx/goHARMLS.cfm'
    current_url == "http://www.harmls.com/default.aspx?showStartPage=true"
  end

  def logout
    agent.get 'http://members.har.com/mopx/doLogout.cfm'
    true
  end

  def rets_login
    @client = Rets::Client.new(:login_url => "http://rets.realtracs.com:6103/rets/login",:username => market.mls_login,:password => market.mls_password, :agent => "MLKay/1.0")
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "Nashville RETS logged in"
    true
  end

  def rets_logout
    @client.logout
    Delayed::Worker.logger.debug "logged out"
    true
  end

  def mls_url_for(property)
    "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
  end
  
  

  def grab(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_rets()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def grab_by_interval(minutes=nil)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS by interval ~~~~~~~~"
      parse_rets_by_interval(minutes) 
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def grab_by_specified_period(start_time,end_time)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS by interval ~~~~~~~~"
      parse_rets_by_specified_period(start_time,end_time) 
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

  def add_dom(days)
    
    super
    
    x = 0
    Delayed::Worker.logger.debug "#{market.name} specific update ##{market.id}"
    x = self.market.properties.active_and_pending.update_all ["cdom = ifnull(cdom,ifnull(dom,0))  + DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL ifnull(dom,0) DAY) - 1) )
                          , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          ["(listing_date is not null) and ((cdom is null) or (dom is null)) "]
                          
    Delayed::Worker.logger.debug "rows affected: #{x}"
    self.rows_affected = (self.rows_affected.nil? ? 0 : self.rows_affected ) + x
    
    notify_listeners
  end
  
  def self.to_date_sql(dt)
    #"2014-05-09T15:40:14"
    field = dt.to_s
    pattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil
    subpattern = field.match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil
    ma =  field.match(pattern).captures.try(:[],0) rescue nil
    ta =  field.match(subpattern).captures.try(:[],0) rescue nil
    if field && field.match(/(\d+\/\d+\/\d+)/)
      strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil
    else
      strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'
      !ma.blank? ? ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil
    end
  rescue
    nil
  end

  protected

  private

    def parse_raw(data, filename)

      begin
        CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

          # row['PropType'] ||= filename.try(:slice, -7, 3)
          save_property(row, DATA_MAPPING, DATA_ENUMS)
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "error #{e.message}"
      end
    end

    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end
     
    def parse_rets()
      
        # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
        timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
        timerange = "2014-01-01-2014-06-11"
        sd = Date.parse('2015-01-22')
        ed = Date.parse('2015-01-23')
        Delayed::Worker.logger.debug "--------- pulling single family ---------"
        ed.downto(sd) { |date|
          
          Delayed::Worker.logger.debug "---------  #{date} ---------"
          date1 = date.strftime("%Y-%m-%dT00:00:00")
          date2 = date.strftime("%Y-%m-%dT04:00:00")
          date3 = date.strftime("%Y-%m-%dT08:00:00")
          date4 = date.strftime("%Y-%m-%dT12:00:00")
          date5 = date.strftime("%Y-%m-%dT16:00:00")
          date6 = date.strftime("%Y-%m-%dT20:00:00")
          tomorrow = date + 1.day
          date7 = tomorrow.strftime("%Y-%m-%dT00:00:00")
          dates = []
          dates = [date1,date2,date3,date4,date5,date6,date7]
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
          for i in 0..5
            Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
            begin  
             
              recdate = "#{dates[i]}-#{dates[i+1]}"
              Delayed::Worker.logger.debug "--------- pulling single family ---------"
              
              results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{recdate})",:limit => 60000)
              
              Delayed::Worker.logger.debug "--------- importing single family ---------"
      #
      
              results.each do |row|
                 Delayed::Worker.logger.debug "##row: #{row}"
                 save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
               end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end

            begin  
             
              recdate = "#{dates[i]}-#{dates[i+1]}"
              Delayed::Worker.logger.debug "--------- pulling single family ---------"
              
              results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{recdate})",:limit => 60000)
              
              Delayed::Worker.logger.debug "--------- importing single family ---------"
      #
      
              results.each do |row|
                 Delayed::Worker.logger.debug "##row: #{row}"
                 save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
               end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end

            begin  
             
              recdate = "#{dates[i]}-#{dates[i+1]}"
              Delayed::Worker.logger.debug "--------- pulling single family ---------"
              
              results = client.find(:all,:search_type => "Property",:class => "LLF",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{recdate})",:limit => 60000)
              
              Delayed::Worker.logger.debug "--------- importing single family ---------"
      #
      
              results.each do |row|
                 Delayed::Worker.logger.debug "##row: #{row}"
                 save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
               end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end

            begin  
             
              recdate = "#{dates[i]}-#{dates[i+1]}"
              Delayed::Worker.logger.debug "--------- pulling rental ---------"
              
              results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{recdate})",:limit => 60000)
              
              Delayed::Worker.logger.debug "--------- importing rental ---------"
      #
      
              results.each do |row|
                 Delayed::Worker.logger.debug "##row: #{row}"
                 save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
               end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
             
            
            
          
          end
          
         
        }

       

    end
    
    
    
    
    
    
#     def parse_rets()

#       begin
        
#         divisor = 720
#         offset = 1440
#         time_zone_diff = 840
                
#         timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
#         new_offset = offset
#         step = (offset / divisor) + 1

#         step.downto(1) do |increment|
          
#           start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
#           if (new_offset - divisor) >= 0
#             end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
#           else
#              end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
#           end
          
#           Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
#           date_range = "#{start_time}-#{end_time}"

          
#           begin
#             Delayed::Worker.logger.debug "--------- pulling RES ---------"
#             results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
#             Delayed::Worker.logger.debug "--------- importing RES ---------"
#             results.each do |row|
#                Delayed::Worker.logger.debug "##row: #{row}"
#                save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
#             end
#           rescue  Exception => e
#              Delayed::Worker.logger.debug "No record found #{e.message}"
#           end
# #           
#           begin
#             Delayed::Worker.logger.debug "--------- pulling CND ---------"
#             results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
#             Delayed::Worker.logger.debug "--------- importing CND ---------"
#             results.each do |row|
#                Delayed::Worker.logger.debug "##row: #{row}"
#                save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
#             end
#           rescue  Exception => e
#              Delayed::Worker.logger.debug "No record found #{e.message}"
#           end
# #           
#           begin
#             Delayed::Worker.logger.debug "--------- pulling LLF ---------"
#             results = client.find(:all,:search_type => "Property",:class => "LLF",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
#             Delayed::Worker.logger.debug "--------- importing LLF ---------"
#             results.each do |row|
#                Delayed::Worker.logger.debug "##row: #{row}"
#                save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
#             end
#           rescue  Exception => e
#              Delayed::Worker.logger.debug "No record found #{e.message}"
#           end
          
         
#           begin
#             Delayed::Worker.logger.debug "--------- pulling RNT ---------"
#             results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
#             Delayed::Worker.logger.debug "--------- importing RNT ---------"
#             results.each do |row|
#                Delayed::Worker.logger.debug "##row: #{row}"
#                save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
#             end
#           rescue  Exception => e
#              Delayed::Worker.logger.debug "No record found #{e.message}"
#           end

#           new_offset = new_offset - divisor

#         end

#       rescue Exception => e
#         Delayed::Worker.logger.debug "No record found #{e.message}"
#       end

#     end

    def parse_rets_by_interval(minutes=nil)

      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
#           
          begin
            Delayed::Worker.logger.debug "--------- pulling CND ---------"
            results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing CND ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
#           
          begin
            Delayed::Worker.logger.debug "--------- pulling LLF ---------"
            results = client.find(:all,:search_type => "Property",:class => "LLF",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing LLF ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
         
          begin
            Delayed::Worker.logger.debug "--------- pulling RNT ---------"
            results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(ListingStatusID=1,3,4,2,5),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RNT ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    def parse_rets_by_specified_period(start_time,end_time)

      begin
        
        divisor = 720
        end_time = Time.parse(end_time)
        start_time = Time.parse(start_time)
        Delayed::Worker.logger.debug "start_time #{start_time} end_time #{end_time}"
      
        offset = (end_time - start_time).to_i / 60
        time_zone_diff = 840
        
        status_list = '1,3,4,2,5'
        # status_list = '3'
                
        timenow =  !Rails.env.development? ? end_time  : end_time - time_zone_diff.minutes
        new_offset = offset
        
        Delayed::Worker.logger.debug "offset #{offset} divisor #{divisor}"
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          
          begin
            Delayed::Worker.logger.debug "--------- pulling RES ---------"
            results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(ListingStatusID=#{status_list}),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RES ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling CND ---------"
            results = client.find(:all,:search_type => "Property",:class => "CND",:query => "(ListingStatusID=#{status_list}),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing CND ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
          begin
            Delayed::Worker.logger.debug "--------- pulling LLF ---------"
            results = client.find(:all,:search_type => "Property",:class => "LLF",:query => "(ListingStatusID=#{status_list}),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing LLF ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end
          
         
          begin
            Delayed::Worker.logger.debug "--------- pulling RNT ---------"
            results = client.find(:all,:search_type => "Property",:class => "RNT",:query => "(ListingStatusID=#{status_list}),(ModDate=#{date_range})",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing RNT ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    def parse_special_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

    def parse_hotsheet(data, filename)
      Delayed::Worker.logger.debug "entering parsing hotsheet "
      parser = Nokogiri::HTML(data)
      tables = parser.css("table[@width='100%']")
      tables.each_with_index do |table, table_index|
        title = table.at_xpath("./tr/td")
        next if title.nil? || title.text.blank? || title.attr('colspan').blank?
        headers = html_strip(table.xpath("./tr[2]/td"), true)
        table.xpath("./tr").each_with_index do |row, row_index|
          next if row_index <= 1
          values = Hash[headers.zip(html_strip(row.xpath("./td")))]
          update_property(values, HOTSHEET_MAPPING, HOTSHEET_ENUMS)
        end
      end
    end

    def fetch_raw(days)
      tries = 0

      begin
          Delayed::Worker.logger.debug "fetching raw"
           Delayed::Worker.logger.debug " keys: #{DATA_ENUMS[:category].keys}"
          DATA_ENUMS[:category].keys.each do |category|

            statuses = []
            statuses = ['act','op','psho']

            page = agent.post 'http://www.harmls.com/thirdparty/Scripts/GetData.asp', {
              appMinorVersion: '0', appVersion: '4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2)',
              Name: '3RDPARTY', Pass: 'DHA0ATFA', Custom: 'ALL', Num: '4500', NumberOfProperties: '4500',
              Where: "(ListStatus%20IN%20('#{statuses.join('\',\'')}'))%20AND%20(DaysOnMarket%3E=1%20AND%20DaysOnMarket%3C=#{days})",
              ColumnHeader: 'Y', ItemList: '', Format: '0', Tax: 'undefined', DownloadDecodeOverride: '',
              Join: '', Tbl: category, preventDecodeList: '', D1SelectedIndex: '0', D1: 'ALL'
            }

            import(page) if page.is_a?(Mechanize::File)
          end
        rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
      end
    end

    def fetch_hotsheet(days)
      tries = 0
      result = nil
      begin
        Delayed::Worker.logger.debug "fetching hotsheet"
        date_format = (Date.today - days).strftime('%m/%d/%Y')
        today_format = Date.today.strftime('%m/%d/%Y')
        page = agent.post 'http://www.harmls.com/Reports/Scripts/display.asp', {
          formname: 'TodaysActivity',
          dateend: "#{today_format}%2023%3A59",
          datestart: "#{date_format}%2000%3A00",
          EndDate: "#{today_format}%2023%3A59",
          EndStatement: 'End%20of%20Activity%20Report%20for%20Today',
          heading: "#{date_format}",
          propertytype: '%22-ALL-%22',
          rpt: '%27BACK%27%2C%20%27EXP%27%2C%20%27NEW%27%2C%20%27optionp%27%2C%20%27PSHO%27%2C%20%27PEND%27%2C%20%27PRICE%27%2C%20%27CLOSD%27%2C%20%27TERM%27%2C%20%27WITH%27',
          StartDate: "#{date_format}%2000%3A00",
          today: "#{today_format}",
          todaysactivity: 'x',
          fourtharea: '',
          fiftharea: 'Display.asp',
          updatehotsheetdate: '0',
          where: '',
          Area: '-ALL-',
          Location: '-ALL-',
          proptype: 'RES%2C%20CND%2C%20LND%2C%20MUL%2C%20ACR%2C%20HIR%2C%20RNT',
          aReportDetails: 'BACK%2C%20EXP%2C%20NEW%2C%20optionp%2C%20PSHO%2C%20PEND%2C%20PRICE%2C%20CLOSD%2C%20TERM%2C%20WITH',
          narrow: 'true',
          sb: '0'
        }

        #$$Delayed::Worker.logger.debug "Is mechanize file2? #{page.is_a?(Mechanize::File)}"
        #$$Delayed::Worker.logger.debug "body2: #{page.body}"
        import(page) if page.is_a?(Mechanize::File)
      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      rescue Exception => e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      end
    end

    def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
              end 
            elsif (string.is_a?(BigDecimal))
              string
            else
              if attr == :sub_category
                nil
              elsif attr == :tax_id
                string
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
            end

          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import3(self.source, attrs, raw, update_only)

      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      # binding.pry
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end


end
