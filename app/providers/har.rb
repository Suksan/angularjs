class Har < Provider

  HOTSHEET_ENUMS = {
    status: hash_from_csv('statuses'),
  }
  HOTSHEET_MAPPING = {
    list_price: 'List Price',
    mls_number: 'MLS #',
    sold_lease_price: 'Lease Price', 
    sold_price: 'Sp', 
    status: 'St',
  }

  DATA_LOCATION = hash_from_csv('locations')
  DATA_MLS_AREA = hash_from_csv('mls_areas')
  DATA_SCHOOL_DISTRICT = hash_from_csv('schools')
  DATA_ENUMS = {
    category: hash_from_csv('categories'),
    lot_use: hash_from_csv('lot_uses'),
    rental_class: hash_from_csv('rental_classes'),
    rental_type: hash_from_csv('rental_types'),
    single_family_type: hash_from_csv('single_family_types'),
    status: hash_from_csv('statuses'),
    state: hash_from_csv('states')
  }
  DATA_MAPPING = {
    access_lock_box: 'AccessLockbox',
    access_to_complex: 'ComplexAccess',
    acreage: 'Acres',
    annual_maintenance_fee: 'AnnualMaintFee',
    appointment_phone_description: 'PhoneApptDesc',
    bathrooms_full: 'BathsFull',
    bathrooms_half: 'BathsHalf',
    beds: 'Beds',
    builder_name: 'BuilderName',
    building_name: 'BuildingName',
    building_stories: 'BuildingStories',
    buyer_agent_bonus: 'compbonus',
    buyer_agent_compensation: 'compbuy',
    category: 'PropType',
    cdom: 'DaysOnMarketCumulative',
    city: 'City',
    closed_date: 'ClosedDate',
    completion_date: 'CompletionDate',
    cooling_system_type: 'CoolSystem',
    county: 'County',
    coutertop_type: 'Countertops',
    deed_date: nil,
    defects: 'Defects',
    disclosures: 'Disclosures',
    dom: 'DaysOnMarket',
    elementary_school: 'SchoolElem',
    estimated_close_date: 'EstClosedDate',
    expiration_date: 'expiredate',
    exterior_type: 'Exterior',
    financing_available: 'FinanceAvail',
    floor_type: 'Floors',
    foundation_type: 'Foundation',
    garage_capacity: 'GarageCap',
    geo_market_area: 'GeoMarketArea',
    heating_system: 'HeatSystem',
    high_school: 'SchoolHigh',
    hirise_fees: nil,
    interior_description: 'Interior',
    key_map_page: 'MapPage',
    land_size: 'LandSize',
    last_sale_date: nil,
    legal_description: 'Legal',
    legal_subdivision: 'LegalSubdivision',
    level_of_unit: 'UnitLevel',
    list_price: 'ListPrice',
    # list_price_per_square_feet: 'PriceSqFtList',
    listing_agent_phone: 'PhoneApptDesk',
    listing_agent_fax: 'FaxNumber',
    listing_agent_id: 'AgentList',
    listing_agent_name: nil,
    listing_broker_id: 'OfficeList',
    listing_broker_name: 'OfficeName',
    listing_date: 'ListDate',
    loan_amortized_years: 'AmortizedYears',
    loan_amount_interest_rate: 'LoanInterestRateNew',
    loan_amount_new: 'LoanAmountNew',
    loan_term: 'SoldTerms',
    location_key: 'Location',
    location_name: lookup(DATA_LOCATION, 'Location'),
    lock_box_code: 'LockboxNum',
    loss_mitigation: 'LossMitigation',
    lot: nil,
    lot_back_dimensions: 'LotBackDim',
    lot_description: 'LotDesc',
    lot_front_dimension: 'LotFrontDim',
    lot_improvements: 'LotImprovements',
    lot_left_dimension: 'LotLeftDim',
    lot_number: 'LotNum',
    lot_right_dimension: 'LotRightDim',
    lot_square_feet: 'LotSize',
    lot_use: 'LotUse',
    maintenance_fee: 'MaintFee',
    middle_school: 'SchoolJunior',
    mineral_rights: 'MineralRights',
    mls_area_key: 'Area',
    mls_area_name: lookup(DATA_MLS_AREA, 'Area'),
    mls_number: 'MLSNUM',
    mortgagee: nil,
    multi_owner_operating: 'ExpenseOwnerOperating',
    muti_gross_income: 'IncomeGross',
    muti_gross_operating_income: 'IncomeGrossOperating',
    mutli_income_net_operation: 'IncomeNetOperating',
    neighborhood_number: nil,
    new_construction: 'NewConstruction',
    new_construction_description: 'NewConstructionDesc',
    night_phone: 'PhoneNight',
    number_of_photos: 'PhotoCount',
    number_of_units: 'NumUnits',
    occupancy_rate: 'OccupancyRate',
    office_phone: 'PhoneOffice',
    original_list_price: 'ListPriceOrig',
    other_building_fees: 'ExpenseOperating',
    other_fees: 'FeeOtherAmount',
    owner_street_address: nil,
    owner_city: nil,
    owner_state: nil,
    owner_zip_code: nil,
    owner_zip_4: nil,
    owner_name: nil,
    pending_date: 'PendingDate',
    pool_area: 'PoolArea',
    pool_private: 'PoolPrivate',
    public_remarks: 'Remarks',
    realtor_remarks: 'RealRemarks',
    rental_class: 'PropClass',
    rental_terms: 'RentalTerms',
    rental_sale_mls_number: 'SaleMLNum',
    rental_type: 'rentaltype',
    road_surface: 'RoadSurface',
    roof_type: 'Roof',
    school_district: 'SchoolDistrict',
    school_district_name: lookup(DATA_SCHOOL_DISTRICT, 'SchoolDistrict'),
    seller_paid_to_buyer_closing_costs: 'SellerToClosingCosts',
    seller_repair_credits: 'RepairSeller',
    selling_for_lot_value: 'LotValue',
    showing_instructions: 'ShowInstr',
    siding_type: 'Siding',
    single_family_type: proc {|d| d['PropertyType'].try(:gsub, /,?FREES,?/, '') },
    sold_lease_price: 'LeasePrice',
    sold_price: 'SalesPrice',
    square_feet: 'SqFtBldg',
    state: 'State',
    status: 'ListStatus',
    stories: 'Stories',
    street_direction: 'StreetDir',
    street_name: 'StreetName',
    street_number: 'StreetNum',
    street_surface_type: 'StreetSurface',
    style_of_building: 'Style',
    subdivision: 'Subdivision',
    subdivision_section_number: 'SectionNum',
    tax_id: 'TaxID',
    tax_market_value: nil,
    tax_amount: 'TaxAmount',
    tax_rate: 'TaxRate',
    tax_value: nil,
    tax_year: 'TaxYear',
    termination_date: 'TerminationDate',
    unit_number: 'UnitNum',
    withdrawn_date: 'WithdrawnDate',
    year_built: 'YearBuilt',
    zip_4: 'zip4',
    zip_code: 'ZipCode'
  }

  def login
    page = agent.post 'https://members.har.com/login/dispLogin.cfm?app=engine', USERNAME: market.mls_login, PWD: market.mls_password, USERTYPE: 'M', SUBMIT: 'Log In'
    page = agent.get 'http://members.har.com/mopx/goHARMLS.cfm'
    current_url == "http://www.harmls.com/default.aspx?showStartPage=true"
  end
  


  def logout
    agent.get 'http://members.har.com/mopx/doLogout.cfm'
    true
  end

  def mls_url_for(property)
    "http://search.har.com/engine/_HAR#{property.mls_number}.htm" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank? 
  end
  
  def grab(days)
    if login
      Delayed::Worker.logger.debug "~~~~~~~ Fetching data for grab ~~~~~~~~"
      fetch(days)
      logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def sync_download(days)
    if login
      Delayed::Worker.logger.debug "~~~~~~~ Fetching data for download ~~~~~~~~"
      download(days)
      logout
    else
      self.errors[:login] = "could not login"
    end
  end
  
  def fetch(days)
    p "-- fetching raw data"
    
    range = [10,9,8,7,6,5,4,3,2,1,0]
    
    range.each do |r|
      if r.is_a?(Array)
        fetch_raw_range(r.first,r.last)
      else
        fetch_raw_range(r,r)
      end        
    end
    
    p "-- fetching hotsheet"
    fetch_hotsheet(2)
    
    
      
  end
  
  def fetch_special
        
    for i in (91).downto(0)
      if login
         Delayed::Worker.logger.debug "fetch i: #{i}"
         fetch_raw_range(i,i)
         logout
      end
    end
    
    arr = [91,88,85,82,79,76,73,70,67,64,61,58,55,52,49,46,43,40,37,34,31,28,25,22,19,16,13,10,7,4,1]
    # arr = [91,88,85]
    arr.each do |j|
      if login
        Delayed::Worker.logger.debug "fetch j: #{j}"
        if j > 1
          en = (Date.today - (j - 2)).strftime('%m/%d/%Y')
          st = (Date.today - j).strftime('%m/%d/%Y')
        else
          en = (Date.today - (j - 1)).strftime('%m/%d/%Y')
          st = (Date.today - j).strftime('%m/%d/%Y')
         end
        
        fetch_hotsheet_by_specified_dates(st,en)
        logout
      end
    end
    
    
  end

  protected

  private

    def parse_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= filename.try(:slice, -7, 3)
        
        Delayed::Worker.logger.debug "## row class #{row.class.name}" if row
        Delayed::Worker.logger.debug "## each row #{row}"
        save_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    end
    
    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end
    
    def parse_special_raw(data, filename)
    
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        row['PropType'] ||= 'res'
        correct_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

    def parse_hotsheet(data, filename)
      Delayed::Worker.logger.debug "entering parsing hotsheet "
      parser = Nokogiri::HTML(data)
      tables = parser.css("table[@width='100%']")
      tables.each_with_index do |table, table_index|
        title = table.at_xpath("./tr/td")
        next if title.nil? || title.text.blank? || title.attr('colspan').blank?
        headers = html_strip(table.xpath("./tr[2]/td"), true)
        table.xpath("./tr").each_with_index do |row, row_index|
          next if row_index <= 1
          values = Hash[headers.zip(html_strip(row.xpath("./td")))]
          update_property(values, HOTSHEET_MAPPING, HOTSHEET_ENUMS)
        end
      end
    end
    
    def fetch_raw_range(st,en)
      tries = 0
      
      begin
          Delayed::Worker.logger.debug "fetching raw"
           Delayed::Worker.logger.debug " keys: #{DATA_ENUMS[:category].keys}"
          DATA_ENUMS[:category].keys.each do |category| 
            page = fetch_individual(category,st,en)
            import(page) if page.is_a?(Mechanize::File)
          end
        rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e  
        end
    end
    
    def fetch_individual(category,st,en)
      tries = 0
      
      begin
        
         statuses = []
          statuses = ['act','op','psho']
          
          page = agent.post 'http://www.harmls.com/thirdparty/Scripts/GetData.asp', {
            appMinorVersion: '0', appVersion: '4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2)', 
            Name: '3RDPARTY', Pass: 'DHA0ATFA', Custom: 'ALL', Num: '4500', NumberOfProperties: '4500', 
            Where: "(ListStatus%20IN%20('#{statuses.join('\',\'')}'))%20AND%20(DaysOnMarket%3E=#{st}%20AND%20DaysOnMarket%3C=#{en})", 
            ColumnHeader: 'Y', ItemList: '', Format: '0', Tax: 'undefined', DownloadDecodeOverride: '', 
            Join: '', Tbl: category, preventDecodeList: '', D1SelectedIndex: '0', D1: 'ALL'
          }
          
          page
          
      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
      rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e  
      end
    end
    
    def fetch_raw(days)
      tries = 0
      
      begin
          Delayed::Worker.logger.debug "fetching raw"
           Delayed::Worker.logger.debug " keys: #{DATA_ENUMS[:category].keys}"
          DATA_ENUMS[:category].keys.each do |category| 
           
            statuses = []
            statuses = ['act','op','psho']
            
            page = agent.post 'http://www.harmls.com/thirdparty/Scripts/GetData.asp', {
              appMinorVersion: '0', appVersion: '4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2)', 
              Name: '3RDPARTY', Pass: 'DHA0ATFA', Custom: 'ALL', Num: '4500', NumberOfProperties: '4500', 
              Where: "(ListStatus%20IN%20('#{statuses.join('\',\'')}'))%20AND%20(DaysOnMarket%3E=0%20AND%20DaysOnMarket%3C=10)", 
              ColumnHeader: 'Y', ItemList: '', Format: '0', Tax: 'undefined', DownloadDecodeOverride: '', 
              Join: '', Tbl: category, preventDecodeList: '', D1SelectedIndex: '0', D1: 'ALL'
            }
            
            import(page) if page.is_a?(Mechanize::File)
          end
        rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e  
        end
    end
    
    def fetch_download(days)
      tries = 0
      st = days
      en = days
      begin
          Delayed::Worker.logger.debug "fetching raw"
          Delayed::Worker.logger.debug " keys: #{DATA_ENUMS[:category].keys}"
          DATA_ENUMS[:category].keys.each do |category| 
           
            page = fetch_individual(category,st,en)
            import(page) if page.is_a?(Mechanize::File)
          end
        rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e  
        end
    end

    def fetch_hotsheet(days)
      tries = 0
      result = nil
      begin
        Delayed::Worker.logger.debug "fetching hotsheet"
        date_format = (Date.today - days).strftime('%m/%d/%Y')
        Delayed::Worker.logger.debug ">fetch date format: #{date_format}"
        today_format = Date.today.strftime('%m/%d/%Y')
        Delayed::Worker.logger.debug ">fetch today format: #{today_format}"
        page = agent.post 'http://www.harmls.com/Reports/Scripts/display.asp', {
          formname: 'TodaysActivity',
          dateend: "#{today_format}%2023%3A59",
          datestart: "#{date_format}%2000%3A00",
          EndDate: "#{today_format}%2023%3A59",
          EndStatement: 'End%20of%20Activity%20Report%20for%20Today',
          heading: "#{date_format}",
          propertytype: '%22-ALL-%22',
          rpt: '%27BACK%27%2C%20%27EXP%27%2C%20%27NEW%27%2C%20%27optionp%27%2C%20%27PSHO%27%2C%20%27PEND%27%2C%20%27PRICE%27%2C%20%27CLOSD%27%2C%20%27TERM%27%2C%20%27WITH%27',
          StartDate: "#{date_format}%2000%3A00",
          today: "#{today_format}",
          todaysactivity: 'x',
          fourtharea: '',
          fiftharea: 'Display.asp',
          updatehotsheetdate: '0',
          where: '',
          Area: '-ALL-',
          Location: '-ALL-',
          proptype: 'RES%2C%20CND%2C%20LND%2C%20MUL%2C%20ACR%2C%20HIR%2C%20RNT',
          aReportDetails: 'BACK%2C%20EXP%2C%20NEW%2C%20optionp%2C%20PSHO%2C%20PEND%2C%20PRICE%2C%20CLOSD%2C%20TERM%2C%20WITH',
          narrow: 'true',
          sb: '0'
        }
        
        #$$Delayed::Worker.logger.debug "Is mechanize file2? #{page.is_a?(Mechanize::File)}"
        #$$Delayed::Worker.logger.debug "body2: #{page.body}"
        import(page) if page.is_a?(Mechanize::File)
      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      rescue Exception => e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e  
      end
    end
    
    
    
    def fetch_hotsheet_by_specified_dates(st,en)
      tries = 0
      result = nil
      begin
        Delayed::Worker.logger.debug "fetching hotsheet"
        date_format = st  #(Date.today - days).strftime('%m/%d/%Y')
        Delayed::Worker.logger.debug ">fetch date format: #{date_format}"
        today_format = en  #Date.today.strftime('%m/%d/%Y')
        Delayed::Worker.logger.debug ">fetch today format: #{today_format}"
        page = agent.post 'http://www.harmls.com/Reports/Scripts/display.asp', {
          formname: 'TodaysActivity',
          dateend: "#{today_format}%2023%3A59",
          datestart: "#{date_format}%2000%3A00",
          EndDate: "#{today_format}%2023%3A59",
          EndStatement: 'End%20of%20Activity%20Report%20for%20Today',
          heading: "#{date_format}",
          propertytype: '%22-ALL-%22',
          rpt: '%27BACK%27%2C%20%27EXP%27%2C%20%27NEW%27%2C%20%27optionp%27%2C%20%27PSHO%27%2C%20%27PEND%27%2C%20%27PRICE%27%2C%20%27CLOSD%27%2C%20%27TERM%27%2C%20%27WITH%27',
          StartDate: "#{date_format}%2000%3A00",
          today: "#{today_format}",
          todaysactivity: 'x',
          fourtharea: '',
          fiftharea: 'Display.asp',
          updatehotsheetdate: '0',
          where: '',
          Area: '-ALL-',
          Location: '-ALL-',
          proptype: 'RES%2C%20CND%2C%20LND%2C%20MUL%2C%20ACR%2C%20HIR%2C%20RNT',
          aReportDetails: 'BACK%2C%20EXP%2C%20NEW%2C%20optionp%2C%20PSHO%2C%20PEND%2C%20PRICE%2C%20CLOSD%2C%20TERM%2C%20WITH',
          narrow: 'true',
          sb: '0'
        }
        
        #$$Delayed::Worker.logger.debug "Is mechanize file2? #{page.is_a?(Mechanize::File)}"
        #$$Delayed::Worker.logger.debug "body2: #{page.body}"
        import(page) if page.is_a?(Mechanize::File)
      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      rescue Exception => e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e  
      end
    end
    
    
end