class Ntreis < Provider
  APPOINTMENT_CALL = {
    "AGENT" => "Agent",
    "BUILDE" => "Builder",
    "CSS" => "CSS",
    "OFFICE" => "Office",
    "OTHER" => "Other",
    "OWNER" => "Owner"
  }
  HEATING = {
    "ADWAHE" => "Additional Water Heater(s)",
    "CENAIRE" => "Central Air-Elec",
    "CENAIRG" => "Central Air-Gas",
    "CENHEAE" => "Central Heat-Elec",
    "CENHEAG" => "Central Heat-Gas",
    "DIRVEN" => "Direct Vent",
    "ELAIFI" => "Electrostatic Air Filter",
    "EVAPOR" => "Evaporation",
    "GASJET" => "Gas Jets",
    "GEOTHE" => "Geotherm",
    "HEAPUM" => "Heat Pump",
    "HUMIDI" => "Humidifier",
    "NOAIR" => "No Air",
    "NOHEA" => "No Heat",
    "OTHER" => "Other",
    "PAFLWA" => "Panel/Floor/Wall",
    "PROPAN" => "Propane",
    "SOLAR" => "Solar",
    "SPAHEA" => "Space Heater",
    "TWPICO" => "Two(+) Pipe (Condo)",
    "WINUNI" => "Window Unit",
    "ZONED" => "Zoned" 
  }
  LOTSIZE = {
    "10TO100A" => "10 Acres to 100 Acres",
    "1TO2P99A" => "1 Acre to 2.99 Acres",
    "3TO4P99A" => "3 Acres to 4.99 Acres",
    "5TO9P99A" => "5 Acres to 9.99 Acres",
    "COTOLO" => "Condo/Townhome Lot",
    "LTP5A" => "Less Than .5 Acre (not Zero)",
    "OVER100A" => "Over 100 Acres",
    "P5TOP99A" => ".5 Acre to .99 Acre",
    "ZERLOT" => "Zero Lot"
  }
  ASSOCIATION_FEE_FREQUENCY = {
    "ANNUAL" => "1",
    "MONTHL" => "12",
    "OTHER" => "1",
    "QUARTE" => "4",
    "SEMIAN" => "2"
  }
  EXTERIOR_BUILDINGS = {
    "BOIOPE" => "Boiler Operation",
    "BUNHOU" => "Bunk House",
    "MOBILE" => "Mobile",
    "NONE" => "None",
    "OTHER" => "Other",
    "SILO" => "Silo",
    "TENHOU" => "Tenant House",
    "WINERY" => "Winery",
    "WORKSH" => "Workshop"
  }
  EXTERIOR_FEATURES = {
    "ARENA" => "Arena",
    "ATTGRI" => "Attached Grill",
    "BALCON" => "Balcony",
    "BDWL" => "Boat Dock w/ lift",
    "BODOSL" => "Boat Dock/Slip",
    "COVDEC" => "Covered Deck",
    "COVPOR" => "Covered Porch(es)",
    "DECK" => "Deck",
    "EQUCEN" => "Equestrian Center",
    "GARDEN" => "Gardens",
    "GAZPER" => "Gazebo/Pergola",
    "GREENH" => "Greenhouse",
    "GUEQUA" => "Guest Quarters",
    "GUTTER" => "Gutters",
    "LIGSYS" => "Lighting System",
    "MOMISY" => "Mosquito Mist System",
    "Other" => "Other",
    "OUFIPI" => "Outdoor Fireplace/Pit",
    "OULICE" => "Outdoor Living Center",
    "PATCOV" => "Patio Covered",
    "PATOPE" => "Patio Open",
    "PRIHAN" => "Private Hangar",
    "PRLAST" => "Private Landing Strip",
    "RBP" => "RV/ Boat Parking",
    "RTDP" => "Roof Top Deck/Patio",
    "SATDIS" => "Satellite Dish",
    "SEENQU" => "Separate Entry Quarters",
    "SPOCOU" => "Sport Court",
    "SPRSYS" => "Sprinkler System",
    "STABAR" => "Stable/Barn",
    "STOBUI" => "Storage Building",
    "STOCEL" => "Storm Cellar",
    "TENCOU" => "Tennis Court(s)",
    "WORKSH" => "Workshop",
    "WOWEL" => "Workshop w/Electric"
  }
  SPECIAL_NOTES = {
    "AERPHO" => "Aerial Photo",
    "AFFHOU" => "Affordable Housing",
    "BUTOSU" => "Build to Suit",
    "DEERES" => "Deed Restrictions",
    "DEHOTE" => "Deep Hole Test",
    "ENSTCO" => "Environ. Study Complete",
    "FESTAV" => "Feasibility Study Available",
    "FLOEAS" => "Flowage Easement",
    "FLOPLA" => "Flood Plain",
    "HIGHLI" => "Highline",
    "HISTOR" => "Historical",
    "HUD" => "HUD",
    "INWERE" => "Inland/Wetland Restrictions",
    "MEADGU" => "Meets ADA Guidelines",
    "OTHER" => "Other",
    "OWAG" => "Owner/ Agent",
    "PERTES" => "Perc Test",
    "PHICO" => "Phase I Complete",
    "PHIICO" => "Phase II Complete",
    "PIPELI" => "Pipeline",
    "RESECO" => "Res. Service Contract",
    "ROFR" => "Right of First Refusal",
    "SALC" => "Senior/Adult Living Community",
    "SE8QU" => "Section 8 Qualified",
    "SPCOPR" => "Special Contracts/Provisions",
    "SPEASS" => "Special Assessments",
    "SURAVA" => "Survey Available",
    "UTIEAS" => "Utility Easement",
    "VEFLIN" => "Verify Flood Insurance",
    "VEROTA" => "Verify Rollback Tax",
    "VETAEX" => "Verify Tax Exemptions"
  }
  YEAR_BUILT_DETAILS_BOOL = {
    "NCC" => "Y",
    "NCI" => "Y",
    "PREOWN" => "N",
    "PROPOS" => "N",
    "UNKNOW" => "N"
  }
  CITY = {
    "1" => "Abbott",
    "10" => "Adrian",
    "100" => "Bedford",
    "1000" => "Mount Pleasant",
    "1001" => "Mount Selman",
    "1002" => "Mount Vernon",
    "1003" => "Mountain",
    "1004" => "Mountain Valley",
    "1005" => "Mt Herman",
    "1006" => "Mt Zion",
    "1007" => "Muenster",
    "1008" => "Muleshoe",
    "1009" => "Mullin",
    "101" => "Bee Cave",
    "1010" => "Mumford",
    "1011" => "Munday",
    "1012" => "Murchison",
    "1013" => "Murphy",
    "1014" => "Mustang",
    "1015" => "Mustang Ridge",
    "1016" => "Myra",
    "1017" => "Nacogdoches",
    "1018" => "Nance",
    "1019" => "Naples",
    "102" => "Beeville",
    "1020" => "Nash",
    "1021" => "Nassau Bay",
    "1022" => "Natalia",
    "1023" => "Navarro",
    "1024" => "Navasota",
    "1025" => "Nazareth",
    "1026" => "Neches",
    "1027" => "Nederland",
    "1028" => "Needville",
    "1029" => "Nelson",
    "103" => "Bellaire",
    "1030" => "Nelta",
    "1031" => "Nemo",
    "1032" => "Nesbitt",
    "1033" => "Nevada",
    "1034" => "New Baden",
    "1035" => "New Berlin",
    "1036" => "New Boston",
    "1037" => "New Braunfels",
    "1038" => "New Chapel Hill",
    "1039" => "New Home",
    "104" => "Bellevue",
    "1040" => "New Hope",
    "1041" => "New London",
    "1042" => "New Ringold",
    "1043" => "New Salem",
    "1044" => "New Summerfield",
    "1045" => "New Waverly",
    "1046" => "New Woodville",
    "1047" => "Newark",
    "1048" => "Newcastle",
    "1049" => "Newton",
    "105" => "Bellmead",
    "1050" => "Neylandville",
    "1051" => "Nida",
    "1052" => "Niederwald",
    "1053" => "Nixon",
    "1054" => "Nocona",
    "1055" => "Nolan",
    "1056" => "Nolanville",
    "1057" => "Nome",
    "1058" => "Noonday",
    "1059" => "Nordheim",
    "106" => "Bells",
    "1060" => "Normangee",
    "1061" => "North Cleveland",
    "1062" => "North Richland Hills",
    "1063" => "Northcrest",
    "1064" => "Northlake",
    "1065" => "Northpole",
    "1066" => "Novice",
    "1067" => "Nunlee",
    "1068" => "Oak Grove",
    "1069" => "Oak Leaf",
    "107" => "Bellville",
    "1070" => "Oak Point",
    "1071" => "Oak Ridge",
    "1072" => "Oak Ridge North",
    "1073" => "Oak Valley",
    "1074" => "Oakhurst",
    "1075" => "Oakland",
    "1076" => "Oakman",
    "1077" => "Oakville",
    "1078" => "Oakwood",
    "1079" => "OBrien",
    "108" => "Belton",
    "1080" => "Odell",
    "1081" => "Odem",
    "1082" => "Odessa",
    "1083" => "ODonnell",
    "1084" => "Oglesby",
    "1085" => "Oil Center",
    "1086" => "Oklaunion",
    "1087" => "Old River Winfree",
    "1088" => "Olden",
    "1089" => "Olmos Park",
    "109" => "Ben Franklin",
    "1090" => "Olney",
    "1091" => "Olton",
    "1092" => "Omaha",
    "1093" => "Onalaska",
    "1094" => "Opdyke West",
    "1095" => "Orange",
    "1096" => "Orange Grove",
    "1097" => "Orchard",
    "1098" => "Ordgrant",
    "1099" => "Ore City",
    "11" => "Agua Dulce",
    "110" => "Ben Wheeler",
    "1100" => "Otto",
    "1101" => "Overbrook",
    "1102" => "Overton",
    "1103" => "Ovilla",
    "1104" => "Owensville",
    "1105" => "Oyster Creek",
    "1106" => "Ozona",
    "1107" => "Paint Rock",
    "1108" => "Palacios",
    "1109" => "Palestine",
    "111" => "Benavides",
    "1110" => "Palisades",
    "1111" => "Palm Valley",
    "1112" => "Palmer",
    "1113" => "Palmhurst",
    "1114" => "Palmview",
    "1115" => "Palo Pinto",
    "1116" => "Paluxy",
    "1117" => "Pampa",
    "1118" => "Panhandle",
    "1119" => "Panna Maria",
    "112" => "Benbrook",
    "1120" => "Panola",
    "1121" => "Panorama Village",
    "1122" => "Pantego",
    "1123" => "Paradise",
    "1124" => "Paris",
    "1125" => "Parker",
    "1126" => "Paron",
    "1127" => "Pasadena",
    "1128" => "Pattison",
    "1129" => "Patton Village",
    "113" => "Benchley",
    "1130" => "Pattonville",
    "1131" => "Payne Springs",
    "1132" => "Peadenville",
    "1133" => "Pearcy",
    "1134" => "Pearland",
    "1135" => "Pearsall",
    "1136" => "Peaster",
    "1137" => "Pecan Gap",
    "1138" => "Pecan Hill",
    "1139" => "Pecos",
    "114" => "Bend",
    "1140" => "Pelican Bay",
    "1141" => "Pendleton",
    "1142" => "Penelope",
    "1143" => "Penitas",
    "1144" => "Peoria",
    "1145" => "Perla",
    "1146" => "Pernitas Point",
    "1147" => "Perrin",
    "1148" => "Perry",
    "1149" => "Perryton",
    "115" => "Benjamin",
    "1150" => "Petersburg",
    "1151" => "Petrolia",
    "1152" => "Petronila",
    "1153" => "Petty",
    "1154" => "Pettyview",
    "1155" => "Pflugerville",
    "1156" => "Pharr",
    "1157" => "Pickens",
    "1158" => "Pickett",
    "1159" => "Pickton",
    "116" => "Bennington",
    "1160" => "Pickwick",
    "1161" => "Pike",
    "1162" => "Pilot Grove",
    "1163" => "Pilot Point",
    "1164" => "Pine Forest",
    "1165" => "Pine Island",
    "1166" => "Pinehurst",
    "1167" => "Pineland",
    "1168" => "Piney Point Village",
    "1169" => "Pittsburg",
    "117" => "Benton",
    "1170" => "Plains",
    "1171" => "Plainview",
    "1172" => "Plano",
    "1173" => "Platter",
    "1174" => "Pleak",
    "1175" => "Pleasant Hill",
    "1176" => "Pleasant Valley",
    "1177" => "Pleasanton",
    "1178" => "Pledger",
    "1179" => "Plum Grove",
    "118" => "Berlin",
    "1180" => "Poetry",
    "1181" => "Point",
    "1182" => "Point Blank",
    "1183" => "Point Comfort",
    "1184" => "Point Decar",
    "1185" => "Ponder",
    "1186" => "Pontotoc",
    "1187" => "Poolville",
    "1188" => "Port Aransas",
    "1189" => "Port Arthur",
    "119" => "Berryville",
    "1190" => "Port Isabel",
    "1191" => "Port Lavaca",
    "1192" => "Port Neches",
    "1193" => "Portland",
    "1194" => "Possum Kingdom Lake",
    "1195" => "Post",
    "1196" => "Post Oak Bend",
    "1197" => "Poteet",
    "1198" => "Poth",
    "1199" => "Pottsboro",
    "12" => "Ahloso",
    "120" => "Bertram",
    "1200" => "Pottsville",
    "1201" => "Powderly",
    "1202" => "Powell",
    "1203" => "Poynor",
    "1204" => "Prairie Hill",
    "1205" => "Prairie View",
    "1206" => "Premont",
    "1207" => "Presidio",
    "1208" => "Price",
    "1209" => "Priddy",
    "121" => "Bethel",
    "1210" => "Primera",
    "1211" => "Princeton",
    "1212" => "Proctor",
    "1213" => "Progreso",
    "1214" => "Progreso Lakes",
    "1215" => "Prosper",
    "1216" => "Purdon",
    "1217" => "Purmela",
    "1218" => "Purves",
    "1219" => "Putnam",
    "122" => "Beverly Hills",
    "1220" => "Pyote",
    "1221" => "Quanah",
    "1222" => "Queen City",
    "1223" => "Quemado",
    "1224" => "Quinlan",
    "1225" => "Quintana",
    "1226" => "Quitaque",
    "1227" => "Quitman",
    "1228" => "Rainbow",
    "1229" => "Ralls",
    "123" => "Bevil Oaks",
    "1230" => "Rancho Viejo",
    "1231" => "Randlett",
    "1232" => "Randolph",
    "1233" => "Ranger",
    "1234" => "Rangerville",
    "1235" => "Rankin",
    "1236" => "Ratcliff",
    "1237" => "Ravenna",
    "1238" => "Ravia",
    "1239" => "Raymondville",
    "124" => "Big Lake",
    "1240" => "Reagan",
    "1241" => "Red Lick",
    "1242" => "Red Oak",
    "1243" => "Redwater",
    "1244" => "Reese",
    "1245" => "Refugio",
    "1246" => "Reklaw",
    "1247" => "Rendon",
    "1248" => "Reno",
    "1249" => "Retreat",
    "125" => "Big Sandy",
    "1250" => "Reydon",
    "1251" => "Rhea Mills",
    "1252" => "Rhome",
    "1253" => "Rice",
    "1254" => "Richardson",
    "1255" => "Richland",
    "1256" => "Richland Hills",
    "1257" => "Richland Springs",
    "1258" => "Richmond",
    "1259" => "Richwood",
    "126" => "Big Spring",
    "1260" => "Ridge",
    "1261" => "Ridgeway",
    "1262" => "Riesel",
    "1263" => "Ringgold",
    "1264" => "Ringold",
    "1265" => "Rio Bravo",
    "1266" => "Rio Hondo",
    "1267" => "Rio Medina",
    "1268" => "Rio Vista",
    "1269" => "Rising Star",
    "127" => "Birthright",
    "1270" => "River Oaks",
    "1271" => "Riverside",
    "1272" => "Roane",
    "1273" => "Roanoke",
    "1274" => "Roaring Springs",
    "1275" => "Robert Lee",
    "1276" => "Roberta",
    "1277" => "Robinson",
    "1278" => "Robs",
    "1279" => "Robstown",
    "128" => "Bishop",
    "1280" => "Roby",
    "1281" => "Rochester",
    "1282" => "Rockdale",
    "1283" => "Rockett",
    "1284" => "Rockport",
    "1285" => "Rocksprings",
    "1286" => "Rockwall",
    "1287" => "Rockwood",
    "1288" => "Rocky Mound",
    "1289" => "Roff",
    "129" => "Bishop Hills",
    "1290" => "Rogers",
    "1291" => "Rolia",
    "1292" => "Roll",
    "1293" => "Rollingwood",
    "1294" => "Roman Forest",
    "1295" => "Ropesville",
    "1296" => "Roscoe",
    "1297" => "Rose",
    "1298" => "Rose Hill Acres",
    "1299" => "Rosebud",
    "13" => "Alamo",
    "130" => "Bismarck",
    "1300" => "Rosenberg",
    "1301" => "Ross",
    "1302" => "Rosser",
    "1303" => "Rosston",
    "1304" => "Rotan",
    "1305" => "Round Mountain",
    "1306" => "Round Rock",
    "1307" => "Round Top",
    "1308" => "Rowlett",
    "1309" => "Roxton",
    "131" => "Bivins",
    "1310" => "Royal",
    "1311" => "Royse City",
    "1312" => "Rubottom",
    "1313" => "Rule",
    "1314" => "Runaway Bay",
    "1315" => "Runge",
    "1316" => "Rusk",
    "1317" => "Russett",
    "1318" => "Sabinal",
    "1319" => "Sachse",
    "132" => "Blackwell",
    "1320" => "Sacul",
    "1321" => "Sadler",
    "1322" => "Saginaw",
    "1323" => "Saint Hedwig",
    "1324" => "Saint Jo",
    "1325" => "Saint Paul",
    "1326" => "Salado",
    "1327" => "Salem",
    "1328" => "Salesville",
    "1329" => "Saltillo",
    "133" => "Blakely",
    "1330" => "San Angelo",
    "1331" => "San Antonio",
    "1332" => "San Augustine",
    "1333" => "San Benito",
    "1334" => "San Diego",
    "1335" => "San Elizario",
    "1336" => "San Felipe",
    "1337" => "San Juan",
    "1338" => "San Leanna",
    "1339" => "San Marcos",
    "134" => "Blanco",
    "1340" => "San Patricio",
    "1341" => "San Perlita",
    "1342" => "San Saba",
    "1343" => "Sanctuary",
    "1344" => "Sandbluff",
    "1345" => "Sanderson",
    "1346" => "Sanger",
    "1347" => "Sansom Park",
    "1348" => "Santa Anna",
    "1349" => "Santa Fe",
    "135" => "Blanket",
    "1350" => "Santa Rosa",
    "1351" => "Santo",
    "1352" => "Sardis",
    "1353" => "Satin",
    "1354" => "Savoy",
    "1355" => "Sawyer",
    "1356" => "Schertz",
    "1357" => "Schulenburg",
    "1358" => "Schwertner",
    "1359" => "Scotland",
    "136" => "Blessing",
    "1360" => "Scottsville",
    "1361" => "Scroggins",
    "1362" => "Scurry",
    "1363" => "Seabrook",
    "1364" => "Seadrift",
    "1365" => "Seagoville",
    "1366" => "Seagraves",
    "1367" => "Sealy",
    "1368" => "Seguin",
    "1369" => "Selden",
    "137" => "Bloomburg",
    "1370" => "Selma",
    "1371" => "Selman City",
    "1372" => "Seminole",
    "1373" => "Seven Oaks",
    "1374" => "Seven Points",
    "1375" => "Seymore",
    "1376" => "Seymour",
    "1377" => "Shady Shores",
    "1378" => "Shamrock",
    "1379" => "Shavano Park",
    "138" => "Blooming Grove",
    "1380" => "Shenandoah",
    "1381" => "Shepherd",
    "1382" => "Sherman",
    "1383" => "Shiner",
    "1384" => "Shirley",
    "1385" => "Shoreacres",
    "1386" => "Sidney",
    "1387" => "Silo",
    "1388" => "Silsbee",
    "1389" => "Silverton",
    "139" => "Blossom",
    "1390" => "Simms",
    "1391" => "Simonton",
    "1392" => "Sinton",
    "1393" => "Skelly",
    "1394" => "Slidell",
    "1395" => "Smiley",
    "1396" => "Smithland",
    "1397" => "Smithville",
    "1398" => "Smyer",
    "1399" => "Snook",
    "14" => "Alamo Heights",
    "140" => "Blue",
    "1400" => "Snyder",
    "1401" => "Social Hill",
    "1402" => "Socorro",
    "1403" => "Somerset",
    "1404" => "Somerville",
    "1405" => "Sonora",
    "1406" => "Sour Lake",
    "1407" => "South Bend",
    "1408" => "South Houston",
    "1409" => "South Mountain",
    "141" => "Blue Mound",
    "1410" => "South Padre Island",
    "1411" => "Southlake",
    "1412" => "Southmayd",
    "1413" => "Southside Place",
    "1414" => "Spearman",
    "1415" => "Spicewood",
    "1416" => "Splendora",
    "1417" => "Spofford",
    "1418" => "Spring",
    "1419" => "Spring Branch",
    "142" => "Blue Ridge",
    "1420" => "Spring Valley",
    "1421" => "Springlake",
    "1422" => "Springtown",
    "1423" => "Spur",
    "1424" => "St. Hedwig",
    "1425" => "Stafford",
    "1426" => "Stagecoach",
    "1427" => "Stamford",
    "1428" => "Stanton",
    "1429" => "Staples",
    "143" => "Blue Springs",
    "1430" => "Star",
    "1431" => "Star Harbor",
    "1432" => "Stephenville",
    "1433" => "Sterling",
    "1434" => "Stockdale",
    "1435" => "Stonewall",
    "1436" => "Stratford",
    "1437" => "Strawn",
    "1438" => "Streetman",
    "1439" => "Strong City",
    "144" => "Bluegrove",
    "1440" => "Sudan",
    "1441" => "Sugar Land",
    "1442" => "Sulphur Bluff",
    "1443" => "Sulphur Springs",
    "1444" => "Sumner",
    "1445" => "Sun Valley",
    "1446" => "Sundown",
    "1447" => "Sunnyvale",
    "1448" => "Sunray",
    "1449" => "Sunrise Beach Village",
    "145" => "Bluff Dale",
    "1450" => "Sunset",
    "1451" => "Sunset Valley",
    "1452" => "Surfside Beach",
    "1453" => "Sweeny",
    "1454" => "Sweetwater",
    "1455" => "Swink",
    "1456" => "Sylvester",
    "1457" => "Taft",
    "1458" => "Tahoka",
    "1459" => "Talco",
    "146" => "Bluffton",
    "1460" => "Talpa",
    "1461" => "Talty",
    "1462" => "Tatum",
    "1463" => "Taylor",
    "1464" => "Taylor Lake Village",
    "1465" => "Teague",
    "1466" => "Tehuacana",
    "1467" => "Telephone",
    "1468" => "Telico",
    "1469" => "Temple",
    "147" => "Blum",
    "1470" => "Tenaha",
    "1471" => "Tennessee Colony",
    "1472" => "Terrell",
    "1473" => "Terrell Hills",
    "1474" => "Texarkana",
    "1475" => "Texas",
    "1476" => "Texhoma",
    "1477" => "Texline",
    "1478" => "Thackerville",
    "1479" => "The Colony",
    "148" => "Boerne",
    "1480" => "The Hills",
    "1481" => "Thompsons",
    "1482" => "Thorndale",
    "1483" => "Thornton",
    "1484" => "Thorntonville",
    "1485" => "Thrall",
    "1486" => "Three Rivers",
    "1487" => "Throckmorton",
    "1488" => "Tiki Island",
    "1489" => "Tilden",
    "149" => "Bogata",
    "1490" => "Timbercreek Canyon",
    "1491" => "Timpson",
    "1492" => "Tioga",
    "1493" => "Tira",
    "1494" => "Tishomingo",
    "1495" => "Tivoli",
    "1496" => "Toco",
    "1497" => "Todd Mission",
    "1498" => "Tolar",
    "1499" => "Tom",
    "15" => "Alba",
    "150" => "Bokchito",
    "1500" => "Tom Bean",
    "1501" => "Tomball",
    "1502" => "Tool",
    "1503" => "Tornillo",
    "1504" => "Tow",
    "1505" => "Toyah",
    "1506" => "Traskwood",
    "1507" => "Trent",
    "1508" => "Trenton",
    "1509" => "Trinidad",
    "151" => "Bokhoma",
    "1510" => "Trinity",
    "1511" => "Trophy Club",
    "1512" => "Troup",
    "1513" => "Troy",
    "1515" => "Tulia",
    "1516" => "Turkey",
    "1517" => "Turney",
    "1518" => "Tuscola",
    "1519" => "Tye",
    "152" => "Bonham",
    "1520" => "Tyler",
    "1521" => "Uhland",
    "1522" => "Uncertain",
    "1523" => "Unger",
    "1524" => "Union Grove",
    "1525" => "Union Hill",
    "1526" => "Union Valley",
    "1527" => "Universal City",
    "1528" => "University Park",
    "1529" => "Utica",
    "153" => "Bonnerdale",
    "1530" => "Uvalde",
    "1531" => "Valentine",
    "1532" => "Valera",
    "1533" => "Valiant",
    "1534" => "Valley Mills",
    "1535" => "Valley Spring",
    "1536" => "Valley View",
    "1537" => "Van",
    "1538" => "Van Alstyne",
    "1539" => "Van Horn",
    "154" => "Bonney",
    "1540" => "Van Vleck",
    "1541" => "Vega",
    "1542" => "Venus",
    "1543" => "Vernon",
    "1544" => "Victoria",
    "1545" => "Vidor",
    "1546" => "Vimy",
    "1547" => "Vinton",
    "1548" => "Von Ormy",
    "1549" => "Voss",
    "155" => "Booker",
    "1550" => "Waco",
    "1551" => "Wade",
    "1552" => "Wadsworth",
    "1553" => "Waelder",
    "1554" => "Wake",
    "1555" => "Walburg",
    "1556" => "Waller",
    "1557" => "Wallis",
    "1558" => "Walnut Springs",
    "1559" => "Walters",
    "156" => "Boonsville",
    "1560" => "Warren",
    "1561" => "Washington",
    "1562" => "Waskom",
    "1563" => "Watauga",
    "1564" => "Watson",
    "1565" => "Waxahachie",
    "1566" => "Weatherford",
    "1567" => "Weaver",
    "1568" => "Webster",
    "1569" => "Weimar",
    "157" => "Boswell",
    "1570" => "Weinert",
    "1571" => "Weir",
    "1572" => "Wellborn",
    "1573" => "Wellington",
    "1574" => "Wellman",
    "1575" => "Wells",
    "1576" => "Weslaco",
    "1577" => "West",
    "1578" => "West Columbia",
    "1579" => "West Lake Hills",
    "158" => "Bovina",
    "1580" => "West Orange",
    "1581" => "West Tawakoni",
    "1582" => "West University Place",
    "1583" => "Westbrook",
    "1584" => "Westlake",
    "1585" => "Westminster",
    "1586" => "Weston",
    "1587" => "Westover Hills",
    "1588" => "Westworth Village",
    "1589" => "Wharton",
    "159" => "Bowie",
    "1590" => "Wheeler",
    "1591" => "Wheelock",
    "1592" => "White Deer",
    "1593" => "White Oak",
    "1594" => "White Settlement",
    "1595" => "Whiteface",
    "1596" => "Whitehouse",
    "1597" => "Whitesboro",
    "1598" => "Whitewright",
    "1599" => "Whitman",
    "16" => "Albany",
    "160" => "Boyce",
    "1600" => "Whitney",
    "1601" => "Whitsett",
    "1602" => "Whitt",
    "1603" => "Wichita Falls",
    "1604" => "Wickett",
    "1605" => "Willis",
    "1606" => "Willow City",
    "1607" => "Willow Park",
    "1608" => "Wills Point",
    "1609" => "Wilmer",
    "161" => "Boyd",
    "1610" => "Wilson",
    "1611" => "Wimberley",
    "1612" => "Windcrest",
    "1613" => "Windom",
    "1614" => "Windthorst",
    "1615" => "Winfield",
    "1616" => "Wink",
    "1617" => "Winnsboro",
    "1618" => "Winona",
    "1619" => "Winters",
    "162" => "Brackettville",
    "1620" => "Wixon Valley",
    "1621" => "Woden",
    "1622" => "Wolfe City",
    "1623" => "Woodbranch",
    "1624" => "Woodcreek",
    "1625" => "Woodlawn",
    "1626" => "Woodloch",
    "1627" => "Woodsboro",
    "1628" => "Woodson",
    "1629" => "Woodville",
    "163" => "Brad",
    "1630" => "Woodway",
    "1631" => "Wortham",
    "1632" => "Wright City",
    "1633" => "Wylie",
    "1634" => "Yancey",
    "1635" => "Yantis",
    "1636" => "Yoakum",
    "1637" => "York",
    "1638" => "Yreka",
    "1639" => "Yuba",
    "164" => "Brady",
    "1640" => "Zapata",
    "1641" => "Zavalla",
    "1642" => "Zephyr",
    "1644" => "Antibes",
    "1645" => "Moho Caye",
    "1647" => "Atoka",
    "1648" => "Purcell",
    "1649" => "The Woodlands",
    "165" => "Brandon",
    "1650" => "Hugo",
    "1653" => "Lanark",
    "1654" => "Lima",
    "1655" => "Moyers",
    "1656" => "Lubbock",
    "1657" => "San Jose",
    "1658" => "Brazos",
    "1659" => "Mexico",
    "166" => "Brashear",
    "1660" => "Belize",
    "1661" => "Costa Rica",
    "1662" => "Peru",
    "1664" => "Southerland",
    "1665" => "Burneyville",
    "1666" => "Nashoba",
    "1667" => "Muse",
    "1668" => "Utila",
    "1669" => "Montego Bay",
    "167" => "Brazoria",
    "1670" => "Playa del Carmen",
    "1671" => "Piedras Negras",
    "1673" => "Sulphur",
    "1674" => "Cedar Creek",
    "1675" => "No City",
    "1676" => "Sutherland Springs",
    "1677" => "Duncan",
    "1679" => "Celles",
    "168" => "Breckenridge",
    "1680" => "Edmon-Alberta",
    "1681" => "Savannah",
    "1682" => "Monterrey",
    "1683" => "Elwood",
    "1684" => "Tupelo",
    "1685" => "Anguilla",
    "1686" => "Batonrouge",
    "1687" => "Clark",
    "1688" => "Oklahoma City",
    "1689" => "Soper",
    "169" => "Bremond",
    "1690" => "New Fairview",
    "1691" => "Avignon",
    "1692" => "Cannes",
    "1693" => "Mainz",
    "1694" => "Trumbull",
    "1695" => "Praslin",
    "1696" => "Hawk Cove",
    "1697" => "Clinton",
    "1698" => "Providence Village",
    "1699" => "Terral",
    "17" => "Aledo",
    "170" => "Brenham",
    "1700" => "Ninnekah",
    "1701" => "Burkeville",
    "1702" => "Hollis",
    "1703" => "Bland",
    "1704" => "Joplin",
    "1705" => "Tuscany",
    "1706" => "Castiglion Fiorentino",
    "1707" => "Dish",
    "1708" => "Castries",
    "1709" => "Durban",
    "171" => "Briarcliff",
    "1710" => "Cash",
    "1712" => "Duplex",
    "1713" => "Monkstown",
    "1714" => "Wagner",
    "1715" => "Wake Village",
    "1717" => "Mougins",
    "1719" => "St. Tropez",
    "172" => "Briaroaks",
    "1721" => "Nice",
    "1722" => "Villefranche",
    "1723" => "St. Andrews",
    "1724" => "Jaco",
    "1725" => "Geneva",
    "1726" => "Rio De Janeiro",
    "1727" => "Rome",
    "1728" => "Talihina",
    "1729" => "Eagle Nest",
    "173" => "Bridge",
    "1730" => "Prairie Lea",
    "1732" => "Vichy",
    "1733" => "Agen",
    "1734" => "Bordeaux",
    "1735" => "Aix-en-Provence",
    "1737" => "Montpellier",
    "1738" => "Nimes",
    "1739" => "Uz&#xE8;s",
    "174" => "Bridgeport",
    "1740" => "Bagnols",
    "1741" => "Vaucluse",
    "1742" => "St. Emilion",
    "1743" => "Le Mans",
    "1744" => "Beziers",
    "1745" => "St. Raphael",
    "1746" => "Pertuis",
    "1747" => "Ch&#xE2;teauneuf-du-Pape",
    "1748" => "Lyon",
    "1749" => "Turks &amp; Caicos Islands",
    "175" => "Briggs",
    "1750" => "P&#xE9;zenas",
    "1751" => "Romans",
    "1752" => "Caen",
    "1753" => "Limoges",
    "1754" => "Quimper",
    "1755" => "Arles",
    "1756" => "Draguignan",
    "1757" => "Manosque",
    "1758" => "Toulouse",
    "1759" => "Sens",
    "176" => "Briscoe",
    "1760" => "Salon-de-Provence",
    "1761" => "Guingamp",
    "1762" => "Pont-Saint-Esprit",
    "1763" => "Panama City Beach",
    "1764" => "Carrabelle",
    "1765" => "Maysfield",
    "1766" => "Knox City",
    "1767" => "Red River",
    "1769" => "South Lake Tahoe",
    "177" => "Bristol",
    "1770" => "Oranjestad",
    "1771" => "New Caney",
    "1772" => "Winter Haven",
    "1773" => "Old Glory",
    "1774" => "Varna",
    "1775" => "Arecibo",
    "1776" => "Canon City",
    "1777" => "Penn Valley",
    "1778" => "Bolivar",
    "1779" => "Bozeman",
    "178" => "Broaddus",
    "1780" => "Gardiner",
    "1781" => "Gallatin Gateway",
    "1782" => "Hervey Bay",
    "1783" => "Gilbert",
    "1785" => "Hideaway",
    "1786" => "Dew",
    "1787" => "Eleuthera",
    "1788" => "New Providence",
    "1789" => "Wilburton",
    "179" => "Brock",
    "1791" => "Terra Ceia",
    "1792" => "Palmetto",
    "1793" => "Parrish",
    "1794" => "Lakewood Ranch",
    "1795" => "Anna Maria",
    "1796" => "Holmes Beach",
    "1797" => "Norwalk",
    "1798" => "Montilly",
    "1799" => "Cancun",
    "18" => "Alexander",
    "180" => "Broken Bow",
    "1800" => "Belfalls",
    "1801" => "Birome",
    "1802" => "Burlington",
    "1803" => "Danner",
    "1806" => "Heartland",
    "1808" => "Holly Lake Ranch",
    "181" => "Bromide",
    "1811" => "Riverby",
    "1812" => "Bonita Springs",
    "1813" => "Pray",
    "1814" => "Crested Butte",
    "1815" => "Vilas",
    "1816" => "Evres",
    "1817" => "Carolina Beach",
    "1818" => "Fort Payne",
    "1819" => "Layou",
    "182" => "Bronte",
    "1820" => "The Crane",
    "1821" => "Sosua",
    "1822" => "Cabarete",
    "1823" => "Inverness",
    "1824" => "Poteau",
    "1825" => "Remsenburg",
    "1826" => "Tigard",
    "1827" => "Algoa",
    "1828" => "San Salvador",
    "1829" => "Antiguo Cuscaltan",
    "183" => "Brookesmith",
    "1830" => "Sonsonate",
    "1831" => "Cojutepeque",
    "1832" => "St. Simons Island",
    "1833" => "Santa Ana",
    "1834" => "La Libertad",
    "1835" => "La Union",
    "1836" => "Metapan",
    "1837" => "Usulutan",
    "1838" => "El Vendrell",
    "1839" => "McAlester",
    "184" => "Brookshire",
    "1840" => "Panama City",
    "1841" => "Playa Blanca",
    "1842" => "Rio Hato",
    "1843" => "Costa del Este",
    "1844" => "Cartago",
    "1845" => "Hunt",
    "1846" => "Tuskahoma",
    "1847" => "Sedona",
    "1848" => "Brunswick",
    "1849" => "North Port",
    "185" => "Brookside",
    "1850" => "Sarasota",
    "1851" => "Mc Gregor",
    "1852" => "Blainville",
    "1853" => "Santo Domingo",
    "1854" => "Miami Beach",
    "1855" => "Las Vegas",
    "1857" => "San Miguel",
    "1858" => "Loreto",
    "1859" => "Managua",
    "186" => "Brookston",
    "1860" => "Guatemala City",
    "1861" => "Angeot",
    "1862" => "Haughton",
    "1863" => "Carlsbad",
    "1864" => "Christoval",
    "1865" => "Colorado City",
    "1866" => "Eula",
    "1868" => "Hamby",
    "1869" => "Hodges",
    "187" => "Browndell",
    "1871" => "Millersview",
    "1872" => "Ovalo",
    "1873" => "Peacock",
    "1874" => "Potosi",
    "1875" => "Sterling City",
    "1876" => "Wingate",
    "1877" => "Sierra Blanca",
    "1878" => "Great Exuma",
    "1879" => "Cebu City",
    "188" => "Brownfield",
    "1880" => "Caloocan",
    "1881" => "Makati",
    "1882" => "Mandaue City",
    "1883" => "Manila",
    "1884" => "Lapu-Lapu City",
    "1885" => "Las Pinas",
    "1886" => "Malabon",
    "1887" => "Mandaluyong",
    "1888" => "Marikina",
    "1889" => "Muntinglupa",
    "189" => "Brownsboro",
    "1890" => "Navotas",
    "1891" => "Paranaque",
    "1892" => "Pasay",
    "1893" => "Pasig",
    "1894" => "Pateros",
    "1895" => "Quezon City",
    "1896" => "San Juan del Monte",
    "1897" => "Taguig",
    "1898" => "Valenzuela",
    "1899" => "Ocean City",
    "19" => "Alice",
    "190" => "Brownsville",
    "1900" => "Clearwater",
    "1901" => "De Cordova",
    "1902" => "Crossville",
    "1903" => "Dudley",
    "1904" => "Funston",
    "1905" => "Grosvenor",
    "1906" => "Lake Brownwood",
    "1907" => "Lohn",
    "1908" => "London",
    "1909" => "Noodle",
    "191" => "Brownwood",
    "1910" => "Nugent",
    "1911" => "Oplin",
    "1912" => "Paducah",
    "1914" => "DENTON VALLEY",
    "192" => "Bruceville Eddy",
    "1920" => "Roatan",
    "1921" => "Cortona",
    "1922" => "Grand Cayman",
    "1923" => "Indian Hills",
    "1924" => "Gros Islet",
    "1925" => "Comfort",
    "1926" => "Snoqualmie",
    "1927" => "Chicago",
    "1928" => "Derby",
    "1929" => "Falcon",
    "193" => "Bryan",
    "1931" => "Divide",
    "1932" => "Winter Park",
    "1933" => "Bali",
    "1934" => "Pagosa Springs",
    "1935" => "Corozal",
    "1936" => "Cabo San Lucas",
    "1937" => "St Paul",
    "1938" => "Ringling",
    "1939" => "Crystal Beach",
    "194" => "Bryant",
    "1940" => "Availles-sur-Seiche",
    "1941" => "La Romana",
    "1942" => "Morgans Point Resort",
    "1943" => "Little River Academy",
    "1944" => "Cheney",
    "1945" => "Indianapolis",
    "1946" => "Carmel",
    "1947" => "Glen Cove",
    "1948" => "Kerrville",
    "1949" => "NTREIS TEST ONLY",
    "195" => "Bryson",
    "1950" => "Lovell",
    "1951" => "Provincetown",
    "1952" => "Blythe",
    "1953" => "Ipswich",
    "1954" => "Santa Marta",
    "1955" => "Humacao",
    "1956" => "San Pedro",
    "1957" => "Tarcoles",
    "1958" => "Chetumal",
    "1959" => "Slaton",
    "196" => "Buchanan Dam",
    "1960" => "Blacksburg",
    "1961" => "Alamogordo",
    "1962" => "Sayre",
    "1963" => "Elk City",
    "1964" => "Cordell",
    "1965" => "Bentonville",
    "1966" => "Pucon",
    "1967" => "Helena",
    "1968" => "Lolita",
    "1969" => "Barling",
    "197" => "Buckholts",
    "1970" => "Bella Vista",
    "1971" => "Bethel Heights",
    "1972" => "Booneville",
    "1973" => "Branch",
    "1974" => "Cave Springs",
    "1975" => "Centerton",
    "1976" => "Central City",
    "1977" => "Conway",
    "1978" => "Dardanelle",
    "1979" => "Elkins",
    "198" => "Buckingham",
    "1980" => "Farmington",
    "1981" => "Fort Smith",
    "1982" => "Hatfield",
    "1983" => "Gravette",
    "1984" => "Hackett",
    "1985" => "Lavaca",
    "1986" => "Little Rock",
    "1987" => "Lowell",
    "1988" => "Magazine",
    "1989" => "Mena",
    "199" => "Buda",
    "1990" => "Mountain Home",
    "1991" => "Mount Ida",
    "1992" => "Mountainburg",
    "1993" => "Springdale",
    "1994" => "Van Buren",
    "1995" => "Waldron",
    "1996" => "Arkoma",
    "1997" => "Heavener",
    "1998" => "Muldrow",
    "1999" => "Muskogee",
    "2" => "Abernathy",
    "20" => "Allen",
    "200" => "Buffalo",
    "2000" => "Wister",
    "2001" => "Evansville",
    "2002" => "Lenexa",
    "2003" => "Vinita",
    "2004" => "Valliant",
    "2005" => "Idabel",
    "2006" => "Eagletown",
    "2007" => "Marlow",
    "2008" => "Chatham",
    "2009" => "Monrovia",
    "201" => "Buffalo Gap",
    "2010" => "Kaiser",
    "2011" => "Bull Shoals",
    "2012" => "Cotter",
    "2013" => "Flippin",
    "2014" => "Yellville",
    "2015" => "New Ulm",
    "2016" => "Belgrade",
    "2017" => "Tappahannock",
    "2018" => "Cold Spring",
    "202" => "Bug Tusle",
    "2020" => "Chinandega",
    "2021" => "Des Arc",
    "2022" => "Dunlap",
    "2023" => "Rio Grande City",
    "2024" => "Peninsula Papagayo",
    "2025" => "Junction City",
    "2026" => "Estancia",
    "2027" => "Rocky",
    "2028" => "Ryan Park",
    "2029" => "New Smyrna Beach",
    "203" => "Bullard",
    "2030" => "Williamstown",
    "2031" => "Berea",
    "2032" => "Alicante",
    "2033" => "Barcelona",
    "2034" => "Madrid",
    "2035" => "Sevilla",
    "2036" => "Valencia",
    "2037" => "San Sebastian",
    "2038" => "Vigo",
    "2039" => "Malaga",
    "204" => "Bulverde",
    "2040" => "Bradshaw",
    "2041" => "Fairy",
    "2042" => "Jonestown",
    "2043" => "Owenton",
    "2044" => "Shive",
    "2045" => "Stinnett",
    "2049" => "McComb",
    "205" => "Buna",
    "2050" => "Sentinel",
    "2051" => "Adamsville",
    "2052" => "Baytown",
    "2053" => "Capitola",
    "2054" => "Caradan",
    "2055" => "Center City",
    "2056" => "Cherokee",
    "2057" => "Cross Cut",
    "2058" => "Doole",
    "2059" => "Ebony",
    "206" => "Bunker Hill Village",
    "2060" => "Elm Grove",
    "2061" => "Fisk",
    "2062" => "Indian Creek",
    "2063" => "Ingram",
    "2064" => "Lake Cisco",
    "2065" => "Loyal Valley",
    "2066" => "Mercury",
    "2068" => "Owens",
    "2069" => "Pioneer",
    "207" => "Bunyan",
    "2070" => "Placid",
    "2071" => "Regency",
    "2072" => "Rochelle",
    "2073" => "Rosanky",
    "2076" => "Rowden",
    "2077" => "Salt Gap",
    "2079" => "Thrifty",
    "208" => "Burkburnett",
    "2080" => "Trickham",
    "2081" => "Voca",
    "2082" => "Winchell",
    "2083" => "Dothan",
    "2086" => "Asheville",
    "2087" => "Rock Springs",
    "2089" => "Dalton",
    "209" => "Burke",
    "2090" => "Stillwater",
    "2091" => "Wamego",
    "2092" => "Pikeville",
    "2093" => "Norway",
    "2094" => "Jackson",
    "2095" => "Christiansburg",
    "2096" => "Sioux Falls",
    "2097" => "Sedalia",
    "2098" => "Preston",
    "2099" => "Laurel",
    "21" => "Allison",
    "210" => "Burkett",
    "2100" => "Prestonsburg",
    "2101" => "Kansas City",
    "2102" => "Wellsboro",
    "2103" => "West Fargo",
    "2104" => "Lafayette",
    "2105" => "Oxford",
    "2106" => "Wynnewood",
    "2107" => "Lansing",
    "2108" => "Saint Joseph",
    "2109" => "New Salisbury",
    "211" => "Burleson",
    "2110" => "Ohio City",
    "2111" => "Hendersonville",
    "2112" => "Saint Clairsville",
    "2113" => "Salida",
    "2114" => "Grand Junction",
    "2115" => "Lawrenceville",
    "2116" => "Fort Gibson",
    "2117" => "Ashland",
    "2118" => "Balfour",
    "2119" => "Stomsriver Village",
    "212" => "Burnet",
    "2120" => "Woodland Park",
    "2121" => "Burns Flat",
    "2122" => "Dill City",
    "2123" => "Playa Bandera",
    "2124" => "Ellicott",
    "2125" => "Guttengruen",
    "2126" => "Gettengruen",
    "2127" => "Hartsel",
    "2128" => "Dove Creek",
    "2129" => "Punta Mita",
    "213" => "Burton",
    "2130" => "Telluride",
    "2131" => "Ojochal",
    "2132" => "San Isidro",
    "2133" => "Uvita",
    "2134" => "Sun City West",
    "2135" => "Zirahuen",
    "2136" => "Phoenix",
    "2137" => "Gause",
    "2138" => "Port Orange",
    "2139" => "Angel Fire",
    "214" => "Butterfield",
    "2141" => "Bee",
    "2142" => "Port Bolivar",
    "215" => "Byers",
    "216" => "Byng",
    "217" => "Bynum",
    "218" => "Cactus",
    "219" => "Caddo",
    "22" => "Alma",
    "220" => "Caddo Mills",
    "221" => "Caldwell",
    "222" => "Calera",
    "223" => "Callisburg",
    "224" => "Calvert",
    "225" => "Cameron",
    "226" => "Camp Wood",
    "227" => "Campbell",
    "228" => "Canadian",
    "229" => "Caney",
    "23" => "Alpine",
    "230" => "Caney City",
    "231" => "Canton",
    "232" => "Canutillo",
    "233" => "Canyon",
    "234" => "Canyon Lake",
    "235" => "Capps Corner",
    "236" => "Carbon",
    "237" => "Carls Corner",
    "238" => "Carlton",
    "239" => "Carmine",
    "24" => "Alto",
    "240" => "Carpenter",
    "241" => "Carrollton",
    "242" => "Carson",
    "243" => "Carthage",
    "244" => "Cartwright",
    "245" => "Cason",
    "246" => "Castell",
    "247" => "Castle Hills",
    "248" => "Castroville",
    "249" => "Cayuga",
    "25" => "Alton",
    "250" => "Cedar Creek Lake",
    "251" => "Cedar Hill",
    "252" => "Cedar Lane",
    "253" => "Cedar Park",
    "254" => "Celeste",
    "255" => "Celina",
    "256" => "Center",
    "257" => "Centerville",
    "258" => "Chandler",
    "259" => "Channing",
    "26" => "Alvarado",
    "260" => "Chapman Ranch",
    "261" => "Chappell Hill",
    "262" => "Charleston",
    "263" => "Charlotte",
    "264" => "Chatfield",
    "265" => "Chester",
    "266" => "Cheyenne",
    "267" => "Chico",
    "268" => "Chicota",
    "269" => "Childress",
    "27" => "Alvin",
    "270" => "Chillicothe",
    "271" => "Chilton",
    "272" => "China",
    "273" => "China Grove",
    "274" => "China Spring",
    "275" => "Chireno",
    "276" => "Chriesman",
    "277" => "Christine",
    "278" => "Cibolo",
    "279" => "Cisco",
    "28" => "Alvord",
    "280" => "Clairette",
    "281" => "Clarendon",
    "282" => "Clarksville",
    "283" => "Claude",
    "284" => "Clay",
    "285" => "Clayton",
    "286" => "Clear Lake Shores",
    "287" => "Cleburne",
    "288" => "Cleveland",
    "289" => "Clifton",
    "29" => "Amarillo",
    "290" => "Clint",
    "291" => "Clute",
    "292" => "Clyde",
    "293" => "Coahoma",
    "294" => "Cockrell Hill",
    "295" => "Coffee",
    "296" => "Coffee City",
    "297" => "Colbert",
    "298" => "Coldspring",
    "299" => "Coleman",
    "3" => "Abilene",
    "30" => "Ambrose",
    "300" => "College Station",
    "301" => "Collegeport",
    "302" => "Colleyville",
    "303" => "Collinsville",
    "304" => "Colmesneil",
    "305" => "Colorado",
    "306" => "Columbus",
    "307" => "Comanche",
    "308" => "Combes",
    "309" => "Combine",
    "31" => "Ames",
    "310" => "Commerce",
    "311" => "Como",
    "312" => "Concord",
    "313" => "Congo",
    "314" => "Connerville",
    "315" => "Conroe",
    "316" => "Converse",
    "317" => "Cookville",
    "318" => "Cool",
    "319" => "Coolidge",
    "32" => "Amherst",
    "320" => "Cooper",
    "321" => "Copeville",
    "322" => "Coppell",
    "323" => "Copper Canyon",
    "324" => "Copperas Cove",
    "325" => "Corinth",
    "326" => "Corpus Christi",
    "327" => "Corral City",
    "328" => "Corrigan",
    "329" => "Corsicana",
    "33" => "Anahuac",
    "330" => "Cotton Center",
    "331" => "Cottonwood",
    "332" => "Cottonwood Shores",
    "333" => "Cotulla",
    "334" => "Coupland",
    "335" => "Courtney",
    "336" => "Cove",
    "337" => "Covington",
    "338" => "Crandall",
    "339" => "Crane",
    "34" => "Anderson",
    "340" => "Cranfills Gap",
    "341" => "Crawford",
    "342" => "Creedmoor",
    "343" => "Cresson",
    "344" => "Crockett",
    "345" => "Crosbyton",
    "346" => "Cross Plains",
    "347" => "Cross Roads",
    "348" => "Cross Timber",
    "349" => "Crowell",
    "35" => "Andrews",
    "350" => "Crowley",
    "351" => "Crows",
    "352" => "Crystal",
    "353" => "Crystal Springs",
    "354" => "Cuero",
    "355" => "Cumby",
    "356" => "Cuney",
    "357" => "Cunningham",
    "358" => "Cushing",
    "359" => "Cut and Shoot",
    "36" => "Angleton",
    "360" => "D Hanis",
    "361" => "Daingerfield",
    "362" => "Daisetta",
    "363" => "Dalhart",
    "364" => "Dallas",
    "365" => "Dalworthington Gardens",
    "366" => "Danbury",
    "367" => "Darrouzett",
    "368" => "Dawson",
    "369" => "Dayton",
    "37" => "Angus",
    "370" => "Dayton Lakes",
    "371" => "De Berry",
    "372" => "De Kalb",
    "373" => "De Leon",
    "374" => "Dean",
    "375" => "Deanville",
    "376" => "Decatur",
    "377" => "Deer Park",
    "378" => "Del Rio",
    "379" => "Del Valle",
    "38" => "Anna",
    "380" => "Dell",
    "381" => "Denison",
    "382" => "Dennis",
    "383" => "Denton",
    "384" => "Denver",
    "385" => "Deport",
    "386" => "Desdemona",
    "387" => "DeSoto",
    "388" => "Detroit",
    "389" => "Devers",
    "39" => "Annetta",
    "390" => "Devine",
    "391" => "Devol",
    "392" => "Diamond Head",
    "393" => "Diana",
    "394" => "Diboll",
    "395" => "Dickens",
    "396" => "Dickinson",
    "397" => "Dike",
    "398" => "Dilley",
    "399" => "Dimmitt",
    "4" => "Achille",
    "40" => "Annetta North",
    "400" => "Dinero",
    "401" => "Dodd City",
    "402" => "Dodson",
    "403" => "Domino",
    "404" => "Donie",
    "405" => "Donna",
    "406" => "Dorchester",
    "407" => "Doss",
    "408" => "Double Oak",
    "409" => "Douglass",
    "41" => "Annetta South",
    "410" => "Douglassville",
    "411" => "Driftwood",
    "412" => "Dripping Springs",
    "413" => "Driscoll",
    "414" => "Dublin",
    "415" => "Duffau",
    "416" => "Dumas",
    "417" => "Duncanville",
    "418" => "Dunn",
    "419" => "Durant",
    "42" => "Annona",
    "420" => "Durham",
    "421" => "Eagle Lake",
    "422" => "Eagle Pass",
    "423" => "Eagltown",
    "424" => "Early",
    "425" => "Earth",
    "426" => "East End",
    "427" => "East Mountain",
    "428" => "East Tawakoni",
    "429" => "Easterly",
    "43" => "Anson",
    "430" => "Eastland",
    "431" => "Easton",
    "432" => "Ecleto",
    "433" => "Ector",
    "434" => "Edcouch",
    "435" => "Eden",
    "436" => "Edgecliff Village",
    "437" => "Edgewood",
    "438" => "Edinburg",
    "439" => "Edmonson",
    "44" => "Anthony",
    "440" => "Edna",
    "441" => "Edom",
    "442" => "El Campo",
    "443" => "El Cenizo",
    "444" => "El Indio",
    "445" => "El Lago",
    "446" => "El Paso",
    "447" => "Eldorado",
    "448" => "Electra",
    "449" => "Elgin",
    "45" => "Anton",
    "450" => "Elkhart",
    "451" => "Elm Mott",
    "452" => "Elmaton",
    "453" => "Elmendorf",
    "454" => "Elmo",
    "455" => "Elsa",
    "456" => "Elysian Fields",
    "457" => "Emhouse",
    "458" => "Emory",
    "459" => "Enchanted Oaks",
    "46" => "Appleby",
    "460" => "Encinal",
    "461" => "Energy",
    "462" => "Enet",
    "463" => "Enloe",
    "464" => "Ennis",
    "465" => "Era",
    "466" => "Estelline",
    "467" => "Etoile",
    "468" => "Euless",
    "469" => "Eureka",
    "47" => "Aquilla",
    "470" => "Eustace",
    "471" => "Evadale",
    "472" => "Evant",
    "473" => "Everman",
    "474" => "Fabens",
    "475" => "Fair Oaks Ranch",
    "476" => "Fairchilds",
    "477" => "Fairfield",
    "478" => "Fairlie",
    "479" => "Fairview",
    "48" => "Aransas Pass",
    "480" => "Falfurrias",
    "481" => "Falls",
    "482" => "Farmers Branch",
    "483" => "Farmersville",
    "484" => "Farwell",
    "485" => "Fate",
    "486" => "Fayetteville",
    "487" => "Felker",
    "488" => "Ferris",
    "489" => "Filmore",
    "49" => "Archer City",
    "490" => "Fischer",
    "491" => "Fittstown",
    "492" => "Fitzhugh",
    "493" => "Flat",
    "494" => "Flatonia",
    "495" => "Flint",
    "496" => "Flora",
    "497" => "Florence",
    "498" => "Floresville",
    "499" => "Flower Mound",
    "5" => "Ackerly",
    "50" => "Arcola",
    "500" => "Floyd",
    "501" => "Floydada",
    "502" => "Fluvanna",
    "503" => "Flynn",
    "504" => "Fob",
    "505" => "Follett",
    "506" => "Forest",
    "507" => "Forest Hill",
    "508" => "Forestburg",
    "509" => "Forney",
    "51" => "Argyle",
    "510" => "Forreston",
    "511" => "Forsan",
    "512" => "Fort Stockton",
    "513" => "Fort Towson",
    "514" => "Fort Worth",
    "515" => "Fountain Lake",
    "516" => "Francis",
    "517" => "Franklin",
    "518" => "Frankston",
    "519" => "Fredericksburg",
    "52" => "Arlington",
    "520" => "Freeport",
    "521" => "Freer",
    "522" => "Friendship",
    "523" => "Friendswood",
    "524" => "Friona",
    "525" => "Frisco",
    "526" => "Fritch",
    "527" => "Frogville",
    "528" => "Frost",
    "529" => "Fruitvale",
    "53" => "Armstrong",
    "530" => "Fulshear",
    "531" => "Fulton",
    "532" => "Gail",
    "533" => "Gainesville",
    "534" => "Galena Park",
    "535" => "Gallatin",
    "536" => "Galveston",
    "537" => "Ganado",
    "538" => "Garden City",
    "539" => "Garden Ridge",
    "54" => "Arp",
    "540" => "Garland",
    "541" => "Garner",
    "542" => "Garrett",
    "543" => "Garrison",
    "544" => "Garvin",
    "545" => "Gary",
    "546" => "Gatesville",
    "547" => "Gaybluff",
    "548" => "George West",
    "549" => "Georgetown",
    "55" => "Arthur City",
    "550" => "Geronimo",
    "551" => "Gholson",
    "552" => "Giddings",
    "553" => "Gillett",
    "554" => "Gilmer",
    "555" => "Gladewater",
    "556" => "Glen Rose",
    "557" => "Glenn Heights",
    "558" => "Gober",
    "559" => "Godley",
    "56" => "Aspermont",
    "560" => "Golden",
    "561" => "Goldsboro",
    "562" => "Goldsmith",
    "563" => "Goldthwaite",
    "564" => "Goliad",
    "565" => "Golinda",
    "566" => "Gonzales",
    "567" => "Goodland",
    "568" => "Goodlow",
    "569" => "Goodrich",
    "57" => "Atascosa",
    "570" => "Gordon",
    "571" => "Gordonville",
    "572" => "Goree",
    "573" => "Gorman",
    "574" => "Gouldbusk",
    "575" => "Graford",
    "576" => "Graham",
    "577" => "Granbury",
    "578" => "Grand Prairie",
    "579" => "Grand Saline",
    "58" => "Athens",
    "580" => "Grandfalls",
    "581" => "Grandview",
    "582" => "Granger",
    "583" => "Granite Shoals",
    "584" => "Granjeno",
    "585" => "Grapeland",
    "586" => "Grapevine",
    "587" => "Grays Prairie",
    "588" => "Greenview",
    "589" => "Greenville",
    "59" => "Atlanta",
    "590" => "Greenwood",
    "591" => "Gregory",
    "592" => "Grey Forest",
    "593" => "Groesbeck",
    "594" => "Groom",
    "595" => "Groves",
    "596" => "Groveton",
    "597" => "Gruver",
    "598" => "Gun Barrel City",
    "599" => "Gunter",
    "6" => "Acton",
    "60" => "Aubrey",
    "600" => "Gustine",
    "601" => "Guthrie",
    "602" => "Hackberry",
    "603" => "Hagansport",
    "604" => "Hale Center",
    "605" => "Hallettsville",
    "606" => "Hallsburg",
    "607" => "Hallsville",
    "609" => "Haltom City",
    "61" => "Aurora",
    "610" => "Hamilton",
    "611" => "Hamlin",
    "612" => "Hammond",
    "613" => "Happy",
    "614" => "Happy Land",
    "615" => "Harbin",
    "616" => "Harden City",
    "617" => "Hardin",
    "618" => "Harker Heights",
    "619" => "Harleton",
    "62" => "Austin",
    "620" => "Harlingen",
    "621" => "Harper",
    "622" => "Harris",
    "623" => "Harrold",
    "624" => "Hart",
    "625" => "Haske",
    "626" => "Haskell",
    "627" => "Haslet",
    "628" => "Hasse",
    "629" => "Hawkins",
    "63" => "Austwell",
    "630" => "Hawley",
    "631" => "Hays",
    "632" => "Hearne",
    "633" => "Heath",
    "634" => "Hebron",
    "635" => "Hebronville",
    "636" => "Hedley",
    "637" => "Hedwig Village",
    "638" => "Heidenheimer",
    "639" => "Helotes",
    "64" => "Avalon",
    "640" => "Hemphill",
    "641" => "Hempstead",
    "642" => "Hempwallace",
    "643" => "Henderson",
    "644" => "Hendrix",
    "645" => "Henrietta",
    "646" => "Hereford",
    "647" => "Hermleigh",
    "648" => "Hewitt",
    "649" => "Hickory Creek",
    "65" => "Avant",
    "650" => "Hico",
    "651" => "Hidalgo",
    "652" => "Higgins",
    "653" => "Highland Haven",
    "654" => "Highland Park",
    "655" => "Highland Village",
    "656" => "Hill Country",
    "657" => "Hillcrest",
    "658" => "Hillsboro",
    "659" => "Hilshire Village",
    "66" => "Avery",
    "660" => "Hitchcock",
    "661" => "Hobson",
    "662" => "Holiday Lakes",
    "663" => "Holland",
    "664" => "Holliday",
    "665" => "Hollywood Park",
    "666" => "Homer",
    "667" => "Hondo",
    "668" => "Honey Grove",
    "669" => "Hooks",
    "67" => "Avilla",
    "670" => "Horizon",
    "671" => "Horseshoe Bay",
    "672" => "Hot Springs",
    "673" => "Hot Springs Village",
    "674" => "Houston",
    "675" => "Howardwick",
    "676" => "Howe",
    "677" => "Hubbard",
    "678" => "Huckabay",
    "679" => "Hudson",
    "68" => "Avinger",
    "680" => "Hudson Oaks",
    "681" => "Hughes Springs",
    "682" => "Hulen",
    "683" => "Humble",
    "684" => "Hunters Creek Village",
    "685" => "Huntington",
    "686" => "Huntsville",
    "687" => "Hurst",
    "688" => "Huskey",
    "689" => "Hutchins",
    "69" => "Axtell",
    "690" => "Hutto",
    "691" => "Huxley",
    "692" => "Idabell",
    "693" => "Impact",
    "694" => "Indian Lake",
    "695" => "Industry",
    "696" => "Ingleside",
    "697" => "Ingleside on the Bay",
    "698" => "Iowa Colony",
    "699" => "Iowa Park",
    "7" => "Ada",
    "70" => "Azle",
    "700" => "Ira",
    "701" => "Iraan",
    "702" => "Iredell",
    "703" => "Irene",
    "704" => "Iron Springs",
    "705" => "Irving",
    "706" => "Italy",
    "707" => "Itasca",
    "708" => "Ivanhoe",
    "709" => "Jacinto",
    "71" => "Bagwell",
    "710" => "Jacksboro",
    "711" => "Jacksonville",
    "712" => "Jamaica Beach",
    "713" => "Jarrell",
    "714" => "Jasper",
    "715" => "Jayton",
    "716" => "Jefferson",
    "717" => "Jermyn",
    "718" => "Jersey Village",
    "719" => "Jesse",
    "72" => "Bailey",
    "720" => "Jessieville",
    "721" => "Jewett",
    "722" => "Jimtown",
    "723" => "Joaquin",
    "724" => "Johnson",
    "725" => "Joinerville",
    "726" => "Jolly",
    "727" => "Jones",
    "728" => "Jones Creek",
    "729" => "Jones Mill",
    "73" => "Baileys Prairie",
    "730" => "Jonesboro",
    "731" => "Jonesville",
    "732" => "Josephine",
    "733" => "Joshua",
    "734" => "Jourdanton",
    "735" => "Judson",
    "736" => "Junction",
    "737" => "Justin",
    "738" => "Kamay",
    "739" => "Karnack",
    "74" => "Baird",
    "740" => "Karnes",
    "741" => "Karnes City",
    "742" => "Katy",
    "743" => "Kaufman",
    "744" => "Keene",
    "745" => "Keller",
    "746" => "Kemah",
    "747" => "Kemp",
    "748" => "Kempner",
    "749" => "Kendleton",
    "75" => "Balch Springs",
    "750" => "Kenedy",
    "751" => "Kenefick",
    "752" => "Kennard",
    "753" => "Kennedale",
    "754" => "Kenefic",
    "755" => "Kent",
    "756" => "Kentuckytown",
    "757" => "Kerens",
    "758" => "Kermit",
    "759" => "Kiersev",
    "76" => "Balcones Heights",
    "760" => "Kildare",
    "761" => "Kilgore",
    "762" => "Killeen",
    "763" => "Kingsbury",
    "764" => "Kingsland",
    "765" => "Kingston",
    "766" => "Kingsville",
    "767" => "Kirby",
    "768" => "Kirbyville",
    "769" => "Kirvin",
    "77" => "Ballinger",
    "770" => "Klondike",
    "771" => "Knollwood",
    "772" => "Knox",
    "773" => "Kopperl",
    "774" => "Kosse",
    "775" => "Kountze",
    "776" => "Kress",
    "777" => "Krugerville",
    "778" => "Krum",
    "779" => "Kurten",
    "78" => "Balmorhea",
    "780" => "Kyle",
    "781" => "La Coste",
    "782" => "La Feria",
    "783" => "La Grange",
    "784" => "La Joya",
    "785" => "La Marque",
    "786" => "La Porte",
    "787" => "La Vernia",
    "788" => "La Villa",
    "789" => "La Ward",
    "79" => "Bandera",
    "790" => "LaCoste",
    "791" => "Lacy Lakeview",
    "792" => "Ladonia",
    "793" => "Lago Vista",
    "794" => "Laguna Park",
    "795" => "Laguna Vista",
    "796" => "Laird Hill",
    "797" => "Lake",
    "798" => "Lake Bridgeport",
    "799" => "Lake Catherine",
    "8" => "Addison",
    "80" => "Bangs",
    "800" => "Lake Creek",
    "801" => "Lake Dallas",
    "802" => "Lake Hamilton",
    "803" => "Lake Jackson",
    "804" => "Lake Kiowa",
    "805" => "Lake Tanglewood",
    "806" => "Lake Worth",
    "807" => "Lakeport",
    "808" => "Lakeside",
    "809" => "Lakeview",
    "81" => "Banquete",
    "810" => "Lakeway",
    "811" => "Lakewood Village",
    "812" => "Lamasco",
    "813" => "Lamesa",
    "814" => "Lampasas",
    "815" => "Lancaster",
    "816" => "Laneville",
    "817" => "Lannius",
    "818" => "Lantana",
    "819" => "Laredo",
    "82" => "Bardwell",
    "820" => "Larue",
    "821" => "Latexo",
    "822" => "Latta",
    "823" => "Lavon",
    "824" => "Lawn",
    "825" => "League",
    "826" => "Leakey",
    "827" => "Leander",
    "828" => "Leary",
    "829" => "Lebanon",
    "83" => "Barry",
    "830" => "Leesburg",
    "831" => "Lefors",
    "832" => "Leon",
    "833" => "Leon Junction",
    "834" => "Leon Valley",
    "835" => "Leona",
    "836" => "Leonard",
    "837" => "Leroy",
    "838" => "Levelland",
    "839" => "Lewisville",
    "84" => "Barstow",
    "840" => "Lexington",
    "841" => "Liberty",
    "842" => "Liberty Hill",
    "843" => "Lillian",
    "844" => "Lincoln Park",
    "845" => "Lindale",
    "846" => "Linden",
    "847" => "Lindsay",
    "848" => "Lingleville",
    "849" => "Lipan",
    "85" => "Bartlett",
    "850" => "Little City",
    "851" => "Little Elm",
    "852" => "Little River",
    "854" => "Littlefield",
    "855" => "Live Oak",
    "856" => "Liverpool",
    "857" => "Livingston",
    "858" => "Llano",
    "859" => "Lockhart",
    "86" => "Bartonville",
    "860" => "Lockney",
    "861" => "Lodi",
    "862" => "Log Cabin",
    "863" => "Lometa",
    "864" => "Lone Oak",
    "865" => "Lone Star",
    "866" => "Long Branch",
    "867" => "Longview",
    "868" => "Lono",
    "869" => "Lonsdale",
    "87" => "Bastrop",
    "870" => "Loraine",
    "871" => "Lorena",
    "872" => "Lorenzo",
    "873" => "Los Fresnos",
    "874" => "Los Indios",
    "875" => "Los Ybanez",
    "876" => "Lott",
    "877" => "Lovelady",
    "878" => "Loving",
    "879" => "Lowry Crossing",
    "88" => "Battiest",
    "880" => "Lucas",
    "881" => "Lueders",
    "882" => "Luella",
    "883" => "Lufkin",
    "884" => "Luling",
    "885" => "Lumberton",
    "886" => "Lyford",
    "887" => "Lyons",
    "888" => "Lytle",
    "889" => "Mabank",
    "89" => "Bauxite",
    "890" => "Macdona",
    "891" => "Madill",
    "892" => "Madisonville",
    "893" => "Magnet Cove",
    "894" => "Magnolia",
    "895" => "Malakoff",
    "896" => "Malone",
    "897" => "Malvern",
    "898" => "Manchaca",
    "899" => "Mannsville",
    "9" => "Adkins",
    "90" => "Bay",
    "900" => "Manor",
    "901" => "Mansfield",
    "902" => "Manvel",
    "903" => "Marble Falls",
    "904" => "Marcus",
    "905" => "Marfa",
    "906" => "Marietta",
    "907" => "Marion",
    "908" => "Markham",
    "909" => "Marlin",
    "91" => "Bay City",
    "910" => "Marquez",
    "911" => "Marshall",
    "912" => "Marshall Creek",
    "913" => "Mart",
    "914" => "Martin Springs",
    "915" => "Martindale",
    "916" => "Martinsville",
    "917" => "Maryneal",
    "918" => "Mason",
    "919" => "Matador",
    "92" => "Bayou Vista",
    "920" => "Matagorda",
    "921" => "Mathis",
    "922" => "Maud",
    "923" => "May",
    "924" => "Maydelle",
    "925" => "Maypearl",
    "926" => "Mc Leod",
    "927" => "McAllen",
    "928" => "McBride",
    "929" => "McCamey",
    "93" => "Bayside",
    "930" => "McCaulley",
    "932" => "McKinney",
    "933" => "McLean",
    "934" => "McLendon Chisholm",
    "935" => "McNeil",
    "936" => "McQueeney",
    "937" => "Mead",
    "938" => "Meadow",
    "939" => "Meadowlakes",
    "94" => "Beach",
    "940" => "Meadows Place",
    "941" => "Megargel",
    "942" => "Melissa",
    "943" => "Melvin",
    "944" => "Memphis",
    "945" => "Menard",
    "946" => "Mentone",
    "947" => "Mercedes",
    "948" => "Meridian",
    "949" => "Merit",
    "95" => "Bear",
    "950" => "Merkel",
    "951" => "Mertens",
    "952" => "Mertzon",
    "953" => "Mesquite",
    "954" => "Metcalf Gap",
    "955" => "Mexia",
    "956" => "Miami",
    "957" => "Mico",
    "958" => "Midfield",
    "959" => "Midland",
    "96" => "Bear Creek",
    "960" => "Midlothian",
    "961" => "Midway",
    "962" => "Milano",
    "963" => "Milburn",
    "964" => "Mildred",
    "965" => "Miles",
    "966" => "Milford",
    "967" => "Mill Creek",
    "968" => "Miller Grove",
    "969" => "Millers Cove",
    "97" => "Beasley",
    "970" => "Millerton",
    "971" => "Millican",
    "972" => "Millsap",
    "973" => "Minden",
    "974" => "Mineola",
    "975" => "Mineral Wells",
    "976" => "Mingus",
    "977" => "Mission",
    "978" => "Missouri",
    "979" => "Mobeetie",
    "98" => "Beaumont",
    "980" => "Mobile",
    "981" => "Monahans",
    "982" => "Mont Belvieu",
    "983" => "Montague",
    "984" => "Montalba",
    "985" => "Montgomery",
    "986" => "Moody",
    "987" => "Moore Station",
    "988" => "Mooring",
    "989" => "Moran",
    "99" => "Beckville",
    "990" => "Morgan",
    "991" => "Morgan Mill",
    "992" => "Morgans Point",
    "994" => "Morning Star",
    "995" => "Morton",
    "996" => "Moulton",
    "997" => "Mound",
    "998" => "Mount Calm",
    "999" => "Mount Enterprise"     
  }
  SCHOOL_DISTRICT = {
    "1" => "A W Brown-Fellowship Charter S",
    "10" => "Academy of Dallas",
    "100" => "Bexar County Academy",
    "1000" => "Rogers ISD",
    "1001" => "Roma ISD",
    "1002" => "Ron Jackson State Juvenile Cor",
    "1003" => "Roosevelt ISD",
    "1004" => "Ropes ISD",
    "1005" => "Roscoe ISD",
    "1006" => "Rosebud-Lott ISD",
    "1007" => "Rotan ISD",
    "1008" => "Round Rock ISD",
    "1009" => "Round Top-Carmine ISD",
    "101" => "Big Sandy ISD",
    "1010" => "Roxton ISD",
    "1011" => "Royal ISD",
    "1012" => "Royse City ISD",
    "1013" => "Rule ISD",
    "1014" => "Runge ISD",
    "1015" => "Rusk ISD",
    "1016" => "S and S Cons ISD",
    "1017" => "Sabinal ISD",
    "1018" => "Sabine ISD",
    "1019" => "Sabine Pass ISD",
    "102" => "Big Spring ISD",
    "1020" => "Saint Jo ISD",
    "1021" => "Salado ISD",
    "1022" => "Saltillo ISD",
    "1023" => "Sam Rayburn ISD",
    "1024" => "Samnorwood ISD",
    "1025" => "San Angelo ISD",
    "1026" => "San Antonio Can High School",
    "1027" => "San Antonio ISD",
    "1028" => "San Antonio Preparatory Academ",
    "1029" => "San Antonio School for Inquiry",
    "103" => "Big Springs Charter School",
    "1030" => "San Antonio Technology  Academ",
    "1031" => "San Augustine ISD",
    "1032" => "San Benito Cons ISD",
    "1033" => "San Diego ISD",
    "1034" => "San Elizario ISD",
    "1035" => "San Felipe-Del Rio Cons ISD",
    "1036" => "San Isidro ISD",
    "1037" => "San Marcos Cons ISD",
    "1038" => "San Marcos Preparatory School",
    "1039" => "San Perlita ISD",
    "104" => "Birdville ISD",
    "1040" => "San Saba ISD",
    "1041" => "San Saba State School",
    "1042" => "San Vicente ISD",
    "1043" => "Sands CISD",
    "1044" => "Sanford ISD",
    "1045" => "Sanger ISD",
    "1046" => "Santa Anna ISD",
    "1047" => "Santa Fe ISD",
    "1048" => "Santa Gertrudis ISD",
    "1049" => "Santa Maria ISD",
    "105" => "Bishop Cons ISD",
    "1050" => "Santa Rosa ISD",
    "1051" => "Santo ISD",
    "1052" => "Savoy ISD",
    "1053" => "Schertz-Cibolo-U City ISD",
    "1054" => "Schleicher ISD",
    "1055" => "School of Excellence in Educat",
    "1056" => "School of Liberal Arts and Sci",
    "1057" => "Schulenburg ISD",
    "1058" => "Scotland",
    "1059" => "Scurry-Rosser ISD",
    "106" => "Blackwell Cons ISD",
    "1060" => "Seagraves ISD",
    "1061" => "Sealy ISD",
    "1062" => "Seashore Learning Ctr Charter",
    "1063" => "Seguin ISD",
    "1064" => "Seminole ISD",
    "1065" => "Sentry Technology Prep School",
    "1066" => "Ser-Ninos Charter School",
    "1067" => "Seymour ISD",
    "1068" => "Shallowater ISD",
    "1069" => "Shamrock ISD",
    "107" => "Blanco ISD",
    "1070" => "Sharyland ISD",
    "1071" => "Shekinah Radiance Academy",
    "1072" => "Shelbyville ISD",
    "1073" => "Sheldon ISD",
    "1074" => "Shepherd ISD",
    "1075" => "Sherman ISD",
    "1076" => "Shiner ISD",
    "1077" => "Sidney ISD",
    "1078" => "Sierra Blanca ISD",
    "1079" => "Silsbee ISD",
    "108" => "Bland ISD",
    "1080" => "Silverton ISD",
    "1081" => "Simms ISD",
    "1082" => "Sinton ISD",
    "1083" => "Sivells Bend ISD",
    "1084" => "Skidmore-Tynan ISD",
    "1085" => "Slaton ISD",
    "1086" => "Slidell ISD",
    "1087" => "Slocum ISD",
    "1088" => "Smithville ISD",
    "1089" => "Smyer ISD",
    "109" => "Blanket ISD",
    "1090" => "Snook ISD",
    "1091" => "Snyder ISD",
    "1092" => "Socorro ISD",
    "1093" => "Somerset ISD",
    "1094" => "Somerville ISD",
    "1095" => "Sonora ISD",
    "1096" => "South Africa",
    "1097" => "South Plains Academy",
    "1098" => "South San Antonio ISD",
    "1099" => "South Texas ISD",
    "11" => "Academy of Houston",
    "110" => "Bloomburg ISD",
    "1100" => "Southland ISD",
    "1101" => "Southside ISD",
    "1102" => "Southwest High School",
    "1103" => "Southwest ISD",
    "1104" => "Southwest Preparatory School",
    "1105" => "Spade ISD",
    "1106" => "Spain",
    "1107" => "Spearman ISD",
    "1108" => "Splendora ISD",
    "1109" => "Spring Branch ISD",
    "111" => "Blooming Grove ISD",
    "1110" => "Spring Creek ISD",
    "1111" => "Spring Hill ISD",
    "1112" => "Spring ISD",
    "1113" => "Springlake-Earth ISD",
    "1114" => "Springtown ISD",
    "1115" => "Spur ISD",
    "1116" => "Spurger ISD",
    "1117" => "St Anthony Academy",
    "1118" => "St Mary&apos;s Academy Charter Scho",
    "1119" => "St. Lucia",
    "112" => "Bloomington ISD",
    "1120" => "St. Vincent",
    "1121" => "Stafford Municipal School Dist",
    "1122" => "Stamford ISD",
    "1123" => "Stanton ISD",
    "1124" => "Star Charter School",
    "1125" => "Star ISD",
    "1126" => "State of Arizonia",
    "1127" => "State of Arkansas",
    "1128" => "State of Colorado",
    "1129" => "State of Florida",
    "113" => "Blue Ridge ISD",
    "1130" => "State of Idaho",
    "1131" => "State of Illinois",
    "1132" => "State of Kansas",
    "1133" => "State of Massachusetts",
    "1134" => "State of Michigan",
    "1135" => "State of Mississippi",
    "1136" => "State of Missouri",
    "1137" => "State of Montana",
    "1138" => "State of North Carolina",
    "1139" => "State of North Dakota",
    "114" => "Bluff Dale ISD",
    "1140" => "State of Ohio",
    "1141" => "State of Pennsylvania",
    "1142" => "State of South Dakota",
    "1143" => "State of Virginia",
    "1144" => "State of West Virginia",
    "1145" => "State of Wyoming",
    "1146" => "Stephenville",
    "1147" => "Stephenville ISD",
    "1148" => "Sterling City ISD",
    "1149" => "Stockdale ISD",
    "115" => "Blum ISD",
    "1150" => "Stratford ISD",
    "1151" => "Strawn ISD",
    "1152" => "Sudan ISD",
    "1153" => "Sulphur Bluff ISD",
    "1154" => "Sulphur Springs ISD",
    "1155" => "Sulphur, OK",
    "1156" => "Sundown ISD",
    "1157" => "Sunnyvale ISD",
    "1158" => "Sunray ISD",
    "1159" => "Sweeny ISD",
    "116" => "Boerne ISD",
    "1160" => "Sweet Home ISD",
    "1161" => "Sweetwater ISD",
    "1162" => "Taft ISD",
    "1163" => "Tahoka ISD",
    "1164" => "Tarkington ISD",
    "1165" => "Tatum ISD",
    "1166" => "Taylor ISD",
    "1167" => "Teague ISD",
    "1168" => "Technology Education Charter H",
    "1169" => "Tekoa Academy of Accelerated S",
    "117" => "Boles ISD",
    "1170" => "Temple ISD",
    "1171" => "Tenaha ISD",
    "1172" => "Tennessee",
    "1173" => "Terlingua CSD",
    "1174" => "Terrell County ISD",
    "1175" => "Terrell ISD",
    "1176" => "Texarkana ISD",
    "1177" => "Texas A &amp; M University at Galv",
    "1178" => "Texas Academy of Excellence",
    "1179" => "Texas Academy of Leadership in",
    "118" => "Boling ISD",
    "1180" => "Texas City ISD",
    "1181" => "Texas Empowerment Academy",
    "1182" => "Texas Sch for the Blind &amp; Visu",
    "1183" => "Texas Sch for the Deaf",
    "1184" => "Texas Serenity Academy",
    "1185" => "Texas Serenity Academy (Baysho",
    "1186" => "Texas Tech University High Sch",
    "1187" => "Texhoma ISD",
    "1188" => "Texline ISD",
    "1189" => "The Philippines",
    "119" => "Bonham ISD",
    "1190" => "The Zoe Learning Academy",
    "1191" => "Theresa B Lee Academy",
    "1192" => "Thorndale ISD",
    "1193" => "Thrall ISD",
    "1194" => "Three Rivers ISD",
    "1195" => "Three Way ISD",
    "1196" => "Throckmorton ISD",
    "1197" => "Tidehaven ISD",
    "1198" => "Timpson ISD",
    "1199" => "Tioga ISD",
    "12" => "Accelerated Intermediate Acade",
    "120" => "Booker ISD",
    "1200" => "Tolar ISD",
    "1201" => "Tom Bean ISD",
    "1202" => "Tomball ISD",
    "1203" => "Tornillo ISD",
    "1204" => "Tovas-Tactile Oral Visual Alte",
    "1205" => "Transformative Charter Academy",
    "1206" => "Treetops School International",
    "1207" => "Trent ISD",
    "1208" => "Trenton ISD",
    "1209" => "Trinidad ISD",
    "121" => "Borden County ISD",
    "1210" => "Trinity Basin Preparatory",
    "1211" => "Trinity ISD",
    "1212" => "Troup ISD",
    "1213" => "Troy ISD",
    "1214" => "Tulia ISD",
    "1215" => "Tuloso-Midway ISD",
    "1216" => "Tupelo ISD",
    "1217" => "Turkey-Quitaque ISD",
    "1218" => "Two Dimensions Preparatory Aca",
    "1219" => "Tyler ISD",
    "122" => "Borger ISD",
    "1220" => "Union Grove ISD",
    "1221" => "Union Hill ISD",
    "1222" => "United ISD",
    "1223" => "Univ of Houston Charter Sch-Te",
    "1224" => "Universal Academy",
    "1225" => "University Charter School",
    "1226" => "University of North Texas",
    "1227" => "University of Texas at Austin",
    "1228" => "University of Texas Elementary",
    "1229" => "Utopia ISD",
    "123" => "Bosqueville ISD",
    "1230" => "Uvalde Cons ISD",
    "1231" => "Valentine ISD",
    "1232" => "Valley High",
    "1233" => "Valley Mills ISD",
    "1234" => "Valley View ISD",
    "1235" => "Van Alstyne ISD",
    "1236" => "Van ISD",
    "1237" => "Van Vleck ISD",
    "1238" => "Vanguard Academy",
    "1239" => "Varnett Charter School",
    "124" => "Bovina ISD",
    "1240" => "Vega ISD",
    "1241" => "Venus ISD",
    "1242" => "Veribest ISD",
    "1243" => "Vernon ISD",
    "1244" => "Victoria ISD",
    "1245" => "Vidor ISD",
    "1246" => "Vysehrad ISD",
    "1247" => "Wa-Set Preparatory Academy",
    "1248" => "Waco Charter School (EOAC)",
    "1249" => "Waco ISD",
    "125" => "Bowie ISD",
    "1250" => "Waelder ISD",
    "1251" => "Walcott ISD",
    "1252" => "Wall ISD",
    "1253" => "Waller ISD",
    "1254" => "Walnut Bend ISD",
    "1255" => "Walnut Springs ISD",
    "1256" => "Warren ISD",
    "1257" => "Washington",
    "1258" => "Waskom ISD",
    "1259" => "Water Valley ISD",
    "126" => "Boyd ISD",
    "1260" => "Waxahachie ISD",
    "1261" => "Weatherford ISD",
    "1262" => "Webb Cons ISD",
    "1263" => "Weimar ISD",
    "1264" => "Wellington ISD",
    "1265" => "Wellman-Union Cons ISD",
    "1266" => "Wells ISD",
    "1267" => "Weslaco ISD",
    "1268" => "West Hardin County Cons ISD",
    "1269" => "West Houston Charter School",
    "127" => "Boys Ranch ISD",
    "1270" => "West ISD",
    "1271" => "West Orange-Cove Cons ISD",
    "1272" => "West Oso ISD",
    "1273" => "West Rusk ISD",
    "1274" => "West Sabine ISD",
    "1275" => "West Texas State School",
    "1276" => "Westbrook ISD",
    "1277" => "Westhoff ISD",
    "1278" => "Westlake Academy",
    "1279" => "Westphalia ISD",
    "128" => "Brackett ISD",
    "1280" => "Westwood ISD",
    "1281" => "Wharton ISD",
    "1282" => "Wheeler ISD",
    "1283" => "White Deer ISD",
    "1284" => "White Oak ISD",
    "1285" => "White Settlement ISD",
    "1286" => "Whiteface Cons ISD",
    "1287" => "Whitehouse ISD",
    "1288" => "Whitesboro ISD",
    "1289" => "Whitewright ISD",
    "129" => "Brady ISD",
    "1290" => "Whitharral ISD",
    "1291" => "Whitney ISD",
    "1292" => "Wichita Falls ISD",
    "1293" => "Wildorado ISD",
    "1294" => "Willis ISD",
    "1295" => "Wills Point ISD",
    "1296" => "Wilmer-Hutchins ISD",
    "1297" => "Wilson ISD",
    "1298" => "Wimberley ISD",
    "1299" => "Windham School District",
    "13" => "Adrian ISD",
    "130" => "Brazil",
    "1300" => "Windthorst ISD",
    "1301" => "Winfield ISD",
    "1302" => "Winfree Academy",
    "1303" => "Wink-Loving ISD",
    "1304" => "Winnsboro ISD",
    "1305" => "Winona ISD",
    "1306" => "Winters ISD",
    "1307" => "Woden ISD",
    "1308" => "Wolfe City ISD",
    "1309" => "Woodsboro ISD",
    "131" => "Brazos ISD",
    "1310" => "Woodson ISD",
    "1311" => "Woodville ISD",
    "1312" => "Wortham ISD",
    "1313" => "Wylie ISD",
    "1314" => "Wylie ISD, Taylor Co.",
    "1315" => "Yantis ISD",
    "1316" => "Yes College Preparatory School",
    "1317" => "Yoakum ISD",
    "1318" => "Yorktown ISD",
    "1319" => "Ysleta ISD",
    "132" => "Brazos River Charter School",
    "1320" => "Zapata County ISD",
    "1321" => "Zavalla ISD",
    "1322" => "Zephyr ISD",
    "133" => "Brazos School for Inquiry &amp; Cr",
    "134" => "Brazosport ISD",
    "135" => "Breckenridge ISD",
    "136" => "Bremond ISD",
    "137" => "Brenham ISD",
    "138" => "Bridge City ISD",
    "139" => "Bridgeport ISD",
    "14" => "Agua Dulce ISD",
    "140" => "Bright Ideas Charter",
    "141" => "Broaddus ISD",
    "142" => "Brock ISD",
    "143" => "Bronte ISD",
    "144" => "Brookeland ISD",
    "145" => "Brookesmith ISD",
    "146" => "Brooks County ISD",
    "147" => "Brownfield ISD",
    "148" => "Brownsboro ISD",
    "149" => "Brownsville ISD",
    "15" => "Al Price State Juvenile Correc",
    "150" => "Brownwood ISD",
    "151" => "Bruceville-Eddy ISD",
    "152" => "Bryan ISD",
    "153" => "Bryson ISD",
    "154" => "Buckholts ISD",
    "155" => "Buena Vista ISD",
    "156" => "Buffalo ISD",
    "157" => "Bulgaria",
    "158" => "Bullard ISD",
    "159" => "Buna ISD",
    "16" => "Alabama",
    "160" => "Burkburnett ISD",
    "161" => "Burkeville ISD",
    "162" => "Burleson ISD",
    "163" => "Burnet Cons ISD",
    "164" => "Burnham Wood Charter School",
    "165" => "Burton ISD",
    "166" => "Bushland ISD",
    "167" => "Byers ISD",
    "168" => "Bynum ISD",
    "169" => "Caddo Mills ISD",
    "17" => "Alamo Heights ISD",
    "170" => "Calallen ISD",
    "171" => "Caldwell ISD",
    "172" => "Calhoun Co ISD",
    "173" => "California",
    "174" => "Callisburg ISD",
    "175" => "Calvert ISD",
    "176" => "Calvin Nelms Charter Schools",
    "177" => "Cameron ISD",
    "178" => "Campbell ISD",
    "179" => "Canada",
    "18" => "Alaska",
    "180" => "Canadian ISD",
    "181" => "Canton ISD",
    "182" => "Canutillo ISD",
    "183" => "Canyon ISD",
    "184" => "Career Plus Learning Academy",
    "185" => "Carlisle ISD",
    "186" => "Carrizo Springs Cons ISD",
    "187" => "Carroll ISD",
    "188" => "Carrollton-Farmers Branch ISD",
    "189" => "Carthage ISD",
    "19" => "Alba-Golden ISD",
    "190" => "Castleberry ISD",
    "191" => "Cayman Islands",
    "192" => "Cayuga ISD",
    "193" => "Cedar Crest Charter School",
    "194" => "Cedar Hill ISD",
    "195" => "Cedar Ridge Charter School",
    "196" => "Cedars International Academy",
    "197" => "Celeste ISD",
    "198" => "Celina ISD",
    "199" => "Center ISD",
    "2" => "A+ Academy",
    "20" => "Albany ISD",
    "200" => "Center Point ISD",
    "201" => "Centerville ISD",
    "202" => "Central Heights ISD",
    "203" => "Central ISD",
    "204" => "Channelview ISD",
    "205" => "Channing ISD",
    "206" => "Chapel Hill ISD",
    "207" => "Charlotte ISD",
    "208" => "Cherokee ISD",
    "209" => "Chester ISD",
    "21" => "Aldine ISD",
    "210" => "Chico ISD",
    "211" => "Children First Academy of Dall",
    "212" => "Children First Academy of Hous",
    "213" => "Children of the Sun Charter Sc",
    "214" => "Childress ISD",
    "215" => "Chile",
    "216" => "Chillicothe ISD",
    "217" => "Chilton   ISD",
    "218" => "China Spring ISD",
    "219" => "Chireno ISD",
    "22" => "Aledo ISD",
    "220" => "Chisum ISD",
    "221" => "Christoval ISD",
    "222" => "Cisco ISD",
    "223" => "City View ISD",
    "224" => "Clarendon ISD",
    "225" => "Clarksville ISD",
    "226" => "Claude ISD",
    "227" => "Clear Creek ISD",
    "228" => "Cleburne ISD",
    "229" => "Cleveland ISD",
    "23" => "Alice ISD",
    "230" => "Clifton ISD",
    "231" => "Clint ISD",
    "232" => "Clyde Cons ISD",
    "233" => "Coahoma ISD",
    "234" => "Coastal Bend Youth City",
    "235" => "Coldspring-Oakhurst Cons ISD",
    "236" => "Coleman ISD",
    "237" => "College Station ISD",
    "238" => "Collinsville ISD",
    "239" => "Colmesneil ISD",
    "24" => "Alief ISD",
    "240" => "Colorado ISD",
    "241" => "Columbia-Brazoria ISD",
    "242" => "Columbus ISD",
    "243" => "Comal ISD",
    "244" => "Comanche ISD",
    "245" => "Comfort ISD",
    "246" => "Commerce ISD",
    "247" => "Community ISD",
    "248" => "Como-Pickton CISD",
    "249" => "Comquest Academy",
    "25" => "Alief Montessori Community Sch",
    "250" => "Comstock ISD",
    "251" => "Connally ISD",
    "252" => "Connecticut",
    "253" => "Conroe ISD",
    "254" => "Coolidge ISD",
    "255" => "Cooper ISD",
    "256" => "Coppell ISD",
    "257" => "Copperas Cove ISD",
    "258" => "Corpus Christi ISD",
    "259" => "Corrigan-Camden ISD",
    "26" => "Allen ISD",
    "260" => "Corsicana ISD",
    "261" => "Corsicana Residential Treatmen",
    "262" => "Costa Rica",
    "263" => "Cotton Center ISD",
    "264" => "Cotulla ISD",
    "265" => "Coupland ISD",
    "266" => "Covington ISD",
    "267" => "Crandall ISD",
    "268" => "Crane ISD",
    "269" => "Cranfills Gap ISD",
    "27" => "Alpha Charter School",
    "270" => "Crawford ISD",
    "271" => "Crockett Co Cons CSD",
    "272" => "Crockett ISD",
    "273" => "Crockett State School",
    "274" => "Crosby ISD",
    "275" => "Crosbyton CISD",
    "276" => "Cross Plains ISD",
    "277" => "Cross Roads ISD",
    "278" => "Crossroads Community Ed Ctr Ch",
    "279" => "Crowell ISD",
    "28" => "Alphonso Crutch&apos;s-Life Support",
    "280" => "Crowley ISD",
    "281" => "Crystal City ISD",
    "282" => "Cuero ISD",
    "283" => "Culberson County-Allamoore ISD",
    "284" => "Cumberland Academy",
    "285" => "Cumby ISD",
    "286" => "Cushing ISD",
    "287" => "Cypress-Fairbanks ISD",
    "288" => "D&apos;Hanis ISD",
    "289" => "Daingerfield-Lone Star ISD",
    "29" => "Alpine ISD",
    "290" => "Dalhart ISD",
    "291" => "Dallas Can Academy Charter",
    "292" => "Dallas Community Charter Schoo",
    "293" => "Dallas County Juvenile Justice",
    "294" => "Dallas ISD",
    "295" => "Damon ISD",
    "296" => "Danbury ISD",
    "297" => "Darrouzett ISD",
    "298" => "Dawson ISD",
    "299" => "Dayton ISD",
    "3" => "Abbott ISD",
    "30" => "Alto ISD",
    "300" => "De Leon ISD",
    "301" => "Decatur ISD",
    "302" => "Deer Park ISD",
    "303" => "Dekalb ISD",
    "304" => "Del Valle ISD",
    "305" => "Dell City ISD",
    "306" => "Denison ISD",
    "307" => "Denton ISD",
    "308" => "Denver City ISD",
    "309" => "Desoto ISD",
    "31" => "Alvarado ISD",
    "310" => "Detroit ISD",
    "311" => "Devers ISD",
    "312" => "Devine ISD",
    "313" => "Dew ISD",
    "314" => "Deweyville ISD",
    "315" => "Diboll ISD",
    "316" => "Dickinson ISD",
    "317" => "Dilley ISD",
    "318" => "Dime Box ISD",
    "319" => "Dimmitt ISD",
    "32" => "Alvin ISD",
    "320" => "Divide ISD",
    "321" => "Dodd City ISD",
    "322" => "Dominican Republic",
    "323" => "Donna ISD",
    "324" => "Doss Cons CSD",
    "325" => "Douglass ISD",
    "326" => "Dr M L Garza-Gonzalez Charter",
    "327" => "Dripping Springs ISD",
    "328" => "Driscoll ISD",
    "329" => "Dublin ISD",
    "33" => "Alvord ISD",
    "330" => "Dumas ISD",
    "331" => "Duncan, OK",
    "332" => "Duncanville ISD",
    "333" => "Eagle Academy of Beaumont",
    "334" => "Eagle Academy of Brownsville",
    "335" => "Eagle Academy of Bryan",
    "336" => "Eagle Academy of Dallas",
    "337" => "Eagle Academy of Del Rio",
    "338" => "Eagle Academy of Fort Worth",
    "339" => "Eagle Academy of Laredo",
    "34" => "Amarillo ISD",
    "340" => "Eagle Academy of Lubbock",
    "341" => "Eagle Academy of Midland",
    "342" => "Eagle Academy of Pharr/McAllen",
    "343" => "Eagle Academy of San Antonio",
    "344" => "Eagle Academy of Texarkana",
    "345" => "Eagle Academy of Tyler",
    "346" => "Eagle Academy of Waco",
    "347" => "Eagle Advantage School",
    "348" => "Eagle MT-Saginaw ISD",
    "349" => "Eagle Pass ISD",
    "35" => "American Academy of Excellence",
    "350" => "Eanes ISD",
    "351" => "Early ISD",
    "352" => "East Bernard ISD",
    "353" => "East Central ISD",
    "354" => "East Chambers ISD",
    "355" => "East Fort Worth Montessori Aca",
    "356" => "East Texas Charter Schools",
    "357" => "Eastland ISD",
    "358" => "Ector County ISD",
    "359" => "Ector ISD",
    "36" => "American Youth Works Charter S",
    "360" => "Edcouch-Elsa ISD",
    "361" => "Eden C I S D",
    "362" => "Eden Park Academy",
    "363" => "Edgewood ISD",
    "364" => "Edinburg Consolidated",
    "365" => "Edna ISD",
    "366" => "Education Center",
    "367" => "Education Center International",
    "368" => "Ehrhart School",
    "369" => "El Campo ISD",
    "37" => "Amherst ISD",
    "370" => "El Paso Academy",
    "371" => "El Paso ISD",
    "372" => "El Paso School of Excellence",
    "373" => "El Salvador",
    "374" => "Electra ISD",
    "375" => "Elgin ISD",
    "376" => "Elkhart ISD",
    "377" => "Elysian Fields ISD",
    "378" => "Encino School ",
    "379" => "Ennis ISD",
    "38" => "Amigos por Vida-Friends for LI",
    "380" => "Era ISD",
    "381" => "Erath Excels Academy Inc",
    "382" => "Etoile ISD",
    "383" => "Eula ISD",
    "384" => "Eustace ISD",
    "385" => "Evadale ISD",
    "386" => "Evant ISD",
    "387" => "Everman ISD",
    "388" => "Evins Regional Juvenile Center",
    "389" => "Evolution Academy Charter Scho",
    "39" => "Anahuac ISD",
    "390" => "Excelsior ISD",
    "391" => "Ezzell ISD",
    "392" => "Fabens ISD",
    "393" => "Fairfield ISD",
    "394" => "Falls City ISD",
    "395" => "Fannindel ISD",
    "396" => "Farmersville ISD",
    "397" => "Farwell ISD",
    "398" => "Fayetteville ISD",
    "399" => "Ferris ISD",
    "4" => "Abernathy ISD",
    "40" => "Anderson-Shiro Cons ISD",
    "400" => "Flatonia ISD",
    "401" => "Florence ISD",
    "402" => "Floresville ISD",
    "403" => "Florida",
    "404" => "Flour Bluff ISD",
    "405" => "Floydada ISD",
    "406" => "Focus Learning Academy",
    "407" => "Follett ISD",
    "408" => "Forestburg ISD",
    "409" => "Forney ISD",
    "41" => "Andrews ISD",
    "410" => "Forsan ISD",
    "411" => "Fort Bend ISD",
    "412" => "Fort Elliott CISD",
    "413" => "Fort Worth Academy of Fine Art",
    "414" => "Fort Worth Can Academy",
    "415" => "Fort Worth ISD",
    "416" => "France",
    "417" => "Franklin ISD",
    "418" => "Frankston ISD",
    "419" => "Fredericksburg ISD",
    "42" => "Angleton ISD",
    "420" => "Freer ISD",
    "421" => "Frenship ISD",
    "422" => "Friendswood ISD",
    "423" => "Friona ISD",
    "424" => "Frisco ISD",
    "425" => "Frost ISD",
    "426" => "Fruit of Excellence",
    "427" => "Fruitvale ISD",
    "428" => "Ft Davis ISD",
    "429" => "Ft Hancock ISD",
    "43" => "Anguilla ",
    "430" => "Ft Sam Houston ISD",
    "431" => "Ft Stockton ISD",
    "432" => "Gabriel Tafolla Charter School",
    "433" => "Gainesville ISD",
    "434" => "Galena Park ISD",
    "435" => "Galveston ISD",
    "436" => "Ganado ISD",
    "437" => "Garland ISD",
    "438" => "Garner ISD",
    "439" => "Garrison ISD",
    "44" => "Anna ISD",
    "440" => "Gary ISD",
    "441" => "Gatesville ISD",
    "442" => "Gateway  Academy",
    "443" => "Gateway Charter Academy",
    "444" => "Gause ISD",
    "445" => "George Gervin Academy",
    "446" => "George I Sanchez",
    "447" => "George I Sanchez Charter HS SA",
    "448" => "George West ISD",
    "449" => "Georgetown ISD",
    "45" => "Anson ISD",
    "450" => "Georgia",
    "451" => "Germany",
    "452" => "Gholson ISD",
    "453" => "Giddings ISD",
    "454" => "Giddings State School",
    "455" => "Gilmer ISD",
    "456" => "Girls &amp; Boys Prep Academy",
    "457" => "Gladewater ISD",
    "458" => "Glasscock County ISD",
    "459" => "Glen Rose ISD",
    "46" => "Anthony",
    "460" => "Godley ISD",
    "461" => "Gold Burg ISD",
    "462" => "Golden Rule Charter School",
    "463" => "Goldthwaite ISD",
    "464" => "Goliad ISD",
    "465" => "Gonzales ISD",
    "466" => "Goodrich ISD",
    "467" => "Goose Creek CISD",
    "468" => "Gordon ISD",
    "469" => "Gorman ISD",
    "47" => "Anton ISD",
    "470" => "Grady ISD",
    "471" => "Graford ISD",
    "472" => "Graham ISD",
    "473" => "Granbury ISD",
    "474" => "Grand Prairie ISD",
    "475" => "Grand Saline ISD",
    "476" => "Grandfalls-Royalty ISD",
    "477" => "Grandview ISD",
    "478" => "Grandview-Hopkins ISD",
    "479" => "Granger ISD",
    "48" => "Apple Springs ISD",
    "480" => "Grape Creek ISD",
    "481" => "Grapeland ISD",
    "482" => "Grapevine-Colleyville ISD",
    "483" => "Greenville ISD",
    "484" => "Greenwood ISD",
    "485" => "Gregory-Portland ISD",
    "486" => "Groesbeck ISD",
    "487" => "Groom ISD",
    "488" => "Groveton ISD",
    "489" => "Gruver ISD",
    "49" => "Aquilla ISD",
    "490" => "Guardian Angel Performance Aca",
    "491" => "Guatemala",
    "492" => "Gulf Shores Academy",
    "493" => "Gunter ISD",
    "494" => "Gustine ISD",
    "495" => "Guthrie CSD",
    "496" => "Hale Center ISD",
    "497" => "Hallettsville ISD",
    "498" => "Hallsburg ISD",
    "499" => "Hallsville ISD",
    "5" => "Abilene ISD",
    "50" => "Aransas County ISD",
    "500" => "Hamilton ISD",
    "501" => "Hamlin ISD",
    "502" => "Hamshire-Fannett ISD",
    "503" => "Happy ISD",
    "504" => "Hardin ISD",
    "505" => "Hardin-Jefferson ISD",
    "506" => "Harlandale ISD",
    "507" => "Harleton ISD",
    "508" => "Harlingen Cons ISD",
    "509" => "Harmony ISD",
    "51" => "Aransas Pass ISD",
    "510" => "Harmony Science Academy",
    "511" => "Harmony Science Academy - Aust",
    "512" => "Harper ISD",
    "513" => "Harris County Juvenile Justice",
    "514" => "Harrold ISD",
    "515" => "Hart ISD",
    "516" => "Hartley ISD",
    "517" => "Harts Bluff ISD",
    "518" => "Haskell CISD",
    "519" => "Hawkins ISD",
    "52" => "Archer City ISD",
    "520" => "Hawley ISD",
    "521" => "Hays Cons ISD",
    "522" => "Hearne ISD",
    "523" => "Hedley ISD",
    "524" => "Heights Charter School",
    "525" => "Hemphill ISD",
    "526" => "Hempstead ISD",
    "527" => "Henderson ISD",
    "528" => "Henrietta ISD",
    "529" => "Hereford ISD",
    "53" => "Argyle ISD",
    "530" => "Hermleigh ISD",
    "531" => "Hico ISD",
    "532" => "Hidalgo ISD",
    "533" => "Higgins ISD",
    "534" => "Higgs/Carter/King Gifted &amp; Tal",
    "535" => "High Island ISD",
    "536" => "Highland ISD",
    "537" => "Highland Park ISD",
    "538" => "Hillsboro ISD",
    "539" => "Hitchcock ISD",
    "54" => "Arizona",
    "540" => "Holland ISD",
    "541" => "Holliday ISD",
    "542" => "Hondo ISD",
    "543" => "Honduras",
    "544" => "Honey Grove ISD",
    "545" => "Honors Academy",
    "546" => "Hooks ISD",
    "547" => "Houston Alternative Preparator",
    "548" => "Houston Can Academy Charter Sc",
    "549" => "Houston Gateway Academy",
    "55" => "Arkansas",
    "550" => "Houston Heights Learning Acade",
    "551" => "Houston ISD",
    "552" => "Howe ISD",
    "553" => "Hubbard ISD",
    "554" => "Huckabay ISD",
    "555" => "Hudson ISD",
    "556" => "Huffman ISD",
    "557" => "Hughes Springs ISD",
    "558" => "Hull-Daisetta ISD",
    "559" => "Humble ISD",
    "56" => "Arlington Classics Academy",
    "560" => "Hunt ISD",
    "561" => "Huntington ISD",
    "562" => "Huntsville ISD",
    "563" => "Hurst-Euless-Bedford ISD",
    "564" => "Hutto ISD",
    "565" => "I Am That I Am Academy",
    "566" => "I D E A  Academy",
    "567" => "Idalou ISD",
    "568" => "Impact Charter",
    "569" => "Indiana",
    "57" => "Arlington ISD",
    "570" => "Indonesia",
    "571" => "Industrial ISD",
    "572" => "Ingleside ISD",
    "573" => "Ingram ISD",
    "574" => "Inspired Vision Academy",
    "575" => "International",
    "576" => "Iola ISD",
    "577" => "Iowa Park Cons ISD",
    "578" => "Ira ISD",
    "579" => "Iraan-Sheffield ISD",
    "58" => "Arp ISD",
    "580" => "Iredell ISD",
    "581" => "Irion Co ISD",
    "582" => "Irving ISD",
    "583" => "Italy",
    "584" => "Italy ISD",
    "585" => "Itasca ISD",
    "586" => "Jacksboro ISD",
    "587" => "Jacksonville ISD",
    "588" => "Jamaica",
    "589" => "Jamie&apos;s House Charter School",
    "59" => "Aruba",
    "590" => "Jarrell ISD",
    "591" => "Jasper ISD",
    "592" => "Jayton-Girard ISD",
    "593" => "Jean Massieu Academy",
    "594" => "Jefferson ISD",
    "595" => "Jesse Jackson Academy",
    "596" => "Jim Hogg County ISD",
    "597" => "Jim Ned Cons ISD",
    "598" => "Joaquin ISD",
    "599" => "John H Wood Charter School",
    "6" => "Academy ISD",
    "60" => "Aspermont ISD",
    "600" => "Johnson City ISD",
    "601" => "Jonesboro ISD",
    "602" => "Joshua ISD",
    "603" => "Jourdanton ISD",
    "604" => "Juan B Galaviz Charter School",
    "605" => "Jubilee Academic Center",
    "606" => "Judson ISD",
    "607" => "Junction ISD",
    "608" => "Karnack ISD",
    "609" => "Karnes City ISD",
    "61" => "Athens ISD",
    "610" => "Katherine Anne Porter School",
    "611" => "Katy ISD",
    "612" => "Kaufman ISD",
    "613" => "Keene ISD",
    "614" => "Keller ISD",
    "615" => "Kelton ISD",
    "616" => "Kemp ISD",
    "617" => "Kendleton ISD",
    "618" => "Kenedy County Wide CSD",
    "619" => "Kenedy ISD",
    "62" => "Atlanta ISD",
    "620" => "Kennard ISD",
    "621" => "Kennedale ISD",
    "622" => "Kentucky",
    "623" => "Kerens ISD",
    "624" => "Kermit ISD",
    "625" => "Kerrville ISD",
    "626" => "Kilgore ISD",
    "627" => "Killeen ISD",
    "628" => "Kingston",
    "629" => "Kingsville ISD",
    "63" => "Aubrey ISD",
    "630" => "Kipp Inc Charter",
    "631" => "Kirbyville CISD",
    "632" => "Klein ISD",
    "633" => "Klondike ISD",
    "634" => "Knippa ISD",
    "635" => "Knox City OBrien",
    "636" => "Kopperl ISD",
    "637" => "Kountze ISD",
    "638" => "Kress ISD",
    "639" => "Krum ISD",
    "64" => "Austin Can Academy Charter Sch",
    "640" => "La Amistad Love &amp; Learning Aca",
    "641" => "La Escuela de las Americas",
    "642" => "La Feria ISD",
    "643" => "La Gloria ISD",
    "644" => "La Grange ISD",
    "645" => "La Joya ISD",
    "646" => "La Marque ISD",
    "647" => "La Porte ISD",
    "648" => "La Pryor ISD",
    "649" => "La Vega ISD",
    "65" => "Austin ISD",
    "650" => "La Vernia ISD",
    "651" => "La Villa ISD",
    "652" => "Lackland ISD",
    "653" => "Lago Vista ISD",
    "654" => "Lake Dallas ISD",
    "655" => "Lake Travis ISD",
    "656" => "Lake Worth ISD",
    "657" => "Lamar Consolidated ISD",
    "658" => "Lamesa ISD",
    "659" => "Lampasas ISD",
    "66" => "Australia",
    "660" => "Lancaster ISD",
    "661" => "Laneville ISD",
    "662" => "Lapoynor ISD",
    "663" => "Laredo ISD",
    "664" => "Lasara ISD",
    "665" => "Latexo ISD",
    "666" => "Lazbuddie ISD",
    "667" => "Leakey ISD",
    "668" => "Leander ISD",
    "669" => "Leary ISD",
    "67" => "Austwell-Tivoli ISD",
    "670" => "Lefors ISD",
    "671" => "Leggett ISD",
    "672" => "Leon ISD",
    "673" => "Leonard ISD",
    "674" => "Levelland ISD",
    "675" => "Leveretts Chapel ISD",
    "676" => "Lewisville ISD",
    "677" => "Lexington ISD",
    "678" => "Liberty Hill ISD",
    "679" => "Liberty ISD",
    "68" => "Avalon ISD",
    "680" => "Liberty-Eylau ISD",
    "681" => "Life School",
    "682" => "Lighthouse Charter School",
    "683" => "Lindale ISD",
    "684" => "Linden-Kildare Cons ISD",
    "685" => "Lindsay ISD",
    "686" => "Lingleville ISD",
    "687" => "Lipan ISD",
    "688" => "Little Cypress-Mauriceville Ci",
    "689" => "Little Elm ISD",
    "69" => "Avery ISD",
    "690" => "Littlefield ISD",
    "691" => "Livingston ISD",
    "692" => "Llano ISD",
    "693" => "Lockhart ISD",
    "694" => "Lockney ISD",
    "695" => "Lohn ISD",
    "696" => "Lometa ISD",
    "697" => "London ISD",
    "698" => "Lone Oak ISD",
    "699" => "Longview ISD",
    "7" => "Academy of Accelerated Learnin",
    "70" => "Avinger ISD",
    "700" => "Loop ISD",
    "701" => "Loraine ISD",
    "702" => "Lorena ISD",
    "703" => "Lorenzo ISD",
    "704" => "Los Fresnos Cons ISD",
    "705" => "Louise ISD",
    "706" => "Louisiana",
    "707" => "Lovejoy ISD",
    "708" => "Lovelady ISD",
    "709" => "Lubbock ISD",
    "71" => "Axtell ISD",
    "710" => "Lubbock-Cooper ISD",
    "711" => "Lueders-Avoca ISD",
    "712" => "Lufkin ISD",
    "713" => "Luling ISD",
    "714" => "Lumberton ISD",
    "715" => "Lyford CISD",
    "716" => "Lytle ISD",
    "717" => "Mabank ISD",
    "718" => "Madisonville Cons ISD",
    "719" => "Magnolia ISD",
    "72" => "Azle ISD",
    "720" => "Maine",
    "721" => "Mainland Preparatory Academy",
    "722" => "Malakoff ISD",
    "723" => "Malone ISD",
    "724" => "Malta ISD",
    "725" => "Manor ISD",
    "726" => "Mansfield ISD",
    "727" => "Marathon ISD",
    "728" => "Marble Falls ISD",
    "729" => "Marfa ISD",
    "73" => "Azleway Charter School",
    "730" => "Marietta ISD",
    "731" => "Marion ISD",
    "732" => "Marlin ISD",
    "733" => "Marlin Orientation and Assessm",
    "734" => "Marshall ISD",
    "735" => "Mart ISD",
    "736" => "Martins Mill ISD",
    "737" => "Martinsville ISD",
    "738" => "Maryland",
    "739" => "Mason ISD",
    "74" => "Bahamas",
    "740" => "Masonic Home ISD",
    "741" => "Matagorda ISD",
    "742" => "Mathis ISD",
    "743" => "Maud ISD",
    "744" => "May ISD",
    "745" => "Maypearl ISD",
    "746" => "McAllen ISD",
    "747" => "McCamey ISD",
    "748" => "McCullough Academy of Excellen",
    "749" => "McDade ISD",
    "75" => "Baird ISD",
    "750" => "McGregor ISD",
    "751" => "McKinney ISD",
    "752" => "McLean ISD",
    "753" => "McLennan Co St Juvenile Correc",
    "754" => "McLeod ISD",
    "755" => "McMullen County ISD",
    "756" => "Meadow ISD",
    "757" => "Medical Center Charter School",
    "758" => "Medina ISD",
    "759" => "Medina Valley ISD",
    "76" => "Ballinger ISD",
    "760" => "Megargel ISD",
    "761" => "Melissa ISD",
    "762" => "Memphis ISD",
    "763" => "Menard ISD",
    "764" => "Mercedes ISD",
    "765" => "Meridian ISD",
    "766" => "Merkel ISD",
    "767" => "Mesquite ISD",
    "768" => "Metro Charter Academy",
    "769" => "Mexia ISD",
    "77" => "Balmorhea ISD",
    "770" => "Mexico",
    "771" => "Meyersville ISD",
    "772" => "Miami ISD",
    "773" => "Mid-Valley Academy",
    "774" => "Midland Academy Charter School",
    "775" => "Midland ISD",
    "776" => "Midlothian ISD",
    "777" => "Midway ISD",
    "778" => "Midway ISD, Clay County",
    "779" => "Milano ISD",
    "78" => "Bandera ISD",
    "780" => "Mildred ISD",
    "781" => "Miles ISD",
    "782" => "Milford ISD",
    "783" => "Miller Grove ISD",
    "784" => "Millsap ISD",
    "785" => "Mineola ISD",
    "786" => "Mineral Wells ISD",
    "787" => "Mirando City ISD",
    "788" => "Mission Cons ISD",
    "789" => "Monahans-Wickett-Pyote ISD",
    "79" => "Bangs ISD",
    "790" => "Montague ISD",
    "791" => "Montana",
    "792" => "Monte Alto ISD",
    "793" => "Montgomery ISD",
    "794" => "Moody ISD",
    "795" => "Moran ISD",
    "796" => "Morgan ISD",
    "797" => "Morgan Mill ISD",
    "798" => "Morton ISD",
    "799" => "Motley County ISD",
    "8" => "Academy of Beaumont",
    "80" => "Banquete ISD",
    "800" => "Moulton ISD",
    "801" => "Mount Calm ISD",
    "802" => "Mount Enterprise ISD",
    "803" => "Mount Pleasant ISD",
    "804" => "Mount Vernon ISD",
    "805" => "Muenster ISD",
    "806" => "Muleshoe ISD",
    "807" => "Mullin ISD",
    "808" => "Mumford ISD",
    "809" => "Munday CISD",
    "81" => "Barbados",
    "810" => "Murchison ISD",
    "811" => "Nacogdoches ISD",
    "812" => "Nancy Ney Charter School",
    "813" => "Natalia ISD",
    "814" => "Navarro ISD",
    "815" => "Navasota ISD",
    "816" => "Nazareth ISD",
    "817" => "Neches ISD",
    "818" => "Nederland ISD",
    "819" => "Needville ISD",
    "82" => "Barbers Hill ISD",
    "820" => "Nevada",
    "821" => "New Boston ISD",
    "822" => "New Braunfels ISD",
    "823" => "New Caney ISD",
    "824" => "New Deal ISD",
    "825" => "New Diana ISD",
    "826" => "New Frontiers Charter School",
    "827" => "New Home ISD",
    "828" => "New Mexico",
    "829" => "New Summerfield ISD",
    "83" => "Bartlett ISD",
    "830" => "New Waverly ISD",
    "831" => "New York",
    "832" => "Newcastle ISD",
    "833" => "Newton ISD",
    "834" => "Nicaragua",
    "835" => "Nixon-Smiley Cons ISD",
    "836" => "Nocona ISD",
    "837" => "Nordheim ISD",
    "838" => "Normangee ISD",
    "839" => "North Carolina",
    "84" => "Bastrop ISD",
    "840" => "North East ISD",
    "841" => "North Forest ISD",
    "842" => "North Hills School",
    "843" => "North Hopkins ISD",
    "844" => "North Houston H S For Business",
    "845" => "North Lamar ISD",
    "846" => "North Zulch ISD",
    "847" => "Northside ISD",
    "848" => "Northwest ISD",
    "849" => "Northwest Mathematics Science",
    "85" => "Bay Area Charter School",
    "850" => "Northwest Preparatory",
    "851" => "Nova Charter School",
    "852" => "Nova Charter School (Southeast",
    "853" => "Novice ISD",
    "854" => "NTREIS TEST ONLY",
    "855" => "Nueces Canyon CISD",
    "856" => "Nursery ISD",
    "857" => "Nyos Charter School",
    "858" => "O&apos;Donnell ISD",
    "859" => "Oakwood ISD",
    "86" => "Bay City ISD",
    "860" => "Odem-Edroy ISD",
    "861" => "Odyssey Academy Inc",
    "862" => "Oglesby ISD",
    "863" => "Oklahoma",
    "864" => "Olfen ISD",
    "865" => "Olney ISD",
    "866" => "Olton ISD",
    "867" => "Onalaska ISD",
    "868" => "One Stop Multiservice Charter",
    "869" => "Orange Grove ISD",
    "87" => "Beatrice Mayes Institute Chart",
    "870" => "Orangefield ISD",
    "871" => "Ore City ISD",
    "872" => "Oregon",
    "873" => "Outreach Word Academy",
    "874" => "Overton ISD",
    "875" => "Paducah ISD",
    "876" => "Paint Creek ISD",
    "877" => "Paint Rock ISD",
    "878" => "Palacios ISD",
    "879" => "Palestine ISD",
    "88" => "Beaumont ISD",
    "880" => "Palmer ISD",
    "881" => "Palo Pinto ISD",
    "882" => "Pampa ISD",
    "883" => "Panama",
    "884" => "Panhandle ISD",
    "885" => "Panola Charter School",
    "886" => "Panola Public SD",
    "887" => "Panther Creek Cons ISD",
    "888" => "Paradigm Accelerated School",
    "889" => "Paradise ISD",
    "89" => "Beckville ISD",
    "890" => "Paris ISD",
    "891" => "Pasadena ISD",
    "892" => "Paso Del Norte Academy",
    "893" => "Patton Springs ISD",
    "894" => "Pawnee ISD",
    "895" => "Pearland ISD",
    "896" => "Pearsall ISD",
    "897" => "Peaster ISD",
    "898" => "Pecos-Barstow-Toyah ISD",
    "899" => "Pegasus Charter School",
    "9" => "Academy of Careers and Technol",
    "90" => "Beeville ISD",
    "900" => "Penelope ISD",
    "901" => "Perrin-Whitt Cons ISD",
    "902" => "Perryton ISD",
    "903" => "Peru ISD",
    "904" => "Petersburg ISD",
    "905" => "Petrolia ISD",
    "906" => "Pettus ISD",
    "907" => "Pewitt ISD",
    "908" => "Pflugerville ISD",
    "909" => "Pharr-San Juan-Alamo ISD",
    "91" => "Belize",
    "910" => "Phoenix Charter School",
    "911" => "Pilot Point ISD",
    "912" => "Pine Tree ISD",
    "913" => "Pineywoods Community Academy",
    "914" => "Pittsburg ISD",
    "915" => "Plains ISD",
    "916" => "Plainview ISD",
    "917" => "Plano ISD",
    "918" => "Pleasant Grove ISD",
    "919" => "Pleasanton ISD",
    "92" => "Bellevue ISD",
    "920" => "Plemons-Stinnett-Phillips Cons",
    "921" => "Point Isabel ISD",
    "922" => "Ponder ISD",
    "923" => "Poolville ISD",
    "924" => "Por Vida Acad Charter H S",
    "925" => "Port Aransas ISD",
    "926" => "Port Arthur ISD",
    "927" => "Port Neches-Groves ISD",
    "928" => "Positive Solutions Charter Sch",
    "929" => "Post ISD",
    "93" => "Bells ISD",
    "930" => "Poteet ISD",
    "931" => "Poth ISD",
    "932" => "Pottsboro ISD",
    "933" => "Prairie Lea ISD",
    "934" => "Prairie Valley ISD",
    "935" => "Prairiland ISD",
    "936" => "Premont ISD",
    "937" => "Presidio ISD",
    "938" => "Priddy ISD",
    "939" => "Princeton ISD",
    "94" => "Bellville ISD",
    "940" => "Pringle-Morse Cons ISD",
    "941" => "Progreso ISD",
    "942" => "Prosper ISD",
    "943" => "Puerto Rico",
    "944" => "Quanah ISD",
    "945" => "Queen City ISD",
    "946" => "Quinlan ISD",
    "947" => "Quitman ISD",
    "948" => "Radiance Academy of Learning",
    "949" => "Rains ISD",
    "95" => "Belton ISD",
    "950" => "Ralls ISD",
    "951" => "Ramirez CSD",
    "952" => "Ranch Academy",
    "953" => "Randolph Field ISD",
    "954" => "Ranger ISD",
    "955" => "Rankin ISD",
    "956" => "Rapoport Charter School",
    "957" => "Raul Yzaguirre School for Succ",
    "958" => "Raven School",
    "959" => "Raymondville ISD",
    "96" => "Ben Bolt-Palito Blanco ISD",
    "960" => "Reagan County ISD",
    "961" => "Red Lick ISD",
    "962" => "Red Oak ISD",
    "963" => "Redwater ISD",
    "964" => "Refugio ISD",
    "965" => "Ricardo ISD",
    "966" => "Rice Cons ISD",
    "967" => "Rice ISD",
    "968" => "Richard Milburn Academy - Amar",
    "969" => "Richard Milburn Academy - Beau",
    "97" => "Benavides ISD",
    "970" => "Richard Milburn Academy - Corp",
    "971" => "Richard Milburn Academy - Ecto",
    "972" => "Richard Milburn Academy - Fort",
    "973" => "Richard Milburn Academy - Lubb",
    "974" => "Richard Milburn Academy - Midl",
    "975" => "Richard Milburn Academy - Subu",
    "976" => "Richard Milburn Academy-Killee",
    "977" => "Richards ISD",
    "978" => "Richardson ISD",
    "979" => "Richland Springs ISD",
    "98" => "Benjamin ISD",
    "980" => "Riesel ISD",
    "981" => "Rio Grande City CISD",
    "982" => "Rio Hondo ISD",
    "983" => "Rio Vista ISD",
    "984" => "Ripley House Charter School",
    "985" => "Rise Academy",
    "986" => "Rising Star ISD",
    "987" => "River Road ISD",
    "988" => "Rivercrest ISD",
    "989" => "Riviera ISD",
    "99" => "Benji&apos;s Special Educational Ac",
    "990" => "Robert Lee ISD",
    "991" => "Robinson ISD",
    "992" => "Robstown ISD",
    "993" => "Roby Cons ISD",
    "994" => "Rochelle ISD",
    "995" => "Rochester",
    "996" => "Rochester County Line ISD",
    "997" => "Rockdale ISD",
    "998" => "Rocksprings ISD",
    "999" => "Rockwall ISD"
    }

  YEAR_BUILT_DETAILS = {
    "NCC" => "New Construction - Complete",
    "NCI" => "New Construction - Incomplete",
    "PREOWN" => "Preowned",
    "PROPOS" => "Proposed",
    "UNKNOW" => "Unknown"
  }
  FINANCING_PROPOSED = {
    "ASSLOA" => "Assumable Loan",
    "BONMON" => "Bond Money",
    "CASH" => "Cash",
    "CONAGE" => "Contact Agent",
    "CONASS" => "Conventional Assumable",
    "CONVEN" => "Conventional",
    "ESCALA" => "Escalating",
    "FELABA" => "Federal Land Bank",
    "FHA" => "FHA",
    "FHAASS" => "FHA Assumable",
    "FIXED" => "Fixed",
    "FMHA" => "FMHA",
    "LEAPUR" => "Lease Purchase",
    "NOTASS" => "Not Assumable",
    "OCF" => "Owner Carry  First",
    "OCS" => "Owner Carry  Second",
    "OTHER" => "Other",
    "OWCAFI" => "Owner Carry First",
    "OWCASE" => "Owner Carry Second",
    "PRIASS" => "Private Assumable",
    "RELIRE" => "Release Liability Reqd",
    "TEXVET" => "Texas Vet",
    "TRAEXC" => "Trade/Exchange",
    "TXVEAS" => "TX VET Assumable",
    "VA" => "VA",
    "VAASS" => "VA Assumable",
    "VARERE" => "VA Reinstatement Reqd"
  }
  FENCING = {
    "AUTGAT" => "Automatic Gate",
    "BARWIR" => "Barbed Wire",
    "BRICK" => "Brick",
    "CHALIN" => "Chain Link",
    "CROFEN" => "Cross Fenced",
    "DOGRUN" => "Dog Run",
    "IRON" => "Iron",
    "METAL" => "Metal",
    "NET" => "Net",
    "NONE" => "None",
    "OTHER" => "Other",
    "PARFEN" => "Partially Fenced",
    "PIPE" => "Pipe",
    "RAIL" => "Rail",
    "ROCSTO" => "Rock/Stone",
    "SLSMWI" => "Slick/Smooth Wire",
    "VINYL" => "Vinyl",
    "WOOD" => "Wood"
  }
  FLOORING = {
    "BRIADO" => "Brick/Adobe",
    "CARPET" => "Carpet",
    "CERTIL" => "Ceramic Tile",
    "CONCRE" => "Concrete",
    "LAMINA" => "Laminate",
    "MARBLE" => "Marble",
    "OTHER" => "Other",
    "PARQUE" => "Parquet",
    "SLATE" => "Slate",
    "STONE" => "Stone",
    "TERRAZ" => "Terrazzo",
    "VINYL" => "Vinyl",
    "WOOD" => "Wood",
    "WOUNCA" => "Wood Under Carpet"
  }
  FOUNDATION_DETAILS = {
    "BASEME" => "Basement",
    "BODAPO" => "Bois DArc Post",
    "OTHER" => "Other",
    "PIBE" => "Pier &amp; Beam",
    "PIBESL" => "Piered Beam Slab",
    "PILING" => "Pilings",
    "SLAB" => "Slab"
  }
  HOTSHEET_ENUMS = {
    status: hash_from_csv('statuses'),
  }
  HOTSHEET_MAPPING = {
    list_price: 'List Price',
    mls_number: 'MLS #',
    sold_lease_price: 'Lease Price',
    sold_price: 'Sp',
    status: 'St',
  }
  INTERIOR_FEATURES = {
    "BAYWIN" => "Bay Windows",
    "BUWICO" => "Built-in Wine Cooler",
    "CATVAV" => "Cable TV Available",
    "CENVAC" => "Central Vac",
    "DECLIG" => "Decorative Lighting",
    "DRYBAR" => "Dry Bar",
    "ELESHA" => "Electric Shades",
    "ELEVAT" => "Elevator",
    "FLSCWI" => "Flat Screen Wiring",
    "HSIA" => "High Speed Internet Available",
    "INTERC" => "Intercom",
    "LOFT" => "Loft",
    "MULSTA" => "Multiple Staircases",
    "OTHER" => "Other",
    "PANELI" => "Paneling",
    "PLASHU" => "Plantation Shutters",
    "SKYLIG" => "Skylights",
    "SMHOSY" => "Smart Home System",
    "SOSYWI" => "Sound System Wiring",
    "VAUCEI" => "Vaulted Ceilings",
    "WAINSC" => "Wainscoting",
    "WATFIL" => "Water Filter",
    "WATPUR" => "Water Purifier",
    "WATSOF" => "Water Softener",
    "WETBAR" => "Wet Bar",
    "WINCOV" => "Window Coverings"
  }
  
  LOT_FEATURES = {
    "ACREAG" => "Acreage",
    "ADTOGR" => "Adjacent to Greenbelt",
    "AIRSTR" => "Airstrip",
    "CAMAMA" => "Canal (Man Made)",
    "CORNER" => "Corner",
    "CREEK" => "Creek",
    "CUDESA" => "Cul De Sac",
    "CULTIV" => "Cultivated",
    "GOCOLO" => "Golf Course Lot",
    "GREENB" => "Greenbelt",
    "HEATRE" => "Heavily Treed",
    "HORPER" => "Horses Permitted",
    "INTLOT" => "Interior Lot",
    "IRREGU" => "Irregular",
    "LAKFRO" => "Lake Front",
    "LANDSC" => "Landscaped",
    "LEASEH" => "Leasehold",
    "LFMB" => "Lake Front - Main Body",
    "LRBAGR" => "Lrg. Backyard Grass",
    "NOBAGR" => "No Backyard Grass",
    "PARCUL" => "Partially Cultivated",
    "PARVIE" => "Park View",
    "PASTUR" => "Pasture",
    "RIVFRO" => "River Front",
    "SOMTRE" => "Some Trees",
    "SUBDIV" => "Subdivision",
    "TAPO" => "Tank/ Pond",
    "TAXIWA" => "Taxi-way",
    "UNDIVI" => "Undivided",
    "WALAVI" => "Water/Lake View"
  }
  
  TOPOGRAPHY = {
    "BOTTOM" => "Bottom",
    "BRUSH" => "Brush",
    "CEDAR" => "Cedar",
    "CLEARE" => "Cleared",
    "FLOPLA" => "Flood Plain",
    "GULLIE" => "Gullies",
    "HILLY" => "Hilly",
    "LEVEL" => "Level",
    "MESQUI" => "Mesquite",
    "NEEFIL" => "Need Fill",
    "OAK" => "Oak",
    "OTHER" => "Other",
    "PINE" => "Pine",
    "ROLLIN" => "Rolling",
    "RUGGED" => "Rugged",
    "SHINNE" => "Shinnery",
    "SLOPED" => "Sloped",
    "STEEP" => "Steep",
    "VARIED" => "Varied"
  }
  ROAD_FRONTAGE = {
    "ASPHAL" => "Asphalt",
    "CITY" => "City",
    "CONCRE" => "Concrete",
    "COUNTY" => "County",
    "CURGUT" => "Curbs/Gutters",
    "DIRT" => "Dirt",
    "FMROA" => "FM Road",
    "GRAROC" => "Gravel/Rock",
    "INTERE" => "Interest",
    "INTERS" => "Interstate",
    "NONE" => "None",
    "OISC" => "Oil &amp; Screen",
    "OTHER" => "Other",
    "PRIVAT" => "Private",
    "ROAEAS" => "Road Easement",
    "SIDEWA" => "Sidewalk",
    "STATE" => "State",
    "STOSEW" => "Storm Sewer",
    "USHIG" => "U.S. Highway"
  }
  ROOF = {
    "BUILTU" => "Built-Up",
    "COMPOS" => "Composition",
    "CONCRE" => "Concrete",
    "FIBCEM" => "Fiber Cement",
    "METAL" => "Metal",
    "OTHER" => "Other",
    "OVERLA" => "Overlay",
    "SHAMET" => "Shake Metal",
    "TARGRA" => "Tar/Gravel",
    "TILSLA" => "Tile/Slate",
    "WOOSHA" => "Wood Shake",
    "WOOSHI" => "Wood Shingle"
  }
  ARCHITECTURAL_STYLE = {
    "AFRAME" => "A-Frame",
    "COLONI" => "Colonial",
    "CONMOD" => "Contemporary/Modern",
    "CRAFTS" => "Craftsman",
    "EARAME" => "Early American",
    "ENGLIS" => "English",
    "FRENCH" => "French",
    "GEODOM" => "Geo/Dome",
    "LOFT" => "Loft",
    "MEDITE" => "Mediterranean",
    "MIDMOD" => "Mid-Century Modern",
    "ORIENT" => "Oriental",
    "OTHER" => "Other",
    "PRAIRI" => "Prairie",
    "RANCH" => "Ranch",
    "SOUTHW" => "Southwestern",
    "SPANIS" => "Spanish",
    "SPLLEV" => "Split Level",
    "STUDIO" => "Studio",
    "TRADIT" => "Traditional",
    "TUDOR" => "Tudor",
    "VICTOR" => "Victorian"
  
  }
  ASSOCIATION_TYPE = {
    "MANDAT" => "Mandatory",
    "NONE" => "None",
    "VOLUNT" => "Voluntary"
  }
  OTHER_EQUIPMENT = {
    "BUCOMA" => "Built-in Coffee Maker",
    "BUICOM" => "Built-in Compactor",
    "BUIICE" => "Built-in Icemaker",
    "BUIMIC" => "Built-in Microwave",
    "BUREFR" => "Built-in Refrigerator/Freezer",
    "COEL" => "Cooktop - Electric",
    "COGA" => "Cooktop - Gas",
    "COGRRA" => "Commercial Grade Range",
    "COGRVE" => "Commercial Grade Vent",
    "CONOVE" => "Convection Oven",
    "DIROG" => "Drop in Range/Oven - Gas",
    "DISHWA" => "Dishwasher",
    "DISPOS" => "Disposal",
    "DOUOVE" => "Double Oven",
    "DRYER" => "Dryer",
    "DUADIS" => "Dual Dishwashers",
    "DUFURA" => "Dual Fuel Range",
    "INDGRI" => "Indoor Grill",
    "NONE" => "None",
    "OTHER" => "Other",
    "OVENEL" => "Oven-Electric",
    "OVENGA" => "Oven-Gas",
    "PFGIK" => "Plumbed For Gas in Kitchen",
    "RANOVEGA" => "Range/Oven-Gas",
    "RANOVELE" => "Range/Oven-Electric",
    "REFRIG" => "Refrigerator",
    "VENMEC" => "Vent Mechanism",
    "WAOVDR" => "Warmer Oven Drawer",
    "WASHER" => "Washer",
    "WLTR" => "Water Line to Refrigerator"
  }
  LOOKUP_TYPE_ENUMS = {
  }

  DATA_LOCATION = hash_from_csv('locations')
  DATA_MLS_AREA = hash_from_csv('mls_areas')
  DATA_MLS_SUB_AREA = hash_composite('mls_sub_areas')
  DATA_ENUMS = {
    category: hash_from_csv('categories'),
    school_district_name: hash_from_csv('school_districts'),
    county: hash_from_csv('counties'),
    mls_area_name: hash_from_csv('mls_area_names'),
    lot_use: hash_from_csv('lot_uses'),
    
    # rental_class: hash_from_csv('rental_classes'),
    # rental_type: hash_from_csv('rental_types'),
    # single_family_type: hash_from_csv('single_family_types'),
    status: hash_from_csv('statuses'),
    state: hash_from_csv('states'),
    hoa_bill_freq: hash_from_csv('hoa_bill_freqs')
  }
  MLS_SUB_AREA_NAME =  hash_from_csv('mls_sub_areas')
  DATA_MAPPING = {
    access_lock_box: 'KeyboxNumber' ,
    acreage: 'LotSizeArea',
    address: 'STREETDIR',    # CONCATENATE ("StreetDirPrefix"+"StreetDirSuffix")
    adj_sale_price: 'SALESPRICE',
    adj_sale_price_psf: 'SQFTPRICESOLD',
    annual_maintenance_fee: proc { |d| 
      payment = nil
      freq = nil 
      if (d['AssociationFee'] && d['AssociationFeeFrequency']) 
        payment = d['AssociationFee'].to_s.to_d  
        (payment * ASSOCIATION_FEE_FREQUENCY[d['AssociationFeeFrequency']].to_s.to_d).to_s
      end  
    },
    appointment_phone_description: proc {|d| lookup_source(APPOINTMENT_CALL,d['AppointmentCall']) },
    # back_on_market: proc {|d|   field = 'BackOnMarketDate'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    bathrooms: 'BathsTotal',
    bathrooms_full: 'BathsFull',
    bathrooms_half: 'BathsHalf',
    beds: 'BedsTotal',
    building_stories: 'NumberOfStoriesInBuilding',
    buyer_agent_compensation: 'CompensationPaid',
    category: proc { |d| 
      field = d['PropertyType'].to_s 
      if field   
        case field
        when "COM"
          "commercial"
        when "LND"
          "lots"
        when "LSE"
          "rental"
        when "MUL"
          "multi_family"
        when "RES"
          if d['PropertySubType'].in?(['RESCON','RESTOW'])
            "townhouse_condo"
          elsif   d['PropertySubType'].in?(['RESRAN'])
            "ranch_acerage"
          else
            "single_family"
          end
        else
          ""
        end
      end
    },
    
    cdom: 'CDOM',
    city: proc { |d| lookup_source(CITY,d['City']) },
    closed_date:  proc {|d|   field = 'CloseDate'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    cooling_system_type: proc { |d| lookup_source(HEATING, d['Heating']) },
    county: 'CountyOrParish',
    disclosures: proc { |d| lookup_source(SPECIAL_NOTES, d['SpecialNotes']) },
    dom: 'DOM',
    equipment: proc { |d| lookup_source(OTHER_EQUIPMENT, d['OtherEquipment']) },
    
    exemption: 'AGExemptionYN',
    expiration_date:    proc {|d|   field = 'ExpirationDateOption'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    # expiration_date:   proc {|d|   ma =  d['EXPIREDDATE'].match(/(\d+-\d+-\d+)/).captures.try(:[],0) rescue nil; strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d'  : '%Y-%m-%d'  ; !ma.blank? ?   (Date.strptime(ma, strformat).strftime('%m/%d/%Y') rescue nil) : nil  },
    exterior_description: proc { |d| lookup_source(EXTERIOR_FEATURES,d['ExteriorFeatures']) },
    exterior_type: proc { |d| lookup_source(EXTERIOR_BUILDINGS,d['ExteriorBuildings']) },
    fence_type: proc { |d| lookup_source(FENCING,d['Fencing']) },
    fenced_yard: 'FencedYardYN',
    financing_available: proc { |d| lookup_source(FINANCING_PROPOSED,d['FinancingProposed']) },
    fireplace_number: 'FireplacesTotal',

    floor_type: proc { |d| lookup_source(FLOORING,d['Flooring']) },
    foundation_type: proc { |d| lookup_source(FOUNDATION_DETAILS,d['FoundationDetails']) },
    garage_capacity: 'ParkingSpacesGarage' ,
    heating_system:  proc { |d| lookup_source(HEATING, d['Heating']) },
    # hoa_payment: 'ASSOCFEE',
    hoa_bill_freq: proc { |d| lookup_source(ASSOCIATION_FEE_FREQUENCY,d['AssociationFeeFrequency']) },
    hoa_membership: proc { |d| lookup_source(ASSOCIATION_TYPE,d['AssociationType']) },
    
    interior_description: proc { |d| lookup_source(INTERIOR_FEATURES,d['InteriorFeatures']) },
    # key_map_page: 'MAPPAGE',
    land_size: proc { |d|  lookup_source(LOTSIZE,d['LotSize']) || d['LotSizeDimensions'] },  #Lookup
    previous_list_price: 'LastListPrice',  
    latitude:  proc { |d| (d['Latitude'] && d['Latitude'].to_i != 0) ? d['Latitude'].to_d : nil },
    
    legal_description: 'TaxLegalDescription',
    list_price: 'ListPrice',
    list_price_psf: 'RATIO_CurrentPrice_By_SQFT',
    listing_agent_email: 'ListAgentEmail',
    listing_agent_id: 'ListAgentMLSID',
    listing_agent_name: 'ListAgentFullName',
    listing_agent_phone: 'CellPhone',

    listing_broker_id: 'ListOfficeMLSID',
    listing_broker_name: 'ListOfficeName',
    
    listing_date:  proc {|d|   field = 'ListingContractDate'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    # listing_date:  proc {|d|   ma =  d['LISTDATE'].match(/(\d+-\d+-\d+)/).captures.try(:[],0) rescue nil; strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d'  : '%Y-%m-%d'  ; !ma.blank? ?   (Date.strptime(ma, strformat).strftime('%m/%d/%Y') rescue nil) : nil  },
    loan_amortized_years: 'Loan1Years',
    loan_amount_interest_rate: 'Loan1InterestRate',
    loan_amount_new: 'Loan2Amount',
    loan_term: 'SOLDTERMS',
    lock_box_code: 'KeyboxNumber',
    longitude:  proc { |d|  (d['Longitude'] && d['Longitude'].to_i != 0) ? d['Longitude'].to_d : nil },
    lot: 'LotNumber',
    
    lot_number: 'LotNumber',
    lot_description: proc { |d| 
      lf = lookup_source(LOT_FEATURES,d['LotFeatures']).split(", ")   
      tp = lookup_source(TOPOGRAPHY,d['Topography']).split(", ")  
      
      cld = []
      
      cld = cld + lf if !lf.nil?
      cld = cld + tp if !tp.nil?
      if cld.length > 0
        str_lot_desc = cld.reject(&:blank?).join(", ")
        str_lot_desc
      end
      
    }, # fix
    lot_square_feet: proc { |d| 
      if !d['SqFtTotal'].blank?
        d['SqFtTotal'].to_s.to_d.round().to_s
      else
        if d['LotSizeUnits'].blank?
          (d['LotSizeArea'].to_s.to_d * 43560).round.to_s
        else
          case d['LotSizeUnits']
          when "ACRE"
            (d['LotSizeArea'].to_s.to_d * 43560).round.to_s
          when "SQM"
            (d['LotSizeArea'].to_s.to_d * 10.7639).round.to_s
          when "SQFT"
            (d['LotSizeArea'].to_s.to_d).round.to_s 
          else
            (d['LotSizeArea'].to_s.to_d).round.to_s
          end
        end
      end
       
    } ,     #SQFTLot
    
    
    maintenance_fee: 'AssociationFee',
    mineral_rights: 'SurfaceRights',
    mls_area_key: 'MLSAreaMajor',
    mls_area_name: 'MLSAreaMajor',
    mls_elementary_school: 'ElementarySchoolName',
    mls_high_school: 'HighSchoolName',
    mls_middle_school: 'MiddleSchoolName',
    mls_number: 'MLSNumber',
    mls_sub_area: proc { |d| 
      sp = d['MLSAreaMinor'].split(".").try(:[],1) 
      if sp.blank?
        "0"
      else
        sp
      end
    },
    mls_sub_area_name: proc { |d|
      MLS_SUB_AREA_NAME[d['MLSAreaMinor'].to_sym] 
    },
    mls_record_modified:  proc {|d|  field = 'MatrixModifiedDT'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/  rescue nil   ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/  rescue nil   ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?   (Delayed::Worker.logger.debug "~~Date: #{ma} #{ta}";"#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end   },
    mortgagee: 'MortgageCompany',
    mud_district: 'MunicipalUtilityDistrictYN',
    multi_owner_operating: 'GrossAnnualExpenses',
    multi_gross_income:  'GrossAnnualIncome',
    muti_gross_operating_income: proc { |d| d['GrossAnnualIncome'].to_s.to_d - d['GrossAnnualExpenses'].to_s.to_d },
    # mls_area_name: lookup(DATA_MLS_AREA, 'AREA'),
    
    new_construction_description:  proc {|d| lookup_source(YEAR_BUILT_DETAILS,d['YearBuiltDetails']) },
    new_construction: proc { |d| lookup_source(YEAR_BUILT_DETAILS_BOOL,d["YearBuiltDetails"]) },
    number_of_photos: 'PhotoCount',
    number_of_units: 'NumberOfUnits',
    occupancy_rate: 'OccupancyRate',
    office_phone: 'Phone',
    original_list_price: 'OriginalListPrice',
    owner_name: 'OwnerName',
    owner_occupied: proc { |d|
      if !d['Occupancy'].blank? && (d['Occupancy'] == 'OWNER')
        'Y'
      elsif  !d['Occupancy'].blank? && (d['Occupancy'].to_s.in?(['TENANT','VACANT']))
        'N'
      else
        ''
      end
    },
    owner_phone:  'OtherPhone',
    pending_date:  proc {|d|   field = 'PendingDate'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    pool_private: 'PoolYN',
    public_remarks: 'PublicRemarks',
    realtor_remarks: 'PrivateRemarks',
    
    # rental_class: proc {|d| if d['PROPSUBTYPE'] == 'LA' then "multi_family" elsif d['PROPSUBTYPE'] == 'M' then "mobile_home"    elsif d['PROPSUBTYPE'] == 'LT' then "townhouse_condo" elsif d['PROPSUBTYPE'] == 'LX' then "multi_family"  elsif d['PROPSUBTYPE'] == 'LP' then "multi_family" elsif d['PROPSUBTYPE'] == 'LH' then "single_family"  elsif d['PROPSUBTYPE'] == 'LD' then "multi_family"  end    },
    rental_terms: 'LeaseTerms',
    rental_class: proc { |d|
      case d["PropertySubType"]
      when "LSEAPA"
        'multi_family'
      when "LSEDUP"
        'single_family'
      when "LSEFOU"
        'multi_family'
      when "LSEHOU"
        'single_family'
      when "LSEMOB"
        'mobile_home'
      when "LSETOW"
        'townhouse_condo'
      when "LSETRI"
        'multi_family'
      when "MUL5PL"
        'multi_family'
      when "MULDUP"
        'single_family'
      when "MULFOU"
        'multi_family'
      when "MULTRI"
        'multi_family'
      when "MUSIUN"
        'multi_family'
      else
        nil
      end
    },
    rental_type:  proc { |d|
      case d["PropertySubType"]
      when "LSEAPA"
        'apartment'
      when "LSEDUP"
        'duplex'
      when "LSEFOU"
        'fourplex'
      when "LSEHOU"
        'freestanding'
      when "LSETRI"
        'triplex'
      when "MUL5PL"
        'apartment'
      when "MULDUP"
        'duplex'
      when "MULFOU"
        'fourplex'
      when "MULTRI"
        'triplex'
      else
        nil
      end
    },
    road_surface: proc { |d| lookup_source(ROAD_FRONTAGE, d['RoadFrontage'] ) },
    # rental_sale_mls_number: 'LEASEMLSNUM',
    roof_type:  'Roof',
    rooms: 'NumberOfLivingAreas',
    school_district: 'SchoolDistrict',
    school_district_name: 'SchoolDistrict',
    seller_paid_to_buyer_closing_costs: 'SellerContributions',
    selling_agent_email: 'SellingAgentEmail',
    selling_agent_id: 'SellingAgentMLSID',
    selling_agent_name: 'SellingAgentFullName',
    selling_agent_phone: 'SellingAgentDirectWorkPhone',
    showing_instructions: 'ShowingInstructions',
    siding_type: 'ConstructionMaterialWalls',
    single_family_type: 'PropertySubType',
    
    # showing_instructions: 'SHOWINSTR',
    single_family_type: proc {|d| 
      if d['SpecialNotes'] == "HISTOR"
        "historic"   
      else
        if d['PropertyType'] == "RES"
          case d['PropertySubType']
            when "RESDUP"
              "duplex"
            when "RESFAM"
              "freestanding"
            else
              nil
          end   
        end
      end
    },

    # siding_type: 'CONSTRUCTION',
    sold_price: 'ClosePrice',
    sold_lease_price: 'ClosePrice',
    square_feet: 'SqFtTotal',
    status: 'Status',
    state: proc { |d| 'TX' },
    stories: 'NumberOfStories',
    street_direction: 'StreetDirPrefix',
    street_name: proc { |d|  [d['StreetName'], d['StreetSuffix']].compact.join(" ")  } ,
    street_number: 'StreetNumber',
    style_of_building: proc { |d| lookup_source(ARCHITECTURAL_STYLE,d['ArchitecturalStyle'])  },
    
    subdivision: 'SubdivisionName',
    tax_amount: 'UnexemptTaxes',
    tax_id: 'ParcelNumber',
    termination_date: proc {|d|   field = 'OffMarketDate'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    unit_number: 'UnitNumber',
    waterfront: proc {|d|
        ret = nil
        Delayed::Worker.logger.debug ">>waterfront: }"
        unless d['LotFeatures'].blank?
          d['LotFeatures'].split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              
              ret = case item
                when 'CREEK','LAKFRO','LFMB','RIVFRO'
                "Y"
                when 'ACREAG','ADTOGR','AIRSTR','CAMAMA','CORNER','CUDESA','CULTIV','GOCOLO','GREENB','HEATRE','HORPER','INTLOT','IRREGU','LANDSC','LEASEH','LRBAGR','NOBAGR','PARCUL','PARVIE','PASTUR','SOMTRE','SUBDIV','TAPO','TAXIWA','UNDIVI'
                "N"                
                else
                nil
              end
            rescue
              ret = nil
            end
            break if !ret.nil? && ret != "N"

          }
        end
        Delayed::Worker.logger.debug ">>ret: #{ret}"
        # !ret.blank? ? ret : "0"
        !ret.nil? ? ret : nil
    },
    golf_course: proc {|d|
        ret = nil
        Delayed::Worker.logger.debug ">>golf_course: }"
        unless d['LotFeatures'].blank?
          d['LotFeatures'].split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              
              ret = case item
                when 'GOCOLO'
                "Y"
                when 'ACREAG','ADTOGR','AIRSTR','CAMAMA','CORNER','CREEK','CUDESA','CULTIV','GREENB','HEATRE','HORPER','INTLOT','IRREGU','LAKFRO','LANDSC','LEASEH','LFMB','LRBAGR','NOBAGR','PARCUL','PARVIE','PASTUR','RIVFRO','SOMTRE','SUBDIV','TAPO','TAXIWA','UNDIVI'
                "N"                
                else
                nil
              end
            rescue
              ret = nil
            end
            break if !ret.nil? && ret != "N"

          }
        end
        Delayed::Worker.logger.debug ">>ret: #{ret}"
        # !ret.blank? ? ret : "0"
        !ret.nil? ? ret : nil
    },
    gated_community: proc {|d|
        ret = nil
        Delayed::Worker.logger.debug ">>gated_community: }"
        unless d['CommunityFeatures'].blank?
          d['CommunityFeatures'].split(",").each { |item|
            begin
              Delayed::Worker.logger.debug ">>item: #{item}"
              
              ret = case item
                when 'GATENT'
                "Y"
                when 'BOARAM','CAMPGR','CLUHOU','COMDOC','COMELE','COMPOO','COSPSY','GATENT','GREENB','GUAENT','HANGAR','HORFAC','JPBP','LANSTR','LAUNDR','MARINA','OTHER','PARK','PERFEN','PLAYGR','PRLAPO','PUBHAN','RACBAL','RVPAR','SAUNA','SPA','TENNIS'
                "N"                
                else
                nil
              end
            rescue
              ret = nil
            end
            break if !ret.nil? && ret != "N"

          }
        end
        Delayed::Worker.logger.debug ">>ret: #{ret}"
        # !ret.blank? ? ret : "0"
        !ret.nil? ? ret : nil
    },
    withdrawn_date: proc {|d|   field = 'WithdrawnDate'; pattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+\/\d+\/\d+)/ :  /(\d+-\d+-\d+)/ rescue nil    ;  subpattern = d[field].match(/(\d+\/\d+\/\d+)/) ?  /(\d+:\d+)/ :  /(\d+:\d+:\d+)/ rescue nil    ; ma =  d[field].match(pattern).captures.try(:[],0) rescue nil; ta =  d[field].match(subpattern).captures.try(:[],0) rescue nil; if d[field] && d[field].match(/(\d+\/\d+\/\d+)/) then strformat = /\/(\d{4})$/.try(:match,ma).nil? ? '%m/%d/%y'  : '%m/%d/%Y'  ; !ma.blank? ?     ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M') rescue nil) : nil   else strformat = /(^\d{4})/.try(:match,ma).nil? ? '%y-%m-%d %H:%M:%S'  : '%Y-%m-%d %H:%M:%S'  ; !ma.blank? ?  ( "#{ma} #{ta}".to_datetime.strftime('%m/%d/%Y %H:%M:%S') rescue nil) : nil end  },
    year_built: 'YearBuilt',
    zip_4: 'PostalCodePlus4',
    zip_code: 'PostalCode'
    
    
  }



  def login
    page = agent.post 'https://members.har.com/login/dispLogin.cfm?app=engine', USERNAME: market.mls_login, PWD: market.mls_password, USERTYPE: 'M', SUBMIT: 'Log In'
    page = agent.get 'http://members.har.com/mopx/goHARMLS.cfm'
    current_url == "http://www.harmls.com/default.aspx?showStartPage=true"
  end

  def logout
    agent.get 'http://members.har.com/mopx/doLogout.cfm'
    true
  end

  def rets_login
    
    @client = Rets::Client.new(:login_url => "http://matrixrets.ntreis.net/rets/login.ashx",:username => market.mls_login,:password => market.mls_password)
    Delayed::Worker.logger.debug "RETS client #{@client}"
    Delayed::Worker.logger.debug "Dallas RETS logged in"
    true
  end

  def rets_logout
    @client.logout
    Delayed::Worker.logger.debug "logged out"
    true
  end

  def mls_url_for(property)
    " http://dfw.ws.marketlinx.com/prop/en-us/dfw/#{property.mls_number}" unless property.mls_number.blank?
  end

  def tax_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDetail.aspx?Key=HAR&Pin=#{property.tax_id}" unless property.tax_id.blank?
  end

  def deed_url_for(property)
    "https://reidi.propertyinfo.com/HAR/Reports/rptDeed.aspx?txtPin=#{property.tax_id}%20001" unless property.tax_id.blank?
  end

  def grab(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_rets()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end

  def grab_by_interval(minutes=nil)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS by interval ~~~~~~~~"
      parse_rets_by_interval(minutes) 
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  def sync_rets(days)
    if rets_login
      Delayed::Worker.logger.debug "~~~~~~~ Syncing RETS ~~~~~~~~"
      parse_rets_test()
      rets_logout
    else
      self.errors[:login] = "could not login"
    end
    notify_listeners
  end
  
  
  def add_dom(days)
    
    super
    
    x = 0
    Delayed::Worker.logger.debug "#{market.name} specific update ##{market.id}"
    x = self.market.properties.only_active.update_all ["cdom = ifnull(cdom,ifnull(dom,0))  + DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL ifnull(dom,0) DAY) - 1) )
                          , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          ["(listing_date is not null) and ((cdom is null) or (dom is null)) "]
                          
    Delayed::Worker.logger.debug "rows affected: #{x}"
    self.rows_affected = (self.rows_affected.nil? ? 0 : self.rows_affected ) + x
    
    notify_listeners
  end

  def pull_rets(property)

      begin
        
       

        begin
          puts "--------- pulling single family ---------"
          results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(StreetNumber=#{property.street_number}),(StreetName=#{property.street_direction}),(PostalCode=#{property.zip_code})",:limit => 60000)
          
          puts "--------- importing single family ---------"
          
          if results && results.count > 0
            row = results.first 
            puts "##row: #{row}"
            result = push_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            
            selected_fields = result.select{|x| ![:street_number,:street_direction,:street_name].include?(x)}
              
            property.assign_attributes(selected_fields)
          end
          
        rescue  Exception => e
           Delayed::Worker.logger.debug "No record found #{e.message}"
        end

          

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

  protected

  private

    def parse_raw(data, filename)

      begin
        CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|

          # row['PropType'] ||= filename.try(:slice, -7, 3)
          save_property(row, DATA_MAPPING, DATA_ENUMS)
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "error #{e.message}"
      end
    end

    def parse_geometry(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        # row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_geometry(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

     def parse_rets_test()
      
        # timenow = (Time.now - 510.minutes).strftime("2014-05-27T09:00:00")
        timenow = (Time.now - 510.minutes).strftime("%Y-%m-%dT%H:%M:%S")
        timerange = "2014-01-01-2014-06-11"
        sd = Date.parse('2014-09-15')
        ed = Date.parse('2014-12-14')
        Delayed::Worker.logger.debug "--------- pulling single family ---------"
        ed.downto(sd) { |date|
          
          Delayed::Worker.logger.debug "---------  #{date} ---------"
          date1 = date.strftime("%Y-%m-%dT00:00:00")
          date2 = date.strftime("%Y-%m-%dT06:00:00")
          date3 = date.strftime("%Y-%m-%dT12:00:00")
          date4 = date.strftime("%Y-%m-%dT18:00:00")
          tomorrow = date + 1.day
          date5 = tomorrow.strftime("%Y-%m-%dT00:00:00")
          dates = []
          dates = [date1,date2,date3,date4,date5]
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=ACTV,BOMK,CANC,CLSD,CTG,EXP,NEW,PEND,PCHG,RACT,RNTD,TEMP,AUCT),(RECORDMODDATE=#{timenow}),(TYP=AT,DE,MH,MU,MF)",:limit => 60000)
          
          # results = client.find(:all,:search_type => "Property",:class => "ResidentialProperty",:query => "(ST=CLSD,RNTD),(RECORDMODDATE=#{timerange}),(TYP=AT,DE,MH,MU,MF)",:limit => 100000)
          
          
          for i in 0..3
            Delayed::Worker.logger.debug "---------  #{dates[i]} - #{dates[i+1]} ---------"  
            
            begin  
             
              recdate = "#{dates[i]}-#{dates[i+1]}"
              Delayed::Worker.logger.debug "--------- pulling single family ---------"
             
              # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,AC,AKO,AOC,C,I,P,T,W,WS,X),(MatrixModifiedDT=#{recdate}),(PropertyType=LND,LSE,RES)",:limit => 60000)
              results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=L,S),(MatrixModifiedDT=#{recdate}),(PropertyType=LND,LSE,RES)",:limit => 60000)
              
             
              Delayed::Worker.logger.debug "--------- importing single family ---------"
      #
      
              results.each do |row|
                 Delayed::Worker.logger.debug "##row: #{row}"
                 import_waterfront(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
               end
            
            rescue Exception => e
              Delayed::Worker.logger.debug "No record found #{e.message}"
            end
            
            
          
          end
          
         
        }

       

    end
    
    def parse_rets()
      # mlslist  = '13031347,13031491,13030245,13030836,13031497,13031510,13030975,13031513,13031519,13031522,13031516,13031530,13031539,13031540,13031543,13031544,13031542,13031521,13031160,13030655,13031581,13031591,13031596,13031598,13031599,13031563,13031396,12158062,13031625,13031628,13031157,13031633,13031461,13031651,13031663,13029236,13031670,13031672,13031676,13031707,13031718,13030615,13031675,13029937,13029148,13031436,13031766,12072765,13031376,13031784,13030542,13031797,13030451,13031138,13031717,13031761,13031815,13031817,13031820,13031828,13031851,13028183,13028464,13030160,13031859,12107972,13029048,11733615,13031881,13031406,13031874,13031880,13028137,13031758,13031930,13031935,13031939,13031949,13031953,13031955,12178032,12174109,13031984,13031992,13031836,13031996,12092115,13031982,13032024,13032027,12192478,13025831,13032010,13032058,12167251,13032122,13031289,13032110,13032005,13032198,13032191,13032183,13032228,13032229,13032243,12186035,13032239,13032251,13032145,13026243,13032014,13032283,13032290,13032292,13032303,13032311,13032313,13032318,13032322,13031509,13031852,13032334,13032335,13032341,13032356,13032362,13031911,13032030,12171204,13031727,13032180,13031943,13032274,13032384,13032190,13032404,13032407,13032415,13032406,13032419,13032421,13032430,13032431,13024847,13032465,13032466,13015802,13030262,13032492,13032507,13032515,13032199,13032516,13032527,12179387,13031270,13032555,13032480,13031843,13031979,13032532,13032569,13032577,13032599,13032605,13032612,12109245,13028926,13032633,13030244,13030940,13032556,13032626,13032635,13032012,13032615,13032673,13032679,13032695,13032698,13032305,13032658,13031941,13032713,13032753,13032769,13032774,13032652,13032799,13032819,11702953,13031665,13032503,13032731,13032820,13032837,13031669,13032319,12102239,12102246,13032685,13032848,13032209,13032874,13032895,13031520,13032924,13032940,13032941,13032950,13032963,13032686,13032962,13032278,13030425,13032985,13017564,13032842,13031951,13033017,13033020,13032811,13033026,13030064,12133865,13033043,13031964,13033081,13033088,13022050,13033054,13033124,13032968,13031434,13033085,13033146,12137325,13033179,13033180,13033182,13033187,12185378,13032796,13033098,13033208,13033212,13033213,12194029,13032007,12054046,13032955,13033235,13033253,12088942,13033275,13032838,13033287,13033291,13033249,13033303,13033094,13033299,13033142,13032238,13033342,13023754,13033370,13033393,13033290,13033415,13033427,13018853,13031607,13033441,13033447,13030916,13033442,13033454,13030952,13033036,13033186,13033475,13033480,13033485,13015895,13033037,13033507,13033503,13033483,13033527,13033529,13032967,13033561,13033558,13033533,13033572,13033582,13033587,13032252,13031645,13033261,13033600,12133531,12154440,12154442,13033376,13033401,13033621,13033623,13033625,12179764,12179772,13033642,13033689,13033123,13033715,13033566,13033745,13033753,13033649,13033771,13033784,13031465,13033797,12156163,12129665,13016806,13033820,13033833,13033627,13033746,13033830,13033872,13033875,13033545,13033788,13033893,13033880,13033901,13033907,13033912,13031399,13033682,13033724,13033924,13032582,13018685,13033691,13033911,13033954,12166688,13032602,13033971,13033279,13033624,13033845,13034018,13031890,13033975,13033635,13034061,13034052,13034088,13034120,13033489,13034139,13034102,13033769,13034171,13034006,13034198,13033859,13034206,13034204,13031417,13034135,13034221,13034229,13034084,13034243,13033593,13034256,13015384,13031923,13033994,13032786,13032306,13032161,13032372,13024196,13031108,13033943,13034316,13032509,13034199,13033846,13034353,13034178,13034364,13034375,13032146,13032851,12125368,12165458,13034405,13031252,13032727,13033176,11994390,13033670,13031217,13029255,13033311,13034239,13032282,13033834,13034485,13034492,12156172,13034073,13034541,13033383,13034563,13034535,13034570,13034571,13034534,13034607,13034639,13034649,13034276,13034354,13034491,13034675,13034192,12152978,13034538,13031863,13034664,13034732,13034768,13034795,13034226,13033940,13034838,13033700,13032939,13034859,13032580,13034865,13034864,13030328,13034377,13025524,13033550,13034916,13034904,13034040,13034946,13034974,13031041,13017417,13034990,13034989,13035012,13035017,13034888,13035026,13035069,12194315,13035092,13034329,13035215,13035223,12174373,13032351,13035255,13035209,13033423,13035307,12149922,13031975,12125233,13035500,12168594,13035570,12086004,12086024,13035622,13018872,13035724,11729073,12183338,12159555,13035821,13017489,13035833,12155603,13035866,13035880,13035899,13035904,13035925,13035936,13035937,11996488,12072191,13035822,13035970,13017472,13035977,12097814,13035992,13035994,13036051,13036059,13036067,13036118,12144568,13034929,13036197,12065510,12065516,12133311'
      begin
        
        divisor = 720
        offset = 1440
        time_zone_diff = 840
                 
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1
 
        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling single family ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(StreetNumber=1323),(StreetName=Jasmine),(PostalCode=75077)",:limit => 60000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,AC,AKO,AOC,C,I,L,P,S,T,W,WS,X),(MatrixModifiedDT=#{date_range}),(PropertyType=RES,LND,LSE)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=12098959)",:limit => 60000)
            
            Delayed::Worker.logger.debug "--------- importing single family ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end

    def parse_retrieve()
      mlslist  = '11878895,11878891,11904193,12011067,12015630,12015683,12045989,12064728,12064729,12064731,12112131,12140657,12140647,12141352,12184632,12185871,12188049,12190645,12191596,12191607,12196281,12196548,12197235,12197477,10442592,11468518,11629816,11684005,11804159,11804962,11825393,11837658,11842854,11899660,11913501,11913313,11916887,11927836,11943198,11952130,11966205,11986879,11989354,11989320,11989261,11990677,12008090,12007845,12011077,12012140,12013641,12017985,12025134,12030018,12045982,12054227,12061718,12068139,12069458,12077653,12078058,12084086,12089352,12089376,12090639,12092153,12092205,12093741,12094812,12096682,12102426,12107096,12107304,12110693,12111686,12112380,12113688,12116664,12116702,12117018,12121392,12122995,12126168,12127651,12127661,12127673,12128384,12128400,12133661,12133915,12134864,12140689,12142965,12143462,12143903,12144325,12151302,12157867,12162315,12165375,12165414,12166876,12171982,12175639,12177899,12179566,12180469,12181084,12184093,12184631,12184905,12186169,12188179,12191166,12192597,12193984,12194137,12194597,12194619,12194844,12195133,12195173,12195194,12195204,12195418,12195441,12195620,12196590,12196654,12196694,12196942,12197016,12197155,12197184,12197273,13000334,13000342,13000364,13000447,13000487,13000498,13000588,13000631,13000666,13000683,13000689,13000710,13000719,13000785,13000797,13000856,13000863,13000864,13000874,13000875,13000883,13000924,13000940,13000948,13000956,13000985,13001014,13001022,13001033,13001048,13001074,13001100,13001142,13001146,13001162,13001219,10673143,11557633,11557648,11557539,11557564,11557668,11557686,11557718,11557723,11557750,11557753,11907046,11906512,11906515,11915014,11915017,11915100,11915101,12011920,12068470,12097744,12144833,12166035,12186182,12194973,13001229,13001250,13001277,10279201,11027897,11419983,11420080,11420098,11420113,11420122,11420146,11456864,11643295,11643224,11643800,11648913,11660875,11660877,11660873,11671044,11670883,11675511,11706646,11733342,11744200,11757017,11792378,11807438,11807491,11818083,11822392,11822564,11822374,11822335,11822389,11822861,11829164,11857930,11873657,11873205,11880103,11886611,11906235,11907666,11923870,11927598,11963414,11976329,11978107,11978065,11978095,11978002,11993247,11993225,11998106,12001598,12006812,12009993,12010333,12013767,12013407,12015205,12019255,12031055,12035118,12035887,12035902,12037073,12036449,12037078,12040259,12048854,12056050,12055489,12056198,12059204,12063934,12065520,12066845,12067121,12067128,12067146,12067395,12068543,12068653,12068657,12068661,12068664,12071354,12072825,12073647,12078203,12080587,12081116,12084236,12084238,12086306,12086752,12087878,12091733,12094604,12095939,12106690,12108265,12109179,12114020,12114302,12116480,12121661,12121436,12123972,12124652,12124833,12127813,12128579,12129111,12129136,12133961,12134678,12138447,12141132,12141391,12141551,12141558,12141655,12145844,12145851,12148075,12149104,12149237,12149248,12149489,12150165,12150766,12161595,12162404,12163145,12163876,12168707,12175648,12178381,12180946,12184054,12185205,12187202,12187204,12187206,12188980,12189411,12189498,12190007,12191084,12191767,12192846,12192903,12194736,12194861,12196098,12196106,12196120,12196125,12196353,12197254,12197264,13000560,13000643,13000787,13000795,13000995,13001019,13001049,13001217,13001284,13001576,13001588,13001608,13001632,13001650,13001678,13001681,13001688,13001701,13001733,13001741,13001779,13001794,13001798,13001820,13001823,13001840,13001867,13001872,13001879,13001884,13001888,13001915,13001941,13001970,13001972,13001985,13002004,13002005,13002013,13002026,13002031,13002040,13002051,13002067,13002093,13002101,13002121,13002163,13002175,13002196,13002202,11731481,12000569,12010187,12013264,12013894,12039147,12128917,12143464,12163545,12173813,13002214,13002237,13002247,13002248,13002253,13002312,37849,37850,37845,11202563,11370441,11404957,11405009,11448737,11448744,11448901,11448904,11448907,11448908,11448910,11448686,11448922,11448926,11448917,11547639,11599923,11630285,11682611,11742537,11767598,11791032,11827402,11858883,11872404,11886025,11902781,11904997,11911312,11932250,11937308,11937519,11936961,11943349,11945728,11956895,11995218,12012623,12012292,12037676,12060998,12066445,12068498,12069529,12071762,12072106,12072125,12075218,12076025,12076793,12079832,12086096,12091162,12091222,12092199,12093275,12093915,12098245,12101293,12107053,12110026,12117526,12127047,12127735,12128416,12132035,12132041,12132045,12132452,12132472,12132484,12132498,12132605,12132613,12132620,12132630,12133295,12133303,12133313,12133327,12133332,12134406,12140572,12144388,12146178,12148153,12151191,12153104,12156528,12159883,12161563,12176480,12179303,12180413,12185350,12187217,12190181,12191717,12192945,12193086,12193668,12193957,12194427,12194555,12194676,12196315,13000469,13001392,13001696,13001853,13002162,13002456,13002555,13002590,13002595,13002599,13002626,13002656,13002661,13002669,13002671,13002683,13002702,13002714,13002730,13002733,13002739,13002755,13002816,13002834,13002867,13002895,13002946,13002963,13002975,13002984,13003024,13003036,13003064,13003065,11711142,11813129,11813123,12014070,12014080,12014168,12015165,12015172,12015180,12015181,12015186,12015108,12015116,12015243,12015249,12015255,12015267,12015122,12015265,12015268,12015271,12015273,12015276,12015279,12015280,12015252,12052744,12063528,12092451,12117352,12135829,12140718,12141115,12167545,12185518,13001092,13001205,13002454,13003092,13003134,13003148,13003149,13003162,11921343,11933371,12016075,12025426,12025436,12091609,12091638,12125776,12128331,12137309,12146768,12165243,12169316,12191964,13002096,13002744,13003103,13003324,13003337,13003349,13003370,13003427,13003431,13003433,13003435,13003437,13003440,13003444,11247123,12038356,12177900,13003074,13003460,13003464,13003469,13003476,13003477,13003481,13003482,13003506,13003515,13003526,13003532,11961973,12108387,12165253,12175977,12175979,12175980,12184085,12188981,12196620,13000691,13001520,13002651,13003552,13003560,13003563,11528904,11960125,11984919,12074499,12092755,13003649,13003665,13003692,13003705,35736,11278791,11278753,11283857,11489328,11725634,11825890,11843701,11857705,11857714,11904661,11904665,11908588,11913418,11923188,11950617,11974831,11985204,11985852,11994190,11996055,11996104,11996979,12000242,12000255,12000263,12000269,12000692,12000744,12000764,12000785,12000797,12000833,12000906,12000915,12000920,12000928,12004642,12012993,12016071,12036040,12046248,12061038,12068590,12096556,12096884,12098477,12098959,12107926,12115751,12119131,12120349,12122450,12125072,12125113,12131708,12144587,12146348,12146352,12151751,12154669,12155856,12159178,12160740,12160889,12165932,12168980,12171852,12176616,12178074,12180448,12183770,12186610,12195769,12196770,12196845,12197134,12197185,13001860,13003535,13003743,13003744,13003745,13003747,13003758,13003796,13003801,13003806,13003868,13003890,13003893,13004030,13004051,13004100,13004120,13004136,13004137,13004152,13004165,13004171,13004173,13004189,13004221,12093608,12120160,12120207,12131885,12188162,13002387,13004290,13004292,13004294,13004297,13004300,13004312,13004323,11629617,11816801,11826844,11841635,11923104,11945237,11945236,11950828,11959306,12001308,12006434,12025826,12059019,12081977,12082781,12093793,12095223,12095230,12095251,12095259,12095265,12095270,12095273,12095274,12095282,12095281,12095288,12095284,12095285,12095308,12095311,12095312,12095313,12095319,12095322,12095324,12095326,12095328,12095330,12095334,12095339,12095343,12095336,12095342,12095346,12095344,12095347,12095349,12095350,12096045,12096903,12097176,12099615,12114573,12119245,12121676,12121638,12127156,12150781,12156643,12159002,12171235,12175473,12183126,12183236,12185818,12186925,12190587,12191213,12191229,12191242,12191251,12191258,12192448,12193079,12193723,12194768,12196858,13001500,13004435,13004469,13004553,13004590,13004599,13004600,13004604,39492,11655268,11807248,11873231,11873224,11892632,11893902,11923008,11968351,11968341,11968329,11976763,12012365,12017081,12025872,12025783,12040553,12049700,12068523,12070685,12070689,12073648,12073720,12076827,12080045,12095133,12095856,12111242,12120441,12125003,12125017,12134456,12134670,12136823,12141376,12146166,12146994,12153437,12155235,12165280,12173231,12176293,12176323,12176332,12176343,12176350,12178863,12193140,12196292,13003130,13004335,13004588,13004629,13004638,13004653,13004677,13004690,13004716,13004717,13004723,13004731,13004735,13004738,13004748,13004755,13004765,13004782,13004815,13004881,13004912,13004919,11108517,11825931,11911891,11918735,12083700,12116900,12117116,12144291,13004360,13004953,13004973,13004985,13004994,13004995,13005005,13005015,11622360,11622386,11622459,11622464,11622374,11622380,11622659,11622660,11622663,11622666,11622668,11622735,11622745,11622746,11622751,11622754,11622760,11912394,11912392,11912817,11973526,11974427,11974444,11974449,11974482,11974507,11974514,11974529,11974550,11974584,11974591,11974601,11974614,11974637,11981045,12014233,12014429,12018174,12039037,12056703,12061368,12097866,12101628,12110318,12112632,12112759,12114979,12133190,12135521,12151929,12162132,12169383,12175595,12186007,12188719,12191922,12194294,12194438,12196883,13005165,13005198,13005239,13005269,10403383,10402768,11111224,11111172,11111186,11111156,11111332,11113495,11113512,11111162,11111210,11111137,11111303,11113580,11111341,11111218,11111256,11111268,11111246,11111282,11111346,11111324,11111352,11111313,11111360,11111239,11114407,11114422,11114578,11114610,11114628,11114597,11204541,11296421,11497930,11503722,11611186,11728865,11767931,11897671,11905883,11906113,11974606,12003856,12016088,12021376,12029678,12029689,12033777,12031930,12033776,12033775,12033774,12033533,12036932,12045727,12053263,12053269,12053274,12053278,12053266,12073216,12073965,12077348,12079724,12079854,12083100,12083517,12084280,12084575,12093774,12096437,12097037,12100648,12104801,12104866,12106851,12115636,12115837,12136961,12142523,12142609,12145715,12150153,12153699,12157860,12157879,12158033,12158798,12158806,12161771,12162225,12162332,12166689,12166964,12167871,12167879,12168431,12174477,12180262,12185560,12187467,12188863,12189972,12189975,12194369,13001935,13005141,13005339,13005390,13005421,13005445,13005446,13005466,13005476,13005478,13005485,13005487,13005498,13005513,13005528,13005564,13005581,13005588,13005687,11764497,12070067,12094477,12096379,12096445,12110339,12111979,12151233,12165026,12166365,12174456,12178126,12188321,12192549,13005697,13005752,13005821,13005829,13005835,13005841,10480079,11767935,11767939,11767907,11903235,11914103,12016703,12019620,12098265,12150919,12154058,12159990,12171373,12174421,12183282,12188862,12189875,12192693,12194712,12196680,13005860,13005886,13005904,13005924,13005926,13005928,13005940,13005943,13005962,13005979,13005988,13006004,13006009,13006028,13006032,13006084,11365564,11521350,11521371,11521380,11708848,11787287,11809604,11834946,11970743,11981771,12023564,12027034,12076254,12083196,12084122,12089568,12089652,12104109,12125534,12145591,12151543,12162149,12173206,12176367,12179428,12184050,13003414,13003981,13005640,13005665,13005961,13006132,13006143,13006151,13006168,13006172,13006177,13006196,13006197,13006200,13006201,13006202,13006207,13006212,13006214,13006216,13006229,13006234,13006237,13006244,13006245,13006248,13006250,13006287,13006289,13006307,13006327,13006341,13006344,11725413,11740013,12013105,12022179,12084606,12096517,12196215,13006466,13006484,13006491,13006494,13006497,13006514,13006519,13006520,13006525,13006536,13006542,13006548,13006549,13006550,13006553,13006554,13006556,13006557,39420,11565947,11572283,11741356,11772388,11774914,11801950,11904844,11921169,11958379,12014725,12061918,12061995,12075195,12079426,12080593,12093231,12095459,12097840,12144913,12146234,12156697,12181231,12181821,12185551,12185561,12193842,13002295,13005427,13006593,13006612,13006617,13006673,13006687,13006729,13006730,13006740,13006753,13006754,13006759,13006788,13006789,13006803,11305476,12040629,12041413,12042772,12050802,12074028,12084486,12088650,12092590,12100934,12122290,12160614,12168651,12186108,12186109,12186938,12196584,13002957,13005373,13006349,13006772,13006818,13006827,13006831,13006832,13006846,13006847,13006851,13006863,13006870,13006877,13006881,13006882,13006898,13006901,13006914,13006940,13006952,13006955,13006972,13006986,13006991,13007005,13007041,13007053,13007060,13007068,11153036,11826657,11889867,11961109,12122657,12155299,12193063,12195653,13006842,13007151,13007160,13007189,13007213,13007214,13007216,13007240,11753157,11920631,12002179,12050700,12088765,12106785,12123171,12157797,12170090,12187763,13003339,13007304,13007325,13007331,13007332,13007334,11900546,11942497,12063495,12096020,12131683,12167112,12178403,12181886,12181975,12186277,12195810,13002924,13007346,13007349,13007380,13006964,12189760,11792367,11792464,11793976,11886642,11903865,11956653,11974232,11995950,12002522,12004443,12007816,12013694,12023188,12036682,12052831,12059198,12074844,12078359,12092097,12108518,12125680,12136224,12140840,12144887,12155942,12155962,12170175,12173271,12179427,12189188,12194605,12194908,12194910,12195278,12196566,12196582,12196591,12196603,12196606,12196644,12196661,12196711,10596977,10596525,11299360,11457939,11722095,11776017,11844942,11905119,11920028,11920044,11922016,11922035,11920722,11920729,11939710,11939379,11942534,11945656,11946335,12007965,12011306,12012667,12012622,12025817,12032136,12039923,12064067,12064684,12067913,12079101,12087161,12088787,12091085,12093030,12098198,12100877,12101635,12103204,12103210,12103214,12118326,12127159,12128503,12133288,12137256,12141653,12141737,12146602,12148220,12148224,12154416,12155760,12165657,12168352,12168371,12169152,12170044,12171012,12172879,12174406,12179587,12186691,12187441,12188389,12190950,12190992,12191036,12191072,12193569,12194714,12195711,12196108,12196628,12196700,12196729,12196731,12196746,12196757,12196772,12196801,12196802,12196829,12196834,12196893,12196891,12196913,12196938,12196939,12196944,12196957,12196965,12196974,12196985,12197004,12196989,12196990,12197025,12197034,12197079,12197056,12197046,12197052,12197081,12197122,12197165,12197197,12197365,12197256,12197267,12197368,12197374,12197379,12197390,12197401,12197410,12197446,12197367,12197312,12197354,11984913,12075518,11771737,11771598,12067614,12159765,12193049,11705439,11748895,11934628,11934625,11936820,11936852,11936722,12038996,12047118,12062422,12174385,12177460,12195912,12195923,12195929,12195939,12195949,12195951,12196118,12196130,12196142,12196154,12196165,12196181,12196189,12196212,12196253,12196256,12196260,12196268,12196270,12196357,12196359,12196401,12196423,11530295,11822747,12004167,12013058,12013074,12013087,12013093,12013099,12013107,12013152,12013185,12013200,12013538,12013545,12013553,12013585,12013800,12013809,12013822,12013833,12013847,12013854,12013926,12013935,12013944,12014218,12014236,12014262,12014269,12014322,12014334,12013862,12013918,12014228,12014249,12015432,12017902,12017922,12090794,12189756,12192341,12196468,12196525,12161475,11976048,11976049,11976051,12002659,12002644,12002652,12002580,12003616,12068276,12078606,12110722,12177573,12189993,12194503,12194511,12194513,12194518,12194523,11274435,12008858,12009536,12078995,12092087,12094128,12103244,12103265,12103266,12136420,12192658,12196079,12196081,12196122,12054730,12054733,12189044,12189055,12195031,12195651,12196291,11644751,12195027,11642231,11826667,12007079,12009012,12009014,12009018,12009024,12009028,12009030,12009041,12009044,12009045,12009048,12009051,12009054,12009055,12009034,12009039,12009057,12009062,12009064,12009066,12009069,12008566,12008577,12008810,12008837,12008845,12008921,12008927,12008931,12008939,12009000,12009007,12008528,12008513,12008418,12010345,12154047,12155674,12157722,12188325,12188601,12190294,12191093,12194731,12194834,12195020,12195077,12195086,12195109,12195114,12195121,12195206,12195244,12195332,11627549,11826574,11967209,12011305,12051275,12073416,12073430,12073435,12073443,12076656,12091116,12091145,12091153,12103523,12106106,12114774,12114940,12119412,12129188,12130394,12146802,12182046,12183137,12184585,12188742,12192572,12193231,12193619,12193829,12193917,12193945,12194886,12195189,12195224,12195410,12195434,12195443,12195446,12195466,12195478,12195508,12195558,12195657,12195654,12195625,12195635,12195675,12195718,12195743,12195739,12195742,12195785,12195808,12193771,12195892,12195913,12192064,12193498,12194252,11254887,11722910,11824018,12104713,12110920,12116540,12124645,12154489,11901360,11966680,12018604,12093044,12116889,12187758,12194875,13007440,13007454,12155653,12193711,13006811,13006892,13007499,11833738,12152726,12152731,12185212,12034648,11914469,12040111,12046633,12048667,12048724,12104412,12104442,12139349,12139916,12161622,12161629,10610609,11068085,11068113,11832862,11995142,12016110,12050146,12097671,12098359,12098369,12104856,12128727,12136161,12179164,12179987,13005217,13007651,13007689,13007700,11365730,11367224,11483157,11482964,11493569,11750240,11815888,11815911,11815901,11978677,12108761,12115027,12124435,12129279,12131395,12145545,12169237,12176937,12181837,12183512,12183510,12184123,13007423,13007665,13007729,13007736,13007754,13007757,13007810,11281779,11480914,11481279,11493595,11493612,11637951,11637972,11637992,11771137,11893977,11894000,11964374,12041624,12080218,12127890,12130602,12130612,12169051,12174742,13007831,13007849,13007862,13007893,13007922,13007941,13007961,10031665,11238382,11638010,11638077,11741785,11957649,12080140,12081170,12104100,12110114,12136978,12139515,12157684,12161150,12180706,12182688,12193798,13007943,13007970,13007994,13007995,13008001,13008003,13008005,13008012,13008013,13008102,13008113,11898420,12017460,12052664,12076078,12096715,12127275,12143336,12172647,12185574,13008119,13008164,13008186,13008234,12157316,11838311,11900362,11924232,12032676,12046544,12076296,12100063,12151576,12153340,12153435,12153501,12153497,12153529,12153522,12182043,13008260,13008275,13008331,11166645,11306158,11534101,11771286,11838340,11894581,11917337,12014630,12017510,12017551,12020423,12020442,12023129,12119523,12143003,12157675,12170204,12170213,12182399,12187130,12192259,13007580,13008349,13008352,13008355,13008418,13008420,13008443,11905023,12046643,12071700,12072817,12092868,12093759,12095290,12095335,12113065,12130542,12132884,12145578,12150182,12160618,12168367,12175327,12179148,13007054,13008393,13008538,13008561,13008614,13008630,11761407,12008019,12015743,12025608,12026884,12045091,12069564,12107126,12123825,12171278,12188056,13008573,13008640,13008677,13008681,13008682,13008683,13008693,13008709,13008734,13008759,13008781,13008820,11832333,11914907,11937716,12017906,12017962,12020058,12023700,12041243,12085025,12086444,12088362,12102935,12175433,13008842,13008863,13008869,13008875,13008930,13008943,13008945,13008948,13008955,13008963,13008987,13008989,11890609,12091815,12100248,12129347,12132157,12138033,12160702,12166759,12167040,13008016,13009037,13009098,11767753,11772417,11791838,11791855,11792143,11838914,11926315,11948333,12016160,12017927,12028500,12034939,12061173,12068646,12093195,12094521,12111105,12142824,12188858,12190015,13008652,13009149,13009173,13009186,13009188,13009205,13009217,13009224,13009230,11679471,12183817,13009284,13009294,12047694,12098356,12100712,12115935,12149831,12178189,13009140,13009242,13009287,13009320,13009326,12059119,12153731,12194961,13007692,13009365,13009396,13009398,13009468,13009485,13009495,13009505,11199596,11602195,11602193,11778882,11938929,12017283,12063444,12088342,13008339,13009085,13009323,13009446,13009476,13009518,13009527,13009532,13009539,13009542,13009546,13009547,13009566,11206019,11519306,11655545,11706279,11706270,11818988,11825194,11863494,11882614,11908264,12016270,12018247,12022879,12035101,12085164,12119963,12132931,12135995,13008139,13009672,13009676,13009707,13009728,13009730,13009737,13009741,11672671,11786905,11851438,11942678,11944841,11991629,11991628,12074836,12087712,12090750,12127894,12142554,12154320,12185404,12188678,12191460,13009019,13009732,13009821,13009830,13009837,11880619,11926192,11942147,12021599,12047875,12090714,12090743,12090941,12147503,12150308,12162877,12162875,12176316,13008035,13009926,13009953,11181066,11181072,11181074,11181075,11181088,11181113,11181120,11181122,11181130,11181133,11181135,11181138,11181141,11181811,11342174,11865961,11936503,11945515,11959761,12064144,12069186,12103208,12110359,12121657,12124423,12127854,12138002,12159187,12168071,12176910,12181081,12186320,13009413,13009847,13009907,13009920,13010037,13010049,13010102,13010108,13010128,13010131,13010139,13010173,13010179,11866157,11889384,11907279,11909987,11960374,12022894,12025899,12086598,12104435,12150932,12150938,13010189,13010214,13010258,13010270,13010322,13010352,13010381,11669386,11797818,11797817,11857706,11885399,11914407,11957029,12018821,12038677,12068388,12070727,12112083,12113888,12123998,12129894,12132339,12137237,12163708,12165568,13009172,12046434,12051715,11417101,11471917,11880655,11915303,11932866,11945663,12017897,12034418,12061800,12061808,12061814,12061819,12061821,12061827,12074835,12082862,12084169,12084254,12125873,12154945,12181158,12189026,13010475,13010542,13010553,13010587,13010591,10448761,10852899,10852922,11466453,11531881,11563609,11563611,11666641,11706619,11969023,11977622,12020097,12024193,12028103,12032084,12051756,12059206,12094682,12106083,12123561,12133124,12133458,12137420,12152357,12170043,12177757,12181020,12183205,12184759,12188105,12193657,13009387,13009437,13010646,13010724,13010732,13010746,13010757,13010888,13008881,12017872,12058036,12111627,13011036,11892122,12013067,12018899,12019969,12019993,12059065,12059075,12059314,12059303,12084121,12145988,12159327,12189346,12194265,13008780,13011112,13011152,13011164,13011171,11708164,11708333,11930489,11962277,12051057,12054487,12063238,12090878,12093844,12100208,12100327,12100394,12100409,12102154,12103001,12118803,12118806,12118808,12118815,12118818,12118819,12118820,12118823,12118826,12118827,12118830,12118832,12118833,12118834,12118836,12118840,12118843,12118846,12118850,12118854,12118748,12118752,12118661,12118754,12118756,12118758,12118760,12118776,12118777,12118779,12118782,12118763,12118764,12118788,12118791,12118793,12118795,12118800,12133146,12139808,13009804,13010321,13011188,13011221,13011222,13011225,13011226,13011256,13011263,13011267,13011305,12094155,11699377,12087304,12126070,12145292,12185566,13011347,13011350,11847749,11857518,11930640,12079711,12124301,13011351,13011355,13011370,11289684,11299119,11657014,11823413,11844083,11844078,11853126,11866877,11883205,11895596,11903108,11928810,11928764,11937758,11946769,11950838,11950851,12001002,12018877,12028594,12043240,12068486,12086220,12088902,12088955,12089896,12089899,12090518,12095713,12102718,12103360,12108919,12109041,12109048,12109540,12109894,12109908,12110002,12110041,12110083,12113265,12122446,12140415,12153106,12153123,12153191,12174319,13011406,13011441,13011443,12192121,10573724,10573744,10573892,10573918,10573546,10573561,10573697,10573629,10573782,10573876,10573834,10573852,10574405,10574424,10574439,10573930,10574483,10574499,10574619,10574452,10574467,11953334,11953384,11953514,11960405,11972336,12022219,12022227,12043498,12079045,12096947,12112010,12112935,12117582,12155010,12161836,13007290,13011476,13010945,12040103,12099539,12099544,13011245,13011511,13011542,13011553,11776300,11827428,11903420,11981391,12009231,12093034,12102316,12102387,12102453,12102501,12118336,12134426,12179661,12179781,12179787,12179798,12179805,13004948,13011592,13011603,13011630,12057916,12113556,12150794,12188257,13011669,11483312,11602521,11920948,11940678,11940693,11940581,11992630,12011447,12019929,12019899,12019935,12019946,12019987,12019970,12023273,12027609,12034698,12034704,12034715,12034730,12034747,12035224,12034781,12034797,12036672,12036496,12087814,12100364,12104961,12106713,12110136,12142560,12151178,12154569,12157767,12159963,12161372,12161559,12165248,12166883,12168105,12173036,12181593,12181613,12192328,13009967,13009984,13011679,13011692,13011719,13011741,13011744,13011751,13011759,13011762,13011852,13011861,13011867,13011872,13011875,12145026,11802278,11825702,11843893,12021304,12021334,12023275,12030613,12124051,12126151,12153427,12153727,12164841,12173022,12173166,12173213,12173254,12173298,12173340,12173549,12174398,12175438,12179744,12179925,12179929,12179934,12179939,12180045,12180049,12180054,12180056,12180057,12180060,12180062,12180069,12181162,12192086,12193058,13009346,13011146,13011447,13011768,13011928,13012048,13012083,13012091,13012093,13012153,11655588,11811477,11958041,11961409,11999742,12004442,12016708,12023150,12049617,12049641,12049648,12064410,12067933,12075206,12118414,12119271,12152809,12157971,12162928,12177542,12177544,12194661,12194752,13002146,13008954,13012023,13012263,12024239,12025748,12025749,12025750,12025751,12025752,12025694,12078483,12111526,12118856,12118858,12118772,12126568,12126660,12126704,12126737,12159506,12163689,13010970,13012351,11184103,11188154,11188114,11188206,11275504,11751412,11751393,11751376,11751636,11911414,11919881,11935562,12082373,12082440,12132710,12145711,12154751,12190690,12190723,13012132,13012427,13012435,13012437,13012451,13012476,13012483,13012484,13012490,13012494,13012501,13012570,12024026,12037595,12088173,12143156,12165135,12176393,12189085,12192731,12192749,12192848,13008312,13009238,13011016,13011241,13012581,13012614,13012640,13012657,13012682,13012723,13012726,13012736,13012749,13012750,13012751,11778484,11873329,12066674,12084922,12103011,12140922,12162512,13010923,13011455,13012737,13012778,13012787,13012850,13012909,13012923,13012924,11986304,12043196,12090012,12101476,12101480,12114957,12155792,12188080,13012694,13012913,13012981,13012996,11366529,11774915,11894316,11966729,11975041,11975035,12020534,12020553,12020569,12020419,12020138,12023379,12086530,12104060,12143774,12143838,12157163,12157174,12157181,12157193,12159891,12161967,12173284,13013199,10960877,11657302,11818074,11818376,11820321,11919932,12046595,12050214,12065785,12067994,12119708,12143526,12148667,12161352,13012136,13013129,13013201,13013222,13013314,13013316,13013328,13013332,13013337,10960841,11275947,11879438,12033790,12053072,12110122,12118715,12141814,12148871,12164593,12177870,12189963,13006528,13013341,13013350,13013360,13013375,13013385,13013417,13013490,13013499,13013507,13013513,13013538,10718848,10908038,10960849,10960853,10960875,10960886,11135520,11170640,11170341,11172563,11172604,11172619,11172654,11172680,11172686,11172714,11172599,11172613,11186021,11186032,11186035,11186058,11185337,11188073,11188210,11187072,11275891,11318717,11973457,12013934,12071103,12137713,12144216,12170913,12184935,12188016,13013395,13013491,13013556,13013569,13013580,13013582,13013621,13013623,13013658,13013660,13013671,10940690,11073064,11160097,11833288,11994788,12021133,12027416,12101278,13011618,13012092,13013516,13013651,13013706,13013710,13013712,13013788,13013790,13013809,13013848,13013866,11346286,11346340,11926571,11926595,11927173,11927214,11927241,11927331,11927540,11927586,11927514,11927345,11927562,12115107,12162857,12163518,12174629,12176124,12180967,12185736,13012902,13013602,13013912,13013929,13013950,13013963,11135429,11836357,11858238,11928487,11928525,11928550,11928539,11928914,11928930,11929081,11929094,12021259,12089746,12131427,12153754,12156862,12163581,12164769,12184376,13011734,13012700,13013625,13014095,13014117,13014144,13014149,10256416,11168513,11420281,11649315,11759885,11789107,11789070,11789050,11810454,11810458,11810641,11810311,11997941,12006550,12032669,12044743,12052362,12069586,12084229,12085315,12087273,12119405,12138463,12141925,12153470,12153505,12154033,12156436,12156666,12156676,12156684,12156691,12163440,12179103,12187386,12187455,12188702,12188724,13007585,13012866,13013388,13013964,13014145,13014179,13014195,13014213,13014266,13014299,13014318,13014342,12043128,12079666,12089235,12146342,13010080,13013688,13014182,13014218,13014354,13014390,13014444,13014450,13014452,13014469,13014472,13014484,11908934,12111918,12140964,13010329,13014076,13014087,13014178,13014378,13014426,13014493,13014515,13014537,13014557,13014612,13014622,13014627,13014654,13014669,13014683,13014686,11799074,13007401,13014682,13014730,11283169,11346346,12062723,12114428,13014771,13014806,11958416,12113391,12140148,12158976,12159193,12191731,13014804,13014828,13014840,13014843,12048625,12154084,12166273,12175605,12194935,13012431,13014844,13014845,13014848,13014853,13014871,13014884,13014886,13014898,13014940,13014949,10694889,10862191,11170252,13014664,13014970,13014994,13015003,12021848,12189659,13014971,13015067,13015073,13015077,10754065,11446680,11473607,12009177,12026203,12031834,12031869,12043615,12049518,12056330,12086743,12108899,12134139,12146819,12166050,12176953,13011389,13015039,13015106,13015108,13015155,13015180,13015189,11723131,11750729,11750296,11750273,11843610,11851358,11893163,11907543,12019843,12037164,12044477,12083534,12093187,12095951,12105444,12110077,12110102,12123515,12133648,12133990,12134124,12150129,12151935,12154102,12164685,12177128,12191434,13015136,13015208,13015210,13015215,13015222,13015233,13015234,13015244,13015255,13015268,13015280,13015282,13015287,13015299,13015302,13015303,13015309,13015316,13015326,13015328,11869901,11872755,11913182,11971709,12024513,12112589,12113985,12128665,12128875,12142794,12172100,12175819,12180292,13015352,13015353,13015405,13015411,13015426,13015429,10254066,11193721,11631647,11796294,11965999,11991601,12075630,12076729,12124364,12147090,12163222,12181228,12192441,13015403,13015504,13015559,13015587,13015614,11886504,11939256,12002210,12055530,12100158,12100164,12100176,12100203,12101624,12101631,12108784,12108798,12145921,12147437,12170036,13015369,13015613,13015624,13015639,13015678,13015733,11211713,11529251,11623812,11665485,11679362,11698131,11779748,11779753,11875050,12050602,12054314,12066696,12075393,12079822,12081971,12102984,12120569,12142832,12144425,12156378,12162215,12172666,12172882,12189211,12190290,13015784,13015785,13015786,13015804,13015807,13015818,13015864,13015876,11564223,11840466,11999002,11999007,12037402,12037403,12107613,12116568,12124670,12124671,12137245,12137720,12138148,12138149,12138150,12138151,12138152,12138153,12138155,12138156,12158423,12169372,12189209,12189991,13015873,13015882,13015890,13015898,13015901,13015909,13015913,13015917,13015926,13015947,13015992,13016001,13016019,11582839,12083776,12086690,12088385,12170826,12179401,13004652,13013208,13016025,13016042,13016060,13016067,13016082,13016090,13016092,13016170,12017076,12053081,12073458,12078936,12118728,12128495,12147183,12161790,12170654,12172328,12179474,12188589,13013410,13015050,13016063,13016192,13016196,13016211,13016216,13016226,13016227,13016238,13016242,13016281,13016284,13016285,13016309,13016317,13016321,11612992,12071805,12113017,12128147,12189118,13016278,13016351,13016357,13016446,13016465,10474187,10918934,11141390,11815812,11928321,11953906,11999714,12026047,12044217,12065506,12082250,12097438,12113377,12123597,12133059,12167749,12187724,12188052,13016534,13016549,13016559,13016586,11732488,11819540,11925631,11945617,11953299,11953295,12028106,12144186,12149986,12169421,12169564,12169580,13015708,13016458,13016555,13016556,13016565,13016609,13016630,13016644,13016653,13016672,10309199,11794693,11794701,11794796,11794032,11795463,11795475,11795620,11795684,11795568,11795589,11802190,11835965,11877017,11913153,12024330,12064844,12076820,12082047,12101386,12103081,12109166,12116926,12116928,12117746,12117750,12117753,12117764,12117765,12181407,12185400,12194144,13016472,13016532,13016721,13016727,13016746,13016757,13016767,13016785,13016788,13016800,13016821,11516161,11622205,11718845,11741181,11891150,12027915,12029798,12067525,12083733,12086471,12120672,12120680,12128983,12181746,12187452,13006321,13015232,13016658,13016661,13016713,13016775,13016842,13016845,13016856,13016875,13016886,13016890,13016959,11921864,12027861,12060267,12095149,12095151,12095153,12095156,12095158,12095164,12095165,12095166,12120683,12120688,12120698,12131785,13016287,13016979,13016992,13017020,13017036,13017039,13017048,13017106,13017154,13017156,13017162,13017170,10934087,11247523,11679682,11814583,11889900,11992443,12026511,12074235,12082313,12082811,12083823,12092664,12111420,12111497,12120043,12120704,12120710,12120712,12120717,12120720,12120723,12120727,12137367,12138482,12164266,12175313,12175444,12181438,12181825,12189010,13016240,13017058,13017197,13017210,13017230,13017272,11359496,11675342,11674869,11674881,11675328,11674855,1191600'
      begin
          
          begin
            Delayed::Worker.logger.debug "--------- pulling single family ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(StreetNumber=1323),(StreetName=Jasmine),(PostalCode=75077)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,AC,AKO,AOC,C,I,L,P,S,T,W,WS,X),(MatrixModifiedDT=#{date_range}),(PropertyType=RES,LND,LSE)",:limit => 60000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=#{mlslist})",:limit => 60000)
            
            Delayed::Worker.logger.debug "--------- importing single family ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_retrieve(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
    # def parse_rets()
      # mlslist  = '13076527'
      # begin
#         
#        
          # begin
            # Delayed::Worker.logger.debug "--------- pulling single family ---------"
            # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(StreetNumber=1323),(StreetName=Jasmine),(PostalCode=75077)",:limit => 60000)
            # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,AC,AKO,AOC,C,I,L,P,S,T,W,WS,X),(MatrixModifiedDT=#{date_range}),(PropertyType=RES,LND,LSE)",:limit => 60000)
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=#{mlslist})",:limit => 60000)
#             
            # Delayed::Worker.logger.debug "--------- importing single family ---------"
            # results.each do |row|
               # Delayed::Worker.logger.debug "##row: #{row}"
               # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
# 
#          
# 
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end
    
    def parse_rets_by_interval(minutes=nil)

      begin
        
        divisor = 720
        offset = minutes ? minutes : 0
        time_zone_diff = 840
                
        timenow =  !Rails.env.development? ? Time.now  : Time.now - time_zone_diff.minutes
        new_offset = offset
        step = (offset / divisor) + 1

        step.downto(1) do |increment|
          
          start_time = (timenow - new_offset.minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          
          if (new_offset - divisor) >= 0
            end_time =  (timenow - (new_offset - divisor).minutes).strftime("%Y-%m-%dT%H:%M:%S") 
          else
             end_time =  (timenow).strftime("%Y-%m-%dT%H:%M:%S") 
          end
          
          Delayed::Worker.logger.debug "from: #{start_time} to: #{end_time}"
          date_range = "#{start_time}-#{end_time}"

          begin
            Delayed::Worker.logger.debug "--------- pulling single family ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(StreetNumber=1323),(StreetName=Jasmine),(PostalCode=75077)",:limit => 60000)
            results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,AC,AKO,AOC,C,I,L,P,S,T,W,WS,X),(MatrixModifiedDT=#{date_range}),(PropertyType=RES,LND,LSE)",:limit => 60000)
            Delayed::Worker.logger.debug "--------- importing single family ---------"
            results.each do |row|
               Delayed::Worker.logger.debug "##row: #{row}"
               save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            end
          rescue  Exception => e
             Delayed::Worker.logger.debug "No record found #{e.message}"
          end

          new_offset = new_offset - divisor

        end

      rescue Exception => e
        Delayed::Worker.logger.debug "No record found #{e.message}"
      end

    end
    
    
     

     # def parse_rets()
      # begin
        # # 
        # if Rails.env.development?
          # offset = 1950
        # else
          # offset = 840
        # end
#         
        # timenow = (Time.now - offset.minutes).strftime("%Y-%m-%dT%H:%M:%S")
        # ids = "12076092,12080006,11986227,12038378,12059400,12072946,12086941,12061271,12072481,12074593,12087759,12088597,12094156,12027697,12098594,12098952,12047607,12072808,12073700,12076496,12084191,12087225,12096503,12099116,12100490,12100664,12100977,12102013,12102050,12061319,12063111,12038056,12047861,12031689,12075596,12066314,12091690,12023329,12040752,12041694,12048834,12056894,11978893,11954543,11781376,11922211,11931227,11880898,11792676,11800325,11810222,11900846,11929197,11949472,11957646,11967317,12089560,12051286,12070487,12109467,12110097,12111024,12111307,12112042,12116548,12116579,12117112,12117310,12118738,12120131,12121728,12121752,12122032,12122705,12122959,12123668,12124347,12124053,12124042,12126204,12126666,12126540,12126993,12127698,12127720,12127734,12127760,12127774,12128595,12129025,12129351,12129371,12129400,12129523,12129716,12130517,12131140,12131416,12131996,12133043,12133279,12133301,12133549,12133933,12133924,12134325,12134494,12134553,12134609,12134570,12135238,12135366,12135537,12135924,12136152,12136468,12136478,12136484,12136983,12137264,12137646,12137839,12137813,12137915,12137909,12138080,12138111,12138249,12138664,12138944,12140094,12140606,12141228,12141779,12141989,12142106,12142273,12142300,12142381,12142356,12142698,12142746,12143038,12143092,12143097,12143275,12143414,12143631,12143610,12143650,12143993,12144234,12144502,12144879,12145087,12145122,12145435,12145529,12145807,12145809,12146425,12141278,12147053,12147196,12147860,12147907,12148007,12148056,12148317,12148694,12149033,12149049,12149174,12149482,12149512,12149996,12150168,12150202,12150295,12150253,12150343,12150423,12150753,12150788,12150929,12151320,12151908,12152004,12152078,12152132,12152240,12152352,12152464,12152556,12144660,12152766,12152732,12152788,12153023,12153145,12153216,12153220,12153262,12153264,12153426,12153637,12153659,12153665,12153721,12153741,12153812,12153902,12153882,12153958,12154109,12154178,12154255,12154504,12154577,12154692,12154705,12155020,12154994,12154944,12155036,12155113,12155162,12155166,12155167,12155175,12155226,12155357,12155451,12155562,12155601,12155636,12155841,12155955,12156287,12154381,12156251,12156346,12156542,12156846,12156704,12156867,12157057,12157180,12157229,12157318,12157304,12157374,12157444,12157466,12157519,12157493,12157601,12157677,12157809,12157768,12157790,12157831,12157828,12157838,12158034,12158056,12158084,12158049,12158185,12158332,12158380,12158550,12158733,12158725,12158669,12158777,12158804,12158821,12158858,12159092,12159073,12158946,12159174,12159227,12159211,12159265,12159309,12159333,12159391,12159398,12159462,12159536,12159614,12159713,12159782,12159825,12159865,12159957,12159896,12159895,12159983,12160019,12160093,12160077,12160153,12160353,12160417,12160439,12160842,12160782,12160843,12160878,12160950,12160966,12161354,12161024,12161207,12161414,12161746,12161797,12161909,12161907,12161947,12162141,12162000,12161990,12162257,12162391,12162337,12162594,12162467,12162462,12162603,12162320,12162864,12162681,12162911,12162783,12162886,12162692,12163162,12163120,12163027,12163213,12163308,12163684,12163446,12163680,12163444,12163592,12163850,12163744,12163884,12164033,12164026,12164714,12164597,12164198,12164453,12164156,12164497,12164763,12164391,12164731,12164510,12164705,12164335,12164512,12164680,12164405,12164380,12164506,12164796,12164872,12165270,12165114,12165167,12164932,12164870,12165080,12165388,12165452,12165569,12165701,12165602,12165455,12165694,12165685,12165445,12165977,12165872,12166018,12166187,12166191,12166179,12166483,12166401,12166404,12166582,12166626,12166794,12166899,12166645,12166644,12166714,12166665,12166738,12166885,12166887,12166889,12166918,12167006,12167269,12167227,12167206,12167129,12167123,12167283,12167422,12167476,12167521,12167441,12167675,12167683,12167617,12167833,12167595,12167742,12167763,12167709,12167806,12167572,12168150,12167980,12168173,12168012,12167990,12167997,12167948,12168172,12168169,12168087,12168181,12168109,12168193,12168361,12168399,12168440,12168412,12168330,12168345,12168427,12168401,12168907,12168798,12168817,12168696,12168813,12169657,12169361,12169033,12169592,12169030,12169529,12169310,12169163,12169975,12170100,12170130,12169896,12169547,12170061,12169356,12170117,12169596,12169509,12169101,12170134,12170032,12169459,12169435,12169024,12169458,12169703,12169897,12169651,12170334,12170317,12170276,12170269,12170280,12170513,12170477,12170489,12170671,12170419,12170584,12170700,12170612,12170839,12170779,12170783,12170937,12170888,12170909,12170992,12171029,12171044,12171054,12171131,12171312,12171159,12171427,12171396,12171372,12171381,12171343,12171146,12171322,12171189,12171409,12171222,12171468,12171548,12171407,12171540,12171578,12171775,12171580,12171732,12171634,12171671,12171670,12171573,12171709,12171789,12171816,12171817,12171863,12172150,12172292,12172019,12172413,12172205,12172155,12172048,12172364,12172207,12172240,12171972,12172322,12172198,12172199,12172113,12172378,12172471,12172466,12172596,12172650,12172571,12172602,12172580,12172528,12172501,12172469,12172440,12172603,12172686,12172732,12172765,12172781,12173796,12174215,12173329,12173155,12172979,12173515,12174083,12173125,12173389,12174011,12174009,12173856,12173597,12173568,12173869,12173488,12172988,12173810,12173727,12173111,12173096,12173063,12173903,12174299,12172922,12174279,12174292,12174253,12172890,12174186,12173056,12173041,12173704,12173032,12173290,12172960,12174308,12174318,12174313,12174344,12174346,12174347,12174436,12174427,12174486,12174489,12174490,12174679,12174868,12174641,12174836,12175038,12174663,12175027,12174954,12174898,12174591,12174617,12174802,12174618,12175154,12175176,12175138,12175265,12175246,12175277,12175288,12175362,12175629,12175369,12175706,12175620,12175583,12175356,12175644,12175697,12175677,12175360,12175516,12175849,12175801,12175790,12175760,12175725,12175770,12175803,12175805,12175886,12175884,12175956,12175964,12176002,12176059,12176052,12176032,12176067,12176106,12176114,12176119,12176139,12176161,12176243,12176194,12176217,12176286,12176306,12176458,12176392,12176319,12176473,12176457,12176471,12176521,12176477,12176489,12176505,12176509,12176528,12176530,12176526,12176548,12176575,12176626,12176647,12176714,12176697,12176768,12176764,12176809,12176780,12176853,12176829,12176840,12176793,12176926,12176925,12176967,12177209,12177092,12177335,12177390,12177513,12177410,12177423,12177487,12177502,12177534,12177536,12177550,12177617,12177563,12177620,12177649,12177710,12177724,12177744,12177879,12177808,12177897,12177912,12177949,12177933,12177820,12177983,12178048,12178068,12178094,12178124,12178201,12178172,12178143,12178247,12178224,12178254,12178263,12178476,12178391,12178462,12178412,12178397,12178409,12178444,12178457,12178525,12178607,12178615,12178640,12178642,12178695,12178715,12178672,12178619,12178765,12178797,12178884,12178850,12178834,12178875,12178918,12178979,12178997,12179026,12179045,12179064,12179106,12179120,12179083,12179121,12179092,12179084,12179123,12179165,12179168,12179184,12179204,12179093,12179336,12179351,12179398,12179410,12179437,12179562,12179509,12179482,12179524,12179653,12179659,12179674,12179672,12179699,12179803,12179791,12179815,12179830,12179867,12179850,12179869,12179896,12179948,12179973,12179961,12179994,12179971,12180006,12180033,12180071,12180081,12180161,12180166,12180074,12180103,12180200,12180205,12180194,12180261,12180234,12180478,12180323,12180443,12180428,12180476,12180497,12180557,12180562,12180556,12180572,12180577,12180601,12180607,12180626,12180639,12180659,12180654,12180663,12180634,12180631,12180610,12180916,12180825,12180759,12180784,12180744,12180789,12180949,12180778,12180669,12180787,12180668,12181328,12181340,12181298,12181338,12181337,12181309,12181301,12181311,12181307,12181300,12181356,12181350,12181214,12181445,12181226,12181296,12181354,12181349,12181547,12181025,12181518,12181065,12181633,12181003,12181035,12181064,12181000,12181554,12181674,12181163,12181017,12180966,12181120,12181572,12181654,12181718,12181713,12181698,12181726,12181919,12181834,12181790,12181917,12182025,12181873,12181757,12182032,12181948,12182035,12181856,12181830,12182072,12182165,12182145,12182123,12182134,12182155,12182601,12182293,12182882,12182418,12183040,12182403,12183038,12182430,12182912,12182751,12182877,12182494,12182584,12182551,12183008,12182543,12182722,12182236,12182870,12182534,12182900,12182654,12182634,12182411,12182815,12182830,12182999,12182951,12182932,12182694,12182735,12182451,12182520,12182825,12182880,12182784,12183002,12182283,12182531,12182248,12182941,12182384,12182791,12182713,12182483,12182271,12182759,12182987,12182284,12182319,12182649,12182898,12183011,12182217,12183097,12183182,12183170,12183139,12183172,12183101,12183098,12183104,12183198,12183214,12183256,12183249,12183223,12183342,12183327,12183357,12183421,12183377,12183400,12183507,12183526,12183531,12183537,12183435,12183564,12183572,12183773,12183730,12183626,12183631,12183698,12183588,12183612,12183815,12183674,12183798,12183788,12183772,12183624,12183755,12183792,12184024,12184046,12183951,12183943,12183921,12184035,12184012,12183940,12184052,12183925,12184023,12184021,12184062,12183932,12184018,12184000,12184014,12184017,12184101,12184088,12184136,12184120,12184119,12184133,12184140,12184156,12184145,12184252,12184251,12184352,12184615,12184590,12184321,12184567,12184513,12184640,12184405,12184422,12184562,12184490,12184558,12184290,12184598,12184358,12184281,12184527,12184446,12184364,12184500,12184621,12184429,12184336,12184363,12184390,12184330,12184679,12184713,12184776,12184770,12184785,12184846,12184807,12184737,12184836,12184664,12184771,12184693,12184732,12184646,12184965,12184963,12184939,12184972,12185149,12185083,12185320,12185174,12185025,12185161,12185122,12185047,12185187,12185190,12185076,12185289,12185345,12185114,12185203,12185296,12185318,12185319,12185183,12185511,12185413,12185428,12185516,12185429,12185468,12185366,12185406,12185360,12185557,12185414,12185460,12185637,12185591,12185649,12185697,12185701,12185735,12185742,12185775,12185880,12185795,12185822,12185810,12185853,12185854,12185882,12185914,12185983,12186046,12186011,12186298,12186221,12186264,12186148,12186037,12186028,12186163,12186133,12186211,12186233,12186260,12186304,12186287,12186325,12186354,12186378,12186388,12186400,12186446,12186462,12186472,12186520,12186537,12186538,12186548,12186541,12186572,12186580,12186620,12186643,12186752,12186710,12186664,12186728,12186524,12186603,12186731,12186682,12186692,12186775,12186748,12186793,12186823,12186638,12186858,12186734,12186886,12186850,12186867,12186866,12186890,12186933,12186981,12187006,12187161,12187147,12187138,12187108,12187125,12187082,12186926,12187099,12187149,12187058,12187047,12187077,12187134,12187142,12187160,12187180,12187265,12187212,12187205,12187232,12187284,12187253,12187234,12187293,12187330,12187299,12187316,12187513,12187529,12187552,12187534,12187555,12187574,12187576,12187501,12187601,12187621,12187407,12187666,12187690,12187363,12187677,12187691,12187711,12187716,12187755,12187368,12187779,12187839,12187837,12187826,12187848,12187847,12187866,12187897,12187903,12187970,12187925,12187884,12187861,12187990,12187875,12187964,12187919,12188068,12188093,12188108,12188091,12188119,12188182,12188169,12188195,12188174,12188170,12188186,12188223,12188209,12188212,12188221,12188240,12188306,12188287,12188284,12188296,12188346,12188318,12188320,12188330,12187478,12188329,12188351,12188395,12188412,12188391,12188427,12188430,12188439,12188452,12188469,12188497,12188456,12188473,12188515,12188529,12188540,12188532,12188537,12188543,12188569,12188593,12188578,12188639,12188619,12188658,12188667,12188680,12188689,12188700,12188777,12188751,12188728,12188752,12188946,12188921,12188923,12188916,12188794,12188816,12188915,12188868,12188895,12188897,12188962,12188986,12188988,12189012,12189029,12189032,12189042,12189049,12189050,12189071,12189083,12189077,12189142,12189105,12189115,12189169,12189163,12189166,12189183,12189175,12189170,12189227,12189228,12189231,12189225,12189302,12189313,12189311,12189319,12189323,12189249,12189362,12189273,12189279,12189295,12189277,12189425,12189418,12189412,12189461,12189551,12189520,12189574,12189504,12189521,12189560,12189599,12189608,12189696,12189709,12189723,12189739,12189769,12189788,12189838,12189860,12189865,12189902,12189911,12189917,12189928,12190021,12190017,12189839,12190053,12190025,12190043,12190079,12190076,12190106,12190142,12190330,12190231,12190157,12190204,12190183,12190242,12190316,12190327,12190373,12190384,12190390,12190412,12190414,12190431,12190527,12190544,12190564,12190561,12190583,12190585,12190618,12190631,12190641,12190706,12190700,12190787,12190604,12190788,12190467,12190517,12190677,12190559,12190621,12190691,12190676,12190722,12190782,12190827,12190894,12190826,12190796,12190944,12190951,12190933,12190996,12190985,12191110,12191020,12191067,12191221,12191154,12190999,12191286,12191142,12191121,12191168,12191141,12191192,12191196,12191207,12191218,12191260,12191289,12191294,12191303,12191441,12191383,12191385,12191386,12191376,12191078,12191352,12191012,12191086,12191112,12191419,12191188,12191220,12191283,12191316,12191292,12191320,12191340,12191338,12191357,12191392,12191442,12191432,12191437,12191462,12191727,12191728,12191729,12191754,12191751,12191753,12191755,12191759,12191803,12191686,12191820,12191818,12191823,12191833,12191877,12191893,12191905,12191909,12187417,12191942,12191924,12191943,12191946,12191958,12191984,12187444,12191989,12192000,12192008,12192046,12192053,12192045,12192055,12192044,12192088,12192107,12192161,12191531,12192094,12192173,12192163,12192177,12192235,12192251,12192277,12192296,12192164,12192312,12192254,12192308,12192345,12192309,12192343,12192346,12192334,12192504,12191581,12192499,12192456,12192371,12192365,12192402,12192382,12192438,12192484,12192461,12192481,12192509,12192515,12192535,12192512,12192556,12191668,12192569,12192576,12192566,12192601,12192656,12187365,12192670,12190490,12191582,12192700,12192695,12192633,12192753,12192743,12192762,12192767,12192735,12192744,12192736,12192792,12192794,12192809,12192818,12192814,12192834,12192845,12192841,12192866,12192890,12192965,12193000,12192964,12191548,12192810,12192811,12192813,12192816,12192838,12192875,12192897,12192906,12192978,12192919,12192915,12192936,12192938,12192950,12192961,12192971,12193003,12193037,12187449,12190475,12193106,12193294,12193242,12193237,12193157,12193261,12193143,12193228,12193259,12193274,12193306,12193280,12193326,12193286,12193305,12193307,12193328,12193304,12193335,12193358,12193374,12193436,12193422,12193416,12193381,12193429,12193438,12193440,12193452,12193444,12193458,12193450,12193464,12193472,12193479,12193480,12193484,12193485,12193499,12193546,12193543,12193562,12193558,12193580,12193584,12193761,12193642,12193646,12193673,12193688,12193684,12193731,12193708,12193751,12193740,12193769,12193759,12193762,12193789,12193817,12193788,12193806,12193799,12193824,12193835,12193847,12193844,12193871,12193848,12193890,12193865,12193870,12194071,12193919,12193948,12193961,12193973,12193993,12194037,12194085,12194179,12194165,12194188,12194204,12194225,12194262,12194261,12194229,12194228,12194207,12194249,12194264,12194242,12194209,12194195,12194219,12191503,12187369,12187390,12194301,12194312,12194329,12194334,12194362,12194363,12194366,12194278,12194401,12194422,12194428,12194408,12194416,12194414,12194423,12194431,12194436,12194494,12194456,12194469,12194473,12194485,12194506,12194512,12194533,12194557,12194560,12194571,12194586,12194628,12194632,12194854,12194796,12194671,12194663,12194718,12194771,12194769,12194806,12194883,12194872,12194922,12194931,12194932,12194933,12194938,12194966,12194957,12195051,12195059,12194997,12195064,12195088,12195089,12195069,12195183,12195164,12195176,12195180,12195221,12195214,12195283,12195257,12195220,12195229,12195242,12195316,12195378,12195339,12195382,12195362,12195371,12195234,12195481,12195203,12195409,12195225,12195415,12195390,12195444,12195449,12195452,12195499,12195514,12195498,12191477,12195544,12195585,12195534,12195572,12195569,12195589,12195595,12195605,12195750,12195689,12195690,12195776,12195644,12195536,12195622,12195741,12195724,12195725,12195777,12195887,12195814,12195821,12195818,12195832,12195888,12195975,12195933,12195946,12195999,12196022,12196043,12196107,12196066,12196086,12196095,12196136,12196159,12196174,12196176,12196202,12196269,12196242,12196193,12196255,12196272,12196310,12196336,12196345,12196343,12196355,12196372,12196380,12196374,12196389,12196402,12196416,12196418,12196435,12196439,12196438,12196454,12196466,12196493,12196499,12196511,12196533,12196535,12196540,12196543,12196568,12196571,12196610,12196646,12196725,12196682,12196677,12196763,12196722,12196740,12196718,12196730,12196754,12196755,12196768,12196832,12196715,12196781,12196804,12196797,12196841,12196847,12196937,12197042,12197175,12196914,12196865,12196887,12196886,12196920,12196970,12196949,12196952,12197023,12196940,12196954,12197010,12197006,12197038,12197043,12197067,12197060,12197063,12197077,12197083,12197086,12197118,12197228,12197246,12197210,12197237,12197230,12197239,12197304,12197370,12197357,12197269,12197331,12197298,12197319,12197332,12197352,12197362,12197383,12197404,12197416,12197413,12197407,12197411,12197392,12197465,12190464,12190495,13000394,13000442,13000461,13000475,13000544,13000563,13000602,13000626,13000638,13000700,13000711,13000722,13000754,13000816,13000825,13000844,13000869,13000955,13000968,13000981,13001013,13001107,13001132,13001148,13001178,13001183,13001230,13001260,13001275,13001307,13001309,13001313,13001334,13001341,13001435,13001452,13001486,12191508,12191572,12191584,12191583,13001128,13001326,13001336,13001385,13001516,13001530,13001541,13001578,13001639,13001667,13001714,13001723,13001762,13001770,13001806,13001811,13001887,13001921,13001933,13001988,13002001,13002010,13002038,13002058,13002059,13002095,13002142,13001424,13001704,13001786,13002211,13002217,13002223,13002233,13002266,13002277,13002299,13002303,13002305,13002320,13002396,13002398,13002411,13002416,13002421,13002427,13002433,13002438,13001647,13001828,13001830,13001839,13002346,13002462,13002525,13002568,13002586,13002606,13002608,13002613,13002622,13002631,13002708,13002776,13002790,13002792,13002798,13002826,13002829,13002854,13002873,13002932,13002953,13002960,13002987,13002996,13003009,13003048,13003059,13003082,13003083,13001102,13002448,13002904,13002936,13003135,13003154,13003158,13003165,13003206,13003217,13003218,13002520,13002534,13003068,13003264,13003269,13003274,13003283,13003309,13003321,13003357,13003366,13003373,13003402,13003416,13003447,13003362,12190506,12191664,13002688,13003548,13003591,13003636,13003638,13003640,12191525,13003676,13003687,13002111,13002758,13003299,13003424,13003766,13003769,13003770,13003774,13003784,13003786,13003787,13003859,13003869,13003873,13003918,13003943,13003960,13003976,13004007,13004034,13004050,13004150,13004159,13004170,13004190,13004205,13004213,13003710,13004202,13004274,13004320,13004348,13004372,13004397,13004381,13004403,13004451,13004470,13004474,13004510,13004514,13004603,13004613,13002008,13003830,13004263,13004273,13004541,13004615,13004640,13004703,13004788,13004795,13004826,13004864,13004914,12191580,13004935,13004975,13005001,13005011,13005035,13005068,13005083,12191622,13003777,13005168,13005170,13005200,13005342,13005351,12190444,13004175,13004333,13004654,13005363,13005389,13005392,13005432,13005624,13005629,13005633,13005661,13005662,13005596,13005712,13005730,13005742,13005808,13001728,13004607,13005927,13005977,13005984,13006054,13006078,13004593,13004701,13006034,13006106,13006137,13006158,13006264,13006272,11682649,13005584,13006412,13006434,13006437,13006538,13006565,13006704,13006716,13006723,13006726,13006769,13006784,13006365,13006813,13006959,13007029,13002743,13005247,13007139,13007241,13007285,13007310,13007320,13007324,13007342,13004686,13005261,13007387,13007409,11639289,12191667,12191603,13007493,13003903,13007526,13007632,12190471,13007869,13007882,13007898,13007908,13008047,13008111,13005207,13008209,13008211,13008277,13008315,13008387,13008401,13008569,13008608,13008609,13008728,13008221,13008949,13008964,13008995,12191612,13009065,13009081,13009256,13009307,13009471,13009530,13009552,13009646,13009783,13009842,13010014,12191586,13010374,13009781,13010489,13010509,13010922,13011641,13011835,12187500"
#         
        # ids.split(",").each do |id|
          # begin
            # Delayed::Worker.logger.debug "--------- pulling single family ---------"
            # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(MLSNumber=#{id})",:limit => 60000)
            # # results = client.find(:all,:search_type => "Property",:class => "Listing",:query => "(Status=A,AC,AKO,AOC,C,I,L,P,S,T,W,WS,X),(MatrixModifiedDT=#{timenow}+),(PropertyType=RES,LND,LSE)",:limit => 60000)
            # Delayed::Worker.logger.debug "--------- importing single family ---------"
            # results.each do |row|
               # Delayed::Worker.logger.debug "##row: #{row}"
               # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
            # end
          # rescue  Exception => e
             # Delayed::Worker.logger.debug "No record found #{e.message}"
          # end
        # end
#         
        # # begin
          # # Delayed::Worker.logger.debug "--------- pulling rental ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "LSE",:query => "(LISTSTATUS=KO,WTH,EXP,ACT,CAN,LSE,TOM,WS,PND,CON,SLD,OPT),(MODIFIED=#{timenow}+),(PROPSUBTYPE=LA,M,LT,LX,LP,LH,LD)",:limit => 60000)
          # # Delayed::Worker.logger.debug "--------- importing rental ---------"
          # # results.each do |row|
            # # Delayed::Worker.logger.debug "##row: #{row}"
            # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
          # # end
        # # rescue  Exception => e
           # # Delayed::Worker.logger.debug "No record found #{e.message}"
        # # end
#          
        # # begin
          # # Delayed::Worker.logger.debug "--------- pulling ranch acerage ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=KO,WTH,EXP,ACT,CAN,LSE,TOM,WS,PND,CON,SLD,OPT),(MODIFIED=#{timenow}+),(PROPSUBTYPE=LF,R,F)",:limit => 60000)
          # # Delayed::Worker.logger.debug "--------- importing ranch acerage ---------"
          # # results.each do |row|
             # # Delayed::Worker.logger.debug "##row: #{row}"
             # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
          # # end
        # # rescue  Exception => e
           # # Delayed::Worker.logger.debug "No record found #{e.message}"
        # # end
# #         
        # # begin
          # # Delayed::Worker.logger.debug "--------- pulling townhouse condo ---------"
          # # results = client.find(:all,:search_type => "Property",:class => "RES",:query => "(LISTSTATUS=KO,WTH,EXP,ACT,CAN,LSE,TOM,WS,PND,CON,SLD,OPT),(MODIFIED=#{timenow}+),(PROPSUBTYPE=T,C)",:limit => 60000)
          # # Delayed::Worker.logger.debug "--------- importing townhouse condo ---------"
          # # results.each do |row|
             # # Delayed::Worker.logger.debug "##row: #{row}"
             # # save_rets(row, DATA_MAPPING, DATA_ENUMS, LOOKUP_TYPE_ENUMS)
          # # end
        # # rescue  Exception => e
           # # Delayed::Worker.logger.debug "No record found #{e.message}"
        # # end
# 
      # rescue Exception => e
        # Delayed::Worker.logger.debug "No record found #{e.message}"
      # end
# 
    # end

    def parse_special_raw(data, filename)
      CSV.parse(data, :headers => true, :col_sep => "\t").each_with_index do |row, row_index|
        row['PropType'] ||= filename.try(:slice, -7, 3)
        correct_property(row, DATA_MAPPING, DATA_ENUMS)
      end
    end

    def parse_hotsheet(data, filename)
      Delayed::Worker.logger.debug "entering parsing hotsheet "
      parser = Nokogiri::HTML(data)
      tables = parser.css("table[@width='100%']")
      tables.each_with_index do |table, table_index|
        title = table.at_xpath("./tr/td")
        next if title.nil? || title.text.blank? || title.attr('colspan').blank?
        headers = html_strip(table.xpath("./tr[2]/td"), true)
        table.xpath("./tr").each_with_index do |row, row_index|
          next if row_index <= 1
          values = Hash[headers.zip(html_strip(row.xpath("./td")))]
          update_property(values, HOTSHEET_MAPPING, HOTSHEET_ENUMS)
        end
      end
    end

    def fetch_raw(days)
      tries = 0

      begin
          Delayed::Worker.logger.debug "fetching raw"
           Delayed::Worker.logger.debug " keys: #{DATA_ENUMS[:category].keys}"
          DATA_ENUMS[:category].keys.each do |category|

            statuses = []
            statuses = ['act','op','psho']

            page = agent.post 'http://www.harmls.com/thirdparty/Scripts/GetData.asp', {
              appMinorVersion: '0', appVersion: '4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2)',
              Name: '3RDPARTY', Pass: 'DHA0ATFA', Custom: 'ALL', Num: '4500', NumberOfProperties: '4500',
              Where: "(ListStatus%20IN%20('#{statuses.join('\',\'')}'))%20AND%20(DaysOnMarket%3E=1%20AND%20DaysOnMarket%3C=#{days})",
              ColumnHeader: 'Y', ItemList: '', Format: '0', Tax: 'undefined', DownloadDecodeOverride: '',
              Join: '', Tbl: category, preventDecodeList: '', D1SelectedIndex: '0', D1: 'ALL'
            }

            import(page) if page.is_a?(Mechanize::File)
          end
        rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        rescue Exception => e
          tries += 1
          Delayed::Worker.logger.debug "Trying again!" if tries <= 3
          retry if tries <= 3
          Delayed::Worker.logger.debug "No more attempt!"
          raise e
        end
    end

    def fetch_hotsheet(days)
      tries = 0
      result = nil
      begin
        Delayed::Worker.logger.debug "fetching hotsheet"
        date_format = (Date.today - days).strftime('%m/%d/%Y')
        today_format = Date.today.strftime('%m/%d/%Y')
        page = agent.post 'http://www.harmls.com/Reports/Scripts/display.asp', {
          formname: 'TodaysActivity',
          dateend: "#{today_format}%2023%3A59",
          datestart: "#{date_format}%2000%3A00",
          EndDate: "#{today_format}%2023%3A59",
          EndStatement: 'End%20of%20Activity%20Report%20for%20Today',
          heading: "#{date_format}",
          propertytype: '%22-ALL-%22',
          rpt: '%27BACK%27%2C%20%27EXP%27%2C%20%27NEW%27%2C%20%27optionp%27%2C%20%27PSHO%27%2C%20%27PEND%27%2C%20%27PRICE%27%2C%20%27CLOSD%27%2C%20%27TERM%27%2C%20%27WITH%27',
          StartDate: "#{date_format}%2000%3A00",
          today: "#{today_format}",
          todaysactivity: 'x',
          fourtharea: '',
          fiftharea: 'Display.asp',
          updatehotsheetdate: '0',
          where: '',
          Area: '-ALL-',
          Location: '-ALL-',
          proptype: 'RES%2C%20CND%2C%20LND%2C%20MUL%2C%20ACR%2C%20HIR%2C%20RNT',
          aReportDetails: 'BACK%2C%20EXP%2C%20NEW%2C%20optionp%2C%20PSHO%2C%20PEND%2C%20PRICE%2C%20CLOSD%2C%20TERM%2C%20WITH',
          narrow: 'true',
          sb: '0'
        }

        #$$Delayed::Worker.logger.debug "Is mechanize file2? #{page.is_a?(Mechanize::File)}"
        #$$Delayed::Worker.logger.debug "body2: #{page.body}"
        import(page) if page.is_a?(Mechanize::File)
      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      rescue Exception => e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      end
    end
    
    def save_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        # if raw[:owner_occupied].blank?
          # raw.delete(:owner_occupied)
        # end
        
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
              
              
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              string
            else
              if attr == :owner_occupied 
                Delayed::Worker.logger.debug "owner occupied nil #{string.to_s}"
                if string.to_s == '' || string.to_s.blank?
                  Delayed::Worker.logger.debug "blank"
                  Property.parse(attr, nil) 
                else
                  Delayed::Worker.logger.debug "value set"
                  Property.parse(attr, string)    
                end
              # elsif (attr == :cdom || attr == :dom)
                 # if string.to_s == '' || string.to_s.blank?
                  # Delayed::Worker.logger.debug "blank"
                  # Property.parse(attr, '0') 
                # else
                  # Delayed::Worker.logger.debug "value set"
                  # Property.parse(attr, string)    
                # end
                       
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import3(self.source, attrs, raw, update_only)
      
      if raw[:owner_occupied].blank?
        property.owner_occupied = nil
        property.save
      end
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end


    def save_retrieve(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        # if raw[:owner_occupied].blank?
          # raw.delete(:owner_occupied)
        # end
        
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
              
              
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              string
            else
              if attr == :owner_occupied 
                Delayed::Worker.logger.debug "owner occupied nil #{string.to_s}"
                if string.to_s == '' || string.to_s.blank?
                  Delayed::Worker.logger.debug "blank"
                  Property.parse(attr, nil) 
                else
                  Delayed::Worker.logger.debug "value set"
                  Property.parse(attr, string)    
                end
              # elsif (attr == :cdom || attr == :dom)
                 # if string.to_s == '' || string.to_s.blank?
                  # Delayed::Worker.logger.debug "blank"
                  # Property.parse(attr, '0') 
                # else
                  # Delayed::Worker.logger.debug "value set"
                  # Property.parse(attr, string)    
                # end
                       
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import5(self.source, attrs, raw, update_only)
      
      
      # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
      self.errors[current_filename] ||= {}
      Delayed::Worker.logger.debug "~~ before insert id ~~"  
      if property.nil? || property.is_a?(String) 
        self.errors[current_filename][info] = property || 'No match' unless update_only
      else
        self.results[current_filename] ||= 0
        self.results[current_filename] += 1
        Delayed::Worker.logger.debug "~~ inserting ID  #{property.id} ~~"
        self.ids << property.id
        unless property.errors.empty?
          self.errors[current_filename][info] = property.errors.map { |attr, message| "#{attr} [#{attrs[attr]}](#{raw[attr]}) #{message}" }
        end
      end
    end
    
    
    
    def push_rets(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        # if raw[:owner_occupied].blank?
          # raw.delete(:owner_occupied)
        # end
        
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
              
              
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              string
            else
              if attr == :owner_occupied 
                Delayed::Worker.logger.debug "owner occupied nil #{string.to_s}"
                if string.to_s == '' || string.to_s.blank?
                  Delayed::Worker.logger.debug "blank"
                  Property.parse(attr, nil) 
                else
                  Delayed::Worker.logger.debug "value set"
                  Property.parse(attr, string)    
                end
              # elsif (attr == :cdom || attr == :dom)
                 # if string.to_s == '' || string.to_s.blank?
                  # Delayed::Worker.logger.debug "blank"
                  # Property.parse(attr, '0') 
                # else
                  # Delayed::Worker.logger.debug "value set"
                  # Property.parse(attr, string)    
                # end
                       
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
            end
            # if string.try(:downcase).in?(missing_values)
              # Delayed::Worker.logger.debug "~~ string attr: #{attr}  value: #{string}" if attr == :dom
              # if attr == :dom || attr == :cdom
                # "0"
              # else
                # nil
              # end 
            # elsif enums.has_key?(attr)
              # Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              # Provider.lookup(enums[attr], attr).call(raw)
            # else
              # Delayed::Worker.logger.debug "Attribute property parse #{attr}"
              # Property.parse(attr, string)
            # end
          result
        end
      
      end
      
    end
    
    def import_waterfront(original, mapping=nil, enums={}, lookup_types={}, categories={}, update_only=false)
      # exectute mappings and retrieve raw data
      if mapping
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ mapping success"  #$$
        # original = original.to_hash.with_case_insensitive_access
        Delayed::Worker.logger.debug "MLSNUM: #{original}"  
        raw = mapping.select{|a,m| !m.nil?}.inject({}) do |result, (attr, method)|
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ provider looking up #{attr}"
          method = Provider.lookup_or(*method) if method.is_a?(Array)
           Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value"
           
          new_value = method.is_a?(Proc) ? method.call(original) : original[method]
          
          Delayed::Worker.logger.debug "zZZZZzzzzZZZ new value missing"
          result[attr] = new_value.missing? ? nil : new_value
          result
        end
        # if raw[:owner_occupied].blank?
          # raw.delete(:owner_occupied)
        # end
        
        Delayed::Worker.logger.debug "zZZZZzzzzZZZ attrib raw"  #$$
        attrs = raw.inject({}) do |result, (attr, string)|
          Delayed::Worker.logger.debug "~~downcase: #{attr} class: #{string.class.name}"
          result[attr] =

            
            
          
            if (!!string != string) && (!string.is_a?(BigDecimal)) && ( string.try(:downcase).in?(missing_values) )
              if attr == :dom || attr == :cdom
                "0"
              else
                nil
              end 
              
              
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (enums.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup"  #$$
              Provider.lookup(enums[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (lookup_types.has_key?(attr))
              Delayed::Worker.logger.debug "Attribute provider lookup types"  #$$
              Provider.lookup_type(lookup_types[attr], attr).call(raw)
            elsif (!!string != string) && (!string.is_a?(BigDecimal)) && (categories.has_key?(attr))
              Delayed::Worker.logger.debug "Category lookup"  #$$
              case attr
                when :category
                  Provider.lookup_composite(categories[attr], attr, :sub_category).call(raw)
               
              end 
            elsif (string.is_a?(BigDecimal))
              string
            else
              if attr == :owner_occupied 
                Delayed::Worker.logger.debug "owner occupied nil #{string.to_s}"
                if string.to_s == '' || string.to_s.blank?
                  Delayed::Worker.logger.debug "blank"
                  Property.parse(attr, nil) 
                else
                  Delayed::Worker.logger.debug "value set"
                  Property.parse(attr, string)    
                end
              
              else
                Delayed::Worker.logger.debug "Attribute property parse #{attr}"
                Property.parse(attr, string)
              end
            end
            
          result
        end
      else
        raw = {}
        Delayed::Worker.logger.debug "original: #{original}"
        # remove all key-value pairs with no available value, and include calcs (e.g street_address, address)
        attrs = market.properties.override(original).active_attributes(delegate: PropertyDetail, missing: false)
        Delayed::Worker.logger.debug "attrs: #{attrs}"
      end
      info = "MLS #{attrs[:mls_number]}" if attrs[:mls_number]
      info ||= "TAX #{attrs[:tax_id]}" if attrs[:tax_id]
      info ||= "ADDR #{attrs[:street_address]}, #{attrs[:zip_code]}"
      Delayed::Worker.logger.debug "Importing property: #{info}  ~attr: #{attrs}"  #$$
      Delayed::Worker.logger.debug "Importing raw #{raw}" #$$
      property = market.import_waterfront(self.source, attrs, raw, update_only)
      
      
    end


end


