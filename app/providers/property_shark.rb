class PropertyShark

  attr_accessor :agent, :service

  COUNTY_MAPPING = {
    Harris: 'Harris',
    Brazoria: 'Brazoria',
    Fort_Bend: 'Fort Bend',
    Galveston: 'Galveston',
    Montgomery: 'Montgomery'
  }
  
  def initialize()
    
    @service = Service.find_available('property_shark')    
    
    @agent = Mechanize.new { |ag|
      ag.read_timeout = 20
      ag.open_timeout = 20
      ag.idle_timeout = 0.9
      ag.retry_change_requests = true
    }
    
    @lot_square_feet = nil
    
    if Rails.env.development?
      @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE    
    else
      Delayed::Worker.logger.debug "SSLv3 verify mode set"
      @agent.ssl_version='SSLv3'
      cert_store = OpenSSL::X509::Store.new
      
      Delayed::Worker.logger.debug "loading cert: #{File.dirname('cacert.pem')}/cacert.pem"
      cert_path = File.expand_path "#{File.dirname('cacert.pem')}/cacert.pem"
      cert_store.add_file cert_path
      @agent.cert_store = cert_store
      
      Delayed::Worker.logger.debug  "setting verify mode to SSL"
      @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    end

  end

  def logged_in?
    Delayed::Worker.logger.debug "page present: #{@agent.page.present?}"
    Delayed::Worker.logger.debug "page body:  #{@agent.page.body}"   if @agent.page.present?
    if @agent.page.present? 
      if ((@agent.page.links_with(:text => 'Sign Out').first.present? ) || 
        @agent.page.search("//div[@class='psrk-report']").length > 0)
        return true
      end
    end
    
    return false
  end

  def login
    tries = 0
    result = nil
    begin
      Delayed::Worker.logger.debug "logging in"
      return true if logged_in?
  
      page = agent.post 'https://secure.propertyshark.com/mason/Accounts/logon.html', email: service.login, password: service.password, submit: 'Sign in'
      agent.page.link_with(:text => "Start using PropertyShark").click if(agent.page.link_with(:text => "Start using PropertyShark"))
      agent.page.link_with(:text => "Continue using PropertyShark").click if(agent.page.link_with(:text => "Continue using PropertyShark"))
      agent.page.link_with(:text => "Continue to PropertyShark").click if(agent.page.link_with(:text => "Continue to PropertyShark"))
      
      @service.increment_calls
      result = logged_in?
      Delayed::Worker.logger.debug  "login does not work anymore!!!" unless result
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    end
    result
  end

  def logout
    agent.get 'https://secure.propertyshark.com/mason/Accounts/logout.html'
    true
  end

  ENUMS = {
    # single_family townhouse_condo lots rental multi_family commercial mid_hi_rise_condo ranch_acerage
    category: {
      'Residential Single Family' => 'single_family',
      'Single Family Residence' => 'single_family',
      'Residential' => 'single_family',
      'Real Residential Single Family' => 'single_family'
    }
  }
  
 
  
  def lookup(property, options={})
    begin
      @lot_square_feet = nil
      if property.respond_to?(:length)
        property.map {|p| lookup(p, options) }
      else
        if login
          if property.pshark_id.nil? || property.pshark_county.nil?
            Delayed::Worker.logger.debug "state #{property.state}"
            Delayed::Worker.logger.debug self.class.search_url(property).gsub(/\\/, "")
            loc_ary = []
            loc_ary = loc_ary << property.city if property.city
            if property.county
              if property.county.match(/(.+(?i)county)/)
                property.county = property.county.gsub(/(?i)county/,'')
              end
              loc_ary = loc_ary << "#{property.county} County" 
            end
            
            loc_ary = loc_ary << property.state if property.state
            loc_key_search = loc_ary.join(', ')
            loc = property.zip_code ? property.zip_code : loc_key_search
            type = property.tax_id ? 'parcelID' : 'address'
            token = property.tax_id || property.street_address
            Delayed::Worker.logger.debug "loc: #{loc}  type: #{type}  token: #{token}"
            #page = agent.post "http://www.propertyshark.com/mason/UI/homepage_search.html", location: "#{property.zip_code}", search_token: "#{token}", search_type: "#{type}",  submit_button: "Search"
            page = search_address(loc, token, type)
            Delayed::Worker.logger.debug "page: #{page.uri.to_s}"
            @service.increment_calls
            if page.uri.to_s.match('^http://www.propertyshark.com/mason/Lookup/resolve4.html?')
              Delayed::Worker.logger.debug "uri: #{page.parser.xpath("//div[@id='content']/div[@id='content-inner']//ul/li/a/text()").first}"
              # .parser.xpath //div[@id='content']/div[@id='content-inner']//h3/ul/a
              Delayed::Worker.logger.debug "determining length of match #{page.search("//div[@id='content']/div[@id='content-inner']//ul/li/a").length.to_s}"
              page = predict_address(page)
              @service.increment_calls
            end
            if m = page.uri.to_s.match(/^http:\/\/www\.propertyshark\.com\/mason\/(.*)\/Reports2\/showsection.html\?propkey=(\d*)$/)
              Delayed::Worker.logger.debug "exact match found"
              # **
              property.pshark_county = m[1]
              # attrs[:count] m[1]
              property.pshark_id = m[2]
              property.street_address = nil
              loaded = true
            elsif   m = page.uri.to_s.match(/^http:\/\/www\.propertyshark\.com\/mason\/Reports2\/showsection.html\?propkey=(\d*)$/)
              Delayed::Worker.logger.debug "similar match found"
              
              property.pshark_county = nil
              property.pshark_id = m[1]
              property.street_address = nil
              loaded = true
              
            else
              Delayed::Worker.logger.debug  "property not found on pshark"
              return nil
            end
          end
          if property.pshark_id || property.pshark_county
            unless loaded
              page = agent.get "http://www.propertyshark.com/mason/#{property.pshark_county}/Reports2/showsection.html?propkey=#{property.pshark_id}"
              @service.increment_calls
            end
            attrs = {}
            parse_address(page, attrs)
            
            # Delayed::Worker.logger.debug "?/page: #{page.body}"
            tokens = page.search("ol/li/a").map {|n| n['href'].try(:gsub, '#', '') }
            
            Delayed::Worker.logger.debug  "token #{tokens}"
            load_sections(tokens, property, attrs, 'generic_overview', 'for_sale', 'building', 'land', 'property_taxes', 'property_tax')
            Delayed::Worker.logger.debug  attrs
            attrs.each do |attr, value|
              if ENUMS.has_key?(attr)
                Delayed::Worker.logger.debug  "~~~~ attr #{attr}  value #{value}"
                value = 'single_family'  #ENUMS[attr][value]
                Delayed::Worker.logger.debug  "~~~~ value #{value} ENUM =  #{ENUMS[attr][value]}"
              end
               Delayed::Worker.logger.debug "attr name: #{attr}"
              attrs[attr] = Property.parse(attr, value) unless (value.blank? || attr.to_sym == :tax_id)
            end
            Delayed::Worker.logger.debug  attrs
            # only add missing attrs
            attrs = attrs.select {|k, v| property.send(k).missing? }
            Delayed::Worker.logger.debug  attrs
            property.pshark_updated_at = DateTime.now
            
            property.override_attributes(attrs)
          end
           # logout
        else
          Delayed::Worker.logger.debug  "cannot login into pshark!!"
          return nil
        end
        property
      end
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      Delayed::Worker.logger.debug  "timeout error"
      return nil
    rescue Exception => e
      Delayed::Worker.logger.debug  "other error"
      return nil
    end
    
  end

  def predict_address(org_page)
    tries = 0
    new_page = nil
    begin
      new_page = org_page
      if new_page.search("//div[@id='content']/div[@id='content-inner']//ul/li/a").length > 0 
        node = new_page.search("//div[@id='content']/div[@id='content-inner']//ul/li/a")[0]
        Delayed::Worker.logger.debug "searching similar items"
        next_link = Mechanize::Page::Link.new( node, agent, new_page )
        Delayed::Worker.logger.debug "instantiating next link"
        new_page = next_link.click
        Delayed::Worker.logger.debug "next link clicked"
        
        # Delayed::Worker.logger.debug "next page uri: #{page.uri.to_s}"
      end
    
    rescue Timeout::Error, Net::HTTP::Persistent::Error  =>  e 
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    end
    
    new_page
      
  end
 
  def search_address(loc, token, type)
    tries = 0
    page = nil
    begin
      page = agent.post "http://www.propertyshark.com/mason/UI/homepage_search.html", location: "#{loc}", search_token: "#{token}", search_type: "#{type}",  submit_button: "Search"
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e 
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    rescue Exception => e 
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    end
    
    page
  end
  
  private

    def load_sections(tokens, property, attrs, *queries)
      tries = 0
      page = nil
      begin
        Delayed::Worker.logger.debug "#{tokens}"
        for query in queries
          # select element from an array according to criteria defined
          tokens.select{|t| t.match query}.each do |token|
          # token = tokens.select{|t| t.match query}.first
          
            if token
              if property.pshark_county
                url = "http://www.propertyshark.com/mason/#{property.pshark_county}/Reports2/showsection.html?propkey=#{property.pshark_id}&&token=#{token}&content_only=1"
                
              else
                url = "http://www.propertyshark.com/mason/Reports2/showsection.html?propkey=#{property.pshark_id}&&token=#{token}&content_only=1"
              end
              Delayed::Worker.logger.debug  url
              page = agent.get url
              @service.increment_calls
              self.send("parse_#{query}", page, attrs)
            end
          end
        end
      rescue Timeout::Error, Net::HTTP::Persistent::Error  =>  e 
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      rescue Exception => e
        tries += 1
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        raise e
      end
    end

    def parse_address(page, attrs)
        matched_county = page.uri.to_s.match(/\/(?i)tx_(\D+?)\//).captures.first rescue nil
        if matched_county
          COUNTY_MAPPING.each do |k,v|
            if matched_county == k.to_s.downcase.gsub(/_/,'')
              attrs[:county] ||= v
            end
          end
        end
        Delayed::Worker.logger.debug "Current URL: #{page.uri.to_s}" 
        Delayed::Worker.logger.debug "COUNTY MAPPING: #{COUNTY_MAPPING}"
        
        county_mapping = (COUNTY_MAPPING.keys.map do |k| k.to_s.gsub(/_/,'-')  end).join("|")
        
        Delayed::Worker.logger.debug "county_mapping: #{county_mapping}"
        link = page.link_with(:href => /mason\/info\/Property-Records\/TX\/(#{county_mapping})/)
        
        Delayed::Worker.logger.debug "link: #{link}"
        if link
          county = link.href.gsub(/\/$/, "").split('/').last.gsub(/-County/,"").gsub(/-/,"_")
          Delayed::Worker.logger.debug "county: #{county}"
          attrs[:county] ||= COUNTY_MAPPING[county.to_sym]
          Delayed::Worker.logger.debug "county saved: #{attrs[:county]}"
        end
        
        address = page.at(".report_title").text().strip.split(',')
        Delayed::Worker.logger.debug "address: #{address}"
        if address.length == 3
                 
          attrs[:street_address] = address[0].strip
          attrs[:city] = address[1].strip
          state_zip = address[2].strip.split(' ')
          Delayed::Worker.logger.debug "state_zip: #{state_zip}"
          if state_zip.length == 2
            Delayed::Worker.logger.debug "setting state and zip_code state zip 0 #{state_zip[0].strip} state zip 1 #{state_zip[1].strip}"
            
            attrs[:state] = state_zip[0].strip
            attrs[:zip_code] = state_zip[1].strip
            
            Delayed::Worker.logger.debug "attrs[:state] 1: #{attrs[:state]}"
          end
          address_ary = []
          address_ary << attrs[:street_address] if attrs[:street_address]
          address_ary << attrs[:city] if attrs[:city]
          pstate = attrs[:state].clone
          pstate << " #{attrs[:zip_code]}" if attrs[:zip_code]
          address_ary << pstate
          attrs[:address] = address_ary.join(', ')
          
          Delayed::Worker.logger.debug "attrs[:state] 2: #{attrs[:state]}"
        end
    end

    def parse_generic_overview(page, attrs)
      
      case COUNTY_MAPPING.invert[attrs[:county]]

        when :Harris
          Delayed::Worker.logger.debug "Harris"
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel ID']").parent.next_element.text rescue nil
          attrs[:tax_id] ||= page.at("th[text()*='Parcel ID']").next_element.text rescue nil
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel number']").parent.next_element.text rescue nil
          
          Delayed::Worker.logger.debug "tax id scraped: #{attrs[:tax_id]}"
          attrs[:legal_description] ||= page.at("h6[text()*='Legal Description']").ancestors('.title').first.next_element.at('td').text rescue nil
          attrs[:legal_description] ||= page.at("th[text()*='Legal description']").next_element.text rescue nil
          if attrs[:legal_description]
            attrs[:legal_description].scan(/[A-z]+\s+\d+\s+[A-z]+\s+\d+\s+(\D+?)((?i)SEC)?(\s?\d+\D*)?$/) do |legal_subdivision, sec, end_of_line| 
              attrs[:legal_subdivision] = legal_subdivision.strip
            end
          end 
          neighborhood = page.at("strong[text()*='Neighborhood']").parent.next_element.text rescue nil
          Delayed::Worker.logger.debug "neighborhood #{neighborhood}"
          if neighborhood
            Delayed::Worker.logger.debug "neighborhood length #{neighborhood.match(/(?:(?!(?i)sec|#).)*/).length.to_s}"
            attrs[:subdivision] = neighborhood.match(/(?:(?!(?i)sec|#).)*/)[0].to_s.strip if neighborhood.match(/(?:(?!(?i)sec|#).)*/)
            Delayed::Worker.logger.debug "sub division grabbed #{attrs[:subdivision]}"
          end 
          attrs[:key_map_page] ||= page.at("strong[text()*='Key Map']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("strong[text()*='Tax year']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("th[text()*='Tax year']").next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("strong[text()*='Property tax']").parent.next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("th[text()*='Property tax']").next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("strong[text()*='Market Value']").parent.next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("th[text()*='Market value']").next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year Built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("th[text()*='Year Built']").next_element.text rescue nil
          @lot_square_feet ||= page.at("th[text()*='Acreage']").next_element.text.to_d * 43650 rescue nil 
          attrs[:beds] ||= page.at("strong[text()*='Bedrooms']").parent.next_element.text rescue nil
          attrs[:beds] ||= page.at("th[text()*='Bedrooms']").next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("th[text()*='Square Footage']").next_element.text rescue nil
          attrs[:category] ||= page.at("th[text()*='Building class']").next_element.text rescue nil
          attrs[:category] ||= page.at("h6[text()*='Building']").ancestors('.title').first.next_element.at('td').at("strong[text()*='Description']").parent.next_element.text  rescue nil
        
        when :Brazoria
          Delayed::Worker.logger.debug "Brazoria"
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel ID']").parent.next_element.text rescue nil
          attrs[:tax_id] ||= page.at("th[text()*='Parcel ID']").next_element.text rescue nil
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel number']").parent.next_element.text rescue nil
          
          Delayed::Worker.logger.debug "tax id scraped: #{attrs[:tax_id]}"
          attrs[:legal_description] ||= page.at("th[text()*='Legal description']" || "th[text()*='Legal Description']").next_element.text rescue nil  #Y
          # attrs[:legal_description] ||= page.at("h6[text()*='Legal Description']").ancestors('.title').first.next_element.at('td').text rescue nil
          # attrs[:legal_description] ||= page.at("th[text()*='Legal description']").next_element.text rescue nil
          if attrs[:legal_description]
            attrs[:legal_description].scan(/^(\D+?)((?i)phase|sec)/) do |legal_subdivision, sec, end_of_line| 
              attrs[:legal_subdivision] ||= legal_subdivision.strip  #Y
            end
          end 
          
          attrs[:subdivision] = page.at("th[text()*='Neighborhood Code']").next_element.text rescue nil #Y
          attrs[:key_map_page] ||= nil
          attrs[:tax_year] ||= page.at("strong[text()*='Tax year']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("th[text()*='Tax year']").next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("strong[text()*='Property tax']").parent.next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("th[text()*='Property tax']").next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("strong[text()*='Market Value']").parent.next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("th[text()*='Market value']").next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year Built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("th[text()*='Year Built']").next_element.text rescue nil
          @lot_square_feet ||= page.at("th[text()*='Acreage']").next_element.text.to_d * 43650 rescue nil 
          attrs[:beds] ||= page.at("strong[text()*='Bedrooms']").parent.next_element.text rescue nil
          attrs[:beds] ||= page.at("th[text()*='Bedrooms']").next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("th[text()*='Square Footage']").next_element.text rescue nil
          attrs[:category] ||= page.at("th[text()*='Building class']").next_element.text rescue nil
          attrs[:category] ||= page.at("h6[text()*='Building']").ancestors('.title').first.next_element.at('td').at("strong[text()*='Building class']").parent.next_element.text  rescue nil
        when :Fort_Bend
          Delayed::Worker.logger.debug "Fort Bend"
          
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel ID']").parent.next_element.text rescue nil
          attrs[:tax_id] ||= page.at("th[text()*='Parcel ID']").next_element.text rescue nil
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel number']").parent.next_element.text rescue nil
          
          Delayed::Worker.logger.debug "tax id scraped: #{attrs[:tax_id]}"
          attrs[:legal_description] ||= page.at("h6[text()*='Legal Description']").ancestors('.title').first.next_element.at('td').text rescue nil
          attrs[:legal_description] ||= page.at("th[text()*='Legal description']").next_element.text rescue nil
          
          if attrs[:legal_description]
            attrs[:legal_description].scan(/^(\D+?)((?i)phase|sec)/) do |legal_subdivision, sec, end_of_line| 
              attrs[:legal_subdivision] ||= legal_subdivision.strip  #Y
            end
          end 
          
          neighborhood ||= page.at("strong[text()*='Neighborhood']").parent.next_element.text rescue nil
          neighborhood ||= page.at("strong[text()*='Neighborhood Description']").parent.next_element.text rescue nil
          Delayed::Worker.logger.debug "neighborhood #{neighborhood}"
          if neighborhood
            Delayed::Worker.logger.debug "neighborhood length #{neighborhood.match(/(?:(?!(?i)sec|#).)*/).length.to_s}"
            attrs[:subdivision] = neighborhood.match(/(?:(?!(?i)sec|#).)*/)[0].to_s.strip if neighborhood.match(/(?:(?!(?i)sec|#).)*/)
            Delayed::Worker.logger.debug "sub division grabbed #{attrs[:subdivision]}"
          end 
          attrs[:key_map_page] ||= page.at("strong[text()*='Key Map']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("strong[text()*='Tax year']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("th[text()*='Tax year']").next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("strong[text()*='Property tax']").parent.next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("th[text()*='Property tax']").next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("strong[text()*='Market Value']").parent.next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("th[text()*='Market value']").next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year Built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("th[text()*='Year Built']").next_element.text rescue nil
          @lot_square_feet ||= page.at("th[text()*='Acreage']").next_element.text.to_d * 43650 rescue nil 
          attrs[:beds] ||= page.at("strong[text()*='Bedrooms']").parent.next_element.text rescue nil
          attrs[:beds] ||= page.at("th[text()*='Bedrooms']").next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("th[text()*='Square Footage']").next_element.text rescue nil
          attrs[:category] ||= page.at("th[text()*='Building class']").next_element.text rescue nil
          attrs[:category] ||= page.at("h6[text()*='Building']").ancestors('.title').first.next_element.at('td').at("strong[text()*='Description']").parent.next_element.text  rescue nil
        when :Galveston
           Delayed::Worker.logger.debug "Galveston"
           
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel ID']").parent.next_element.text rescue nil
          attrs[:tax_id] ||= page.at("th[text()*='Parcel ID']").next_element.text rescue nil
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel number']").parent.next_element.text rescue nil
          
          Delayed::Worker.logger.debug "tax id scraped: #{attrs[:tax_id]}"
          attrs[:legal_description] ||= page.at("h6[text()*='Legal Description']").ancestors('.title').first.next_element.at('td').text rescue nil
          attrs[:legal_description] ||= page.at("th[text()*='Legal description']").next_element.text rescue nil
          
          if attrs[:legal_description]
            
            legal_subdiv_found = false
            
            attrs[:legal_description].scan(/^(\D+?)((?i)phase|sec)/) do |legal_subdivision, sec, end_of_line| 
              attrs[:legal_subdivision] ||= legal_subdivision.strip  #Y
            end
            
            legal_subdiv_found = true if attrs[:legal_subdivision]
            
            if (!legal_subdiv_found)
              attrs[:legal_description].scan(/\d+\s+(\D+)$/) do |legal_subdivision,end_phrase|
                attrs[:legal_subdivision] ||= legal_subdivision.strip
              end
            end
          end 
          
          neighborhood ||= page.at("strong[text()*='Neighborhood']").parent.next_element.text rescue nil
          neighborhood ||= page.at("strong[text()*='Neighborhood Description']").parent.next_element.text rescue nil
          Delayed::Worker.logger.debug "neighborhood #{neighborhood}"
          if neighborhood
            Delayed::Worker.logger.debug "neighborhood length #{neighborhood.match(/(?:(?!(?i)sec|#).)*/).length.to_s}"
            attrs[:subdivision] = neighborhood.match(/(?:(?!(?i)sec|#).)*/)[0].to_s.strip if neighborhood.match(/(?:(?!(?i)sec|#).)*/)
            Delayed::Worker.logger.debug "sub division grabbed #{attrs[:subdivision]}"
          end 
          attrs[:key_map_page] ||= page.at("strong[text()*='Key Map']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("strong[text()*='Tax year']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("th[text()*='Tax year']").next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("strong[text()*='Property tax']").parent.next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("th[text()*='Property tax']").next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("strong[text()*='Market Value']").parent.next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("th[text()*='Market value']").next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year Built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("th[text()*='Year Built']").next_element.text rescue nil
          @lot_square_feet ||= page.at("th[text()*='Acreage']").next_element.text.to_d * 43650 rescue nil 
          attrs[:beds] ||= page.at("strong[text()*='Bedrooms']").parent.next_element.text rescue nil
          attrs[:beds] ||= page.at("th[text()*='Bedrooms']").next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square feet']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("th[text()*='Square Footage']").next_element.text rescue nil
          attrs[:category] ||= page.at("th[text()*='Building class']").next_element.text rescue nil
          attrs[:category] ||= page.at("strong[text()*='Property Class']").parent.next_element.text rescue nil
          attrs[:category] ||= page.at("h6[text()*='Building']").ancestors('.title').first.next_element.at('td').at("strong[text()*='Description']").parent.next_element.text  rescue nil
        when :Montgomery
           Delayed::Worker.logger.debug "Montgomery"
           
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel ID']").parent.next_element.text rescue nil
          attrs[:tax_id] ||= page.at("th[text()*='Parcel ID']").next_element.text rescue nil
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel number']").parent.next_element.text rescue nil
          
          Delayed::Worker.logger.debug "tax id scraped: #{attrs[:tax_id]}"
          attrs[:legal_description] ||= page.at("h6[text()*='Legal Description']").ancestors('.title').first.next_element.at('td').text rescue nil
          attrs[:legal_description] ||= page.at("th[text()*='Legal description']").next_element.text rescue nil
          
          if attrs[:legal_description]
            
            legal_subdiv_found = false
            
            attrs[:legal_description].scan(/^(\D+?)\s+((?i)ck|br|phase|sec)/) do |legal_subdivision, sec, end_of_line|
              attrs[:legal_subdivision] ||= legal_subdivision.strip  #Y
            end
            
            legal_subdiv_found = true if attrs[:legal_subdivision]
            
            if (!legal_subdiv_found)
              attrs[:legal_description].scan(/\d+\s+(\D+)$/) do |legal_subdivision,end_phrase|
                attrs[:legal_subdivision] ||= legal_subdivision.strip
              end
            end
            
          end
          
          neighborhood ||= page.at("strong[text()*='Neighborhood']").parent.next_element.text rescue nil
          neighborhood ||= page.at("strong[text()*='Neighborhood Description']").parent.next_element.text rescue nil
          Delayed::Worker.logger.debug "neighborhood #{neighborhood}"
          if neighborhood
            Delayed::Worker.logger.debug "neighborhood length #{neighborhood.match(/(?:(?!(?i)sec|#).)*/).length.to_s}"
            attrs[:subdivision] = neighborhood.match(/(?:(?!(?i)sec|#).)*/)[0].to_s.strip if neighborhood.match(/(?:(?!(?i)sec|#).)*/)
            Delayed::Worker.logger.debug "sub division grabbed #{attrs[:subdivision]}"
          end 
          attrs[:key_map_page] ||= page.at("strong[text()*='Key Map']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("strong[text()*='Tax year']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("th[text()*='Tax year']").next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("strong[text()*='Property tax']").parent.next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("th[text()*='Property tax']").next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("strong[text()*='Market Value']").parent.next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("th[text()*='Market value']").next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year Built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("th[text()*='Year Built']").next_element.text rescue nil
          @lot_square_feet ||= page.at("th[text()*='Acreage']").next_element.text.to_d * 43650 rescue nil 
          attrs[:beds] ||= page.at("strong[text()*='Bedrooms']").parent.next_element.text rescue nil
          attrs[:beds] ||= page.at("th[text()*='Bedrooms']").next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("th[text()*='Square Footage']").next_element.text rescue nil
          attrs[:category] ||= page.at("th[text()*='Building class']").next_element.text rescue nil 
          attrs[:category] ||= page.at("h6[text()*='Building']").ancestors('.title').first.next_element.at('td').at("strong[text()*='Description']").parent.next_element.text  rescue nil
        else
           Delayed::Worker.logger.debug "Else"
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel ID']").parent.next_element.text rescue nil
          attrs[:tax_id] ||= page.at("th[text()*='Parcel ID']").next_element.text rescue nil
          attrs[:tax_id] ||= page.at("strong[text()*='Parcel number']").parent.next_element.text rescue nil
          
          
          Delayed::Worker.logger.debug "tax id scraped: #{attrs[:tax_id]}"
          attrs[:legal_description] ||= page.at("h6[text()*='Legal Description']").ancestors('.title').first.next_element.at('td').text rescue nil
          attrs[:legal_description] ||= page.at("th[text()*='Legal description']").next_element.text rescue nil
          if attrs[:legal_description]
            attrs[:legal_description].scan(/[A-z]+\s+\d+\s+[A-z]+\s+\d+\s+(\D+?)((?i)SEC)?(\s?\d+\D*)?$/) do |legal_subdivision, sec, end_of_line| 
              attrs[:legal_subdivision] = legal_subdivision.strip
            end
          end 
          neighborhood = page.at("strong[text()*='Neighborhood']").parent.next_element.text rescue nil
          Delayed::Worker.logger.debug "neighborhood #{neighborhood}"
          if neighborhood
            Delayed::Worker.logger.debug "neighborhood length #{neighborhood.match(/(?:(?!(?i)sec|#).)*/).length.to_s}"
            attrs[:subdivision] = neighborhood.match(/(?:(?!(?i)sec|#).)*/)[0].to_s.strip if neighborhood.match(/(?:(?!(?i)sec|#).)*/)
            Delayed::Worker.logger.debug "sub division grabbed #{attrs[:subdivision]}"
          end 
          attrs[:key_map_page] ||= page.at("strong[text()*='Key Map']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("strong[text()*='Tax year']").parent.next_element.text rescue nil
          attrs[:tax_year] ||= page.at("th[text()*='Tax year']").next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("strong[text()*='Property tax']").parent.next_element.text rescue nil
          attrs[:tax_amount] ||= page.at("th[text()*='Property tax']").next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("strong[text()*='Market Value']").parent.next_element.text rescue nil
          attrs[:tax_market_value] ||= page.at("th[text()*='Market value']").next_element.text rescue nil
          attrs[:year_built] ||= page.at("strong[text()*='Year Built']").parent.next_element.text rescue nil
          attrs[:year_built] ||= page.at("th[text()*='Year Built']").next_element.text rescue nil
          @lot_square_feet ||= page.at("th[text()*='Acreage']").next_element.text.to_d * 43650 rescue nil 
          attrs[:beds] ||= page.at("strong[text()*='Bedrooms']").parent.next_element.text rescue nil
          attrs[:beds] ||= page.at("th[text()*='Bedrooms']").next_element.text rescue nil
          attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
          attrs[:square_feet] ||= page.at("th[text()*='Square Footage']").next_element.text rescue nil
          attrs[:category] ||= page.at("th[text()*='Building class']").next_element.text rescue nil
          attrs[:category] ||= page.at("h6[text()*='Building']").ancestors('.title').first.next_element.at('td').at("strong[text()*='Description']").parent.next_element.text  rescue nil
      end
      
      if attrs[:tax_id]
        attrs[:tax_id] = attrs[:tax_id].gsub(/-/, '')
      end
       
    end
    
    def parse_for_sale(page, attrs)
        attrs[:category] ||= page.at("th[text()*='Property Type']").next_element.text rescue nil
        # attrs[:bathrooms] ||= page.at("th[text()*='Bathrooms']").next_element.text rescue nil
    end


    def parse_building(page, attrs)
      attrs[:beds] ||= page.at("td[text()*='Bedroom']").next_element.text rescue nil
      
      Delayed::Worker.logger.debug "zZZZzzz  Full Bath #{page.at("td[text()*='Full Bath']").next_element.text}"  if page.at("td[text()*='Full Bath']")
      Delayed::Worker.logger.debug "zZZZzzz  attr #{attrs[:bathrooms_full]}"
      if page.at("td[text()*='Full Bath']")
        attrs[:bathrooms_full] ||= page.at("td[text()*='Full Bath']").next_element.text rescue nil
      end 
      Delayed::Worker.logger.debug "zZZZZzz  attr after #{attrs[:bathrooms_full]}" 
      if page.at("strong[text()*='Square Footage']")
        attrs[:square_feet] ||= page.at("strong[text()*='Square Footage']").parent.next_element.text rescue nil
      end
      if page.at("strong[text()*='Building Type']")
        attrs[:category] ||= page.at("strong[text()*='Building Type']").parent.next_element.text rescue nil
      end
    end
    
    def parse_land(page, attrs)
      attrs[:lot_square_feet] ||= page.at("strong[text()*='Units']").parent.next_element.text rescue nil      
      if page.search("td[text()*='Res improved table value']").length > 0
        lsft  = 0
        page.search("td[text()*='Res improved table value']").each do |re|
          lsft = lsft + re.next_element.text.gsub(/,(?=\d{3}\b)/, '').to_i
        end
        attrs[:lot_square_feet] ||= lsft
      end
      attrs[:lot_square_feet] ||= (page.at("strong[text()='Land']").parent.next_element.text.to_d* 43650).to_i  rescue nil
      attrs[:lot_square_feet] ||= @lot_square_feet.to_i
    end
    
    def parse_property_taxes(page, attrs)
      attrs[:tax_value] ||= page.at("td[text()*='Assessed value']").next_element.next_element.text   rescue nil
      attrs[:list_price] ||=   attrs[:tax_value]
      
      attrs[:tax_rate] ||= page.at("td[text()*='Property tax']").parent.at("td[text()*='%']").text.gsub(/\n/,"").gsub(/\t/,"").gsub(/%/,"").to_f/100 rescue nil
      attrs[:tax_market_value] ||= page.at("td[text()*='Market value']").parent.at("td[@class='label_ptax stat right_n']" &&  "td[text()*='$']").text.gsub(/\n/,"").gsub(/\t/,"").gsub(/$/,"") rescue nil
    end
    
     def parse_property_tax(page, attrs)
      attrs[:tax_value] ||= page.at("td[text()*='Assessed value']").next_element.next_element.text   rescue nil
      attrs[:list_price] ||=   attrs[:tax_value]
      
      attrs[:tax_rate] ||= page.at("td[text()*='Base tax']").parent.at("td[text()*='%']").text.gsub(/\n/,"").gsub(/\t/,"").gsub(/%/,"").to_f/100 rescue nil 
    end
    
    def self.search_url(property)
      loc_ary = []
      loc_ary = loc_ary << property.city if property.city
      loc_ary = loc_ary << property.county if property.county
      loc_ary = loc_ary << property.state if property.state
      loc_key_search = loc_ary.join(', ')
      loc = property.zip_code ? property.zip_code : loc_key_search
      type = property.tax_id ? 'parcelID' : 'address'
      token = property.tax_id || property.street_address
      "\"http://www.propertyshark.com/mason/UI/homepage_search.html\", location: \"#{loc}\", search_token: \"#{token}\", search_type: \"#{type}\",  submit_button: \"Search\""
    end
end
