class QueryInput < SimpleForm::Inputs::TextInput

  def input
    original = (object.class.attr_config(attribute_name) || {})[:default] if object
    template.content_tag(:div) do
      area = @builder.text_area(attribute_name, input_html_options.merge_with_class(rows: 10, style: 'display: block', class: 'span8'))
      original ? area.concat(template.link_to(template.t(:reset), 'javascript:void(0)', class: 'query reset btn btn-warning btn-small', data: {original: original})) : original
    end
  end

end