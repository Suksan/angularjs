class FileInput < SimpleForm::Inputs::FileInput

  def input
    if object && object.class.respond_to?(:active_config)
      paperclip = object.send(attribute_name)
      content_type = (object.class.attr_config(attribute_name) || {})[:content_type]
      input_html_options[:accept] ||= content_type if content_type
      link = template.content_tag(:div) { template.link_to(object["#{attribute_name}_file_name"], paperclip.url(:original, false), target: :_blank) } if paperclip.try(:file?)
    end
    field = @builder.file_field(attribute_name, input_html_options) 
    link ? link.concat(field) : field
  end

  def errors_on_attribute
    result = object.errors[attribute_name]
    %w{file_name file_size content_type}.each do |ext|
      result += object.errors["#{attribute_name}_#{ext}"]
    end
    result
  end

end