class GoodIp < ActiveRecord::Base
 
  
  validates_presence_of :ip
  
  attr_config do |c|
    
    c.ip :string
    
  end
  
  def self.find_working_ips
    good_ips = GoodIp.order("id").limit(6).pluck(:ip)
    return good_ips
  end
  
  def self.remove_all
    GoodIp.delete_all
  end
      
end