class MarketRenovationTimeline < ActiveRecord::Base
  
  attr_config do |c|
    c.min :money
    c.max :money
    c.weeks! :integer
  end

  validates :min, :max, :uniqueness => { :scope => [:source_type, :source_id] }
  validate :consistency

  belongs_to :source, :polymorphic => true

  def include?(value)
    (self.min.nil? || (self.min <= value)) && (self.max.nil? || (value < self.max))
  end

  private

    def consistency
      if min.nil? && max.nil?
        [:min, :max].each {|attr| errors.add(attr, :is_blank) }
      elsif min && max && min > max
        [:min, :max].each {|attr| errors.add(attr, :invalid) }
      end
    end

end