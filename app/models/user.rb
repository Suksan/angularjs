class User < ActiveRecord::Base

  scope :admins, where(admin: true)

  devise :async, :database_authenticatable, :recoverable, :trackable, :validatable, :timeoutable
          # :token_authenticatable, :rememberable, :confirmable, :registerable, :confirmable
          # :lockable, :timeoutable and :omniauthable
  
  attr_config do |c|
    c.preferences :serialize, with: UserPreferences
    c.inactive boolean: false
    c.admin boolean: false
    c.firstname
    c.lastname
    c.role enum: 'assessor', in: %w{analyst assessor client}
    c.phone :phone
    c.address
    c.city
    c.state :state
    c.zip :string, format: /\A\d{5}\z/
    c.company
    c.fax :phone
  end

  attr_protected :encrypted_password, :reset_password_token, :reset_password_sent_at, :remember_created_at,
                 :sign_in_count, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip,
                 :confirmation_token, :confirmed_at, :confirmation_sent_at, :unconfirmed_email,
                 :failed_attempts, :unlock_token, :locked_at, :authentication_token

  has_many :assignments, :class_name => 'MarketAssignment', :dependent => :destroy
  has_many :markets, :through => :assignments
  has_many :my_props, foreign_key: :user_id, dependent: :destroy

  # validates :firstname, :lastname, :role, :phone, :address, :city, :state, :zip, presence: true, unless: 'self.admin || !self.new_record?'
  # validates :markets, :length => { :minimum => 1, :message => 'at least one must be selected' }

  def status
    if inactive
      'inactive'
    elsif sign_in_count == 0
      'registered'
    elsif reset_password_token && reset_password_period_valid?
      'reset'
    else
      'active'
    end
  end

  def analyst?; role == 'analyst'; end
  def assessor?; role == 'assessor'; end

  def active_for_authentication?
    !inactive
  end
  
  def inactive_message
    "Sorry, this account has been deactivated."
  end

  def to_s
    self.name
  end

  def name
    [self.firstname, self.lastname].compact.join(' ')
  end

  def send_welcome_instructions
    generate_reset_password_token!
    Mailer.delay.welcome_instructions(self)
  end
  
  # def valid_password?(password)
    # if self.encrypted_password.present?
      # if ::Digest::MD5.hexdigest(password).upcase == self.encrypted_password
        # self.password = password
        # self.legacy_password_hash = nil
        # self.save!
        # true
      # else
        # false
      # end
    # else
      # super
    # end
  # end
# 
  # def reset_password!(*args)
    # self.encrypted_password = nil
    # super
  # end

end