class MarketDefinition < ActiveRecord::Base

  belongs_to :source, :polymorphic => true

  attr_config do |c|
    c.purchase_price_discount! percent: '0.05', in: -0.5..0.5
    c.purchase_commission_earned! percent: '0.03', in: 0..0.5 
    c.purchase_bank_short_discount! percent: '0.1', in: -0.5..0.5 
    c.purchase_homeowner_short_discount! percent: '0.1', in: -0.5..0.5 
    
    c.rental_loan loan: 'long'
    c.rental_annual_rent_increase! percent: '0.03', in: -0.5..0.5 
    c.rental_annual_vacancy_rate! percent: '0.05', in: 0..1 
    c.rental_annual_maintenance_expense! percent: '0.05', in: 0..1 
    c.rental_management_expense! percent: '0.08', in: 0..1 
    c.rental_annual_expense_increase! percent: '0.02', in: 0..1 
    c.rental_years_to_sale! integer: '5'
    
    c.seller_finance_loan! loan: 'long'
    c.seller_finance_sale_time_discount! percent: '0.1'
    c.seller_finance_down_payment! percent: '0.1'
    c.seller_finance_interest_rate! percent: '0.0825'
    c.seller_finance_term! integer: '360'
    c.seller_finance_realtor_fee! percent: '0.03'
    c.seller_finance_management_expense! percent: '0.02'
    c.seller_finance_years_to_sale! integer: '5'
    
    c.rental_resale_loan! loan: 'bridge'
    # c.rental_resale_investor_cap money: '10000'
    c.rental_resale_marketing_expense! money: '1000'
    c.rental_resale_closing_expense! percent: '0.015'
    c.rental_resale_warranty_expense! money: '1500'
    c.rental_resale_investor_cap_rate percent: '0.08'
    
    c.sale_loan! loan: 'bridge'
    c.sale_realtor_fee_closing_costs! percent: '0.045'
    c.sale_seller_contribution! percent: '0.01'
    
    c.build_loan! loan: 'short'
    c.build_realtor_fee_closing_costs! percent: '0.045'
    c.build_seller_contribution! percent: '0.01'
    
    c.listing_prep_days! integer: '15'
    c.listing_sale_comp_discount! percent: '0.1'
    c.listing_commission_income! percent: '0.03'
    c.listing_marketing_expense! money: '500'
    c.listing_agent_expense! percent: '0.50'
    
    c.listing_partnership_loan! loan: 'bridge'
    c.listing_partnership_commission_income! percent: '0.03'
    c.listing_partnership_construction_markup! percent: '0.125'
    c.listing_partnership_profits_percent! percent: '0.20'
    c.listing_partnership_marketing_legal_expense! money: '1000'
    c.listing_partnership_seller_contribution! percent: '0.01'

    c.assignment_sale_time_discount! percent: '0.3'
    c.assignment_investor_arv_sale_ltv! percent: '0.8'
    c.assignment_marketing_expense! money: '500'
    c.assignment_closing_expense! money: '500'
    
    c.lending_bridge_purchase_down_payment! percent: '0.2'
    c.lending_bridge_renovation_down_payment! percent: '0.5'
    c.lending_bridge_max_ltv! percent: '0.75'
    c.lending_bridge_interest_rate! percent: '0.1'
    c.lending_bridge_points! percent: '0.01'
    c.lending_bridge_term! integer: '6'
    c.lending_bridge_closing_costs! percent: '0.005'
    c.lending_bridge_interest_only! boolean: true
    c.lending_bridge_amortization :integer
    
    c.lending_short_down_payment! percent: '0.2'
    c.lending_short_max_ltv! percent: '0.75'
    c.lending_short_interest_rate! percent: '0.1'
    c.lending_short_points! percent: '0.01'
    c.lending_short_term! integer: '12'
    c.lending_short_closing_costs! percent: '0.005'
    c.lending_short_interest_only! boolean: true
    c.lending_short_amortization :integer

    c.lending_short2_down_payment! percent: '0.2'
    c.lending_short2_max_ltv! percent: '0.75'
    c.lending_short2_interest_rate! percent: '0.06'
    c.lending_short2_points! percent: '0.01'
    c.lending_short2_term! integer: '60'
    c.lending_short2_closing_costs! percent: '0.005'
    c.lending_short2_interest_only! boolean: true
    c.lending_short2_amortization :integer
    
    c.lending_long_down_payment! percent: '0.25'
    c.lending_long_max_ltv! percent: '0.75'
    c.lending_long_interest_rate! percent: '0.04'
    c.lending_long_points! percent: '0.01'
    c.lending_long_term! integer: '360'
    c.lending_long_closing_costs! percent: '0.005'
    c.lending_long_interest_only! boolean: 'false'
    c.lending_long_amortization integer: '360'

    c.general_company_marginal_tax_rate! percent: '0.3'
    c.general_investor_marginal_tax_rate! percent: '0.3'
    c.general_capital_gains_rate! percent: '0.15'
    c.general_improvement_ratio! percent: '0.28'
    c.general_years_depreciation! decimal: '27.5'
    c.general_annual_insurance_expense! percent: '0.011'
    c.general_annual_utility_expense! money: '2400'
    c.general_days_to_close! integer: '15'
    c.general_appreciation_rate! percent: '0.03'
    c.general_tax_rate! percent: '0.0285'

    c.return_minimum_cap_rate! percent: '0.10'
    c.return_minimum_roi! percent: '0.15'
    c.return_minimum_return_on_total_project! money: '5000' 
    c.return_rental_comp_level integer: '5', in: 1..6 
    c.return_rental_sale_comp_level integer: '3', in: 1..6 
    c.return_seller_finance_comp_level integer: '3', in: 1..6 
    c.return_rental_resale_comp_level integer: '4', in: 1..6 
    c.return_sale_comp_level integer: '5', in: 1..6 
    c.return_build_comp_level integer: '5', in: 1..6 
    c.return_assignment_comp_level integer: '6', in: 1..6 

    c.renovation_unspecified_sale_rehab! money: '18'
    c.renovation_unspecified_rental_rehab! money: '10'
    c.return_new_sale_comp_level! comp_level: 'expanded'
    c.return_new_rental_comp_level! comp_level: 'expanded'
  end

end