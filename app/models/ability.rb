class Ability
  include CanCan::Ability

  def initialize(user,controller_namespace)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
    return if user.nil?
    
    puts "controller namespace: #{controller_namespace}"
    if user.admin?
      if(controller_namespace != 'Client')
        can :manage, :all
        cannot :destroy, User, id: user.try(:id)
      end
    elsif user.role.in?(['analyst','assessor'])
      if(controller_namespace != 'Client')
        can :show, user.markets
        can [:show, :summary, :proforma, :save_proforma, :save_comment,
          :comps, :comps_rental, :comps_sale, :comps_build,
          :map, :mls, :tax, :deed, :saved, :comment, :add_saved, :remove_saved, :update_rep, :update_flood, :update_near_commercial, :update_tag, :rem_myprop, :export, :comps_rental_export, :comps_sale_export, :comps_build_export], Property
        can [:read, :create], Job, name: 'MarketUploadJob'
        can [:search_self], User
  
        case user.role
          when 'analyst'
            can :update, Market
            can :manage, Search
            can [:recompute, :download, :level2, :level3, :update, :comment], Property
            can :bulk, Job, name: 'MarketUploadJob'
          when 'assessor'
        end
        
        # model bases permissions (should we move to object instead?)
        can :destroy, Job do |job|
          job.user == user && !job.running?
        end
      end
    elsif user.role.in?(['client'])
      
      if(controller_namespace == 'Client')
        # can :manage, :all
        puts "come to this"
        can :edit_preferences
        can :show, user.markets
        can [:recompute,:show,:summary,:save,:client_sale_data_set_per_page,:client_rental_data_set_per_page,:tax,:deed,:zillow,:pshark,:update_flood,:update_tag,:update_near_commercial,:update_rep,:comps_rental_export,:comps_sale_export,
          :comps, :comps_rental, :comps_sale, :comps_build, :map, :mls,:comment,:save_comment,:saved,:rem_myprop,:set_per_page,:add_saved, :remove_saved, :capex_calc, :request_visit, :request_visit_form, :schedule_visit, :neighborhood,:opportunities,:search_opportunities,:show_rental_comps, :show_sale_comps,:show_subject_property, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others], Property
        can [:read, :create], Job, name: 'MarketUploadJob'
        can [:search_self], User
        
        case user.role
          when 'client'            
            can :manage, Search
            can :manage, Market
            can :bulk, Job, name: 'MarketUploadJob'
            can :manage, PropertyTag
         
        end
        
        can :destroy, Job do |job|
          job.user == user && !job.running?
        end
      
      end
    else
      can :show, user.markets
      can [:show, :summary, :proforma, :save_proforma, :save_comment,
        :comps, :comps_rental, :comps_sale, :comps_build,
        :map, :mls, :tax, :deed, :saved, :comment, :add_saved, :remove_saved, :update_rep, :update_flood, :update_near_commercial, :update_tag, :rem_myprop, :export, :comps_rental_export, :comps_sale_export, :comps_build_export, :show_rental_comps, :show_sale_comps, :show_subject_property, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others], Property
      can [:read, :create], Job, name: 'MarketUploadJob'
      can [:search_self], User
      
      can :destroy, Job do |job|
        job.user == user && !job.running?
      end
    end
  end
end
