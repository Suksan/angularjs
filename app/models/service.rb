class Service < ActiveRecord::Base

  attr_config do |c|
    c.code! :enum, in: %w{property_shark zillow google_geocoding mapquest_geocoding}
    c.login :string, uniqueness: { scope: :code }
    c.password :password
    c.key
    c.daily_limit :integer
    c.daily_calls :integer
    c.last_date :date
    c.too_many_attempts boolean: false
    c.error_count :integer
    c.last_called_at :datetime
  end

  validate :consistency

  def name
    code.titleize
  end

  def self.find_available(code)

    if code == :google_geocoding
      Service.where(["code = ? AND (
        (daily_limit IS NULL) OR ((last_date = ?) && (daily_limit > daily_calls) && (error_count < ?) &&  ((last_called_at IS NULL) or (last_called_at < ?)))
      OR
        (
          ((last_date IS NULL) OR ((last_date < ?) && ((last_called_at IS NULL) or (last_called_at < ?)) )) && (error_count < ?)
        )
      )","google_geocoding", Date.today,3,Time.now - 2.minutes,Date.today,Time.now - 16.hours,3]).first
    elsif (code == :mapquest_geocoding) || (code == :geocodio_geocoding) 
      Service.where(["code = ? AND ((daily_limit IS NULL) OR ((last_date = ?) && (daily_limit > daily_calls) && (error_count < ?)) OR (((last_date IS NULL) OR (last_date < ?)) && (error_count < ?)))",code, Date.today,3,Date.today,3]).first
    else
      Service.where(["code = ? AND ((daily_limit IS NULL) OR (last_date = ? && daily_limit > daily_calls) OR (last_date IS NULL OR last_date < ?))", code, Date.today, Date.today]).first
    end

  end

  def increment_calls(by = 1)

    begin
      return if self.daily_limit.nil?

      current_service = self.class.find(self.id)
      dcall = current_service.daily_calls ? current_service.daily_calls  : 0
      dlast_date = current_service.last_date



      if dlast_date == Date.today
        self.daily_calls = dcall + by
      else
        self.last_date = Date.today
        self.daily_calls = by
      end

      Delayed::Worker.logger.debug "~~current_service.code #{current_service.code}"
      if (current_service.code == "google_geocoding")
        Delayed::Worker.logger.debug "~~self.daily_calls: #{self.daily_calls}"
        if (self.daily_calls % 5 == 0)
          Delayed::Worker.logger.debug "~~temp lock: #{self.daily_calls}"
          self.last_called_at = Time.now
        end
      end

      save(validate: false)
    rescue Exception => e
      Delayed::Worker.logger.debug "~~e.message: #{e.message}"
    end
  end

  def reconcile_daily_limit_reached
    return if self.daily_limit.nil?

    self.daily_calls = self.daily_limit
    save(validate: false)
  end

  def mark_error
    self.too_many_attempts = true
    save(validate: false)
  end

  def suspend
    self.daily_calls = self.daily_limit
    save(validate: false)
  end

  def add_error_count
    self.error_count += 1
    save(validate: false)
  end
  
  def set_max_error
    self.error_count  = 3
    save(validate: false)
  end

  def rem_error_count
    self.error_count = 0
    save(validate: false)
  end

  private

    def consistency
      errors.add(:password, :is_blank) if password.blank? && !login.blank?
      errors.add(:login, :is_blank) if login.blank? && !password.blank?
      errors.add(:daily_limit, :is_blank) if daily_limit.blank? && !key.blank?
    end

end