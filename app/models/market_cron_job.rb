class MarketCronJob < ActiveRecord::Base
  attr_config do |c|
    c.name :enum, in: %w{Mon Tue Wed Thu Fri Sat Sun}
    c.start_hour :hour
    c.start_min :minute
    c.duration_hour :hour_twenty_four
    c.duration_min :minute
    c.mer_indicator :meridian_indicator
    c.frequency_type :string, calc: proc {|m| "every_day_of_week" }
    c.day_of_week :string, calc: proc {|m| m.name } 
    
    
    # c.bathrooms :decimal,           calc: proc {|m| m.bathrooms_full + ((m.bathrooms_half || 0) * 0.5.to_d) } 
  end
  
    

  validates_presence_of :name,:start_hour,:start_min
  validates :start_hour, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :start_min, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  
  belongs_to :market
  
  before_save :calculate
  after_save :send_cron_later
  before_destroy :calculate,:send_cron_later
  
 
  #Setting.for(:display).set=(value)
  def self.for(name)
    MarketCronJob.where(["name = ?", name]).first

  end
 
  def send_cron_later
    #Using delayed job to run this later to not hold up an HTTP thread.
    Delayed::Worker.logger.debug  "start sand cron later"
    @notifier = DeletedCronJob.new(self.clone.attributes)
    @notifier.save
    @notifier.delay.update_cron 
    Delayed::Worker.logger.debug  "end send cron later"
  end  
 
  def update_cron
    system "bundle exec whenever --clear-crontab greenlight  --user greenlight "
    system "bundle exec whenever --update-crontab greenlight --set environment=#{ENV['RAILS_ENV']}  --user greenlight "
  end
  
  def calculate
    Delayed::Worker.logger.debug "calculate"
    self.send(:calculate_fields) 
  end
  
  
end

# class MarketCronJob < ActiveRecord::Base
  # attr_accessible :name,:start_hour,:start_min,:duration,:frequency_type,:day_of_week
  # validates_uniqueness_of :name
  # validates_presence_of :name,:start_hour,:start_min
  # validates :start_hour, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  # validates :start_min, numericality: { greater_than_or_equal_to: 0, only_integer: true }
#  
  # after_save :send_cron_later
#  
  # #Setting.for(:display).set=(value)
  # def self.for(name)
    # CronJob.where(["name = ?", name]).first
# 
  # end
#  
  # def send_cron_later
    # #Using delayed job to run this later to not hold up an HTTP thread.
    # p "start send cron later"
    # delay.update_cron 
    # p "end send cron later"
  # end  
#  
  # def update_cron
    # system "bundle exec whenever --update-crontab greenlight --set environment=#{ENV['RAILS_ENV']}  --user greenlight "
  # end
# end