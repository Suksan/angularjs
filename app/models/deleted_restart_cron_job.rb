class DeletedRestartCronJob < ActiveRecord::Base
  attr_config do |c|
    c.name :string
  end

  def update_cron
    system "bundle exec whenever --clear-crontab greenlight  --user greenlight "
    system "bundle exec whenever --update-crontab greenlight --set environment=#{ENV['RAILS_ENV']}  --user greenlight "
  end
end