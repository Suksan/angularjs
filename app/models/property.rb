class Property < ActiveRecord::Base
PROX_1 = '23.95.193.93'
PROX_2 = '192.230.49.225'
PROX_3 = '23.94.87.170'
PROX_4 = '23.95.193.32'
PROX_5 = '192.69.254.216'
PROX_6 = '23.94.87.197'

  @@class_user_id = nil
  attr_accessor :is_non_admin, :current_user_id

  COMP_STATS_FIELDS = [:square_feet, :beds, :bathrooms, :lot_square_feet, :list_price, :list_price_psf, :sale_price, :sale_price_psf, :cdom, :year_built, :dom]
  COMP_STATS_OPERATORS = [:min, :avg, :max, :mean]
  SEARCH_STATS_DISTINCTS = [:geo_market_area]
  SEARCH_STATS_FIELDS = [:street_number, :beds, :bathrooms, :square_feet, :year_built, :list_price, :sold_lease_price, :sold_price, :owner_equity_ratio, :listing_date, :closed_date, :cdom, :comparables_avm_sale, :comparables_cdom_sale, :sale_output_pre_tax_roi_amount, :sale_output_pre_tax_roi_ratio, :sale_output_renovation_expense, :comparables_avm_rental, :comparables_cdom_rental, :rental_income_monthly_gross_rent, :rental_output_cap_rate, :rental_output_pre_tax_roi_amount, :rental_output_pre_tax_roi_ratio, :rental_output_renovation_expense, :rental_resale_output_investor_cap_rate, :rental_resale_output_pre_tax_roi_amount, :rental_resale_output_pre_tax_roi_ratio, :seller_finance_output_cap_rate, :seller_finance_output_pre_tax_roi_amount, :seller_finance_output_pre_tax_roi_ratio, :comparables_avm_build, :comparables_cdom_build, :build_financing_total_build_cost, :build_output_pre_tax_roi_amount, :build_output_pre_tax_roi_ratio,  :elementary_school_rating, :middle_school_rating, :high_school_rating, :avg_school_rating]
  SEARCH_STATS_OPERATORS = [:min, :max]
  GEOMAP_FIELDS = [:id, :mls_number, :status, :tax_id, :address, :street_address, :city, :zip, :subdivision, :subdivision_section_number, :county, :state, :beds, :bathrooms, :square_feet, :year_built, :list_price, :sold_lease_price, :category, :sold_price, :cdom]
  EXPORT_FIELDS = %w{mls_number status tax_id street_address city zip subdivision subdivision_section_number county state beds bathrooms square_feet year_built list_price sold_lease_price category rental_class rental_type single_family_type owner_name owner_street_address owner_city owner_state owner_zip_code owner_zip_4 mortgagee deed_date last_sale_date sold_price dom cdom listing_date closed_date lot_square_feet pool_private selling_for_lot_value garage_capacity geo_market_area disclosures lock_box_code key_map_page listing_agent_phone listing_agent_name realtor_remarks owner_estimated_loan_balance owner_equity_ratio purchase_price adj_sale_price adj_sale_price_psf renovation_avg_rehab comparables_avm_sale comparables_cdom_sale renovation_sale_rehab sale_output_max_purchase_amount sale_output_max_purchase_ratio sale_output_pre_tax_roe_ratio sale_output_pre_tax_roi_amount sale_output_pre_tax_roi_ratio sale_output_renovation_expense comparables_avm_rental renovation_rental_rehab rental_income_monthly_gross_rent rental_output_current_avm comparables_cdom_rental rental_output_cap_rate rental_output_ltv rental_output_max_purchase_amount rental_output_max_purchase_ratio rental_output_pre_tax_roe rental_output_pre_tax_roi_amount rental_output_pre_tax_roi_ratio rental_output_renovation_expense comparables_avm_rental_resale renovation_rental_resale_cost rental_resale_income_investor_cap_rate rental_resale_output_investor_cap_rate rental_resale_output_investor_ltv comparables_avm_rental_sale renovation_rental_hold_cost rental_resale_output_max_purchase_amount rental_resale_output_max_purchase_ratio rental_resale_output_pre_tax_roe_ratio rental_resale_output_pre_tax_roi_amount rental_resale_output_pre_tax_roi_ratio comparables_avm_seller_finance seller_finance_output_cap_rate seller_finance_output_max_purchase_amount seller_finance_output_max_purchase_ratio seller_finance_output_pre_tax_roe_ratio seller_finance_output_pre_tax_roi_amount renovation_seller_finance_cost seller_finance_output_pre_tax_roi_ratio comparables_avm_build comparables_cdom_build renovation_build_cost build_financing_total_build_cost build_output_max_build_amount build_output_max_build_ratio build_output_max_purchase_amount build_output_max_purchase_ratio build_output_pre_tax_roe_ratio build_output_pre_tax_roi_amount build_output_pre_tax_roi_ratio comparables_avm_wholesale comparables_avm_listing comparables_avm_listing_partnership}
  COMPS_REPORT_FIELDS = %w{tax_id street_address zip}
  IMPORT_FIELDS = %w{mls_number tax_id street_address zip_code county city list_price subdivision subdivision_section_number beds bathrooms square_feet year_built sold_lease_price lot_square_feet legal_subdivision pool_private category rental_class rental_type single_family_type}

  scope :latest,    order('updated_at DESC')
  scope :saved_for, proc {|user| joins(:proformas).where('proformas.user_id' => user.id) }

  scope :since,     proc {|date| where('updated_at > date') unless date.nil? }
  scope :similar,   proc {|tax_id, street_address, zip_code| where(["tax_id = ? OR (street_address = ? AND zip_code = ?)", tax_id, street_address, zip_code]).desc(:listing_date) }
  scope :similar_desc_id,   proc {|tax_id, street_address, zip_code| where(["tax_id = ? OR (street_address = ? AND zip_code = ?)", tax_id, street_address, zip_code]).desc(:id) }
  scope :order_by,  proc {|field, ascending| order("#{attr_sql_name(field)} #{ascending ? 'ASC' : 'DESC'}") if field.to_sym.in?(all_attributes_with_delegates) }
  scope :comps,     proc {|p, query| where(query)  }
  scope :on_market, where("status in (:statuses)", statuses: ['active', 'pending'])
  scope :only_active,  where("status in (:statuses)", statuses: ['active'])
  scope :active_and_pending,  where("status in (:statuses)", statuses: ['active','pending'])
  scope :on_market_with_dom_update, where("(status in (:statuses)) and (dom_updated_at is not null) and ((listing_date is not null) and ((cdom is not null) and (dom is not null)))", statuses: ['active', 'pending'])
  scope :my_prop_for, proc {|user| joins(:my_props).joins("LEFT JOIN proformas ON proformas.property_id = properties.id and proformas.user_id = #{user.id}").joins("LEFT OUTER JOIN users ON users.id = properties.rep").where('my_props.user_id' => user.id) }
  scope :client_my_prop_for, proc {|user| joins(:my_props).joins("LEFT JOIN client_proformas ON client_proformas.property_id = properties.id and client_proformas.user_id = #{user.id}").joins("LEFT OUTER JOIN users ON users.id = properties.rep").where('my_props.user_id' => user.id) }

  geocoded_by :address

  belongs_to :market
  belongs_to :detail, class_name: 'PropertyDetail', dependent: :destroy
  belongs_to :property_rep, class_name: 'User', foreign_key: :rep
  belongs_to :property_tag, class_name: 'PropertyTag', foreign_key: :tag
  # has_one :school_district, through: :detail

  has_many :assigned_tags, dependent: :destroy
  # has_one  :user_assigned_tag, class_name: 'AssignedTag', foreign_key: :property_id, conditions: proc { "assigned_tags.user_id = #{@@class_user_id}" }
  has_one  :user_assigned_tag, class_name: 'AssignedTag', foreign_key: :property_id, conditions: proc { ["assigned_tags.user_id = ?", @@class_user_id] }
  # has_one :user_assigned_tag, class_name: 'AssignedTag', foreign_key: :property_id,  -> (o){ where "assigned_tags.user_id = '#{o.current_user_id}'"}

  has_many :proformas, dependent: :destroy
  has_many :user_proformas, class_name: 'Proforma', conditions: ["user_id IS NOT NULL"]
  has_one :proforma, conditions: { user_id: nil }
  has_many :notes, dependent: :destroy
  has_many :my_props, foreign_key: :property_id, dependent: :destroy
  has_one  :user_proforma, class_name: 'Proforma', foreign_key: :property_id, conditions: proc { "proformas.user_id = #{@@class_user_id}" }

  has_many :client_proformas, dependent: :destroy
  has_many :user_client_proformas, class_name: 'ClientProforma', conditions: ["user_id IS NOT NULL"]
  has_one :client_proforma, conditions: { user_id: nil }
  has_one  :user_client_proforma, class_name: 'ClientProforma', foreign_key: :property_id, conditions: proc { "client_proformas.user_id = #{@@class_user_id}" }
  accepts_nested_attributes_for :user_client_proforma,:notes,:detail,:user_proforma

  attr_config do |c|
    c._overrides :serialize
    # Delegates
    c._delegate Proforma, :proforma
    c._delegate PropertyDetail, :detail
    # c._delegate ClientProforma, :client_proforma
    # Raw fields
    c.beds :integer
    c.category  :enum, in: %w{single_family townhouse_condo lots rental multi_family commercial mid_hi_rise_condo ranch_acerage}
    c.cdom :integer
    c.city :string, inflect: :titleize
    c.closed_date :date
    c.county :string, inflect: :titleize
    c.deed_date :date
    c.disclosures :string
    c.dom :integer
    c.elementary_school :string
    c.elementary_school_rating :integer
    c.expiration_date :date
    c.garage_capacity :string
    c.geo_market_area :string
    c.high_school :string
    c.high_school_rating :integer
    c.key_map_page :string
    c.last_sale_date :date
    c.legal_subdivision :string, inflect: :titleize
    c.list_price :money
    c.listing_agent_phone :phone
    c.listing_agent_name :string
    c.listing_date :date
    c.lock_box_code :string
    c.location_key :string
    c.location_name :string
    c.lot_square_feet :integer
    c.middle_school :string
    c.middle_school_rating :integer
    c.mls_area_key :string
    c.mls_area_name :string
    c.mls_number :string  #, format: /^\d{5,9}+$/
    c.mortgagee :string
    c.owner_estimated_loan_balance :money
    c.owner_street_address :string
    c.owner_city :string
    c.owner_name :string, inflect: :titleize
    c.owner_state :state
    c.owner_zip_code :string
    c.owner_zip_4 :string
    c.pending_date :date
    c.pool_private :boolean
    c.realtor_remarks :text
    c.rental_class :enum, in: %w{mobile_home single_family townhouse_condo lots rental multi_family commercial mid_hi_rise_condo ranch_acerage manufactured}
    c.rental_type :enum, in: %w{apartment condo duplex efficiency fourplex freestanding historic other patio_home town_house triplex}
    c.selling_for_lot_value boolean: false
    c.single_family_type :enum, in: %w{duplex freestanding historic manufactured patio multiplex townhome single_family other}
    c.sold_price :money
    c.sold_lease_price :money
    c.square_feet :integer
    c.street_direction :string
    c.street_name :string, inflect: :titleize
    c.street_number :integer
    c.state :state
    c.status  :enum, in: %w{off active expired pending sold terminated withdrawn cancelled}
    c.subdivision :string, inflect: :titleize
    c.subdivision_section_number :string
    c.tax_amount :money
    c.tax_id :string, parser: proc {|s|      
      (s.squeeze.length > 1 && s.upcase.first != 'R') ? s.gsub(/[\A\d]+/, '').slice(0,13) : s
    }
    c.tax_market_value :money
    c.tax_rate :percent
    c.tax_value :money
    c.tax_year :year
    c.termination_date :date
    c.year_built :year
    c.zip_4 :string, format: /\A\d{4}\z/
    c.zip_code! :string, format: /\A\d{5}\z/
    c.withdrawn_date :date
    # calculations
    c.sale_price :money,            calc: proc {|m| (m.category == 'rental' ? m.sold_lease_price : m.sold_price) if m.status == 'sold' }
    c.adj_sale_price :money,        calc: proc {|m| m.sale_price - (Math.sum(m.seller_paid_to_buyer_closing_costs, m.seller_repair_credits) || 0) }
    c.adj_sale_price_psf :money,    calc: proc {|m| m.adj_sale_price / m.square_feet }
    c.list_price_psf :money,        calc: proc {|m| (m.list_price / m.square_feet) }
    c.sale_price_psf :money,        calc: proc {|m| (m.sale_price / m.square_feet) }
    c.bathrooms :decimal,           calc: proc {|m| m.bathrooms_full + ((m.bathrooms_half || 0) * 0.5.to_d) }
    c.street_address :string,       calc: proc {|m| [m.street_number, m.street_direction, m.street_name].reject(&:blank?).join(' ') unless m.street_name.blank? }
    c.zip :string,                  calc: proc {|m| [m.zip_code, m.zip_4].reject(&:blank?).join('-') unless m.zip_code.blank? }
    c.city_state_zip :string,       calc: proc {|m| [m.city, [m.state, m.zip].reject(&:blank?).join(' ')].reject(&:blank?).join(', ') }
    c.address :string,              calc: proc {|m| [m.street_address, m.city_state_zip].join(', ') unless m.street_address.blank? || m.city_state_zip.blank? }
    c.owner_zip :string,            calc: proc {|m| [m.owner_zip_code, m.owner_zip_4].reject(&:blank?).join('-') unless m.owner_zip_code.blank? }
    c.owner_city_state_zip :string, calc: proc {|m| [m.owner_city, [m.owner_state, m.owner_zip].reject(&:blank?).join(' ')].compact.join(', ') }, transient: true
    c.mls_area :string,             calc: proc {|m| [mls_area_key, mls_area_name].reject(&:blank?).join(' - ') }, transient: true
    c.location :string,             calc: proc {|m| [location_key, location_name].reject(&:blank?).join(' - ') }, transient: true
    # others (generated via APIs)
    c.latitude :decimal
    c.longitude :decimal
    c.zillow_id :string
    c.zillow_sale_estimate :money
    c.zillow_rental_estimate :money
    c.zillow_updated_at :datetime
    c.pshark_id :string
    c.pshark_updated_at :datetime
    c.pshark_county :string
    # comps
    c.comps_rental_stats :serialize
    c.comps_sale_stats :serialize
    c.comps_build_stats :serialize
    c.comps_updated_at :datetime
    # data
    c.data_source :string
    c.data_updated_at :datetime

    c.dom_updated_at :datetime
    c.auto_updatable boolean: false

    c.rep                 :user,          depend: "User"
    c.user_updated_at     :datetime
    # c.tag                 :tag,       depend: "PropertyTag"

    c.owner_phone  :string
    c.owner_email  :string
    c.offer_price  :money
    c.listing_agent_email :string

    c.school_updated_at :datetime

    c.avg_school_rating :decimal,    calc: proc {|m| Rails.logger.debug "school"; [m.elementary_school_rating,m.middle_school_rating,m.high_school_rating].compact.count > 0 ? ((m.elementary_school_rating || 0) + (m.middle_school_rating || 0) + (m.high_school_rating || 0)).to_f/[m.elementary_school_rating,m.middle_school_rating,m.high_school_rating].compact.count : 0   }
    c.walk_score :integer
    c.transit_score :integer
    c.zillow_tax_amount :money
    c.appreciation_rate :percent

    c.exemption :string
    c.land_use_code :string
    c.last_ad_price :money
    c.owner_occupied :boolean
    c.appraise_subdivision :string
    c.land_use :string

    c.school_district :string
    c.school_district_name :string

    c.mls_record_creation_date :datetime
    c.mls_record_modified :datetime

    c.mls_sub_area :string
    c.mls_sub_area_name :string

    c.mls_elementary_school :string
    c.mls_middle_school :string
    c.mls_high_school :string

    c.hoa_payment :money
    c.hoa_bill_freq :integer

    c.zillow_sale_only boolean: nil
    c.zillow_rent_only boolean: nil
    c.flood  :enum, in: %w{high moderate-low undetermined}
    c.near_commercial boolean: nil

    c.back_on_market :datetime
    c.basement_desc :string
    c.garage_attach :boolean
    # c.listing_broker_name :string
    c.previous_list_price :integer
    c.rooms :integer
    c.selling_agent_name :string
    c.selling_agent_phone :string
    c.township :string
    c.waterfront :boolean
    c.selling_agent_email :string

    c.mls_elementary_school_number :string
    c.mls_middle_school_number :string
    c.mls_high_school_number :string
    c.sub_category :string
    c.key_map_book :string
    c.mls_flood :string
    c.is_back_on_market :boolean
    c.price_reduction :boolean

    c.mls_source :string

    c.comp_grade :string
    c.new_siding_type :string
    c.new_style_of_building :string
    c.new_stories :decimal
    #
    c.water_sewer :string
    c.zillow_school_1 :string
    c.zillow_school_2 :string
    c.zillow_school_3 :string
    
    c.golf_course :boolean
    c.gated_community :boolean

    c.census_county :string          
    c.census_geoid :string  
    c.census_tract :string 
    c.census_state :string 
    c.census_block :string 
    
  end

  def school_district_full
    self.school_district_name rescue "-"
  end

  def subdivision_full
    [self.subdivision, subdivision_section_number].compact.join(" ")
  end

  def elementary_school_full
    rating = "(" + self.elementary_school_rating.to_s + ")" if self.elementary_school_rating
    full_desc = [self.elementary_school ? self.elementary_school : self.mls_elementary_school,rating].join(" ")
    if !full_desc.blank?
      full_desc
    else
      "-"
    end
  end

  def middle_school_full
    rating = "(" + self.middle_school_rating.to_s + ")" if self.middle_school_rating
    full_desc = [self.middle_school ? self.middle_school : self.mls_middle_school,rating].join(" ")
    if !full_desc.blank?
      full_desc
    else
      "-"
    end
  end

  def high_school_full
    rating = "(" + self.high_school_rating.to_s + ")" if self.high_school_rating
    full_desc = [self.high_school ? self.high_school : self.mls_high_school,rating].join(" ")
    if !full_desc.blank?
      full_desc
    else
      "-"
    end
  end

  def updateSchoolInfo(force=false)

    if force || (!self.school_updated_at) || (self.school_updated_at < 2.months.ago)
      zillow = Zillow.new
      attrs = zillow.lookup(self)
      puts "$%1 attrs #{attrs}"

      if (self.tax_amount.blank? || self.tax_amount == 0)
        if attrs.try(:[],:zillow_tax_amount) && attrs.try(:[],:zillow_tax_amount) > 0
          attrs = attrs.merge({:tax_amount => attrs[:zillow_tax_amount]})
        elsif (self.tax_rate) || (self.market.definition.general_tax_rate)
          atax_amount =  (!self.tax_rate.nil? ? self.tax_rate : self.market.definition.general_tax_rate) * (!self.tax_value.nil? ? self.tax_value : self.list_price) rescue nil
          attrs ||= {}
          attrs = attrs.merge({:tax_amount => atax_amount})
        end
      end
      puts "$atax #{attrs}"

      self.assign_attributes(attrs) unless !attrs
      if attrs.try(:[],:lot_description)
        self.detail.lot_description = attrs[:lot_description]
        self.detail.save(validate: false)
      end
    end
  end

  def updateSchoolInfo2(force=false)
    if force || (!self.school_updated_at) || (self.school_updated_at < 2.months.ago)
      zillow = Zillow.new
      attrs = zillow.lookup2(self)
      puts "$%1 attrs #{attrs}"
      self.assign_attributes(attrs) unless !attrs
      if attrs.try(:[],:lot_description)
        self.detail.lot_description = attrs[:lot_description]
        self.detail.save(validate: false)
      end
    end
  end

  def updateSchoolInfo3(force=false)
    if force || (!self.school_updated_at) || (self.school_updated_at < 2.months.ago)
      zillow = Zillow.new
      attrs = zillow.lookup3(self)
      puts "$%1 attrs #{attrs}"
      self.assign_attributes(attrs) unless !attrs
      if attrs.try(:[],:lot_description)
        self.detail.lot_description = attrs[:lot_description]
        self.detail.save(validate: false)
      end
    end
  end

  def updateSchoolInfo5(force=false)
    if force || (!self.school_updated_at) || (self.school_updated_at < 2.months.ago)
      zillow = Zillow.new
      attrs = zillow.lookup5(self)
      self.assign_attributes(attrs) unless !attrs

    end
  end

  def mapquest_geocode()
    begin
      agent = Mechanize.new  { |ag|
           ag.open_timeout = 26
           ag.read_timeout = 26
           ag.idle_timeout = 0.9
           ag.retry_change_requests = true
         }

      Delayed::Worker.logger.debug "~~ mapquest geocode ~~"
      service = Service.find_available(:mapquest_geocoding)

      if service
        Delayed::Worker.logger.debug "~~ setting proxy"
        proxy = Proxy.find_available if !Rails.env.development?
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        agent.user_agent = "ruby/net::http"
        agent.pluggable_parser.default = Mechanize::Page

        Delayed::Worker.logger.debug "~~ on search"


        page = agent.get("http://www.mapquestapi.com/geocoding/v1/address?&key=#{service.key}",
            location: self.address)

        service.increment_calls
        service.try(:rem_error_count)
        proxy.increment_calls if !Rails.env.development?
        proxy.rem_error_count if !Rails.env.development?

        Delayed::Worker.logger.debug "~~ on search #{page.body}"

        json = page.body
        dec = ActiveSupport::JSON.decode json

        lat = dec.try(:[],"results").try(:[],0).try(:[],"locations").try(:[],0).try(:[],"latLng").try(:[],"lat") rescue nil
        long = dec.try(:[],"results").try(:[],0).try(:[],"locations").try(:[],0).try(:[],"latLng").try(:[],"lng") rescue nil
        Delayed::Worker.logger.debug "obtained lat: #{lat} long: #{long}"

        self.latitude = lat
        self.longitude = long

        save(validate: false)
      end

     rescue Mechanize::ResponseCodeError => e

      Delayed::Worker.logger.debug "Error"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)

      case e.message
        when /403/ then
          Service.find_available(:mapquest_geocoding)
          service.try(:increment_calls)
          service.try(:mark_error)
          service.try(:add_error_count)
        when /400/ then
          proxy = Proxy.find_available
          proxy.try(:increment_calls)
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
      end


     rescue Exception =>  e

      Delayed::Worker.logger.debug "Error"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)

     end
  end

  def google_geocode()
    tries = 0
    agent = Mechanize.new  { |ag|
         ag.open_timeout = 26
         ag.read_timeout = 26
         ag.idle_timeout = 0.9
         ag.retry_change_requests = true
       }

    Delayed::Worker.logger.debug "~~ google geocode ~~"

    service = nil
    proxy = nil
      # ActiveRecord::Base.transaction do

    begin
      $lock.synchronize("x-key", expiry: 30.seconds, initial_wait: 0.1) do
        begin
          ActiveRecord::Base.transaction do

            for i in 0..2
              service = Service.find_available(:google_geocoding)
              Delayed::Worker.logger.debug "~~ setting proxy"
              if service
                proxy = Proxy.where(["(ip = ?) AND (error_count < ?)",service.key,16]).first # Proxy.find(service.key)
                service.try(:increment_calls)
              else
                break
              end

              if proxy
                proxy.try(:increment_calls) if !Rails.env.development?
                break
              else
                service.try(:mark_error)
                service.try(:set_max_error)
              end
            end

          end
        rescue Exception => e
          Delayed::Worker.logger.debug "~~ registering service account error #{e.message}"

        end
      end

      begin
        if (( service ) && (proxy ))
          agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
          agent.user_agent = "ruby/net::http"
          agent.pluggable_parser.default = Mechanize::Page

          Delayed::Worker.logger.debug "~~ on search"
          page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{self.address}")

          Delayed::Worker.logger.debug "~~ on search #{page.body}"

          json = page.body
          dec = ActiveSupport::JSON.decode json

          lat = dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat") rescue nil
          long = dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng") rescue nil
          status =  dec.try(:[],"status")

          if status.in?(['OVER_QUERY_LIMIT','REQUEST_DENIED','INVALID_REQUEST'])
            service.try(:suspend)

          end

          Delayed::Worker.logger.debug "obtained lat: #{lat} long: #{long}"
          Delayed::Worker.logger.debug  "status: #{status}"
          self.latitude = lat
          self.longitude = long

          save(validate: false)
        end
      rescue Mechanize::ResponseCodeError => e

        Delayed::Worker.logger.debug "Error x1: #{e.mssage}"
        raise e

      rescue Exception =>  e
        Delayed::Worker.logger.debug "Error x2: #{e.message} service: #{service.id}"
        raise e
      end

    rescue Mechanize::ResponseCodeError => e
      Delayed::Worker.logger.debug "Error db1: #{e.mssage}"
      case e.message
      when /403/ then
        # Service.find_available(:google_geocoding)

        service.try(:mark_error)
        service.try(:add_error_count)
      when /400/ then


        proxy.try(:mark_error)
        proxy.try(:add_error_count)
      end
    rescue Exception =>  e
      tries += 1

      if tries > 2

        service.try(:mark_error)
        service.try(:add_error_count)
        proxy.try(:mark_error) if !Rails.env.development?
        proxy.try(:add_error_count) if !Rails.env.development?
      end
      retry if tries <= 3
    end


  end
  
  def google_geocode2()
    tries = 0
    agent = Mechanize.new  { |ag|
         ag.open_timeout = 26
         ag.read_timeout = 26
         ag.idle_timeout = 0.9
         ag.retry_change_requests = true
       }

    Delayed::Worker.logger.debug "~~ google geocode ~~"

    service = nil
    proxy = nil
      # ActiveRecord::Base.transaction do

    begin
      $lock.synchronize("x-key", expiry: 30.seconds, initial_wait: 0.1) do
        begin
          ActiveRecord::Base.transaction do

            for i in 0..2
              skip_service = nil
              service = Service.find_available(:google_geocoding)
              Delayed::Worker.logger.debug "~~ setting proxy"
              if service
                proxy = Proxy.where(["(ip = ?) AND (error_count < ?)",service.key,16]).first # Proxy.find(service.key)
                service.try(:increment_calls)
              else
                skip_service = true
                
                # break
              end
              
              if !skip_service
                if proxy
                  proxy.try(:increment_calls) if !Rails.env.development?
                  break
                else
                  service.try(:mark_error)
                  service.try(:set_max_error)
                end
              else
                sleep 60
              end
            end

          end
        rescue Exception => e
          Delayed::Worker.logger.debug "~~ registering service account error #{e.message}"

        end
      end

      begin
        if (( service ) && (proxy ))
          agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
          agent.user_agent = "ruby/net::http"
          agent.pluggable_parser.default = Mechanize::Page

          Delayed::Worker.logger.debug "~~ on search"
          page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{self.address}")

          Delayed::Worker.logger.debug "~~ on search #{page.body}"

          json = page.body
          dec = ActiveSupport::JSON.decode json

          lat = dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat") rescue nil
          long = dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng") rescue nil
          status =  dec.try(:[],"status")

          if status.in?(['OVER_QUERY_LIMIT','REQUEST_DENIED','INVALID_REQUEST'])
            service.try(:suspend)

          end

          Delayed::Worker.logger.debug "obtained lat: #{lat} long: #{long}"
          Delayed::Worker.logger.debug  "status: #{status}"
          self.latitude = lat
          self.longitude = long

          save(validate: false)
        end
      rescue Mechanize::ResponseCodeError => e

        Delayed::Worker.logger.debug "Error x1: #{e.mssage}"
        raise e

      rescue Exception =>  e
        Delayed::Worker.logger.debug "Error x2: #{e.message} service: #{service.id}"
        raise e
      end

    rescue Mechanize::ResponseCodeError => e
      Delayed::Worker.logger.debug "Error db1: #{e.mssage}"
      case e.message
      when /403/ then
        # Service.find_available(:google_geocoding)

        service.try(:mark_error)
        service.try(:add_error_count)
      when /400/ then


        proxy.try(:mark_error)
        proxy.try(:add_error_count)
      end
    rescue Exception =>  e
      tries += 1

      if tries > 2

        service.try(:mark_error)
        service.try(:add_error_count)
        proxy.try(:mark_error) if !Rails.env.development?
        proxy.try(:add_error_count) if !Rails.env.development?
      end
      retry if tries <= 3
    end


  end

  def geocodio_geocode()
    begin
      agent = Mechanize.new  { |ag|
           ag.open_timeout = 26
           ag.read_timeout = 26
           ag.idle_timeout = 0.9
           ag.retry_change_requests = true
         }

      Delayed::Worker.logger.debug "~~ geocodio geocode ~~"
      service = Service.find_available(:geocodio_geocoding)

      if service
        Delayed::Worker.logger.debug "~~ setting proxy"
        proxy = Proxy.find_available if !Rails.env.development?
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        agent.user_agent = "ruby/net::http"
        agent.pluggable_parser.default = Mechanize::Page

        Delayed::Worker.logger.debug "~~ on search"


        # page = agent.get("http://www.mapquestapi.com/geocoding/v1/address?&key=#{service.key}",
            # location: self.address)
        page = agent.get("https://api.geocod.io/v1/geocode?q=#{self.address}&api_key=#{service.key}")

        service.increment_calls
        service.try(:rem_error_count)
        proxy.increment_calls if !Rails.env.development?
        proxy.rem_error_count if !Rails.env.development?

        Delayed::Worker.logger.debug "~~ on search #{page.body}"

        json = page.body
        dec = ActiveSupport::JSON.decode json

        # lat = dec.try(:[],"results").try(:[],0).try(:[],"locations").try(:[],0).try(:[],"latLng").try(:[],"lat") rescue nil
        # long = dec.try(:[],"results").try(:[],0).try(:[],"locations").try(:[],0).try(:[],"latLng").try(:[],"lng") rescue nil
        lat = dec.try(:[],"results").try(:[],0).try(:[],"location").try(:[],"lat")
        long = dec.try(:[],"results").try(:[],0).try(:[],"location").try(:[],"lng")

        Delayed::Worker.logger.debug "obtained lat: #{lat} long: #{long}"

        self.latitude = lat
        self.longitude = long

        save(validate: false)
      end

     rescue Mechanize::ResponseCodeError => e

      Delayed::Worker.logger.debug "Geocodio Error #{e.message}"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)

      case e.message
        when /403/ then
          Service.find_available(:geocodio_geocoding)
          service.try(:increment_calls)
          service.try(:mark_error)
          service.try(:add_error_count)
        when /400/ then
          proxy = Proxy.find_available
          proxy.try(:increment_calls)
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
      end


     rescue Exception =>  e

      Delayed::Worker.logger.debug "Geocodio Error #{e.message}"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)

     end
  end

  def osm_geocode()
    tries = 0
     page = nil

     begin
       agent = Mechanize.new  { |ag|
         ag.open_timeout = 26
         ag.read_timeout = 26
         ag.idle_timeout = 0.9
         ag.retry_change_requests = true
       }


       puts "!! setting proxy"
       Delayed::Worker.logger.debug "~~ setting proxy"
       proxy = Proxy.find_available if !Rails.env.development?
       agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
       agent.user_agent = "ruby/net::http"
       agent.pluggable_parser.default = Mechanize::Page

       Delayed::Worker.logger.debug "~~ on search"


       if !(self.county == "Fortbend")
         page = agent.get("http://nominatim.openstreetmap.org/search",
        street: self.street_address, county: self.county, format: "xml", polygon: "1", addressdetails: "1")
       else
         page = agent.get("http://nominatim.openstreetmap.org/search",
        street: self.street_address, county: "Fort Bend", format: "xml", polygon: "1", addressdetails: "1")
       end

       proxy.increment_calls if !Rails.env.development?
       proxy.rem_error_count if !Rails.env.development?


       lat = page.at("//searchresults/place[@type='residential']/@lat").value rescue nil
       lon = page.at("//searchresults/place[@type='residential']/@lon").value rescue nil



       if lat && lon
         # puts "~~ setting lat #{lat}"
         # puts "~~ setting long #{lon}"
         Delayed::Worker.logger.debug "~~ setting lat #{lat}"
         Delayed::Worker.logger.debug "~~ setting long #{lon}"

         self.latitude = lat
         self.longitude = lon

       # else
         # puts "google geocode"
#
         # mapquest_geocode
#
         # if self.latitude.blank? || self.longitude.blank?
            # self.force_geo_coordinates2
         # end
#
#
#
       end

     rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      tries += 1
      proxy = Proxy.find_available if !Rails.env.development?
      proxy.try(:increment_calls) if !Rails.env.development?
      Delayed::Worker.logger.debug "Trying again! #{e.message}" if tries <= 3
      if tries > 2
        proxy.try(:mark_error) if !Rails.env.development?
        proxy.try(:add_error_count) if !Rails.env.development?
      end
      retry if tries <= 3

      Delayed::Worker.logger.debug "No more attempt!"
      Delayed::Worker.logger.debug "Timeout in osm api call 1: #{e.message} #{e.class.to_s}"
      page = nil
     rescue Exception =>  e
      tries += 1
      proxy = Proxy.find_available if !Rails.env.development?
      proxy.try(:increment_calls) if !Rails.env.development?
      Delayed::Worker.logger.debug "Trying again! #{e.message}" if tries <= 3
      if tries > 2
        proxy.try(:mark_error) if !Rails.env.development?
        proxy.try(:add_error_count) if !Rails.env.development?
      end
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      Delayed::Worker.logger.debug "Timeout in osm api call 2: #{e.message} #{e.class.to_s}"
      page = nil
     end

     return true if !page.nil?

  end

   def osm_geocode2()
    tries = 0
     page = nil

     begin
       agent = Mechanize.new  { |ag|
         ag.open_timeout = 26
         ag.read_timeout = 26
         ag.idle_timeout = 0.9
         ag.retry_change_requests = true
       }


       puts "!! setting proxy"
       Delayed::Worker.logger.debug "~~ setting proxy"
       proxy = Proxy.find_available if !Rails.env.development?
       agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
       agent.user_agent = "ruby/net::http"
       agent.pluggable_parser.default = Mechanize::Page

       Delayed::Worker.logger.debug "~~ on search"


       if !(self.county == "Fortbend")
         page = agent.get("http://nominatim.openstreetmap.org/search",
        street: self.street_address, county: self.county, format: "xml", polygon: "1", addressdetails: "1")
       else
         page = agent.get("http://nominatim.openstreetmap.org/search",
        street: self.street_address, county: "Fort Bend", format: "xml", polygon: "1", addressdetails: "1")
       end

       proxy.increment_calls if !Rails.env.development?
       proxy.rem_error_count if !Rails.env.development?


       lat = page.at("//searchresults/place[@type='residential']/@lat").value rescue nil
       lon = page.at("//searchresults/place[@type='residential']/@lon").value rescue nil



       if lat && lon
         # puts "~~ setting lat #{lat}"
         # puts "~~ setting long #{lon}"
         Delayed::Worker.logger.debug "~~ setting lat #{lat}"
         Delayed::Worker.logger.debug "~~ setting long #{lon}"

         self.latitude = lat
         self.longitude = lon

       else
         puts "google geocode"
         mapquest_geocode

         if self.latitude.blank? || self.longitude.blank?
            self.force_geo_coordinates2
         end

       end

     rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      tries += 1
      proxy = Proxy.find_available if !Rails.env.development?
      proxy.try(:increment_calls) if !Rails.env.development?
      Delayed::Worker.logger.debug "Trying again! #{e.message}" if tries <= 3
      if tries > 2
        proxy.try(:mark_error) if !Rails.env.development?
        proxy.try(:add_error_count) if !Rails.env.development?
      end
      retry if tries <= 3

      Delayed::Worker.logger.debug "No more attempt!"
      Delayed::Worker.logger.debug "Timeout in osm api call 1: #{e.message} #{e.class.to_s}"
      page = nil
     rescue Exception =>  e
      tries += 1
      proxy = Proxy.find_available if !Rails.env.development?
      proxy.try(:increment_calls) if !Rails.env.development?
      Delayed::Worker.logger.debug "Trying again! #{e.message}" if tries <= 3
      if tries > 2
        proxy.try(:mark_error) if !Rails.env.development?
        proxy.try(:add_error_count) if !Rails.env.development?
      end
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      Delayed::Worker.logger.debug "Timeout in osm api call 2: #{e.message} #{e.class.to_s}"
      page = nil
     end

     return true if !page.nil?

  end


  def force_geo_coordinates2()

      service = Service.find_available(:google_geocoding)

      if service && !self.address.blank? && !self.geocoded?
        self.geocode
        service.increment_calls if self.geocoded?

        # puts "$A$ renova #{self.proforma.renovation_sale_listing_partnership_cost}"
        # save(validate: false)

        if self.latitude
          puts "self.latitude #{self.latitude} self.longitude #{self.longitude}"
          return true
        end
        # puts "$A$ renova #{self.proforma.renovation_sale_listing_partnership_cost}"
      end

      return false
    end

  def user_i
    1
  end

  def owner_equity_amount
    Rails.logger.debug "$A$ owner_equity_amount"
    self.proforma.owner_equity_amount
  end

  validates :mls_number, :status, presence: true, if: 'self.tax_id.blank?'
  validates :tax_id, presence: true, if: 'self.mls_number.blank?'
  validate :validate_rep, if: 'self.is_non_admin'

  def validate_rep

    if ((!self.rep.blank?) && (self.current_user_id != self.rep))
      Delayed::Worker.logger.debug "~~ cur: #{self.current_user_id} rep: #{self.rep}"
      errors.add(:rep, "non admin users are only allowed to assign himself as Rep")
    end

 end



  alias proforma_without_build proforma
  def proforma

    @proforma ||= proforma_without_build || build_proforma
    Rails.logger.debug "%%% build proforma #{@proforma}"
    # @proforma.restore if @proforma.new_record?
    @proforma
  end

  alias client_proforma_without_build client_proforma
  def client_proforma
    @client_proforma ||= client_proforma_without_build || build_client_proforma
    Rails.logger.debug "%%% build client proforma #{@proforma}"
    # @proforma.restore if @proforma.new_record?
    @client_proforma
  end

  alias detail_without_build detail
  def detail
    @detail ||= detail_without_build || build_detail
  end

  def to_s
    self.mls_number
  end

  def mls_url
    self.market.mls_provider.mls_url_for(self)
  end

  def tax_url
    self.market.mls_provider.tax_url_for(self)
  end

  def deed_url
    self.market.mls_provider.deed_url_for(self)
  end

  def zillow_url
    "http://www.zillow.com/homedetails/0/#{self.zillow_id}_zpid/" unless self.zillow_id.blank?
  end

  def pshark_url
    "http://www.propertyshark.com/mason/Reports2/showsection.html?propkey=#{self.pshark_id}" unless self.pshark_id.blank?
  end

  def history
    # self.market.properties.where(["properties.id <> ? AND (tax_id = ? OR address LIKE ?)", self.id, self.tax_id, self.address]).desc(:listing_date)

    tax_pids = Property.where(["market_id = ? and (properties.id <> ? AND (tax_id = ?))",self.market.id,self.id,self.tax_id]).map(&:id)
    addr_pids = Property.where(["market_id = ? and (properties.id <> ?  and (address LIKE ?))",self.market.id,self.id,self.address + '%']).map(&:id) if self.address

    Property.where(["id in (?) OR id in (?)",tax_pids,addr_pids]).desc(:listing_date)

  end

  def interpolate_sql(query)
    values = []
    query = query.gsub(/\#\{[a-z0-9_]+\}/) do |s|
      values << self.send(s[2..-2]); '?'
    end
    ret_val = values.unshift(query)
    return ret_val
  end

  def interpolate_sql7(query)
    values = []
    query = query.gsub(/\#\{[a-z0-9_]+\}/) do |s|
      if s == '#{subdivision}'

        subdivision = self.send(s[2..-2])
        subdivision_components = subdivision.match(/(.*?)\s*(\(*(PH|SEC)\s*[\d#]+\)*)/i).captures rescue nil
        if ((subdivision_components) && (subdivision_components.length > 0))
          subdivision = subdivision_components[0]
        end
        values << subdivision; '?'
      else
        Delayed::Worker.logger.debug "~s:#{s}"
        values << self.send(s[2..-2]); '?'
      end

    end
    ret_val = values.unshift(query)
    return ret_val
  end

  def self.interpolate_sql(queryset)
    values = []
    query = queryset.first

    i = 1
    query = query.gsub(/\?/) do |s|
      values << queryset[i]; i += 1; '?'
    end
    ret_val = values.unshift(query)
    return ret_val
  end

  def comparablesx(category, stats=false, force=false)

    tries = 0
    result = nil

    begin


      Delayed::Worker.logger.debug "start comparable"
      # query = self.market.send("#{category}_comp_query_for", self)
      # Delayed::Worker.logger.debug "~~ #{category} query: #{query}"
#
      if !stats
        result = self.market.properties.where(["id in (:ids)",ids: self.send("comps_#{category}_stats")[:comps]])
        Delayed::Worker.logger.debug "property check return collection"
        return result if !self.send("comps_#{category}_stats")[:comps].nil? # if result.length > 0
      end
      Delayed::Worker.logger.debug "self.id #{self.id}"
      # Delayed::Worker.logger.debug "~~ Prop Comps Query:  #{self.market.properties.comps(self, query).where("id <> ?", self.id).to_sql}"
      # Delayed::Worker.logger.debug "~~ query: #{query}"
      # result = self.market.properties.comps(self, query).where("id <> ?", self.id)
      # ----
      # test_result = result.group(:status).having("status = ?","sold").select("count(*) AS count") rescue nil
      # s_count =  test_result.map(&:count) if test_result && test_result.length > 0
#
      conds = []
      front_cond = nil
      back_cond = nil
      test_result = nil
      s_count = 0


      case category
      when :rental

         case self.market.id
          when 2
            conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[5] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '
            conds[9] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 3
            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_sub_area is not null  and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '
            conds[2] =  ' (mls_area_key is not null and mls_sub_area is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and (mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school})) '

            conds[3] =  ' (mls_area_key is not null and mls_sub_area is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}  and mls_high_school = #{mls_high_school}) '

            conds[4] =  ' (mls_area_key is not null and mls_sub_area is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}) '

            conds[5] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and ( mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}  )   )    '

            conds[7] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and (  mls_high_school = #{mls_high_school}  )   )    '

            conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 1) '

          when 4

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 5,10

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 6

            conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

            # conds[1] = ' (neighborhood_number IS NOT NULL and neighborhood_number= #{neighborhood_number} ) '

            conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

            conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[6] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 7

            conds[0] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
            conds[2] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
            conds[3] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
            conds[4] = ' ((mls_area_key IS NOT NULL and mls_area_key = #{mls_area_key}) and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
            conds[5] = ' (zip IS NOT NULL and zip = #{zip} and  (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
            conds[6] = ' ((mls_area_key IS NOT NULL and mls_area_key = #{mls_area_key}) and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
            conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'
            conds[8] = ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 1) '

          else
            # Subdivision and subdivision_section_number
            conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

            conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
            conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
            conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
            conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
            conds[7] = ' (zip IS NOT NULL and zip = #{zip} and  (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
            conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
            conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'
            conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          end


         case self.market.id
          when 4

            if self.rental_type
              front_cond = ' (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                        AND (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)
                        AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                        AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                        AND (category = "rental")
                        AND (rental_type = #{rental_type})
                        AND
                        (
                           (status <> "sold")
                            OR
                           ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                        )
                        AND
                        (  '
            else
               front_cond = ' (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                      AND (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)
                      AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                      AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                      AND (category = "rental")
                      AND (rental_type is null)
                      AND
                      (
                         (status <> "sold")
                          OR
                         ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                      )
                      AND
                      (  '
            end
           else

              front_cond  =  ' (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                      AND (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)
                      AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                      AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                      AND (category = "rental")
                      AND
                      (
                         (status <> "sold")
                          OR
                         ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                      )
                      AND
                      (  '






          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
          conds.each_with_index do |cond,index|
            if self.market_id == 7
              interpol_queries[index] = interpolate_sql7 [front_cond,cond,back_cond].join("")
            else
              interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            end
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"
            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []
            # test_result = self.market.properties.comps(self, interpol_queries[index]).where("id <> ? and status = ?", self.id, "sold")
            # test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []

            # s_count =  test_result.map(&:count) if test_result && test_result.length > 0

            qres = (qres + test_result).uniq

            rres = (rres + query_results).uniq


            if self.market_id.in?([3,7])
              qres = self.market.properties.where(["(id in (?)) and (((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 1) or (latitude is null))",qres, self.latitude, self.longitude])
            else
              qres = self.market.properties.where(["(id in (?)) and (((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",qres, self.latitude, self.longitude])
            end



            acc_count = qres.length

            Delayed::Worker.logger.debug "~~ query result: #{index}   #{test_result.try(:length)}  "
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"

            last_index = index
            break if acc_count >= 5
          end

          Delayed::Worker.logger.debug "~~ first  "
          # result = self.market.properties.where(["(id in (?)) ",rres])
          if self.market_id.in?([3,7])
            result = self.market.properties.where(["(id in (?)) and (((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 1) or (latitude is null))",rres, self.latitude, self.longitude])
          else
            result = self.market.properties.where(["(id in (?)) and (((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])
          end
          Delayed::Worker.logger.debug "~~ last #{result}  "

      when :sale

          case self.market.id
            when 2
              conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
              conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
              conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                              mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

              conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

              conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                              and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


              conds[5] =  ' (mls_area_key is not null and  high_school is not null
                              and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

              conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

              conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

              conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

              conds[9] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

            when 3
              conds[0] = ' (1 = 1) '

            when 4

              conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
              conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                              mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

              conds[2] =  ' (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

              conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                              and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


              conds[4] =  ' (mls_area_key is not null and  high_school is not null
                              and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

              conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

              conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

              conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

              conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

            when 5,10

               conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
                conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                                mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

                conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

                conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                                and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


                conds[4] =  ' (mls_area_key is not null and  high_school is not null
                                and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

                conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

                conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

                conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

                conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

            when 6
                conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

                conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

                conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

                conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                                mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

                conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

                conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                                and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


                conds[6] =  ' (mls_area_key is not null and  high_school is not null
                                and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

                conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

                conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

                conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

                conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

            when 7

                conds[0] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
                conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
                conds[2] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
                conds[3] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
                conds[4] = ' ((mls_area_key IS NOT NULL and mls_area_key = #{mls_area_key}) and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
                conds[5] = ' (zip IS NOT NULL and zip = #{zip} and  (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
                conds[6] = ' ((mls_area_key IS NOT NULL and mls_area_key = #{mls_area_key}) and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
                conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'
                conds[8] = ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 1) '

            else
              conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

              conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
              conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
              conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
              conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
              conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
              conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
              conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
              conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
              conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'

              conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          end

          case  self.market.id
            when 4

              if self.single_family_type
                front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                           AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                           AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                           AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                           AND (category = #{category})
                           AND (single_family_type = #{single_family_type})
                           AND
                           (
                              (status <> "sold")
                              OR
                              ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                           )
                           AND
                           ( '
               else
                  front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                           AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                           AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                           AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                           AND (category = #{category})
                           AND (single_family_type is null)
                           AND
                           (
                              (status <> "sold")
                              OR
                              ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                           )
                           AND
                           ( '
               end

             when 3

                 front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                            AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                            AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                            AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                            AND (category = #{category})
                            AND
                            (
                                (status <> "sold")
                              OR
                                ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                            )
                            AND
                            ('



             else

                front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                         AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                         AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                         AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                         AND (category = #{category})
                         AND
                         (
                            (status <> "sold")
                            OR
                            ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                         )
                         AND
                         ( '


          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
          conds.each_with_index do |cond,index|
            if self.market_id == 7
              interpol_queries[index] = interpolate_sql7 [front_cond,cond,back_cond].join("")
            else
              interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            end
            Delayed::Worker.logger.debug "~~ output: #{interpol_queries[index]}"
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"

            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?

            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []

            qres = (qres + test_result).uniq

            rres = (rres + query_results).uniq



            if self.market_id == 7
              qres = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 1) or (latitude is null))",qres, self.latitude, self.longitude])
            elsif self.market_id == 3
              qres = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 1.25) or (latitude is null))",qres, self.latitude, self.longitude])
            else
              qres = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",qres, self.latitude, self.longitude])
            end

            acc_count = qres.length
            Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
            last_index = index
            break if acc_count >= 5

            # last_index = index
            # break if acc_count >= 5
          end


           # result = self.market.properties.where(["(id in (?))",rres])
           if self.market_id == 7
             result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 1) or (latitude is null))",rres, self.latitude, self.longitude])
           elsif self.market_id == 3
             result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 1.25) or (latitude is null))",rres, self.latitude, self.longitude])
           else
             result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])
           end

      when :build
         case self.market.id
          when 2

            conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[5] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            # conds[9] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 3
            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_sub_area is not null  and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '
            conds[2] =  ' (mls_area_key is not null and mls_sub_area is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and (mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school})) '

            conds[3] =  ' (mls_area_key is not null and mls_sub_area is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}  and mls_high_school = #{mls_high_school}) '

            conds[4] =  ' (mls_area_key is not null and mls_sub_area is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}) '

            conds[5] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and ( mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}  )   )    '

            conds[6] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and (  mls_high_school = #{mls_high_school}  )   )    '

            # conds[7] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 4

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            # conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 5,10

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 6

            conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

            # conds[1] = ' (neighborhood_number IS NOT NULL and neighborhood_number= #{neighborhood_number} ) '

            conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

            conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[6] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            # conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '


          else

            conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

            conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
            conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
            conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
            conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
            conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
            conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
            conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'
            # conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '


          end

          case self.market.id
          when 2,3,4,5,6,10
              front_cond  =  ' (year_built >= Year(CURDATE())-3)
                         AND (category = "single_family")
                         AND (single_family_type = "freestanding")
                         AND
                         (
                            (status <> "sold")
                            OR
                            ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                         )
                         AND
                         (  '



            else
              front_cond  =  ' (year_built >= Year(CURDATE())-3)
                       AND (category = "single_family")
                       AND
                       (
                          (status <> "sold")
                          OR
                          ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                       )
                       AND
                       (  '



          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
           conds.each_with_index do |cond,index|
            interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"
            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []
            Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
            qres = (qres + test_result).uniq
            acc_count = qres.length
            rres = (rres + query_results).uniq
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
            last_index = index
            break if acc_count >= 5
          end


          # result = self.market.properties.where(["(id in (?))",rres])
          result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])

      end

      if stats

        comps = []
        if result
         Delayed::Worker.logger.debug "assign property comparable"
         comps = result.map do |e| e.id end rescue []
        end

         Delayed::Worker.logger.debug "after assign property comparable"
        fields = COMP_STATS_QUERY.last + [:count]

        Delayed::Worker.logger.debug "field ret"
        # Delayed::Worker.logger.debug "~~ COMP_STATS_QUERY: #{COMP_STATS_QUERY}"
        # Delayed::Worker.logger.debug "~~ result group: #{result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).to_sql}"
        result = result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).inject({}) do |r, item|
          fields.each do |field|
            r["#{item.status}_#{field}"] = item[field]
          end
          r
        end

        Delayed::Worker.logger.debug "group"

        for status in ::Property::STATUSES
          result["#{status}_count"] ||= 0
        end
        result = result.with_indifferent_access
        sold_sale_price_psf_min = result[:sold_sale_price_psf_min]
        sold_sale_price_psf_max = result[:sold_sale_price_psf_max]
        sold_sale_price_psf_mean = result[:sold_sale_price_psf_mean]
        sold_sale_price_psf_avg = result[:sold_sale_price_psf_avg]
        sold_sale_price_min = result[:sold_sale_price_min]
        sold_sale_price_max = result[:sold_sale_price_max]
        sold_sale_price_mean = result[:sold_sale_price_mean]
        sold_sale_price_avg = result[:sold_sale_price_avg]
        sold_cdom_mean = result[:sold_cdom_mean]
        sold_square_feet_mean = result[:sold_square_feet_mean]
        active_square_feet_mean = result[:active_square_feet_mean]
        result[:comps] = comps


        case category.to_sym
          when  :rental then
            self.zillow_rent_only = false
            result[:zestimate] = self.send("zillow_#{category}_estimate")  rescue nil
            result[:zestimate_psf] = result[:zestimate] / self.square_feet rescue nil
            zestimate = result[:zestimate].nil? ? 0: result[:zestimate]
            zestimate_psf = result[:zestimate_psf].nil? ? 0: result[:zestimate_psf]

            Delayed::Worker.logger.debug "zill_cat_estimate: #{result[:zestimate]}"
            Delayed::Worker.logger.debug "result sold count  #{result[:sold_count]} "
            Delayed::Worker.logger.debug "sold sale price psf mean nil:  #{sold_sale_price_psf_mean} "


            if (result[:sold_count] >= 1)
              result[:level1] = sold_sale_price_min rescue nil
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],(sold_sale_price_min + sold_sale_price_mean)/2].max rescue nil
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],[sold_sale_price_mean, sold_sale_price_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],[sold_sale_price_mean, sold_sale_price_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],(2*sold_sale_price_mean + sold_sale_price_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],sold_sale_price_max].max rescue nil
            elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
#
              result[:level1] =  zestimate  rescue nil
              result[:level2] =  zestimate  rescue nil
              result[:level3] =  zestimate rescue nil
              result[:level4] =  zestimate  rescue nil
              result[:level5] =  zestimate  rescue nil
              result[:level6] =  zestimate  rescue nil
              self.zillow_rent_only = true

            else

              result[:level1] =  0  rescue nil
              result[:level2] =  0  rescue nil
              result[:level3] =  0  rescue nil
              result[:level4] =  0  rescue nil
              result[:level5] =  0  rescue nil
              result[:level6] =  0  rescue nil
            end

            # if (result[:sold_count] >= 3)
#
              # Delayed::Worker.logger.debug "rental sold count >= 3 "
              # result[:level1] =  sold_sale_price_min rescue nil
              # result[:level2] =  [result[:level1].nil? ? 0: result[:level1], (sold_sale_price_min + sold_sale_price_mean)/2].max  rescue nil
              # result[:level3] =  [result[:level2].nil? ? 0: result[:level2], sold_sale_price_mean ].max  rescue nil
              # result[:level4] =  [result[:level3].nil? ? 0: result[:level3], (sold_sale_price_mean*2 + sold_sale_price_max)/3].max  rescue nil
              # result[:level5] =  [result[:level4].nil? ? 0: result[:level4], (sold_sale_price_mean + sold_sale_price_max)/2].max  rescue nil
              # result[:level6] =  [result[:level5].nil? ? 0: result[:level5], sold_sale_price_max].max rescue nil
#
            # elsif ((result[:sold_count] >= 1 && result[:sold_count] <= 2 ) && (![nil,0].include?(zestimate)))
#
              # Delayed::Worker.logger.debug "rental sold count < 3 and zestimate has value #{result[:zestimate]}"
              # result[:level1] =  (result[:zestimate] + sold_sale_price_min)/2   rescue nil
              # result[:level2] =  [result[:level1].nil? ? 0: result[:level1], (zestimate + sold_sale_price_min + sold_sale_price_mean)/3].max   rescue nil
              # result[:level3] =  [result[:level2].nil? ? 0: result[:level2], (zestimate + sold_sale_price_mean)/2].max   rescue nil
              # result[:level4] =  [result[:level3].nil? ? 0: result[:level3], [zestimate, (sold_sale_price_mean*2 + sold_sale_price_max + zestimate)/4  ].max ].max   rescue nil
              # result[:level5] =  [result[:level4].nil? ? 0: result[:level4], [zestimate, (zestimate + sold_sale_price_max*2 + sold_sale_price_mean)/4 ].max ].max rescue nil
              # result[:level6] =  [result[:level5].nil? ? 0: result[:level5], [zestimate, (sold_sale_price_max*2 + zestimate)/3].max].max   rescue nil
#
            # elsif ((result[:sold_count] >= 1 && result[:sold_count] <= 2 ) && ([nil,0].include?(zestimate)))
#
              # Delayed::Worker.logger.debug "fall under rental < 3 and zestimate has no value #{result[:zestimate]}"
              # result[:level1] =  sold_sale_price_min   rescue nil
              # result[:level2] = [result[:level1].nil? ? 0: result[:level1], (sold_sale_price_min + sold_sale_price_mean)/2].max   rescue nil
              # result[:level3] = [result[:level2].nil? ? 0: result[:level2], sold_sale_price_mean].max   rescue nil
              # result[:level4] = [result[:level3].nil? ? 0: result[:level3], (sold_sale_price_mean*2 + sold_sale_price_max)/3].max  rescue nil
              # result[:level5] = [result[:level4].nil? ? 0: result[:level4], (sold_sale_price_max + sold_sale_price_mean)/2].max  rescue nil
              # result[:level6] = [result[:level5].nil? ? 0: result[:level5], sold_sale_price_max].max  rescue nil
#
            # elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
#
              # result[:level1] =  zestimate  rescue nil
              # result[:level2] =  zestimate  rescue nil
              # result[:level3] =  zestimate rescue nil
              # result[:level4] =  zestimate  rescue nil
              # result[:level5] =  zestimate  rescue nil
              # result[:level6] =  zestimate  rescue nil
#
            # else
#
              # result[:level1] =  0  rescue nil
              # result[:level2] =  0  rescue nil
              # result[:level3] =  0  rescue nil
              # result[:level4] =  0  rescue nil
              # result[:level5] =  0  rescue nil
              # result[:level6] =  0  rescue nil
#
            # end
            sold_cdom_mean = self.cdom
            sold_sale_price_psf_mean = (sold_sale_price_psf_min + sold_sale_price_psf_max) / 2.to_d rescue nil

          when :sale then
            self.zillow_sale_only = false
            result[:zestimate] = self.send("zillow_#{category}_estimate") rescue nil
            result[:zestimate_psf] = result[:zestimate] / self.square_feet rescue nil
            zestimate = result[:zestimate].nil? ? 0: result[:zestimate]
            zestimate_psf = result[:zestimate_psf].nil? ? 0: result[:zestimate_psf]

            if (result[:sold_count] >= 1)
              Delayed::Worker.logger.debug "@@sold count >= 1"
              # Delayed::Worker.logger.debug "square feet: #{self.square_feet} sppsf_min: #{sold_sale_price_psf_min}"
              result[:level1] = self.square_feet * sold_sale_price_psf_min rescue nil

              # Delayed::Worker.logger.debug "square feet: #{self.square_feet * (sold_sale_price_psf_min.to_s.to_d + sold_sale_price_psf_mean.to_s.to_d)/2}"
              # Delayed::Worker.logger.debug "sale_price psf min nil?: #{sold_sale_price_psf_min.nil?}"
              # Delayed::Worker.logger.debug "sale_price psf mean nil?: #{sold_sale_price_psf_mean.nil?}"
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],self.square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil

              # Delayed::Worker.logger.debug "square feet: #{self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min}"
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],self.square_feet * (2*sold_sale_price_psf_mean + sold_sale_price_psf_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],self.square_feet * sold_sale_price_psf_max].reject(&:blank?).max rescue nil

            elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
              Delayed::Worker.logger.debug "@@sold count == 0"
              result[:level1] =  zestimate  rescue nil
              result[:level2] =  zestimate  rescue nil
              result[:level3] =  zestimate  rescue nil
              result[:level4] =  zestimate  rescue nil
              result[:level5] =  zestimate  rescue nil
              result[:level6] =  zestimate  rescue nil
              self.zillow_sale_only = true
            else
              Delayed::Worker.logger.debug "@@sold count else"
              result[:level1] =  0  rescue nil
              result[:level2] =  0  rescue nil
              result[:level3] =  0  rescue nil
              result[:level4] =  0  rescue nil
              result[:level5] =  0  rescue nil
              result[:level6] =  0  rescue nil
            end

            sold_cdom_mean = self.cdom

          when :build

            build_square_feet = [(sold_square_feet_mean.nil? ? 0 : sold_square_feet_mean), (active_square_feet_mean.nil? ?  0: active_square_feet_mean)].max
            # build_square_feet = self.square_feet
            sale_comp_level5_psf = self["comps_sale_stats"][:sold_sale_price_psf_max]
            Delayed::Worker.logger.debug "~~build_square_feet: #{build_square_feet} sale_comp_level5_psf #{sale_comp_level5_psf}"
            sale_comp_level5 = (sale_comp_level5_psf.blank? ? 0 : sale_comp_level5_psf)  * build_square_feet      # self["comps_sale_stats"][:level6]
            # (sale_comp_level5/build_square_feet) rescue nil

            if "Infinity" == "#{sale_comp_level5_psf}"
              sale_comp_level5_psf = 0
            end

            if (result[:sold_count] >= 2)
              result[:level1] = build_square_feet * sold_sale_price_psf_min rescue nil
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],build_square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],build_square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],build_square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],build_square_feet * (2*sold_sale_price_psf_mean + sold_sale_price_psf_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],build_square_feet * sold_sale_price_psf_max].max rescue nil
            elsif ((result[:sold_count] >= 1) && (![nil, 0].include?(sale_comp_level5)))
              result[:level1] =  ((build_square_feet * sold_sale_price_psf_min) +  (build_square_feet * sale_comp_level5_psf))/2 rescue nil
              result[:level2] =  [result[:level1].nil? ? 0: result[:level1],build_square_feet * (((sold_sale_price_psf_min + sold_sale_price_psf_mean) /2) + sale_comp_level5_psf)/2].max  rescue nil
              result[:level3] =  [result[:level2].nil? ? 0: result[:level2],([ sold_sale_price_psf_avg  ,  sold_sale_price_psf_mean  ].reject(&:blank?).min + sale_comp_level5_psf) /2 *  build_square_feet].max   rescue nil
              result[:level4] =  [result[:level3].nil? ? 0: result[:level3],([ sold_sale_price_psf_avg  ,  sold_sale_price_psf_mean  ].reject(&:blank?).max + sale_comp_level5_psf) /2 *  build_square_feet].max   rescue nil
              result[:level5] =  [result[:level4].nil? ? 0: result[:level4],(((sold_sale_price_psf_max + sold_sale_price_psf_mean*2)  /3)  +      sale_comp_level5_psf) /2  * build_square_feet].max rescue nil
              result[:level6] =  [result[:level5].nil? ? 0: result[:level5],(( build_square_feet *       sold_sale_price_psf_max ) +  (build_square_feet * sale_comp_level5_psf)  )/2].max    rescue nil
            elsif ((result[:sold_count] == 0) && (![nil, 0].include?(sale_comp_level5)))
              result[:level1] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level2] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level3] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level4] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level5] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level6] = build_square_feet * sale_comp_level5_psf rescue nil
            else
              result[:level1] = 0  rescue nil
              result[:level2] = 0  rescue nil
              result[:level3] = 0  rescue nil
              result[:level4] = 0  rescue nil
              result[:level5] = 0  rescue nil
              result[:level6] = 0  rescue nil
            end

            # if (result[:sold_count] >= 3)

            #   Delayed::Worker.logger.debug "build comp sold count >= 3"
            #   result[:level1] = build_square_feet * sold_sale_price_psf_min rescue nil
            #   result[:level2] = [result[:level1].nil? ? 0: result[:level1], build_square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil
            #   result[:level3] = [result[:level2].nil? ? 0: result[:level2], build_square_feet * sold_sale_price_psf_mean].max rescue nil
            #   result[:level4] = [result[:level3].nil? ? 0: result[:level3], build_square_feet * (sold_sale_price_psf_mean*2 + sold_sale_price_psf_max) / 3].max  rescue nil
            #   result[:level5] = [result[:level4].nil? ? 0: result[:level4], build_square_feet * (sold_sale_price_psf_mean  +  sold_sale_price_psf_max) / 2].max  rescue nil
            #   result[:level6] = [result[:level5].nil? ? 0: result[:level5], build_square_feet * sold_sale_price_psf_max].max   rescue nil

            # elsif ((result[:sold_count] >= 1 && result[:sold_count] <= 2 ) && (![nil, 0].include?(sale_comp_level5)))

            #   Delayed::Worker.logger.debug "build comp sold count between 1 and 2"
            #   result[:level1] = build_square_feet * (sale_comp_level5_psf + sold_sale_price_psf_min)/2 rescue nil
            #   result[:level2] = [result[:level1].nil? ? 0: result[:level1], build_square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean + sale_comp_level5_psf)/3].max rescue nil
            #   result[:level3] = [result[:level2].nil? ? 0: result[:level2], build_square_feet * (sold_sale_price_psf_mean + sale_comp_level5_psf)/2].max rescue nil
            #   result[:level4] = [result[:level3].nil? ? 0: result[:level3], [sale_comp_level5, build_square_feet * (sold_sale_price_psf_mean*2 + sold_sale_price_psf_max + sale_comp_level5_psf)/4].max].max rescue nil
            #   result[:level5] = [result[:level4].nil? ? 0: result[:level4], [sale_comp_level5, build_square_feet * (sale_comp_level5_psf + sold_sale_price_psf_max*2 + sold_sale_price_psf_mean)/4 ].max].max rescue nil
            #   result[:level6] = [result[:level5].nil? ? 0: result[:level5], [sale_comp_level5, build_square_feet * (sold_sale_price_psf_max*2 + sale_comp_level5_psf)/3  ].max ].max rescue nil

            # elsif ((result[:sold_count] >= 1 && result[:sold_count] <= 2 ) && ([nil, 0].include?(sale_comp_level5)))

            #   Delayed::Worker.logger.debug "build comp sold count between 1 and 2 with sale comp level 5 = nil"
            #   result[:level1] = build_square_feet * sold_sale_price_psf_min rescue nil
            #   result[:level2] = [result[:level1].nil? ? 0: result[:level1], build_square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil
            #   result[:level3] = [result[:level2].nil? ? 0: result[:level2], build_square_feet * sold_sale_price_psf_mean].max rescue nil
            #   result[:level4] = [result[:level3].nil? ? 0: result[:level3], build_square_feet * (sold_sale_price_psf_mean*2 + sold_sale_price_psf_max) / 3].max  rescue nil
            #   result[:level5] = [result[:level4].nil? ? 0: result[:level4], build_square_feet * (sold_sale_price_psf_mean  +  sold_sale_price_psf_max) / 2].max  rescue nil
            #   result[:level6] = [result[:level5].nil? ? 0: result[:level5], build_square_feet * sold_sale_price_psf_max].max   rescue nil

            # elsif ((result[:sold_count] == 0) && (![nil, 0].include?(sale_comp_level5)))

            #   result[:level1] = build_square_feet * sale_comp_level5_psf rescue nil
            #   result[:level2] = build_square_feet * sale_comp_level5_psf rescue nil
            #   result[:level3] = build_square_feet * sale_comp_level5_psf rescue nil
            #   result[:level4] = build_square_feet * sale_comp_level5_psf rescue nil
            #   result[:level5] = build_square_feet * sale_comp_level5_psf rescue nil
            #   result[:level6] = build_square_feet * sale_comp_level5_psf rescue nil

            # else

            #   result[:level1] = 0  rescue nil
            #   result[:level2] = 0  rescue nil
            #   result[:level3] = 0  rescue nil
            #   result[:level4] = 0  rescue nil
            #   result[:level5] = 0  rescue nil
            #   result[:level6] = 0  rescue nil

            # end

            result[:level5_sale_comp] = sale_comp_level5
            result[:level5_psf] = sale_comp_level5_psf
            result[:projected_square_feet] = build_square_feet

        end

        result[:adj_sale_price_psf_min] = sold_sale_price_psf_min
        result[:adj_sale_price_psf_max] = sold_sale_price_psf_max
        result[:adj_sale_price_psf_mean] = sold_sale_price_psf_mean
        result[:adj_cdom_mean] = sold_cdom_mean

        result[:timeframe] = 365
        result[:sold_homes] = result[:sold_count]
        result[:active_homes] = result[:active_count]
        result[:sold_homes_rate] = result[:timeframe] / result[:sold_homes] rescue nil
        result[:absorption_rate] = (result[:sold_homes_rate] * result[:active_homes]) / 30.to_d rescue nil
        Delayed::Worker.logger.debug "end comparable"

      end

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "backtrace #{e.backtrace}"
      Delayed::Worker.logger.debug "The cause is: #{e.message}"
      raise e
    end
    result

  end

  def comparables(category, stats=false, force=false)
    
    tries = 0
    result = nil
    new_result = nil

    begin


      Delayed::Worker.logger.debug "start comparable"
      # query = self.market.send("#{category}_comp_query_for", self)
      # Delayed::Worker.logger.debug "~~ #{category} query: #{query}"
#
      if !stats
        result = self.market.properties.where(["id in (:ids)",ids: self.send("comps_#{category}_stats")[:comps]])
        Delayed::Worker.logger.debug "property check return collection"
        return result if !self.send("comps_#{category}_stats")[:comps].nil? # if result.length > 0
      end
      Delayed::Worker.logger.debug "self.id #{self.id}"
      # Delayed::Worker.logger.debug "~~ Prop Comps Query:  #{self.market.properties.comps(self, query).where("id <> ?", self.id).to_sql}"
      # Delayed::Worker.logger.debug "~~ query: #{query}"
      # result = self.market.properties.comps(self, query).where("id <> ?", self.id)
      # ----
      # test_result = result.group(:status).having("status = ?","sold").select("count(*) AS count") rescue nil
      # s_count =  test_result.map(&:count) if test_result && test_result.length > 0
#
      conds = []
      tiers = []
      front_cond = nil
      back_cond = nil
      test_result = nil
      s_count = 0

      comps = []
      comp_grades = []
      comp_hash = {}

      case category
      when :rental

        tiers[0] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END)  ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                     
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )    ',                
                    case self.market.id
                    when 6
                    ' AND (legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision}) '  
                    else
                    ' AND (subdivision IS NOT NULL AND subdivision = #{subdivision}) '                   
                    end,
                    ' AND (beds = #{beds})
                    AND (bathrooms BETWEEN #{bathrooms}-0.5 AND #{bathrooms}+0.5) ',                   
                    case self.market.id
                    when 6
                    ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private IS NULL OR pool_private = false) '                     
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront IS NULL OR waterfront = false) '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course IS NULL OR golf_course = false) '                     
                    end,
                    '                    
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL) ' ].reject(&:blank?).join(' ')

        tiers[1] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10) AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id
                    when 6
                    ' AND (legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision}) '
                    else
                    ' AND (subdivision IS NOT NULL AND subdivision = #{subdivision}) '                    
                    end,                    
                    '  AND (beds = #{beds}) ',
                    ' AND (bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5) ',
                    case self.market.id
                    when 6
                    ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = false) '                     
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false) '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = false)  '                     
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')

        tiers[2] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10) AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                    ' AND ((legal_subdivision IS NULL) OR (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) ) )  '
                    else
                    ' AND ((subdivision IS NULL) OR (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) ) )  '
                    end,
                    '  AND (beds = #{beds})
                    AND (bathrooms BETWEEN #{bathrooms}-0.5 AND #{bathrooms}+0.5) ',
                    case self.market.id
                    when 6
                    ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,                    
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = false)  '                     
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false)  '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = false) '                     
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})   AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')

        tiers[3] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                    ' AND ((legal_subdivision IS NULL) OR (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) ) )   '
                    else
                    ' AND ((subdivision IS NULL) OR (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) ) )   '
                    end,
                    case self.market.id
                    when 10,11
                    ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END)   '
                    else
                    ' AND beds BETWEEN #{beds} - 1 AND #{beds} + 1 '
                    end,
                    case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = false)  '                     
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false)  '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '   AND (golf_course IS NULL OR golf_course = false)  '                     
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL)  ' ].reject(&:blank?).join(' ')      
          
        tiers[4] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")                        
                      AND 
                      (
                        (status <> "sold")
                        OR
                        (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                      ) ',
                      case self.market.id
                      when 6
                      ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )    '
                      else
                      ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )    '                      
                      end,                      
                      ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END)  ',
                     case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = false)  '                            
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false)  '                             
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = false)  '                    
                    end,
                    ' AND ((pool_private IS NOT NULL) AND (pool_private = #{pool_private}))                    
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')



        tiers[5] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                    ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )   '
                    else
                    ' AND ((subdivision IS NULL) OR (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) ) )   '
                    end,
                    ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) ',
                    case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end, 
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',                                       
                    
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = false)  '                    
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false)  '                
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = false)  '                  
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')

        tiers[6] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )
                    
                        
                    AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) ',
                    case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,                    
                    case self.market.id
                    when
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private IS NULL OR pool_private = false) '                     
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false)  '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = false)  '                      
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')
        
        tiers[7] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )
                    
                       
                   
                    AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) ',
                    case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end, 
                    case self.market.id
                    when 6 
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = false)  '                       
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = false)  '                   
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = false)  '                     
                    end,
                    '                    
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL)'].reject(&:blank?).join(' ')
        
        tiers[8] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                         THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))
                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.55 AND #{square_feet}*1.25) ',
                    ' AND (category = "rental")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                    ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )    '
                    else
                    ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )     '
                    end,
                    ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) ',
                    case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = 0)  '                      
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                     '  AND (golf_course IS NULL OR golf_course = 0)  '                     
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')
        
                 
        
        tiers[9] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 12 AND #{year_built} + 12)  AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.55 AND #{square_feet}*1.25) ',
                    ' AND (category = "rental")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )  
                     AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    )  ',
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = 0)  '                    
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = 0)  '                        
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')
        
        tiers[10] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',                                        
                     if !self.rental_class.blank?
                       ' AND (rental_class = #{rental_class})'
                     end ,
                    ' AND (square_feet BETWEEN #{square_feet}*0.60 AND #{square_feet}*1.20) ',
                    ' AND (category = "rental")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )                     
                    
                    AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) ',
                    case self.market.id
                    when 6
                      ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    else 
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      '  AND (pool_private IS NULL OR pool_private = 0)  '                      
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      '  AND (waterfront IS NULL OR waterfront = 0)  '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      '  AND (golf_course IS NULL OR golf_course = 0)  '                     
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL)  '
                    ].reject(&:blank?).join(' ')        
        
            
        
        

        interpol_queries = []
        query_results = []
        qres = []
        rres = []
        last_index = 0
        comp_grade_index = 0
        acc_count = 0
        all_count = 0
        # distances = [0.5,0.7,0.7,0.7,0.7,0.5,0.5,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,1,1,1]
        distances = [
                      1,
                      1,
                      0.5,
                      case self.market.id
                      when 6
                        0.7
                      else
                        0.5
                      end,
                      0,
                      1,
                      0.5,
                      1,
                      1.5,
                      1,
                      2
        ]
        tier_max_comps = [
                          1000,
                          1000,
                          3,
                          3,
                          3,
                          3,
                          3,
                          3,
                          3,
                          3,
                          3
        ]
        



          # if self.market_id != 3
        tiers.each_with_index do |tier,index|
          test_result = []
          # tier_max_comps  = 3

          
          if self.rental_class
            mtier = [tier,'((rental_class = #{rental_class}) AND (rental_class = "single_family"))'].join(" AND ")
          else 
            mtier = [tier, '((rental_class IS NULL) OR (rental_class = "single_family"))'].join(" AND ")
          end
          

          interpol_queries[index] =  if ( self.market_id == 7 )                                         
                                       interpolate_sql7 mtier
                                     else
                                       interpolate_sql mtier
                                     end
          Delayed::Worker.logger.debug "~~ output: #{interpol_queries[index]}"
          Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"


          query_results = self.market.properties.comps(self, interpol_queries[index]).select("id, status").where("id <> ?", self.id) if !interpol_queries[index].blank?

          if distances[index] > 0
            # query_results = self.market.properties.select("id, status").where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= #{distances[index]}) or (latitude is null))",query_results, self.latitude, self.longitude])
            query_results = self.market.properties.select("id, status, street_address, mls_source").where(["(id in (?)) and ( ( (latitude is not null) and ((( 69*haversine(?,?,latitude,longitude) > 0.25 AND 69*haversine(?,?,latitude,longitude) <=  #{distances[index]} ) AND (zip_code IS NOT NULL AND zip_code = #{self.zip_code})) or ( 69*haversine(?,?,latitude,longitude) <= 0.25  )  ) ) or (latitude is null) ) ",query_results.pluck(:id), self.latitude, self.longitude, self.latitude, self.longitude, self.latitude, self.longitude])
          else
            query_results = self.market.properties.select("id, status, street_address, mls_source").where(["(id in (?)) and ( ( (latitude is not null) and ((( 69*haversine(?,?,latitude,longitude) > 0.25 )  AND (zip_code IS NOT NULL AND zip_code = #{self.zip_code})) or ( 69*haversine(?,?,latitude,longitude) <= 0.25  ) ) )  or (latitude is null)  ) ",query_results.pluck(:id), self.latitude, self.longitude, self.latitude, self.longitude])
          end
          
          
          d_address = []
          if self.market.id.in?([10,11])
            query_results.group(:street_address).count(:id).each do |x,y| 
              if y > 1
                d_address << x
              end
            end

            if d_address  && d_address.length > 0
              d_address.each do |d|
                query_results.delete_if do |x|  (x.street_address == d)    &&  (x.mls_source == 'fmls')     end
              end
            end

          elsif  self.market.id == 6
            query_results.group(:street_address).count(:id).each do |x,y| 
              if y > 1
                d_address << x
              end
            end

            if d_address  && d_address.length > 0
              d_address.each do |d|
                query_results.delete_if do |x|  (x.street_address == d)    &&  (x.mls_source == 'flexmls')     end
              end
            end
          end 


          if acc_count < tier_max_comps[index]
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue [] 
          end
          Delayed::Worker.logger.debug ">>>rres: #{rres}"
          Delayed::Worker.logger.debug ">>>query_results: #{query_results}"
          # Delayed::Worker.logger.debug ">>>rres: #{rres.map(&:id)}"
          # Delayed::Worker.logger.debug ">>>query_results.map(&:id): #{query_results.map(&:id)}"


          # diffs = test_result.reject{|x| qres.include? x}

          if self.market.id.in?([10,11])
            rets_loop = nil
            c_deletes = []
            comp_grades = query_results.inject(comp_grades) do |rst,g| 
              pround_comps = rres

              Delayed::Worker.logger.debug ">>>check inclusion g.id: #{g.id}"
              indc = 0
              if !pround_comps.map(&:id).include?(g.id)

                Delayed::Worker.logger.debug ">>>after check inclusion g.id: #{g.id}"
                # ret_loop = true

                rets_loop = true

                Delayed::Worker.logger.debug ">>>rres : #{rres}"
                pround_comps.each do |elem| 
                  if !((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'gamls' && g.mls_source = 'fmls'))  
                    rets_loop = true
                    # delete

                  else
                    rets_loop = false   
                    break
                  end
                  if ((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'fmls' && g.mls_source = 'gamls'))  

                    Delayed::Worker.logger.debug ">>>delete loop: #{g.id} rets_loop #{rets_loop}"
                    idx = comps.find_index(elem.id)

                    if idx 
                      comps.delete_at(idx)
                      rst.delete_at(idx)
                      comp_hash.delete(elem.id)
                      c_deletes <<  elem.id
                    end
                    # delete
                    rets_loop = true
                    
                  else
                    rets_loop = true


                  end
                  Delayed::Worker.logger.debug ">>>check rets_loop: #{g.id} rets_loop #{rets_loop}"
                end


                if rets_loop
                  comps << g.id
                  case index
                  when 0..2
                    rst << 'A'
                    comp_hash[g.id] = 'A'
                  when 3..7
                    Delayed::Worker.logger.debug ">>>insert b: #{g.id}"
                    rst << 'B'
                    comp_hash[g.id] = 'B'
                  when 8..10
                    rst << 'C'   
                    comp_hash[g.id] = 'C'           
                  end
                end
              end
              rst 
            end 


            Delayed::Worker.logger.debug ">>>after check inclusion comp_hash: #{comp_hash}"
          elsif  self.market.id == 6

            rets_loop = nil
            c_deletes = []
            comp_grades = query_results.inject(comp_grades) do |rst,g| 
              pround_comps = rres

              Delayed::Worker.logger.debug ">>>check inclusion g.id: #{g.id}"
              indc = 0
              if !pround_comps.map(&:id).include?(g.id)

                Delayed::Worker.logger.debug ">>>after check inclusion g.id: #{g.id}"
                # ret_loop = true

                rets_loop = true

                Delayed::Worker.logger.debug ">>>rres : #{rres}"
                pround_comps.each do |elem| 
                  if !((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'mfrmls' && g.mls_source = 'flexmls'))  
                    rets_loop = true
                    # delete

                  else
                    rets_loop = false   
                    break
                  end
                  if ((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'flexmls' && g.mls_source = 'mfrmls'))  

                    Delayed::Worker.logger.debug ">>>delete loop: #{g.id} rets_loop #{rets_loop}"
                    idx = comps.find_index(elem.id)

                    if idx 
                      comps.delete_at(idx)
                      rst.delete_at(idx)
                      comp_hash.delete(elem.id)
                      c_deletes <<  elem.id
                    end
                    # delete
                    rets_loop = true
                    
                  else
                    rets_loop = true


                  end
                  Delayed::Worker.logger.debug ">>>check rets_loop: #{g.id} rets_loop #{rets_loop}"
                end


                if rets_loop
                  comps << g.id
                  case index
                  when 0..2
                    rst << 'A'
                    comp_hash[g.id] = 'A'
                  when 3..7
                    Delayed::Worker.logger.debug ">>>insert b: #{g.id}"
                    rst << 'B'
                    comp_hash[g.id] = 'B'
                  when 8..10
                    rst << 'C'   
                    comp_hash[g.id] = 'C'           
                  end
                end
              end
              rst 
            end 


            Delayed::Worker.logger.debug ">>>after check inclusion comp_hash: #{comp_hash}"

          else
            comp_grades = query_results.map(&:id).inject(comp_grades) do |rst,g| 
              if !rres.map(&:id).include?(g)
                comps << g
                case index
                when 0..2
                  rst << 'A'
                  comp_hash[g] = 'A'
                when 3..7
                  rst << 'B'
                  comp_hash[g] = 'B'
                when 8..10
                  rst << 'C'   
                  comp_hash[g] = 'C'           
                end
              end
              rst 
            end 
          end


          

          qres = (qres + test_result).uniq  if acc_count < tier_max_comps[index]

          rres = (rres + query_results).uniq

          Delayed::Worker.logger.debug ">>>last check qres: #{qres}"
          Delayed::Worker.logger.debug ">>>last check rres: #{rres}"
          if self.market.id.in? [6,10,11]
            qres.delete_if do |d| c_deletes.include?(d) end
            rres.delete_if do |d| c_deletes.include?(d.id) end  
          end


          # qres = (qres + test_result).uniq if acc_count < tier_max_comps[index]



          # rres = (rres + query_results).uniq

          
          
          # qres = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= #{distances[index]}) or (latitude is null))",qres, self.latitude, self.longitude])

          acc_count = qres.length
          all_count = rres.length
          Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
          Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
          last_index = index
          if (test_result.try(:length) > 0)
            comp_grade_index = index 
          end
          break if ((acc_count >= tier_max_comps[index]) && (all_count >= 8))
        end
        
        Delayed::Worker.logger.debug "~~ comp_grade_index = #{comp_grade_index}"
        if acc_count > 0 

          self.comp_grade = case comp_grade_index 
                          when 0..2
                            'A'
                          when 3..7
                            'B'
                          when 8..10
                            'C'                          
                          end
        else
          self.comp_grade = nil
        end

        cres = (qres + rres).uniq
        result = self.market.properties.where(["(id in (?)) ",cres])

      when :sale

        case self.market.id
        when 1
          conds[0] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[1] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[2] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[3] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[4] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[5] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[6] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[7] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[8] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
         
          conds[9] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[10] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[11] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          
          conds[12] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[13] = '(geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[14] = '(geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[15] = '(geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[16] = '(geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[17] = '(geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
        
        when 2
          conds[0] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})'
          conds[1] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})'
          conds[2] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})'
          conds[3] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})'
          conds[4] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})'
          conds[5] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
                      
          conds[6] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
          conds[7] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
          conds[8] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
        
          conds[9] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
          conds[10] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
          conds[11] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) '
          conds[12] = '(legal_subdivision IS NOT NULL AND legal_subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{legal_subdivision},' ',''), 1, 5),\'%\') ) '
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)'
        when 3
          conds[0] = '(1 = 1)'
          conds[1] = '(1 = 1)'
          conds[2] = '(1 = 1)'
          conds[3] = '(1 = 1)'
          conds[4] = '(1 = 1)'
          conds[5] = '(1 = 1)'
          conds[6] = '(1 = 1)'
          conds[7] = '(1 = 1)'
         
          conds[8] = '(1 = 1)'
          conds[9] = '(1 = 1)'
          conds[10] = '(1 = 1)'
          conds[11] = '(1 = 1)'
          conds[12] = '(1 = 1)'
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)'
          
              
        when 4

          conds[0] = '(1 = 1)'
          conds[1] = '(1 = 1)'
          conds[2] = '(1 = 1)'
          onds[3] = '(1 = 1)'
          conds[4] = '(1 = 1)'
          conds[5] = '(1 = 1)'
          conds[6] = '(1 = 1)'
          conds[7] = '(1 = 1)'
         
          conds[8] = '(1 = 1)'
          conds[9] = '(1 = 1)'
          conds[10] = '(1 = 1)'
          conds[11] = '(1 = 1)'
          conds[12] = '(1 = 1)'
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)'
          

        when 6
          conds[0] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[1] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[2] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[3] = '(subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND
                       legal_subdivision = #{legal_subdivision})
                       AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number})'
          conds[4] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[5] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[6] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[7] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          
          conds[8] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[9] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[10] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[11] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[12] = '(subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') )'
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)'
          

        when 7

          conds[0] = ' (subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})
                        AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number}) '
          conds[1] = ' (subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})
                        AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number}) '
          conds[2] = ' (subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})
                        AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number}) '
          conds[3] = ' (subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})
                        AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number}) '        
          conds[4] = ' (subdivision IS NOT NULL AND subdivision = #{subdivision}) OR (subdivision IS NULL AND legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})
                        AND (subdivision_section_number IS NOT NULL AND subdivision_section_number = #{subdivision_section_number}) '
          
          conds[5] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                       AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[6] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                       AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[7] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                       AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[8] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                       AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
         
          conds[9] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[10] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[11] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[12] = ' (subdivision IS NOT NULL AND subdivision LIKE CONCAT(SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5),\'%\') ) 
                      AND (geo_market_area IS NOT NULL AND geo_market_area = #{geo_market_area})'
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)'
        when 10
          conds[0] = '(1 = 1)'
          conds[1] = '(1 = 1)'
          conds[2] = '(1 = 1)'
          conds[3] = '(1 = 1)'
          conds[4] = '(1 = 1)'
          conds[5] = '(1 = 1)'
          conds[6] = '(1 = 1)'
          conds[7] = '(1 = 1)'
         
          conds[8] = '(1 = 1)'
          conds[9] = '(1 = 1)'
          conds[10] = '(1 = 1)'
          conds[11] = '(1 = 1)'
          conds[12] = '(1 = 1)'
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)' 
           
        else
          conds[0] = '(1 = 1)'
          conds[1] = '(1 = 1)'
          conds[2] = '(1 = 1)'
          conds[3] = '(1 = 1)'
          conds[4] = '(1 = 1)'
          conds[5] = '(1 = 1)'
          conds[6] = '(1 = 1)'
          conds[7] = '(1 = 1)'
         
          conds[8] = '(1 = 1)'
          conds[9] = '(1 = 1)'
          conds[10] = '(1 = 1)'
          conds[11] = '(1 = 1)'
          conds[12] = '(1 = 1)'
          conds[13] = '(1 = 1)'
          conds[14] = '(1 = 1)'
          conds[15] = '(1 = 1)'
          conds[16] = '(1 = 1)'
          conds[17] = '(1 = 1)'
          

        end

          
            
        tiers[0] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 5 AND #{year_built} + 5)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.75 , (self.square_feet ? self.square_feet : 0) - 600].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.25 , (self.square_feet ? self.square_feet : 0) + 600].min} ) ",
                    ' AND (category = "single_family")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 270 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                    ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )   '
                    else
                    ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )     '
                    end,                    
                    ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END)
                    AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) ',
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND  (IFNULL(#{garage_capacity},0)  = IFNULL(garage_capacity,0) + 2) ) OR ( (IFNULL(#{garage_capacity},0) >= 1) AND (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 ) ) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0)) ) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private = false or pool_private IS NULL)      '                        
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                      
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                        
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.75 AND #{lot_square_feet}*1.25) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.90 AND #{lot_square_feet}*1.10) '
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')

        tiers[1] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ( (year_built BETWEEN #{year_built} - 5 AND #{year_built} + 5)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.75 , (self.square_feet ? self.square_feet : 0) - 600].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.25 , (self.square_feet ? self.square_feet : 0) + 600].min} ) ",
                    ' AND (category = "single_family")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                    ' AND (legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})  '
                    else
                    ' AND (subdivision IS NOT NULL AND subdivision = #{subdivision})   '
                    end,    
                    ' AND (CASE
                      WHEN #{beds} <= 3
                        THEN  beds = #{beds}
                      ELSE
                          beds BETWEEN #{beds} - 1 AND #{beds} + 1

                    END)
                    AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                    END) ',
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND  (IFNULL(#{garage_capacity},0)  = IFNULL(garage_capacity,0) + 2) ) OR ( (IFNULL(#{garage_capacity},0) >= 1) AND (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 ) ) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0)) ) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    )  ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private = false or pool_private IS NULL)      '                         
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                        
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                     
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.75 AND #{lot_square_feet}*1.25) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.90 AND #{lot_square_feet}*1.10) '
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})   AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')

        
          
        tiers[2] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 7 AND #{year_built} + 7)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END)  ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.75 , (self.square_feet ? self.square_feet : 0) - 600].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.25 , (self.square_feet ? self.square_feet : 0) + 600].min} ) ",
                    ' AND (category = "single_family")                        
                      AND 
                      (
                        (status <> "sold")
                        OR
                        (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                      ) ',
                      case self.market.id                    
                      when 6
                      ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )    '
                      else
                      ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )     '
                      end,  
                      case self.market.id
                      when 6  
                        ' AND (CASE
                          WHEN #{beds} <= 3
                            THEN  beds = #{beds}
                          ELSE
                              beds BETWEEN #{beds} - 1 AND #{beds} + 1

                        END) '
                      when 10,11
                        ' AND (CASE
                          WHEN #{beds} <= 2
                            THEN  beds = #{beds}
                          ELSE
                              beds BETWEEN #{beds} - 1 AND #{beds} + 1

                        END) '
                      end,
                      case self.market.id 
                      when 6
                        ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                        END) '
                      when 10,11
                        ' AND (CASE
                          WHEN #{bathrooms} < 3
                            THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                          ELSE
                              bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                        END) '
                      end,
                      case self.market.id
                      when 6
                        ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND  (IFNULL(#{garage_capacity},0)  = IFNULL(garage_capacity,0) + 2) ) OR ( (IFNULL(#{garage_capacity},0) >= 1) AND (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 ) ) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0)) ) '
                      end, 
                      ' AND 
                      (                      
                         
                        (
                          (
                            (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                          ) 
                          AND
                          (
                            (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                            (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                            (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                            (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                            (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                            (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                               
                          )
                        )
                        OR
                        (
                          (
                            (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                          )
                          AND
                          (
                            (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                            (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                            (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                            (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                            (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                            (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                          )
                        )
                        OR
                        (
                          (
                            (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                          )
                          AND
                          (
                            (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                            (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                            (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                            (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                            (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                            (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                          )
                        )
                          
                        
                      ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private = false or pool_private IS NULL)      '                        
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                       
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                         
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.50 AND #{lot_square_feet}*1.50) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.75 AND #{lot_square_feet}*1.25) '
                    end,
                    ' 
                      AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL)   ' ].reject(&:blank?).join(' ')



        tiers[3] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ((year_built BETWEEN #{year_built} - 7 AND #{year_built} + 7)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.75 , (self.square_feet ? self.square_feet : 0) - 600].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.25 , (self.square_feet ? self.square_feet : 0) + 600].min} ) ",
                    ' AND (category = "single_family")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                      ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )      '
                    else
                      ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )      '
                    end,  
                    case self.market.id
                    when 6
                    ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    end,
                    case self.market.id 
                    when 6
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,
                    case self.market.id
                    when 6
                    ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND  (IFNULL(#{garage_capacity},0)  = IFNULL(garage_capacity,0) + 2) ) OR ( (IFNULL(#{garage_capacity},0) >= 1) AND (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 ) ) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0)) )  '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private = false or pool_private IS NULL)      '                       
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                        
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                        
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.50 AND #{lot_square_feet}*1.50) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.75 AND #{lot_square_feet}*1.25) '
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')

        tiers[4] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 7 AND #{year_built} + 7)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.75 , (self.square_feet ? self.square_feet : 0) - 600].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.25 , (self.square_feet ? self.square_feet : 0) + 600].min} ) ",
                    ' AND (category = "single_family")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id
                    when 6
                      ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    end,
                    case self.market.id 
                    when 6
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1.5 AND #{bathrooms} + 1.5

                      END) '
                    end,
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND  (IFNULL(#{garage_capacity},0)  = IFNULL(garage_capacity,0) + 2) ) OR ( (IFNULL(#{garage_capacity},0) >= 1) AND (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 ) ) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0)) )  '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                        )
                      )
                        
                      
                    )  ',
                    case                    
                    when (self.pool_private.nil? || self.pool_private == false)  
                      ' AND (pool_private = false or pool_private IS NULL)      '                        
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                       
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                        
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.50 AND #{lot_square_feet}*1.50) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.75 AND #{lot_square_feet}*1.25) '
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')
                
        
        tiers[5] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                         THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))
                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.65 , (self.square_feet ? self.square_feet : 0) - 650].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.35 , (self.square_feet ? self.square_feet : 0) + 650].min} ) ",
                    ' AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                      ' AND (legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})    '
                    else
                      ' AND (subdivision IS NOT NULL AND subdivision = #{subdivision})      '
                    end,  
                    case self.market.id
                    when 6
                      ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    end,
                    case self.market.id 
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    )  ',
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                         
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                         
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.40 AND #{lot_square_feet}*1.60) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.65 AND #{lot_square_feet}*1.35) '
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL)  ' ].reject(&:blank?).join(' ')
        
                 
        
        tiers[6] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ( (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.65 , (self.square_feet ? self.square_feet : 0) - 650].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.35 , (self.square_feet ? self.square_feet : 0) + 650].min} ) ",
                    ' AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                      ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )                        '
                    else
                      ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )                       '
                    end,                      
                    case self.market.id
                    when 6
                      ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    end,
                    case self.market.id
                    when 6
                    ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                        )
                      )
                        
                      
                    )  ',
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                          
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                         
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.40 AND #{lot_square_feet}*1.60) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.65 AND #{lot_square_feet}*1.35) '
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL) ' ].reject(&:blank?).join(' ')
        
        tiers[7] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ( (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.65 , (self.square_feet ? self.square_feet : 0) - 650].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.35 , (self.square_feet ? self.square_feet : 0) + 650].min} ) ",
                    ' AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    ) ',
                    case self.market.id                    
                    when 6
                      ' AND (legal_subdivision IS NOT NULL AND SUBSTRING(REPLACE(legal_subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{legal_subdivision},\' \',\'\'), 1, 5) )                 '
                    else
                      ' AND (subdivision IS NOT NULL AND SUBSTRING(REPLACE(subdivision,\' \',\'\'),1,5) = SUBSTRING(REPLACE(#{subdivision},\' \',\'\'), 1, 5) )    '
                    end,                        
                    case self.market.id
                    when 6
                      ' AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    when 10,11
                      ' AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) '
                    end,
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    ) ',
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)  
                      ' AND (waterfront = false or waterfront IS NULL)      '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false) 
                     ' AND (golf_course = false or golf_course IS NULL)      '                         
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.40 AND #{lot_square_feet}*1.60) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.65 AND #{lot_square_feet}*1.35) '
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL) ' ].reject(&:blank?).join(' ')        
        
        tiers[8] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.65 , (self.square_feet ? self.square_feet : 0) - 650].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.35 , (self.square_feet ? self.square_feet : 0) + 650].min} ) ",
                    ' AND (category = "single_family")
                    AND 
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )
                    
                   
                    AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) ',
                    case self.market.id
                    when 6
                     ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    ' AND 
                    (
                      (
                        (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)  AND (#{zillow_school_3} IS NOT NULL)
                      )
                      AND
                      (
                        (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2}  AND zillow_school_3 = #{zillow_school_3}) OR                      
                        (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2}  AND zillow_school_2 = #{zillow_school_3}) OR
                        (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2}  AND zillow_school_3 = #{zillow_school_3}) OR
                        (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2}  AND zillow_school_1 = #{zillow_school_3}) OR
                        (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2}  AND zillow_school_2 = #{zillow_school_3}) OR
                        (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2}  AND zillow_school_1 = #{zillow_school_3})
                      )
                    )  ',
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)                         
                      ' AND (waterfront = false or waterfront IS NULL)      '                     
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                    
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.40 AND #{lot_square_feet}*1.60) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.65 AND #{lot_square_feet}*1.35) '
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL) '].reject(&:blank?).join(' ')
        
       
        tiers[9] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN   ((year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END)  ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.65 , (self.square_feet ? self.square_feet : 0) - 650].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.35 , (self.square_feet ? self.square_feet : 0) + 650].min} ) ",
                    ' AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 270 DAY))
                    )                      
                                    
                    AND (CASE
                        WHEN #{beds} <= 3
                          THEN  beds = #{beds}
                        ELSE
                           beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END) 
                    AND (CASE
                        WHEN #{bathrooms} < 3
                          THEN  bathrooms BETWEEN #{bathrooms} - 0.5 AND #{bathrooms} + 0.5
                        ELSE
                            bathrooms BETWEEN #{bathrooms} - 1 AND #{bathrooms} + 1

                      END)  ',
                    case self.market.id 
                    when 6
                     ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,                    
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)                        
                      ' AND (waterfront = false or waterfront IS NULL)      '                      
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)  
                      ' AND (golf_course = false or golf_course IS NULL)      '                      
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.40 AND #{lot_square_feet}*1.60) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.65 AND #{lot_square_feet}*1.35) '
                    end,
                    ' 
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')
        
        tiers[10] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ( (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END) ',
                    " AND (square_feet BETWEEN #{[(self.square_feet ? self.square_feet : 0) * 0.65 , (self.square_feet ? self.square_feet : 0) - 650].max} AND  #{[(self.square_feet ? self.square_feet : 0) * 1.35 , (self.square_feet ? self.square_feet : 0) + 650].min} ) ",
                    ' AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )  ',                    
                    case self.market.id                    
                    when 6
                      ' AND (legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision})   '
                    else
                      ' AND (subdivision IS NOT NULL AND subdivision = #{subdivision})   '
                    end,                     
                    ' AND (CASE
                        WHEN #{beds} <= 2
                          THEN  beds = #{beds}
                        ELSE
                            beds BETWEEN #{beds} - 1 AND #{beds} + 1

                      END)  ',
                    case self.market.id
                    when 6
                      ' AND ((IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 1) OR (IFNULL(#{garage_capacity},0) >= 3 AND IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0) + 2)  OR (IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 1 OR IFNULL(garage_capacity,0) = IFNULL(#{garage_capacity},0) + 2) OR (IFNULL(#{garage_capacity},0) = IFNULL(garage_capacity,0))) '
                    end,
                    case                    
                    when (self.waterfront.nil? || self.waterfront == false)                        
                      ' AND (waterfront = false or waterfront IS NULL)      '                 
                    end,
                    case                    
                    when (self.golf_course.nil? || self.golf_course == false)
                      ' AND (golf_course = false or golf_course IS NULL)      '                                               
                    end,
                    case                    
                    when (self.gated_community.nil? || self.gated_community == false)  
                      ' AND (gated_community = false or gated_community IS NULL)      '                        
                    end,
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.40 AND #{lot_square_feet}*1.60) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0  && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.65 AND #{lot_square_feet}*1.35) '
                    end,
                    '
                    AND (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county}) AND (street_address is NOT NULL)  '].reject(&:blank?).join(' ')
        
        tiers[11] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ( (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END)
                    AND 
                      (                        
                        (square_feet BETWEEN #{square_feet}*0.65 AND #{square_feet}*1.35)
                        
                      )
                    AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )                                          
                                           
                    AND (beds BETWEEN #{beds} - 1 AND #{beds} + 1)

                    AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                        )
                      )
                        
                      
                    ) 
                    
                    
                    AND    (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})   AND (street_address is NOT NULL) ', 
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0 && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.25 AND #{lot_square_feet}*1.75) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0 && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.55 AND #{lot_square_feet}*1.45) '
                    end].reject(&:blank?).join(' ')
        
        tiers[12] = ['(CASE
                      WHEN #{year_built} >= YEAR(CURDATE()) - 2
                        THEN  year_built BETWEEN #{year_built} - 2 AND #{year_built} + 2
                      WHEN #{year_built} < YEAR(CURDATE()) - 2
                        THEN  ( (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)   AND (NOT ( year_built BETWEEN YEAR(CURDATE()) - 2 AND YEAR(CURDATE()))))

                    END)
                    AND  
                     (                        
                        (square_feet BETWEEN #{square_feet}*0.65 AND #{square_feet}*1.35)
                        
                      )
                    AND (category = "single_family")
                    AND
                    (
                      (status <> "sold")
                      OR
                      (closed_date >= (CURDATE() - INTERVAL 365 DAY))
                    )
                    AND (beds BETWEEN #{beds} - 2 AND #{beds} + 2)   
                    
                    AND 
                    (                      
                       
                      (
                        (
                          (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                        ) 
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                          (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                          (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                             
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                          (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                          (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                        )
                      )
                      OR
                      (
                        (
                          (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                        )
                        AND
                        (
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                          (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                          (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                        )
                      )
                        
                      
                    )

                    
                    AND  (city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})  AND (street_address is NOT NULL)  ',
                    if !self.lot_square_feet.blank? && self.lot_square_feet > 0 && self.lot_square_feet < 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.25 AND #{lot_square_feet}*1.75) '
                    elsif !self.lot_square_feet.blank? && self.lot_square_feet > 0 && self.lot_square_feet >= 53999
                       '  AND (lot_square_feet BETWEEN #{lot_square_feet}*0.55 AND #{lot_square_feet}*1.45) '
                    end].reject(&:blank?).join(' ')
        
       

        interpol_queries = []
        query_results = []
        qres = []
        rres = []
        last_index = 0
        comp_grade_index = 0
        acc_count = 0
        all_count = 0
        # distances = [0.5,0.7,0.7,0.7,0.7,0.5,0.5,0.7,0.7,0.7,0.7,0.7,0.7,0.7,0.7,1,1,1]
        distances = [ 
                      0.7,
                      0.7,
                      0.7,
                      0,
                      0.5,
                      0.7,
                      0.7,
                      0.7,
                      0.7,
                      0.7,
                      1,
                      1,
                      1
        ]
        tier_max_comps = [
                          1000,
                          5,
                          5,
                          1000,
                          5,
                          1000,
                          4,
                          1000,
                          1000,
                          4,
                          4,
                          4,
                          4
        ]
        

          # if self.market_id != 3
        tiers.each_with_index do |tier,index|
          test_result = []
          # tier_max_comps  = 8

          if self.market_id == 4
            if self.single_family_type
              mtier = [tier,'(single_family_type = #{single_family_type}) AND '].join(" ")
            else 
              mtier = [tier,'(single_family_type is null) AND '].join(" ")
            end
          else
            mtier = tier
          end


          interpol_queries[index] =  if ( self.market_id == 7 )                                         
                                       interpolate_sql7 mtier
                                     else
                                       interpolate_sql mtier
                                     end
          Delayed::Worker.logger.debug "~~ output: #{interpol_queries[index]}"
          Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"


          query_results = self.market.properties.comps(self, interpol_queries[index]).select("id, status").where("id <> ?", self.id) if !interpol_queries[index].blank?

          if (index != 9)
            if distances[index] > 0
              # query_results = self.market.properties.select("id, status").where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= #{distances[index]}) or (latitude is null))",query_results, self.latitude, self.longitude])
              query_results = self.market.properties.select("id, status, street_address, mls_source").where(["(id in (?)) and ( ( (latitude is not null) and ((( 69*haversine(?,?,latitude,longitude) > 0.25 AND 69*haversine(?,?,latitude,longitude) <=  #{distances[index]} ) AND (zip_code IS NOT NULL AND zip_code = #{self.zip_code})) or ( 69*haversine(?,?,latitude,longitude) <= 0.25  )  ) ) or (latitude is null) ) ",query_results.pluck(:id), self.latitude, self.longitude, self.latitude, self.longitude, self.latitude, self.longitude])
            else
              query_results = self.market.properties.select("id, status, street_address, mls_source").where(["(id in (?)) and ( ( (latitude is not null) and ((( 69*haversine(?,?,latitude,longitude) > 0.25 )  AND (zip_code IS NOT NULL AND zip_code = #{self.zip_code})) or ( 69*haversine(?,?,latitude,longitude) <= 0.25  ) ) )  or (latitude is null)  ) ",query_results.pluck(:id), self.latitude, self.longitude, self.latitude, self.longitude])
            end
          else
            sub_interpol_sql = '(

                                    ( 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 0.5 )
                                 
                                       and
                                 
                                 
                                        (                      
                                           
                                          (
                                            (
                                              (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                                            ) 
                                            AND
                                            (
                                              (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )   OR                      
                                              (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                                              (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                                              (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} )   OR
                                              (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} )   OR
                                              (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} )
                                                 
                                            )
                                          )
                                          OR
                                          (
                                            (
                                              (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                                            )
                                            AND
                                            (
                                              (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} )   OR                      
                                              (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                                              (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                                              (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} )   OR
                                              (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} )   OR
                                              (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} ) 
                                            )
                                          )
                                          OR
                                          (
                                            (
                                              (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                                            )
                                            AND
                                            (
                                              (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} )   OR                      
                                              (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                                              (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                                              (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} )   OR
                                              (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} )   OR
                                              (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} ) 
                                            )
                                          )
                                            
                                          
                                        )
                                 
                                   )  OR  (

                                      ( 69*haversine(#{latitude},#{longitude},latitude,longitude) > 0.5  and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 0.7  )
                                 
                                       and
                                 
                                 
                                        (                      
                       
                                          (
                                            (
                                              (#{zillow_school_1} IS NOT NULL) AND (#{zillow_school_2} IS NOT NULL)
                                            ) 
                                            AND
                                            (
                                              (zillow_school_1 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR                      
                                              (zillow_school_1 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                                              (zillow_school_2 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                                              (zillow_school_2 = #{zillow_school_1} AND zillow_school_3 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                                              (zillow_school_3 = #{zillow_school_1} AND zillow_school_1 = #{zillow_school_2} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_3},\'\'))   OR
                                              (zillow_school_3 = #{zillow_school_1} AND zillow_school_2 = #{zillow_school_2} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_3},\'\'))
                                                 
                                            )
                                          )
                                          OR
                                          (
                                            (
                                              (#{zillow_school_2} IS NOT NULL) AND (#{zillow_school_3} IS NOT NULL) 
                                            )
                                            AND
                                            (
                                              (zillow_school_1 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR                      
                                              (zillow_school_1 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                                              (zillow_school_2 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                                              (zillow_school_2 = #{zillow_school_2} AND zillow_school_3 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                                              (zillow_school_3 = #{zillow_school_2} AND zillow_school_1 = #{zillow_school_3} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_1},\'\'))   OR
                                              (zillow_school_3 = #{zillow_school_2} AND zillow_school_2 = #{zillow_school_3} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_1},\'\')) 
                                            )
                                          )
                                          OR
                                          (
                                            (
                                              (#{zillow_school_3} IS NOT NULL) AND (#{zillow_school_1} IS NOT NULL) 
                                            )
                                            AND
                                            (
                                              (zillow_school_1 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR                      
                                              (zillow_school_1 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                                              (zillow_school_2 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_3,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                                              (zillow_school_2 = #{zillow_school_3} AND zillow_school_3 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                                              (zillow_school_3 = #{zillow_school_3} AND zillow_school_1 = #{zillow_school_1} AND COALESCE(zillow_school_2,\'\') = COALESCE(#{zillow_school_2},\'\'))   OR
                                              (zillow_school_3 = #{zillow_school_3} AND zillow_school_2 = #{zillow_school_1} AND COALESCE(zillow_school_1,\'\') = COALESCE(#{zillow_school_2},\'\')) 
                                            )
                                          )
                                            
                                          
                                        ) 

                                   )'



            sub_interpol_sql = interpolate_sql sub_interpol_sql
            Delayed::Worker.logger.debug "sql: #{self.market.properties.select("id, status").where(["(id in (?)) and ( ( (latitude is not null) and ((( 69*haversine(?,?,latitude,longitude) > 0.25 AND 69*haversine(?,?,latitude,longitude) <=  #{distances[index]} ) AND (zip_code IS NOT NULL AND zip_code = #{self.zip_code})) or ( 69*haversine(?,?,latitude,longitude) <= 0.25  )  ) ) or (latitude is null) ) ",query_results, self.latitude, self.longitude, self.latitude, self.longitude, self.latitude, self.longitude]).where(sub_interpol_sql).to_sql}"
            query_results = self.market.properties.select("id, status, street_address, mls_source").where(["(id in (?)) and ( ( (latitude is not null) and ((( 69*haversine(?,?,latitude,longitude) > 0.25 AND 69*haversine(?,?,latitude,longitude) <=  #{distances[index]} ) AND (zip_code IS NOT NULL AND zip_code = #{self.zip_code})) or ( 69*haversine(?,?,latitude,longitude) <= 0.25  )  ) ) or (latitude is null) ) ",query_results.pluck(:id), self.latitude, self.longitude, self.latitude, self.longitude, self.latitude, self.longitude]).where(sub_interpol_sql)
          end

          d_address = []
          if self.market.id.in?([10,11])
            
            query_results.group(:street_address).count(:id).each do |x,y| 
              if y > 1
                d_address << x
              end
            end

            if d_address  && d_address.length > 0
              d_address.each do |d|
                query_results.delete_if do |x|  (x.street_address == d)    &&  (x.mls_source == 'fmls')     end
              end
            end
          elsif self.market.id == 6
            query_results.group(:street_address).count(:id).each do |x,y| 
              if y > 1
                d_address << x
              end
            end

            if d_address  && d_address.length > 0
              d_address.each do |d|
                query_results.delete_if do |x|  (x.street_address == d)    &&  (x.mls_source == 'flexmls')     end
              end
            end
          end 


          
          
          if acc_count <  tier_max_comps[index]
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue [] 
          end

          Delayed::Worker.logger.debug ">>>rres: #{rres.map(&:id)}"
          Delayed::Worker.logger.debug ">>>query_results.map(&:id): #{query_results.map(&:id)}"
          # diffs = test_result.reject{|x| qres.include? x}


          if self.market.id.in?([10,11])
            rets_loop = nil
            c_deletes = []
            comp_grades = query_results.inject(comp_grades) do |rst,g| 
              pround_comps = rres

              Delayed::Worker.logger.debug ">>>check inclusion g.id: #{g.id}"
              indc = 0
              if !pround_comps.map(&:id).include?(g.id)

                Delayed::Worker.logger.debug ">>>after check inclusion g.id: #{g.id}"
                # ret_loop = true

                rets_loop = true

                Delayed::Worker.logger.debug ">>>rres : #{rres}"
                pround_comps.each do |elem| 
                  if !((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'gamls' && g.mls_source = 'fmls'))  
                    rets_loop = true
                    # delete

                  else
                    rets_loop = false   
                    break
                  end
                  if ((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'fmls' && g.mls_source = 'gamls'))  

                    Delayed::Worker.logger.debug ">>>delete loop: #{g.id} rets_loop #{rets_loop}"
                    idx = comps.find_index(elem.id)

                    if idx 
                      comps.delete_at(idx)
                      rst.delete_at(idx)
                      comp_hash.delete(elem.id)
                      c_deletes <<  elem.id
                    end
                    # delete
                    rets_loop = true
                    
                  else
                    rets_loop = true


                  end
                  Delayed::Worker.logger.debug ">>>check rets_loop: #{g.id} rets_loop #{rets_loop}"
                end


                if rets_loop
                  comps << g.id
                  case index
                  when 0..1
                    rst << 'A'
                    comp_hash[g.id] = 'A'
                  when 2..4
                    rst << 'B'
                    comp_hash[g.id] = 'B'
                  when 5..10
                    rst << 'C'
                    comp_hash[g.id] = 'C'
                  when 11..12
                    rst << 'D'
                    comp_hash[g.id] = 'D'
                  end
                end
              end
              rst 
            end 


            Delayed::Worker.logger.debug ">>>after check inclusion comp_hash: #{comp_hash}"
          elsif  self.market.id == 6
            rets_loop = nil
            c_deletes = []
            comp_grades = query_results.inject(comp_grades) do |rst,g| 
              pround_comps = rres

              Delayed::Worker.logger.debug ">>>check inclusion g.id: #{g.id}"
              indc = 0
              if !pround_comps.map(&:id).include?(g.id)

                Delayed::Worker.logger.debug ">>>after check inclusion g.id: #{g.id}"
                # ret_loop = true

                rets_loop = true

                Delayed::Worker.logger.debug ">>>rres : #{rres}"
                pround_comps.each do |elem| 
                  if !((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'mfrmls' && g.mls_source = 'flexmls'))  
                    rets_loop = true
                    # delete

                  else
                    rets_loop = false   
                    break
                  end
                  if ((elem.street_address.to_s[0..7] == g.street_address.to_s[0..7]) && (elem.mls_source == 'flexmls' && g.mls_source = 'mfrmls'))  

                    Delayed::Worker.logger.debug ">>>delete loop: #{g.id} rets_loop #{rets_loop}"
                    idx = comps.find_index(elem.id)

                    if idx 
                      comps.delete_at(idx)
                      rst.delete_at(idx)
                      comp_hash.delete(elem.id)
                      c_deletes <<  elem.id
                    end
                    # delete
                    rets_loop = true
                    
                  else
                    rets_loop = true


                  end
                  Delayed::Worker.logger.debug ">>>check rets_loop: #{g.id} rets_loop #{rets_loop}"
                end


                if rets_loop
                  comps << g.id
                  case index
                  when 0..1
                    rst << 'A'
                    comp_hash[g.id] = 'A'
                  when 2..4
                    rst << 'B'
                    comp_hash[g.id] = 'B'
                  when 5..10
                    rst << 'C'
                    comp_hash[g.id] = 'C'
                  when 11..12
                    rst << 'D'
                    comp_hash[g.id] = 'D'
                  end
                end
              end
              rst 
            end 


            Delayed::Worker.logger.debug ">>>after check inclusion comp_hash: #{comp_hash}"
          else
            comp_grades = query_results.map(&:id).inject(comp_grades) do |rst,g| 
              if !rres.map(&:id).include?(g)
                comps << g
                case index
                when 0..1
                  rst << 'A'
                  comp_hash[g] = 'A'
                when 2..4
                  rst << 'B'
                  comp_hash[g] = 'B'
                when 5..10
                  rst << 'C'
                  comp_hash[g] = 'C'
                when 11..12
                  rst << 'D'
                  comp_hash[g] = 'D'
                end
              end
              rst 
            end 
          end

          # comp_grades = query_results.map(&:id).inject(comp_grades) do |rst,g| 
          #   if !rres.map(&:id).include?(g)

          #     comps << g
          #     case index
          #     when 0..1
          #       rst << 'A'
          #       comp_hash[g] = 'A'
          #     when 2..4
          #       rst << 'B'
          #       comp_hash[g] = 'B'
          #     when 5..10
          #       rst << 'C'
          #       comp_hash[g] = 'C'
          #     when 11..12
          #       rst << 'D'
          #       comp_hash[g] = 'D'
          #     end
          #   end
          #   rst 
          # end 


          qres = (qres + test_result).uniq if acc_count < tier_max_comps[index]



          rres = (rres + query_results).uniq

          
          Delayed::Worker.logger.debug ">>>last check qres: #{qres}"
          Delayed::Worker.logger.debug ">>>last check rres: #{rres}"
          if self.market.id.in? [6,10,11]
            qres.delete_if do |d| c_deletes.include?(d) end
            rres.delete_if do |d| c_deletes.include?(d.id) end  
          end
          
          # qres = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= #{distances[index]}) or (latitude is null))",qres, self.latitude, self.longitude])

          acc_count = qres.length
          all_count = rres.length
          Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
          Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
          Delayed::Worker.logger.debug "~~ comp hash: #{comp_hash}"
          last_index = index
          if (test_result.try(:length) > 0)
            comp_grade_index = index 
          end
          break if ((acc_count >= tier_max_comps[index]) && (all_count >= 8))
        end
        
        Delayed::Worker.logger.debug "~~ comp_grade_index = #{comp_grade_index}"
        if acc_count > 0 

          self.comp_grade = case comp_grade_index 
                          when 0..2
                            'A'
                          when 3..6
                            'B'
                          when 7..13
                            'C'
                          when 14..15
                            'D'
                          end
        else
          self.comp_grade = nil
        end

        cres = (qres + rres).uniq
        result = self.market.properties.where(["(id in (?)) ",cres])
        

      when :build
         case self.market.id
          when 2

            conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[5] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            # conds[9] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 3
            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_sub_area is not null  and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '
            conds[2] =  ' (mls_area_key is not null and mls_sub_area is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and (mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school})) '

            conds[3] =  ' (mls_area_key is not null and mls_sub_area is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}  and mls_high_school = #{mls_high_school}) '

            conds[4] =  ' (mls_area_key is not null and mls_sub_area is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}) '

            conds[5] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and ( mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}  )   )    '

            conds[6] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and (  mls_high_school = #{mls_high_school}  )   )    '

            # conds[7] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 4

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            # conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 5,10

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            conds[8] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '

          when 6

            conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

            # conds[1] = ' (neighborhood_number IS NOT NULL and neighborhood_number= #{neighborhood_number} ) '

            conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

            conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[6] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            # conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '


          else

            conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

            conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
            conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
            conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
            conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
            conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
            conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
            conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'
            # conds[10] =  ' ((latitude is not null) and 69*haversine(#{latitude},#{longitude},latitude,longitude) <= 2) '


          end

          case self.market.id
          when 2,3,4,5,6,10,11
              front_cond  =  ' (year_built >= Year(CURDATE())-3)
                         AND (category = "single_family")
                         AND (single_family_type = "freestanding")
                         AND
                         (
                            (status <> "sold")
                            OR
                            ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                         )
                         AND
                         (  '



            else
              front_cond  =  ' (year_built >= Year(CURDATE())-3)
                       AND (category = "single_family")
                       AND
                       (
                          (status <> "sold")
                          OR
                          ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                       )
                       AND
                       (  '



          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
           conds.each_with_index do |cond,index|
            interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"
            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []
            Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
            qres = (qres + test_result).uniq
            acc_count = qres.length
            rres = (rres + query_results).uniq
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
            last_index = index
            break if acc_count >= 5
          end


          # result = self.market.properties.where(["(id in (?))",rres])
          result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])

      end

      if stats

        avg_expanded_sale_price_psfs =  nil
        avg_moderate_sale_price_psfs =  nil
        avg_conservative_sale_price_psfs =  nil
        
        if result
         # Delayed::Worker.logger.debug "assign property comparable"
         # comps = result.map do |e| e.id end rescue []
        end

         Delayed::Worker.logger.debug "after assign property comparable"
        fields = COMP_STATS_QUERY.last + [:count]

        Delayed::Worker.logger.debug "field ret"
        # Delayed::Worker.logger.debug "~~ COMP_STATS_QUERY: #{COMP_STATS_QUERY}"
        # Delayed::Worker.logger.debug "~~ result group: #{result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).to_sql}"

        comp_hash_selected = comp_hash.select { |key,value| %{A B C}.include? value }
        Delayed::Worker.logger.debug "comp_hash_selected: #{comp_hash_selected}"
        comp_hash_keys = comp_hash_selected.keys

        selected_results = result.where(["(id in (?)) AND status = 'sold'",comp_hash_keys]).order("sale_price_psf DESC").to_a.map(&:serializable_hash).map(&:with_indifferent_access)
        Delayed::Worker.logger.debug "selected_results #{selected_results}"

        # ary_selected_results = selected_results.to_a.map(&:serializable_hash)
        # Delayed::Worker.logger.debug "ary_selected_results: #{ary_selected_results}"

        # expanded_results = ary_selected_results.select { |key,value| %{A B C D}.include? value }

        conservative_results = (selected_results.select do |e| comp_hash_selected[e["id"]].in?(%{A})  end) #.inject([]) do |rst,e| hsh = {}; e.each do |k,v| hsh[k] = v end; rst << hsh end
        moderate_results = (selected_results.select do |e| comp_hash_selected[e["id"]].in?(%{A B})  end) #.inject([]) do |rst,e| hsh = {}; e.each do |k,v| hsh[k] = v end; rst << hsh end
        expanded_results = (selected_results.select do |e| comp_hash_selected[e["id"]].in?(%{A B C})  end) #.inject([]) do |rst,e| hsh = {}; e.each do |k,v| hsh[k] = v end; rst << hsh end

        Delayed::Worker.logger.debug "conservative_results: #{conservative_results.length}"
        Delayed::Worker.logger.debug "moderate_results: #{moderate_results.length}"
        Delayed::Worker.logger.debug "expanded_results: #{expanded_results.length}"

        # Delayed::Worker.logger.debug "expanded_results[2] #{expanded_results[0]["address"]}"

        # exp_rs = expanded_results.clone
        # Delayed::Worker.logger.debug "exp_rs: #{exp_rs[0]["sale_price_psf"]}"

        # Delayed::Worker.logger.debug "c rs #{conservative_results[1].try(:[],"sale_price_psf")}" 
        # Delayed::Worker.logger.debug "e rs #{expanded_results[2].try(:[],"sale_price_psf")}"
        # Delayed::Worker.logger.debug "e rs #{expanded_results[1].try(:[],"sale_price_psf")}"
        
        # Delayed::Worker.logger.debug "expanded_2 #{expanded_2} #{expanded_2.class}"
        # Delayed::Worker.logger.debug "expanded_1 #{expanded_1} #{expanded_1.class}"
        expanded_results.each do |d|
          Delayed::Worker.logger.debug "expanded result: #{d["id"]} #{d["status"]} #{d["address"]} #{d["sale_price_psf"]} #{comp_hash_selected[d["id"]]}"
        end

        
        #=IF((LARGE(Q7:Q32,3)>((LARGE(Q7:Q32,2)*(85/100)))),(SUMIF(Q7:Q32,( ">="&LARGE(Q7:Q32,3)))/3),(SUMIF(Q7:Q32,( ">="&LARGE(Q7:Q32,2)))/2))

       
        if (expanded_results.length > 0)  &&  !(expanded_results.any? {|h| h[:sale_price_psf].blank?})  #
          

          if expanded_results.length >= 3 #
            expanded_2 = expanded_results[2].try(:[],"sale_price_psf")
            expanded_1 = expanded_results[1].try(:[],"sale_price_psf")
            if expanded_2 > (expanded_1 * 0.85)
              Delayed::Worker.logger.debug " 3 case"
              expanded_sale_price_psfs = (expanded_results[0,3].map do |d| d["sale_price_psf"] end)
              sum_expanded_sale_price_psfs = expanded_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
              avg_expanded_sale_price_psfs = sum_expanded_sale_price_psfs / 3
               Delayed::Worker.logger.debug "expanded_sum 1 = #{expanded_sale_price_psfs}"
               Delayed::Worker.logger.debug "expanded_sum 2 = #{sum_expanded_sale_price_psfs}"
               Delayed::Worker.logger.debug "expanded_sum 3 = #{avg_expanded_sale_price_psfs}"
            else
              Delayed::Worker.logger.debug " 2 case"
              expanded_sale_price_psfs = (expanded_results[0,2].map do |d| d["sale_price_psf"] end) 
              sum_expanded_sale_price_psfs = expanded_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
              avg_expanded_sale_price_psfs = sum_expanded_sale_price_psfs / 2
                  Delayed::Worker.logger.debug "expanded_sum 1 = #{expanded_sale_price_psfs}"
               Delayed::Worker.logger.debug "expanded_sum 2 = #{sum_expanded_sale_price_psfs}"
               Delayed::Worker.logger.debug "expanded_sum 3 = #{avg_expanded_sale_price_psfs}"
            end
          elsif expanded_results.length == 2  #
            expanded_sale_price_psfs = (expanded_results[0,2].map do |d| d["sale_price_psf"] end)
            sum_expanded_sale_price_psfs = expanded_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
            avg_expanded_sale_price_psfs = sum_expanded_sale_price_psfs / 2
          elsif expanded_results.length == 1  #
            expanded_sale_price_psfs = (expanded_results[0,1].map do |d| d["sale_price_psf"] end)
            sum_expanded_sale_price_psfs = expanded_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
            avg_expanded_sale_price_psfs = sum_expanded_sale_price_psfs 
          end
        end
        
        if moderate_results.length > 0  &&  !(moderate_results.any? {|h| h[:sale_price_psf].blank?})
          if moderate_results.length >= 3 #
            moderate_2 = moderate_results[2].try(:[],"sale_price_psf")
            moderate_1 = moderate_results[1].try(:[],"sale_price_psf")
            if moderate_2 > (moderate_1 * 0.85)
              Delayed::Worker.logger.debug " 3 case"
              moderate_sale_price_psfs = (moderate_results[0,3].map do |d| d["sale_price_psf"] end)
              sum_moderate_sale_price_psfs = moderate_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
              avg_moderate_sale_price_psfs = sum_moderate_sale_price_psfs / 3
               Delayed::Worker.logger.debug "moderate_sum 1 = #{moderate_sale_price_psfs}"
               Delayed::Worker.logger.debug "moderate_sum 2 = #{sum_moderate_sale_price_psfs}"
               Delayed::Worker.logger.debug "moderate_sum 3 = #{avg_moderate_sale_price_psfs}"
            else
              Delayed::Worker.logger.debug " 2 case"
              moderate_sale_price_psfs = (moderate_results[0,2].map do |d| d["sale_price_psf"] end) 
              sum_moderate_sale_price_psfs = moderate_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
              avg_moderate_sale_price_psfs = sum_moderate_sale_price_psfs / 2
                  Delayed::Worker.logger.debug "moderate_sum 1 = #{moderate_sale_price_psfs}"
               Delayed::Worker.logger.debug "moderate_sum 2 = #{sum_moderate_sale_price_psfs}"
               Delayed::Worker.logger.debug "moderate_sum 3 = #{avg_moderate_sale_price_psfs}"
            end
          elsif  moderate_results.length == 2 #
            moderate_sale_price_psfs = (moderate_results[0,2].map do |d| d["sale_price_psf"] end)
            sum_moderate_sale_price_psfs = moderate_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
            avg_moderate_sale_price_psfs = sum_moderate_sale_price_psfs / 2

          elsif  moderate_results.length == 1 #
            moderate_sale_price_psfs = (moderate_results[0,1].map do |d| d["sale_price_psf"] end)
            sum_moderate_sale_price_psfs = moderate_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
            avg_moderate_sale_price_psfs = sum_moderate_sale_price_psfs 
          end
          
        end

        if conservative_results.length > 0  &&  !(conservative_results.any? {|h| h[:sale_price_psf].blank?})
          if conservative_results.length >= 3 #
            conservative_2 = conservative_results[2].try(:[],"sale_price_psf")
            conservative_1 = conservative_results[1].try(:[],"sale_price_psf")
            if conservative_2 > (conservative_1 * 0.85)
              Delayed::Worker.logger.debug " 3 case"
              conservative_sale_price_psfs = (conservative_results[0,3].map do |d| d["sale_price_psf"] end)
              sum_conservative_sale_price_psfs = conservative_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
              avg_conservative_sale_price_psfs = sum_conservative_sale_price_psfs / 3
               Delayed::Worker.logger.debug "conservative_sum 1 = #{conservative_sale_price_psfs}"
               Delayed::Worker.logger.debug "conservative_sum 2 = #{sum_conservative_sale_price_psfs}"
               Delayed::Worker.logger.debug "conservative_sum 3 = #{avg_conservative_sale_price_psfs}"
            else
              Delayed::Worker.logger.debug " 2 case"
              conservative_sale_price_psfs = (conservative_results[0,2].map do |d| d["sale_price_psf"] end) 
              sum_conservative_sale_price_psfs = conservative_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
              avg_conservative_sale_price_psfs =  sum_conservative_sale_price_psfs / 2
                  Delayed::Worker.logger.debug "conservative_sum 1 = #{conservative_sale_price_psfs}"
               Delayed::Worker.logger.debug "conservative_sum 2 = #{sum_conservative_sale_price_psfs}"
               Delayed::Worker.logger.debug "conservative_sum 3 = #{avg_conservative_sale_price_psfs}"
            end
          elsif conservative_results.length == 2 #
            conservative_sale_price_psfs = (conservative_results[0,2].map do |d| d["sale_price_psf"] end)
            sum_conservative_sale_price_psfs = conservative_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
            avg_conservative_sale_price_psfs = sum_conservative_sale_price_psfs / 2
          elsif conservative_results.length == 1 #
            conservative_sale_price_psfs = (conservative_results[0,1].map do |d| d["sale_price_psf"] end)
            sum_conservative_sale_price_psfs = conservative_sale_price_psfs.inject(0) do |rst, e| rst += e  end 
            avg_conservative_sale_price_psfs = sum_conservative_sale_price_psfs 
          end
            
        end


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if category == :rental 

          if (expanded_results.length > 0)  &&  !(expanded_results.any? {|h| h[:sale_price].blank?})
            tmp_expanded_results =  expanded_results.sort_by { |hsh| hsh["sale_price"] }.reverse
            if expanded_results.length >= 3
              
              expanded_2 = tmp_expanded_results[2].try(:[],"sale_price")
              expanded_1 = tmp_expanded_results[1].try(:[],"sale_price")
              if expanded_2 > (expanded_1 * 0.85)
                Delayed::Worker.logger.debug " 3 case"
                expanded_sale_prices = (tmp_expanded_results[0,3].map do |d| d["sale_price"] end)
                sum_expanded_sale_prices = expanded_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
                avg_expanded_sale_prices = sum_expanded_sale_prices / 3
                 Delayed::Worker.logger.debug "expanded_sum 1 = #{expanded_sale_prices}"
                 Delayed::Worker.logger.debug "expanded_sum 2 = #{sum_expanded_sale_prices}"
                 Delayed::Worker.logger.debug "expanded_sum 3 = #{avg_expanded_sale_prices}"
              else
                Delayed::Worker.logger.debug " 2 case"
                expanded_sale_prices = (tmp_expanded_results[0,2].map do |d| d["sale_price"] end) 
                sum_expanded_sale_prices = expanded_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
                avg_expanded_sale_prices = sum_expanded_sale_prices / 2
                    Delayed::Worker.logger.debug "expanded_sum 1 = #{expanded_sale_prices}"
                 Delayed::Worker.logger.debug "expanded_sum 2 = #{sum_expanded_sale_prices}"
                 Delayed::Worker.logger.debug "expanded_sum 3 = #{avg_expanded_sale_prices}"
              end
            elsif  expanded_results.length == 2
              expanded_sale_prices = (tmp_expanded_results[0,2].map do |d| d["sale_price"] end)
              sum_expanded_sale_prices = expanded_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
              avg_expanded_sale_prices = sum_expanded_sale_prices / 2
            elsif  expanded_results.length == 1
              expanded_sale_prices = (tmp_expanded_results[0,1].map do |d| d["sale_price"] end)
              sum_expanded_sale_prices = expanded_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
              avg_expanded_sale_prices = sum_expanded_sale_prices 
            end
            
          end
          
          if moderate_results.length > 0  &&  !(moderate_results.any? {|h| h[:sale_price].blank?})
            tmp_moderate_results =  moderate_results.sort_by { |hsh| hsh["sale_price"] }.reverse
            if moderate_results.length >= 3 
              
              moderate_2 = tmp_moderate_results[2].try(:[],"sale_price")
              moderate_1 = tmp_moderate_results[1].try(:[],"sale_price")
              if moderate_2 > (moderate_1 * 0.85)
                Delayed::Worker.logger.debug " 3 case"
                moderate_sale_prices = (tmp_moderate_results[0,3].map do |d| d["sale_price"] end)
                sum_moderate_sale_prices = moderate_sale_prices.inject(0) do |rst, e|  rst += (e.nil? ? 0 : e)  end 
                avg_moderate_sale_prices = sum_moderate_sale_prices / 3
                 Delayed::Worker.logger.debug "moderate_sum 1 = #{moderate_sale_prices}"
                 Delayed::Worker.logger.debug "moderate_sum 2 = #{sum_moderate_sale_prices}"
                 Delayed::Worker.logger.debug "moderate_sum 3 = #{avg_moderate_sale_prices}"
              else
                Delayed::Worker.logger.debug " 2 case"
                moderate_sale_prices = (tmp_moderate_results[0,2].map do |d| d["sale_price"] end) 
                sum_moderate_sale_prices = moderate_sale_prices.inject(0) do |rst, e| rst += (e.nil? ? 0 : e)  end 
                avg_moderate_sale_prices = sum_moderate_sale_prices / 2
                    Delayed::Worker.logger.debug "moderate_sum 1 = #{moderate_sale_prices}"
                 Delayed::Worker.logger.debug "moderate_sum 2 = #{sum_moderate_sale_prices}"
                 Delayed::Worker.logger.debug "moderate_sum 3 = #{avg_moderate_sale_prices}"
              end
            elsif  moderate_results.length == 2
              moderate_sale_prices = (tmp_moderate_results[0,2].map do |d| d["sale_price"] end)
              sum_moderate_sale_prices = moderate_sale_prices.inject(0) do |rst, e|  rst += (e.nil? ? 0 : e)  end 
              avg_moderate_sale_prices = sum_moderate_sale_prices / 2
            elsif  moderate_results.length == 1
              moderate_sale_prices = (tmp_moderate_results[0,1].map do |d| d["sale_price"] end)
              sum_moderate_sale_prices = moderate_sale_prices.inject(0) do |rst, e|  rst += (e.nil? ? 0 : e)  end 
              avg_moderate_sale_prices = sum_moderate_sale_prices / 1
            end
          end

          if conservative_results.length > 0  &&  !(conservative_results.any? {|h| h[:sale_price].blank?})
            tmp_conservative_results =  conservative_results.sort_by { |hsh| hsh["sale_price"] }.reverse
            if conservative_results.length >= 3 
              
              conservative_2 = tmp_conservative_results[2].try(:[],"sale_price")
              conservative_1 = tmp_conservative_results[1].try(:[],"sale_price")
              if conservative_2 > (conservative_1 * 0.85)
                Delayed::Worker.logger.debug " 3 case"
                conservative_sale_prices = (tmp_conservative_results[0,3].map do |d| d["sale_price"] end)
                sum_conservative_sale_prices = conservative_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
                avg_conservative_sale_prices = sum_conservative_sale_prices / 3
                 Delayed::Worker.logger.debug "conservative_sum 1 = #{conservative_sale_prices}"
                 Delayed::Worker.logger.debug "conservative_sum 2 = #{sum_conservative_sale_prices}"
                 Delayed::Worker.logger.debug "conservative_sum 3 = #{avg_conservative_sale_prices}"
              else
                Delayed::Worker.logger.debug " 2 case"
                conservative_sale_prices = (tmp_conservative_results[0,2].map do |d| d["sale_price"] end) 
                sum_conservative_sale_prices = conservative_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
                avg_conservative_sale_prices =  sum_conservative_sale_prices / 2
                    Delayed::Worker.logger.debug "conservative_sum 1 = #{conservative_sale_prices}"
                 Delayed::Worker.logger.debug "conservative_sum 2 = #{sum_conservative_sale_prices}"
                 Delayed::Worker.logger.debug "conservative_sum 3 = #{avg_conservative_sale_prices}"
              end
            elsif conservative_results.length == 2  
              conservative_sale_prices = (tmp_conservative_results[0,2].map do |d| d["sale_price"] end)
              sum_conservative_sale_prices = conservative_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
              avg_conservative_sale_prices = sum_conservative_sale_prices / 2
            elsif conservative_results.length == 1
              conservative_sale_prices = (tmp_conservative_results[0,1].map do |d| d["sale_price"] end)
              sum_conservative_sale_prices = conservative_sale_prices.inject(0) do |rst, e| rst +=  (e.nil? ? 0 : e)   end 
              avg_conservative_sale_prices = sum_conservative_sale_prices 
            end
          end
        else
          if (expanded_results.length > 0)  &&  !(expanded_results.any? {|h| h[:sale_price].blank?})
            tmp_expanded_results =  expanded_results.sort_by { |hsh| hsh["sale_price"] }.reverse
            max_expanded_sale_price = tmp_expanded_results[0].try(:[],"sale_price")
          end

          if (moderate_results.length > 0)  &&  !(moderate_results.any? {|h| h[:sale_price].blank?})
            tmp_moderate_results =  moderate_results.sort_by { |hsh| hsh["sale_price"] }.reverse
            max_moderate_sale_price = tmp_moderate_results[0].try(:[],"sale_price")
          end

          if (conservative_results.length > 0)  &&  !(conservative_results.any? {|h| h[:sale_price].blank?})
            tmp_conservative_results =  conservative_results.sort_by { |hsh| hsh["sale_price"] }.reverse
            max_conservative_sale_price = tmp_conservative_results[0].try(:[],"sale_price")
          end

        end

        expanded_stats = result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).where(["status = ? AND id in (?)",'sold',expanded_results.map { |x| x[:id] }]).inject({}) do |r, item|
          fields.each do |field|
            r["#{item.status}_#{field}"] = item[field]
          end
          r
        end
        expanded_stats = expanded_stats.with_indifferent_access
        Delayed::Worker.logger.debug "expanded stats #{expanded_stats}"

        moderate_stats = result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).where(["status = ? AND id in (?)",'sold',moderate_results.map { |x| x[:id] }]).inject({}) do |r, item|
          fields.each do |field|
            r["#{item.status}_#{field}"] = item[field]
          end
          r
        end
        moderate_stats = moderate_stats.with_indifferent_access
        Delayed::Worker.logger.debug "moderate stats #{moderate_stats}"

        conservative_stats = result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).where(["status = ? AND id in (?)",'sold',conservative_results.map { |x| x[:id] }]).inject({}) do |r, item|
          fields.each do |field|
            r["#{item.status}_#{field}"] = item[field]
          end
          r
        end
        conservative_stats = conservative_stats.with_indifferent_access
        Delayed::Worker.logger.debug "conservative stats #{conservative_stats}"


        

        result = result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).inject({}) do |r, item|
          fields.each do |field|
            r["#{item.status}_#{field}"] = item[field]
          end
          r
        end

        Delayed::Worker.logger.debug "group"

        for status in ::Property::STATUSES
          result["#{status}_count"] ||= 0
        end
        result = result.with_indifferent_access
        sold_sale_price_psf_min = result[:sold_sale_price_psf_min]
        sold_sale_price_psf_max = result[:sold_sale_price_psf_max]
        sold_sale_price_psf_mean = result[:sold_sale_price_psf_mean]
        sold_sale_price_psf_avg = result[:sold_sale_price_psf_avg]
        sold_sale_price_min = result[:sold_sale_price_min]
        sold_sale_price_max = result[:sold_sale_price_max]
        sold_sale_price_mean = result[:sold_sale_price_mean]
        sold_sale_price_avg = result[:sold_sale_price_avg]
        sold_cdom_mean = result[:sold_cdom_mean]
        sold_square_feet_mean = result[:sold_square_feet_mean]
        active_square_feet_mean = result[:active_square_feet_mean]

        


        result[:comps] = comps
        result[:comp_grades] = comp_grades
        Delayed::Worker.logger.debug "avg_conservative_sale_price_psfs #{avg_conservative_sale_price_psfs}"
        result[:expanded_sale_price_max] =  max_expanded_sale_price.nil?  ? nil : max_expanded_sale_price        
        result[:moderate_sale_price_max] =  max_moderate_sale_price.nil?  ? nil : max_moderate_sale_price
        result[:conservative_sale_price_max] =  max_conservative_sale_price.nil?  ? nil : max_conservative_sale_price

        result[:expanded_sale_price_psf_mean] = expanded_stats[:sold_sale_price_psf_mean].nil? ? nil : expanded_stats[:sold_sale_price_psf_mean]
        result[:moderate_sale_price_psf_mean] = moderate_stats[:sold_sale_price_psf_mean].nil? ? nil : moderate_stats[:sold_sale_price_psf_mean]
        result[:conservative_sale_price_psf_mean] = conservative_stats[:sold_sale_price_psf_mean].nil? ? nil : conservative_stats[:sold_sale_price_psf_mean]
        result[:expanded_sale_price_mean] = expanded_stats[:sold_sale_price_mean].nil? ? nil : expanded_stats[:sold_sale_price_mean]
        result[:moderate_sale_price_mean] = moderate_stats[:sold_sale_price_mean].nil? ? nil : moderate_stats[:sold_sale_price_mean]
        result[:conservative_sale_price_mean] = conservative_stats[:sold_sale_price_mean].nil? ? nil : conservative_stats[:sold_sale_price_mean]


        result[:expanded_sale_price_psf] = case category
                                          when :sale 
                                            avg_expanded_sale_price_psfs.nil? ? nil : [result[:expanded_sale_price_psf_mean], (avg_expanded_sale_price_psfs.nil? ? nil : avg_expanded_sale_price_psfs)].reject(&:blank?).max
                                          when :rental 
                                             (avg_expanded_sale_price_psfs.nil? ? nil : avg_expanded_sale_price_psfs)
                                          end
        

        # Delayed::Worker.logger.debug "expanded sale price max: #{result[:expanded_sale_price_max] } vs calc avm  #{(result[:expanded_sale_price_psf].nil? ?  nil : (((result[:expanded_sale_price_psf] * self.square_feet ) / 1000.to_d).round * 1000))}  "
        Delayed::Worker.logger.debug "expanded sale price mean #{result[:expanded_sale_price_mean]} VS avg_expanded_sale_prices #{avg_expanded_sale_prices}"
        result[:expanded_avm] =  case category
                                when :sale
                                  [(result[:expanded_sale_price_max] / 1000.to_d).round * 1000, (result[:expanded_sale_price_psf].nil? ?  nil : (((result[:expanded_sale_price_psf] * self.square_feet ) / 1000.to_d).round * 1000)) ].min rescue nil
                                when :rental
                                   avg_expanded_sale_prices.nil? ? nil : [ (result[:expanded_sale_price_mean].nil? ?  nil : ((result[:expanded_sale_price_mean] / 25.to_d).ceil * 25)) ,(avg_expanded_sale_prices.nil? ?  nil : ((avg_expanded_sale_prices / 25.to_d).ceil * 25))].reject(&:blank?).max
                                end
        
        result[:moderate_sale_price_psf]  = case category
                                          when :sale
                                            avg_moderate_sale_price_psfs.nil? ? nil : [result[:moderate_sale_price_psf_mean], (avg_moderate_sale_price_psfs.nil? ? nil : avg_moderate_sale_price_psfs)].reject(&:blank?).max
                                          when :rental
                                            (avg_moderate_sale_price_psfs.nil? ? nil : avg_moderate_sale_price_psfs)
                                          end

        result[:moderate_avm] = case category
                                when :sale
                                  [(result[:moderate_sale_price_max] / 1000.to_d).round * 1000, (result[:moderate_sale_price_psf].nil? ?  nil : (((result[:moderate_sale_price_psf] * self.square_feet ) / 1000.to_d).round * 1000))].min rescue nil
                                when :rental
                                   avg_moderate_sale_prices.nil? ? nil : [ (result[:moderate_sale_price_mean].nil? ? nil : ((result[:moderate_sale_price_mean] / 25.to_d).ceil * 25)) ,(avg_moderate_sale_prices.nil? ?  nil : ((avg_moderate_sale_prices  / 25.to_d).ceil * 25) )].reject(&:blank?).max
                                end
        result[:conservative_sale_price_psf] =  case category
                                when :sale
                                  avg_conservative_sale_price_psfs.nil? ? nil : [result[:conservative_sale_price_psf_mean], (avg_conservative_sale_price_psfs.nil? ? nil : avg_conservative_sale_price_psfs) ].reject(&:blank?).max
                                when :rental
                                  (avg_conservative_sale_price_psfs.nil? ? nil : avg_conservative_sale_price_psfs) 
                                end

        result[:conservative_avm] =  case category
                                when :sale
                                  [(result[:conservative_sale_price_max] / 1000.to_d).round * 1000, (result[:conservative_sale_price_psf].nil? ?  nil : (((result[:conservative_sale_price_psf] * self.square_feet ) / 1000.to_d).round * 1000))].min rescue nil
                                when :rental
                                  avg_conservative_sale_prices.nil? ? nil : [  (result[:conservative_sale_price_mean].nil? ? nil : ((result[:conservative_sale_price_mean] / 25.to_d).ceil * 25)) ,(avg_conservative_sale_prices.nil? ?  nil : ((avg_conservative_sale_prices / 25.to_d).ceil * 25) )].reject(&:blank?).max
                                end
        
        result[:expanded_sale_price_psf_max] = expanded_stats[:sold_sale_price_psf_max].nil? ? nil : expanded_stats[:sold_sale_price_psf_max]
        
        result[:expanded_list_price_mean] = expanded_stats[:sold_list_price_mean].nil? ? nil : expanded_stats[:sold_list_price_mean]
        result[:expanded_sale_price_to_list_price_mean] = ( result[:expanded_sale_price_mean] && result[:expanded_list_price_mean] > 0 ) ? result[:expanded_sale_price_mean] / result[:expanded_list_price_mean] : nil
        result[:expanded_dom_mean] = expanded_stats[:sold_dom_mean].nil? ? nil : expanded_stats[:sold_dom_mean]
        
        result[:moderate_sale_price_psf_max] = moderate_stats[:sold_sale_price_psf_max].nil? ? nil : moderate_stats[:sold_sale_price_psf_max]        
        
        result[:moderate_list_price_mean] = moderate_stats[:sold_list_price_mean].nil? ? nil : moderate_stats[:sold_list_price_mean]
        result[:moderate_sale_price_to_list_price_mean] = ( result[:moderate_sale_price_mean] && result[:moderate_list_price_mean] > 0 ) ? result[:moderate_sale_price_mean] / result[:moderate_list_price_mean] : nil
        result[:moderate_dom_mean] = moderate_stats[:sold_dom_mean].nil? ? nil : moderate_stats[:sold_dom_mean]
        
        result[:conservative_sale_price_psf_max] = conservative_stats[:sold_sale_price_psf_max].nil? ? nil : conservative_stats[:sold_sale_price_psf_max]        
        
        result[:conservative_list_price_mean] = conservative_stats[:sold_list_price_mean].nil? ? nil : conservative_stats[:sold_list_price_mean]
        result[:conservative_sale_price_to_list_price_mean] = ( result[:conservative_sale_price_mean] && result[:conservative_list_price_mean] > 0 ) ? result[:conservative_sale_price_mean] / result[:conservative_list_price_mean] : nil
        result[:conservative_dom_mean] = conservative_stats[:sold_dom_mean].nil? ? nil : conservative_stats[:sold_dom_mean]
        
        result[:expanded_avm_max] = case category 
                                    when :sale
                                      expanded_stats[:sold_sale_price_psf_max].nil? ? nil : ((((expanded_stats[:sold_sale_price_psf_max]) * self.square_feet ) / 1000.to_d ).round * 1000)
                                    when :rental
                                      expanded_stats[:sold_sale_price_max].nil? ? nil : ((expanded_stats[:sold_sale_price_max] / 10.to_d) * 10)
                                    end
        result[:expanded_avm_mean] = case category
                                    when :sale 
                                      expanded_stats[:sold_sale_price_psf_mean].nil? ? nil : ((((expanded_stats[:sold_sale_price_psf_mean]) * self.square_feet ) / 1000.to_d ).round * 1000)
                                    when :rental
                                      expanded_stats[:sold_sale_price_mean].nil? ? nil : ((expanded_stats[:sold_sale_price_mean] / 10.to_d) * 10)
                                    end
        result[:expanded_list_price_to_avm] = result[:expanded_avm].nil?  ?  nil : self.list_price / result[:expanded_avm] 
        
        
        result[:moderate_avm_max] = case category
                                    when :sale
                                      moderate_stats[:sold_sale_price_psf_max].nil? ? nil : ((((moderate_stats[:sold_sale_price_psf_max]) * self.square_feet ) / 1000.to_d ).round * 1000)
                                    when :rental
                                      moderate_stats[:sold_sale_price_max].nil? ? nil : ((moderate_stats[:sold_sale_price_max] / 10.to_d) * 10)
                                    end
        result[:moderate_avm_mean] = case category
                                     when :sale
                                      moderate_stats[:sold_sale_price_psf_mean].nil? ? nil : ((((moderate_stats[:sold_sale_price_psf_mean]) * self.square_feet ) / 1000.to_d ).round * 1000)
                                     when :rental
                                      moderate_stats[:sold_sale_price_mean].nil? ? nil : ((moderate_stats[:sold_sale_price_mean] / 10.to_d) * 10)
                                     end
        result[:moderate_list_price_to_avm] = result[:moderate_avm].nil?  ? nil  : self.list_price / result[:moderate_avm] 
        
        
        result[:conservative_avm_max] = case category 
                                    when :sale 
                                      conservative_stats[:sold_sale_price_psf_max].nil? ? nil : ((((conservative_stats[:sold_sale_price_psf_max]) * self.square_feet ) / 1000.to_d ).round * 1000)
                                    when :rental
                                      conservative_stats[:sold_sale_price_max].nil? ? nil : ((conservative_stats[:sold_sale_price_max] / 10.to_d) * 10)
                                    end
        result[:conservative_avm_mean] = case category
                                    when :sale
                                     conservative_stats[:sold_sale_price_psf_mean].nil? ? nil : ((((conservative_stats[:sold_sale_price_psf_mean]) * self.square_feet ) / 1000.to_d ).round * 1000)
                                    when :rental
                                      conservative_stats[:sold_sale_price_mean].nil? ? nil : ((conservative_stats[:sold_sale_price_mean] / 10.to_d) * 10)
                                    end

        result[:conservative_list_price_to_avm] = result[:conservative_avm].nil?  ? nil :  self.list_price / result[:conservative_avm] 
        

        case category.to_sym
          when  :rental then
            self.zillow_rent_only = false
            result[:zestimate] = self.send("zillow_#{category}_estimate")  rescue nil
            result[:zestimate_psf] = result[:zestimate] / self.square_feet rescue nil
            zestimate = result[:zestimate].nil? ? 0: result[:zestimate]
            zestimate_psf = result[:zestimate_psf].nil? ? 0: result[:zestimate_psf]

            Delayed::Worker.logger.debug "zill_cat_estimate: #{result[:zestimate]}"
            Delayed::Worker.logger.debug "result sold count  #{result[:sold_count]} "
            Delayed::Worker.logger.debug "sold sale price psf mean nil:  #{sold_sale_price_psf_mean} "


            if (result[:sold_count] >= 1)
              result[:level1] = sold_sale_price_min rescue nil
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],(sold_sale_price_min + sold_sale_price_mean)/2].max rescue nil
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],[sold_sale_price_mean, sold_sale_price_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],[sold_sale_price_mean, sold_sale_price_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],(2*sold_sale_price_mean + sold_sale_price_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],sold_sale_price_max].max rescue nil
            elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
#
              result[:level1] =  zestimate  rescue nil
              result[:level2] =  zestimate  rescue nil
              result[:level3] =  zestimate rescue nil
              result[:level4] =  zestimate  rescue nil
              result[:level5] =  zestimate  rescue nil
              result[:level6] =  zestimate  rescue nil
              self.zillow_rent_only = true

            else

              result[:level1] =  0  rescue nil
              result[:level2] =  0  rescue nil
              result[:level3] =  0  rescue nil
              result[:level4] =  0  rescue nil
              result[:level5] =  0  rescue nil
              result[:level6] =  0  rescue nil
            end
           
            sold_cdom_mean = self.cdom
            sold_sale_price_psf_mean = (sold_sale_price_psf_min + sold_sale_price_psf_max) / 2.to_d rescue nil

          when :sale then
            self.zillow_sale_only = false
            result[:zestimate] = self.send("zillow_#{category}_estimate") rescue nil
            result[:zestimate_psf] = result[:zestimate] / self.square_feet rescue nil
            zestimate = result[:zestimate].nil? ? 0: result[:zestimate]
            zestimate_psf = result[:zestimate_psf].nil? ? 0: result[:zestimate_psf]

            if (result[:sold_count] >= 1)
              Delayed::Worker.logger.debug "@@sold count >= 1"
              # Delayed::Worker.logger.debug "square feet: #{self.square_feet} sppsf_min: #{sold_sale_price_psf_min}"
              result[:level1] = self.square_feet * sold_sale_price_psf_min rescue nil

              result[:level2] = [result[:level1].nil? ? 0: result[:level1],self.square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil

              # Delayed::Worker.logger.debug "square feet: #{self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min}"
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],self.square_feet * (2*sold_sale_price_psf_mean + sold_sale_price_psf_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],self.square_feet * sold_sale_price_psf_max].reject(&:blank?).max rescue nil

            elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
              Delayed::Worker.logger.debug "@@sold count == 0"
              result[:level1] =  zestimate  rescue nil
              result[:level2] =  zestimate  rescue nil
              result[:level3] =  zestimate  rescue nil
              result[:level4] =  zestimate  rescue nil
              result[:level5] =  zestimate  rescue nil
              result[:level6] =  zestimate  rescue nil
              self.zillow_sale_only = true
            else
              Delayed::Worker.logger.debug "@@sold count else"
              result[:level1] =  0  rescue nil
              result[:level2] =  0  rescue nil
              result[:level3] =  0  rescue nil
              result[:level4] =  0  rescue nil
              result[:level5] =  0  rescue nil
              result[:level6] =  0  rescue nil
            end

            sold_cdom_mean = self.cdom

          when :build

            build_square_feet = [(sold_square_feet_mean.nil? ? 0 : sold_square_feet_mean), (active_square_feet_mean.nil? ?  0: active_square_feet_mean)].max
            # build_square_feet = self.square_feet
            sale_comp_level5_psf = self["comps_sale_stats"][:sold_sale_price_psf_max]
            Delayed::Worker.logger.debug "~~build_square_feet: #{build_square_feet} sale_comp_level5_psf #{sale_comp_level5_psf}"
            sale_comp_level5 = (sale_comp_level5_psf.blank? ? 0 : sale_comp_level5_psf)  * build_square_feet      # self["comps_sale_stats"][:level6]
            # (sale_comp_level5/build_square_feet) rescue nil

            if "Infinity" == "#{sale_comp_level5_psf}"
              sale_comp_level5_psf = 0
            end

            if (result[:sold_count] >= 2)
              result[:level1] = build_square_feet * sold_sale_price_psf_min rescue nil
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],build_square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],build_square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],build_square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],build_square_feet * (2*sold_sale_price_psf_mean + sold_sale_price_psf_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],build_square_feet * sold_sale_price_psf_max].max rescue nil
            elsif ((result[:sold_count] >= 1) && (![nil, 0].include?(sale_comp_level5)))
              result[:level1] =  ((build_square_feet * sold_sale_price_psf_min) +  (build_square_feet * sale_comp_level5_psf))/2 rescue nil
              result[:level2] =  [result[:level1].nil? ? 0: result[:level1],build_square_feet * (((sold_sale_price_psf_min + sold_sale_price_psf_mean) /2) + sale_comp_level5_psf)/2].max  rescue nil
              result[:level3] =  [result[:level2].nil? ? 0: result[:level2],([ sold_sale_price_psf_avg  ,  sold_sale_price_psf_mean  ].reject(&:blank?).min + sale_comp_level5_psf) /2 *  build_square_feet].max   rescue nil
              result[:level4] =  [result[:level3].nil? ? 0: result[:level3],([ sold_sale_price_psf_avg  ,  sold_sale_price_psf_mean  ].reject(&:blank?).max + sale_comp_level5_psf) /2 *  build_square_feet].max   rescue nil
              result[:level5] =  [result[:level4].nil? ? 0: result[:level4],(((sold_sale_price_psf_max + sold_sale_price_psf_mean*2)  /3)  +      sale_comp_level5_psf) /2  * build_square_feet].max rescue nil
              result[:level6] =  [result[:level5].nil? ? 0: result[:level5],(( build_square_feet *       sold_sale_price_psf_max ) +  (build_square_feet * sale_comp_level5_psf)  )/2].max    rescue nil
            elsif ((result[:sold_count] == 0) && (![nil, 0].include?(sale_comp_level5)))
              result[:level1] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level2] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level3] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level4] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level5] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level6] = build_square_feet * sale_comp_level5_psf rescue nil
            else
              result[:level1] = 0  rescue nil
              result[:level2] = 0  rescue nil
              result[:level3] = 0  rescue nil
              result[:level4] = 0  rescue nil
              result[:level5] = 0  rescue nil
              result[:level6] = 0  rescue nil
            end

            result[:level5_sale_comp] = sale_comp_level5
            result[:level5_psf] = sale_comp_level5_psf
            result[:projected_square_feet] = build_square_feet

        end

        result[:adj_sale_price_psf_min] = sold_sale_price_psf_min
        result[:adj_sale_price_psf_max] = sold_sale_price_psf_max
        result[:adj_sale_price_psf_mean] = sold_sale_price_psf_mean
        result[:adj_cdom_mean] = sold_cdom_mean

        result[:timeframe] = 365
        result[:sold_homes] = result[:sold_count]
        result[:active_homes] = result[:active_count]
        result[:sold_homes_rate] = result[:timeframe] / result[:sold_homes] rescue nil
        result[:absorption_rate] = (result[:sold_homes_rate] * result[:active_homes]) / 30.to_d rescue nil
        Delayed::Worker.logger.debug "end comparable"

      end

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "backtrace #{e.backtrace}"
      Delayed::Worker.logger.debug "The cause is: #{e.message}"
      raise e
    end
    result

  end

  def comparables2(category, stats=false, force=false)

    tries = 0
    result = nil

    begin

      Delayed::Worker.logger.debug "start comparable"
      # query = self.market.send("#{category}_comp_query_for", self)
      # Delayed::Worker.logger.debug "~~ #{category} query: #{query}"
#
      if !stats
        result = self.market.properties.where(["id in (:ids)",ids: self.send("comps_#{category}_stats")[:comps]])
        Delayed::Worker.logger.debug "property check return collection"
        return result if !self.send("comps_#{category}_stats")[:comps].nil? # if result.length > 0
      end
      Delayed::Worker.logger.debug "self.id #{self.id}"
      # Delayed::Worker.logger.debug "~~ Prop Comps Query:  #{self.market.properties.comps(self, query).where("id <> ?", self.id).to_sql}"
      # Delayed::Worker.logger.debug "~~ query: #{query}"
      # result = self.market.properties.comps(self, query).where("id <> ?", self.id)
      # ----
      # test_result = result.group(:status).having("status = ?","sold").select("count(*) AS count") rescue nil
      # s_count =  test_result.map(&:count) if test_result && test_result.length > 0
#
      conds = []
      front_cond = nil
      back_cond = nil
      test_result = nil
      s_count = 0


      case category
      when :rental

         case self.market.id
          when 2
            conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[5] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

          when 3
            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_sub_area is not null  and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '
            conds[2] =  ' (mls_area_key is not null and mls_sub_area is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and (mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school})) '

            conds[3] =  ' (mls_area_key is not null and mls_sub_area is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}  and mls_high_school = #{mls_high_school}) '

            conds[4] =  ' (mls_area_key is not null and mls_sub_area is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}) '

            conds[5] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and ( mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}  )   )    '

            conds[7] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and (  mls_high_school = #{mls_high_school}  )   )    '
          when 4

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

          when 5

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

          when 6

            conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

            # conds[1] = ' (neighborhood_number IS NOT NULL and neighborhood_number= #{neighborhood_number} ) '

            conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

            conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[6] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '


          else
            # Subdivision and subdivision_section_number
            conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

            conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
            conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
            conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
            conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
            conds[7] = ' (zip IS NOT NULL and zip = #{zip} and  (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
            conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
            conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'


          end


         case self.market.id
          when 4

            if self.rental_type
              front_cond = ' (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                        AND (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)
                        AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                        AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                        AND (category = "rental")
                        AND (rental_type = #{rental_type})
                        AND
                        (
                           (status <> "sold")
                            OR
                           ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                        )
                        AND
                        (  '
            else
               front_cond = ' (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                      AND (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)
                      AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                      AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                      AND (category = "rental")
                      AND (rental_type is null)
                      AND
                      (
                         (status <> "sold")
                          OR
                         ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                      )
                      AND
                      (  '
            end
           else

              front_cond  =  ' (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                      AND (year_built BETWEEN #{year_built} - 10 AND #{year_built} + 10)
                      AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                      AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                      AND (category = "rental")
                      AND
                      (
                         (status <> "sold")
                          OR
                         ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                      )
                      AND
                      (  '






          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
          conds.each_with_index do |cond,index|
            interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"
            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []
            # test_result = self.market.properties.comps(self, interpol_queries[index]).where("id <> ? and status = ?", self.id, "sold")
            # test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []

            # s_count =  test_result.map(&:count) if test_result && test_result.length > 0
            Delayed::Worker.logger.debug "~~ query result: #{index}   #{test_result.try(:length)}  "
            qres = (qres + test_result).uniq
            acc_count = qres.length
            rres = (rres + query_results).uniq
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
            last_index = index
            break if acc_count >= 5
          end


          # result = self.market.properties.where(["(id in (?)) ",rres])
          result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])


      when :sale

          case self.market.id
            when 2
            conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[5] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '


            when 3
              conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
              conds[1] =  '   (mls_area_key is not null and mls_sub_area is not null  and mls_elementary_school is not null and mls_middle_school is not null and
                              mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '
              conds[2] =  ' (mls_area_key is not null and mls_sub_area is not null and
                              mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and (mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school})) '

              conds[3] =  ' (mls_area_key is not null and mls_sub_area is not null and  mls_high_school is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}  and mls_high_school = #{mls_high_school}) '

              conds[4] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}) '

              conds[5] =  ' (mls_area_key is not null and mls_sub_area is not null
                                and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and ( mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}  )   )    '

              conds[6] =  ' (mls_area_key is not null and mls_sub_area is not null
                                and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and (  mls_high_school = #{mls_high_school}  )   )    '

            when 4

              conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
              conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                              mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

              conds[2] =  ' (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

              conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                              and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


              conds[4] =  ' (mls_area_key is not null and  high_school is not null
                              and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

              conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

              conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

              conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            when 5

               conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
                conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                                mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

                conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

                conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                                and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


                conds[4] =  ' (mls_area_key is not null and  high_school is not null
                                and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

                conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

                conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

                conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            when 6
                conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

                # conds[1] = ' (neighborhood_number IS NOT NULL and neighborhood_number= #{neighborhood_number} ) '

                conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

                conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

                conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                                mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

                conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

                conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                                and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


                conds[6] =  ' (mls_area_key is not null and  high_school is not null
                                and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

                conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

                conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

                conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

            else
              conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

              conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
              conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
              conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
              conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
              conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
              conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
              conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
              conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
              conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'



          end

          case  self.market.id
            when 4

              if self.single_family_type
                front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                           AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                           AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                           AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                           AND (category = #{category})
                           AND (single_family_type = #{single_family_type})
                           AND
                           (
                              (status <> "sold")
                              OR
                              ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                           )
                           AND
                           ( '
               else
                  front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                           AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                           AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                           AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                           AND (category = #{category})
                           AND (single_family_type is null)
                           AND
                           (
                              (status <> "sold")
                              OR
                              ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                           )
                           AND
                           ( '
               end

             when 3

                 front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                            AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                            AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                            AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                            AND (category = #{category})
                            AND
                            (
                                (status <> "sold")
                              OR
                                ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                            )
                            AND
                            ('



             else

                front_cond  =  '  (square_feet BETWEEN #{square_feet}*0.80 AND #{square_feet}*1.2)
                         AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
                         AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
                         AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms}+1)
                         AND (category = #{category})
                         AND
                         (
                            (status <> "sold")
                            OR
                            ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                         )
                         AND
                         ( '


          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
          conds.each_with_index do |cond,index|
            interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            Delayed::Worker.logger.debug "~~ output: #{interpol_queries[index]}"
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"
            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []
            Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
            qres = (qres + test_result).uniq
            acc_count = qres.length
            rres = (rres + query_results).uniq
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
            last_index = index
            break if acc_count >= 5
          end

           # result = self.market.properties.where(["(id in (?))",rres])

           result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])

      when :build
         case self.market.id
          when 2
            conds[0] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[1] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[2] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[3] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[4] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[5] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[6] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[7] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[8] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '


          when 3
            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_sub_area is not null  and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '
            conds[2] =  ' (mls_area_key is not null and mls_sub_area is not null and
                            mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area} and (mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school})) '

            conds[3] =  ' (mls_area_key is not null and mls_sub_area is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}  and mls_high_school = #{mls_high_school}) '

            conds[4] =  ' (mls_area_key is not null and mls_sub_area is not null
                            and mls_area_key = #{mls_area_key} and mls_sub_area = #{mls_sub_area}) '

            conds[5] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and ( mls_elementary_school = #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}  )   )    '

            conds[6] =  ' (mls_area_key is not null and mls_sub_area is not null
                              and mls_area_key = #{mls_area_key} and mls_sub_area between #{mls_sub_area} - 1 and #{mls_sub_area} + 1  and (  mls_high_school = #{mls_high_school}  )   )    '

          when 4

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

          when 5

            conds[0] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '
            conds[1] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[2] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[3] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[4] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[5] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[6] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[7] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '

          when 6

            conds[0] = ' (subdivision is not null and subdivision_section_number is not null
                            and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}) '

            # conds[1] = ' (neighborhood_number IS NOT NULL and neighborhood_number= #{neighborhood_number} ) '

            conds[1] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '

            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} ) '

            conds[3] =  '   (mls_area_key is not null and mls_elementary_school is not null and mls_middle_school is not null and
                            mls_area_key = #{mls_area_key} and mls_elementary_school = #{mls_elementary_school} and mls_middle_school = #{mls_middle_school}) '

            conds[4] =  '   (mls_area_key is not null and elementary_school is not null and middle_school is not null and mls_area_key = #{mls_area_key} and elementary_school = #{elementary_school} and middle_school = #{middle_school}) '

            conds[5] =  ' (mls_area_key is not null and  mls_high_school is not null
                            and mls_area_key = #{mls_area_key} and mls_high_school = #{mls_high_school}) '


            conds[6] =  ' (mls_area_key is not null and  high_school is not null
                            and mls_area_key = #{mls_area_key} and high_school = #{high_school}) '

            conds[7] =  ' (mls_area_key is not null and mls_area_key = #{mls_area_key} ) '

            conds[8] =  ' (mls_elementary_school =  #{mls_elementary_school} or mls_middle_school = #{mls_middle_school}) '

            conds[9] =  ' (elementary_school =  #{elementary_school} or middle_school = #{middle_school}) '



          else

            conds[0] = ' (subdivision IS NOT NULL and subdivision_section_number IS NOT NULL and subdivision = #{subdivision} and subdivision_section_number = #{subdivision_section_number}    )'

            conds[1] = ' (subdivision IS NOT NULL and elementary_school is NOT NULL and middle_school is not null and subdivision = #{subdivision} and elementary_school = #{elementary_school} and middle_school = #{middle_school}  )'
            conds[2] = ' (subdivision IS NOT NULL and subdivision = #{subdivision} and (elementary_school = #{elementary_school} or  middle_school = #{middle_school}) )'
            conds[3] = ' (subdivision IS NOT NULL and high_school is not null and subdivision = #{subdivision} and high_school = #{high_school})'
            conds[4] = ' (subdivision IS NOT NULL and subdivision = #{subdivision})'
            conds[5] = ' (legal_subdivision IS NOT NULL and legal_subdivision = #{legal_subdivision} ) '
            conds[6] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} ) )'
            conds[7] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school}) )'
            conds[8] = ' (geo_market_area IS NOT NULL and geo_market_area = #{geo_market_area} and (elementary_school = #{elementary_school}  or middle_school = #{middle_school} or high_school = #{high_school}) )'
            conds[9] = ' (zip IS NOT NULL and zip = #{zip} and (elementary_school = #{elementary_school} OR middle_school = #{middle_school} OR high_school = #{high_school}  ) )'



          end

          case self.market.id
          when 2,3,4,5,6
              front_cond  =  ' (year_built >= Year(CURDATE())-3)
                         AND (category = "single_family")
                         AND (single_family_type = "freestanding")
                         AND
                         (
                            (status <> "sold")
                            OR
                            ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                         )
                         AND
                         (  '



            else
              front_cond  =  ' (year_built >= Year(CURDATE())-3)
                       AND (category = "single_family")
                       AND
                       (
                          (status <> "sold")
                          OR
                          ( closed_date >= CURDATE() - INTERVAL 9 MONTH )
                       )
                       AND
                       (  '



          end

          back_cond =  ' )  AND ( city IS NOT NULL AND county IS NOT NULL AND  city LIKE #{city} AND county LIKE #{county})'

          interpol_queries = []
          query_results = []
          qres = []
          rres = []
          last_index = 0
          acc_count = 0
           conds.each_with_index do |cond,index|
            interpol_queries[index] = interpolate_sql [front_cond,cond,back_cond].join("")
            Delayed::Worker.logger.debug "~~ check sql: #{self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id).to_sql}"
            query_results = self.market.properties.comps(self, interpol_queries[index]).where("id <> ?", self.id) if !interpol_queries[index].blank?
            test_result = query_results.select{|x| x.status == "sold"}.map(&:id) rescue []
            Delayed::Worker.logger.debug "~~ query result: #{index}  #{test_result.try(:length)}  "
            qres = (qres + test_result).uniq
            acc_count = qres.length
            rres = (rres + query_results).uniq
            Delayed::Worker.logger.debug "~~ query ##{index}length: #{acc_count}"
            last_index = index
            break if acc_count >= 5
          end


          # result = self.market.properties.where(["(id in (?))",rres])
          result = self.market.properties.where(["(id in (?)) and ( ((latitude is not null) and 69*haversine(?,?,latitude,longitude) <= 2) or (latitude is null))",rres, self.latitude, self.longitude])

      end

      if stats

        comps = []
        if result
         Delayed::Worker.logger.debug "assign property comparable"
         comps = result.map do |e| e.id end rescue []
        end

         Delayed::Worker.logger.debug "after assign property comparable"
        fields = COMP_STATS_QUERY.last + [:count]

        Delayed::Worker.logger.debug "field ret"
        # Delayed::Worker.logger.debug "~~ COMP_STATS_QUERY: #{COMP_STATS_QUERY}"
        # Delayed::Worker.logger.debug "~~ result group: #{result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).to_sql}"
        result = result.group(:status).select("status, count(*) AS count").select(COMP_STATS_QUERY.first).inject({}) do |r, item|
          fields.each do |field|
            r["#{item.status}_#{field}"] = item[field]
          end
          r
        end

        Delayed::Worker.logger.debug "group"

        for status in ::Property::STATUSES
          result["#{status}_count"] ||= 0
        end
        result = result.with_indifferent_access
        sold_sale_price_psf_min = result[:sold_sale_price_psf_min]
        sold_sale_price_psf_max = result[:sold_sale_price_psf_max]
        sold_sale_price_psf_mean = result[:sold_sale_price_psf_mean]
        sold_sale_price_psf_avg = result[:sold_sale_price_psf_avg]
        sold_sale_price_min = result[:sold_sale_price_min]
        sold_sale_price_max = result[:sold_sale_price_max]
        sold_sale_price_mean = result[:sold_sale_price_mean]
        sold_sale_price_avg = result[:sold_sale_price_avg]
        sold_cdom_mean = result[:sold_cdom_mean]
        sold_square_feet_mean = result[:sold_square_feet_mean]
        active_square_feet_mean = result[:active_square_feet_mean]
        result[:comps] = comps


        case category.to_sym
          when  :rental then
            self.zillow_rent_only = false
            result[:zestimate] = self.send("zillow_#{category}_estimate")  rescue nil
            result[:zestimate_psf] = result[:zestimate] / self.square_feet rescue nil
            zestimate = result[:zestimate].nil? ? 0: result[:zestimate]
            zestimate_psf = result[:zestimate_psf].nil? ? 0: result[:zestimate_psf]

            Delayed::Worker.logger.debug "zill_cat_estimate: #{result[:zestimate]}"
            Delayed::Worker.logger.debug "result sold count  #{result[:sold_count]} "
            Delayed::Worker.logger.debug "sold sale price psf mean nil:  #{sold_sale_price_psf_mean} "


            if (result[:sold_count] >= 1)
              result[:level1] = sold_sale_price_min rescue nil
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],(sold_sale_price_min + sold_sale_price_mean)/2].max rescue nil
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],[sold_sale_price_mean, sold_sale_price_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],[sold_sale_price_mean, sold_sale_price_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],(2*sold_sale_price_mean + sold_sale_price_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],sold_sale_price_max].max rescue nil
            elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
#
              result[:level1] =  zestimate  rescue nil
              result[:level2] =  zestimate  rescue nil
              result[:level3] =  zestimate rescue nil
              result[:level4] =  zestimate  rescue nil
              result[:level5] =  zestimate  rescue nil
              result[:level6] =  zestimate  rescue nil
              self.zillow_rent_only = true

            else

              result[:level1] =  0  rescue nil
              result[:level2] =  0  rescue nil
              result[:level3] =  0  rescue nil
              result[:level4] =  0  rescue nil
              result[:level5] =  0  rescue nil
              result[:level6] =  0  rescue nil
            end

            # if (result[:sold_count] >= 3)
#
              # Delayed::Worker.logger.debug "rental sold count >= 3 "
              # result[:level1] =  sold_sale_price_min rescue nil
              # result[:level2] =  [result[:level1].nil? ? 0: result[:level1], (sold_sale_price_min + sold_sale_price_mean)/2].max  rescue nil
              # result[:level3] =  [result[:level2].nil? ? 0: result[:level2], sold_sale_price_mean ].max  rescue nil
              # result[:level4] =  [result[:level3].nil? ? 0: result[:level3], (sold_sale_price_mean*2 + sold_sale_price_max)/3].max  rescue nil
              # result[:level5] =  [result[:level4].nil? ? 0: result[:level4], (sold_sale_price_mean + sold_sale_price_max)/2].max  rescue nil
              # result[:level6] =  [result[:level5].nil? ? 0: result[:level5], sold_sale_price_max].max rescue nil
#
            # elsif ((result[:sold_count] >= 1 && result[:sold_count] <= 2 ) && (![nil,0].include?(zestimate)))
#
              # Delayed::Worker.logger.debug "rental sold count < 3 and zestimate has value #{result[:zestimate]}"
              # result[:level1] =  (result[:zestimate] + sold_sale_price_min)/2   rescue nil
              # result[:level2] =  [result[:level1].nil? ? 0: result[:level1], (zestimate + sold_sale_price_min + sold_sale_price_mean)/3].max   rescue nil
              # result[:level3] =  [result[:level2].nil? ? 0: result[:level2], (zestimate + sold_sale_price_mean)/2].max   rescue nil
              # result[:level4] =  [result[:level3].nil? ? 0: result[:level3], [zestimate, (sold_sale_price_mean*2 + sold_sale_price_max + zestimate)/4  ].max ].max   rescue nil
              # result[:level5] =  [result[:level4].nil? ? 0: result[:level4], [zestimate, (zestimate + sold_sale_price_max*2 + sold_sale_price_mean)/4 ].max ].max rescue nil
              # result[:level6] =  [result[:level5].nil? ? 0: result[:level5], [zestimate, (sold_sale_price_max*2 + zestimate)/3].max].max   rescue nil
#
            # elsif ((result[:sold_count] >= 1 && result[:sold_count] <= 2 ) && ([nil,0].include?(zestimate)))
#
              # Delayed::Worker.logger.debug "fall under rental < 3 and zestimate has no value #{result[:zestimate]}"
              # result[:level1] =  sold_sale_price_min   rescue nil
              # result[:level2] = [result[:level1].nil? ? 0: result[:level1], (sold_sale_price_min + sold_sale_price_mean)/2].max   rescue nil
              # result[:level3] = [result[:level2].nil? ? 0: result[:level2], sold_sale_price_mean].max   rescue nil
              # result[:level4] = [result[:level3].nil? ? 0: result[:level3], (sold_sale_price_mean*2 + sold_sale_price_max)/3].max  rescue nil
              # result[:level5] = [result[:level4].nil? ? 0: result[:level4], (sold_sale_price_max + sold_sale_price_mean)/2].max  rescue nil
              # result[:level6] = [result[:level5].nil? ? 0: result[:level5], sold_sale_price_max].max  rescue nil
#
            # elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
#
              # result[:level1] =  zestimate  rescue nil
              # result[:level2] =  zestimate  rescue nil
              # result[:level3] =  zestimate rescue nil
              # result[:level4] =  zestimate  rescue nil
              # result[:level5] =  zestimate  rescue nil
              # result[:level6] =  zestimate  rescue nil
#
            # else
#
              # result[:level1] =  0  rescue nil
              # result[:level2] =  0  rescue nil
              # result[:level3] =  0  rescue nil
              # result[:level4] =  0  rescue nil
              # result[:level5] =  0  rescue nil
              # result[:level6] =  0  rescue nil
#
            # end
            sold_cdom_mean = self.cdom
            sold_sale_price_psf_mean = (sold_sale_price_psf_min + sold_sale_price_psf_max) / 2.to_d rescue nil

          when :sale then
            self.zillow_sale_only = false
            result[:zestimate] = self.send("zillow_#{category}_estimate") rescue nil
            result[:zestimate_psf] = result[:zestimate] / self.square_feet rescue nil
            zestimate = result[:zestimate].nil? ? 0: result[:zestimate]
            zestimate_psf = result[:zestimate_psf].nil? ? 0: result[:zestimate_psf]

            if (result[:sold_count] >= 1)
              Delayed::Worker.logger.debug "@@sold count >= 1"
              # Delayed::Worker.logger.debug "square feet: #{self.square_feet} sppsf_min: #{sold_sale_price_psf_min}"
              result[:level1] = self.square_feet * sold_sale_price_psf_min rescue nil

              # Delayed::Worker.logger.debug "square feet: #{self.square_feet * (sold_sale_price_psf_min.to_s.to_d + sold_sale_price_psf_mean.to_s.to_d)/2}"
              # Delayed::Worker.logger.debug "sale_price psf min nil?: #{sold_sale_price_psf_min.nil?}"
              # Delayed::Worker.logger.debug "sale_price psf mean nil?: #{sold_sale_price_psf_mean.nil?}"
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],self.square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil

              # Delayed::Worker.logger.debug "square feet: #{self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min}"
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],self.square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],self.square_feet * (2*sold_sale_price_psf_mean + sold_sale_price_psf_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],self.square_feet * sold_sale_price_psf_max].reject(&:blank?).max rescue nil

            elsif  ((result[:sold_count] == 0) && (!zestimate.blank?))
              Delayed::Worker.logger.debug "@@sold count == 0"
              result[:level1] =  zestimate  rescue nil
              result[:level2] =  zestimate  rescue nil
              result[:level3] =  zestimate  rescue nil
              result[:level4] =  zestimate  rescue nil
              result[:level5] =  zestimate  rescue nil
              result[:level6] =  zestimate  rescue nil
              self.zillow_sale_only = true
            else
              Delayed::Worker.logger.debug "@@sold count else"
              result[:level1] =  0  rescue nil
              result[:level2] =  0  rescue nil
              result[:level3] =  0  rescue nil
              result[:level4] =  0  rescue nil
              result[:level5] =  0  rescue nil
              result[:level6] =  0  rescue nil
            end

            sold_cdom_mean = self.cdom

          when :build

            build_square_feet = [(sold_square_feet_mean.nil? ? 0 : sold_square_feet_mean), (active_square_feet_mean.nil? ?  0: active_square_feet_mean)].max
            # build_square_feet = self.square_feet
            sale_comp_level5_psf = self["comps_sale_stats"][:sold_sale_price_psf_max]
            Delayed::Worker.logger.debug "~~build_square_feet: #{build_square_feet} sale_comp_level5_psf #{sale_comp_level5_psf}"
            sale_comp_level5 = (sale_comp_level5_psf.blank? ? 0 : sale_comp_level5_psf)  * build_square_feet      # self["comps_sale_stats"][:level6]
            # (sale_comp_level5/build_square_feet) rescue nil

            if "Infinity" == "#{sale_comp_level5_psf}"
              sale_comp_level5_psf = 0
            end

            if (result[:sold_count] >= 2)
              result[:level1] = build_square_feet * sold_sale_price_psf_min rescue nil
              result[:level2] = [result[:level1].nil? ? 0: result[:level1],build_square_feet * (sold_sale_price_psf_min + sold_sale_price_psf_mean)/2].max rescue nil
              result[:level3] = [result[:level2].nil? ? 0: result[:level2],build_square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).min].max rescue nil
              result[:level4] = [result[:level3].nil? ? 0: result[:level3],build_square_feet *  [sold_sale_price_psf_mean , sold_sale_price_psf_avg].reject(&:blank?).max].max rescue nil
              result[:level5] = [result[:level4].nil? ? 0: result[:level4],build_square_feet * (2*sold_sale_price_psf_mean + sold_sale_price_psf_max)/3].max rescue nil
              result[:level6] = [result[:level5].nil? ? 0: result[:level5],build_square_feet * sold_sale_price_psf_max].max rescue nil
            elsif ((result[:sold_count] >= 1) && (![nil, 0].include?(sale_comp_level5)))
              result[:level1] =  ((build_square_feet * sold_sale_price_psf_min) +  (build_square_feet * sale_comp_level5_psf))/2 rescue nil
              result[:level2] =  [result[:level1].nil? ? 0: result[:level1],build_square_feet * (((sold_sale_price_psf_min + sold_sale_price_psf_mean) /2) + sale_comp_level5_psf)/2].max  rescue nil
              result[:level3] =  [result[:level2].nil? ? 0: result[:level2],([ sold_sale_price_psf_avg  ,  sold_sale_price_psf_mean  ].reject(&:blank?).min + sale_comp_level5_psf) /2 *  build_square_feet].max   rescue nil
              result[:level4] =  [result[:level3].nil? ? 0: result[:level3],([ sold_sale_price_psf_avg  ,  sold_sale_price_psf_mean  ].reject(&:blank?).max + sale_comp_level5_psf) /2 *  build_square_feet].max   rescue nil
              result[:level5] =  [result[:level4].nil? ? 0: result[:level4],(((sold_sale_price_psf_max + sold_sale_price_psf_mean*2)  /3)  +      sale_comp_level5_psf) /2  * build_square_feet].max rescue nil
              result[:level6] =  [result[:level5].nil? ? 0: result[:level5],(( build_square_feet *       sold_sale_price_psf_max ) +  (build_square_feet * sale_comp_level5_psf)  )/2].max    rescue nil
            elsif ((result[:sold_count] == 0) && (![nil, 0].include?(sale_comp_level5)))
              result[:level1] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level2] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level3] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level4] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level5] = build_square_feet * sale_comp_level5_psf rescue nil
              result[:level6] = build_square_feet * sale_comp_level5_psf rescue nil
            else
              result[:level1] = 0  rescue nil
              result[:level2] = 0  rescue nil
              result[:level3] = 0  rescue nil
              result[:level4] = 0  rescue nil
              result[:level5] = 0  rescue nil
              result[:level6] = 0  rescue nil
            end
            
            result[:level5_sale_comp] = sale_comp_level5
            result[:level5_psf] = sale_comp_level5_psf
            result[:projected_square_feet] = build_square_feet

        end

        result[:adj_sale_price_psf_min] = sold_sale_price_psf_min
        result[:adj_sale_price_psf_max] = sold_sale_price_psf_max
        result[:adj_sale_price_psf_mean] = sold_sale_price_psf_mean
        result[:adj_cdom_mean] = sold_cdom_mean

        result[:timeframe] = 365
        result[:sold_homes] = result[:sold_count]
        result[:active_homes] = result[:active_count]
        result[:sold_homes_rate] = result[:timeframe] / result[:sold_homes] rescue nil
        result[:absorption_rate] = (result[:sold_homes_rate] * result[:active_homes]) / 30.to_d rescue nil
        Delayed::Worker.logger.debug "end comparable"

      end

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError  =>  e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      raise e
    rescue Exception => e
      tries += 1
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      retry if tries <= 3
      Delayed::Worker.logger.debug "backtrace #{e.backtrace}"
      Delayed::Worker.logger.debug "The cause is: #{e.message}"
      raise e
    end
    result

  end



  def to_xls(columns, template_path=nil, out_path=nil)
    Property.write_to_excel(columns, template_path, out_path) do |sheet|
      columns.each_with_index do |column, index|
        sheet[index, 0] = column.to_s.dup
        sheet[index, 1] = self.send(column)
      end
    end
  end



  # - upon entry
  # - upon hot sheet change
  # - on demand when user open property pages
  # - every 7 days for active/pending properties
  # - every 30 days for others
  def compute(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      self.send(:fetch_zestimate, force)
 
      self.updateSchoolInfo(force)
 
 
      self.google_geocode if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
      # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
        # self.geocodio_geocode
      # end
      # mapquest_geocode if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
#
      # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
#
        # self.osm_geocode
        # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
          # self.force_geo_coordinates2
        # end
      # end

      [:rental, :sale].each do |key|
        Delayed::Worker.logger.debug "------------ key: #{key} ------------"
        # if market.id == 7
        #   stats = self.comparables(key, true, force)
        # else
        stats = self.comparables(key, true, force)
        # end
        self["comps_#{key}_stats"] = stats
      end


      # save property model
      Delayed::Worker.logger.debug "self.save"
      self.save(validate: false)

      # execute and save market proforma
      Delayed::Worker.logger.debug "proforma restore"
      self.proforma.restore(true)

      if self.market.id.in? [3,6,10,11]
        # sft_user_id = 14
        # sft_proformas = SixFourThreeProforma.where(["user_id = ? and property_id = ?", sft_user_id, self.id])
        # if sft_proformas.count == 0
          # sft_proforma = SixFourThreeProforma.new(user_id: sft_user_id, property_id: self.id)
        # else
          # sft_proforma = sft_proformas.first
        # end
        # sft_proforma.restore(true)

        # atfz_user_id = 20
        # atfz_proformas = AhrThreeFiveZeroProforma.where(["user_id = ? and property_id = ?", atfz_user_id, self.id])
        # if atfz_proformas.count == 0
          # atfz_proforma = AhrThreeFiveZeroProforma.new(user_id: atfz_user_id, property_id: self.id)
        # else
          # atfz_proforma = atfz_proformas.first
        # end
        # atfz_proforma.restore(true)

        invitation_user_id = 23
        invitation_proformas = InvitationProforma.where(["user_id = ? and property_id = ?", invitation_user_id, self.id])
        if invitation_proformas.count == 0
          invitation_proforma = InvitationProforma.new(user_id: invitation_user_id, property_id: self.id)
        else
          invitation_proforma = invitation_proformas.first
        end
        invitation_proforma.restore(true)

        # invitation_two_user_id = 22
        # invitation_two_proformas = InvitationTwoProforma.where(["user_id = ? and property_id = ?", invitation_two_user_id, self.id])
        # if invitation_two_proformas.count == 0
        #   invitation_two_proforma = InvitationTwoProforma.new(user_id: invitation_two_user_id, property_id: self.id)
        # else
        #   invitation_two_proforma = invitation_two_proformas.first
        # end
        # invitation_two_proforma.restore(true)
        
        standard_user_id = 1
        standard_proformas = Proforma.where(["user_id = ? and property_id = ?", standard_user_id, self.id])
        if standard_proformas.count == 0
          standard_proforma = Proforma.new(user_id: standard_user_id, property_id: self.id)
        else
          standard_proforma = invitation_proformas.first
        end
        standard_proforma.restore(true)
      end



      Delayed::Worker.logger.debug "update_attribute"
      self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched #{e.message}"
      nil
    end

  end

  def compute2(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      # self.send(:fetch_zestimate2, force)

      # self.updateSchoolInfo(force)


      # self.google_geocode2 if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )



      [:rental, :sale].each do |key|
        Delayed::Worker.logger.debug "------------ key: #{key} ------------"
        # if market.id == 7
        #   stats = self.comparables(key, true, force)
        # else
        stats = self.comparables(key, true, force)
        # end
        self["comps_#{key}_stats"] = stats
      end


      # save property model
      Delayed::Worker.logger.debug "self.save"
      self.save(validate: false)

      # # execute and save market proforma
      # Delayed::Worker.logger.debug "proforma restore"
      self.proforma.restore(true)

      if self.market.id.in? [3,6,10]
        # sft_user_id = 14
        # sft_proformas = SixFourThreeProforma.where(["user_id = ? and property_id = ?", sft_user_id, self.id])
        # if sft_proformas.count == 0
          # sft_proforma = SixFourThreeProforma.new(user_id: sft_user_id, property_id: self.id)
        # else
          # sft_proforma = sft_proformas.first
        # end
        # sft_proforma.restore(true)

        # atfz_user_id = 20
        # atfz_proformas = AhrThreeFiveZeroProforma.where(["user_id = ? and property_id = ?", atfz_user_id, self.id])
        # if atfz_proformas.count == 0
          # atfz_proforma = AhrThreeFiveZeroProforma.new(user_id: atfz_user_id, property_id: self.id)
        # else
          # atfz_proforma = atfz_proformas.first
        # end
        # atfz_proforma.restore(true)

        invitation_user_id = 21
        invitation_proformas = InvitationProforma.where(["user_id = ? and property_id = ?", invitation_user_id, self.id])
        if invitation_proformas.count == 0
          invitation_proforma = InvitationProforma.new(user_id: invitation_user_id, property_id: self.id)
        else
          invitation_proforma = invitation_proformas.first
        end
        invitation_proforma.restore(true)
        
        standard_user_id = 1
        standard_proformas = Proforma.where(["user_id = ? and property_id = ?", standard_user_id, self.id])
        if standard_proformas.count == 0
          standard_proforma = Proforma.new(user_id: standard_user_id, property_id: self.id)
        else
          standard_proforma = invitation_proformas.first
        end
        standard_proforma.restore(true)
      end

      # Delayed::Worker.logger.debug "update_attribute"
      self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched #{e.message}"
      nil
    end

  end

  def compute3(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      self.send(:fetch_zestimate, force)

      self.updateSchoolInfo(force)


      self.google_geocode if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
      if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
        self.geocodio_geocode
      end
      # mapquest_geocode if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
#
      # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
#
        # self.osm_geocode
        # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
          # self.force_geo_coordinates2
        # end
      # end

      [:rental, :sale].each do |key|
        Delayed::Worker.logger.debug "------------ key: #{key} ------------"
        # if market.id == 7
        #   stats = self.comparables(key, true, force)
        # else
        stats = self.comparables(key, true, force)
        # end
        self["comps_#{key}_stats"] = stats
      end


      # save property model
      Delayed::Worker.logger.debug "self.save"
      self.save(validate: false)

      # execute and save market proforma
      Delayed::Worker.logger.debug "proforma restore"
      self.proforma.restore(true)

      if self.market.id.in? [3,6]
        # sft_user_id = 14
        # sft_proformas = SixFourThreeProforma.where(["user_id = ? and property_id = ?", sft_user_id, self.id])
        # if sft_proformas.count == 0
          # sft_proforma = SixFourThreeProforma.new(user_id: sft_user_id, property_id: self.id)
        # else
          # sft_proforma = sft_proformas.first
        # end
        # sft_proforma.restore(true)
# 
        # atfz_user_id = 20
        # atfz_proformas = AhrThreeFiveZeroProforma.where(["user_id = ? and property_id = ?", atfz_user_id, self.id])
        # if atfz_proformas.count == 0
          # atfz_proforma = AhrThreeFiveZeroProforma.new(user_id: atfz_user_id, property_id: self.id)
        # else
          # atfz_proforma = atfz_proformas.first
        # end
        # atfz_proforma.restore(true)

        invitation_user_id = 21
        invitation_proformas = InvitationProforma.where(["user_id = ? and property_id = ?", invitation_user_id, self.id])
        if invitation_proformas.count == 0
          invitation_proforma = InvitationProforma.new(user_id: invitation_user_id, property_id: self.id)
        else
          invitation_proforma = invitation_proformas.first
        end
        invitation_proforma.restore(true)
        
        standard_user_id = 1
        standard_proformas = Proforma.where(["user_id = ? and property_id = ?", standard_user_id, self.id])
        if standard_proformas.count == 0
          standard_proforma = Proforma.new(user_id: standard_user_id, property_id: self.id)
        else
          standard_proforma = invitation_proformas.first
        end
        standard_proforma.restore(true)
      end



      Delayed::Worker.logger.debug "update_attribute"
      self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched #{e.message}"
      nil
    end

  end

   def compute4(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      # self.send(:fetch_zestimate, force)
# #
      # self.updateSchoolInfo(force)

      self.osm_geocode if self.latitude.nil? || self.longitude.nil?

      # [:rental, :sale, :build].each do |key|
        # Delayed::Worker.logger.debug "------------ key: #{key} ------------"
        # stats = self.comparables(key, true, force)
        # self["comps_#{key}_stats"] = stats
      # end


      # save property model
      Delayed::Worker.logger.debug "self.save"
      self.save(validate: false)

      # # execute and save market proforma
      # Delayed::Worker.logger.debug "proforma restore"
      # self.proforma.restore(true)

      # Delayed::Worker.logger.debug "update_attribute"
      # self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched #{e.message}"
      nil
    end

  end

  def compute5(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      self.send(:fetch_zestimate5, force)
      # self.updateSchoolInfo5(force)
      puts "zillow id: #{self.zillow_id} zest_sale: #{self.zillow_sale_estimate} zest_rent: #{self.zillow_rental_estimate} elementary_school: #{self.elementary_school} elementary_school_rating: #{self.elementary_school_rating}"

# #
      # self.updateSchoolInfo2(force)
#
      # self.osm_geocode if self.latitude.nil? || self.longitude.nil?

#       [:rental, :sale, :build].each do |key|
#         Delayed::Worker.logger.debug "------------ key: #{key} ------------"
#         stats = self.comparables(key, true, force)
#         self["comps_#{key}_stats"] = stats
#       end

#       # save property model
#       Delayed::Worker.logger.debug "self.save"
#       self.save(validate: false)

#       # # execute and save market proforma
#       Delayed::Worker.logger.debug "proforma restore"
#       self.proforma.restore(true)
# #
#       Delayed::Worker.logger.debug "update_attribute"
#       self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched"
      nil
    end

  end


  def compute6(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      self.send(:fetch_zestimate3, force)

      self.updateSchoolInfo3(force)

      self.osm_geocode2 if self.latitude.nil? || self.longitude.nil?

      [:sale].each do |key|
        Delayed::Worker.logger.debug "------------ key: #{key} ------------"
        stats = self.comparables2(key, true, force)
        puts "stat #{stats}"
        self["comps_#{key}_stats"] = stats
      end


      # save property model
      # Delayed::Worker.logger.debug "self.save"
      # self.save(validate: false)

      # # execute and save market proforma
      # Delayed::Worker.logger.debug "proforma restore"
      # self.proforma.restore(true)

      # Delayed::Worker.logger.debug "update_attribute"
      # self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched"
      nil
    end

  end
  
   def compute7(force=false)
    begin
      Delayed::Worker.logger.debug "------------- getting #{self.tax_id} to compute -------------" if self.tax_id
      # compute all comparables and save necessary stats

      self.send(:fetch_zestimate6, force)
# 
      self.updateSchoolInfo(true)
# 
# 
      self.google_geocode if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
      # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
        # self.geocodio_geocode
      # end
      # mapquest_geocode if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
#
      # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
#
        # self.osm_geocode
        # if ( (self.latitude.blank? || self.latitude == 0) || (self.longitude.blank? || self.longitude == 0) )
          # self.force_geo_coordinates2
        # end
      # end

      # [:rental, :sale].each do |key|
        # Delayed::Worker.logger.debug "------------ key: #{key} ------------"
        # stats = self.comparables(key, true, force)
        # self["comps_#{key}_stats"] = stats
      # end


      # save property model
      Delayed::Worker.logger.debug "self.save"
      self.save(validate: false)

      # execute and save market proforma
      Delayed::Worker.logger.debug "proforma restore"
      # self.proforma.restore(true)

      if self.market.id.in? [3,6]
        # sft_user_id = 14
        # sft_proformas = SixFourThreeProforma.where(["user_id = ? and property_id = ?", sft_user_id, self.id])
        # if sft_proformas.count == 0
          # sft_proforma = SixFourThreeProforma.new(user_id: sft_user_id, property_id: self.id)
        # else
          # sft_proforma = sft_proformas.first
        # end
        # sft_proforma.restore(true)

        # atfz_user_id = 20
        # atfz_proformas = AhrThreeFiveZeroProforma.where(["user_id = ? and property_id = ?", atfz_user_id, self.id])
        # if atfz_proformas.count == 0
          # atfz_proforma = AhrThreeFiveZeroProforma.new(user_id: atfz_user_id, property_id: self.id)
        # else
          # atfz_proforma = atfz_proformas.first
        # end
        # atfz_proforma.restore(true)

        # invitation_user_id = 21
        # invitation_proformas = InvitationProforma.where(["user_id = ? and property_id = ?", invitation_user_id, self.id])
        # if invitation_proformas.count == 0
          # invitation_proforma = InvitationProforma.new(user_id: invitation_user_id, property_id: self.id)
        # else
          # invitation_proforma = invitation_proformas.first
        # end
        # invitation_proforma.restore(true)
#         
        # standard_user_id = 1
        # standard_proformas = Proforma.where(["user_id = ? and property_id = ?", standard_user_id, self.id])
        # if standard_proformas.count == 0
          # standard_proforma = Proforma.new(user_id: standard_user_id, property_id: self.id)
        # else
          # standard_proforma = invitation_proformas.first
        # end
        # standard_proforma.restore(true)
      end



      Delayed::Worker.logger.debug "update_attribute"
      self.update_attribute(:comps_updated_at, DateTime.now)

      true

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      Delayed::Worker.logger.debug "Timeout related exception"
      nil
    rescue Exception => e
      Delayed::Worker.logger.debug "Exception catched #{e.message}"
      nil
    end

  end

  def self.override(attrs)
    record = self.new
    record.override_attributes(attrs)
    puts ">>>> send prepare "
    record.send(:prepare)
    record
  end

  def on_market?
    self.status.in?('active', 'pending')
  end

  def update_dom
    diff = (self.created_at - Date.today)
    self.dom = self.dom_original + diff
    self.cdom = self.cdom_original + diff
    self.save(validate: false)
  end

  def self.from_xls(filepath)
    results = []
    Property.read_from_excel(filepath) do |sheet|

    end
  end

  def self.from_csv(filepath)
    CSV.foreach(filepath, :headers => true) do |row|
      property = self.find_or_initialize_by_mls_number(row[:mls_number])
      property.assign_attributes(row)
      property.save(validate: false)
    end
  end

  def self.to_comps_report(columns, out_path=nil)
    %w{}
    out_path ? CSV.open(out_path, "wb") : CSV.generate do |csv|
      csv << ["tax_id","street_address","zip","rent lvl 1",  "rent lvl 2",  "rent lvl 3",  "rent lvl 4",  "rent lvl 5",  "rent lvl 6",  "sale lvl 1",  "sale lvl 2",  "sale lvl 3",  "sale lvl 4",  "sale lvl 5",  "sale lvl 6"]
      self.all.each do |property|
        # csv << property.extract(*columns).values
        fields = [property.tax_id,property.street_address,property.zip,property.comps_rental_stats[:level1],property.comps_rental_stats[:level2],property.comps_rental_stats[:level3],
        property.comps_rental_stats[:level4],property.comps_rental_stats[:level5],property.comps_rental_stats[:level6],
        property.comps_sale_stats[:level1],property.comps_sale_stats[:level2],property.comps_sale_stats[:level3],
        property.comps_sale_stats[:level4],property.comps_sale_stats[:level5],property.comps_sale_stats[:level6]]
        csv << fields
      end
    end
  end

  def self.to_csv(columns, out_path=nil)
    out_path ? CSV.open(out_path, "wb") : CSV.generate do |csv|
      csv << columns
      self.all.each do |property|
        csv << property.extract(*columns).values
      end
    end
  end

  def self.to_csv2(columns, coordinate, user_id, out_path=nil)
    out_path ? CSV.open(out_path, "wb") : CSV.generate do |csv|

      csv << ['id'] + columns
      self.all.each do |property|

        result = {}
        result[:id] = property.id
        columns.each {|key|
          if property.respond_to?(key) && key.to_s != "tag"

            if key.to_s == "rep"
              Delayed::Worker.logger.debug "##>%% user value #{key.class}"
              user = User.find_by_id(property.send(key)) rescue nil
              user_full = [user.firstname, user.lastname].compact.join(' ') rescue nil
              result[key] = user_full rescue nil

            # elsif key == "school_district"
#
#
               # Delayed::Worker.logger.debug "##>%% school district"
               # sd = SchoolDistrict.find_by_id(property.send(key)) rescue nil
               # result[key] = sd.school_district_name rescue nil
            else
              result[key] = property.send(key)
            end

          else

              if key.to_s.include?("_")
                value = nil
                method_call = key.to_s[key.to_s.index("_") + 1 , key.to_s.length - key.to_s.index("_") - 1 ]

                case key
                  when "comps_distance"
                        if !property.latitude.nil? && !property.longitude.nil?
                          Delayed::Worker.logger.debug "~~ coor lat #{coordinate[:latitude]} long #{coordinate[:longitude]}"
                          Delayed::Worker.logger.debug "~~ coor2 lat #{property.latitude} long #{property.longitude}"
                          Delayed::Worker.logger.debug "~~ Haversine #{Haversine.distance(coordinate[:latitude],coordinate[:longitude],property.latitude,property.longitude).to_miles.round(4)}"
                          value = Haversine.distance(coordinate[:latitude],coordinate[:longitude],property.latitude,property.longitude).to_miles.round(4)
                        else
                          value = nil
                        end
                  when  "sale_level1"                    ,
                        "sale_level2"                    ,
                        "sale_level3"                    ,
                        "sale_level4"                    ,
                        "sale_level5"                    ,
                        "sale_level6"
                         value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_active_count"              ,
                        "sale_pending_count"             ,
                        "sale_sold_count"                ,
                        "sale_terminated_count"          ,
                        "sale_withdrawn_count"           ,
                        "sale_expired_count"             ,
                        "sale_sold_square_feet_min"      ,
                        "sale_sold_square_feet_avg"      ,
                        "sale_sold_square_feet_min"      ,
                        "sale_sold_square_feet_avg"      ,
                        "sale_sold_square_feet_max"      ,
                        "sale_sold_square_feet_mean"     ,
                        "sale_sold_beds_min"             ,
                        "sale_sold_beds_avg"             ,
                        "sale_sold_beds_max"             ,
                        "sale_sold_beds_mean"            ,
                        "sale_sold_lot_square_feet_min"  ,
                        "sale_sold_lot_square_feet_avg"  ,
                        "sale_sold_lot_square_feet_max"  ,
                        "sale_sold_lot_square_feet_mean"  ,
                        "sale_sold_cdom_min"             ,
                        "sale_sold_cdom_avg"             ,
                        "sale_sold_cdom_max"             ,
                        "sale_sold_cdom_mean"            ,
                        "sale_sold_year_built_min"       ,
                        "sale_sold_year_built_avg"       ,
                        "sale_sold_year_built_max"       ,
                        "sale_sold_year_built_mean"

                      value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_sold_homes_rate"           ,
                        "sale_absorbtion_rate"
                      value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_sold_bathrooms_min"        ,
                        "sale_sold_bathrooms_avg"        ,
                        "sale_sold_bathrooms_max"        ,
                        "sale_sold_bathrooms_mean"        ,
                        "sale_sold_list_price_min"       ,
                        "sale_sold_list_price_avg"       ,
                        "sale_sold_list_price_max"       ,
                        "sale_sold_list_price_mean"      ,
                        "sale_sold_list_price_psf_min"   ,
                        "sale_sold_list_price_psf_avg"   ,
                        "sale_sold_list_price_psf_max"   ,
                        "sale_sold_list_price_psf_mean"  ,
                        "sale_sold_sale_price_min"       ,
                        "sale_sold_sale_price_avg"       ,
                        "sale_sold_sale_price_max"       ,
                        "sale_sold_sale_price_mean"      ,
                        "sale_sold_sale_price_psf_min"   ,
                        "sale_sold_sale_price_psf_avg"   ,
                        "sale_sold_sale_price_psf_max"   ,
                        "sale_sold_sale_price_psf_mean"

                       value = property.send(:comps_sale_stats)[method_call] rescue nil


                  when  "rental_level1"                    ,
                        "rental_level2"                    ,
                        "rental_level3"                    ,
                        "rental_level4"                    ,
                        "rental_level5"                    ,
                        "rental_level6"
                         value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_active_count"              ,
                        "rental_pending_count"             ,
                        "rental_sold_count"                ,
                        "rental_terminated_count"          ,
                        "rental_withdrawn_count"           ,
                        "rental_expired_count"             ,
                        "rental_sold_square_feet_min"      ,
                        "rental_sold_square_feet_avg"      ,
                        "rental_sold_square_feet_min"      ,
                        "rental_sold_square_feet_avg"      ,
                        "rental_sold_square_feet_max"      ,
                        "rental_sold_square_feet_mean"     ,
                        "rental_sold_beds_min"             ,
                        "rental_sold_beds_avg"             ,
                        "rental_sold_beds_max"             ,
                        "rental_sold_beds_mean"            ,
                        "rental_sold_lot_square_feet_min"  ,
                        "rental_sold_lot_square_feet_avg"  ,
                        "rental_sold_lot_square_feet_max"  ,
                        "rental_sold_lot_square_feet_mean" ,
                        "rental_sold_cdom_min"             ,
                        "rental_sold_cdom_avg"             ,
                        "rental_sold_cdom_max"             ,
                        "rental_sold_cdom_mean"            ,
                        "rental_sold_year_built_min"       ,
                        "rental_sold_year_built_avg"       ,
                        "rental_sold_year_built_max"       ,
                        "rental_sold_year_built_mean"
                      value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_sold_homes_rate"           ,
                        "rental_absorbtion_rate"
                      value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_sold_bathrooms_min"        ,
                        "rental_sold_bathrooms_avg"        ,
                        "rental_sold_bathrooms_max"        ,
                        "rental_sold_bathrooms_mean"       ,
                        "rental_sold_list_price_min"       ,
                        "rental_sold_list_price_avg"       ,
                        "rental_sold_list_price_max"       ,
                        "rental_sold_list_price_mean"      ,
                        "rental_sold_list_price_psf_min"   ,
                        "rental_sold_list_price_psf_avg"   ,
                        "rental_sold_list_price_psf_max"   ,
                        "rental_sold_list_price_psf_mean"  ,
                        "rental_sold_sale_price_min"       ,
                        "rental_sold_sale_price_avg"       ,
                        "rental_sold_sale_price_max"       ,
                        "rental_sold_sale_price_mean"      ,
                        "rental_sold_sale_price_psf_min"   ,
                        "rental_sold_sale_price_psf_avg"   ,
                        "rental_sold_sale_price_psf_max"   ,
                        "rental_sold_sale_price_psf_mean"


                       value = property.send(:comps_rental_stats)[method_call] rescue nil

                   when "buildbuild_level1"                    ,
                        "build_level2"                    ,
                        "build_level3"                    ,
                        "build_level4"                    ,
                        "build_level5"                    ,
                        "build_level6"
                         value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_active_count"              ,
                        "build_pending_count"             ,
                        "build_sold_count"                ,
                        "build_terminated_count"          ,
                        "build_withdrawn_count"           ,
                        "build_expired_count"             ,
                        "build_sold_square_feet_min"      ,
                        "build_sold_square_feet_avg"      ,
                        "build_sold_square_feet_min"      ,
                        "build_sold_square_feet_avg"      ,
                        "build_sold_square_feet_max"      ,
                        "build_sold_square_feet_mean"     ,
                        "build_sold_beds_min"             ,
                        "build_sold_beds_avg"             ,
                        "build_sold_beds_max"             ,
                        "build_sold_beds_mean"            ,
                        "build_sold_lot_square_feet_min"  ,
                        "build_sold_lot_square_feet_avg"  ,
                        "build_sold_lot_square_feet_max"  ,
                        "build_sold_lot_square_feet_mean" ,
                        "build_sold_cdom_min"             ,
                        "build_sold_cdom_avg"             ,
                        "build_sold_cdom_max"             ,
                        "build_sold_cdom_mean"            ,
                        "build_sold_year_built_min"       ,
                        "build_sold_year_built_avg"       ,
                        "build_sold_year_built_max"       ,
                        "build_sold_year_built_mean"
                      value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_sold_homes_rate"           ,
                        "build_absorbtion_rate"
                      value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_sold_bathrooms_min"        ,
                        "build_sold_bathrooms_avg"        ,
                        "build_sold_bathrooms_max"        ,
                        "build_sold_bathrooms_mean"       ,
                        "build_sold_list_price_min"       ,
                        "build_sold_list_price_avg"       ,
                        "build_sold_list_price_max"       ,
                        "build_sold_list_price_mean"      ,
                        "build_sold_list_price_psf_min"   ,
                        "build_sold_list_price_psf_avg"   ,
                        "build_sold_list_price_psf_max"   ,
                        "build_sold_list_price_psf_mean"  ,
                        "build_sold_sale_price_min"       ,
                        "build_sold_sale_price_avg"       ,
                        "build_sold_sale_price_max"       ,
                        "build_sold_sale_price_mean"      ,
                        "build_sold_sale_price_psf_min"   ,
                        "build_sold_sale_price_psf_avg"   ,
                        "build_sold_sale_price_psf_max"   ,
                        "build_sold_sale_price_psf_mean"


                       value = property.send(:comps_build_stats)[method_call] rescue nil

                end



                Delayed::Worker.logger.debug "~~ Key #{key} Value #{value}"
                result[key] = value
              else
                if key == 'tag'
                  uid = user_id.blank? ?  0 : user_id
                  Property.assign_user_id(user_id)

                  tag = PropertyTag.find_by_id(property.send(:user_assigned_tag).send(:tag)) rescue nil
                  value = tag.name rescue nil

                  result[key] = value
                end
              end
          end
        }

        Delayed::Worker.logger.debug "~~ #{result.values}"
        csv << result.values
      end
    end
  end

  def self.to_csv3(current_user, out_path=nil)
    @@class_user_id = current_user.id
    columns = current_user.preferences.columns_saved
    out_path ? CSV.open(out_path, "wb") : CSV.generate do |csv|

      csv << ['id'] + columns
      self.all.each do |property|

        result = {}
        result[:id] = property.id
        columns.each {|key|
          if property.proforma.respond_to?(key)

            result[key] = property.proformas.find_by_user_id(1).send(key) rescue nil

          elsif (property.respond_to?(key) && key.to_s != "tag")

            if key.to_s == "rep"
              Delayed::Worker.logger.debug "##>%% user value #{key.class}"
              user = User.find_by_id(property.send(key)) rescue nil
              user_full = [user.firstname, user.lastname].compact.join(' ') rescue nil
              result[key] = user_full rescue nil

            # elsif key.to_s == "school_district"
#
              # Delayed::Worker.logger.debug "##>%% school district"
              # sd = SchoolDistrict.find_by_id(property.send(key)) rescue nil
              # result[key] = sd.school_district_name rescue nil
            else
              result[key] = property.send(key)
            end
          else

              if key.to_s.include?("_")
                value = nil
                method_call = key.to_s[key.to_s.index("_") + 1 , key.to_s.length - key.to_s.index("_") - 1 ]

                case key

                  when  "sale_level1"                    ,
                        "sale_level2"                    ,
                        "sale_level3"                    ,
                        "sale_level4"                    ,
                        "sale_level5"                    ,
                        "sale_level6"
                         value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_active_count"              ,
                        "sale_pending_count"             ,
                        "sale_sold_count"                ,
                        "sale_terminated_count"          ,
                        "sale_withdrawn_count"           ,
                        "sale_expired_count"             ,
                        "sale_sold_square_feet_min"      ,
                        "sale_sold_square_feet_avg"      ,
                        "sale_sold_square_feet_min"      ,
                        "sale_sold_square_feet_avg"      ,
                        "sale_sold_square_feet_max"      ,
                        "sale_sold_square_feet_mean"     ,
                        "sale_sold_beds_min"             ,
                        "sale_sold_beds_avg"             ,
                        "sale_sold_beds_max"             ,
                        "sale_sold_beds_mean"            ,
                        "sale_sold_lot_square_feet_min"  ,
                        "sale_sold_lot_square_feet_avg"  ,
                        "sale_sold_lot_square_feet_max"  ,
                        "sale_sold_lot_square_feet_mean" ,
                        "sale_sold_cdom_min"             ,
                        "sale_sold_cdom_avg"             ,
                        "sale_sold_cdom_max"             ,
                        "sale_sold_cdom_mean"            ,
                        "sale_sold_year_built_min"       ,
                        "sale_sold_year_built_avg"       ,
                        "sale_sold_year_built_max"       ,
                        "sale_sold_year_built_mean"

                      value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_sold_homes_rate"           ,
                        "sale_absorbtion_rate"
                      value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_sold_bathrooms_min"        ,
                        "sale_sold_bathrooms_avg"        ,
                        "sale_sold_bathrooms_max"        ,
                        "sale_sold_bathrooms_mean"        ,
                        "sale_sold_list_price_min"       ,
                        "sale_sold_list_price_avg"       ,
                        "sale_sold_list_price_max"       ,
                        "sale_sold_list_price_mean"      ,
                        "sale_sold_list_price_psf_min"   ,
                        "sale_sold_list_price_psf_avg"   ,
                        "sale_sold_list_price_psf_max"   ,
                        "sale_sold_list_price_psf_mean"  ,
                        "sale_sold_sale_price_min"       ,
                        "sale_sold_sale_price_avg"       ,
                        "sale_sold_sale_price_max"       ,
                        "sale_sold_sale_price_mean"      ,
                        "sale_sold_sale_price_psf_min"   ,
                        "sale_sold_sale_price_psf_avg"   ,
                        "sale_sold_sale_price_psf_max"   ,
                        "sale_sold_sale_price_psf_mean"


                       value = property.send(:comps_sale_stats)[method_call] rescue nil


                  when  "rental_level1"                    ,
                        "rental_level2"                    ,
                        "rental_level3"                    ,
                        "rental_level4"                    ,
                        "rental_level5"                    ,
                        "rental_level6"
                         value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_active_count"              ,
                        "rental_pending_count"             ,
                        "rental_sold_count"                ,
                        "rental_terminated_count"          ,
                        "rental_withdrawn_count"           ,
                        "rental_expired_count"             ,
                        "rental_sold_square_feet_min"      ,
                        "rental_sold_square_feet_avg"      ,
                        "rental_sold_square_feet_min"      ,
                        "rental_sold_square_feet_avg"      ,
                        "rental_sold_square_feet_max"      ,
                        "rental_sold_square_feet_mean"     ,
                        "rental_sold_beds_min"             ,
                        "rental_sold_beds_avg"             ,
                        "rental_sold_beds_max"             ,
                        "rental_sold_beds_mean"            ,
                        "rental_sold_lot_square_feet_min"  ,
                        "rental_sold_lot_square_feet_avg"  ,
                        "rental_sold_lot_square_feet_max"  ,
                        "rental_sold_lot_square_feet_mean" ,
                        "rental_sold_cdom_min"             ,
                        "rental_sold_cdom_avg"             ,
                        "rental_sold_cdom_max"             ,
                        "rental_sold_cdom_mean"            ,
                        "rental_sold_year_built_min"       ,
                        "rental_sold_year_built_avg"       ,
                        "rental_sold_year_built_max"       ,
                        "rental_sold_year_built_mean"
                      value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_sold_homes_rate"           ,
                        "rental_absorbtion_rate"
                      value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_sold_bathrooms_min"        ,
                        "rental_sold_bathrooms_avg"        ,
                        "rental_sold_bathrooms_max"        ,
                        "rental_sold_bathrooms_mean"       ,
                        "rental_sold_list_price_min"       ,
                        "rental_sold_list_price_avg"       ,
                        "rental_sold_list_price_max"       ,
                        "rental_sold_list_price_mean"      ,
                        "rental_sold_list_price_psf_min"   ,
                        "rental_sold_list_price_psf_avg"   ,
                        "rental_sold_list_price_psf_max"   ,
                        "rental_sold_list_price_psf_mean"  ,
                        "rental_sold_sale_price_min"       ,
                        "rental_sold_sale_price_avg"       ,
                        "rental_sold_sale_price_max"       ,
                        "rental_sold_sale_price_mean"      ,
                        "rental_sold_sale_price_psf_min"   ,
                        "rental_sold_sale_price_psf_avg"   ,
                        "rental_sold_sale_price_psf_max"   ,
                        "rental_sold_sale_price_psf_mean"


                       value = property.send(:comps_rental_stats)[method_call] rescue nil

                   when "buildbuild_level1"                    ,
                        "build_level2"                    ,
                        "build_level3"                    ,
                        "build_level4"                    ,
                        "build_level5"                    ,
                        "build_level6"
                         value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_active_count"              ,
                        "build_pending_count"             ,
                        "build_sold_count"                ,
                        "build_terminated_count"          ,
                        "build_withdrawn_count"           ,
                        "build_expired_count"             ,
                        "build_sold_square_feet_min"      ,
                        "build_sold_square_feet_avg"      ,
                        "build_sold_square_feet_min"      ,
                        "build_sold_square_feet_avg"      ,
                        "build_sold_square_feet_max"      ,
                        "build_sold_square_feet_mean"     ,
                        "build_sold_beds_min"             ,
                        "build_sold_beds_avg"             ,
                        "build_sold_beds_max"             ,
                        "build_sold_beds_mean"            ,
                        "build_sold_lot_square_feet_min"  ,
                        "build_sold_lot_square_feet_avg"  ,
                        "build_sold_lot_square_feet_max"  ,
                        "build_sold_lot_square_feet_mean" ,
                        "build_sold_cdom_min"             ,
                        "build_sold_cdom_avg"             ,
                        "build_sold_cdom_max"             ,
                        "build_sold_cdom_mean"            ,
                        "build_sold_year_built_min"       ,
                        "build_sold_year_built_avg"       ,
                        "build_sold_year_built_max"       ,
                        "build_sold_year_built_mean"
                      value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_sold_homes_rate"           ,
                        "build_absorbtion_rate"
                      value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_sold_bathrooms_min"        ,
                        "build_sold_bathrooms_avg"        ,
                        "build_sold_bathrooms_max"        ,
                        "build_sold_bathrooms_mean"       ,
                        "build_sold_list_price_min"       ,
                        "build_sold_list_price_avg"       ,
                        "build_sold_list_price_max"       ,
                        "build_sold_list_price_mean"      ,
                        "build_sold_list_price_psf_min"   ,
                        "build_sold_list_price_psf_avg"   ,
                        "build_sold_list_price_psf_max"   ,
                        "build_sold_list_price_psf_mean"  ,
                        "build_sold_sale_price_min"       ,
                        "build_sold_sale_price_avg"       ,
                        "build_sold_sale_price_max"       ,
                        "build_sold_sale_price_mean"      ,
                        "build_sold_sale_price_psf_min"   ,
                        "build_sold_sale_price_psf_avg"   ,
                        "build_sold_sale_price_psf_max"   ,
                        "build_sold_sale_price_psf_mean"


                       value = property.send(:comps_build_stats)[method_call] rescue nil

                end



                result[key] = value
              else

                if key == 'tag'
                  Property.assign_user_id(current_user.id)

                  tag = PropertyTag.find_by_id(property.send(:user_assigned_tag).send(:tag)) rescue nil
                  value = tag.name rescue nil

                  result[key] = value
                end

              end
          end
        }


        csv << result.values
      end
    end
  end

  def self.to_csv4(columns, user_id, out_path=nil)
    out_path ? CSV.open(out_path, "wb") : CSV.generate do |csv|

      csv << ['id'] + columns
      self.all.each do |property|

        result = {}
        result[:id] = property.id
        columns.each {|key|
          if property.proforma.respond_to?(key)

            result[key] = property.proforma.send(key)
          elsif property.respond_to?(key) && key.to_s != "tag"

            if key.to_s == "rep"
              Delayed::Worker.logger.debug "##>%% user value #{key.class}"
              user = User.find_by_id(property.send(key)) rescue nil
              user_full = [user.firstname, user.lastname].compact.join(' ') rescue nil
              result[key] = user_full rescue nil

            # elsif key == "school_district"
#
#
               # Delayed::Worker.logger.debug "##>%% school district"
               # sd = SchoolDistrict.find_by_id(property.send(key)) rescue nil
               # result[key] = sd.school_district_name rescue nil
            else
              result[key] = property.send(key)
            end

          else

              if key.to_s.include?("_")
                value = nil
                method_call = key.to_s[key.to_s.index("_") + 1 , key.to_s.length - key.to_s.index("_") - 1 ]

                case key

                  when  "sale_level1"                    ,
                        "sale_level2"                    ,
                        "sale_level3"                    ,
                        "sale_level4"                    ,
                        "sale_level5"                    ,
                        "sale_level6"
                         value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_active_count"              ,
                        "sale_pending_count"             ,
                        "sale_sold_count"                ,
                        "sale_terminated_count"          ,
                        "sale_withdrawn_count"           ,
                        "sale_expired_count"             ,
                        "sale_sold_square_feet_min"      ,
                        "sale_sold_square_feet_avg"      ,
                        "sale_sold_square_feet_min"      ,
                        "sale_sold_square_feet_avg"      ,
                        "sale_sold_square_feet_max"      ,
                        "sale_sold_square_feet_mean"     ,
                        "sale_sold_beds_min"             ,
                        "sale_sold_beds_avg"             ,
                        "sale_sold_beds_max"             ,
                        "sale_sold_beds_mean"            ,
                        "sale_sold_lot_square_feet_min"  ,
                        "sale_sold_lot_square_feet_avg"  ,
                        "sale_sold_lot_square_feet_max"  ,
                        "sale_sold_lot_square_feet_mean"  ,
                        "sale_sold_cdom_min"             ,
                        "sale_sold_cdom_avg"             ,
                        "sale_sold_cdom_max"             ,
                        "sale_sold_cdom_mean"            ,
                        "sale_sold_year_built_min"       ,
                        "sale_sold_year_built_avg"       ,
                        "sale_sold_year_built_max"       ,
                        "sale_sold_year_built_mean"

                      value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_sold_homes_rate"           ,
                        "sale_absorbtion_rate"
                      value = property.send(:comps_sale_stats)[method_call] rescue nil

                  when  "sale_sold_bathrooms_min"        ,
                        "sale_sold_bathrooms_avg"        ,
                        "sale_sold_bathrooms_max"        ,
                        "sale_sold_bathrooms_mean"        ,
                        "sale_sold_list_price_min"       ,
                        "sale_sold_list_price_avg"       ,
                        "sale_sold_list_price_max"       ,
                        "sale_sold_list_price_mean"      ,
                        "sale_sold_list_price_psf_min"   ,
                        "sale_sold_list_price_psf_avg"   ,
                        "sale_sold_list_price_psf_max"   ,
                        "sale_sold_list_price_psf_mean"  ,
                        "sale_sold_sale_price_min"       ,
                        "sale_sold_sale_price_avg"       ,
                        "sale_sold_sale_price_max"       ,
                        "sale_sold_sale_price_mean"      ,
                        "sale_sold_sale_price_psf_min"   ,
                        "sale_sold_sale_price_psf_avg"   ,
                        "sale_sold_sale_price_psf_max"   ,
                        "sale_sold_sale_price_psf_mean"

                       value = property.send(:comps_sale_stats)[method_call] rescue nil


                  when  "rental_level1"                    ,
                        "rental_level2"                    ,
                        "rental_level3"                    ,
                        "rental_level4"                    ,
                        "rental_level5"                    ,
                        "rental_level6"
                         value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_active_count"              ,
                        "rental_pending_count"             ,
                        "rental_sold_count"                ,
                        "rental_terminated_count"          ,
                        "rental_withdrawn_count"           ,
                        "rental_expired_count"             ,
                        "rental_sold_square_feet_min"      ,
                        "rental_sold_square_feet_avg"      ,
                        "rental_sold_square_feet_min"      ,
                        "rental_sold_square_feet_avg"      ,
                        "rental_sold_square_feet_max"      ,
                        "rental_sold_square_feet_mean"     ,
                        "rental_sold_beds_min"             ,
                        "rental_sold_beds_avg"             ,
                        "rental_sold_beds_max"             ,
                        "rental_sold_beds_mean"            ,
                        "rental_sold_lot_square_feet_min"  ,
                        "rental_sold_lot_square_feet_avg"  ,
                        "rental_sold_lot_square_feet_max"  ,
                        "rental_sold_lot_square_feet_mean" ,
                        "rental_sold_cdom_min"             ,
                        "rental_sold_cdom_avg"             ,
                        "rental_sold_cdom_max"             ,
                        "rental_sold_cdom_mean"            ,
                        "rental_sold_year_built_min"       ,
                        "rental_sold_year_built_avg"       ,
                        "rental_sold_year_built_max"       ,
                        "rental_sold_year_built_mean"
                      value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_sold_homes_rate"           ,
                        "rental_absorbtion_rate"
                      value = property.send(:comps_rental_stats)[method_call] rescue nil

                  when  "rental_sold_bathrooms_min"        ,
                        "rental_sold_bathrooms_avg"        ,
                        "rental_sold_bathrooms_max"        ,
                        "rental_sold_bathrooms_mean"       ,
                        "rental_sold_list_price_min"       ,
                        "rental_sold_list_price_avg"       ,
                        "rental_sold_list_price_max"       ,
                        "rental_sold_list_price_mean"      ,
                        "rental_sold_list_price_psf_min"   ,
                        "rental_sold_list_price_psf_avg"   ,
                        "rental_sold_list_price_psf_max"   ,
                        "rental_sold_list_price_psf_mean"  ,
                        "rental_sold_sale_price_min"       ,
                        "rental_sold_sale_price_avg"       ,
                        "rental_sold_sale_price_max"       ,
                        "rental_sold_sale_price_mean"      ,
                        "rental_sold_sale_price_psf_min"   ,
                        "rental_sold_sale_price_psf_avg"   ,
                        "rental_sold_sale_price_psf_max"   ,
                        "rental_sold_sale_price_psf_mean"


                       value = property.send(:comps_rental_stats)[method_call] rescue nil

                   when "buildbuild_level1"                    ,
                        "build_level2"                    ,
                        "build_level3"                    ,
                        "build_level4"                    ,
                        "build_level5"                    ,
                        "build_level6"
                         value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_active_count"              ,
                        "build_pending_count"             ,
                        "build_sold_count"                ,
                        "build_terminated_count"          ,
                        "build_withdrawn_count"           ,
                        "build_expired_count"             ,
                        "build_sold_square_feet_min"      ,
                        "build_sold_square_feet_avg"      ,
                        "build_sold_square_feet_min"      ,
                        "build_sold_square_feet_avg"      ,
                        "build_sold_square_feet_max"      ,
                        "build_sold_square_feet_mean"     ,
                        "build_sold_beds_min"             ,
                        "build_sold_beds_avg"             ,
                        "build_sold_beds_max"             ,
                        "build_sold_beds_mean"            ,
                        "build_sold_lot_square_feet_min"  ,
                        "build_sold_lot_square_feet_avg"  ,
                        "build_sold_lot_square_feet_max"  ,
                        "build_sold_lot_square_feet_mean" ,
                        "build_sold_cdom_min"             ,
                        "build_sold_cdom_avg"             ,
                        "build_sold_cdom_max"             ,
                        "build_sold_cdom_mean"            ,
                        "build_sold_year_built_min"       ,
                        "build_sold_year_built_avg"       ,
                        "build_sold_year_built_max"       ,
                        "build_sold_year_built_mean"
                      value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_sold_homes_rate"           ,
                        "build_absorbtion_rate"
                      value = property.send(:comps_build_stats)[method_call] rescue nil

                  when  "build_sold_bathrooms_min"        ,
                        "build_sold_bathrooms_avg"        ,
                        "build_sold_bathrooms_max"        ,
                        "build_sold_bathrooms_mean"       ,
                        "build_sold_list_price_min"       ,
                        "build_sold_list_price_avg"       ,
                        "build_sold_list_price_max"       ,
                        "build_sold_list_price_mean"      ,
                        "build_sold_list_price_psf_min"   ,
                        "build_sold_list_price_psf_avg"   ,
                        "build_sold_list_price_psf_max"   ,
                        "build_sold_list_price_psf_mean"  ,
                        "build_sold_sale_price_min"       ,
                        "build_sold_sale_price_avg"       ,
                        "build_sold_sale_price_max"       ,
                        "build_sold_sale_price_mean"      ,
                        "build_sold_sale_price_psf_min"   ,
                        "build_sold_sale_price_psf_avg"   ,
                        "build_sold_sale_price_psf_max"   ,
                        "build_sold_sale_price_psf_mean"


                       value = property.send(:comps_build_stats)[method_call] rescue nil

                end



                result[key] = value
              else

                if key == 'tag'
                  Property.assign_user_id(user_id)

                  tag = PropertyTag.find_by_id(property.send(:user_assigned_tag).send(:tag)) rescue nil
                  value = tag.name rescue nil

                  result[key] = value
                end
              end
          end
        }


        csv << result.values
      end
    end
  end

  def self.to_xls(columns, template_path=nil, out_path=nil)
    Property.write_to_excel(columns, template_path, out_path) do |sheet|
      columns.each_with_index do |column, cindex|
        sheet[0, cindex] = column.to_s.dup
      end

      row_index = 1
      all.each do |property|
        columns.each_with_index do |column, cindex|
          sheet[row_index, cindex] = property.send(column)
        end
        row_index += 1
      end
    end
  end

  def self.joins_property_tag
    joins("LEFT JOIN `property_tags` ON `property_tags`.`id` =  `assigned_tags`.`tag`")
    # joins("LEFT JOIN `property_tags` ON `property_tags`.`id` =  cast(`assigned_tags`.`tag` as UNSIGNED)")

  end

  # def self.joins_school_district
    # joins("LEFT JOIN `school_districts` ON `school_districts`.`id` = `properties`.`school_district`")
  # end

  def self.joins_property_rep
    joins("LEFT JOIN `users` ON `users`.`id` = `properties`.`rep`")
  end

  def self.joins_proforma
    joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND ifnull(`proformas`.`user_id`,0) = 0 ")
    # joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND `proformas`.`user_id` IS NULL")
  end

  def self.joins_client_proforma
    joins("LEFT JOIN `client_proformas` ON `client_proformas`.`property_id` = `properties`.`id` AND ifnull(`client_proformas`.`user_id`,0) = 0 ")
    # joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND `proformas`.`user_id` IS NULL")
  end

  def self.joins_user_proforma(user_id)
    joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND `proformas`.`user_id` = #{user_id} ")
    # joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND `proformas`.`user_id` IS NULL")
  end

  def self.joins_client_user_proforma(user_id)
    joins("LEFT JOIN `client_proformas` ON `client_proformas`.`property_id` = `properties`.`id` AND `client_proformas`.`user_id` = #{user_id} ")
    # joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND `proformas`.`user_id` IS NULL")
  end

  def self.joins_assigned_tag(user_id)
    joins("LEFT JOIN `assigned_tags` ON `assigned_tags`.`property_id` = `properties`.`id`   AND `assigned_tags`.`user_id` = #{user_id}")
    # joins("LEFT JOIN `proformas` ON `proformas`.`property_id` = `properties`.`id` AND `proformas`.`user_id` IS NULL")
  end

  def self.joins_detail
    joins("LEFT JOIN `property_details` ON `property_details`.`id` = `properties`.`detail_id` ")
  end



  def self.search_stats
    result = { mls_area_key_distinct: self.distinct_order_unsigned(:mls_area_key, :mls_area_name) }
    SEARCH_STATS_DISTINCTS.each do |field|
      result["#{field}_distinct".to_sym] = self.distinct(field)
    end
    result.merge!(self.joins_proforma.select(SEARCH_STATS_QUERY.first).first.extract(*SEARCH_STATS_QUERY.last))
    Delayed::Worker.logger.debug "### result #{result}"
    result.with_indifferent_access
  end

  def self.search(config, since=nil,user_id)
    query = config.query(proc {|k| attr_sql_name(k) })
    Delayed::Worker.logger.debug "@@@# query: #{query}"
    self.joins_proforma.joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).since(since)
  end

  def self.search_for_user(config, since=nil,user_id)

    query = config.query(proc {|k| attr_sql_name(k) })
    Delayed::Worker.logger.debug "@@@# query: #{query}"
    # Delayed::Worker.logger.debug "@@# query: #{self.joins_proforma.joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).since(since).to_sql}"
    self.joins_proforma.joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).since(since)
  end

  def self.search_for_client_user(config, since=nil,user_id)

    query = config.query(proc {|k| client_attr_sql_name(k) })
    Delayed::Worker.logger.debug "@@@# query: #{query}"
    # Delayed::Worker.logger.debug "@@# query: #{self.joins_proforma.joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).since(since).to_sql}"
    self.joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).since(since)
  end
  
  def self.retrieve_for_client_user(since=nil,user_id)
     # list = ['5695714']
     list = ['5690971','8001248','5693012','5694665','5695485','5695382',
'5695515','8005034','8004765','8005326','5695701','5695714',
'5695966','5695674','5695991','5601254','7583785','5661319',
'5682138','7630674','5631393','5684576','5628526','7621952',
'5683941','5684361','5684226','5686042','7638290','7636794',
'5687646','7632929','7637019','7640352','5662096','7638392',
'7628411','5688701','5690822','5692085','5692011','5690813',
'5629477','5692568','5689097','8001628','8002948','8000456','7642217']
    query = ["properties.mls_number IN (?) and status IN (?)", list,['active']]
    Delayed::Worker.logger.debug "@@@# query: #{query}"
    Delayed::Worker.logger.debug "@@# query: #{self.joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).since(since).to_sql}"
    if since
      self.joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).order("properties.updated_at desc").limit(since)
    else
      self.joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.where(query).order("properties.updated_at desc")
    end
  end

  def self.prepare
    ids = self.pluck('properties.id')
    Delayed::Worker.logger.debug "prepare ids #{ids}"
    Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:property_rep)
  end

  def self.prepare_user(user_id)
    @@class_user_id = user_id

    ids = self.pluck('properties.id')
    Delayed::Worker.logger.debug "prepare ids #{ids}"
    # Delayed::Worker.logger.debug "#%%# #{Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:user_assigned_tag => :property_tag).includes(:property_rep).to_sql}"
    # Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:user_assigned_tag => :property_tag).includes(:property_rep)
    Property.unscoped.where(id: ids).joins_proforma.joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*")
  end

  def self.set_user_id(user_id)
    @@class_user_id = user_id
  end

  def self.prepare_client_user(user_id)
    @@class_user_id = user_id

    ids = self.pluck('properties.id')
    Delayed::Worker.logger.debug "prepare ids #{ids}"
    # Delayed::Worker.logger.debug "#%%# #{Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:user_assigned_tag => :property_tag).includes(:property_rep).to_sql}"
    # Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:user_assigned_tag => :property_tag).includes(:property_rep)
    Property.unscoped.where(id: ids).joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*")
  end
  
  def self.prepare_custom_user(user_id)
    @@class_user_id = user_id

    ids = self.pluck('properties.id')
    Delayed::Worker.logger.debug "prepare ids #{ids}"
    # Delayed::Worker.logger.debug "#%%# #{Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:user_assigned_tag => :property_tag).includes(:property_rep).to_sql}"
    # Property.unscoped.where(id: ids).includes(:proforma).includes(:detail).includes(:user_assigned_tag => :property_tag).includes(:property_rep)
    Property.unscoped.where(id: ids).joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*,client_proformas.*")
  end

  def self.assign_user_id(user_id)
    @@class_user_id = user_id
  end

  def self.prepare_saved(user_id)
    ids = self.pluck('properties.id')

    @@class_user_id = user_id
    Delayed::Worker.logger.debug "prepare ids #{ Property.unscoped.where(id: ids).joins_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*").to_sql}"

    Property.unscoped.where(id: ids).joins_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*")
    # Property.unscoped.where(id: ids).includes(:user_proforma).includes(:property_rep)
  end
  
  def self.prepare_client_saved(user_id)
    ids = self.pluck('properties.id')

    @@class_user_id = user_id
    Delayed::Worker.logger.debug "prepare ids #{Property.unscoped.where(id: ids).joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*").to_sql}"

    Property.unscoped.where(id: ids).joins_client_user_proforma(user_id).joins_detail.joins_assigned_tag(user_id).joins_property_tag.joins_property_rep.select("properties.*")
    # Property.unscoped.where(id: ids).includes(:user_proforma).includes(:property_rep)
  end

  def self.prepare_geomap_json(url_proc,user_id)
    result = []
    self.readonly(false).all.each_with_index do |property, index|
      property.send(:fetch_geo_coordinates, user_id)
      data = GEOMAP_FIELDS.inject({}) {|r, field| r[label(field)] = property.format(field); r }
      result << property.extract(:id, :latitude, :longitude, :address).merge(
        data: data, index: index+1, url: url_proc.call(property.id), geocoded: property.geocoded?
      )
    end
    result.to_json
  end

  def census_api_call()

    tries = 0
    attrs = nil

    begin
      agent = Mechanize.new  { |ag|
        ag.open_timeout = 26
        ag.read_timeout = 26
        ag.idle_timeout = 0.9
        ag.retry_change_requests = true
      }

      proxy = Proxy.find_available

      if proxy
        ips = GoodIp.find_working_ips
         if (!ips || (ips && (ips.count < 6)))
          ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
        end
        case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
        when 0
          agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
        when 1
          agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
        when 2
          agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
        when 3
          agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
        when 4
          agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
        when 5
          agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
        end
         # agent.set_proxy('198.23.223.133',80,'glt','gltproxy') if !Rails.env.development?
         # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        href = 'http://geocoding.geo.census.gov/geocoder/geographies/onelineaddress?form'
        page = agent.get href
        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
         
        form = page.form_with(:id => "command")
        form.address = self.address
         
        page = form.submit
        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
         
        px = page.parser.xpath("//div[@id='pl_gov_census_geo_geocoder_domain_AddressResult']/div[@version='2.0']")
         
        if px && px.count > 0

          puts "attrs: px has value #{px.to_s}"
          ts = px.to_s
          
          m = ts.match(/<br>COUNTY:\s*([0-9][0-9.]*[0-9]*)/)
          census_county = m[1]            
          
          m = ts.match(/<br>GEOID:\s*([0-9]{7,15})/)
          census_geoid  = m[1]            
          
          m = ts.match(/<br>TRACT:\s*([0-9][0-9.]*[0-9]*)/)
          census_tract = m[1]            
          
          m = ts.match(/<br>STATE:\s*([0-9][0-9.]*[0-9]*)/)
          census_state = m[1]            
          
          m = ts.match(/<br>BLOCK:\s*([0-9][0-9.]*[0-9]*)/)
          census_block = m[1]

          attrs = {}
          attrs = attrs.merge({:census_county => census_county}) if census_county
          attrs = attrs.merge({:census_geoid => census_geoid}) if census_geoid
          attrs = attrs.merge({:census_tract => census_tract}) if census_tract
          attrs = attrs.merge({:census_state => census_state}) if census_state
          attrs = attrs.merge({:census_block => census_block}) if census_block

          puts "attrs: #{attrs}"

          self.assign_attributes(attrs) unless !attrs
          self.save(validate: false)
          return true
      
        else  
          ret = false
        end
      else
        ret = false
      end

    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
      tries += 1
      proxy = Proxy.find_available
      proxy.try(:increment_calls)
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      if tries > 2
        proxy.try(:mark_error)
        proxy.try(:add_error_count)
      end
      retry if tries <= 3

      Delayed::Worker.logger.debug "No more attempt!"
      Delayed::Worker.logger.debug "Timeout in census api call: #{e.message} #{e.class.to_s}"
      page = nil
     rescue Exception =>  e
      tries += 1
      proxy = Proxy.find_available
      proxy.try(:increment_calls)
      puts "e.message: #{e.message}"
      Delayed::Worker.logger.debug "Trying again!" if tries <= 3
      if tries > 2
        proxy.try(:mark_error)
        proxy.try(:add_error_count)
      end
      retry if tries <= 3
      Delayed::Worker.logger.debug "No more attempt!"
      Delayed::Worker.logger.debug "Timeout in census api call: #{e.message} #{e.class.to_s}"
      ret = false
    end

    return ret 

  end

  private

    def fetch_geo_coordinates(user_id)

      service = Service.find_available(:google_geocoding)

      if service && !self.address.blank? && !self.geocoded?
        self.geocode
        service.increment_calls if self.geocoded?

        puts "$A$ renova #{self.proforma.renovation_sale_listing_partnership_cost}"

        save(validate: false)

        puts "$A$ renova #{self.proforma.renovation_sale_listing_partnership_cost}"
      end
    end



    def force_geo_coordinates()

      service = Service.find_available(:google_geocoding)

      if service && !self.address.blank? && !self.geocoded?
        self.geocode
        service.increment_calls if self.geocoded?

        # puts "$A$ renova #{self.proforma.renovation_sale_listing_partnership_cost}"
        save(validate: false)

        if self.latitude
          return true
        end
        # puts "$A$ renova #{self.proforma.renovation_sale_listing_partnership_cost}"
      end

      return false
    end

    def fetch_zestimate(force=false)

      begin
        service = Service.find_available(:zillow)
        puts "trying to get to zillow"
        if service
          puts "zillow service available"
          puts "self zillow id: #{self.zillow_id}"
          if ((self.zillow_id.nil?)  || (self.zillow_id == "1"))

            m = self.street_address.match(/\A\d+\s+\d+/)
            if m
              saddress = self.street_address.gsub(/\A\d+\s+/,"")
            else
              saddress = self.street_address
            end

            result = Property.zillow_api_call(:GetSearchResults,  service, address: saddress, citystatezip: self.city_state_zip)
            if result.nil?  || ( !result.nil? && result.at('//response/results').children.count < 1) || (!result.nil? && result.at('//response/results').children.count >= 1 && result.at('//response/results/result/zpid').nil?)
              result = Property.zillow_api_call(:GetSearchResults,  service, address: saddress, citystatezip: self.city_state_zip)
            end
            puts "track 1"
            puts "zillow result count: #{result.at('//response/results').children.count}" if result
            if ((result) && (result.at('//response/results').children.count >= 1))

              ary = result.search('//response/results/result/zpid').map do |v|
                v.try(:text).to_i
              end

              if ary && ary.count > 1


                zpid = Property.search_zpid_through_address_bar(self)

                # if zpid
                  # Property.test_zpid(self,"1")
                # end

                self.zillow_id =  zpid   if !zpid.nil?
                Delayed::Worker.logger.debug "-- grabbing zpid through address bar: #{self.zillow_id}"
              else
                Delayed::Worker.logger.debug "-- grabbing zpid: #{ary.max.to_s}"

                # if ary && ary.try(:max).try(:to_s)
                  # Property.test_zpid(self,"0")
                # end

                self.zillow_id = ary.max.to_s rescue nil
              end
            else
              puts "track 2"
              zpid = Property.search_zpid_through_address_bar(self)
              self.zillow_id =  zpid   if !zpid.nil?
            end
          
          end
          Delayed::Worker.logger.debug "post-process zillow id check: #{self.zillow_id}"
          Delayed::Worker.logger.debug "zillow updated at: #{self.zillow_updated_at}"

          if self.zillow_id && (force || self.zillow_updated_at.nil? || self.zillow_updated_at <= 1.month.ago)
            result = Property.zillow_api_call(:GetZestimate,  service , zpid: self.zillow_id, rentzestimate: true)

            if result.nil?
              result = Property.zillow_api_call(:GetZestimate,  service, zpid: self.zillow_id, rentzestimate: true)
            end

            if result
              Delayed::Worker.logger.debug "updating zillow"
              Delayed::Worker.logger.debug "fetching zillow sale estimate: #{result.at('//response/zestimate/amount').try(:text)}"
              self.zillow_sale_estimate = result.at('//response/zestimate/amount').try(:text)
              Delayed::Worker.logger.debug "fetching zillow rent estimate: #{result.at('//response/rentzestimate/amount').try(:text)}"
              self.zillow_rental_estimate = result.at('//response/rentzestimate/amount').try(:text)
              self.zillow_updated_at = DateTime.now
              save(validate: false)
            end

          end
        end
      rescue  Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        puts "error: #{e.message}"
      rescue  Exception =>  e
        puts "error: #{e.message}"
      end
    end


    def fetch_zestimate2(force=false)

      begin
        service = Service.find_available(:zillow)
        Delayed::Worker.logger.debug "trying to get to zillow"
        if service
          Delayed::Worker.logger.debug "zillow service available"
          Delayed::Worker.logger.debug "self zillow id: #{self.zillow_id}"
          if self.zillow_id.nil? || force 

            m = self.street_address.match(/\A\d+\s+\d+/)
            if m
              saddress = self.street_address.gsub(/\A\d+\s+/,"")
            else
              saddress = self.street_address
            end

            result = Property.zillow_api_call(:GetSearchResults,  service, address: saddress, citystatezip: self.city_state_zip)
            if result.nil?  || ( !result.nil? && result.at('//response/results').children.count < 1) || (!result.nil? && result.at('//response/results').children.count >= 1 && result.at('//response/results/result/zpid').nil?)
              result = Property.zillow_api_call(:GetSearchResults,  service, address: saddress, citystatezip: self.city_state_zip)
            end
            Delayed::Worker.logger.debug "track 1"
            Delayed::Worker.logger.debug "zillow result count: #{result.at('//response/results').children.count}" if result
            if ((result) && (result.at('//response/results').children.count >= 1))

              ary = result.search('//response/results/result/zpid').map do |v|
                v.try(:text).to_i
              end

              if ary && ary.count > 1


                zpid = Property.search_zpid_through_address_bar(self)

                # if zpid
                  # Property.test_zpid(self,"1")
                # end

                self.zillow_id =  zpid   if !zpid.nil?
                Delayed::Worker.logger.debug "-- grabbing zpid through address bar: #{self.zillow_id}"
              else
                Delayed::Worker.logger.debug "-- grabbing zpid: #{ary.max.to_s}"

                # if ary && ary.try(:max).try(:to_s)
                  # Property.test_zpid(self,"0")
                # end

                self.zillow_id = ary.max.to_s rescue nil
              end
            else
              Delayed::Worker.logger.debug "track 2"
              zpid = Property.search_zpid_through_address_bar(self)
              self.zillow_id =  zpid   if !zpid.nil?
            end
          
          end
          Delayed::Worker.logger.debug "post-process zillow id check: #{self.zillow_id}"
          Delayed::Worker.logger.debug "zillow updated at: #{self.zillow_updated_at}"

          if self.zillow_id && (force || self.zillow_updated_at.nil? || self.zillow_updated_at <= 1.month.ago)
            result = Property.zillow_api_call(:GetZestimate,  service , zpid: self.zillow_id, rentzestimate: true)

            if result.nil?
              result = Property.zillow_api_call(:GetZestimate,  service, zpid: self.zillow_id, rentzestimate: true)
            end

            if result
              Delayed::Worker.logger.debug "updating zillow"
              Delayed::Worker.logger.debug "fetching zillow sale estimate: #{result.at('//response/zestimate/amount').try(:text)}"
              self.zillow_sale_estimate = result.at('//response/zestimate/amount').try(:text)
              Delayed::Worker.logger.debug "fetching zillow rent estimate: #{result.at('//response/rentzestimate/amount').try(:text)}"
              self.zillow_rental_estimate = result.at('//response/rentzestimate/amount').try(:text)
              self.zillow_updated_at = DateTime.now
              save(validate: false)
            end

          end
        end
      rescue  Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        puts "error: #{e.message}"
      rescue  Exception =>  e
        puts "error: #{e.message}"
      end
    end

    def fetch_zestimate3(force=false)

      begin
        service = Service.find_available(:zillow)
        puts "trying to get to zillow"
        if service
          puts "zillow service available"
          puts "self zillow id: #{self.zillow_id}"
          if self.zillow_id.nil?

            result = Property.zillow_api_call(:GetSearchResults,  service, address: self.street_address, citystatezip: [self.state,self.zip].compact.join(" "))
            if result.nil?  || ( !result.nil? && result.at('//response/results').children.count < 1) || (!result.nil? && result.at('//response/results').children.count >= 1 && result.at('//response/results/result/zpid').nil?)
              result = Property.zillow_api_call(:GetSearchResults,  service, address: self.street_address, citystatezip: [self.state,self.zip].compact.join(" "))
            end

            puts "zillow result count: #{result.at('//response/results').children.count}" if result
            if ((result) && (result.at('//response/results').children.count >= 1))

              ary = result.search('//response/results/result/zpid').map do |v|
                v.try(:text).to_i
              end

              if ary && ary.count > 1


                zpid = Property.search_zpid_through_address_bar2(self)

                # if zpid
                  # Property.test_zpid(self,"1")
                # end

                self.zillow_id =  zpid   if !zpid.nil?
                Delayed::Worker.logger.debug "-- grabbing zpid through address bar: #{self.zillow_id}"
              else
                Delayed::Worker.logger.debug "-- grabbing zpid: #{ary.max.to_s}"

                # if ary && ary.try(:max).try(:to_s)
                  # Property.test_zpid(self,"0")
                # end

                self.zillow_id = ary.max.to_s rescue nil
              end

            end

          end
          Delayed::Worker.logger.debug "post-process zillow id check: #{self.zillow_id}"
          Delayed::Worker.logger.debug "zillow updated at: #{self.zillow_updated_at}"
          if self.zillow_id && (force || self.zillow_updated_at.nil? || self.zillow_updated_at <= 1.month.ago)
            result = Property.zillow_api_call(:GetZestimate,  service , zpid: self.zillow_id, rentzestimate: true)

            if result.nil?
              result = Property.zillow_api_call(:GetZestimate,  service, zpid: self.zillow_id, rentzestimate: true)
            end

            if result
              Delayed::Worker.logger.debug "updating zillow"
              Delayed::Worker.logger.debug "fetching zillow sale estimate: #{result.at('//response/zestimate/amount').try(:text)}"
              self.zillow_sale_estimate = result.at('//response/zestimate/amount').try(:text)
              Delayed::Worker.logger.debug "fetching zillow rent estimate: #{result.at('//response/rentzestimate/amount').try(:text)}"
              self.zillow_rental_estimate = result.at('//response/rentzestimate/amount').try(:text)
              self.zillow_updated_at = DateTime.now
              # save(validate: false)
            end

          end
        end
      rescue  Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        puts "error: #{e.message}"
      rescue  Exception =>  e
        puts "error: #{e.message}"
      end
    end


    def fetch_zestimate5(force=false)

      begin
        service = Service.find_available(:zillow)
        Delayed::Worker.logger.debug "trying to get to zillow"
        if service
          Delayed::Worker.logger.debug "zillow service available"
          Delayed::Worker.logger.debug "self zillow id: #{self.zillow_id}"
          if self.zillow_id.nil? || force
            street_address = [self.street_number, self.street_name].reject(&:blank?).join(" ")
            city_state_zip =  [self.city, [self.state,self.zip_code].reject(&:blank?).join(" ")].reject(&:blank?).join(", ")

            result = Property.zillow_api_call(:GetSearchResults,  service, address: street_address, citystatezip: city_state_zip)
            if result.nil?  || ( !result.nil? && result.at('//response/results').children.count < 1) || (!result.nil? && result.at('//response/results').children.count >= 1 && result.at('//response/results/result/zpid').nil?)
              result = Property.zillow_api_call(:GetSearchResults,  service, address: self.street_address, citystatezip: self.city_state_zip)
            end

            Delayed::Worker.logger.debug "zillow result count: #{result.at('//response/results').children.count}" if result
            if ((result) && (result.at('//response/results').children.count >= 1))

              ary = result.search('//response/results/result/zpid').map do |v|
                v.try(:text).to_i
              end

              if ary && ary.count > 1


                zpid = Property.search_zpid_through_address_bar(self)

                self.zillow_id =  zpid   if !zpid.nil?
                Delayed::Worker.logger.debug "-- grabbing zpid through address bar: #{self.zillow_id}"
              else
                Delayed::Worker.logger.debug "-- grabbing zpid: #{ary.max.to_s}"


                self.zillow_id = ary.max.to_s rescue nil
              end


            end

          end
          Delayed::Worker.logger.debug "post-process zillow id check: #{self.zillow_id}"
          Delayed::Worker.logger.debug "zillow updated at: #{self.zillow_updated_at}"
          if self.zillow_id && (force || self.zillow_updated_at.nil? || self.zillow_updated_at <= 1.month.ago)
            result = Property.zillow_api_call(:GetZestimate,  service , zpid: self.zillow_id, rentzestimate: true)

            if result.nil?
              result = Property.zillow_api_call(:GetZestimate,  service, zpid: self.zillow_id, rentzestimate: true)
            end

            if result
              Delayed::Worker.logger.debug "updating zillow"
              Delayed::Worker.logger.debug "fetching zillow sale estimate: #{result.at('//response/zestimate/amount').try(:text)}"
              self.zillow_sale_estimate = result.at('//response/zestimate/amount').try(:text)
              Delayed::Worker.logger.debug "fetching zillow rent estimate: #{result.at('//response/rentzestimate/amount').try(:text)}"
              self.zillow_rental_estimate = result.at('//response/rentzestimate/amount').try(:text)
              self.zillow_updated_at = DateTime.now

            end

          end
        end
      rescue  Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        Delayed::Worker.logger.debug "error: #{e.message}"
      rescue  Exception =>  e
        Delayed::Worker.logger.debug "error: #{e.message}"
      end
    end

    def fetch_zestimate6(force=false)

      begin
        service = Service.find_available(:zillow)
        puts "trying to get to zillow"
        if service
          puts "zillow service available"
          puts "self zillow id: #{self.zillow_id}"
          if ((self.zillow_id.nil?)  || (self.zillow_id == "1") || (force))

            m = self.street_address.match(/\A\d+\s+\d+/)
            if m
              saddress = self.street_address.gsub(/\A\d+\s+/,"")
            else
              saddress = self.street_address
            end

            result = Property.zillow_api_call(:GetSearchResults,  service, address: saddress, citystatezip: self.city_state_zip)
            if result.nil?  || ( !result.nil? && result.at('//response/results').children.count < 1) || (!result.nil? && result.at('//response/results').children.count >= 1 && result.at('//response/results/result/zpid').nil?)
              result = Property.zillow_api_call(:GetSearchResults,  service, address: saddress, citystatezip: self.city_state_zip)
            end
            puts "track 1"
            puts "zillow result count: #{result.at('//response/results').children.count}" if result
            if ((result) && (result.at('//response/results').children.count >= 1))

              ary = result.search('//response/results/result/zpid').map do |v|
                v.try(:text).to_i
              end

              if ary && ary.count > 1


                zpid = Property.search_zpid_through_address_bar(self)

                # if zpid
                  # Property.test_zpid(self,"1")
                # end

                self.zillow_id =  zpid   if !zpid.nil?
                Delayed::Worker.logger.debug "-- grabbing zpid through address bar: #{self.zillow_id}"
              else
                Delayed::Worker.logger.debug "-- grabbing zpid: #{ary.max.to_s}"

                # if ary && ary.try(:max).try(:to_s)
                  # Property.test_zpid(self,"0")
                # end

                self.zillow_id = ary.max.to_s rescue nil
              end
            else
              puts "track 2"
              zpid = Property.search_zpid_through_address_bar(self)
              self.zillow_id =  zpid   if !zpid.nil?
            end
          
          end
          Delayed::Worker.logger.debug "post-process zillow id check: #{self.zillow_id}"
          Delayed::Worker.logger.debug "zillow updated at: #{self.zillow_updated_at}"

          if self.zillow_id && (force || self.zillow_updated_at.nil? || self.zillow_updated_at <= 1.month.ago)
            result = Property.zillow_api_call(:GetZestimate,  service , zpid: self.zillow_id, rentzestimate: true)

            if result.nil?
              result = Property.zillow_api_call(:GetZestimate,  service, zpid: self.zillow_id, rentzestimate: true)
            end

            if result
              Delayed::Worker.logger.debug "updating zillow"
              Delayed::Worker.logger.debug "fetching zillow sale estimate: #{result.at('//response/zestimate/amount').try(:text)}"
              self.zillow_sale_estimate = result.at('//response/zestimate/amount').try(:text)
              Delayed::Worker.logger.debug "fetching zillow rent estimate: #{result.at('//response/rentzestimate/amount').try(:text)}"
              self.zillow_rental_estimate = result.at('//response/rentzestimate/amount').try(:text)
              self.zillow_updated_at = DateTime.now
              save(validate: false)
            end

          end
        end
      rescue  Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        puts "error: #{e.message}"
      rescue  Exception =>  e
        puts "error: #{e.message}"
      end
    end

    def self.zillow_api_call(method, service, args)

       tries = 0
       page = nil

       begin
         agent = Mechanize.new  { |ag|
           ag.open_timeout = 26
           ag.read_timeout = 26
           ag.idle_timeout = 0.9
           ag.retry_change_requests = true
         }

         proxy = Proxy.find_available

         if proxy
            ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
           # agent.set_proxy('198.23.223.133',80,'glt','gltproxy') if !Rails.env.development?
           # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
           agent.pluggable_parser.default = Mechanize::Page
           Delayed::Worker.logger.debug "getting http://www.zillow.com/webservice/#{method}.htm?zws-id=#{service.key}, #{args}"


           page = agent.get("http://www.zillow.com/webservice/#{method}.htm?zws-id=#{service.key}", args)
           service.increment_calls
           proxy.increment_calls
           proxy.rem_error_count

         end

       rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3

        Delayed::Worker.logger.debug "No more attempt!"
        Delayed::Worker.logger.debug "Timeout in zillow api call: #{e.message} #{e.class.to_s}"
        page = nil
       rescue Exception =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        Delayed::Worker.logger.debug "Timeout in zillow api call: #{e.message} #{e.class.to_s}"
        page = nil
       end

       Delayed::Worker.logger.debug "message code: #{page.at('//message/code').try(:text)}" if (page)
       page if (page) && (page.at('//message/code').try(:text) == "0")
    end

    
    
    def self.search_zpid_through_address_bar(prop)

      tries = 0
      page = nil

      begin
        agent = Mechanize.new  { |ag|
          ag.open_timeout = 26
          ag.read_timeout = 26
          ag.idle_timeout = 0.9
          ag.retry_change_requests = true
        }

        proxy = Proxy.find_available
        
        
         ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
        # agent.set_proxy('198.23.223.133',80,'glt','gltproxy') if !Rails.env.development?
        # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        page = agent.get 'http://www.zillow.com/'

        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
          # puts "~~page.body #{page.body}"
        form = page.form_with(:name => "formSearchBar")
          # puts "~~address #{prop.address}"

        m = prop.street_address.match(/\A\d+\s+\d+/)

        if m
          form.citystatezip = prop.address.gsub(/\A\d+\s+/,"")  #street number&street name, TX zip
        else
          form.citystatezip = prop.address  #street number&street name, TX zip
        end

        proxy = Proxy.find_available
        page = form.submit
        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
        agent.cookie_jar.clear!
        # href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
        m = page.body.match(/div\s+id="initial-hdp-jsonp-(\d+)/)
        
        if !m || (m.length <= 0)
          puts "into new check"
          proxy = Proxy.find_available
          
         ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
          # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
          page = agent.get 'http://www.zillow.com/'
  
          proxy.try(:increment_calls)
          proxy.try(:rem_error_count)
            # puts "~~page.body #{page.body}"
          form = page.form_with(:name => "formSearchBar")
            # puts "~~address #{prop.address}"
  
         
          form.citystatezip = [[prop.street_number,prop.street_direction,prop.street_name].reject(&:blank?).join(" "),[prop.state,prop.zip].reject(&:blank?).join(" ")].reject(&:blank?).join(", ")
          
          puts "city state zip: #{form.citystatezip}"
          
          proxy = Proxy.find_available
          page = form.submit
          proxy.try(:increment_calls)
          proxy.try(:rem_error_count)
          agent.cookie_jar.clear!
          # href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
          puts "page.body: #{page.body}"
          # m = page.body.match(/div\s+id="initial-hdp-jsonp-(\d+)/)
          m = page.body.match(/li\s+id="save-menu"\s+data-zpid="(\d+)"/)
        end
        
        if !m || (m.length <= 0)
          puts "into new check"
          proxy = Proxy.find_available
          
        ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
          # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
          page = agent.get 'http://www.zillow.com/'
  
          proxy.try(:increment_calls)
          proxy.try(:rem_error_count)
            # puts "~~page.body #{page.body}"
          form = page.form_with(:name => "formSearchBar")
            # puts "~~address #{prop.address}"
  
         
          form.citystatezip = [[prop.street_number,prop.street_name].reject(&:blank?).join(" "),[prop.state,prop.zip].reject(&:blank?).join(" ")].reject(&:blank?).join(", ")
          
          puts "city state zip: #{form.citystatezip}"
          
          proxy = Proxy.find_available
          page = form.submit
          proxy.try(:increment_calls)
          proxy.try(:rem_error_count)
          agent.cookie_jar.clear!
          # href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
          puts "page.body: #{page.body}"
          # m = page.body.match(/div\s+id="initial-hdp-jsonp-(\d+)/)
          m = page.body.match(/li\s+id="save-menu"\s+data-zpid="(\d+)"/)
        end
        
        
        if m && m.length > 0
          Delayed::Worker.logger.debug "m: #{m}"
          return m.try(:[],1)

        else

          suffixes = HashWithCaseInsensitiveAccess.new( {"alley"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "allee"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "aly"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "ally"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "annex"=>["ANNEX", "ANEX", "ANX"], "anex"=>["ANNEX", "ANEX", "ANX"], "anx"=>["ANNEX", "ANEX", "ANX"], "arcade"=>["ARCADE", "ARC"], "arc"=>["ARCADE", "ARC"], "avenue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "av"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "ave"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "aven"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avenu"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avn"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avnue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "bayoo"=>["BAYOO", "BYU", "BAYOU"], "byu"=>["BAYOO", "BYU", "BAYOU"], "bayou"=>["BAYOO", "BYU", "BAYOU"], "beach"=>["BEACH", "BCH"], "bch"=>["BEACH", "BCH"], "bend"=>["BEND", "BND"], "bnd"=>["BEND", "BND"], "bluff"=>["BLUFF", "BLF", "BLUF"], "blf"=>["BLUFF", "BLF", "BLUF"], "bluf"=>["BLUFF", "BLF", "BLUF"], "bluffs"=>["BLUFFS", "BLFS"], "blfs"=>["BLUFFS", "BLFS"], "bottom"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bot"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "btm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bottm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "boulevard"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "blvd"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boul"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boulv"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "branch"=>["BRANCH", "BR", "BRNCH"], "br"=>["BRANCH", "BR", "BRNCH"], "brnch"=>["BRANCH", "BR", "BRNCH"], "bridge"=>["BRIDGE", "BRDGE", "BRG"], "brdge"=>["BRIDGE", "BRDGE", "BRG"], "brg"=>["BRIDGE", "BRDGE", "BRG"], "brook"=>["BROOK", "BRK"], "brk"=>["BROOK", "BRK"], "brooks"=>["BROOKS", "BRKS"], "brks"=>["BROOKS", "BRKS"], "burg"=>["BURG", "BG"], "bg"=>["BURG", "BG"], "burgs"=>["BURGS", "BGS"], "bgs"=>["BURGS", "BGS"], "bypass"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byp"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypa"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypas"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byps"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "camp"=>["CAMP", "CP", "CMP"], "cp"=>["CAMP", "CP", "CMP"], "cmp"=>["CAMP", "CP", "CMP"], "canyon"=>["CANYON", "CANYN", "CYN", "CNYN"], "canyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cnyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cape"=>["CAPE", "CPE"], "cpe"=>["CAPE", "CPE"], "causeway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "cswy"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "causway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "center"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cen"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "ctr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cent"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centre"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cnter"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cntr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centers"=>["CENTERS", "CTRS"], "ctrs"=>["CENTERS", "CTRS"], "circle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "cir"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circ"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circles"=>["CIRCLES", "CIRS"], "cirs"=>["CIRCLES", "CIRS"], "cliff"=>["CLIFF", "CLF"], "clf"=>["CLIFF", "CLF"], "cliffs"=>["CLIFFS", "CLFS"], "clfs"=>["CLIFFS", "CLFS"], "club"=>["CLUB", "CLB"], "clb"=>["CLUB", "CLB"], "common"=>["COMMON", "CMN"], "cmn"=>["COMMON", "CMN"], "corner"=>["CORNER", "COR"], "cor"=>["CORNER", "COR"], "corners"=>["CORNERS", "CORS"], "cors"=>["CORNERS", "CORS"], "course"=>["COURSE", "CRSE"], "crse"=>["COURSE", "CRSE"], "court"=>["COURT", "CT", "CRT"], "ct"=>["COURTS", "CTS", "CT"], "crt"=>["COURT", "CT", "CRT"], "courts"=>["COURTS", "CTS", "CT"], "cts"=>["COURTS", "CTS", "CT"], "cove"=>["COVE", "CV"], "cv"=>["COVE", "CV"], "coves"=>["COVES", "CVS"], "cvs"=>["COVES", "CVS"], "creek"=>["CREEK", "CK", "CRK", "CR"], "ck"=>["CREEK", "CK", "CRK", "CR"], "crk"=>["CREEK", "CK", "CRK", "CR"], "cr"=>["CREEK", "CK", "CRK", "CR"], "crescent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crecent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cres"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cresent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crscnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crest"=>["CREST", "CRST"], "crst"=>["CREST", "CRST"], "crossing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "xing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssng"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crossroad"=>["CROSSROAD", "XRD"], "xrd"=>["CROSSROAD", "XRD"], "curve"=>["CURVE", "CURV"], "curv"=>["CURVE", "CURV"], "dale"=>["DALE", "DL"], "dl"=>["DALE", "DL"], "dam"=>["DAM", "DM"], "dm"=>["DAM", "DM"], "divide"=>["DIVIDE", "DIV", "DV", "DVD"], "div"=>["DIVIDE", "DIV", "DV", "DVD"], "dv"=>["DIVIDE", "DIV", "DV", "DVD"], "dvd"=>["DIVIDE", "DIV", "DV", "DVD"], "drive"=>["DRIVE", "DR", "DRIV", "DRV"], "dr"=>["DRIVE", "DR", "DRIV", "DRV"], "driv"=>["DRIVE", "DR", "DRIV", "DRV"], "drv"=>["DRIVE", "DR", "DRIV", "DRV"], "drives"=>["DRIVES", "DRS"], "drs"=>["DRIVES", "DRS"], "estate"=>["ESTATE", "EST"], "est"=>["ESTATE", "EST"], "estates"=>["ESTATES", "ESTS"], "ests"=>["ESTATES", "ESTS"], "expressway"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "exp"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expy"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expr"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "express"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expw"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "extension"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "ext"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extnsn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extensions"=>["EXTENSIONS", "EXTS"], "exts"=>["EXTENSIONS", "EXTS"], "fall"=>["FALL"], "falls"=>["FALLS", "FLS"], "fls"=>["FALLS", "FLS"], "ferry"=>["FERRY", "FRY", "FRRY"], "fry"=>["FERRY", "FRY", "FRRY"], "frry"=>["FERRY", "FRY", "FRRY"], "field"=>["FIELD", "FLD"], "fld"=>["FIELD", "FLD"], "fields"=>["FIELDS", "FLDS"], "flds"=>["FIELDS", "FLDS"], "flat"=>["FLAT", "FLT"], "flt"=>["FLAT", "FLT"], "flats"=>["FLATS", "FLTS"], "flts"=>["FLATS", "FLTS"], "ford"=>["FORD", "FRD"], "frd"=>["FORD", "FRD"], "fords"=>["FORDS", "FRDS"], "frds"=>["FORDS", "FRDS"], "forest"=>["FOREST", "FRST", "FORESTS"], "frst"=>["FOREST", "FRST", "FORESTS"], "forests"=>["FOREST", "FRST", "FORESTS"], "forge"=>["FORGE", "FORG", "FRG"], "forg"=>["FORGE", "FORG", "FRG"], "frg"=>["FORGE", "FORG", "FRG"], "forges"=>["FORGES", "FRGS"], "frgs"=>["FORGES", "FRGS"], "fork"=>["FORK", "FRK"], "frk"=>["FORK", "FRK"], "forks"=>["FORKS", "FRKS"], "frks"=>["FORKS", "FRKS"], "fort"=>["FORT", "FT", "FRT"], "ft"=>["FORT", "FT", "FRT"], "frt"=>["FORT", "FT", "FRT"], "freeway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "fwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "freewy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "garden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardens"=>["GARDENS", "GDNS", "GRDNS"], "gdns"=>["GARDENS", "GDNS", "GRDNS"], "grdns"=>["GARDENS", "GDNS", "GRDNS"], "gateway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtwy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatewy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "glen"=>["GLEN", "GLN"], "gln"=>["GLEN", "GLN"], "glens"=>["GLENS", "GLNS"], "glns"=>["GLENS", "GLNS"], "green"=>["GREEN", "GRN"], "grn"=>["GREEN", "GRN"], "greens"=>["GREENS", "GRNS"], "grns"=>["GREENS", "GRNS"], "grove"=>["GROVE", "GROV", "GRV"], "grov"=>["GROVE", "GROV", "GRV"], "grv"=>["GROVE", "GROV", "GRV"], "groves"=>["GROVES", "GRVS"], "grvs"=>["GROVES", "GRVS"], "harbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harb"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hrbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbors"=>["HARBORS", "HBRS"], "hbrs"=>["HARBORS", "HBRS"], "haven"=>["HAVEN", "HVN", "HAVN"], "hvn"=>["HAVEN", "HVN", "HAVN"], "havn"=>["HAVEN", "HVN", "HAVN"], "heights"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "height"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hgts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "ht"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "highway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "highwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hill"=>["HILL", "HL"], "hl"=>["HILL", "HL"], "hills"=>["HILLS", "HLS"], "hls"=>["HILLS", "HLS"], "hollow"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hllw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hollows"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holws"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "inlet"=>["INLET", "INLT"], "inlt"=>["INLET", "INLT"], "island"=>["ISLAND", "IS", "ISLND"], "is"=>["ISLAND", "IS", "ISLND"], "islnd"=>["ISLAND", "IS", "ISLND"], "islands"=>["ISLANDS", "ISS", "ISLNDS"], "iss"=>["ISLANDS", "ISS", "ISLNDS"], "islnds"=>["ISLANDS", "ISS", "ISLNDS"], "isle"=>["ISLE", "ISLES"], "isles"=>["ISLE", "ISLES"], "junction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jct"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "juncton"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctions"=>["JUNCTIONS", "JCTNS", "JCTS"], "jctns"=>["JUNCTIONS", "JCTNS", "JCTS"], "jcts"=>["JUNCTIONS", "JCTNS", "JCTS"], "key"=>["KEY", "KY"], "ky"=>["KEY", "KY"], "keys"=>["KEYS", "KYS"], "kys"=>["KEYS", "KYS"], "knoll"=>["KNOLL", "KNL", "KNOL"], "knl"=>["KNOLL", "KNL", "KNOL"], "knol"=>["KNOLL", "KNL", "KNOL"], "knolls"=>["KNOLLS", "KNLS"], "knls"=>["KNOLLS", "KNLS"], "lake"=>["LAKE", "LK"], "lk"=>["LAKE", "LK"], "lakes"=>["LAKES", "LKS"], "lks"=>["LAKES", "LKS"], "land"=>["LAND"], "landing"=>["LANDING", "LNDG", "LNDNG"], "lndg"=>["LANDING", "LNDG", "LNDNG"], "lndng"=>["LANDING", "LNDG", "LNDNG"], "lane"=>["LANE", "LA", "LN", "LANES"], "la"=>["LANE", "LA", "LN", "LANES"], "ln"=>["LANE", "LA", "LN", "LANES"], "lanes"=>["LANE", "LA", "LN", "LANES"], "light"=>["LIGHT", "LGT"], "lgt"=>["LIGHT", "LGT"], "lights"=>["LIGHTS", "LGTS"], "lgts"=>["LIGHTS", "LGTS"], "loaf"=>["LOAF", "LF"], "lf"=>["LOAF", "LF"], "lock"=>["LOCK", "LCK"], "lck"=>["LOCK", "LCK"], "locks"=>["LOCKS", "LCKS"], "lcks"=>["LOCKS", "LCKS"], "lodge"=>["LODGE", "LDG", "LDGE", "LODG"], "ldg"=>["LODGE", "LDG", "LDGE", "LODG"], "ldge"=>["LODGE", "LDG", "LDGE", "LODG"], "lodg"=>["LODGE", "LDG", "LDGE", "LODG"], "loop"=>["LOOP", "LOOPS"], "loops"=>["LOOP", "LOOPS"], "mall"=>["MALL"], "manor"=>["MANOR", "MNR"], "mnr"=>["MANOR", "MNR"], "manors"=>["MANORS", "MNRS"], "mnrs"=>["MANORS", "MNRS"], "meadow"=>["MEADOW", "MDW"], "mdw"=>["MEADOW", "MDW"], "meadows"=>["MEADOWS", "MDWS", "MEDOWS"], "mdws"=>["MEADOWS", "MDWS", "MEDOWS"], "medows"=>["MEADOWS", "MDWS", "MEDOWS"], "mews"=>["MEWS"], "mill"=>["MILL", "ML"], "ml"=>["MILL", "ML"], "mills"=>["MILLS", "MLS"], "mls"=>["MILLS", "MLS"], "mission"=>["MISSION", "MSN", "MISSN", "MSSN"], "msn"=>["MISSION", "MSN", "MISSN", "MSSN"], "missn"=>["MISSION", "MSN", "MISSN", "MSSN"], "mssn"=>["MISSION", "MSN", "MISSN", "MSSN"], "motorway"=>["MOTORWAY", "MTWY"], "mtwy"=>["MOTORWAY", "MTWY"], "mount"=>["MOUNT", "MNT", "MT"], "mnt"=>["MOUNT", "MNT", "MT"], "mt"=>["MOUNT", "MNT", "MT"], "mountain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountains"=>["MOUNTAINS", "MNTNS", "MTNS"], "mntns"=>["MOUNTAINS", "MNTNS", "MTNS"], "mtns"=>["MOUNTAINS", "MNTNS", "MTNS"], "neck"=>["NECK", "NCK"], "nck"=>["NECK", "NCK"], "orchard"=>["ORCHARD", "ORCH", "ORCHRD"], "orch"=>["ORCHARD", "ORCH", "ORCHRD"], "orchrd"=>["ORCHARD", "ORCH", "ORCHRD"], "oval"=>["OVAL", "OVL"], "ovl"=>["OVAL", "OVL"], "overpass"=>["OVERPASS", "OPAS"], "opas"=>["OVERPASS", "OPAS"], "park"=>["PARKS", "PARK"], "pk"=>["PARK", "PK", "PRK"], "prk"=>["PARK", "PK", "PRK"], "parks"=>["PARKS", "PARK"], "parkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkwy"=>["PARKWAYS", "PKWY", "PKWYS"], "parkwy"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pky"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "parkways"=>["PARKWAYS", "PKWY", "PKWYS"], "pkwys"=>["PARKWAYS", "PKWY", "PKWYS"], "pass"=>["PASS"], "passage"=>["PASSAGE", "PSGE"], "psge"=>["PASSAGE", "PSGE"], "path"=>["PATH", "PATHS"], "paths"=>["PATH", "PATHS"], "pike"=>["PIKE", "PIKES"], "pikes"=>["PIKE", "PIKES"], "pine"=>["PINE", "PNE"], "pne"=>["PINE", "PNE"], "pines"=>["PINES", "PNES"], "pnes"=>["PINES", "PNES"], "place"=>["PLACE", "PL"], "pl"=>["PLACE", "PL"], "plain"=>["PLAIN", "PLN"], "pln"=>["PLAIN", "PLN"], "plains"=>["PLAINS", "PLAINES", "PLNS"], "plaines"=>["PLAINS", "PLAINES", "PLNS"], "plns"=>["PLAINS", "PLAINES", "PLNS"], "plaza"=>["PLAZA", "PLZ", "PLZA"], "plz"=>["PLAZA", "PLZ", "PLZA"], "plza"=>["PLAZA", "PLZ", "PLZA"], "point"=>["POINT", "PT"], "pt"=>["POINT", "PT"], "points"=>["POINTS", "PTS"], "pts"=>["POINTS", "PTS"], "port"=>["PORT", "PRT"], "prt"=>["PORT", "PRT"], "ports"=>["PORTS", "PRTS"], "prts"=>["PORTS", "PRTS"], "prairie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "pr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prarie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "radial"=>["RADIAL", "RAD", "RADL", "RADIEL"], "rad"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radl"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radiel"=>["RADIAL", "RAD", "RADL", "RADIEL"], "ramp"=>["RAMP"], "ranch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "ranches"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnchs"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rapid"=>["RAPID", "RPD"], "rpd"=>["RAPID", "RPD"], "rapids"=>["RAPIDS", "RPDS"], "rpds"=>["RAPIDS", "RPDS"], "rest"=>["REST", "RST"], "rst"=>["REST", "RST"], "ridge"=>["RIDGE", "RDG", "RDGE"], "rdg"=>["RIDGE", "RDG", "RDGE"], "rdge"=>["RIDGE", "RDG", "RDGE"], "ridges"=>["RIDGES", "RDGS"], "rdgs"=>["RIDGES", "RDGS"], "river"=>["RIVER", "RIV", "RIVR", "RVR"], "riv"=>["RIVER", "RIV", "RIVR", "RVR"], "rivr"=>["RIVER", "RIV", "RIVR", "RVR"], "rvr"=>["RIVER", "RIV", "RIVR", "RVR"], "road"=>["ROAD", "RD"], "rd"=>["ROAD", "RD"], "roads"=>["ROADS", "RDS"], "rds"=>["ROADS", "RDS"], "route"=>["ROUTE", "RTE"], "rte"=>["ROUTE", "RTE"], "row"=>["ROW"], "rue"=>["RUE"], "run"=>["RUN"], "shoal"=>["SHOAL", "SHL"], "shl"=>["SHOAL", "SHL"], "shoals"=>["SHOALS", "SHLS"], "shls"=>["SHOALS", "SHLS"], "shore"=>["SHORE", "SHOAR", "SHR"], "shoar"=>["SHORE", "SHOAR", "SHR"], "shr"=>["SHORE", "SHOAR", "SHR"], "shores"=>["SHORES", "SHOARS", "SHRS"], "shoars"=>["SHORES", "SHOARS", "SHRS"], "shrs"=>["SHORES", "SHOARS", "SHRS"], "skyway"=>["SKYWAY", "SKWY"], "skwy"=>["SKYWAY", "SKWY"], "spring"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spg"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "sprng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "springs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spgs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "sprngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spur"=>["SPURS", "SPUR"], "spurs"=>["SPURS", "SPUR"], "square"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sq"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqr"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqre"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squ"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squares"=>["SQUARES", "SQRS", "SQS"], "sqrs"=>["SQUARES", "SQRS", "SQS"], "sqs"=>["SQUARES", "SQRS", "SQS"], "station"=>["STATION", "STA", "STATN", "STN"], "sta"=>["STATION", "STA", "STATN", "STN"], "statn"=>["STATION", "STA", "STATN", "STN"], "stn"=>["STATION", "STA", "STATN", "STN"], "stravenue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stra"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strav"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strave"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "straven"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stravn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvnue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stream"=>["STREAM", "STRM", "STREME"], "strm"=>["STREAM", "STRM", "STREME"], "streme"=>["STREAM", "STRM", "STREME"], "street"=>["STREET", "ST", "STR", "STRT"], "st"=>["STREET", "ST", "STR", "STRT"], "str"=>["STREET", "ST", "STR", "STRT"], "strt"=>["STREET", "ST", "STR", "STRT"], "streets"=>["STREETS", "STS"], "sts"=>["STREETS", "STS"], "summit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "smt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumitt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "terrace"=>["TERRACE", "TER", "TERR"], "ter"=>["TERRACE", "TER", "TERR"], "terr"=>["TERRACE", "TER", "TERR"], "throughway"=>["THROUGHWAY", "TRWY"], "trwy"=>["THROUGHWAY", "TRWY"], "trace"=>["TRACE", "TRCE", "TRACES"], "trce"=>["TRACE", "TRCE", "TRACES"], "traces"=>["TRACE", "TRCE", "TRACES"], "track"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trak"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "tracks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trk"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trafficway"=>["TRAFFICWAY", "TRFY"], "trfy"=>["TRAFFICWAY", "TRFY"], "trail"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tr"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trl"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trails"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trls"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tunnel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunls"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnels"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "turnpike"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpke"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "turnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "underpass"=>["UNDERPASS", "UPAS"], "upas"=>["UNDERPASS", "UPAS"], "union"=>["UNION", "UN"], "un"=>["UNION", "UN"], "unions"=>["UNIONS", "UNS"], "uns"=>["UNIONS", "UNS"], "valley"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vally"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vlly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "valleys"=>["VALLEYS", "VLYS"], "vlys"=>["VALLEYS", "VLYS"], "viaduct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "vdct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "via"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "viadct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "view"=>["VIEW", "VW"], "vw"=>["VIEW", "VW"], "views"=>["VIEWS", "VWS"], "vws"=>["VIEWS", "VWS"], "village"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vill"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vlg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villag"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villiage"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villages"=>["VILLAGES", "VLGS"], "vlgs"=>["VILLAGES", "VLGS"], "ville"=>["VILLE", "VL"], "vl"=>["VILLE", "VL"], "vista"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vis"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vist"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vst"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vsta"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "walk"=>["WALKS", "WALK"], "walks"=>["WALKS", "WALK"], "wall"=>["WALL"], "way"=>["WAY", "WY"], "wy"=>["WAY", "WY"], "ways"=>["WAYS"], "well"=>["WELL", "WL"], "wl"=>["WELL", "WL"], "wells"=>["WELLS", "WLS"], "wls"=>["WELLS", "WLS"]})

          elems = prop.street_name.split(" ")
          if (elems && elems.length > 2)
            max_length = elems.length
            Delayed::Worker.logger.debug "suffx 1"
            sfx = suffixes[elems[max_length - 2]]
            mrst = nil
            if (sfx && sfx.length > 0)

              sfx.each do |sf|
                #{prop.street_number} #{elems}
                erst = elems.inject([]) do |rst,e|

                  if e == elems[max_length - 2]
                    rst << sf
                  else
                    rst << e
                  end
                  rst
                end

                new_address = [[prop.street_number,prop.street_direction,erst.join(" ")].reject(&:blank?).join(" "),prop.city_state_zip].reject(&:blank?).join(", ")
                proxy = Proxy.find_available
                
          ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
                # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?

                page = agent.get 'http://www.zillow.com/'
                proxy.try(:increment_calls)
                proxy.try(:rem_error_count)

                form = page.form_with(:name => "formSearchBar")
                  # puts "~~address #{prop.address}"
                form.citystatezip = new_address  #street number&street name, TX zip

                proxy = Proxy.find_available
                page = form.submit
                agent.cookie_jar.clear!
                proxy.try(:increment_calls)
                proxy.try(:rem_error_count)

                m = page.body.match(/div\s+id="initial-hdp-jsonp-(\d+)/)

                if m && m.length > 0
                  Delayed::Worker.logger.debug "m: #{m}"
                  mrst = m.try(:[],1)
                  Delayed::Worker.logger.debug "street_address: zpid: #{mrst}"
                  return mrst





                end


              end

              if mrst.nil?
                Delayed::Worker.logger.debug "suffx 2"
                sfx = suffixes["#{elems[max_length - 2]}s"]
                Delayed::Worker.logger.debug "suffx 2: #{sfx}"
                if (sfx && sfx.length > 0)
                  sfx.each do |sf|
                    #{prop.street_number} #{elems}
                    erst = elems.inject([]) do |rst,e|
                      if e == "#{elems[max_length - 2]}"
                        Delayed::Worker.logger.debug "suffx 2 replacing #{sf}"
                        rst << sf
                      else
                        rst << e
                      end
                      rst
                    end
                    new_address = [[prop.street_number,prop.street_direction,erst.join(" ")].reject(&:blank?).join(" "),prop.city_state_zip].reject(&:blank?).join(", ")
                    Delayed::Worker.logger.debug "suffx 2 new address: #{new_address}"
                    proxy = Proxy.find_available
                   
                 ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
                    # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
                    page = agent.get 'http://www.zillow.com/'
                    proxy.try(:increment_calls)
                    proxy.try(:rem_error_count)

                    form = page.form_with(:name => "formSearchBar")
                      # puts "~~address #{prop.address}"
                    form.citystatezip = new_address  #street number&street name, TX zip

                    proxy = Proxy.find_available
                    page = form.submit
                    agent.cookie_jar.clear!
                    proxy.try(:increment_calls)
                    proxy.try(:rem_error_count)

                    m = page.body.match(/div\s+id="initial-hdp-jsonp-(\d+)/)

                    if m && m.length > 0
                      Delayed::Worker.logger.debug "m: #{m}"
                      Delayed::Worker.logger.debug "zpid: #{m.try(:[],1)}"
                      return m.try(:[],1)


                    end


                  end

                end
              end


            else
              return nil
            end
          else
            return nil
          end

          return nil

        end

      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3


      rescue Exception =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        Delayed::Worker.logger.debug "Timeout in zillow api call: #{e.message} #{e.class.to_s}"

       end

    end

    def self.search_zpid_through_address_bar2(prop)

      tries = 0
      page = nil

      begin
        agent = Mechanize.new  { |ag|
          ag.open_timeout = 26
          ag.read_timeout = 26
          ag.idle_timeout = 0.9
          ag.retry_change_requests = true
        }

        proxy = Proxy.find_available
        agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        page = agent.get 'http://www.zillow.com/'
        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
          # puts "~~page.body #{page.body}"
        form = page.form_with(:name => "formSearchBar")
          # puts "~~address #{prop.address}"
        form.citystatezip = prop.address  #street number&street name, TX zip

        proxy = Proxy.find_available
        page = form.submit
        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)

        href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil

        if href

          m = href.to_s.match(/(\d+)_zpid(\/)?/)
          return m.try(:[],1)

        else
          sa = prop.street_address
          sl = sa.scan(" ").length rescue 0

          if sa.blank? || sl == 0
            return nil
          end

          sp = sa.split(" ")
          key = sp.last
          new_street_name = sp.delete_if {|e| e == key}.join(" ")

          new_street_address = new_street_name
          state_zip = [prop.state,prop.zip].reject(&:blank?).join(" ")
          new_address = [new_street_address,state_zip].reject(&:blank?).join(", ")

          proxy = Proxy.find_available
          page = agent.get 'http://www.zillow.com/'
          proxy.try(:increment_calls)
          proxy.try(:rem_error_count)

          form = page.form_with(:name => "formSearchBar")
          # puts "~~address #{prop.address}"
          form.citystatezip = new_address  #street number&street name, TX zip

          proxy = Proxy.find_available
          page = form.submit
          proxy.try(:increment_calls)
          proxy.try(:rem_error_count)

          href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil

          m = href.to_s.match(/(\d+)_zpid(\/)?/)
          return m.try(:[],1)
        end

      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3


      rescue Exception =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3
        Delayed::Worker.logger.debug "No more attempt!"
        Delayed::Worker.logger.debug "Timeout in zillow api call: #{e.message} #{e.class.to_s}"

       end

    end
    
    def self.search_zpid(ad)

      tries = 0
      page = nil

      begin
        agent = Mechanize.new  { |ag|
          ag.open_timeout = 26
          ag.read_timeout = 26
          ag.idle_timeout = 0.9
          ag.retry_change_requests = true
        }

        proxy = Proxy.find_available
        
         ips = GoodIp.find_working_ips
           if (!ips || (ips && (ips.count < 6)))
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end
          case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
          when 0
            agent.set_proxy(ips[0],80,'glt','gltproxy') if !Rails.env.development?
          when 1
            agent.set_proxy(ips[1],80,'glt','gltproxy') if !Rails.env.development?
          when 2
            agent.set_proxy(ips[2],80,'glt','gltproxy') if !Rails.env.development? 
          when 3
            agent.set_proxy(ips[3],80,'glt','gltproxy') if !Rails.env.development?
          when 4
            agent.set_proxy(ips[4],80,'glt','gltproxy') if !Rails.env.development?
          when 5
            agent.set_proxy(ips[5],80,'glt','gltproxy') if !Rails.env.development?
          end
        # agent.set_proxy('198.23.223.133',80,'glt','gltproxy') if !Rails.env.development?
        # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        page = agent.get 'http://www.zillow.com/'

        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
          # puts "~~page.body #{page.body}"
        form = page.form_with(:name => "formSearchBar")
          # puts "~~address #{ad}"

        m = ad.match(/\A\d+\s+\d+/)

        if m
          form.citystatezip = ad.gsub(/\A\d+\s+/,"")  #street number&street name, TX zip
        else
          form.citystatezip = ad  #street number&street name, TX zip
        end

        proxy = Proxy.find_available
        page = form.submit
        proxy.try(:increment_calls)
        proxy.try(:rem_error_count)
        agent.cookie_jar.clear!
        # href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
        # puts "~~page.body #{page.body}"
        m = page.body.match(/div\s+id="initial-hdp-jsonp-(\d+)/)

                

        if m && m.length > 0
          puts "address: #{ad}/m: #{m.try(:[],1)}"
          return m.try(:[],1)       
        end

        m = page.body.match(/li\s+id="save-menu"\s+data-zpid="(\d+)"/)


        if m && m.length > 0
          puts "address: #{ad}/m: #{m.try(:[],1)}"
          return m.try(:[],1)
        else
          puts "address: #{ad}/m: "
          return nil
        end
        

      rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        # Delayed::Worker.logger.debug "Trying again!" if tries <= 3
            if tries > 2
              proxy.try(:mark_error)
              proxy.try(:add_error_count)
            end
            retry if tries <= 3


      rescue Exception =>  e
        tries += 1
        proxy = Proxy.find_available
        proxy.try(:increment_calls)
        # Delayed::Worker.logger.debug "Trying again!" if tries <= 3
        if tries > 2
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
        end
        retry if tries <= 3
        # Delayed::Worker.logger.debug "No more attempt!"
        # Delayed::Worker.logger.debug "Timeout in zillow api call: #{e.message} #{e.class.to_s}"

       end

    end



    def self.read_from_excel(file_path, &block)
      book = Spreadsheet.open(file_path)
      sheet = book.worksheet 'Data'

      block.call(sheet) if sheet
    end

    def self.write_to_excel(columns, template_path, out_path, &block)
      book = template_path ? Spreadsheet.open(template_path) : Spreadsheet::Workbook.new
      sheet = book.worksheet 'Data'
      sheet = book.create_worksheet name: 'Data' if sheet.nil?

      block.call(sheet)

      if out_path
        book.write out_path
      else
        blob = StringIO.new("")
        book.write blob
        blob
      end
    end

    def self.prepare_stats_query(operators, sources)
      fields = []
      query = sources.inject([]) do |r, attr|
        attr_path = attr_sql_name(attr)
        operators.each do |operator|
          field = [attr, operator].join('_')
          r << case operator
            when :mean then

              if attr.in? [:list_price_psf, :sale_price_psf]
                "median(#{attr_path}) AS #{field}"
                # "median(round(#{attr_path})) AS #{field}"
              else
                "median(#{attr_path}) AS #{field}"
              end
            when :avg then
              if attr.in? [:list_price_psf, :sale_price_psf]
                "sum(#{attr_path})/count(#{attr_path}) AS #{field}"
              elsif attr.in? [:square_feet,:lot_square_feet,:list_price, :sale_price, :cdom, :year_built]
                "round(sum(round(#{attr_path}))/count(#{attr_path})) AS #{field}"
              else
                "#{operator.upcase}(#{attr_path}) AS #{field}"
              end
            else
              if attr.in? [:list_price_psf, :sale_price_psf]
                "#{operator.upcase}(#{attr_path}) AS #{field}"
                # "#{operator.upcase}(round(#{attr_path})) AS #{field}"
              else
                "#{operator.upcase}(#{attr_path}) AS #{field}"
              end
          end
          fields << field
        end
        r
      end
      [query.join(', '), fields]
    end

    def self.attr_sql_name(attr)
      table =
        if attr.in?(::Proforma.active_config.keys)
          :proformas
        elsif attr.to_sym == :id
          :properties
        elsif attr.in?(::Property.active_config.keys)
          :properties
        end
      [table, attr].compact.join('.')
    end

    def self.client_attr_sql_name(attr)
      table =
        if attr.in?(::ClientProforma.active_config.keys)
          :client_proformas
        elsif attr.to_sym == :id
          :properties
        elsif attr.in?(::Property.active_config.keys)
          :properties
        end
      [table, attr].compact.join('.')
    end

    COMP_STATS_QUERY = Property.prepare_stats_query(COMP_STATS_OPERATORS, COMP_STATS_FIELDS)
    SEARCH_STATS_QUERY = Property.prepare_stats_query(SEARCH_STATS_OPERATORS, SEARCH_STATS_FIELDS)
end
