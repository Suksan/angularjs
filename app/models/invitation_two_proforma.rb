class InvitationTwoProforma < ClientProforma
  # set_table_name "client_proformas"
  belongs_to :property
  belongs_to :user

  validates :user_id, uniqueness: { scope: :property_id }

  attr_config do |c|
    # overriden fields config
    c._overrides :serialize
    c.rental_output_condition_grade  enum: 'B', in: %w{A B C D F}
    c.rental_output_rent_user_entry :money
    c.rental_output_renovation_user_entry :money
    
    
    c.purchase_price :money,                  calc: proc {|m| (m.property.list_price * (1 - m.definition.purchase_price_discount)).round(0) }  #
    c.purchase_date :date,                    calc: proc {|m| Date.today }
    c.purchase_commission :percent,           calc: proc {|m| m.definition.purchase_commission_earned }
    c.purchase_annual_appreciation :percent,  calc: proc {|m| m.property.appreciation_rate ? m.property.appreciation_rate : m.definition.general_appreciation_rate }
    
    c.renovation_rental_rehab :money,                     calc: proc {|m|  m.renovation_rehab_for(m.property.year_built, m.rental_output_condition_grade, :rental)  || m.definition.renovation_unspecified_rental_rehab }
    c.renovation_sale_rehab :money,                       calc: proc {|m| m.renovation_rehab_for(m.property.year_built, m.rental_output_condition_grade, :sale) || m.definition.renovation_unspecified_sale_rehab }
    c.renovation_avg_rehab :money,                        calc: proc {|m| Math.avg(m.renovation_rental_rehab, m.renovation_sale_rehab) }
    c.renovation_rental_hold_cost :money,                 calc: proc {|m| (m.renovation_rental_rehab * m.property.square_feet).round(0) }
    c.renovation_rental_hold_weeks :integer,              calc: proc {|m| m.renovation_timeline_get(m.renovation_rental_hold_cost) }
        

    c.renovation_rental_resale_cost :money,               calc: proc {|m| (m.renovation_rental_rehab * m.property.square_feet).round(0) }
    c.renovation_rental_resale_weeks :integer,            calc: proc {|m| m.renovation_timeline_for(m.renovation_rental_resale_cost, :weeks) }
    c.renovation_seller_finance_cost :money,              calc: proc {|m| (m.renovation_rental_rehab * m.property.square_feet).round(0) }
    c.renovation_seller_finance_weeks :integer,           calc: proc {|m| m.renovation_timeline_for(m.renovation_seller_finance_cost, :weeks) }
    c.renovation_sale_listing_partnership_cost :money,    calc: proc {|m| (m.renovation_sale_rehab * m.property.square_feet).round(0) }
    c.renovation_sale_listing_partnership_weeks :integer, calc: proc {|m| m.renovation_timeline_for(m.renovation_sale_listing_partnership_cost, :weeks) }
    c.renovation_build_cost :money,                       calc: proc {|m| sqf = (m.property.comps_build_stats[:sold_count] > 0  ? m.property.comps_build_stats[:sold_square_feet_mean] : m.property.comps_sale_stats[:sold_square_feet_max]) || m.property.square_feet; (sqf * m.build_cost_for(sqf, :cost)).round(0) }
    c.renovation_build_weeks :integer,                    calc: proc {|m| m.build_cost_for((m.property.comps_build_stats[:sold_count] > 0  ? m.property.comps_build_stats[:sold_square_feet_mean] : m.property.comps_sale_stats[:sold_square_feet_max]) || m.property.square_feet, :weeks) }
    c.renovation_assignment_cost :money,                  calc: proc {|m| (m.renovation_avg_rehab * m.property.square_feet).round(0) }

    c.comparables_standard_avm_sale :money,               calc: proc {|m| Delayed::Worker.logger.debug ["return comp level #{m.definition.return_new_sale_comp_level}_avm",m.property.comps_sale_stats["#{m.definition.return_new_sale_comp_level}_avm"]].join(" x ") ; m.property.comps_sale_stats["#{m.definition.return_new_sale_comp_level}_avm"] }
    c.comparables_user_avm_sale :money    ,               calc: proc {|m| '' }           
    c.comparables_user_sale_price_psf_sale :decimal ,     calc: proc {|m| '' }  
    c.comparables_user_comp_list_sale :string,            calc: proc {|m| '' }
    c.comparables_standard_avm_rental :money,             calc: proc {|m| m.property.comps_rental_stats["#{m.definition.return_new_rental_comp_level}_avm"] }      
    c.comparables_user_avm_rental :money,                 calc: proc {|m| '' }                 
    c.comparables_user_sale_price_psf_rental :decimal,    calc: proc {|m| '' }  
    c.comparables_user_comp_list_rental :string,          calc: proc {|m| '' }
    c.sale_income_user_estimated_resale :money,           calc: proc {|m| '' }  
    c.sale_income_estimated_resale :money,                calc: proc {|m| '' }            
    c.rental_income_user_estimated_rent :money,          calc: proc {|m| '' }
    
# 
    c.comparables_avm_rental :money,      calc: proc {|m| 
                                                            if (m.rental_income_user_estimated_rent.to_i > 0)  
                                                                m.rental_income_user_estimated_rent.to_i
                                                            elsif (m.comparables_user_avm_rental.to_i > 0)
                                                                m.comparables_user_avm_rental.to_i
                                                            elsif (m.comparables_standard_avm_rental.to_i > 0)  
                                                                m.comparables_standard_avm_rental.to_i
                                                            elsif (m.property.comps_rental_stats["zestimate"].to_i > 0 ) 
                                                                m.property.comps_rental_stats["zestimate"].to_i
                                                            end
                                                     }    

    c.comparables_cdom_rental :integer,              calc: proc {|m| m.property.comps_rental_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_rental_sale :money,            calc: proc {|m| m.property.comps_sale_stats["level#{m.definition.return_rental_sale_comp_level}"] }
    c.comparables_cdom_rental_sale :integer,         calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_rental_resale :money,          calc: proc {|m| m.property.comps_sale_stats["level#{m.definition.return_rental_resale_comp_level}"] }
    c.comparables_cdom_rental_resale :integer,       calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_seller_finance :money,         calc: proc {|m| m.property.comps_sale_stats["level#{m.definition.return_seller_finance_comp_level}"] }
    c.comparables_cdom_seller_finance :integer,      calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }


     c.comparables_avm_sale :money,            calc: proc {|m| 

                                                            # Delayed::Worker.logger.debug "m.comparables_standard_avm_sale #{m.comparables_standard_avm_sale}";
                                                            if (m.sale_income_user_estimated_resale.to_i > 0)  
                                                                 Delayed::Worker.logger.debug "1"
                                                                 m.sale_income_user_estimated_resale.to_i
                                                            
                                                            elsif (m.comparables_user_avm_sale.to_i > 0)
                                                                Delayed::Worker.logger.debug "2"
                                                                m.comparables_user_avm_sale.to_i
                                                            elsif (m.comparables_standard_avm_sale.to_i > 0)  
                                                                Delayed::Worker.logger.debug "3"
                                                                m.comparables_standard_avm_sale.to_i
                                                            elsif (m.property.comps_sale_stats["zestimate"].to_i > 0 ) 
                                                                Delayed::Worker.logger.debug "4"
                                                                m.property.comps_sale_stats["zestimate"].to_i
                                                            end                                                            
                                                        }   


    c.comparables_cdom_sale :integer,                calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_build :money,                  calc: proc {|m| m.property.comps_build_stats["level#{m.definition.return_build_comp_level}"] }
    c.comparables_cdom_build :integer,               calc: proc {|m| m.property.comps_build_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_listing :money,                calc: proc {|m| m.property.comps_sale_stats["level#{m.definition.return_sale_comp_level}"] }
    c.comparables_cdom_listing :integer,             calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_listing_partnership :money,    calc: proc {|m| m.property.comps_sale_stats["level#{m.definition.return_sale_comp_level}"] }
    c.comparables_cdom_listing_partnership :integer, calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }
    c.comparables_avm_wholesale :money,              calc: proc {|m| m.property.comps_sale_stats["level#{m.definition.return_assignment_comp_level}"] }
    c.comparables_cdom_wholesale :integer,           calc: proc {|m| m.property.comps_sale_stats[:sold_cdom_mean].to_i }
# 
    c.owner_equity_ratio :percent,                calc: proc {|m| 1 - (m.property.owner_estimated_loan_balance / m.comparables_avm_sale) }
    c.owner_equity_amount :money,                 calc: proc {|m| m.comparables_avm_sale / m.property.owner_estimated_loan_balance }
# 
    c.carrying_expenses_property_taxes :money,  calc: proc {|m| (!m.property.tax_amount.nil? ? m.property.tax_amount : (!m.property.zillow_tax_amount.nil? ? m.property.zillow_tax_amount  : (!m.property.tax_rate.nil? ? m.property.tax_rate : m.definition.general_tax_rate) * (!m.property.tax_value.nil? ? m.property.tax_value : m.property.list_price))).round(0) }
    c.carrying_expenses_insurance :money,       calc: proc {|m| 0 }
    c.carrying_expenses_utilities :money,       calc: proc {|m| m.definition.general_annual_utility_expense.round(0) }
      
    c.tax_rate_company_marginal :percent,       calc: proc {|m| m.definition.general_company_marginal_tax_rate }
    c.tax_rate_investor_marginal :percent,      calc: proc {|m| m.definition.general_investor_marginal_tax_rate }
    c.tax_rate_capital_gains_rate :percent,     calc: proc {|m| m.definition.general_capital_gains_rate }
    c.tax_rate_improvement_ratio :percent,      calc: proc {|m| m.definition.general_improvement_ratio }
    c.tax_rate_years_depreciation :decimal,     calc: proc {|m| m.definition.general_years_depreciation }
#     
    c.rental_income_completion_date :date,              calc: proc {|m| m.purchase_date + (m.renovation_rental_hold_weeks * 7) }
    c.rental_income_lease_up_date :date,                calc: proc {|m| m.rental_income_completion_date + m.comparables_cdom_rental }

    c.rental_income_monthly_gross_rent :money,          calc: proc {|m| m.comparables_avm_rental.round(0) }
    c.rental_income_estimated_rent :money,          calc: proc {|m| m.rental_income_monthly_gross_rent.round(0) }
    c.rental_income_sale_price_current :money,    calc: proc {|m| (m.comparables_avm_rental_sale).round(0)}
    c.rental_income_annual_gross_rent :money,           calc: proc {|m| 

        # IF((D36>0),(D36*12),IF((B38>0),(B38*12),IF((B37>0),(B37*12))))
        # D36 - rental_income_estimated_rent
        # =IF(D29>0,D29*12,IF(B31>0,B31*12,IF(B30>0,B30*12)))
        Delayed::Worker.logger.debug ">> m.property.zillow_rental_estimate #{m.property.zillow_rental_estimate}"
        if (!m.rental_income_user_estimated_rent.blank? && m.rental_income_user_estimated_rent > 0) 
            Delayed::Worker.logger.debug ">> rental_income_estimated_rent #{m.rental_income_estimated_rent}"
            (m.rental_income_estimated_rent * 12).round(0)
        # elsif (!m.rental_output_renovation_user_entry.blank? && m.rental_output_renovation_user_entry > 0)
        #     Delayed::Worker.logger.debug ">> rental_output_renovation_user_entry #{m.rental_output_renovation_user_entry}"
        #     (m.rental_output_renovation_user_entry * 12).round(0)
        # elsif (!m.property.zillow_rental_estimate.blank? && m.property.zillow_rental_estimate > 0)
        #     Delayed::Worker.logger.debug ">> m.property.zillow_rental_estimate #{m.property.zillow_rental_estimate}"
        #     (m.property.zillow_rental_estimate * 12).round(0)
        elsif (!m.comparables_standard_avm_rental.blank? && m.comparables_standard_avm_rental > 0)
            Delayed::Worker.logger.debug ">> m.pcomparables_standard_avm_rental #{m.comparables_standard_avm_rental}"
            (m.comparables_standard_avm_rental * 12).round(0)
        else
            0
        end

        # if (!m.rental_income_estimated_rent.blank? && m.rental_income_estimated_rent > 0) 
        #     (m.rental_income_estimated_rent * 12).round(0)
        # elsif (!m.comparables_avm_rental.blank? && m.comparables_avm_rental > 0) 
        #     (m.comparables_avm_rental * 12).round(0)
        # else 
        #     (m.property.zillow_rental_estimate * 12).round(0)    
        # end
    }
    c.carrying_expenses_maintenance :money,       calc: proc {|m| m.property.annual_maintenance_fee ? m.property.annual_maintenance_fee : (m.definition.rental_annual_maintenance_expense * m.rental_income_annual_gross_rent).round(0) }
    c.carrying_expenses_annual :money,          calc: proc {|m| Math.sum(m.carrying_expenses_property_taxes, m.carrying_expenses_insurance, m.carrying_expenses_utilities, m.carrying_expenses_maintenance).round(0) }
    c.rental_income_other_income money: 0
    c.rental_income_total_annual_income :money,         calc: proc {|m| 
        if (m.rental_income_estimated_rent && m.rental_income_estimated_rent > 0) 
            ((m.rental_income_estimated_rent + m.rental_income_estimated_rent * 0.02) * 12 ) 
        elsif (m.rental_output_rent_user_entry && m.rental_output_rent_user_entry > 0)
            ((m.rental_output_rent_user_entry + m.rental_output_rent_user_entry * 0.02) * 12 ) 
        else
            ((m.property.zillow_rental_estimate + m.property.zillow_rental_estimate * 0.02) * 12 ) 
        end
    } # m.rental_income_annual_gross_rent + m.rental_income_other_income - m.rental_income_annual_vacancy }
    c.rental_income_annual_vacancy_rate :percent,       calc: proc {|m| m.definition.rental_annual_vacancy_rate }
    c.rental_income_annual_vacancy :money,              calc: proc {|m|  (m.rental_income_total_annual_income * m.rental_income_annual_vacancy_rate) }
    
    # Net Income
    
    c.rental_income_total_net_income :money,         calc: proc {|m| m.rental_income_total_annual_income   -  m.rental_income_annual_vacancy} # m.rental_income_annual_gross_rent + m.rental_income_other_income - m.rental_income_annual_vacancy }
    c.rental_income_annual_rent_increase :percent,      calc: proc {|m| m.definition.rental_annual_rent_increase }
    c.rental_income_years_to_sale :decimal,             calc: proc {|m| m.definition.rental_years_to_sale }
    
# 

    c.rental_expenses_management_fee :money,                  calc: proc {|m| 
        # (0.0316*(IF(B17>0,B17,B16)+IF(B22>0,B22,D21)))  
        # (H39-B35-B37-B38)/(B17+H36+B39)
            # n = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - m.property.tax_amount - m.property.annual_maintenance_fee) / (m.rental_output_recommended_offer - m.rental_output_renovation_expense - m.rental_expenses_management_fee)
            Delayed::Worker.logger.debug "~~rental_output_recommended_offer #{m.rental_output_recommended_offer}"
            Delayed::Worker.logger.debug "~~m.property.list_price #{m.property.list_price}"
            Delayed::Worker.logger.debug "~~m.rental_output_renovation_user_entry #{m.rental_output_renovation_user_entry}"
            Delayed::Worker.logger.debug "~~m.rental_output_renovation_calculated #{m.rental_output_renovation_calculated}"
            rsl = (0.0316 * ((m.rental_output_recommended_offer && m.rental_output_recommended_offer > 0 ? m.rental_output_recommended_offer : m.property.list_price) + (m.rental_output_renovation_user_entry && m.rental_output_renovation_user_entry > 0 ? m.rental_output_renovation_user_entry : m.rental_output_renovation_calculated))).try(:round,0)
            Delayed::Worker.logger.debug "~~rental_expenses_management_fee #{rsl}"
            rsl
    }  #((m.rental_income_total_net_income) * m.definition.rental_management_expense)
    c.rental_expenses_repairs_maintenance :money,             calc: proc {|m| 300  }  # {|m| (m.definition.rental_annual_maintenance_expense*m.rental_income_annual_gross_rent).round(0) }  # m.rental_income_annual_gross_rent * m.definition.rental_annual_maintenance_expense }
    c.rental_expenses_other money: 0
    c.rental_expenses_annual_expense_increase :percent,       calc: proc {|m| m.definition.rental_annual_expense_increase }
    c.rental_expenses_sale_realtor_closing_expense :percent,  calc: proc {|m| m.definition.sale_realtor_fee_closing_costs }
    
#   
    c.rental_financing_total_price :money,              calc: proc {|m| m.purchase_price + m.renovation_rental_hold_cost}
    c.rental_financing_loan :loan,                        calc: proc {|m| m.definition.rental_loan }
    c.rental_financing_purchase_renovation :money,        calc: proc {|m| m.purchase_price + m.renovation_rental_hold_cost  }
    c.rental_financing_down_payment_rate :percent,        calc: proc {|m| m.definition.lending_long_down_payment  }
    c.rental_financing_down_payment :money,               calc: proc {|m| m.rental_financing_total_price * m.rental_financing_down_payment_rate   }
    c.rental_financing_commission_credit_rate :percent,   calc: proc {|m| m.purchase_commission }
    c.rental_financing_commission_credit :money,          calc: proc {|m| (m.purchase_price * m.rental_financing_commission_credit_rate).round(0) }
    c.rental_financing_available_rate :percent,           calc: proc {|m| m.definition.lending_long_max_ltv   }# m.loan(m.rental_financing_loan, :max_ltv) }
    c.rental_financing_available :money,                  calc: proc {|m| m.rental_financing_purchase_renovation * m.rental_financing_available_rate}   #m.comparables_avm_rental_sale * m.rental_financing_available_rate }
    c.rental_financing_loan_amount :money,                calc: proc {|m| [m.rental_financing_available.nil? ? 0.0 : m.rental_financing_available , ( (m.rental_financing_total_price.nil? ? 0.0 : m.rental_financing_total_price) -  (m.rental_financing_down_payment.nil? ? 0.0 : m.rental_financing_down_payment) )].min  }# m.rental_financing_purchase_renovation - m.rental_financing_down_payment }
    
    c.rental_financing_loan_points_rate :percent,         calc: proc {|m| m.definition.lending_long_points }
    
c.rental_financing_loan_points :money,                calc: proc {|m| m.rental_financing_loan_amount * m.rental_financing_loan_points_rate }
    c.rental_financing_total_down_payment :money,         calc: proc {|m| m.rental_financing_total_price - m.rental_financing_loan_amount - m.rental_financing_commission_credit + m.rental_financing_loan_points }
    c.rental_financing_interest_rate :percent,            calc: proc {|m| m.definition.lending_long_interest_rate }  #** m.loan(m.rental_financing_loan, :interest_rate)
    c.rental_financing_term :integer,                     calc: proc {|m| m.definition.lending_long_term }  #** m.loan(m.rental_financing_loan, :term)
    c.rental_financing_interest_only :boolean,            calc: proc {|m| m.definition.lending_long_interest_only  } #** m.loan(m.rental_financing_loan, :interest_only) }
    c.rental_financing_closing_costs_rate :percent,       calc: proc {|m| m.definition.lending_long_closing_costs }
    c.rental_financing_closing_costs :money,              calc: proc {|m| m.rental_financing_loan_amount * m.rental_financing_closing_costs_rate }
    c.rental_financing_monthly_payment :money,            calc: proc {|m| m.rental_financing_interest_only ? (m.rental_financing_loan_amount * m.rental_financing_interest_rate/12)   :  - Math.pmt(m.rental_financing_interest_rate/12,m.rental_financing_term,m.rental_financing_loan_amount,0,0) }

    c.rental_output_minimum_capex :money,                   calc: proc {|m|            8000 }
    c.rental_output_renovation_calculated :money,                   calc: proc {|m|    
        b15 = m.renovation_rehab_for(m.property.year_built, "" , :rental)
        b13 = [40000,((m.renovation_rehab_for(m.property.year_built, m.rental_output_condition_grade, :rental) * m.property.square_feet)/1000.to_d).ceil * 1000].reject(&:blank?).min
        Delayed::Worker.logger.debug "b13: #{b13}"
        Delayed::Worker.logger.debug "b15: #{b15}"
        ([b13,b15].reject(&:blank?).max / 100.to_d).ceil * 100

       #  if (m.rental_output_renovation_user_entry && m.rental_output_renovation_user_entry > 0)
       #      used = m.rental_output_renovation_user_entry
       #  else
       #      used = ((([m.renovation_rehab_for(m.property.year_built, "", :rental), m.renovation_rental_rehab * m.property.square_feet].reject(&:blank?).max)/1000.to_d).ceil * 1000)
       #  end

       # [used,m.rental_output_minimum_capex].reject(&:blank?).max 
   }
    
    
    # =IF(rental_financing_interest_only="yes",(rental_expenses_annual_expense_increase*rental_financing_interest_rate/12),-PMT(rental_financing_interest_rate/12,rental_financing_term,rental_financing_loan_amount,0,0))
    #m.rental_expenses_annual_expense_increase*m.rental_financing_interest_rate/12)   :  - Math.pmt(m.rental_financing_interest_rate/12,m.rental_financing_term,m.rental_financing_loan_amount,0,0) 
# m.rental_financing_interest_only ? (m.rental_expenses_annual_expense_increase*m.rental_financing_interest_rate/12)   :  - Math.pmt(m.rental_financing_interest_rate/12,m.rental_financing_term,m.rental_financing_loan_amount,0,0)
    #loan_payment(m.rental_financing_loan_amount, m.rental_financing_interest_rate, 12.to_d, 12.to_d, m.loan(m.rental_financing_loan, :amortization), m.rental_financing_interest_only ? nil : 0.to_d)  }
# 
    c.rental_output_recommended_offer :money
    c.rental_output_purchase_expense :money,              calc: proc {|m| (m.purchase_price ).round(0) }

    #>>
    c.rental_output_renovation_expense :money,            calc: proc {|m|  

        rsl = nil
        if ((m.rental_output_renovation_user_entry) && (m.rental_output_renovation_user_entry > 0))
            rsl  = m.rental_output_renovation_user_entry.try(:round,0)
        elsif ((m.rental_output_renovation_calculated) && (m.rental_output_renovation_calculated > 0))
            rsl  = m.rental_output_renovation_calculated.try(:round,0)
        end

        rsl.blank? ? 0 : rsl

     } # m.renovation_rental_hold_cost.round(0) }
    c.rental_output_hold_weeks :decimal,                  calc: proc {|m| (m.rental_income_lease_up_date - m.purchase_date) / 7.to_d }
    c.rental_output_carrying_expenses :money,             calc: proc {|m| (m.carrying_expenses_annual / 52.to_d) * m.rental_output_hold_weeks }
    c.rental_output_lending_expenses :money,              calc: proc {|m| m.rental_output_hold_weeks / 4.33.to_d * (m.rental_financing_interest_only ? (m.rental_financing_loan_amount*m.rental_financing_interest_rate/12)   :  - Math.pmt(m.rental_financing_interest_rate/12,m.rental_financing_term,m.rental_financing_loan_amount,0,0)) + m.rental_financing_loan_points + (m.rental_expenses_closing_costs + m.rental_expenses_lease_up + m.rental_expenses_broker_commission) }
    c.rental_output_commission_credit :money,             calc: proc {|m| m.purchase_price * m.purchase_commission }
    
    c.rental_output_renovation :money,                  calc: proc {|m| m.rental_output_renovation_expense}
    
    c.rental_output_total_investment :money,              calc: proc {|m| m.rental_financing_down_payment }
    c.rental_output_annual_income :money,                 calc: proc {|m| m.rental_income_annual_gross_rent + m.rental_income_other_income - (m.rental_income_annual_gross_rent*m.rental_income_annual_vacancy_rate)   }
    c.rental_expenses_pool :money,                   calc: proc {|m|            if (m.property.pool_private) then 1582 else 0 end }
    c.rental_expenses_turnover :money,                   calc: proc {|m|          (m.property.square_feet * 0.25) }
    c.rental_expenses_leasing :money,                   calc: proc {|m|  ((m.rental_income_annual_gross_rent/12.to_d) * 0.5) }
    c.rental_expenses_insurance :money,           calc: proc {|m| 149 }  #{|m|  (((!m.property.tax_value.nil? ? m.property.tax_value : m.property.list_price)) * m.definition.general_annual_insurance_expense).round(0) }
    c.rental_output_annual_operating_expenses :money,     calc: proc  {|m|  Math.sum(m.rental_expenses_management_fee,m.rental_expenses_repairs_maintenance,m.rental_expenses_turnover,m.rental_expenses_leasing,m.rental_expenses_pool,m.rental_expenses_insurance)  }  #{|m| Math.sum(m.rental_expenses_management_fee, m.rental_expenses_repairs_maintenance, m.rental_expenses_other, m.carrying_expenses_property_taxes, m.carrying_expenses_insurance) }
    c.rental_output_annual_operating_expenses_percent :percent, calc: proc {|m| n = m.rental_output_annual_operating_expenses / m.rental_output_annual_income ;  n.try(:nan?) ? 0 : n}
    c.rental_output_annual_principal_interest :money,     calc: proc {|m| (m.rental_financing_interest_only ? (m.rental_financing_loan_amount*m.rental_financing_interest_rate/12)   :  - Math.pmt(m.rental_financing_interest_rate/12,m.rental_financing_term,m.rental_financing_loan_amount,0,0)) * 12.to_d }

    c.rental_output_equity_contribution :money,           calc: proc {|m| m.rental_financing_total_down_payment }
    c.rental_output_total_annual_expenses :money,         calc: proc {|m|  m.rental_output_annual_operating_expenses + (!m.carrying_expenses_property_taxes ? 0 : m.carrying_expenses_property_taxes)  + (m.property.annual_maintenance_fee ? m.property.annual_maintenance_fee : 0) }
    c.rental_output_total_annual_expenses_percent :percent,  calc: proc {|m| n = m.rental_output_total_annual_expenses / m.rental_output_annual_income ; n.try(:nan?) ? 0 : n }
    c.rental_output_cash_yield_amount :money,             calc: proc {|m| (m.rental_output_annual_income - m.rental_output_total_annual_expenses).round(0)}
                                                    
    c.rental_output_cash_yield_ratio :percent,            calc: proc {|m| n =  m.rental_output_cash_yield_amount / m.rental_output_annual_income ; n.try(:nan?) ? 0 : n }
    c.rental_output_avg_cash_yield_amount :money,         calc: proc {|m| ((m.rental_output_annual_income * (1.to_d + m.rental_income_annual_rent_increase)**m.rental_income_years_to_sale) - m.rental_output_total_annual_expenses * (1.to_d + m.rental_expenses_annual_expense_increase)**m.rental_income_years_to_sale).round(0) }
                                                                          


    c.rental_output_avg_cash_yield_ratio :percent,        calc: proc {|m| Delayed::Worker.logger.debug "~~ avg cash yield amount #{m.rental_output_avg_cash_yield_amount} "; Delayed::Worker.logger.debug "~~ rental output annual income #{m.rental_output_annual_income}"; Delayed::Worker.logger.debug "~~ annual rent increase #{m.rental_income_annual_rent_increase} ";  Delayed::Worker.logger.debug "~~ rental income yts #{m.rental_income_years_to_sale} "; n = m.rental_output_avg_cash_yield_amount / (m.rental_output_annual_income*(1.to_d + m.rental_income_annual_rent_increase)**m.rental_income_years_to_sale) ;  Delayed::Worker.logger.debug "~~ supposely nan: #{n.class}" ;(n.try(:nan?)) ? 0 : n }
    
    c.rental_output_noi :money,                   calc: proc {|m|  

      ((m.rental_income_total_annual_income) - m.rental_income_annual_vacancy - m.rental_output_total_annual_expenses) - 100  

    } #(m.rental_output_annual_income - m.rental_output_annual_operating_expenses).round(0) }
    c.rental_output_noi_ratio :percent,           calc: proc {|m|   n = m.rental_output_noi / m.rental_income_total_annual_income ; n.try(:nan?) ? 0 : n }
    
    

    
        #(m.rental_output_annual_income - m.rental_output_annual_operating_expenses) / m.rental_output_total_pre_lease_up_expenses }
    c.rental_output_current_avm :money,                   calc: proc {|m| m.comparables_avm_rental_sale }
    c.rental_sale_output_sale_price_current :money,     calc: proc {|m| m.comparables_avm_rental_sale}
    c.rental_output_ltv :percent,                         calc: proc {|m| m.rental_output_total_pre_lease_up_expenses/m.rental_sale_output_sale_price_current}
    
    c.rental_output_projected_sale_date :date,            calc: proc {|m| m.rental_income_lease_up_date >> (m.rental_income_years_to_sale * 12).to_i }
    c.rental_output_projected_sale_price :money,          calc: proc {|m| (m.rental_sale_output_sale_price_current * (1.to_d + m.purchase_annual_appreciation)**m.rental_income_years_to_sale).round(0) }
    c.rental_expenses_seller_contributions :money,      calc: proc {|m| (m.rental_output_projected_sale_price * m.definition.sale_seller_contribution).round(0)}
    c.rental_output_loan_payoff :money,                   calc: proc {|m| puts "~~ months  #{ (m.rental_output_projected_sale_date.year * 12 + m.rental_output_projected_sale_date.month) - (m.purchase_date.year * 12 + m.purchase_date.month)}"  ; puts "projected date: #{m.rental_output_projected_sale_date}"; puts "Purchase date: #{m.purchase_date}";  Math.cumprinc(m.rental_financing_interest_rate/12, m.rental_financing_term, m.rental_financing_loan_amount, 1, (m.rental_output_projected_sale_date.year * 12 + m.rental_output_projected_sale_date.month) - (m.purchase_date.year * 12 + m.purchase_date.month) - 1, 0) * -1.0 }
                                                        
    c.rental_output_realtor_fees_closing_expense :money,  calc: proc {|m| (m.rental_output_projected_sale_price * m.rental_expenses_sale_realtor_closing_expense).round(0) }
    c.rental_output_seller_contributions :money,          calc: proc {|m| m.rental_expenses_seller_contributions.round(0) }
    c.rental_output_total_expenses :money,                calc: proc {|m| m.rental_output_projected_sale_price - m.rental_output_loan_payoff - m.rental_output_realtor_fees_closing_expense - m.rental_output_seller_contributions }
    
    c.rental_sale_output_total_pre_lease_up_expense  :money,  calc: proc {|m|  m.rental_output_total_pre_lease_up_expenses.round(0)}
    c.rental_sale_output_total_sale_return :money,      calc: proc {|m| (m.rental_output_projected_sale_price - m.rental_sale_output_total_pre_lease_up_expense - m.rental_output_realtor_fees_closing_expense - m.rental_output_seller_contributions).round(0)}
    c.rental_output_return_on_sale_amount :money,         calc: proc {|m| m.rental_sale_output_total_sale_return}
    c.rental_output_return_on_sale_ratio :percent,        calc: proc {|m| m.rental_output_return_on_sale_amount/m.rental_output_total_pre_lease_up_expenses }
    c.rental_output_pre_tax_roi_amount :money,            calc: proc {|m| m.rental_output_avg_cash_yield_amount*m.rental_income_years_to_sale + m.rental_sale_output_total_sale_return/m.rental_sale_output_total_pre_lease_up_expense }
    c.rental_output_pre_tax_roi_ratio :percent,           calc: proc {|m| m.rental_output_pre_tax_roi_amount/m.rental_sale_output_total_pre_lease_up_expense}
    c.rental_output_post_tax_roi_amount :money,           calc: proc {|m| m.rental_output_pre_tax_roi_amount - (((m.rental_income_years_to_sale*12)<52 )? m.rental_output_pre_tax_roi_amount*m.tax_rate_company_marginal : m.rental_output_pre_tax_roi_amount*m.tax_rate_capital_gains_rate) }

    # =rental_output_pre_tax_roi_amount-IF((rental_income_years_to_sale*12)<52,rental_output_pre_tax_roi_amount*tax_rate_company_marginal,rental_output_pre_tax_roi_amount*tax_rate_capital_gains_rate)

    # rental_output_pre_tax_roi_amount-IF((rental_income_years_to_sale*12)<52,rental_output_pre_tax_roi_amount*tax_rate_company_marginal,rental_output_pre_tax_roi_amount*tax_rate_capital_gains_rate)

    c.rental_output_post_tax_roi_ratio :percent,          calc: proc {|m| m.rental_output_post_tax_roi_amount/m.rental_sale_output_total_pre_lease_up_expense }
    c.rental_output_pre_tax_roe :percent,                 calc: proc {|m| m.rental_output_pre_tax_roi_amount/(m.rental_output_equity_contribution + m.rental_output_loan_payoff) }
    c.rental_output_post_tax_roe :percent,                calc: proc {|m| m.rental_output_post_tax_roi_amount / m.rental_output_equity_contribution }
    c.rental_output_minimum_cap_rate            :percent,  calc: proc {|m|   if (m.property.market.id == 6) then 0.067 else 0.074  end } #m.definition.return_minimum_cap_rate 
    
    c.rental_expenses_lease_up :money,                   calc: proc {|m|  (m.rental_income_annual_gross_rent/12.to_d).round(0) }
    
    
    
     c.rental_expenses_closing_costs :money,                   calc: proc {|m|  
        
        ((m.rental_expenses_pool ? m.rental_expenses_pool : 0) + (m.rental_expenses_management_fee ? m.rental_expenses_management_fee : 0))
        # 1000
        # if (m.rental_output_recommended_offer && m.rental_output_recommended_offer > 0) 
        #     ((m.rental_output_recommended_offer * (0.25/100)) + 1100).round(0)
        # else
        #     ((m.property.list_price * 0.25/100) + 1100).round(0)
        # end
    }
    c.rental_expenses_broker_commission :money,     calc: proc {|m| 
        rsl = nil
        # =IF((B17>1),(((B17+H43)+B42)*0.032),(((B16+H43)+B42)*0.032))
        if (m.rental_output_recommended_offer  && m.rental_output_recommended_offer > 1 )
            rsl = ((m.rental_output_recommended_offer + m.rental_output_renovation_expense + m.rental_expenses_closing_costs) * 0.032).round(0)
        else
            rsl = ((m.property.list_price + m.rental_output_renovation_expense + m.rental_expenses_closing_costs) * 0.032).round(0)
        end
        rsl.blank? ? 0 : rsl
    }

     c.rental_output_closing_costs :money,        calc: proc {|m|
        m.rental_expenses_pool  +  m.rental_expenses_management_fee
        # (m.rental_expenses_closing_costs  + m.rental_expenses_broker_commission).round(0)
    }
    #>>
    c.rental_output_max_purchase_amount :money,           calc: proc {|m| 
        
        Delayed::Worker.logger.debug "~~~ m.rental_output_minimum_cap_rate: #{m.rental_output_minimum_cap_rate}"
        Delayed::Worker.logger.debug "~~~ m.rental_output_renovation_expense: #{m.rental_output_renovation_expense}"
        Delayed::Worker.logger.debug "~~~ m.rental_output_closing_costs: #{m.rental_output_closing_costs}"
        Delayed::Worker.logger.debug "~~~ m.rental_expenses_broker_commission #{m.rental_expenses_broker_commission}"


Delayed::Worker.logger.debug "~~~ rental_output_renovation_calculated #{m.rental_output_renovation_calculated}"
Delayed::Worker.logger.debug "~~~ m.rental_income_annual_gross_rent: #{m.rental_income_annual_gross_rent}"
Delayed::Worker.logger.debug "~~~ m.property.annual_maintenance_fee #{m.property.annual_maintenance_fee}"
Delayed::Worker.logger.debug "~~~ m.property.tax_amount #{m.property.tax_amount}"
        # Delayed::Worker.logger.debug "m.rental_expenses_closing_costs: #{m.rental_expenses_closing_costs}"
        # Delayed::Worker.logger.debug "m.rental_expenses_broker_commission: #{m.rental_expenses_broker_commission}"

Delayed::Worker.logger.debug "~~~ m.rental_output_renovation_calculated #{m.rental_output_renovation_calculated}"
Delayed::Worker.logger.debug "~~~ m.rental_expenses_management_fee #{m.rental_expenses_management_fee}"
        # CEILING((((H46/D17)-H43)-H44),25)
        # *(H39-B38-B37-B35)/D17-(IF(B22>0,B22,D21)+B39)*

         # (H39-B35-B37-B38)/(B17+H36+B39)
        # n = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - m.property.tax_amount - m.property.annual_maintenance_fee) / (m.rental_output_recommended_offer - m.rental_output_renovation_expense - m.rental_expenses_management_fee)

        # (H39-B35-B37-B38)/(B18+H36+B39)
        # n = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - m.property.tax_amount - m.property.annual_maintenance_fee) / (m.rental_output_calc_offer - m.rental_output_renovation_expense - m.rental_expenses_management_fee)
        
        rsl = nil
Delayed::Worker.logger.debug "~~~ tir"
        rsldot5 = (m.rental_income_annual_gross_rent - (m.property.annual_maintenance_fee ? m.property.annual_maintenance_fee : 0) - (m.property.tax_amount ? m.property.tax_amount : 0) - (if m.property.market.id == 6 then 714 else 204 end))
        rsl1 = ((m.rental_income_annual_gross_rent - (m.property.annual_maintenance_fee ? m.property.annual_maintenance_fee : 0) - (m.property.tax_amount ? m.property.tax_amount : 0) - (if m.property.market.id == 6 then 714 else 204 end)) / m.rental_output_minimum_cap_rate)
Delayed::Worker.logger.debug "~~~ tor"
        rsl2 = (((m.rental_output_renovation_user_entry && m.rental_output_renovation_user_entry > 0 ) ? m.rental_output_renovation_user_entry : m.rental_output_renovation_calculated )  + (m.rental_expenses_management_fee ? m.rental_expenses_management_fee : 0)  )
        
        Delayed::Worker.logger.debug "~~~ rsldot5: #{rsldot5}"
        Delayed::Worker.logger.debug "~~~ rsl1: #{rsl1}"
        Delayed::Worker.logger.debug "~~~ rsl2: #{rsl2}"
        rsl = rsl1 - rsl2
        Delayed::Worker.logger.debug "~~~ raw rsl: #{rsl}"
        rslx = (rsl / 25.to_d ).ceil * 25
        Delayed::Worker.logger.debug "~~~ raw rslx: #{rslx}"
        rslx.blank? ? 0 : rslx
        # ((((m.rental_income_annual_gross_rent / m.rental_output_minimum_cap_rate) - m.rental_output_renovation_expense) - m.rental_output_closing_costs).to_d / 25.to_d).ceil * 25
         # (((((m.rental_output_noi/m.rental_output_minimum_cap_rate) - (m.rental_output_renovation_expense  )) - m.rental_expenses_closing_costs) ).to_d / 25.to_d).ceil  * 25
    } #[(m.rental_output_annual_income - m.rental_output_annual_operating_expenses)/(((m.rental_output_minimum_cap_rate.nil?) || (m.rental_output_minimum_cap_rate ==  m.definition.return_minimum_cap_rate)) ? m.definition.return_minimum_cap_rate : m.rental_output_minimum_cap_rate) - m.rental_output_renovation_expense, m.rental_income_sale_price_current].min }
    c.rental_output_max_price :money,                   calc: proc {|m|  
        
        
        rsl = m.property.comps_sale_stats[:expanded_sale_price_max].try(:round,0)
        rsl.blank? ? 0 : rsl
        # n = (m.rental_income_annual_gross_rent / m.rental_output_minimum_cap_rate).round(0)
        # Delayed::Worker.logger.debug "~~~~ m.n: #{n}"
        # n.try(:nan?) ? 0 : n
        

    }
    c.rental_output_max_ltv_offer :money,                   calc: proc {|m|  
        # CEILING(IF((H36>0),(((H36/(99.5/100))-H43)-H44),
        rsl = nil       
        
        # CEILING(IF(H29>0,H29/100%-H36-H37,F31/100%-H36-H37),25)
        Delayed::Worker.logger.debug "~~~ m.sale_income_user_estimated_resale: #{m.sale_income_user_estimated_resale}"
        Delayed::Worker.logger.debug "~~~ m.rental_output_max_price: #{m.rental_output_max_price}"
        Delayed::Worker.logger.debug "~~~  m.rental_output_renovation_expense: #{ m.rental_output_renovation_expense}"
        Delayed::Worker.logger.debug "~~~  m.rental_expenses_closing_costs: #{ m.rental_expenses_closing_costs}"
        rsl = (((m.sale_income_user_estimated_resale && m.sale_income_user_estimated_resale > 0) ?  ((m.sale_income_user_estimated_resale) - m.rental_output_renovation_expense - m.rental_expenses_closing_costs) : ( m.rental_output_max_price - m.rental_output_renovation_expense - m.rental_expenses_closing_costs)) / 25.to_d ).ceil * 25

        Delayed::Worker.logger.debug "~~~ max ltv: #{rsl}"
        rsl.blank? ? 0 : rsl
        # if m.rental_income_sale_price_current && m.rental_income_sale_price_current > 0
        #     rsl = ((( m.rental_income_sale_price_current/(99.5/100)) - m.rental_output_renovation_expense) -  m.rental_output_closing_costs).round(0)
        # else
        #     rsl = ((((m.comparables_avm_sale * (105/100))/((100/100)+(0.25/100))) - m.rental_output_renovation_expense) - m.rental_output_closing_costs).round(0)
        # end

        # H36 = rental_income_sale_price_current
        # H43 = rental_output_renovation_expense
        # H44 = rental_output_closing_costs
        # F36 = m.comparables_avm_sale
               
        
        # (rsl/25.to_d).ceil * 25

    }

    c.rental_output_calc_offer :money,                   calc: proc {|m|   
        Delayed::Worker.logger.debug "rental_output_max_purchase_amount: #{m.rental_output_max_purchase_amount}"
        Delayed::Worker.logger.debug "rental_output_max_ltv_offer: #{m.rental_output_max_ltv_offer}"
        
        [m.rental_output_max_purchase_amount, m.rental_output_max_ltv_offer].reject(&:blank?).min 
    }

    #>>
    c.rental_output_purchase :money,                    calc: proc {|m|  (!m.rental_output_recommended_offer.blank? && (m.rental_output_recommended_offer > 0)) ? m.rental_output_recommended_offer : m.rental_output_calc_offer }  ###
    c.rental_output_total_pre_lease_up_expenses :money,   calc: proc {|m|  m.rental_output_purchase + m.rental_output_renovation + m.rental_output_carrying_expenses + m.rental_output_lending_expenses - m.rental_output_commission_credit } ###
    
    ##---------

   

    

    c.rental_output_max_renovation_amount :money,         calc: proc {|m|   
        
        # ((m.rental_output_noi / (m.rental_output_minimum_cap_rate + (0.3/100))) - ((m.rental_output_purchase + m.rental_output_closing_costs) * 92.to_d/100.to_d)).round(0)
        Delayed::Worker.logger.debug "m.rental_output_cap_rate : #{m.rental_output_cap_rate}"
        n = (((m.rental_income_annual_gross_rent/m.rental_output_minimum_cap_rate) - m.property.list_price) - m.rental_output_closing_costs)
        n.try(:nan?) ? 0 : n
        #B17 = rental_output_recommended_offer
            #H46 = rental_income_annual_gross_rent
            #H43 = rental_output_renovation_expense
            #H44 = rental_output_closing_costs
            #B18 = rental_output_calc_offer
    }

    c.rental_output_max_renovation_ratio :percent,        calc: proc {|m| n = m.rental_output_max_renovation_amount/m.rental_output_renovation_expense;  n.try(:nan?) ? 0 : n }
    #[((m.rental_output_annual_income - m.rental_output_annual_operating_expenses)/(((m.rental_output_minimum_cap_rate.nil?) || (m.rental_output_minimum_cap_rate ==  m.definition.return_minimum_cap_rate)) ? m.definition.return_minimum_cap_rate : m.rental_output_minimum_cap_rate) - m.rental_output_purchase_expense).round(0), (m.rental_income_sale_price_current - m.rental_output_purchase_expense).round(0) ].min}

    ##---------

    c.rental_output_max_purchase_ratio :percent,          calc: proc {|m|  
        n = nil
        if (m.rental_output_recommended_offer && m.rental_output_recommended_offer > 0)
            n = m.rental_output_recommended_offer/m.property.list_price
        else
            n = m.rental_output_calc_offer/m.property.list_price
        end
        n.try(:nan?) ? 0 : n
    } #m.rental_output_max_purchase_amount/m.rental_output_purchase_expense}
    
    

    
    #===========

    

    

   
    
    c.rental_output_total_price :money,                 calc: proc {|m| m.rental_output_purchase + m.rental_output_renovation_expense + m.rental_output_closing_costs } # m.rental_financing_purchase_renovation

    c.rental_output_cap_rate :percent,                    calc: proc {|m|  
        #=IF((B17>0),(H46/((B17+H43)+H44)),(H46/((B18+H43)+H44))
            #B17 = rental_output_recommended_offer
            #H46 = rental_income_annual_gross_rent
            #H43 = rental_output_renovation_expense
            #H44 = rental_output_closing_costs
            #B18 = rental_output_calc_offer

            #**
            # (H39-B35-B37-B38)/(B17+H36+B39)
            # n = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - m.property.tax_amount - m.property.annual_maintenance_fee) / (m.rental_output_recommended_offer - m.rental_output_renovation_expense - m.rental_expenses_management_fee)

            # (H39-B35-B37-B38)/(B18+H36+B39)
            # n = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - m.property.tax_amount - m.property.annual_maintenance_fee) / (m.rental_output_calc_offer - m.rental_output_renovation_expense - m.rental_expenses_management_fee)

            Delayed::Worker.logger.debug "~~m.rental_income_annual_gross_rent #{m.rental_income_annual_gross_rent}"
            if (m.rental_output_recommended_offer && m.rental_output_recommended_offer > 0)
                # n = (m.rental_income_annual_gross_rent/((m.rental_output_recommended_offer + m.rental_output_renovation_expense) + m.rental_output_closing_costs)).round(3)
                n = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - (m.property.tax_amount ? m.property.tax_amount : 0) - (m.property.annual_maintenance_fee ? m.property.annual_maintenance_fee : 0)) / (m.rental_output_recommended_offer + m.rental_output_renovation_expense + m.rental_expenses_management_fee).round(4)
            else
                Delayed::Worker.logger.debug "rental_output_calc_offer = #{m.rental_output_calc_offer}"
            Delayed::Worker.logger.debug "rental_output_renovation_expense = #{m.rental_output_renovation_expense}"
            Delayed::Worker.logger.debug "rental_output_closing_costs = #{m.rental_output_closing_costs}"
                # n = (m.rental_income_annual_gross_rent/((m.rental_output_calc_offer + m.rental_output_renovation_expense) + m.rental_output_closing_costs)).round(3)
                n1 = (m.rental_income_annual_gross_rent - (if m.property.market.id == 6 then 714 else 204 end)  - (m.property.tax_amount ? m.property.tax_amount : 0)  - (m.property.annual_maintenance_fee ? m.property.annual_maintenance_fee : 0)) 
                Delayed::Worker.logger.debug "n1 = #{n1}"
                n2 = (m.rental_output_calc_offer + m.rental_output_renovation_expense + m.rental_expenses_management_fee)
                Delayed::Worker.logger.debug "n2 = #{n2}"
                n = (n1 / n2).round(4)
                Delayed::Worker.logger.debug "n = #{n}"
            end

            
            n.try(:nan?) ? 0 : n 
    }
    #==========

    
    c.rental_output_max_renov_to_predict_ratio :percent,   calc: proc {|m| m.definition.return_minimum_roi  }
    c.rental_output_max_purchase_to_lp_ratio  :percent,    calc: proc {|m| m.definition.return_minimum_return_on_total_project }

    c.seller_finance_income_completion_date :date,      calc: proc {|m| m.purchase_date + (m.renovation_seller_finance_weeks * 7) }
    c.seller_finance_income_sale_date :date,            calc: proc {|m| m.seller_finance_income_completion_date + (m.comparables_cdom_seller_finance - (m.comparables_cdom_seller_finance * m.definition.seller_finance_sale_time_discount)) + m.definition.general_days_to_close }
    c.seller_finance_income_sale_price :money,          calc: proc {|m| m.comparables_avm_seller_finance }
    c.seller_finance_income_down_payment_rate :percent, calc: proc {|m| m.definition.seller_finance_down_payment }
    c.seller_finance_income_down_payment :money,        calc: proc {|m| m.seller_finance_income_sale_price * m.seller_finance_income_down_payment_rate }
    c.seller_finance_income_loan_amount :money,         calc: proc {|m| m.seller_finance_income_sale_price - m.seller_finance_income_down_payment }
    c.seller_finance_income_interest_rate :percent,     calc: proc {|m| m.definition.seller_finance_interest_rate }
    c.seller_finance_income_term :integer,              calc: proc {|m| m.definition.seller_finance_term }
    c.seller_finance_income_monthly_payment :money,     calc: proc {|m| loan_payment(m.seller_finance_income_loan_amount, m.seller_finance_income_interest_rate, 12.to_d, 12.to_d, m.loan(m.seller_finance_financing_loan, :amortization), 0.to_d) }
    c.seller_finance_income_years_to_sale :decimal,     calc: proc {|m| m.definition.seller_finance_years_to_sale }

    c.seller_finance_expenses_realtor_closing_expense :percent, calc: proc {|m| m.definition.seller_finance_realtor_fee }
    c.seller_finance_expenses_management_expense :money,        calc: proc {|m| m.seller_finance_income_monthly_payment * m.definition.seller_finance_management_expense }

    c.seller_finance_financing_loan :loan,                          calc: proc {|m| m.definition.seller_finance_loan }
    c.seller_finance_financing_purchase_renovation :money,          calc: proc {|m| m.purchase_price + m.renovation_seller_finance_cost }
    c.seller_finance_financing_down_payment_purchase_rate :percent, calc: proc {|m| m.loan('bridge', :purchase_down_payment) }
    c.seller_finance_financing_down_payment_purchase :money,        calc: proc {|m| m.purchase_price * m.seller_finance_financing_down_payment_purchase_rate }
    c.seller_finance_financing_commission_credit_rate :percent,     calc: proc {|m| m.purchase_commission }
    c.seller_finance_financing_commission_credit :money,            calc: proc {|m| m.purchase_price * m.seller_finance_financing_commission_credit_rate }
    c.seller_finance_financing_total_down_payment :money,           calc: proc {|m| m.seller_finance_financing_down_payment_purchase - m.seller_finance_financing_commission_credit }
    c.seller_finance_financing_loan_amount :money,                  calc: proc {|m| m.seller_finance_financing_purchase_renovation - m.seller_finance_financing_down_payment_purchase }
    c.seller_finance_financing_interest_rate :percent,              calc: proc {|m| m.loan(m.seller_finance_financing_loan, :interest_rate) }
    c.seller_finance_financing_loan_points_rate :percent,           calc: proc {|m| m.loan(m.seller_finance_financing_loan, :points) }
    c.seller_finance_financing_loan_points :money,                  calc: proc {|m| m.seller_finance_financing_loan_amount * m.seller_finance_financing_loan_points_rate }
    c.seller_finance_financing_term :integer,                       calc: proc {|m| m.loan(m.seller_finance_financing_loan, :term) }
    c.seller_finance_financing_interest_only :boolean,              calc: proc {|m| m.loan(m.seller_finance_financing_loan, :interest_only) }
    c.seller_finance_financing_closing_costs_rate :percent,         calc: proc {|m| m.loan(m.seller_finance_financing_loan, :closing_costs) }
    c.seller_finance_financing_closing_costs :money,                calc: proc {|m| m.seller_finance_financing_loan_amount * m.seller_finance_financing_closing_costs_rate }
    c.seller_finance_financing_monthly_payment :money,              calc: proc {|m| loan_payment(m.seller_finance_financing_loan_amount, m.seller_finance_financing_interest_rate, 12.to_d, 12.to_d, m.loan(m.seller_finance_financing_loan, :amortization), m.seller_finance_financing_interest_only ? nil : 0.to_d)  }

    c.seller_finance_output_sale_price :money,                    calc: proc {|m| m.seller_finance_income_sale_price }
    c.seller_finance_output_sale_date :date,                      calc: proc {|m| m.seller_finance_income_sale_date }          
    c.seller_finance_output_buyer_down_payment :money,            calc: proc {|m| m.seller_finance_income_down_payment }
    c.seller_finance_output_purchase_price :money,                calc: proc {|m| m.purchase_price }    
    c.seller_finance_output_renovation_expense :money,            calc: proc {|m| m.renovation_seller_finance_cost }
    c.seller_finance_output_hold_weeks :decimal,                  calc: proc {|m| (m.seller_finance_income_sale_date - m.purchase_date) / 7 }
    c.seller_finance_output_carrying_expenses :money,             calc: proc {|m| (m.carrying_expenses_annual / 52) * m.seller_finance_output_hold_weeks }
    c.seller_finance_output_lending_expenses :money,              calc: proc {|m| (m.seller_finance_output_hold_weeks / 4.33 * m.seller_finance_financing_monthly_payment) + m.seller_finance_financing_loan_points + m.seller_finance_financing_closing_costs }
    c.seller_finance_output_realtor_closing_expense :money,       calc: proc {|m| m.seller_finance_expenses_realtor_closing_expense * m.seller_finance_output_sale_price }
    c.seller_finance_output_total_pre_sale_expenses :money,       calc: proc {|m| m.seller_finance_output_purchase_price + m.seller_finance_output_renovation_expense + m.seller_finance_output_lending_expenses + m.seller_finance_output_realtor_closing_expense }
    c.seller_finance_output_equity_contribution :money,           calc: proc {|m| m.seller_finance_financing_total_down_payment }      
    c.seller_finance_output_annual_income :money,                 calc: proc {|m| m.seller_finance_income_monthly_payment * 12 }
    c.seller_finance_output_annual_operating_expenses :money,     calc: proc {|m| m.seller_finance_expenses_management_expense * 12 }
    c.seller_finance_output_annual_principal_interest :money,     calc: proc {|m| m.seller_finance_financing_monthly_payment * 12 }
    c.seller_finance_output_total_annual_expenses :money,         calc: proc {|m| m.seller_finance_output_annual_operating_expenses + m.seller_finance_output_annual_principal_interest }
    c.seller_finance_output_yearly_cash_yield_amount :money,      calc: proc {|m| m.seller_finance_output_annual_income - m.seller_finance_output_total_annual_expenses }
    c.seller_finance_output_cap_rate :percent,                    calc: proc {|m| (m.seller_finance_output_annual_income - m.seller_finance_output_annual_operating_expenses) / m.seller_finance_output_total_pre_sale_expenses }
    c.seller_finance_output_loan_payoff :money,                   calc: proc {|m| m.seller_finance_financing_loan_amount - (m.seller_finance_financing_interest_only ? 0 : Math.cumprinc(m.seller_finance_financing_interest_rate / 12.to_d, m.loan(m.seller_finance_financing_loan, :amortization), m.seller_finance_financing_total_down_payment, 1, m.seller_finance_income_years_to_sale * 12, 0)) }
    c.seller_finance_output_return_on_sale :money,                calc: proc {|m| m.seller_finance_output_sale_price - m.seller_finance_output_total_pre_sale_expenses } 
    c.seller_finance_output_pre_tax_roi_amount :money,            calc: proc {|m| (m.seller_finance_output_yearly_cash_yield_amount * m.seller_finance_income_years_to_sale) + m.seller_finance_output_return_on_sale }
    c.seller_finance_output_pre_tax_roi_ratio :percent,           calc: proc {|m| m.seller_finance_output_pre_tax_roi_amount / m.seller_finance_output_total_pre_sale_expenses }
    c.seller_finance_output_post_tax_roi_amount :money,           calc: proc {|m| m.seller_finance_output_pre_tax_roi_amount <=1 ? m.seller_finance_output_pre_tax_roi_amount : m.seller_finance_output_pre_tax_roi_amount - (m.seller_finance_income_years_to_sale * 12.to_d < 52 ? m.seller_finance_output_pre_tax_roi_amount * m.tax_rate_company_marginal : m.seller_finance_output_pre_tax_roi_amount * m.tax_rate_capital_gains_rate) }
    c.seller_finance_output_post_tax_roi_ratio :percent,          calc: proc {|m| m.seller_finance_output_post_tax_roi_amount / m.seller_finance_output_total_pre_sale_expenses }
    c.seller_finance_output_pre_tax_roe_ratio :percent,           calc: proc {|m| m.seller_finance_output_pre_tax_roi_amount / m.seller_finance_output_equity_contribution }
    c.seller_finance_output_post_tax_roe_ratio :percent,          calc: proc {|m| m.seller_finance_output_post_tax_roi_amount / m.seller_finance_output_equity_contribution }
    c.seller_finance_output_max_renovation_amount :money,         calc: proc {|m| ((m.seller_finance_output_annual_income - m.seller_finance_output_annual_operating_expenses) / m.definition.return_minimum_cap_rate) - m.purchase_price - (m.seller_finance_output_sale_price * m.definition.return_minimum_cap_rate) }
    c.seller_finance_output_max_renovation_ratio :percent,        calc: proc {|m| m.seller_finance_output_max_renovation_amount / m.seller_finance_output_renovation_expense }
    c.seller_finance_output_max_purchase_amount :money,           calc: proc {|m| [m.comparables_avm_seller_finance - m.renovation_seller_finance_cost, ((m.seller_finance_output_annual_income - m.seller_finance_output_annual_operating_expenses) / m.definition.return_minimum_cap_rate) - m.renovation_seller_finance_cost - (m.seller_finance_output_total_pre_sale_expenses * m.definition.return_minimum_roi) - (m.seller_finance_output_sale_price * m.definition.return_minimum_cap_rate)].min }
    c.seller_finance_output_max_purchase_ratio :percent,          calc: proc {|m| m.seller_finance_output_max_purchase_amount / m.purchase_price }
# 
    
    
    c.rental_resale_income_completion_date :date,      calc: proc {|m| m.purchase_date + (m.renovation_rental_resale_weeks * 7) }
    c.rental_resale_income_sale_date :date,            calc: proc {|m| m.rental_resale_income_completion_date + m.definition.general_days_to_close } #+ m.comparables_cdom_rental_resale
    c.rental_resale_income_sale_price :money,          calc: proc {|m| [m.comparables_avm_rental_resale, (m.rental_output_annual_income - m.rental_output_annual_operating_expenses) / m.definition.rental_resale_investor_cap_rate].min }
    c.rental_resale_income_investor_cap_rate :percent, calc: proc {|m| m.definition.rental_resale_investor_cap_rate }

    c.rental_resale_expenses_marketing_expense :money,        calc: proc {|m| m.definition.rental_resale_marketing_expense }
    c.rental_resale_expenses_closing_expense :percent,        calc: proc {|m| m.definition.rental_resale_closing_expense }
    c.rental_resale_expenses_warranty_expenses :money,        calc: proc {|m| m.definition.rental_resale_warranty_expense }
    
    c.rental_resale_financing_loan :loan,                             calc: proc {|m| m.definition.rental_resale_loan }
    c.rental_resale_financing_purchase_price :money,                  calc: proc {|m| m.purchase_price }
    c.rental_resale_financing_renovation_price :money,                calc: proc {|m| m.renovation_rental_resale_cost }
    c.rental_resale_financing_total_price :money,                     calc: proc {|m| m.rental_resale_financing_purchase_price + m.rental_resale_financing_renovation_price }
    c.rental_resale_financing_down_payment_purchase_rate :percent,    calc: proc {|m| m.definition.lending_bridge_purchase_down_payment }
    c.rental_resale_financing_down_payment_purchase :money,           calc: proc {|m| m.rental_resale_financing_purchase_price * m.rental_resale_financing_down_payment_purchase_rate }
    c.rental_resale_financing_down_payment_renovation_rate :percent,  calc: proc {|m| m.loan(:bridge, :renovation_down_payment) }
    c.rental_resale_financing_down_payment_renovation :money,         calc: proc {|m| m.rental_resale_financing_renovation_price * m.rental_resale_financing_down_payment_renovation_rate }
    c.rental_resale_financing_commission_credit_rate :percent,        calc: proc {|m| m.purchase_commission }
    c.rental_resale_financing_commission_credit :money,               calc: proc {|m| m.purchase_price * m.rental_resale_financing_commission_credit_rate }
    c.rental_resale_financing_loan_amount :money,                     calc: proc {|m| m.rental_resale_financing_total_price - m.rental_resale_financing_down_payment_purchase - m.rental_resale_financing_down_payment_renovation }
    
    c.rental_resale_financing_loan_points_rate :percent,              calc: proc {|m| m.definition.ending_bridge_points }
    c.rental_resale_financing_loan_points :money,                     calc: proc {|m| m.rental_resale_financing_loan_amount * m.rental_resale_financing_loan_points_rate }
    c.rental_resale_financing_total_down_payment :money,              calc: proc {|m| m.sale_financing_total_price - m.sale_financing_loan_amount - m.sale_financing_commission_credit + m.sale_financing_loan_points}
    c.rental_resale_financing_total_available_rate :percent,          calc: proc {|m| m.loan(:bridge, :max_ltv) }
    c.rental_resale_financing_total_available :money,                 calc: proc {|m| m.comparables_avm_rental_resale * m.rental_resale_financing_total_available_rate }
    c.rental_resale_financing_interest_rate :percent,                 calc: proc {|m| m.loan(m.rental_resale_financing_loan, :interest_rate) }
    c.rental_resale_financing_term :integer,                          calc: proc {|m| m.loan(m.rental_resale_financing_loan, :term) }
    c.rental_resale_financing_interest_only :boolean,                 calc: proc {|m| m.loan(m.rental_resale_financing_loan, :interest_only) }
    c.rental_resale_financing_closing_costs_rate :percent,            calc: proc {|m| m.loan(m.rental_resale_financing_loan, :closing_costs) }
    c.rental_resale_financing_closing_costs :money,                   calc: proc {|m| m.rental_resale_financing_loan_amount * m.rental_resale_financing_closing_costs_rate }
    c.rental_resale_financing_monthly_payment :money,                 calc: proc {|m| m.sale_financing_interest_only ? (m.sale_financing_loan_amount*m.salel_financing_interest_rate/12)   :  - Math.pmt(m.sale_financing_interest_rate/12,m.sale_financing_term,m.sale_financing_loan_amount,0,0)  }
# 
    c.rental_resale_output_sale_price :money,                   calc: proc {|m| m.rental_resale_income_sale_price }
    c.rental_resale_output_investor_cap_rate :percent,          calc: proc {|m| m.rental_resale_income_investor_cap_rate }
    c.rental_resale_output_investor_ltv :percent,               calc: proc {|m| x=m.rental_resale_output_sale_price / m.comparables_avm_rental_resale; x == Float::INFINITY ? nil: x }
    c.rental_resale_output_purchase_expense :money,             calc: proc {|m| m.rental_resale_financing_purchase_price }
    c.rental_resale_output_renovation_expense :money,           calc: proc {|m| m.rental_resale_financing_renovation_price }
    c.rental_resale_output_hold_weeks :decimal,                 calc: proc {|m| (m.rental_resale_income_sale_date - m.purchase_date) / 7.to_d }
    c.rental_resale_output_carrying_expense :money,             calc: proc {|m| (m.carrying_expenses_annual / 52) * m.rental_resale_output_hold_weeks }
    c.rental_resale_output_lending_expense :money,              calc: proc {|m| (m.rental_resale_output_hold_weeks / 4.33.to_d * m.rental_resale_financing_monthly_payment) + m.rental_resale_financing_loan_points + m.rental_resale_financing_closing_costs }
    c.rental_resale_output_realtor_fees_closing_expense :money, calc: proc {|m| m.rental_resale_expenses_closing_expense * m.rental_resale_income_sale_price }
    c.rental_resale_output_seller_contributions :money,         calc: proc {|m| m.rental_resale_expenses_warranty_expenses }
    c.rental_resale_output_total_expenses :money,               calc: proc {|m| m.rental_resale_output_purchase_expense + m.rental_resale_output_renovation_expense + m.rental_resale_output_carrying_expense + m.rental_resale_output_lending_expense + m.rental_resale_output_realtor_fees_closing_expense + m.rental_resale_output_seller_contributions }
    c.rental_resale_output_equity_contribution :money,          calc: proc {|m| m.rental_resale_financing_total_down_payment }
    c.rental_resale_output_pre_tax_roi_amount :money,           calc: proc {|m| m.rental_resale_output_sale_price - m.rental_resale_output_total_expenses }
    c.rental_resale_output_pre_tax_roi_ratio :percent,          calc: proc {|m| m.rental_resale_output_pre_tax_roi_amount / m.rental_resale_output_total_expenses }
    c.rental_resale_output_post_tax_roi_amount :money,          calc: proc {|m| m.rental_resale_output_pre_tax_roi_amount < 0 ? m.rental_resale_output_pre_tax_roi_amount : m.rental_resale_output_pre_tax_roi_amount - (m.rental_resale_output_hold_weeks < 52 ? m.rental_resale_output_pre_tax_roi_amount * m.tax_rate_company_marginal : m.rental_resale_output_pre_tax_roi_amount * m.tax_rate_capital_gains_rate) }
    c.rental_resale_output_post_tax_roi_ratio :percent,         calc: proc {|m| m.rental_resale_output_post_tax_roi_amount / m.rental_resale_output_total_expenses }
    c.rental_resale_output_pre_tax_roe_ratio :percent,          calc: proc {|m| m.rental_resale_output_pre_tax_roi_amount / m.rental_resale_output_equity_contribution }
    c.rental_resale_output_post_tax_roe_ratio :percent,         calc: proc {|m| m.rental_resale_output_post_tax_roi_amount / m.rental_resale_output_equity_contribution }
    c.rental_resale_output_max_renovation_amount :money,        calc: proc {|m| m.rental_resale_output_sale_price - (m.rental_resale_output_purchase_expense + m.rental_resale_output_carrying_expense + m.rental_resale_output_lending_expense + m.rental_resale_output_realtor_fees_closing_expense + m.rental_resale_output_seller_contributions + (m.rental_resale_output_sale_price * m.definition.return_minimum_cap_rate)) }
    c.rental_resale_output_max_renovation_ratio :percent,       calc: proc {|m| m.rental_resale_output_max_renovation_amount / m.rental_resale_output_renovation_expense }
    c.rental_resale_output_max_purchase_amount :money,          calc: proc {|m| max_purchase(m.rental_resale_output_sale_price, m.rental_resale_income_sale_date, m.purchase_date, m.rental_resale_output_renovation_expense, m.rental_resale_output_carrying_expense, m.rental_resale_output_realtor_fees_closing_expense, m.rental_resale_financing_interest_only, m.loan(m.rental_resale_financing_loan, :amortization), m.rental_resale_financing_total_down_payment, m.rental_resale_financing_loan_points, m.rental_resale_financing_term, m.rental_resale_financing_interest_rate, m.definition.return_minimum_roi, m.definition.return_minimum_return_on_total_project) }
    c.rental_resale_output_max_purchase_ratio :percent,         calc: proc {|m| m.rental_resale_output_max_purchase_amount / m.purchase_price }

    c.sale_income_completion_date :date,  calc: proc {|m| m.purchase_date + (m.renovation_sale_listing_partnership_weeks * 7) }
    c.sale_income_sale_date :date,        calc: proc {|m| m.sale_income_completion_date + (m.comparables_cdom_sale + m.definition.general_days_to_close) }
    c.sale_income_sale_price :money,      calc: proc {|m| m.comparables_avm_sale.round(0) }
# 
    c.sale_expenses_realtor_fees_closing_expense :percent,  calc: proc {|m| m.definition.sale_realtor_fee_closing_costs }
    c.sale_expenses_seller_contributions :money,            calc: proc {|m| puts "$$figure : #{m.comparables_avm_sale*m.definition.sale_seller_contribution}";(m.comparables_avm_sale*m.definition.sale_seller_contribution).round(0)}
#        
    c.sale_financing_loan :loan,                            calc: proc {|m| m.definition.sale_loan }
    c.sale_financing_purchase_price :money,                 calc: proc {|m| (m.purchase_price*(1.to_d - m.definition.purchase_price_discount )).round(0)}
    c.sale_financing_renovation_price :money,               calc: proc {|m| (m.renovation_sale_listing_partnership_cost).round(0)}
     c.sale_expenses_total_price :money,           calc: proc {|m| m.sale_financing_purchase_price + m.sale_financing_renovation_price}
     
    c.sale_financing_down_payment_purchase_rate :percent,   calc: proc {|m| m.definition.lending_bridge_purchase_down_payment }
    c.sale_financing_down_payment_purchase :money,          calc: proc {|m| m.sale_financing_down_payment_purchase_rate*m.sale_financing_purchase_price }
    c.sale_financing_down_payment_renovation_rate :percent, calc: proc {|m| m.definition.lending_bridge_renovation_down_payment }
    c.sale_financing_down_payment_renovation :money,        calc: proc {|m| m.sale_financing_down_payment_renovation_rate*m.sale_financing_renovation_price }
    c.sale_financing_commission_credit_rate :percent,       calc: proc {|m| m.purchase_commission }
    c.sale_financing_commission_credit :money,              calc: proc {|m| (m.purchase_price * m.sale_financing_commission_credit_rate).round(0) }
    
    
    c.sale_financing_interest_rate :percent,                calc: proc {|m| m.definition.lending_bridge_interest_rate  }
    c.sale_financing_loan_points_rate :percent,             calc: proc {|m| m.loan(m.sale_financing_loan, :points) }
    
    c.sale_financing_total_price :money,                    calc: proc {|m| m.sale_expenses_total_price }
    
    c.sale_financing_total_available_rate :percent,         calc: proc {|m| m.definition.lending_bridge_max_ltv  }
    c.sale_financing_total_available :money,                calc: proc {|m| m.sale_financing_total_price*m.sale_financing_total_available_rate }
    c.sale_financing_loan_amount :money,                    calc: proc {|m| [m.sale_financing_total_available,(m.sale_expenses_total_price - m.sale_financing_down_payment_purchase - m.sale_financing_down_payment_renovation)].min }
    c.sale_financing_loan_points :money,                    calc: proc {|m| m.sale_financing_loan_amount * m.sale_financing_loan_points_rate }
    c.sale_financing_total_down_payment :money,             calc: proc {|m| m.sale_financing_down_payment_purchase + m.sale_financing_down_payment_renovation - m.sale_financing_commission_credit + m.sale_financing_loan_points }
    c.sale_financing_term :integer,                         calc: proc {|m| m.definition.lending_bridge_term}
    c.sale_financing_interest_only :boolean,                calc: proc {|m| m.definition.lending_bridge_interest_only }
    c.sale_financing_closing_costs_rate :percent,           calc: proc {|m| m.definition.lending_bridge_closing_costs }
    c.sale_financing_closing_costs :money,                  calc: proc {|m| m.sale_financing_loan_amount * m.sale_financing_closing_costs_rate }
    c.sale_financing_monthly_payment :money,                calc: proc {|m| m.sale_financing_interest_only ?  (m.sale_financing_loan_amount*m.sale_financing_interest_rate/12) :  -Math.pmt(m.sale_financing_interest_rate/12,m.sale_financing_term,m.sale_financing_loan_amount,0,0) }
#   IF(sale_financing_interest_only="yes",(sale_financing_loan_amount*sale_financing_interest_rate/12),-PMT(sale_financing_interest_rate/12,sale_financing_term,sale_financing_loan_amount,0,0))
# loan_payment(m.sale_financing_loan_amount, m.sale_financing_interest_rate, 12.to_d, 12.to_d, m.loan(m.sale_financing_loan, :amortization), m.sale_financing_interest_only ? nil : 0.to_d)
    

    c.sale_output_sale_price :money,                    calc: proc {|m| m.sale_income_sale_price }
    c.sale_output_sale_date :date,                      calc: proc {|m| m.sale_income_sale_date }
    c.sale_output_purchase_expense :money,              calc: proc {|m| m.sale_financing_purchase_price.round(0)  }
    c.sale_output_renovation_expense :money,            calc: proc {|m| m.sale_financing_renovation_price.round(0) }
    c.sale_output_hold_weeks :decimal,                  calc: proc {|m| (m.sale_income_sale_date - m.purchase_date)/7 }
    c.sale_output_carrying_expense :money,              calc: proc {|m| ((m.carrying_expenses_annual / 52.to_d) * m.sale_output_hold_weeks).round(0) }
    c.sale_output_lending_expense :money,               calc: proc {|m| (m.sale_output_hold_weeks / 4.33.to_d * m.sale_financing_monthly_payment + m.sale_financing_closing_costs + m.sale_financing_loan_points).round(0) }

    c.sale_output_realtor_fees_closing_expense :money,  calc: proc {|m| (m.sale_output_sale_price * m.sale_expenses_realtor_fees_closing_expense).round(0) }   
    c.sale_output_seller_contributions :money,          calc: proc {|m| m.sale_expenses_seller_contributions.round(0) }
    c.sale_output_total_expenses :money,                calc: proc {|m| m.sale_output_purchase_expense + m.sale_output_renovation_expense + m.sale_output_carrying_expense + m.sale_output_lending_expense + m.sale_output_realtor_fees_closing_expense + m.sale_output_seller_contributions - m.sale_financing_commission_credit}
    c.sale_output_equity_contribution :money,           calc: proc {|m| m.sale_financing_total_down_payment }
    c.sale_output_pre_tax_roi_amount :money,            calc: proc {|m| m.sale_output_sale_price - m.sale_output_total_expenses }
    c.sale_output_pre_tax_roi_ratio :percent,           calc: proc {|m| m.sale_output_pre_tax_roi_amount / m.sale_output_total_expenses }
    c.sale_output_post_tax_roi_amount :money,           calc: proc {|m| m.sale_output_pre_tax_roi_amount - (m.sale_output_hold_weeks < 52 ? m.sale_output_pre_tax_roi_amount*m.tax_rate_company_marginal : m.sale_output_pre_tax_roi_amount*m.tax_rate_capital_gains_rate) }
    c.sale_output_post_tax_roi_ratio :percent,          calc: proc {|m| m.sale_output_post_tax_roi_amount / m.sale_output_total_expenses }
    c.sale_output_pre_tax_roe_ratio :percent,           calc: proc {|m| m.sale_output_pre_tax_roi_amount / m.sale_output_equity_contribution }
    c.sale_output_post_tax_roe_ratio :percent,          calc: proc {|m| m.sale_output_post_tax_roi_amount / m.sale_output_equity_contribution }
    
    c.sale_output_minimum_roi :percent,                 calc: proc {|m| m.definition.return_minimum_roi  }
    c.sale_output_minimum_return :money,                calc: proc {|m| (m.definition.return_minimum_return_on_total_project).round(0) }
    c.sale_output_max_renovation_amount :money,         calc: proc {|m|[m.sale_income_sale_price - m.sale_financing_purchase_price - (m.sale_income_sale_price*0.09) - m.sale_income_sale_price *  ( ((m.sale_output_minimum_roi.nil?) || (m.sale_output_minimum_roi ==  m.definition.return_minimum_roi)) ?  m.definition.return_minimum_roi : m.sale_output_minimum_roi ) , m.sale_income_sale_price - m.sale_financing_purchase_price - (m.sale_income_sale_price*0.09) - ( ((m.sale_output_minimum_return.nil?) || (m.sale_output_minimum_return ==  m.definition.return_minimum_return_on_total_project)) ?  m.definition.return_minimum_return_on_total_project : m.sale_output_minimum_return )].min}
                                                 
    c.sale_output_max_renovation_ratio :percent,        calc: proc {|m| m.sale_output_max_renovation_amount / m.sale_financing_renovation_price }
    c.sale_output_max_purchase_amount :money,           calc: proc {|m|  [m.sale_income_sale_price - m.sale_financing_renovation_price - (m.sale_income_sale_price*0.09) - m.sale_income_sale_price* ( ((m.sale_output_minimum_roi.nil?) || (m.sale_output_minimum_roi ==  m.definition.return_minimum_roi)) ?  m.definition.return_minimum_roi : m.sale_output_minimum_roi ) , m.sale_income_sale_price - m.sale_financing_renovation_price - (m.sale_income_sale_price*0.09) -  ( ((m.sale_output_minimum_return.nil?) || (m.sale_output_minimum_return ==  m.definition.return_minimum_return_on_total_project)) ?  m.definition.return_minimum_return_on_total_project : m.sale_output_minimum_return ) ].min  }
    c.sale_output_max_purchase_ratio :percent,          calc: proc {|m|  m.sale_output_max_purchase_amount / m.sale_financing_purchase_price }
                                                                            
    c.sale_output_max_renov_to_predict_ratio :percent,  calc: proc {|m| m.sale_output_max_renovation_amount / m.sale_financing_renovation_price  }
    c.sale_output_max_purchase_to_lp_ratio  :percent,   calc: proc {|m| m.sale_output_max_purchase_amount / m.sale_financing_purchase_price }


    c.build_income_completion_date :date,           calc: proc {|m| m.purchase_date + (m.renovation_build_weeks * 7)   }
    c.build_income_sale_date :date,                 calc: proc {|m| m.build_income_completion_date  + m.comparables_cdom_build + m.definition.general_days_to_close }   
    c.build_income_sale_price :money,               calc: proc {|m| (m.comparables_avm_build).round(0) }

#     
    c.build_expenses_realtor_fees_closing_expense :percent, calc: proc {|m| m.definition.build_realtor_fee_closing_costs }
    c.build_expenses_seller_contributions :money,           calc: proc {|m| (m.build_income_sale_price * m.definition.build_seller_contribution).round(0) }
    c.build_expenses_purchase_price :money,       calc: proc {|m| (m.purchase_price * (1.to_d -  m.definition.purchase_price_discount)).round(0) }
#     
    c.build_financing_loan :loan,                       calc: proc {|m| m.definition.build_loan }
    c.build_financing_total_build_cost :money,          calc: proc {|m| m.renovation_build_cost.round(0) }
    c.build_expenses_total_price :money,          calc: proc {|m| m.build_expenses_purchase_price + m.build_financing_total_build_cost}
    c.build_financing_total_price :money,         calc: proc {|m| m.build_expenses_total_price}
    
    c.build_financing_down_payment_rate :percent,       calc: proc {|m| m.definition.lending_short_down_payment }
    c.build_financing_down_payment :money,              calc: proc {|m| m.build_financing_down_payment_rate * m.build_financing_total_price }
    c.build_financing_commission_credit_rate :percent,  calc: proc {|m| m.purchase_commission }
    c.build_financing_commission_credit :money,         calc: proc {|m| m.purchase_price * m.build_financing_commission_credit_rate }
    c.build_financing_total_down_payment :money,        calc: proc {|m| m.build_financing_down_payment - m.build_financing_commission_credit }  
    c.build_financing_total_available_rate :percent,    calc: proc {|m| m.definition.lending_short_max_ltv }
    c.build_financing_total_available :money,           calc: proc {|m| m.comparables_avm_build * m.build_financing_total_available_rate }  
    c.build_financing_loan_amount :money,               calc: proc {|m| [m.build_financing_total_available,(m.build_financing_total_price - m.build_financing_down_payment)].min } 
                                                                        
    c.build_financing_interest_rate :percent,           calc: proc {|m| m.definition.lending_short_interest_rate }
    c.build_financing_loan_points_rate :percent,        calc: proc {|m| m.definition.lending_short_points }
    c.build_financing_loan_points :money,               calc: proc {|m| m.build_financing_loan_amount * m.build_financing_loan_points_rate }

    c.build_financing_term :integer,                    calc: proc {|m| m.definition.lending_short_term }
    c.build_financing_interest_only :boolean,           calc: proc {|m| true }
    c.build_financing_closing_costs_rate :percent,      calc: proc {|m| m.definition.lending_bridge_closing_costs }
    c.build_financing_closing_costs :money,             calc: proc {|m| m.build_financing_loan_amount * m.build_financing_closing_costs_rate }   
    c.build_financing_monthly_payment :money,           calc: proc {|m| m.build_financing_interest_only ? (m. build_financing_loan_amount*m. build_financing_interest_rate/12.to_d)   :  - Math.pmt(m. build_financing_interest_rate/12.to_d,m. build_financing_term,m. build_financing_loan_amount,0,0)  }
# 
    c.build_output_sale_price :money,                     calc: proc {|m| m.build_income_sale_price }
    c.build_output_sale_date :date,                       calc: proc {|m| m.build_income_sale_date }
    c.build_output_total_build_expenses :money,           calc: proc {|m| m.renovation_build_cost}
    c.build_output_hold_weeks :decimal,                   calc: proc {|m| (m.build_income_sale_date - m.purchase_date) / 7.to_d }  
    c.build_output_carrying_expense :money,               calc: proc {|m| (m.carrying_expenses_annual / 52.to_d) * m.build_output_hold_weeks }   
    c.build_output_lending_expense :money,                calc: proc {|m| (m.build_output_hold_weeks / 4.33.to_d * m.build_financing_monthly_payment) + m.build_financing_closing_costs + m.build_financing_loan_points }

    c.build_output_realtor_fees_closing_expense :money,   calc: proc {|m| m.build_expenses_realtor_fees_closing_expense * m.build_income_sale_price }

    c.build_output_seller_contributions :money,           calc: proc {|m| m.build_expenses_seller_contributions } 
    c.build_output_commission_credit :money,      calc: proc {|m| m.build_financing_commission_credit}
    c.build_output_purchase_expense :money,       calc: proc {|m| m.build_expenses_purchase_price}
    c.build_output_total_price :money,            calc: proc {|m| m.build_output_purchase_expense + m.build_output_total_build_expenses} 
    c.build_output_total_expenses :money,                 calc: proc {|m| m.build_output_total_price + m.build_output_carrying_expense + m.build_output_lending_expense + m.build_output_realtor_fees_closing_expense + m.build_output_seller_contributions - m.build_output_commission_credit }

    c.build_output_equity_contribution :money,            calc: proc {|m| m.build_financing_total_down_payment }
    c.build_output_pre_tax_roi_amount :money,             calc: proc {|m| m.build_output_sale_price - m.build_output_total_expenses }

    c.build_output_pre_tax_roi_ratio :percent,            calc: proc {|m| m.build_output_pre_tax_roi_amount / m.build_output_total_expenses }

    

    c.build_output_pre_tax_roe_ratio :percent,            calc: proc {|m| m.build_output_pre_tax_roi_amount / m.build_output_equity_contribution }

    

    c.build_output_post_tax_roi_amount :money,            calc: proc {|m| m.build_output_pre_tax_roi_amount - ((m.build_output_hold_weeks < 52) ? m.build_output_pre_tax_roi_amount * m.tax_rate_company_marginal : m.build_output_pre_tax_roi_amount * m.tax_rate_capital_gains_rate )}
    c.build_output_post_tax_roi_ratio :percent,           calc: proc {|m| m.build_output_post_tax_roi_amount / m.build_output_total_expenses   }
    c.build_output_post_tax_roe_ratio :percent,           calc: proc {|m| m.build_output_post_tax_roi_amount / m.build_output_equity_contribution }
    
    c.build_output_minimum_roi                          :percent,  calc: proc {|m| m.definition.return_minimum_roi }
    c.build_output_minimum_cap_rate                     :money,    calc: proc {|m| m.definition.return_minimum_return_on_total_project }

    c.build_output_max_build_amount :money,               calc: proc {|m| m.build_income_sale_price - m.build_expenses_purchase_price - (m.build_income_sale_price*0.12) - ( ((m.build_output_minimum_roi.nil?) || (m.build_output_minimum_roi ==  m.definition.return_minimum_roi)) ?  m.definition.return_minimum_roi : m.build_output_minimum_roi )*m.build_income_sale_price }
    c.build_output_max_build_ratio :percent,              calc: proc {|m| m.build_output_max_build_amount / m.build_financing_total_build_cost }

    c.build_output_max_purchase_amount :money,            calc: proc {|m| m.build_income_sale_price - m.build_financing_total_build_cost - (m.build_income_sale_price*0.12) - ( ((m.build_output_minimum_roi.nil?) || (m.build_output_minimum_roi ==  m.definition.return_minimum_roi)) ?  m.definition.return_minimum_roi : m.build_output_minimum_roi )*m.build_income_sale_price}
    c.build_output_max_purchase_ratio :percent,           calc: proc {|m| m.build_output_max_purchase_amount / m.build_expenses_purchase_price }
    
   
    c.build_output_max_construction_to_predict_ratio    :percent,    calc: proc {|m| m.build_output_max_build_amount / m.build_financing_total_build_cost }
    c.build_output_max_purchase_to_lp_ratio             :percent,    calc: proc {|m| m.build_output_max_purchase_amount / m.build_expenses_purchase_price }

    # 
    c.listing_partnership_income_completion_date :date,           calc: proc {|m| m.purchase_date + (m.renovation_sale_listing_partnership_weeks * 7) + m.definition.listing_prep_days }
    c.listing_partnership_income_sale_date :date,                 calc: proc {|m| m.listing_partnership_income_completion_date + (m.comparables_cdom_listing_partnership + m.definition.general_days_to_close) }
    c.listing_partnership_income_sale_price :money,               calc: proc {|m| m.comparables_avm_sale }
    c.listing_partnership_income_commission :percent,             calc: proc {|m| m.definition.listing_partnership_commission_income }
    c.listing_partnership_income_construction_markup :percent,    calc: proc {|m| m.definition.listing_partnership_construction_markup }
    c.listing_partnership_income_percent_of_profit :percent,      calc: proc {|m| m.definition.listing_partnership_profits_percent }
     
    c.listing_partnership_expenses_marketing_legal :money,      calc: proc {|m| m.definition.listing_partnership_marketing_legal_expense }
    c.listing_partnership_expenses_seller_contributions :money, calc: proc {|m| m.listing_partnership_income_sale_price * m.definition.listing_partnership_seller_contribution }
 
    c.listing_partnership_financing_loan :loan,                            calc: proc {|m| m.definition.listing_partnership_loan }
    c.listing_partnership_financing_renovation :money,                     calc: proc {|m| m.renovation_sale_listing_partnership_cost }
    c.listing_partnership_financing_down_payment_renovation_rate :percent, calc: proc {|m| m.definition.lending_bridge_renovation_down_payment }
    c.listing_partnership_financing_down_payment_renovation :money,        calc: proc {|m| m.listing_partnership_financing_renovation * m.listing_partnership_financing_down_payment_renovation_rate }
    c.listing_partnership_financing_total_down_payment :money,             calc: proc {|m| m.listing_partnership_financing_down_payment_renovation }
    c.listing_partnership_financing_loan_amount :money,                    calc: proc {|m| m.listing_partnership_financing_renovation - m.listing_partnership_financing_total_down_payment }
    c.listing_partnership_financing_interest_rate :percent,                calc: proc {|m| m.loan(m.listing_partnership_financing_loan, :interest_rate) }
    c.listing_partnership_financing_loan_points_rate :percent,             calc: proc {|m| m.loan(m.listing_partnership_financing_loan, :points) }
    c.listing_partnership_financing_loan_points :money,                    calc: proc {|m| m.listing_partnership_financing_loan_amount * m.listing_partnership_financing_loan_points_rate }
    c.listing_partnership_financing_term :integer,                         calc: proc {|m| m.loan(m.listing_partnership_financing_loan, :term) }
    c.listing_partnership_financing_interest_only :boolean,                calc: proc {|m| m.loan(m.listing_partnership_financing_loan, :interest_only) }
    c.listing_partnership_financing_closing_costs_rate :percent,           calc: proc {|m| m.loan(m.listing_partnership_financing_loan, :closing_costs) }
    c.listing_partnership_financing_closing_costs :money,                  calc: proc {|m| m.listing_partnership_financing_loan_amount * m.listing_partnership_financing_closing_costs_rate }
    c.listing_partnership_financing_monthly_payment :money,                calc: proc {|m| loan_payment(m.listing_partnership_financing_loan_amount, m.listing_partnership_financing_interest_rate, 12.to_d, 12.to_d, m.loan(m.listing_partnership_financing_loan, :amortization), m.listing_partnership_financing_interest_rate ? nil : 0.to_d) }
 
    c.listing_partnership_output_sale_price :money,                         calc: proc {|m| m.listing_partnership_income_sale_price }
    c.listing_partnership_output_sale_date :date,                           calc: proc {|m| m.listing_partnership_income_sale_date }
    c.listing_partnership_output_purchase_price :money,                     calc: proc {|m| m.purchase_price }
    c.listing_partnership_output_renovation_expense :money,                 calc: proc {|m| m.listing_partnership_financing_renovation }
    c.listing_partnership_output_hold_weeks :decimal,                       calc: proc {|m| (m.listing_partnership_income_sale_date - m.purchase_date) / 7.to_d }
    c.listing_partnership_output_lending_expense :money,                    calc: proc {|m| m.listing_partnership_output_hold_weeks / 4.33.to_d * m.listing_partnership_financing_monthly_payment + m.listing_partnership_financing_closing_costs + m.listing_partnership_financing_loan_points }
    c.listing_partnership_output_marketing_legal_expense :money,            calc: proc {|m| m.listing_partnership_expenses_marketing_legal }
    c.listing_partnership_output_seller_contributions :money,               calc: proc {|m| m.listing_partnership_expenses_seller_contributions }
    c.listing_partnership_output_total_expenses :money,                     calc: proc {|m| m.listing_partnership_output_sale_price - m.listing_partnership_output_purchase_price - m.listing_partnership_output_renovation_expense - m.listing_partnership_output_lending_expense - m.listing_partnership_output_marketing_legal_expense - m.listing_partnership_output_seller_contributions }
    c.listing_partnership_output_equity_contribution :money,                calc: proc {|m| m.listing_partnership_output_renovation_expense }
    c.listing_partnership_output_overall_return :money,                     calc: proc {|m| m.listing_partnership_output_sale_price - m.listing_partnership_output_total_expenses }
    c.listing_partnership_output_commission_income :money,                  calc: proc {|m| m.listing_partnership_output_sale_price * m.listing_partnership_income_commission }
    c.listing_partnership_output_construction_reimbursement_income :money,  calc: proc {|m| m.listing_partnership_output_renovation_expense + m.listing_partnership_output_renovation_expense * m.listing_partnership_income_construction_markup }
    c.listing_partnership_output_profit_participation :money,               calc: proc {|m| m.listing_partnership_output_overall_return * m.listing_partnership_income_percent_of_profit }
    c.listing_partnership_output_pre_tax_roi_amount :money,                 calc: proc {|m| m.listing_partnership_output_commission_income + m.listing_partnership_output_construction_reimbursement_income + m.listing_partnership_output_profit_participation - m.listing_partnership_output_renovation_expense }
    c.listing_partnership_output_pre_tax_roi_ratio :percent,                calc: proc {|m| m.listing_partnership_output_pre_tax_roi_amount / m.listing_partnership_output_equity_contribution }
    c.listing_partnership_output_post_tax_roi_amount :money,                calc: proc {|m| m.listing_partnership_output_pre_tax_roi_amount < 0 ?  m.listing_partnership_output_pre_tax_roi_amount : m.listing_partnership_output_pre_tax_roi_amount - (m.listing_partnership_output_hold_weeks < 52 ? m.listing_partnership_output_pre_tax_roi_amount * m.tax_rate_company_marginal : m.listing_partnership_output_pre_tax_roi_amount * m.tax_rate_capital_gains_rate) }
    c.listing_partnership_output_post_tax_roi_ratio :percent,               calc: proc {|m| m.listing_partnership_output_post_tax_roi_amount / m.listing_partnership_output_construction_reimbursement_income }
    c.listing_partnership_output_pre_tax_roe_ratio :percent,                calc: proc {|m| m.listing_partnership_output_pre_tax_roi_amount / m.listing_partnership_output_profit_participation }
    c.listing_partnership_output_post_tax_roe_ratio :percent,               calc: proc {|m| m.listing_partnership_output_post_tax_roi_amount / m.listing_partnership_output_profit_participation }
    c.listing_partnership_output_max_renovation_amount :money,              calc: proc {|m| m.listing_partnership_output_sale_price - (Math.sum(m.listing_partnership_output_purchase_price, m.listing_partnership_output_lending_expense, m.listing_partnership_output_marketing_legal_expense, m.listing_partnership_output_seller_contributions) + (m.listing_partnership_output_sale_price * m.definition.return_minimum_roi))}
    c.listing_partnership_output_max_renovation_ratio :percent,             calc: proc {|m| m.listing_partnership_output_max_renovation_amount / m.listing_partnership_output_renovation_expense }
    c.listing_partnership_output_max_purchase_amount :money,                calc: proc {|m| max_purchase(m.listing_partnership_output_sale_price, m.listing_partnership_output_sale_date, m.purchase_date, m.listing_partnership_output_renovation_expense, 0, 0, m.listing_partnership_financing_interest_only, m.loan(m.listing_partnership_financing_loan, :amortization), m.listing_partnership_financing_total_down_payment, m.listing_partnership_financing_loan_points, m.listing_partnership_financing_term, m.listing_partnership_financing_interest_rate, m.definition.return_minimum_roi, m.definition.return_minimum_return_on_total_project) }
    c.listing_partnership_output_max_purchase_ratio :percent,               calc: proc {|m| m.listing_partnership_output_max_purchase_amount / m.purchase_price }
 
    c.assignment_income_sale_date :date,                calc: proc {|m| m.purchase_date + (m.comparables_cdom_sale - (m.comparables_cdom_sale * m.definition.assignment_sale_time_discount)) }
    c.assignment_income_investor_arv_sale_ltv :percent, calc: proc {|m| m.definition.assignment_investor_arv_sale_ltv }
    c.assignment_income_sale_price :money,              calc: proc {|m| (m.comparables_avm_sale * m.definition.assignment_investor_arv_sale_ltv - m.renovation_assignment_cost) * m.assignment_income_investor_arv_sale_ltv }
    
    c.assignment_expenses_marketing :money, calc: proc {|m| m.definition.assignment_marketing_expense }
    c.assignment_expenses_closing :money,   calc: proc {|m| m.definition.assignment_closing_expense }
 
    c.assignment_output_sale_price :money,              calc: proc {|m| m.assignment_income_sale_price }
    c.assignment_output_acquisition_price :money,       calc: proc {|m| m.purchase_price }
    c.assignment_output_income :money,                  calc: proc {|m| m.assignment_output_sale_price - m.assignment_output_acquisition_price }
    c.assignment_output_marketing_expense :money,       calc: proc {|m| m.assignment_expenses_marketing }
    c.assignment_output_closing_expenses :money,        calc: proc {|m| m.assignment_expenses_closing }
    c.assignment_output_total_expenses :money,          calc: proc {|m| m.assignment_output_acquisition_price + m.assignment_output_marketing_expense + m.assignment_output_closing_expenses }
    c.assignment_output_pre_tax_roi_amount :money,      calc: proc {|m| m.assignment_output_sale_price - m.assignment_output_total_expenses }
    c.assignment_output_post_tax_roi_amount :money,     calc: proc {|m| m.assignment_output_pre_tax_roi_amount * (1.to_d - m.tax_rate_company_marginal) }
    c.assignment_output_max_renovation_amount :money,   calc: proc {|m| m.assignment_output_sale_price - (m.assignment_output_acquisition_price + Math.sum(m.assignment_output_marketing_expense, m.assignment_output_closing_expenses) + (m.assignment_output_sale_price * m.definition.return_minimum_roi))}
    c.assignment_output_max_renovation_ratio :percent,  calc: proc {|m| m.assignment_output_max_renovation_amount / m.assignment_output_acquisition_price }
    c.assignment_output_max_purchase_amount :money,     calc: proc {|m| max_purchase(m.assignment_output_sale_price, m.assignment_income_sale_date, m.purchase_date, m.assignment_output_acquisition_price, 0, m.assignment_output_closing_expenses, true, nil, 0, 0, nil, nil, m.definition.return_minimum_roi, m.definition.return_minimum_return_on_total_project) }
    c.assignment_output_max_purchase_ratio :percent,    calc: proc {|m| m.assignment_output_max_purchase_amount / m.purchase_price }
 
    c.listing_income_listing_date :date,         calc: proc {|m| m.purchase_date + m.definition.listing_prep_days }
    c.listing_income_sale_date :date,            calc: proc {|m| m.listing_income_listing_date + m.comparables_cdom_sale + m.definition.general_days_to_close }
    c.listing_income_sale_price :money,          calc: proc {|m| m.comparables_avm_sale - (m.comparables_avm_sale * m.definition.listing_sale_comp_discount) }
    c.listing_income_commission :percent,        calc: proc {|m| m.definition.listing_commission_income }
     
    c.listing_expenses_marketing :money, calc: proc {|m| m.definition.listing_marketing_expense }
    c.listing_expenses_agent :percent,   calc: proc {|m| m.definition.listing_agent_expense }

    c.listing_output_sale_price :money,           calc: proc {|m| m.listing_income_sale_price }
    c.listing_output_income :money,               calc: proc {|m| m.listing_output_sale_price * m.listing_income_commission }
    c.listing_output_marketing_expense :money,    calc: proc {|m| m.listing_expenses_marketing }
    c.listing_output_agent_expense :money,        calc: proc {|m| m.listing_output_sale_price * m.listing_income_commission * m.listing_expenses_agent }
    c.listing_output_total_expenses :money,       calc: proc {|m| m.listing_output_marketing_expense + m.listing_output_agent_expense }
    c.listing_output_pre_tax_roi_amount :money,   calc: proc {|m| m.listing_output_income - m.listing_output_total_expenses }
    c.listing_output_post_tax_roi_amount :money,  calc: proc {|m| m.listing_output_pre_tax_roi_amount < 0 ?  m.listing_output_pre_tax_roi_amount : m.listing_output_pre_tax_roi_amount * (1.to_d - m.tax_rate_company_marginal) }
   
    
   
   
    c.sale_output_total_price :money,             calc: proc {|m| m.sale_expenses_total_price}
    c.sale_output_commission_credit :money,       calc: proc {|m| m.sale_financing_commission_credit}
    c.sale_output_ltv :percent,                     calc: proc {|m| m.sale_expenses_total_price/m.sale_income_sale_price}
    
    c.rental_expenses_total_price :money,         calc: proc {|m| (m.rental_output_purchase_expense + m.rental_output_renovation_expense).round(0)}
    c.rental_expenses_property_taxes :money,      calc: proc {|m| (m.carrying_expenses_property_taxes).round(0)}
    
    
    
    
    
    
    c.rental_output_monthly_gross_rent :money,          calc: proc {|m| m.rental_income_monthly_gross_rent}
    
    
    
    c.rental_output_gross_yield :percent,           calc: proc {|m|  
        if ( m.rental_output_recommended_offer && m.rental_output_recommended_offer > 0 )
          rsl = ((m.rental_income_total_net_income )/m.rental_output_recommended_offer)
        else
          rsl = ( (m.rental_income_total_net_income )/m.rental_output_calc_offer)
        end
        rsl
    } # m.rental_income_annual_gross_rent/m.rental_financing_purchase_renovation}
    c.rental_output_dscr :decimal,                  calc: proc {|m| n = m.rental_output_noi / m.rental_output_annual_principal_interest; n.try(:nan?) ? 0 : n}
   
    c.build_recommended_size :money,              calc: proc {|m|  m.property.comps_build_stats[:projected_square_feet]}
    c.build_cost_sq_ft :money,                    calc: proc {|m| n = m.build_financing_total_build_cost / m.build_recommended_size.to_d;  n.try(:nan?) ? 0 : n}
    
    
    
    
    
    
    
    

    c.rental_output_repair_description :string,                     calc: proc {|m|  
        
        desc_part = case m.rental_output_condition_grade 
            when 'A'
                "Capex Grade A:(7) Rent Ready. Light Paint, Final Clean, Rekey, HVAC Service Only."
            when 'B'
                "Capex Grade B:(6) Partial Carpet, 1/2 Paint, Minor Cosmetics, Improve Curb Appeal. Provide new Refridgerator only."
            when 'C'
                "Capex Grade C:(5) Paint, Carpet, Replace All Appliances, Update Kitchen or Two Baths, HVAC Service Only. Not More than 1/2 fixtures replaced."
            when 'D'
                "Capex Grade D:(4) Paint, Carpet, Other Flooring, Appliances, Update Kitchen and Baths, HVAC Replace. All Fixtures Replace, Water Heater Replace, Extra Trash Out. Extra Landscape Repair."
            when 'E'
                ""
            when 'F'
                "Capex Grade F:(2) Paint, Carpet, Other Flooring, Appliances, Update Kitchen and Baths, HVAC Replace. All Fixtures, Replace Water Heater Replace, Extra Trash Out. Extra Landscape Repair, Garage Doors, Broken Windows."
            else
                ""
        end
        if (m.rental_output_renovation_user_entry && m.rental_output_renovation_user_entry > 0)
            used = m.rental_output_renovation_user_entry
        else
            used = ((([m.renovation_rehab_for(m.property.year_built, "", :rental),m.renovation_rental_rehab * m.property.square_feet].reject(&:blank?).max)/1000.to_d).ceil * 1000)
        end

        rh = m.property.square_feet ? (((used.to_d / m.property.square_feet)/0.25).floor * 0.25).to_i : 0
        
        calc_part = "#{m.property.square_feet} sq ft. x $#{rh} per sft.= $#{used}"        
        [desc_part,calc_part].join(" ")      

    }

                
    
  end

  def restore(persist=false)
    self._overrides = {}
    if persist
      save(validate: false)
    else
      self.send(:calculate_fields) 
      @changed_attributes = {}
    end
  end
  
  def self.user_proforma_exists( property_id, user_id)
    true if select("id, property_id, user_id").where(["property_id = ? and  user_id = ?", property_id, user_id] ).count > 0
  end
  
  def source
    @source ||= (self.user_id ? self.user.assignments.where(market_id: property.market_id).first : property.market)
  end

  def definition
    @definition ||= self.source.definition
  end

  def loan(type, property)
    self.definition.send("lending_#{type}_#{property}")
  end

  def self.max_purchase(sale_price, sale_date, purchase_date, renovation_expense, carrying_expense, realtor_expense, interest_only, amortization, down_payment, points, term, rate, desired_yield, min_return)
    p "self.max_purchase(#{[sale_price, sale_date, purchase_date, renovation_expense, carrying_expense, realtor_expense, interest_only, amortization, down_payment, points, term, rate, desired_yield, min_return].join(', ')})" if ActiveConfig.debug?
    total_investment = down_payment
    loan_amount = 1 - total_investment
    points_expense = points * loan_amount
    total_loan_amount = loan_amount + points_expense
    return_sale_price = [sale_price * desired_yield, min_return].max
    if interest_only
      paid_amount = 0
      loan_payments = 0
    else
      term_of_loan = amortization * 12
      payoff_months = term_of_loan
      interest_monthly = rate / 12
      pmt = (((1+interest_monthly)**term_of_loan)*interest_monthly)/(((1+interest_monthly)**term_of_loan)-1)
      pmt_loan = total_loan_amount * pmt
      months_held = diff_months(sale_date, purchase_date)
      paid_amount = interest_only ? 0 : (Math.cumprinc(interest_monthly,term_of_loan,total_loan_amount,1,6,0)/6 * -1) * months_held
      loan_payments = pmt_loan * months_held
    end
    loan_payoff = total_loan_amount - paid_amount
    ti_lp_paid = Math.sum(total_investment, loan_payoff, loan_payments)
    sp_ce_te_re = sale_price - Math.sum(renovation_expense, carrying_expense, realtor_expense)
    sp_ce_te_tr = sp_ce_te_re - return_sale_price
    result = (sp_ce_te_tr / ti_lp_paid / 1000).round(0) * 1000
    p [total_investment, loan_amount, points_expense, total_loan_amount, return_sale_price, term_of_loan, payoff_months, interest_monthly, pmt, pmt_loan, months_held, paid_amount, loan_payments, loan_payoff, ti_lp_paid, sp_ce_te_re, sp_ce_te_tr, result].join(', ') if ActiveConfig.debug?
    result
  end

  def self.diff_months(d1, d2, full=true)
    r = 12 * (d1.year - d2.year).abs + (d1.month - d2.month).abs
    r -= 1 if full && (d1 - d2) * (d1.day - d2.day) < 0
    r
  end

  def self.loan_rate(interest_rate, compound_period, periods_per_year)
    ((1.to_d+interest_rate/compound_period)**(compound_period/periods_per_year))-1.to_d
  end

  def self.loan_payment(loan_amount, interest_rate, compound_period, periods_per_year, amortization, payment_type=nil)
    rate = loan_rate(interest_rate.to_d, compound_period.to_d, periods_per_year.to_d)
    if payment_type.nil?
      rate * loan_amount.to_d
    else
      -Math.pmt(rate, amortization || 360, loan_amount.to_d, 0, payment_type)
    end
  end

  def build_cost_for(value, target)
    @build_costs ||= self.source.build_costs
    @build_costs.each do |build_cost|
      return build_cost.send(target) if build_cost.include?(value)
    end
  end

  def renovation_timeline_for(value, target)
    @timelines ||= self.source.renovation_timelines
    @timelines.each do |timeline|
      return timeline.send(target) if timeline.include?(value)
    end
  end

  def renovation_timeline_get(value)
    @timelines ||= self.source.renovation_timelines.order(:weeks)
   
    @timelines.each do |timeline|
      puts ">>timeline #{timeline.min} #{timeline.max}"
      if value < timeline.max
        return timeline.send(:weeks) 
      end
    end

    return
  end

  def renovation_rehab_for(year,cg,target)
    @rehabs ||= self.source.client_renovation_rehabs
    
    @rehabs.each do |rehab|
      
      return rehab.send(target) if rehab.include?(year,cg)
    end
  end

end
# based on http://www.8tiny.com/source/phpmyadmin/nav.html?_functions/index.html
module Math
  
  def self.default_zero(value)
    !value.nil? ?  value.round(0) : 0
  end

  def self.sum(*values)
    values.compact.inject(:+) 
  end

  def self.avg(*values)
    sum(*values) / values.compact.length.to_d rescue nil
  end

  def self.floor(value,precision)
    precison > 0 ? (value / precision).floor * precision  : 0
  end

  def self.min(*values)
    values.inject(nil) do |r, value|
      r = value if (r.nil? || value && value < r); r
    end
  end

  def self.max(*values)
    values.inject(nil) do |r, value|
      r = value if (r.nil? || value && value > r); r
    end
  end

  def self.pmt(rate, nper, pv, fv=0, type=0)
    if rate != 0
      # Interest rate exists
      q = (1.to_d + rate)**nper
      -(rate * (fv + (q * pv))) / ((-1.to_d + q) * (1.to_d + rate * (type)))
    elsif nper == 0
      # No interest rate, but number of payments exists
      -(fv + pv) / nper
    end
  end

  def self.ppmt(rate, per, nper, pv, fv=0, type=0)
    interest_and_principal = self.interest_and_principal(rate, nper, pv, 1, per, fv, type)
    interest_and_principal[:principal]
  end

  # Returns the cumulative principal paid on a loan between start_period and end_period.
  def self.cumprinc(rate, nper, pv, start_period, end_period, type)
    interest_and_principal = self.interest_and_principal(rate, nper, pv, start_period, end_period, 0, type)
    interest_and_principal[:cum_principal]
  end

  private 

    def self.interest_and_principal(rate, nper, pv, start_period, end_period, fv, type)
      pmt = self.pmt(rate, nper, pv, fv, type)
      capital = pv
      cum_principal = 0
      
      for i in 1..end_period
        interest = (type == 1 && i == 1) ? 0 : -capital * rate
        principal = pmt - interest
        capital += principal
        cum_principal += principal if i >= start_period
      end

      {interest: interest, principal: principal, cum_principal: cum_principal}
    end
end

