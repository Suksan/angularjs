class SearchConfig < ActiveModel::Base
  IN      = %w{id mls_number tax_id status category rental_class rental_type single_family_type mls_area_key geo_market_area zip_code rep tag flood near_commercial county}
  TEXT    = %w{legal_subdivision subdivision subdivision_section_number street_name city disclosures realtor_remarks}
  BOOLEAN = %w{selling_for_lot_value pool_private zillow_rent_only zillow_sale_only is_back_on_market price_reduction}
  MINMAX  = %w{beds street_number list_price bathrooms square_feet lot_square_feet year_built cdom dom owner_equity_ratio owner_equity_amount sold_price sold_lease_price listing_date closed_date pending_date termination_date withdrawn_date expiration_date comparables_avm_sale comparables_cdom_sale comparables_avm_rental comparables_cdom_rental comparables_avm_build comparables_cdom_build sale_output_renovation_expense rental_output_renovation_expense renovation_build_cost rental_output_max_purchase_ratio sale_output_max_purchase_ratio sale_output_pre_tax_roi_amount sale_output_pre_tax_roi_ratio rental_income_monthly_gross_rent rental_output_cap_rate rental_output_pre_tax_roi_amount rental_output_pre_tax_roi_ratio rental_resale_output_investor_cap_rate rental_resale_output_pre_tax_roi_amount rental_resale_output_pre_tax_roi_ratio seller_finance_output_cap_rate seller_finance_output_pre_tax_roi_amount seller_finance_output_pre_tax_roi_ratio build_output_pre_tax_roi_amount build_output_pre_tax_roi_ratio assignment_output_pre_tax_roi_amount elementary_school_rating middle_school_rating high_school_rating avg_school_rating garage_capacity}
  ALL = IN + TEXT + BOOLEAN + MINMAX
  attr_accessor *ALL

  def query(attr_proc=nil)
    attrs = self.attributes
    
    Delayed::Worker.logger.debug "attrs: #{attrs}"
    %w{id mls_number tax_id}.each do |group|
      if attrs.keys.include?(group)
        attrs = attrs.extract(group)
        break
      end
    end
    reps = []
    other_options = []
    combined = []
    cond_query = nil
    result = attrs.inject([[],[]]) do |r, (key, value)|
      attr = attr_proc ? attr_proc.call(key) : key
      internal =
        if key.in?(IN)
          
          Delayed::Worker.logger.debug "key #{key}"
          if key.in?(["tax_id"])
             plain_value = nil
             unless value.is_a?(Array)
               Delayed::Worker.logger.debug "value #{value}"
               plain_value = value.strip.gsub(/,\s+/, ',').gsub(/\s+,/,',')             
               value = value.split(',') 
             end
             
             if ['-',' '].any? { |word| plain_value.include?(word) }
               r.first << "replace(replace(#{attr},' ',''),'-','') IN (?)"
               r.last << value.reject(&:missing?).map do |d|  x = d.gsub(/\s/, '').gsub('-', '') ; x   end
             else
               r.first << "#{attr} IN (?)"
               r.last << value.reject(&:missing?).map(&:strip)
             end 
             
          elsif  key.in?(["rep"])   
             Delayed::Worker.logger.debug "~~mls area key #{value}"
            value = value.split(',') unless value.is_a?(Array)
             if (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0) && (value.select{ |k| k == "0" || k == "-999" }.length <= 0)
               
               bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
               concat_value = bookvalue.join(" ")
               Delayed::Worker.logger.debug "book matched #{bookvalue.first.match(/\A-([\A\s]+)/)}"
               if bookvalue.first.match(/\A-([\A\s]+)/)
                 
                 nots = concat_value.scan(/-([\A\s]+)/).flatten
                 
                 nots.each do |n|
                  combined << " (not (find_in_set(#{n.gsub("-","")},rep) > 0))"
                  
                 end
                 reps << "((#{attr} is null) OR (#{attr} = ''))"
                 # find_in_set(#{val.gsub("-","")},rep) > 0
                 # r.first << "(NOT ( #{attr} in (?) ) OR #{attr} is null)"
                 # r.last << nots
               else 
                 selected = concat_value.scan(/([\A\s]+)/).flatten
                 selected.each do |n|
                   combined << "(find_in_set(#{n.gsub("-","")},rep) > 0)"
                    
                 end
               end
               
               Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
                
               
            end
             if value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0)
                bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
                concat_value = bookvalue.join(" ")
                if bookvalue.first.match(/\A-([\A\s]+)/)
                  
                  nots = concat_value.scan(/-([\A\s]+)/).flatten
                  
                  nots.each do |n|
                   combined << "(not (find_in_set(#{n.gsub("-","")},rep) > 0))"
                  
                  end
                  reps << "(NOT ((#{attr} is null) OR (#{attr} = '')))"
                else
                  selected = concat_value.scan(/([\A\s]+)/).flatten
                  selected.each do |n|
                    combined << "(find_in_set(#{n.gsub("-","")},rep) > 0)"
                        
                  end
                  reps << "((#{attr} is null)  OR (#{attr} = ''))"
                end
              # if (i > 0)
                # if value.first == "0"
                  # reps << "(mls_area_key is null)"
                # else
                  # reps << "(NOT mls_area_key is null)"
                # end
                  # end
            elsif value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length <= 0)
               found = value.select{ |k| k == "0" || k == "-999" }
               if found.first == "0"
                  reps << "((#{attr} is null ) OR (#{attr} = '')) "
               else
                  reps << "(not ((#{attr} is null ) OR (#{attr} = ''))) "
               end
            end 
            found = value.reject{ |k| (k == "" || k == "0" || k == "-999") }
            if found && found.length > 0 && ! found.first.match(/\A-([\A\s]+)/)
              cond_query = combined.join(" OR ")
            else
              cond_query = combined.join(" AND ")
            end       
            reps << "(#{cond_query})" if !cond_query.blank?  
            # Delayed::Worker.logger.debug "~~rep #{value}"
            # combined = []
            # vals = value
            # cond_query = nil
#             
            # if vals && vals.length > 0
              # # vals.shift
              # vals.each_with_index do |val,i|
                # if val != "0" && val != ""  && val != "-999"
                  # if val.match(/-([^\s]+)/)
                    # combined << "(NOT (find_in_set(#{val.gsub("-","")},rep) > 0) or ((rep is null) or (rep = '')))"
                  # else
                    # combined << "(find_in_set(#{val.gsub("-","")},rep) > 0)"
                  # end
                # elsif val == "-999"
                   # reps << "NOT ((rep is null) or (rep = ''))"
                # elsif val == "0"
                  # # if (i > 0)
                  # reps << "((rep is null) or (rep = ''))"
                  # # end
                # end                
              # end
              # found = value.reject{ |k| (k == "" || k == "0" || k == "-999") }
              # if found && found.length > 0 && ! found.first.match(/^-([^\s]+)/)
                # cond_query = combined.join(" OR ")
              # else
                # cond_query = combined.join(" AND ")
              # end
              # reps << "(#{cond_query})" if !cond_query.blank?
            # end
          elsif key.in?(["tag"]) 
            Delayed::Worker.logger.debug "~~mls area key #{value}"
            value = value.split(',') unless value.is_a?(Array)
             if (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0) && (value.select{ |k| k == "0" || k == "-999" }.length <= 0)
               
               bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
               Delayed::Worker.logger.debug "book matched #{bookvalue.first.match(/\A-([\A\s]+)/)}"
               if bookvalue.first.match(/\A-([\A\s]+)/)
                 concat_value = bookvalue.join(" ")
                 nots = concat_value.scan(/-([\A\s]+)/).flatten
                 r.first << "(NOT ( assigned_tags.#{attr} in (?) ) OR assigned_tags.#{attr} is null)"
                 r.last << nots
               else 
                r.first << "assigned_tags.#{attr} in (?)"
                r.last << bookvalue
               end
               
               Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
                
               
            end
             if value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0)
                bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
                if bookvalue.first.match(/\A-([\A\s]+)/)
                  concat_value = bookvalue.join(" ")
                  nots = concat_value.scan(/-([\A\s]+)/).flatten
                  r.first << "( (NOT assigned_tags.#{attr} in (?)) OR (NOT assigned_tags.#{attr} is null)) "
                  r.last << nots
                else
                  r.first << "assigned_tags.#{attr} in (?)  OR assigned_tags.#{attr} is null "
                  r.last << bookvalue
                end
              # if (i > 0)
                # if value.first == "0"
                  # reps << "(mls_area_key is null)"
                # else
                  # reps << "(NOT mls_area_key is null)"
                # end
                  # end
            elsif value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length <= 0)
               found = value.select{ |k| k == "0" || k == "-999" }
               if found.first == "0"
                  r.first << "(assigned_tags.#{attr} is null ) "
               else
                  r.first << "(not assigned_tags.#{attr} is null ) "
               end
            end  
          elsif key.in?(["flood"]) 
            Delayed::Worker.logger.debug "~~flood #{value}"
            value = value.split(',') unless value.is_a?(Array)
            if (value.reject{ |k| (k == "" || k == "0" || k == "-1") }.length > 0) && (value.select{ |k| k == "0" || k == "-1" }.length <= 0)
               
               bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-1") }.map(&:strip)
               Delayed::Worker.logger.debug "book matched #{bookvalue.first.match(/\A-([\A\s]+)/)}"
               if bookvalue.first.match(/\A-([\A\s]+)/)
                 concat_value = bookvalue.join(" ")
                 nots = concat_value.scan(/-([\A\s]+)/).flatten
                 r.first << "NOT #{attr} in (?)"
                 r.last << nots
               else 
                r.first << "#{attr} in (?)"
                r.last << bookvalue
               end
               
               Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
                
               
            end
            if value.select{ |k| k == "0" || k == "-1" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-1") }.length > 0)
              # if (i > 0)
              bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-1") }.map(&:strip)
              r.first << "(#{attr} in (?) OR #{attr} is null ) "
              r.last << bookvalue
                
            elsif value.select{ |k| k == "0" || k == "-1" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-1") }.length <= 0)
              r.first << "(#{attr} is null ) " 
            end
          elsif key.in?(["near_commercial"]) 
            Delayed::Worker.logger.debug "~~flood #{value}"
            value = value.split(',') unless value.is_a?(Array)
             if (value.reject{ |k| (k == "" || k == "0" || k == "-1") }.length > 0) && (value.select{ |k| k == "0" || k == "-1" }.length <= 0)
               
               bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-1") }.map(&:strip)
               Delayed::Worker.logger.debug "book matched #{bookvalue.first.match(/\A-([\A\s]+)/)}"
               if bookvalue.first.match(/\A-([\A\s]+)/)
                 concat_value = bookvalue.join(" ")
                 nots = concat_value.scan(/-([\A\s]+)/).flatten
                 r.first << "NOT #{attr} in (?)"
                 r.last << nots.map { |k| if k == 'a' then true else false end }
               else 
                r.first << "#{attr} in (?)"
                r.last << bookvalue.map { |k| if k == 'a' then true else false end }
               end
               
               Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
                
               
            end
            if value.select{ |k| k == "0" || k == "-1" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-1") }.length > 0)
                bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-1") }.map(&:strip)
                r.first << "(#{attr} in (?) OR #{attr} is null ) "
                r.last << bookvalue.map { |k| if k == 'a' then true else false end }
            elsif value.select{ |k| k == "0" || k == "-1" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-1") }.length <= 0)
                r.first << "(#{attr} is null ) "              
            end
            # Delayed::Worker.logger.debug "~~near_commercial #{value}"
            # value = value.split(',') unless value.is_a?(Array)
             # if value.reject{ |k| (k == "" || k == "-999"|| k == "-9999") }.length > 0
               # bookvalue = value.reject{ |k| (k == "" || k == "-999" || k == "-9999") }.map(&:strip)
               # Delayed::Worker.logger.debug "~~book value #{bookvalue}"
               # if bookvalue.first.match(/^-([^\s]+)/)
                 # r.first << "NOT #{attr} = ?"
                 # one = bookvalue.first.match(/^-([^\s]+)/).captures
                 # Delayed::Worker.logger.debug "~~a #{one}"
                 # r.last << (one.first == "a" ? true : false) 
               # else 
                # r.first << "#{attr} = ?"
                # r.last << (bookvalue.first == "a" ? true : false)
               # end
#                
               # # r.first << "#{attr} = ?"
               # # bookvalue = value.reject{ |k| (k == "" || k == "-999") }.map(&:strip)
               # # Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
               # # r.last << (bookvalue.first == "1" ? true : false )
            # end
             # if value.select{ |k| k == "-999" || k == "-9999" }.length > 0
              # # if (i > 0)
              # if value.first == "-999"
                # reps << "(near_commercial is null)"
              # else
                # reps << "(NOT near_commercial is null)"
              # end
#              
                  # # end
            # end
          elsif key.in?(["zip_code"])
            value = value.gsub(',',' ')
            nots = value.scan(/-([\A\s]+)/).flatten
            Delayed::Worker.logger.debug "~~nots #{nots}"
            
            if nots && nots.length > 0 
              r.first << "#{attr} NOT IN (?)"
              r.last << nots
            end
            
            for n in nots
              value = value.gsub("-#{n}", '')
            end
            included = value.scan(/([\A\s]+)/).flatten
            unless value.blank?
              r.first << "#{attr} IN (?)"
              r.last << included
            end
          elsif key.in?(["mls_area_key"]) 
            Delayed::Worker.logger.debug "~~mls area key #{value}"
            value = value.split(',') unless value.is_a?(Array)
             if (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0) && (value.select{ |k| k == "0" || k == "-999" }.length <= 0)
               
               bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
               Delayed::Worker.logger.debug "book matched #{bookvalue.first.match(/\A-([\A\s]+)/)}"
               if bookvalue.first.match(/\A-([\A\s]+)/)
                 concat_value = bookvalue.join(" ")
                 nots = concat_value.scan(/-([\A\s]+)/).flatten
                 r.first << "((NOT #{attr} in (?)) OR (#{attr} is null OR #{attr} = '') )"
                 r.last << nots
               else 
                r.first << "#{attr} in (?)"
                r.last << bookvalue
               end
               
               Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
                
               
            end
             if value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0)
                bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
                if bookvalue.first.match(/\A-([\A\s]+)/)
                  concat_value = bookvalue.join(" ")
                  nots = concat_value.scan(/-([\A\s]+)/).flatten
                  r.first << "((NOT #{attr} in (?))  OR (NOT #{attr} is null) ) "
                  r.last << nots
                else
                  r.first << "(#{attr} in (?)  OR #{attr} is null) "
                  r.last << bookvalue
                end
              # if (i > 0)
                # if value.first == "0"
                  # reps << "(mls_area_key is null)"
                # else
                  # reps << "(NOT mls_area_key is null)"
                # end
                  # end
            elsif value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length <= 0)
                found = value.select{ |k| k == "0" || k == "-999" }
               if found.first == "0"
                  r.first << "(#{attr} is null ) "
               else
                  r.first << "(not #{attr} is null ) "
               end
            end
            
          elsif key.in?(["geo_market_area"]) 
            Delayed::Worker.logger.debug "~~mls area key #{value}"
            value = value.split(',') unless value.is_a?(Array)
             if (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0) && (value.select{ |k| k == "0" || k == "-999" }.length <= 0)
               
               bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
               Delayed::Worker.logger.debug "book matched #{bookvalue.first.match(/\A-([\A\s]+)/)}"
               if bookvalue.first.match(/\A-([\A\s]+)/)
                 concat_value = bookvalue.join(" ")
                 nots = concat_value.scan(/-([\A\s]+)/).flatten
                 r.first << "((NOT #{attr} in (?))   OR (#{attr} is null OR #{attr} = '')  )"
                 r.last << nots
               else 
                r.first << "(#{attr} in (?))"
                r.last << bookvalue
               end
               
               Delayed::Worker.logger.debug "~~bool_value #{bookvalue}"
                
               
            end
             if value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length > 0)
                bookvalue = value.reject{ |k| (k == "" || k == "0" || k == "-999") }.map(&:strip)
                if bookvalue.first.match(/\A-([\A\s]+)/)
                  concat_value = bookvalue.join(" ")
                  nots = concat_value.scan(/-([\A\s]+)/).flatten
                  
                  r.first << "((NOT #{attr} in (?)) OR (NOT #{attr} is null) ) "
                  r.last << nots
                else
                  r.first << "(#{attr} in (?)  OR #{attr} is null )"
                  r.last << bookvalue
                end
              # if (i > 0)
                # if value.first == "0"
                  # reps << "(mls_area_key is null)"
                # else
                  # reps << "(NOT mls_area_key is null)"
                # end
                  # end
            elsif value.select{ |k| k == "0" || k == "-999" }.length > 0 && (value.reject{ |k| (k == "" || k == "0" || k == "-999") }.length <= 0)
               found = value.select{ |k| k == "0" || k == "-999" }
               if found.first == "0"
                  r.first << "(#{attr} is null ) "
               else
                  r.first << "(not #{attr} is null ) "
               end
            end  
          else
             value = value.split(',') unless value.is_a?(Array)
             r.first << "#{attr} IN (?)"
             r.last << value.reject(&:missing?).map(&:strip)
          
          end   
          
        elsif key.in?(TEXT)
          nots = value.scan(/-([^-\s]+)/).flatten
          Delayed::Worker.logger.debug "~~nots #{nots}"
          for n in nots
            r.first << "#{attr} NOT LIKE ?"
            r.last << "%#{n}%"
            value = value.gsub("-#{n}", '')
          end
          unless value.blank?
            r.first << "#{attr} LIKE ?"
            r.last << "%#{value.strip}%"
          end
        elsif key.in?(BOOLEAN) && value.in?('0', '1')
          if key.in?(["is_back_on_market"]) 
            if value.in?(['1']) 
              
              other_options << "#{attr} = #{value}"
              # r.first << "#{attr} = ?"
              # r.last << value
            end
          elsif key.in?(["price_reduction"]) 
            if value.in?(['1']) 
              
              other_options << "#{attr} = #{value}"
              # r.first << "#{attr} = ?"
              # r.last << value
            end
          else
            r.first << "#{attr} = ?"
            r.last << value
          end
          
        elsif key.in?(MINMAX)
          as = Property.attr_config(key, :as).try(:to_sym)
          unless value.first.missing?
            r.first << "#{attr} >= ?"
            
            r.last << case as
              when :percent then
                (value.first.to_d / 100.to_d rescue nil)
              when :date then
                puts "~~~~~~ value= #{value}"
                puts "value first class 2 #{Date.strptime(value.first,"%m/%d/%Y").strftime("%Y-%m-%d")}"
                # puts "value first class #{Date.strptime(value.first, "%m/%d/%Y").strftime(Date::DATE_FORMATS["%Y-%m-%d"])}" 
                Date.strptime(value.first,"%m/%d/%Y").strftime("%Y-%m-%d")  #.strftime(Date::DATE_FORMATS["%Y-%m-%d"])
              else
                value.first
            end
              
            # r.last << (as == :percent ? (value.first.to_d / 100.to_d rescue nil) : value.first)
          end
          unless value.last.missing?
            r.first << "#{attr} <= ?"
            r.last << case as
              when :percent then
                (value.last.to_d / 100.to_d rescue nil)
              when :date then
                puts "~~~~~~ value= #{value}"
                Date.strptime(value.last,"%m/%d/%Y").strftime("%Y-%m-%d")
                # value.last  #.strftime(Date::DATE_FORMATS["%Y-%m-%d"])
              else
                value.last
            end
            # r.last << (as == :percent ? (value.last.to_d / 100.to_d rescue nil) : value.last)
          end
        end
      r
    end
    rep_string = nil
    other_string = nil
    if reps.length > 0
      rep_string = reps.join(" OR ")
      
      final_string =  "#{result.first.join(' AND ')}"
      if !final_string.blank?
        final_string =  "#{final_string} AND (#{rep_string})" 
      else
        final_string =  "#{rep_string}" 
      end
      
      
      
    else
      final_string =  "#{result.first.join(' AND ')}"
    end
    
    if other_options.length > 0
      other_string = other_options.join(" OR ")
      final_string = [ "#{final_string}", "(#{other_string})"].reject(&:blank?).join(" AND ")      
    end
    
    rsl = result.last.unshift(final_string)
    # rsl = [final_string] if rsl.first.blank?
    Delayed::Worker.logger.debug "~~rsl #{rsl}"
    rsl
  end

  def attributes
    super.select do |attr, value|
      Delayed::Worker.logger.debug "~~attr #{attr}"
      Delayed::Worker.logger.debug "~~attr2 attr.in?(MINMAX) #{attr.in?(MINMAX)}"
      Delayed::Worker.logger.debug "~~value #{value}"
      !(attr.in?(MINMAX) && value.reject(&:missing?).empty?)
    end
  end

  def id=(value)
    @id = value.is_a?(Array) ? value.join(',') : value
  end

  def to_s
    Delayed::Worker.logger.debug "!! sending condition"
    Property.send(:sanitize_sql_for_conditions, query)
    # Property.send(:interpolate_sql,query)
  end
  
  
end