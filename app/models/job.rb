class Job < ActiveRecord::Base 
  include Delayed::Backend::Base

  self.per_page = 20

  attr_config do |c|
    # c._delegate JobDetail, :detail
    # delayed_jobs default columns
    c.priority integer: 0                     # Allows some jobs to jump to the front of the queue
    c.attempts integer: 0                     # Provides for retries, but still fail eventually.
    c.handler :text                           # YAML-encoded string of the object that will do work
    c.last_error :text                        # reason for last failure (See Note below)
    c.run_at :datetime                        # When to run. Could be Time.zone.now for immediately, or sometime in the future.
    c.locked_at :datetime                     # Set when a client is working on this object
    c.failed_at :datetime                     # Set when all retries have failed (actually, by default, the record is deleted instead)
    c.locked_by :string                       # Who is working on this object (if locked)
    c.queue :string                           # The name of the queue this job is in
    # custom columns
    c.started_at :datetime
    c.completed_at :datetime
    c.results_count :integer
    c.errors_count :integer
    c.status :enum, in: %w{error success pending running}
    c.name :string
    c.info :serialize
  end

  belongs_to :market
  belongs_to :user

  before_save :set_default_run_at

  # Delayed::Backend::ActiveRecord::Job overrides

  def handler_hash
    YAML.load(self.handler)
  end

  def running?
    self.status == 'running'
  end

  def destroy
    # do not destroy
  end

  def self.ready_to_run(worker_name, max_run_time)
    where('completed_at IS NULL && (run_at <= ? AND (locked_at IS NULL OR locked_at < ?) OR locked_by = ?) AND failed_at IS NULL', db_time_now, db_time_now - max_run_time, worker_name)
  end
  
  def invoke_job
    Delayed::Worker.lifecycle.run_callbacks(:invoke_job, self) do
      begin
        hook :before
        update_attributes!(status: 'running', started_at: Job.db_time_now)
        payload_object.perform
        Delayed::Worker.logger.debug "~~job name:#{name}"
        hook :success
        
        if name != 'MarketComputeJob'
          update_attributes!(status: 'success', completed_at: Job.db_time_now)
        else
          update_attributes!(status: 'running', completed_at: Job.db_time_now, results_count: nil, errors_count: nil)
        end
        
      rescue Exception => e
        hook :error, e
        update_attributes!(status: 'error', completed_at: Job.db_time_now)
        raise e
      ensure
        hook :after
        save!
      end
    end
  end

  # Delayed::Backend::ActiveRecord::Job implementation
  def self.by_priority
    order('priority ASC, run_at ASC')
  end

  def self.before_fork
    ::ActiveRecord::Base.clear_all_connections!
  end

  def self.after_fork
    ::ActiveRecord::Base.establish_connection
  end

  # When a worker is exiting, make sure we don't have any locked jobs.
  def self.clear_locks!(worker_name)
    update_all("locked_by = null, locked_at = null", ["locked_by = ?", worker_name])
  end

  # Find a few candidate jobs to run (in case some immediately get locked by others).
  def self.find_available(worker_name, limit = 5, max_run_time = Delayed::Worker.max_run_time)
    scope = self.ready_to_run(worker_name, max_run_time)
    scope = scope.scoped(:conditions => ['priority >= ?', Delayed::Worker.min_priority]) if Delayed::Worker.min_priority
    scope = scope.scoped(:conditions => ['priority <= ?', Delayed::Worker.max_priority]) if Delayed::Worker.max_priority
    scope = scope.scoped(:conditions => ["queue IN (?)", Delayed::Worker.queues]) if Delayed::Worker.queues.any?

    silence do
      scope.by_priority.all(:limit => limit)
    end
    # nil
  end

  # Lock this job for this worker.
  # Returns true if we have the lock, false otherwise.
  def lock_exclusively!(max_run_time, worker)
    now = self.class.db_time_now
    affected_rows = if locked_by != worker
      # We don't own this job so we will update the locked_by name and the locked_at
      self.class.update_all(["locked_at = ?, locked_by = ?", now, worker], ["id = ? and (locked_at is null or locked_at < ?) and (run_at <= ?)", id, (now - max_run_time.to_i), now])
    else
      # We already own this job, this may happen if the job queue crashes.
      # Simply resume and update the locked_at
      self.class.update_all(["locked_at = ?", now], ["id = ? and locked_by = ?", id, worker])
    end
    if affected_rows == 1
      self.locked_at = now
      self.locked_by = worker
      self.changed_attributes.clear
      return true
    else
      return false
    end
  end

  # Get the current time (GMT or local depending on DB)
  # Note: This does not ping the DB to get the time, so all your clients
  # must have syncronized clocks.
  def self.db_time_now
    if Time.zone
      Time.zone.now
    elsif ::ActiveRecord::Base.default_timezone == :utc
      Time.now.utc
    else
      Time.now
    end
  end

  def reload(*args)
    reset
    super
  end
end