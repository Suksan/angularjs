class Search < ActiveRecord::Base
  
  attr_config do |c|
    c.config! :serialize, with: SearchConfig
    c.name! :string, uniqueness: { scope: [:market_id, :user_id] }
    c.sort_by! enum: 'data_updated_at', in: Property.active_attributes(delegates: [Proforma], sort: true, symbols: false) 
    c.sort_ascending boolean: false
    c.email_start_on :date
    c.email_frequency :enum, in: %w{daily weekly monthly}
    c.email_public boolean: false
    c.email_last_on :date
    c.email_most_recent boolean: false
  end

  belongs_to :market
  belongs_to :user

  validates :market_id, :user_id, presence: true
  validates :email_frequency, presence: true, unless: 'email_start_on.blank?'
  validates :email_start_on, presence: true, unless: 'email_frequency.blank? && email_public.blank? && email_most_recent.blank?'
  validates :name, presence: true #, unless: 'email_start_on.blank?'
  validate :consistency

  def results(email=false,user_id=nil)
    since = email_last_on if email && self.email_most_recent
    logger.debug "self config #{self.config.attributes}"
    self.market.properties.search_for_user(self.config, since,0)  # .order_by("properties.id DESC") #.order_by(self.sort_by, self.sort_ascending)
    
    
  end
  
  def results_for_user(email=false,user_id=nil)
    since = email_last_on if email && self.email_most_recent
    logger.debug "self config #{self.config.attributes}"
    self.market.properties.search_for_user(self.config, since,user_id)  # .order_by("properties.id DESC") #.order_by(self.sort_by, self.sort_ascending)
    
    
  end
  
  def results_for_client_user(email=false,user_id=nil)
    since = email_last_on if email && self.email_most_recent
    logger.debug "self config #{self.config.attributes}"
    self.market.properties.search_for_client_user(self.config, since,user_id)  # .order_by("properties.id DESC") #.order_by(self.sort_by, self.sort_ascending)
    
    
  end

  def email_on?(date)
    if self.email_start_on && self.email_start_on <= date
      return case self.email_frequency
        when 'daily' then true
        when 'weekly' then date.cwday == self.email_start_on.cwday
        when 'monthly' then date.day == self.email_start_on.day
        else false
      end
    end
    false
  end

  def to_param
    self.id == 0 ? 'go' : self.id
  end

  private

    def consistency
      errors.add(:name, :empty_config) if self.config.attributes.empty?
    end

end