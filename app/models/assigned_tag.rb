class AssignedTag < ActiveRecord::Base
  attr_accessible :property_id, :tag, :user_id
  
  belongs_to :property
  belongs_to :property_tag, class_name: 'PropertyTag', foreign_key: :tag
  
  attr_config do |c|
    c.property_id :integer
    c.user_id :integer
    c.tag :tag
  end


end
