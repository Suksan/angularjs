class PropertyDetail < ActiveRecord::Base
  has_one :property, :foreign_key => :detail_id
  
  
  attr_config do |c|
    # store original raw data
    c._raw :serialize
    # raw fields
    c.access_lock_box :string
    c.access_to_complex :string
    c.acreage :decimal
    c.annual_maintenance_fee :money
    c.appointment_phone_description :text
    c.bathrooms_full :integer
    c.bathrooms_half :integer
    c.builder_name :string
    c.building_name :string
    c.building_stories :decimal
    c.buyer_agent_bonus :string
    c.buyer_agent_compensation :string
    c.completion_date :date
    c.cooling_system_type :string
    c.coutertop_type :string
    c.defects :string
    
    c.estimated_close_date :date
    c.exterior_type :string
    c.financing_available :string
    c.floor_type :string
    c.foundation_type :string
    c.heating_system :string
    
    c.hirise_fees :money
    c.interior_description :text
    c.land_size :string
    c.legal_description :string
    c.level_of_unit :string
    c.listing_agent_fax :phone
    c.listing_agent_id :string
    c.listing_broker_id :string
    c.listing_broker_name :string
    c.loan_amortized_years :integer
    c.loan_amount_interest_rate :percent
    c.loan_amount_new :money
    c.loan_term :string
    c.loss_mitigation :boolean
    c.lot :string
    c.lot_back_dimensions :string
    c.lot_description :string
    c.lot_front_dimension :string
    c.lot_improvements :string
    c.lot_left_dimension :string
    c.lot_number :string
    c.lot_right_dimension :string
    c.lot_use :string, in: %w{condo other mobile_home multi_family patio_home resort_vacation single_family townhouse}
    c.maintenance_fee :money
    
    c.mineral_rights :string
    c.multi_owner_operating :money
    c.muti_gross_income :money
    c.muti_gross_operating_income :money
    c.mutli_income_net_operation :money
    c.neighborhood_number :string
    c.new_construction :boolean
    c.new_construction_description :text
    c.night_phone :phone
    c.number_of_photos :integer
    c.number_of_units :integer
    c.occupancy_rate :string
    c.office_phone :phone
    c.original_list_price :money
    c.other_building_fees :money
    c.other_fees :money
    c.pool_area :boolean
    c.public_remarks :text
    c.rental_sale_mls_number :string
    c.rental_terms :string
    c.road_surface :string
    c.roof_type :string
    
    c.seller_paid_to_buyer_closing_costs :money
    c.seller_repair_credits :money
    c.showing_instructions :text
    c.siding_type :text ###
    c.stories :decimal  ###
    c.street_surface_type :text
    c.style_of_building :text ###
    c.unit_number :string
    
    c.selling_agent_id  :integer
    
    c.equipment :text
    c.exterior_description :text
    c.fence_type :string
    c.fenced_yard :string
    c.fireplace_number :integer
    c.hoa_membership  :string
    c.mud_district  :string
    c.millage_rate :decimal
    c.range_number :string
    c.section_number :string
    
  end
  
  def elementary_school_full
    rating = "(" + self.elementary_school_rating.to_s + ")" if self.elementary_school_rating
    [self.elementary_school,rating].join(" ")
  end
  
  def middle_school_full
    rating = "(" + self.middle_school_rating.to_s + ")" if self.middle_school_rating
    [self.middle_school,rating].join(" ")
  end
  
  def high_school_full
    rating = "(" + self.high_school_rating.to_s + ")" if self.high_school_rating
    [self.high_school,rating].join(" ")
  end
  
  def updateSchoolInfo
    if (!self.school_updated_at) || (self.school_updated_at < 2.months.ago)
      zillow = Zillow.new
      attrs = zillow.lookup(self.property.address)  
      self.update_attributes(attrs)
    end
  end
  
  
    
end