class DeletedCronJob < ActiveRecord::Base
  attr_config do |c|
    c.name :enum, in: %w{Mon Tue Wed Thu Fri Sat Sun}
    c.start_hour :hour
    c.start_min :minute
    c.duration_hour :hour_twenty_four
    c.duration_min :minute
    c.mer_indicator :meridian_indicator
    c.frequency_type :string, calc: proc {|m| "every_day_of_week" }
    c.day_of_week :string, calc: proc {|m| m.name } 
  end
  
  validates_presence_of :name,:start_hour,:start_min
  validates :start_hour, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  validates :start_min, numericality: { greater_than_or_equal_to: 0, only_integer: true }
  
  belongs_to :market
  before_save :calculate
  before_destroy :calculate
   
  #Setting.for(:display).set=(value)
  def self.for(name)
    MarketCronJob.where(["name = ?", name]).first
  end
   
  def calculate
    self.send(:calculate_fields) 
  end
  
  def update_cron
    system "bundle exec whenever --update-crontab greenlight --set environment=#{ENV['RAILS_ENV']}  --user greenlight "
  end
  
end
