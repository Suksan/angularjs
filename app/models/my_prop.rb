class MyProp < ActiveRecord::Base
  attr_accessible :property_id, :user_id
  belongs_to :property, :foreign_key => :property_id
  belongs_to :user, :foreign_key => :user_id
  
  attr_config do |c|
    c.property_id :integer
    c.user_id :integer
  end
  
  def self.my_prop_exists( property_id, user_id)
    true if select("id, property_id, user_id").where(["property_id = ? and  user_id = ?", property_id, user_id] ).count > 0
  end
  
end
