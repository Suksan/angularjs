class MarketAssignment < ActiveRecord::Base
  attr_protected

  belongs_to :user
  belongs_to :market

  has_one :definition, :class_name => 'MarketDefinition', :as => :source, :dependent => :destroy
  has_many :renovation_rehabs, :class_name => 'MarketRenovationRehab', :as => :source, :dependent => :destroy
  has_many :client_renovation_rehabs, :class_name => 'ClientMarketRenovationRehab', :as => :source, :dependent => :destroy
  has_many :renovation_timelines, :class_name => 'MarketRenovationTimeline', :as => :source, :dependent => :destroy
  has_many :build_costs, :class_name => 'MarketBuildCost', :as => :source, :dependent => :destroy

  before_create :restore, :if => 'definition.nil?'

  accepts_nested_attributes_for :definition
  accepts_nested_attributes_for :renovation_rehabs, :renovation_timelines, :build_costs, :allow_destroy => true, :reject_if => :all_blank

  #after_create :notify_user #TODO RENABLE ?

  def restore!
    restore
    save(:validate => false)
  end

  after_initialize do
    restore if self.new_record?
  end

  private

    def restore
      if market.present?
        self.build_definition(market.definition.attributes)
        self.renovation_rehabs = []
        self.renovation_rehabs.build(market.renovation_rehabs.map(&:attributes))
        self.renovation_timelines = []
        self.renovation_timelines.build(market.renovation_timelines.map(&:attributes))
        self.build_costs = []
        self.build_costs.build(market.build_costs.map(&:attributes))
      end
    end

    def notify_user
      Mailer.delay.market_assignment(self)
    end

end