class Note < ActiveRecord::Base
  attr_accessible :content, :posted_by, :property_id
  
  belongs_to :property
  belongs_to :user, :foreign_key => :posted_by
  
  validates_presence_of :content
  
  attr_config do |c|
    
    c.property_id :integer
    c.content :string
    c.posted_by :integer
    
  end
      
end
