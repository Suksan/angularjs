class PropertyTag < ActiveRecord::Base
  attr_accessible :market_id, :name
  default_scope { order(:seq) }
  belongs_to :market
  
  attr_config do |c|
    c.name :string
    c.market_id :integer
    c.seq :integer
  end
end
