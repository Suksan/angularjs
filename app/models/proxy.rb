class Proxy < ActiveRecord::Base
  
  attr_config do |c|
  
    c.ip :string 
    c.port :integer 
    c.login :string 
    c.password :password 
    c.daily_limit :integer 
    c.daily_calls :integer 
    c.last_date :date
    c.too_many_attempts boolean: false
    c.error_count :integer
  end

  def self.find_available
    Proxy.where([" ((daily_limit IS NULL) OR ((last_date = ?) && (daily_limit > daily_calls) && (error_count < ?)) OR (((last_date IS NULL) OR (last_date < ?)) && (error_count < ?)))", Date.today,16,Date.today,16]).first
  end
    
  def increment_calls(by = 1)
    return if self.daily_limit.nil?

    current_service = self.class.find(self.id)
    dcall = current_service.daily_calls ? current_service.daily_calls  : 0
    dlast_date = current_service.last_date
    
    if dlast_date == Date.today
      self.daily_calls = dcall + by
    else
      self.last_date = Date.today
      self.daily_calls = by
    end
    save(validate: false)
  end
  
  def mark_error
    self.too_many_attempts = true
    save(validate: false)
  end
 
  def add_error_count
    self.error_count += 1 
    save(validate: false)
  end
  
  def rem_error_count
    self.error_count = 0
    save(validate: false)
  end
 
end