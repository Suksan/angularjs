class Neighborhood < ActiveRecord::Base

  attr_config do |c|
    c.market_id :integer
    c.address :string
    c.subdivision :string
    c.legal_subdivision :string
    c.mls_area_name :string
    c.latitude :decimal
    c.longitude :decimal
    c.ns_neigborhood :string
    c.ns_neigborhood_percent :string
    c.ns_comp1 :string
    c.ns_comp1_percent :string
    c.ns_comp2 :string
    c.ns_comp2_percent :string
    c.ns_comp3 :string
    c.ns_comp3_percent :string
    c.ns_comp4 :string
    c.ns_comp4_percent :string
    c.ns_comp5 :string
    c.ns_comp5_percent :string
    c.ns_comp6 :string
    c.ns_comp6_percent :string
    c.ns_comp7  :string
    c.ns_comp7_percent  :string
    c.ns_comp8  :string
    c.ns_comp8_percent :string
    c.ns_comp9 :string
    c.ns_comp9_percent :string
    c.census_state :string
    c.census_county :string
    c.census_tract  :string
    c.census_block :string
    c.census_geoid :string
    c.glt_neighborhood :string
    c.polygon_id :string
    c.html_source :text
  end

end