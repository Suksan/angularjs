class ClientMarketRenovationRehab < ActiveRecord::Base
  
  attr_config do |c|
    c.condition_grade :string
    c.min :integer
    c.max :integer
    c.sale! :money
    c.rental! :money
  end

  validates :min, :max, :uniqueness => { :scope => [:source_type, :source_id] }
  validate :consistency

  belongs_to :source, :polymorphic => true

  def include?(year,cg)
    (self.min.nil? || (self.min <= year)) && (self.max.nil? || (year < self.max))  if (self.condition_grade.to_s.downcase == cg.to_s.downcase)
  end

  private

    def consistency
      if min.nil? && max.nil?
        [:min, :max].each {|attr| errors.add(attr, :is_blank) }
      elsif min && max && min > max
        [:min, :max].each {|attr| errors.add(attr, :invalid) }
      end
    end

end