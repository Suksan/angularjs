class RestartCronJob < ActiveRecord::Base
  attr_config do |c|
    c.name :string
  end

  after_save :send_cron_later
  before_destroy :send_cron_later

  def send_cron_later
    @notifier = DeletedRestartCronJob.new(self.clone.attributes)
    @notifier.save
    @notifier.delay.update_cron 
  end  

  def update_cron
    system "bundle exec whenever --clear-crontab greenlight  --user greenlight "
    system "bundle exec whenever --update-crontab greenlight --set environment=#{ENV['RAILS_ENV']}  --user greenlight "
  end

end