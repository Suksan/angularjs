class Market < ActiveRecord::Base

  attr_config do |c|
    c.name! :string, uniqueness: true
    c.state! :state
    c.code :string
    c.mls_code! :string, format: /\A[a-z_]+\z/, uniqueness: true
    c.mls_login! :string
    c.mls_password! :password
    c.level2 :file, content_type: 'application/vnd.ms-excel'
    c.level3 :file, content_type: 'application/vnd.ms-excel'
     c.rental_comp_query query: '(square_feet BETWEEN #{square_feet}*0.8 AND #{square_feet}*1.2)
AND (year_built BETWEEN #{year_built}-12 AND #{year_built}+12)
AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms_full}+1)
AND (category = "rental")
AND ((status <> "sold" OR (closed_date IS NULL OR closed_date >= CURDATE() - INTERVAL 1 YEAR)) 
AND ((legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision}) 
OR 
(legal_subdivision IS NULL AND subdivision IS NOT NULL AND subdivision = #{subdivision})  
OR
(legal_subdivision IS NULL AND subdivision IS NULL AND key_map_page IS NOT NULL AND key_map_page = #{key_map_page}))
AND ((zip_code IS NOT NULL AND zip_code = #{zip_code}) OR (zip_code IS NULL AND city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})))'
    c.sale_comp_query query: '(square_feet BETWEEN #{square_feet}*0.85 AND #{square_feet}*1.15)
AND (year_built BETWEEN #{year_built}-10 AND #{year_built}+10)
AND (beds BETWEEN #{beds}-1 AND #{beds}+1)
AND (bathrooms BETWEEN #{bathrooms}-1 AND #{bathrooms_full}+1)
AND (category = "single_family")
AND (#{lot_square_feet} IS NULL OR lot_square_feet BETWEEN #{lot_square_feet}-10890 AND #{lot_square_feet}+10890)
AND ((status <> "sold" OR (closed_date IS NULL OR closed_date >= CURDATE() - INTERVAL 1 YEAR)) 
AND ((legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision}) 
OR 
(legal_subdivision IS NULL AND subdivision IS NOT NULL AND subdivision = #{subdivision})  
OR
(legal_subdivision IS NULL AND subdivision IS NULL AND key_map_page IS NOT NULL AND key_map_page = #{key_map_page}))
AND ((zip_code IS NOT NULL AND zip_code = #{zip_code}) OR (zip_code IS NULL AND city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})))'
   
   
    c.build_comp_query query: '(year_built >= Year(CURDATE())-3)
AND (county LIKE #{county})
AND (category = "single_family")
AND (#{lot_square_feet} IS NULL OR lot_square_feet BETWEEN #{lot_square_feet}-10890 AND #{lot_square_feet}+10890)
AND ((status <> "sold" OR (closed_date IS NULL OR closed_date >= CURDATE() - INTERVAL 1 YEAR)) 
AND ((legal_subdivision IS NOT NULL AND legal_subdivision = #{legal_subdivision}) 
OR 
(legal_subdivision IS NULL AND subdivision IS NOT NULL AND subdivision = #{subdivision})  
OR
(legal_subdivision IS NULL AND subdivision IS NULL AND key_map_page IS NOT NULL AND key_map_page = #{key_map_page}))
AND ((zip_code IS NOT NULL AND zip_code = #{zip_code}) OR (zip_code IS NULL AND city IS NOT NULL AND county IS NOT NULL AND city LIKE #{city} AND county LIKE #{county})))' 
    c.search_stats :serialize
  end

  validate :check_mls_code_provider, :unless => "mls_code.blank?"
  validate :check_excel_templates
  validate :check_comp_queries
  # validate :check_comp

  has_many :assignments, :class_name => 'MarketAssignment', :dependent => :destroy
  has_many :users, :through => :assignments
  has_many :properties, dependent: :destroy
  has_many :searches, dependent: :destroy
  has_many :jobs, dependent: :destroy
  has_many :uploads, class_name: 'Job', conditions: {name: 'market_upload_job'}
  has_many :proformas, through: :properties
 
  has_many :renovation_rehabs, :class_name => 'MarketRenovationRehab', :as => :source, :dependent => :destroy
  has_many :client_renovation_rehabs, :class_name => 'ClientMarketRenovationRehab', :as => :source, :dependent => :destroy
  has_many :renovation_timelines, :class_name => 'MarketRenovationTimeline', :as => :source, :dependent => :destroy
  has_many :build_costs, :class_name => 'MarketBuildCost', :as => :source, :dependent => :destroy
  has_many :cron_jobs, :class_name => 'MarketCronJob', :dependent => :destroy
  has_many :restart_cron_jobs, :class_name => 'RestartCronJob', :dependent => :destroy
  has_one :definition, :class_name => 'MarketDefinition', :as => :source, :dependent => :destroy
  has_many :property_tags, :class_name => 'PropertyTag', :foreign_key => :market_id

  accepts_nested_attributes_for :renovation_rehabs, :renovation_timelines, :build_costs, :cron_jobs, :property_tags, :allow_destroy => true, :reject_if => :all_blank
  accepts_nested_attributes_for :definition

  before_save :set_code

  DEFAULT_RENOVATIONS_REHABS = [
    {            max: 1940, sale: '25', rental: '20' },
    { min: 1940, max: 1950, sale: '17', rental: '12' },
    { min: 1950, max: 1966, sale: '15', rental: '10' },
    { min: 1966, max: 1980, sale: '12', rental: '8' },
    { min: 1980, max: 1996, sale: '10', rental: '6' },
    { min: 1996, max: 2003, sale: '8',  rental: '4' },
    { min: 2003,            sale: '5',  rental: '2' },
  ]

  DEFAULT_RENOVATIONS_TIMELINES = [
    {             max: 10000,  weeks: 2 },
    { min: 10000, max: 20000,  weeks: 3 },
    { min: 20000, max: 30000,  weeks: 4 },
    { min: 30000, max: 40000,  weeks: 5 },
    { min: 40000, max: 50000,  weeks: 6 },
    { min: 50000, max: 60000,  weeks: 7 },
    { min: 60000, max: 70000,  weeks: 8 },
    { min: 70000, max: 80000,  weeks: 9 },
    { min: 80000, max: 90000,  weeks: 10 },
    { min: 90000, max: 100000, weeks: 11 },
    { min: 100000,             weeks: 12 },
  ]

  DEFAULT_BUILD_COSTS = [
    {            max: 1000,  weeks: 20, cost: '70' },
    { min: 1000, max: 2000,  weeks: 30, cost: '70' },
    { min: 2000, max: 3000,  weeks: 40, cost: '85' },
    { min: 3000, max: 4000,  weeks: 50, cost: '95' },
    { min: 4000, max: 5000,  weeks: 60, cost: '110' },
    { min: 5000, max: 6000,  weeks: 70, cost: '110' },
    { min: 6000, max: 7000,  weeks: 80, cost: '115' },
    { min: 7000, max: 8000,  weeks: 90, cost: '120' },
    { min: 8000, max: 9000,  weeks: 100, cost: '125' },
    { min: 9000, max: 10000, weeks: 110, cost: '130' },
    { min: 100000,           weeks: 120, cost: '140' },
  ]
  
  DEFAULT_CRON_JOBS = [
    { name: 'Tue', frequency_type: 'every_day_of_week', day_of_week: 'Tue', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
    { name: 'Wed', frequency_type: 'every_day_of_week', day_of_week: 'Wed', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0},
    { name: 'Thu', frequency_type: 'every_day_of_week', day_of_week: 'Thu', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
    { name: 'Fri', frequency_type: 'every_day_of_week', day_of_week: 'Fri', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
    { name: 'Sat', frequency_type: 'every_day_of_week', day_of_week: 'Sat', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
    { name: 'Sat', frequency_type: 'every_day_of_week', day_of_week: 'Sat', start_hour: 5, start_min: 0, mer_indicator: 'pm',duration_hour: 17, duration_min: 0 },
    { name: 'Sun', frequency_type: 'every_day_of_week', day_of_week: 'Sun', start_hour: 5, start_min: 0, mer_indicator: 'pm',duration_hour: 14, duration_min: 0 }
  ]

  def mls_provider
    @mls_provider ||= mls_code.camelcase.constantize.new(self) if mls_code 
  end

  def fullname
    [self.name, self.state].join(', ')
  end

  after_initialize do
    if self.new_record?
      self.build_definition if definition.nil?
      self.renovation_rehabs.build(DEFAULT_RENOVATIONS_REHABS) if renovation_rehabs.empty?
      self.renovation_timelines.build(DEFAULT_RENOVATIONS_TIMELINES) if renovation_timelines.empty?
      self.build_costs.build(DEFAULT_BUILD_COSTS) if build_costs.empty?
      self.cron_jobs.build(DEFAULT_CRON_JOBS) if cron_jobs.empty?
    end
  end
  
  def assumptions_changed?
    puts "rental_comp_query: #{self.rental_comp_query}"
    puts "rental_comp_query changed?: #{self.rental_comp_query_changed?}"
    
   
    
    
    if self.rental_comp_query_changed? || self.sale_comp_query_changed? ||  self.build_comp_query_changed?    
      logger.debug "!!!!!!! assumption changed: query change" 
      return true
    end
    # self.market_renovation_rehabs.each
    self.renovation_rehabs.each do |r|
      ['min','max','sale','rental'].each do |c|
        if r.send("#{c}_changed?")
          logger.debug "!!!!!!! assumption changed: #{c} rev rehab change"
          return true 
        end
      end
    end
    
    self.renovation_timelines.each do |r|
      ['min','max','weeks'].each do |c|
        if r.send("#{c}_changed?")
          logger.debug "!!!!!!! assumption changed: #{c}  rev timeliness change"
          return true 
        end
      end
    end
    
    self.build_costs.each do |r|
      ['min','max','weeks','cost'].each do |c|
         if r.send("#{c}_changed?")
           logger.debug "!!!!!!! assumption changed: #{c} build cost change"
           return true 
         end
      end
    end
    
    %w{purchase_price_discount purchase_commission_earned purchase_bank_short_discount purchase_homeowner_short_discount
      rental_loan rental_annual_rent_increase rental_annual_vacancy_rate rental_annual_maintenance_expense rental_management_expense rental_annual_expense_increase rental_years_to_sale
      seller_finance_loan seller_finance_sale_time_discount seller_finance_down_payment seller_finance_interest_rate seller_finance_term seller_finance_realtor_fee seller_finance_management_expense seller_finance_years_to_sale
      rental_resale_loan rental_resale_marketing_expense rental_resale_closing_expense rental_resale_warranty_expense rental_resale_investor_cap_rate
      sale_loan sale_realtor_fee_closing_costs sale_seller_contribution
      build_loan build_realtor_fee_closing_costs build_seller_contribution
      listing_prep_days listing_sale_comp_discount listing_commission_income listing_marketing_expense listing_agent_expense
      listing_partnership_loan listing_partnership_commission_income listing_partnership_construction_markup listing_partnership_profits_percent listing_partnership_marketing_legal_expense listing_partnership_seller_contribution
      assignment_sale_time_discount assignment_investor_arv_sale_ltv assignment_marketing_expense assignment_closing_expense
      lending_bridge_purchase_down_payment lending_bridge_renovation_down_payment lending_bridge_max_ltv lending_bridge_interest_rate lending_bridge_points lending_bridge_term lending_bridge_closing_costs lending_bridge_interest_only lending_bridge_amortization lending_short_down_payment lending_short_max_ltv lending_short_interest_rate lending_short_points lending_short_term lending_short_closing_costs lending_short_interest_only lending_short_amortization lending_short2_down_payment lending_short2_max_ltv lending_short2_interest_rate lending_short2_points lending_short2_term lending_short2_closing_costs lending_short2_interest_only lending_short2_amortization lending_long_down_payment lending_long_max_ltv lending_long_interest_rate lending_long_points lending_long_term lending_long_closing_costs lending_long_interest_only lending_long_amortization 
      general_company_marginal_tax_rate general_investor_marginal_tax_rate general_capital_gains_rate general_improvement_ratio general_years_depreciation general_annual_insurance_expense general_annual_utility_expense general_days_to_close general_appreciation_rate
      return_minimum_cap_rate return_minimum_roi return_minimum_return_on_total_project return_rental_comp_level return_rental_sale_comp_level return_seller_finance_comp_level return_rental_resale_comp_level return_sale_comp_level return_build_comp_level return_assignment_comp_level renovation_unspecified_sale_rehab renovation_unspecified_rental_rehab}.each do |c|
      old_value =  self.definition.send("#{c}_was")
        new_value =  self.definition.send("#{c}")
        old_new = (old_value == new_value)

      if (old_value != new_value)
       return true
      end

      #if self.definition.send("#{c}_changed?")
       # old_value =  self.definition.send("#{c}_was")
        #new_value =  self.definition.send("#{c}")
        #old_new = (old_value == new_value)
        #logger.debug "!!!!!!! old_value = new_value : #{old_new}"
        #logger.debug "!!!! old value was : #{old_value} new value is: #{new_value} "
        #logger.debug "!!!!!!! assumption changed: #{c}  definition change"
        #return true 
      #end
    end
    
    return false
    # %w{purchase_price_discount purchase_commission_earned purchase_bank_short_discount purchase_homeowner_short_discount}
    
    
  end

  def pshark
    @pshark ||= PropertyShark.new
  end
    
  # Function that lookup a property according to given data attributes.
  # Executed while grabbing daily MLS data, importing MLS and other datasets via the web interface
  # Parameters: 
  # - attributes: data hash
  # - raw: raw data hash coming from original file (e.g. har) [optional]
  # - hotsheet: boolean indicating if data coming from hotsheet [not used yet]
  # (Question: does the data coming from hotsheet imply anything)
  def import(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      raw_list_price = nil
       Delayed::Worker.logger.debug "mls number exists"
      # look for property with mls number in market database 
      found = self.properties.find_by_mls_number(attrs[:mls_number])  
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      Delayed::Worker.logger.debug "## what is found #{found}  => #{attrs[:mls_number]}"
      
      if update_only
        
        if found # a property is found
          Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}" if found.list_price
          Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw && raw[:list_price]
          Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw && raw[:list_price]
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
          
          if found.status_changed?
            if found.status_was == "pending" && found.status == "active"
              found.is_back_on_market = true
              found.back_on_market = attrs[:mls_record_modified]
            else
              found.is_back_on_market = nil
              found.back_on_market = nil  
            end             
          end

          if found.list_price_changed?
            if found.list_price_was > found.list_price 
              found.price_reduction = true
              found.back_on_market = attrs[:mls_record_modified]
            else
              found.price_reduction = nil
              found.back_on_market = nil
            end
          end
          
        end
      else
        if found # a property is found
          Delayed::Worker.logger.debug "duplicate record is entering"
          # Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}"
          # Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw
          # Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw
          # raw_list_price = raw[:list_price].gsub(/[\$,]/, '').to_i
          # Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          # Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          # if raw 
            # # changed_values = {}
            # # changed_values = changed_values.with_indifferent_access
            # found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              # true
            # else
              # false
            # end
#            
          # end
          
        
        else  # no property found and not in hotsheet
          Delayed::Worker.logger.debug "creating new property record in market.import()"
          
          found = self.properties.new(attrs) # create new property record
          found.auto_updatable  = true
        end
      end 
    else # no mls number exists
      if ((attrs[:tax_id] && attrs[:zip_code]) || (attrs[:street_address] && attrs[:zip_code]) || 
        (attrs[:tax_id] && attrs[:county])  || 
        (attrs[:street_address] && attrs[:county] )) # a tax account id or both street_address+zip_code exists
        
        
        # look for properties with the same properties
        history = self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        history_desc_id =  self.properties.similar_desc_id(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        bathrooms = attrs[:bathrooms]
        attrs = attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
        # Delayed::Worker.logger.debug  "similar prop #{history}"
        # Delayed::Worker.logger.debug "similar prop  #{self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).order("id desc").all}"
        
        Delayed::Worker.logger.debug "ZzzZ similar prop #{history_desc_id}"
        if history.empty? # if no property found
          # create new property
          found = self.properties.new(attrs)
          
          
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
        elsif (!history_desc_id.first.status.blank? &&  history_desc_id.first.status.in?('off'))
          update_found =  history_desc_id.first
           # create new property
          # update_found = self.properties.new(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark update lookup"
          found = pshark.lookup(found)
          
          if found
          # Delayed::Worker.logger.debug "override attributes #{found["address"]} state: #{found["state"]}"
            temp_id = update_found.id
            # Delayed::Worker.logger.debug "update found object: #{update_found}"
            update_found.destroy
            found.id = temp_id
            found.detail.id = temp_id
            # found.attributes.each do |k,v|
              # Delayed::Worker.logger.debug "attr_name  #{k}"
              # Delayed::Worker.logger.debug "update found k #{k}  value #{update_found[k]}"
              # found.send(:"#{k}=",update_found[k]) unless (k.in?(%w{id created_at}) || update_found[k].nil?)
            # end
            Delayed::Worker.logger.debug "found address value:  #{found.address}"
            Delayed::Worker.logger.debug "found id value:  #{found.id}"
          
          end
          
          
          # found.override_attributes(update_found.to_h)
          
        elsif found = history.select{|p| p.mls_number.nil? }.first
          # Delayed::Worker.logger.debug "override no mls"
          # # a property with no mls_number found
          # # then override existing property
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        elsif history.first.status.in?('active', 'pending')
          # Delayed::Worker.logger.debug "override use first"
          # # if most recent history item is active or pending
          # # override existing item
          # found = history.first
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        else
          
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
          # Delayed::Worker.logger.debug "create new prop"
          # Delayed::Worker.logger.debug "#{history}"
          # # otherwise create new property record and use the appropriate property field data from history
          # history_attrs = history.first.active_attributes(delegate: PropertyDetail, calc: false, missing: false)
          # # filter out only attributes of interest
#           
          # # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
          #  --------------------------------- commented out ----------------------------------
          # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS.map { |fld| fld.to_sym  }).select {|k, v| !v.missing? }
          # history_extra_attrs = {}
#                    
          # %w{street_address address street_name street_number state}.each { |item| 
             # history_extra_attrs = history_extra_attrs.merge(item.to_sym => history.first[item.to_sym])
          # }
          # history_extra_attrs = history_extra_attrs.merge(:status => 'off')
          # found = self.properties.new(history_attrs.merge(attrs).merge(history_extra_attrs))
          #  --------------------------------- commented out ----------------------------------
          
          # merge history attrs with user attrs (superior) and create new record
          # found = self.properties.new(history_attrs.merge(attrs))
          
          
        end
      else
         Delayed::Worker.logger.debug "do nothing"
        # do nothing
      end
    end

    if found 
      
      Delayed::Worker.logger.debug "found true"
      # validate data quality on found property
      unless found.valid?
        # reset fields with errors
        Delayed::Worker.logger.debug "found error"
        found.errors.keys.each do |key| 
          Delayed::Worker.logger.debug "error key: #{key}"
          found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
        end
      end
      found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
      Delayed::Worker.logger.debug "found new record? #{found.new_record?}"
      Delayed::Worker.logger.debug "found id #{found.id}"
      found.data_source = source if found.new_record?
      found.status = 'off' if found.mls_number.missing?
      found.data_updated_at = DateTime.now
      found.save
      
      
      
      found
    end
  end
  
  def import2(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      # look for property with mls number in market database 
      founds = self.properties.where(["(mls_number = ?) and (status = 'sold') ", attrs[:mls_number]]) rescue nil; 
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
          
      if founds && (founds.length > 0)# a property is found
               
        for found in founds
          if !found.closed_date
            
            found.assign_attributes(closed_date: attrs[:closed_date])
            # validate data quality on found property
            unless found.valid?
              # reset fields with errors
              found.errors.keys.each do |key| 
                found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
              end
            end
            found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
            Delayed::Worker.logger.debug "updated id: #{found.id} closed date: #{attrs[:closed_date]}"
            found.data_source = source if found.new_record?
            found.status = 'off' if found.mls_number.missing?
            found.data_updated_at = DateTime.now
            found.save
          end
          
          
        end
        
        return founds
      # else
#           
        # found = self.properties.new(attrs) # create new property record
#         
        # # validate data quality on found property
        # unless found.valid?
          # found.errors.keys.each do |key| 
            # found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
          # end
        # end
        # found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
        # found.data_source = source if found.new_record?
        # found.status = 'off' if found.mls_number.missing?
        # Delayed::Worker.logger.debug "inserted id: #{found.id}"
        # found.data_updated_at = DateTime.now
        # found.save
      end     
    end
  end
  
  
  def import3(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    Delayed::Worker.logger.debug " state comparing self:  #{attrs[:state]} = #{self.state} " 
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      raw_list_price = nil
       Delayed::Worker.logger.debug "mls number exists"
      # look for property with mls number in market database 
      found = self.properties.find_by_mls_number(attrs[:mls_number])  
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      Delayed::Worker.logger.debug "## what is found #{found}  => #{attrs[:mls_number]}"
      
      if update_only
        
        if found # a property is found
          Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}" if found.list_price
          Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw && raw[:list_price]
          Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw && raw[:list_price]
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
        end
      else
        if found # a property is found
          
          
          Delayed::Worker.logger.debug "duplicate record is entering"
          # Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}"
          # Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw
          # Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          
          
          # Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          # Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          # if raw 
            # # changed_values = {}
            # # changed_values = changed_values.with_indifferent_access
            # found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              # true
            # else
              # false
            # end
#            
          # end
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
          if found.status_changed?
            if found.status_was == "pending" && found.status == "active"
              found.is_back_on_market = true
              found.back_on_market = attrs[:mls_record_modified]
            else
              found.is_back_on_market = nil
              found.back_on_market = nil  
            end            
          end

          if found.list_price_changed?
            
            if found.list_price_was > found.list_price 
              found.price_reduction = true
              found.back_on_market = attrs[:mls_record_modified]
            else
              found.price_reduction = nil
              found.back_on_market = nil
            end
          end
          
        
        else  # no property found and not in hotsheet
          Delayed::Worker.logger.debug "creating new property record in market.import()"
          
          found = self.properties.new(attrs) # create new property record
          found.auto_updatable  = true
        end
      end 
    else # no mls number exists
      if ((attrs[:tax_id] && attrs[:zip_code]) || (attrs[:street_address] && attrs[:zip_code]) || 
        (attrs[:tax_id] && attrs[:county])  || 
        (attrs[:street_address] && attrs[:county] )) # a tax account id or both street_address+zip_code exists
        
        
        # look for properties with the same properties
        history = self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        history_desc_id =  self.properties.similar_desc_id(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        bathrooms = attrs[:bathrooms]
        attrs = attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
        # Delayed::Worker.logger.debug  "similar prop #{history}"
        # Delayed::Worker.logger.debug "similar prop  #{self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).order("id desc").all}"
        
        Delayed::Worker.logger.debug "ZzzZ similar prop #{history_desc_id}"
        if history.empty? # if no property found
          # create new property
          found = self.properties.new(attrs)
          
          
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
        elsif (!history_desc_id.first.status.blank? &&  history_desc_id.first.status.in?('off'))
          update_found =  history_desc_id.first
           # create new property
          # update_found = self.properties.new(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark update lookup"
          found = pshark.lookup(found)
          
          if found
          # Delayed::Worker.logger.debug "override attributes #{found["address"]} state: #{found["state"]}"
            temp_id = update_found.id
            # Delayed::Worker.logger.debug "update found object: #{update_found}"
            update_found.destroy
            found.id = temp_id
            found.detail.id = temp_id
            # found.attributes.each do |k,v|
              # Delayed::Worker.logger.debug "attr_name  #{k}"
              # Delayed::Worker.logger.debug "update found k #{k}  value #{update_found[k]}"
              # found.send(:"#{k}=",update_found[k]) unless (k.in?(%w{id created_at}) || update_found[k].nil?)
            # end
            Delayed::Worker.logger.debug "found address value:  #{found.address}"
            Delayed::Worker.logger.debug "found id value:  #{found.id}"
          
          end
          
          
          # found.override_attributes(update_found.to_h)
          
        elsif found = history.select{|p| p.mls_number.nil? }.first
          # Delayed::Worker.logger.debug "override no mls"
          # # a property with no mls_number found
          # # then override existing property
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        elsif history.first.status.in?('active', 'pending')
          # Delayed::Worker.logger.debug "override use first"
          # # if most recent history item is active or pending
          # # override existing item
          # found = history.first
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        else
          
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
          # Delayed::Worker.logger.debug "create new prop"
          # Delayed::Worker.logger.debug "#{history}"
          # # otherwise create new property record and use the appropriate property field data from history
          # history_attrs = history.first.active_attributes(delegate: PropertyDetail, calc: false, missing: false)
          # # filter out only attributes of interest
#           
          # # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
          #  --------------------------------- commented out ----------------------------------
          # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS.map { |fld| fld.to_sym  }).select {|k, v| !v.missing? }
          # history_extra_attrs = {}
#                    
          # %w{street_address address street_name street_number state}.each { |item| 
             # history_extra_attrs = history_extra_attrs.merge(item.to_sym => history.first[item.to_sym])
          # }
          # history_extra_attrs = history_extra_attrs.merge(:status => 'off')
          # found = self.properties.new(history_attrs.merge(attrs).merge(history_extra_attrs))
          #  --------------------------------- commented out ----------------------------------
          
          # merge history attrs with user attrs (superior) and create new record
          # found = self.properties.new(history_attrs.merge(attrs))
          
          
        end
      else
         Delayed::Worker.logger.debug "do nothing"
        # do nothing
      end
    end

    if found 
      
      Delayed::Worker.logger.debug "found true"
      # validate data quality on found property
      unless found.valid?
        # reset fields with errors
        Delayed::Worker.logger.debug "found error"
        found.errors.keys.each do |key| 
          Delayed::Worker.logger.debug "error key: #{key}"
          found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
        end
      end
      found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
      Delayed::Worker.logger.debug "found new record? #{found.new_record?}"
      Delayed::Worker.logger.debug "found id #{found.id}"
      found.data_source = source if found.new_record?
      found.status = 'off' if found.mls_number.missing?
      found.data_updated_at = DateTime.now
      found.save
      
      
      
      found
    end
  end

  def import4(source, attrs, raw={}, update_only=false)
    
    suffixes = HashWithCaseInsensitiveAccess.new( {"alley"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "allee"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "aly"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "ally"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "annex"=>["ANNEX", "ANEX", "ANX"], "anex"=>["ANNEX", "ANEX", "ANX"], "anx"=>["ANNEX", "ANEX", "ANX"], "arcade"=>["ARCADE", "ARC"], "arc"=>["ARCADE", "ARC"], "avenue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "av"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "ave"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "aven"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avenu"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avn"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avnue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "bayoo"=>["BAYOO", "BYU", "BAYOU"], "byu"=>["BAYOO", "BYU", "BAYOU"], "bayou"=>["BAYOO", "BYU", "BAYOU"], "beach"=>["BEACH", "BCH"], "bch"=>["BEACH", "BCH"], "bend"=>["BEND", "BND"], "bnd"=>["BEND", "BND"], "bluff"=>["BLUFF", "BLF", "BLUF"], "blf"=>["BLUFF", "BLF", "BLUF"], "bluf"=>["BLUFF", "BLF", "BLUF"], "bluffs"=>["BLUFFS", "BLFS"], "blfs"=>["BLUFFS", "BLFS"], "bottom"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bot"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "btm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bottm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "boulevard"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "blvd"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boul"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boulv"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "branch"=>["BRANCH", "BR", "BRNCH"], "br"=>["BRANCH", "BR", "BRNCH"], "brnch"=>["BRANCH", "BR", "BRNCH"], "bridge"=>["BRIDGE", "BRDGE", "BRG"], "brdge"=>["BRIDGE", "BRDGE", "BRG"], "brg"=>["BRIDGE", "BRDGE", "BRG"], "brook"=>["BROOK", "BRK"], "brk"=>["BROOK", "BRK"], "brooks"=>["BROOKS", "BRKS"], "brks"=>["BROOKS", "BRKS"], "burg"=>["BURG", "BG"], "bg"=>["BURG", "BG"], "burgs"=>["BURGS", "BGS"], "bgs"=>["BURGS", "BGS"], "bypass"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byp"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypa"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypas"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byps"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "camp"=>["CAMP", "CP", "CMP"], "cp"=>["CAMP", "CP", "CMP"], "cmp"=>["CAMP", "CP", "CMP"], "canyon"=>["CANYON", "CANYN", "CYN", "CNYN"], "canyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cnyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cape"=>["CAPE", "CPE"], "cpe"=>["CAPE", "CPE"], "causeway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "cswy"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "causway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "center"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cen"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "ctr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cent"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centre"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cnter"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cntr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centers"=>["CENTERS", "CTRS"], "ctrs"=>["CENTERS", "CTRS"], "circle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "cir"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circ"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circles"=>["CIRCLES", "CIRS"], "cirs"=>["CIRCLES", "CIRS"], "cliff"=>["CLIFF", "CLF"], "clf"=>["CLIFF", "CLF"], "cliffs"=>["CLIFFS", "CLFS"], "clfs"=>["CLIFFS", "CLFS"], "club"=>["CLUB", "CLB"], "clb"=>["CLUB", "CLB"], "common"=>["COMMON", "CMN"], "cmn"=>["COMMON", "CMN"], "corner"=>["CORNER", "COR"], "cor"=>["CORNER", "COR"], "corners"=>["CORNERS", "CORS"], "cors"=>["CORNERS", "CORS"], "course"=>["COURSE", "CRSE"], "crse"=>["COURSE", "CRSE"], "court"=>["COURT", "CT", "CRT"], "ct"=>["COURTS", "CTS", "CT"], "crt"=>["COURT", "CT", "CRT"], "courts"=>["COURTS", "CTS", "CT"], "cts"=>["COURTS", "CTS", "CT"], "cove"=>["COVE", "CV"], "cv"=>["COVE", "CV"], "coves"=>["COVES", "CVS"], "cvs"=>["COVES", "CVS"], "creek"=>["CREEK", "CK", "CRK", "CR"], "ck"=>["CREEK", "CK", "CRK", "CR"], "crk"=>["CREEK", "CK", "CRK", "CR"], "cr"=>["CREEK", "CK", "CRK", "CR"], "crescent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crecent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cres"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cresent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crscnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crest"=>["CREST", "CRST"], "crst"=>["CREST", "CRST"], "crossing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "xing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssng"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crossroad"=>["CROSSROAD", "XRD"], "xrd"=>["CROSSROAD", "XRD"], "curve"=>["CURVE", "CURV"], "curv"=>["CURVE", "CURV"], "dale"=>["DALE", "DL"], "dl"=>["DALE", "DL"], "dam"=>["DAM", "DM"], "dm"=>["DAM", "DM"], "divide"=>["DIVIDE", "DIV", "DV", "DVD"], "div"=>["DIVIDE", "DIV", "DV", "DVD"], "dv"=>["DIVIDE", "DIV", "DV", "DVD"], "dvd"=>["DIVIDE", "DIV", "DV", "DVD"], "drive"=>["DRIVE", "DR", "DRIV", "DRV"], "dr"=>["DRIVE", "DR", "DRIV", "DRV"], "driv"=>["DRIVE", "DR", "DRIV", "DRV"], "drv"=>["DRIVE", "DR", "DRIV", "DRV"], "drives"=>["DRIVES", "DRS"], "drs"=>["DRIVES", "DRS"], "estate"=>["ESTATE", "EST"], "est"=>["ESTATE", "EST"], "estates"=>["ESTATES", "ESTS"], "ests"=>["ESTATES", "ESTS"], "expressway"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "exp"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expy"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expr"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "express"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expw"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "extension"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "ext"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extnsn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extensions"=>["EXTENSIONS", "EXTS"], "exts"=>["EXTENSIONS", "EXTS"], "fall"=>["FALL"], "falls"=>["FALLS", "FLS"], "fls"=>["FALLS", "FLS"], "ferry"=>["FERRY", "FRY", "FRRY"], "fry"=>["FERRY", "FRY", "FRRY"], "frry"=>["FERRY", "FRY", "FRRY"], "field"=>["FIELD", "FLD"], "fld"=>["FIELD", "FLD"], "fields"=>["FIELDS", "FLDS"], "flds"=>["FIELDS", "FLDS"], "flat"=>["FLAT", "FLT"], "flt"=>["FLAT", "FLT"], "flats"=>["FLATS", "FLTS"], "flts"=>["FLATS", "FLTS"], "ford"=>["FORD", "FRD"], "frd"=>["FORD", "FRD"], "fords"=>["FORDS", "FRDS"], "frds"=>["FORDS", "FRDS"], "forest"=>["FOREST", "FRST", "FORESTS"], "frst"=>["FOREST", "FRST", "FORESTS"], "forests"=>["FOREST", "FRST", "FORESTS"], "forge"=>["FORGE", "FORG", "FRG"], "forg"=>["FORGE", "FORG", "FRG"], "frg"=>["FORGE", "FORG", "FRG"], "forges"=>["FORGES", "FRGS"], "frgs"=>["FORGES", "FRGS"], "fork"=>["FORK", "FRK"], "frk"=>["FORK", "FRK"], "forks"=>["FORKS", "FRKS"], "frks"=>["FORKS", "FRKS"], "fort"=>["FORT", "FT", "FRT"], "ft"=>["FORT", "FT", "FRT"], "frt"=>["FORT", "FT", "FRT"], "freeway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "fwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "freewy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "garden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardens"=>["GARDENS", "GDNS", "GRDNS"], "gdns"=>["GARDENS", "GDNS", "GRDNS"], "grdns"=>["GARDENS", "GDNS", "GRDNS"], "gateway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtwy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatewy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "glen"=>["GLEN", "GLN"], "gln"=>["GLEN", "GLN"], "glens"=>["GLENS", "GLNS"], "glns"=>["GLENS", "GLNS"], "green"=>["GREEN", "GRN"], "grn"=>["GREEN", "GRN"], "greens"=>["GREENS", "GRNS"], "grns"=>["GREENS", "GRNS"], "grove"=>["GROVE", "GROV", "GRV"], "grov"=>["GROVE", "GROV", "GRV"], "grv"=>["GROVE", "GROV", "GRV"], "groves"=>["GROVES", "GRVS"], "grvs"=>["GROVES", "GRVS"], "harbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harb"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hrbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbors"=>["HARBORS", "HBRS"], "hbrs"=>["HARBORS", "HBRS"], "haven"=>["HAVEN", "HVN", "HAVN"], "hvn"=>["HAVEN", "HVN", "HAVN"], "havn"=>["HAVEN", "HVN", "HAVN"], "heights"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "height"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hgts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "ht"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "highway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "highwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hill"=>["HILL", "HL"], "hl"=>["HILL", "HL"], "hills"=>["HILLS", "HLS"], "hls"=>["HILLS", "HLS"], "hollow"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hllw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hollows"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holws"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "inlet"=>["INLET", "INLT"], "inlt"=>["INLET", "INLT"], "island"=>["ISLAND", "IS", "ISLND"], "is"=>["ISLAND", "IS", "ISLND"], "islnd"=>["ISLAND", "IS", "ISLND"], "islands"=>["ISLANDS", "ISS", "ISLNDS"], "iss"=>["ISLANDS", "ISS", "ISLNDS"], "islnds"=>["ISLANDS", "ISS", "ISLNDS"], "isle"=>["ISLE", "ISLES"], "isles"=>["ISLE", "ISLES"], "junction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jct"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "juncton"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctions"=>["JUNCTIONS", "JCTNS", "JCTS"], "jctns"=>["JUNCTIONS", "JCTNS", "JCTS"], "jcts"=>["JUNCTIONS", "JCTNS", "JCTS"], "key"=>["KEY", "KY"], "ky"=>["KEY", "KY"], "keys"=>["KEYS", "KYS"], "kys"=>["KEYS", "KYS"], "knoll"=>["KNOLL", "KNL", "KNOL"], "knl"=>["KNOLL", "KNL", "KNOL"], "knol"=>["KNOLL", "KNL", "KNOL"], "knolls"=>["KNOLLS", "KNLS"], "knls"=>["KNOLLS", "KNLS"], "lake"=>["LAKE", "LK"], "lk"=>["LAKE", "LK"], "lakes"=>["LAKES", "LKS"], "lks"=>["LAKES", "LKS"], "land"=>["LAND"], "landing"=>["LANDING", "LNDG", "LNDNG"], "lndg"=>["LANDING", "LNDG", "LNDNG"], "lndng"=>["LANDING", "LNDG", "LNDNG"], "lane"=>["LANE", "LA", "LN", "LANES"], "la"=>["LANE", "LA", "LN", "LANES"], "ln"=>["LANE", "LA", "LN", "LANES"], "lanes"=>["LANE", "LA", "LN", "LANES"], "light"=>["LIGHT", "LGT"], "lgt"=>["LIGHT", "LGT"], "lights"=>["LIGHTS", "LGTS"], "lgts"=>["LIGHTS", "LGTS"], "loaf"=>["LOAF", "LF"], "lf"=>["LOAF", "LF"], "lock"=>["LOCK", "LCK"], "lck"=>["LOCK", "LCK"], "locks"=>["LOCKS", "LCKS"], "lcks"=>["LOCKS", "LCKS"], "lodge"=>["LODGE", "LDG", "LDGE", "LODG"], "ldg"=>["LODGE", "LDG", "LDGE", "LODG"], "ldge"=>["LODGE", "LDG", "LDGE", "LODG"], "lodg"=>["LODGE", "LDG", "LDGE", "LODG"], "loop"=>["LOOP", "LOOPS"], "loops"=>["LOOP", "LOOPS"], "mall"=>["MALL"], "manor"=>["MANOR", "MNR"], "mnr"=>["MANOR", "MNR"], "manors"=>["MANORS", "MNRS"], "mnrs"=>["MANORS", "MNRS"], "meadow"=>["MEADOW", "MDW"], "mdw"=>["MEADOW", "MDW"], "meadows"=>["MEADOWS", "MDWS", "MEDOWS"], "mdws"=>["MEADOWS", "MDWS", "MEDOWS"], "medows"=>["MEADOWS", "MDWS", "MEDOWS"], "mews"=>["MEWS"], "mill"=>["MILL", "ML"], "ml"=>["MILL", "ML"], "mills"=>["MILLS", "MLS"], "mls"=>["MILLS", "MLS"], "mission"=>["MISSION", "MSN", "MISSN", "MSSN"], "msn"=>["MISSION", "MSN", "MISSN", "MSSN"], "missn"=>["MISSION", "MSN", "MISSN", "MSSN"], "mssn"=>["MISSION", "MSN", "MISSN", "MSSN"], "motorway"=>["MOTORWAY", "MTWY"], "mtwy"=>["MOTORWAY", "MTWY"], "mount"=>["MOUNT", "MNT", "MT"], "mnt"=>["MOUNT", "MNT", "MT"], "mt"=>["MOUNT", "MNT", "MT"], "mountain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountains"=>["MOUNTAINS", "MNTNS", "MTNS"], "mntns"=>["MOUNTAINS", "MNTNS", "MTNS"], "mtns"=>["MOUNTAINS", "MNTNS", "MTNS"], "neck"=>["NECK", "NCK"], "nck"=>["NECK", "NCK"], "orchard"=>["ORCHARD", "ORCH", "ORCHRD"], "orch"=>["ORCHARD", "ORCH", "ORCHRD"], "orchrd"=>["ORCHARD", "ORCH", "ORCHRD"], "oval"=>["OVAL", "OVL"], "ovl"=>["OVAL", "OVL"], "overpass"=>["OVERPASS", "OPAS"], "opas"=>["OVERPASS", "OPAS"], "park"=>["PARKS", "PARK"], "pk"=>["PARK", "PK", "PRK"], "prk"=>["PARK", "PK", "PRK"], "parks"=>["PARKS", "PARK"], "parkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkwy"=>["PARKWAYS", "PKWY", "PKWYS"], "parkwy"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pky"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "parkways"=>["PARKWAYS", "PKWY", "PKWYS"], "pkwys"=>["PARKWAYS", "PKWY", "PKWYS"], "pass"=>["PASS"], "passage"=>["PASSAGE", "PSGE"], "psge"=>["PASSAGE", "PSGE"], "path"=>["PATH", "PATHS"], "paths"=>["PATH", "PATHS"], "pike"=>["PIKE", "PIKES"], "pikes"=>["PIKE", "PIKES"], "pine"=>["PINE", "PNE"], "pne"=>["PINE", "PNE"], "pines"=>["PINES", "PNES"], "pnes"=>["PINES", "PNES"], "place"=>["PLACE", "PL"], "pl"=>["PLACE", "PL"], "plain"=>["PLAIN", "PLN"], "pln"=>["PLAIN", "PLN"], "plains"=>["PLAINS", "PLAINES", "PLNS"], "plaines"=>["PLAINS", "PLAINES", "PLNS"], "plns"=>["PLAINS", "PLAINES", "PLNS"], "plaza"=>["PLAZA", "PLZ", "PLZA"], "plz"=>["PLAZA", "PLZ", "PLZA"], "plza"=>["PLAZA", "PLZ", "PLZA"], "point"=>["POINT", "PT"], "pt"=>["POINT", "PT"], "points"=>["POINTS", "PTS"], "pts"=>["POINTS", "PTS"], "port"=>["PORT", "PRT"], "prt"=>["PORT", "PRT"], "ports"=>["PORTS", "PRTS"], "prts"=>["PORTS", "PRTS"], "prairie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "pr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prarie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "radial"=>["RADIAL", "RAD", "RADL", "RADIEL"], "rad"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radl"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radiel"=>["RADIAL", "RAD", "RADL", "RADIEL"], "ramp"=>["RAMP"], "ranch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "ranches"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnchs"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rapid"=>["RAPID", "RPD"], "rpd"=>["RAPID", "RPD"], "rapids"=>["RAPIDS", "RPDS"], "rpds"=>["RAPIDS", "RPDS"], "rest"=>["REST", "RST"], "rst"=>["REST", "RST"], "ridge"=>["RIDGE", "RDG", "RDGE"], "rdg"=>["RIDGE", "RDG", "RDGE"], "rdge"=>["RIDGE", "RDG", "RDGE"], "ridges"=>["RIDGES", "RDGS"], "rdgs"=>["RIDGES", "RDGS"], "river"=>["RIVER", "RIV", "RIVR", "RVR"], "riv"=>["RIVER", "RIV", "RIVR", "RVR"], "rivr"=>["RIVER", "RIV", "RIVR", "RVR"], "rvr"=>["RIVER", "RIV", "RIVR", "RVR"], "road"=>["ROAD", "RD"], "rd"=>["ROAD", "RD"], "roads"=>["ROADS", "RDS"], "rds"=>["ROADS", "RDS"], "route"=>["ROUTE", "RTE"], "rte"=>["ROUTE", "RTE"], "row"=>["ROW"], "rue"=>["RUE"], "run"=>["RUN"], "shoal"=>["SHOAL", "SHL"], "shl"=>["SHOAL", "SHL"], "shoals"=>["SHOALS", "SHLS"], "shls"=>["SHOALS", "SHLS"], "shore"=>["SHORE", "SHOAR", "SHR"], "shoar"=>["SHORE", "SHOAR", "SHR"], "shr"=>["SHORE", "SHOAR", "SHR"], "shores"=>["SHORES", "SHOARS", "SHRS"], "shoars"=>["SHORES", "SHOARS", "SHRS"], "shrs"=>["SHORES", "SHOARS", "SHRS"], "skyway"=>["SKYWAY", "SKWY"], "skwy"=>["SKYWAY", "SKWY"], "spring"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spg"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "sprng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "springs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spgs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "sprngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spur"=>["SPURS", "SPUR"], "spurs"=>["SPURS", "SPUR"], "square"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sq"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqr"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqre"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squ"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squares"=>["SQUARES", "SQRS", "SQS"], "sqrs"=>["SQUARES", "SQRS", "SQS"], "sqs"=>["SQUARES", "SQRS", "SQS"], "station"=>["STATION", "STA", "STATN", "STN"], "sta"=>["STATION", "STA", "STATN", "STN"], "statn"=>["STATION", "STA", "STATN", "STN"], "stn"=>["STATION", "STA", "STATN", "STN"], "stravenue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stra"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strav"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strave"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "straven"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stravn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvnue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stream"=>["STREAM", "STRM", "STREME"], "strm"=>["STREAM", "STRM", "STREME"], "streme"=>["STREAM", "STRM", "STREME"], "street"=>["STREET", "ST", "STR", "STRT"], "st"=>["STREET", "ST", "STR", "STRT"], "str"=>["STREET", "ST", "STR", "STRT"], "strt"=>["STREET", "ST", "STR", "STRT"], "streets"=>["STREETS", "STS"], "sts"=>["STREETS", "STS"], "summit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "smt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumitt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "terrace"=>["TERRACE", "TER", "TERR"], "ter"=>["TERRACE", "TER", "TERR"], "terr"=>["TERRACE", "TER", "TERR"], "throughway"=>["THROUGHWAY", "TRWY"], "trwy"=>["THROUGHWAY", "TRWY"], "trace"=>["TRACE", "TRCE", "TRACES"], "trce"=>["TRACE", "TRCE", "TRACES"], "traces"=>["TRACE", "TRCE", "TRACES"], "track"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trak"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "tracks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trk"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trafficway"=>["TRAFFICWAY", "TRFY"], "trfy"=>["TRAFFICWAY", "TRFY"], "trail"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tr"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trl"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trails"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trls"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tunnel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunls"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnels"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "turnpike"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpke"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "turnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "underpass"=>["UNDERPASS", "UPAS"], "upas"=>["UNDERPASS", "UPAS"], "union"=>["UNION", "UN"], "un"=>["UNION", "UN"], "unions"=>["UNIONS", "UNS"], "uns"=>["UNIONS", "UNS"], "valley"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vally"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vlly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "valleys"=>["VALLEYS", "VLYS"], "vlys"=>["VALLEYS", "VLYS"], "viaduct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "vdct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "via"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "viadct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "view"=>["VIEW", "VW"], "vw"=>["VIEW", "VW"], "views"=>["VIEWS", "VWS"], "vws"=>["VIEWS", "VWS"], "village"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vill"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vlg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villag"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villiage"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villages"=>["VILLAGES", "VLGS"], "vlgs"=>["VILLAGES", "VLGS"], "ville"=>["VILLE", "VL"], "vl"=>["VILLE", "VL"], "vista"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vis"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vist"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vst"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vsta"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "walk"=>["WALKS", "WALK"], "walks"=>["WALKS", "WALK"], "wall"=>["WALL"], "way"=>["WAY", "WY"], "wy"=>["WAY", "WY"], "ways"=>["WAYS"], "well"=>["WELL", "WL"], "wl"=>["WELL", "WL"], "wells"=>["WELLS", "WLS"], "wls"=>["WELLS", "WLS"]})
    
    source_elem = nil
    mls_source_elem = nil
    
    if source.is_a?(Hash)
      source_elem = source[:source]
      mls_source_elem = source[:mls_source]
    else
      source_elem = source
    end
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    Delayed::Worker.logger.debug " state comparing self:  #{attrs[:state]} = #{self.state} " 
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state
    
    if attrs[:mls_number] # a mls number exists
      raw_list_price = nil
       Delayed::Worker.logger.debug "mls number exists"
      # look for property with mls number in market database 
     # look for property with mls number in market database 
      found = self.properties.find_by_mls_number(attrs[:mls_number])  
      if !found && !attrs[:street_name].blank? 
        street_number_str = attrs[:street_number].blank? ? "street_number is null" : "street_number = '#{attrs[:street_number]}'"
        street_dir_str = attrs[:street_direction].blank? ? "street_direction is null" : "street_direction = '#{attrs[:street_direction]}'"
        
        suff_key = attrs[:street_name].to_s.split(" ").last
        if suff_key.in?(suffixes)
          
          street_name = attrs[:street_name].to_s.gsub(suff_key,"").gsub(/\s+/,"")
          tokens = []
          suffixes[suff_key].each do |suff|
            tokens << "street_name like '#{street_name} #{suff}%'"
          end
          # q1 = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql
          # Delayed::Worker.logger.debug "dedup sql: #{q1}"
           Delayed::Worker.logger.debug "suff query #{self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql}"
          # founds = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem).order("id desc")

          founds = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc")
        else
          # q2 = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql
          # Delayed::Worker.logger.debug "dedup sql: #{q2}"
          Delayed::Worker.logger.debug "suff query #{self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql}"
          # founds = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem).order("id desc")
          founds = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc")
        end 

        if founds.length > 0
          Delayed::Worker.logger.debug "## what is found in different source: #{found}  => #{attrs[:mls_number]}"
          found = founds.first
        end 
        
      end
      
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      
      
      if update_only
        
        if found # a property is found
          Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}" if found.list_price
          Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw && raw[:list_price]
          Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw && raw[:list_price]
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
        end
      else
        if found # a property is found
          
          Delayed::Worker.logger.debug "~~sub_category: #{found.sub_category} attr sub_cat: #{attrs[:sub_category]}"
          if !(found.sub_category && (found.sub_category == attrs[:sub_category]))
            Delayed::Worker.logger.debug "duplicate record is entering"
            # Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}"
            # Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw
            # Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw
            if attrs && attrs[:list_price]
              attr_list_price = attrs[:list_price].to_s.gsub(/[\\z,]/, '').to_i
            end
            # Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
            # Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
            # if raw 
              # # changed_values = {}
              # # changed_values = changed_values.with_indifferent_access
              # found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
                # true
              # else
                # false
              # end
  #            
            # end
            if (attrs[:status].in?(['active']) && !attrs[:listing_date].blank?)

              selected_fields = attrs.select{|x| ![:listing_date,:dom,:cdom].include?(x)}
            else
              selected_fields = attrs
            end
                      
            if attrs && (!found.auto_updatable)
              # changed_values = {}
              # changed_values = changed_values.with_indifferent_access
              found.auto_updatable =  if (found.list_price.to_i != attr_list_price)
                true
              else
                false
              end
             
            end
            
            found.assign_attributes(selected_fields)
            found.mls_source = mls_source_elem
            
            if found.status_changed?
              if found.status_was == "pending" && found.status == "active"
                found.is_back_on_market = true
                found.back_on_market = attrs[:mls_record_modified]
              else
                found.is_back_on_market = nil
                found.back_on_market = nil  
              end    
            end
  
            if found.list_price_changed?
              
              if found.list_price_was > found.list_price 
                found.price_reduction = true
                found.back_on_market = attrs[:mls_record_modified]
              else
                found.price_reduction = nil
                found.back_on_market = nil
              end
            end
          
          end
        
        else  # no property found and not in hotsheet
          Delayed::Worker.logger.debug "creating new property record in market.import()"
          
          found = self.properties.new(attrs) # create new property record
          found.mls_source = mls_source_elem
          found.auto_updatable  = true
        end
      end 
    else # no mls number exists
      if ((attrs[:tax_id] && attrs[:zip_code]) || (attrs[:street_address] && attrs[:zip_code]) || 
        (attrs[:tax_id] && attrs[:county])  || 
        (attrs[:street_address] && attrs[:county] )) # a tax account id or both street_address+zip_code exists
        
        
        # look for properties with the same properties
        history = self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        history_desc_id =  self.properties.similar_desc_id(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        bathrooms = attrs[:bathrooms]
        attrs = attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
        # Delayed::Worker.logger.debug  "similar prop #{history}"
        # Delayed::Worker.logger.debug "similar prop  #{self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).order("id desc").all}"
        
        Delayed::Worker.logger.debug "ZzzZ similar prop #{history_desc_id}"
        if history.empty? # if no property found
          # create new property
          found = self.properties.new(attrs)
          
          
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
        elsif (!history_desc_id.first.status.blank? &&  history_desc_id.first.status.in?('off'))
          update_found =  history_desc_id.first
           # create new property
          # update_found = self.properties.new(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark update lookup"
          found = pshark.lookup(found)
          
          if found
          # Delayed::Worker.logger.debug "override attributes #{found["address"]} state: #{found["state"]}"
            temp_id = update_found.id
            # Delayed::Worker.logger.debug "update found object: #{update_found}"
            update_found.destroy
            found.id = temp_id
            found.detail.id = temp_id
            # found.attributes.each do |k,v|
              # Delayed::Worker.logger.debug "attr_name  #{k}"
              # Delayed::Worker.logger.debug "update found k #{k}  value #{update_found[k]}"
              # found.send(:"#{k}=",update_found[k]) unless (k.in?(%w{id created_at}) || update_found[k].nil?)
            # end
            Delayed::Worker.logger.debug "found address value:  #{found.address}"
            Delayed::Worker.logger.debug "found id value:  #{found.id}"
          
          end
          
          
          # found.override_attributes(update_found.to_h)
          
        elsif found = history.select{|p| p.mls_number.nil? }.first
          # Delayed::Worker.logger.debug "override no mls"
          # # a property with no mls_number found
          # # then override existing property
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        elsif history.first.status.in?('active', 'pending')
          # Delayed::Worker.logger.debug "override use first"
          # # if most recent history item is active or pending
          # # override existing item
          # found = history.first
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        else
          
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
          # Delayed::Worker.logger.debug "create new prop"
          # Delayed::Worker.logger.debug "#{history}"
          # # otherwise create new property record and use the appropriate property field data from history
          # history_attrs = history.first.active_attributes(delegate: PropertyDetail, calc: false, missing: false)
          # # filter out only attributes of interest
#           
          # # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
          #  --------------------------------- commented out ----------------------------------
          # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS.map { |fld| fld.to_sym  }).select {|k, v| !v.missing? }
          # history_extra_attrs = {}
#                    
          # %w{street_address address street_name street_number state}.each { |item| 
             # history_extra_attrs = history_extra_attrs.merge(item.to_sym => history.first[item.to_sym])
          # }
          # history_extra_attrs = history_extra_attrs.merge(:status => 'off')
          # found = self.properties.new(history_attrs.merge(attrs).merge(history_extra_attrs))
          #  --------------------------------- commented out ----------------------------------
          
          # merge history attrs with user attrs (superior) and create new record
          # found = self.properties.new(history_attrs.merge(attrs))
          
          
        end
      else
         Delayed::Worker.logger.debug "do nothing"
        # do nothing
      end
    end

    if found 
      
      Delayed::Worker.logger.debug "found true"
      # validate data quality on found property
      unless found.valid?
        # reset fields with errors
        Delayed::Worker.logger.debug "found error"
        found.errors.keys.each do |key| 
          Delayed::Worker.logger.debug "error key: #{key}"
          found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
        end
      end
      found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
      Delayed::Worker.logger.debug "found new record? #{found.new_record?}"
      Delayed::Worker.logger.debug "found id #{found.id}"
      found.data_source = source_elem if found.new_record?
      
      found.status = 'off' if found.mls_number.missing?
      found.data_updated_at = DateTime.now
      found.save
      
      
      
      found
    end
  end

  def import5(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    Delayed::Worker.logger.debug " state comparing self:  #{attrs[:state]} = #{self.state} " 
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      raw_list_price = nil
       Delayed::Worker.logger.debug "mls number exists"
      # look for property with mls number in market database 
      found = self.properties.find_by_mls_number(attrs[:mls_number])  
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      Delayed::Worker.logger.debug "## what is found #{found}  => #{attrs[:mls_number]}"
      
      if update_only
        
        if found # a property is found
          Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}" if found.list_price
          Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw && raw[:list_price]
          Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw && raw[:list_price]
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
        end
      else
        if found # a property is found
          Delayed::Worker.logger.debug "duplicate record is entering"
          
          found.assign_attributes(list_price: attrs[:list_price])
          
          if found.list_price_changed?
            
            if found.list_price_was > found.list_price 
              found.price_reduction = true
            else
              found.price_reduction = nil
            end
          end
          
        
        else  # no property found and not in hotsheet
          Delayed::Worker.logger.debug "creating new property record in market.import()"
          
          found = self.properties.new(attrs) # create new property record
          found.auto_updatable  = true
        end
      end 
    else # no mls number exists
      if ((attrs[:tax_id] && attrs[:zip_code]) || (attrs[:street_address] && attrs[:zip_code]) || 
        (attrs[:tax_id] && attrs[:county])  || 
        (attrs[:street_address] && attrs[:county] )) # a tax account id or both street_address+zip_code exists
        
        
        # look for properties with the same properties
        history = self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        history_desc_id =  self.properties.similar_desc_id(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        bathrooms = attrs[:bathrooms]
        attrs = attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
        # Delayed::Worker.logger.debug  "similar prop #{history}"
        # Delayed::Worker.logger.debug "similar prop  #{self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).order("id desc").all}"
        
        Delayed::Worker.logger.debug "ZzzZ similar prop #{history_desc_id}"
        if history.empty? # if no property found
          # create new property
          found = self.properties.new(attrs)
          
          
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
        elsif (!history_desc_id.first.status.blank? &&  history_desc_id.first.status.in?('off'))
          update_found =  history_desc_id.first
           # create new property
          # update_found = self.properties.new(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark update lookup"
          found = pshark.lookup(found)
          
          if found
          # Delayed::Worker.logger.debug "override attributes #{found["address"]} state: #{found["state"]}"
            temp_id = update_found.id
            # Delayed::Worker.logger.debug "update found object: #{update_found}"
            update_found.destroy
            found.id = temp_id
            found.detail.id = temp_id
            # found.attributes.each do |k,v|
              # Delayed::Worker.logger.debug "attr_name  #{k}"
              # Delayed::Worker.logger.debug "update found k #{k}  value #{update_found[k]}"
              # found.send(:"#{k}=",update_found[k]) unless (k.in?(%w{id created_at}) || update_found[k].nil?)
            # end
            Delayed::Worker.logger.debug "found address value:  #{found.address}"
            Delayed::Worker.logger.debug "found id value:  #{found.id}"
          
          end
          
          
          # found.override_attributes(update_found.to_h)
          
        elsif found = history.select{|p| p.mls_number.nil? }.first
          # Delayed::Worker.logger.debug "override no mls"
          # # a property with no mls_number found
          # # then override existing property
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        elsif history.first.status.in?('active', 'pending')
          # Delayed::Worker.logger.debug "override use first"
          # # if most recent history item is active or pending
          # # override existing item
          # found = history.first
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        else
          
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
          # Delayed::Worker.logger.debug "create new prop"
          # Delayed::Worker.logger.debug "#{history}"
          # # otherwise create new property record and use the appropriate property field data from history
          # history_attrs = history.first.active_attributes(delegate: PropertyDetail, calc: false, missing: false)
          # # filter out only attributes of interest
#           
          # # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
          #  --------------------------------- commented out ----------------------------------
          # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS.map { |fld| fld.to_sym  }).select {|k, v| !v.missing? }
          # history_extra_attrs = {}
#                    
          # %w{street_address address street_name street_number state}.each { |item| 
             # history_extra_attrs = history_extra_attrs.merge(item.to_sym => history.first[item.to_sym])
          # }
          # history_extra_attrs = history_extra_attrs.merge(:status => 'off')
          # found = self.properties.new(history_attrs.merge(attrs).merge(history_extra_attrs))
          #  --------------------------------- commented out ----------------------------------
          
          # merge history attrs with user attrs (superior) and create new record
          # found = self.properties.new(history_attrs.merge(attrs))
          
          
        end
      else
         Delayed::Worker.logger.debug "do nothing"
        # do nothing
      end
    end

    if found 
      
      Delayed::Worker.logger.debug "found true"
      # validate data quality on found property
      unless found.valid?
        # reset fields with errors
        Delayed::Worker.logger.debug "found error"
        found.errors.keys.each do |key| 
          Delayed::Worker.logger.debug "error key: #{key}"
          found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
        end
      end
      found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
      Delayed::Worker.logger.debug "found new record? #{found.new_record?}"
      Delayed::Worker.logger.debug "found id #{found.id}"
      found.data_source = source if found.new_record?
      found.status = 'off' if found.mls_number.missing?
      found.data_updated_at = DateTime.now
      found.save
      
      
      
      found
    end
  end
  
  def import_waterfront(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self c state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      # look for property with mls number in market database 
      puts "attr mls_number: #{attrs[:mls_number]}"
      founds = self.properties.where(["(mls_number = ?)", attrs[:mls_number]]) rescue nil; 
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      
      if founds && (founds.length > 0)# a property is found
               
        for found in founds
          # if !found.closed_date
            
            found.assign_attributes({square_feet: attrs[:square_feet]})
            # validate data quality on found property
            unless found.valid?
              # reset fields with errors
              found.errors.keys.each do |key| 
                found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
              end
            end
            found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
            puts "updated id: #{found.id} square_feet: #{attrs[:square_feet]} "
            Delayed::Worker.logger.debug "updated id: #{found.id} square_feet: #{attrs[:square_feet]} "
            found.data_source = source if found.new_record?
            found.status = 'off' if found.mls_number.missing?
            found.data_updated_at = DateTime.now
            found.save
          # end
          
          
        end
        
        return founds
      
      end     
    end
  end
  
  def import6(source, attrs, raw={}, update_only=false)
    
    # suffixes = HashWithCaseInsensitiveAccess.new
    suffixes = HashWithCaseInsensitiveAccess.new( {"alley"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "allee"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "aly"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "ally"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "annex"=>["ANNEX", "ANEX", "ANX"], "anex"=>["ANNEX", "ANEX", "ANX"], "anx"=>["ANNEX", "ANEX", "ANX"], "arcade"=>["ARCADE", "ARC"], "arc"=>["ARCADE", "ARC"], "avenue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "av"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "ave"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "aven"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avenu"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avn"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avnue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "bayoo"=>["BAYOO", "BYU", "BAYOU"], "byu"=>["BAYOO", "BYU", "BAYOU"], "bayou"=>["BAYOO", "BYU", "BAYOU"], "beach"=>["BEACH", "BCH"], "bch"=>["BEACH", "BCH"], "bend"=>["BEND", "BND"], "bnd"=>["BEND", "BND"], "bluff"=>["BLUFF", "BLF", "BLUF"], "blf"=>["BLUFF", "BLF", "BLUF"], "bluf"=>["BLUFF", "BLF", "BLUF"], "bluffs"=>["BLUFFS", "BLFS"], "blfs"=>["BLUFFS", "BLFS"], "bottom"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bot"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "btm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bottm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "boulevard"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "blvd"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boul"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boulv"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "branch"=>["BRANCH", "BR", "BRNCH"], "br"=>["BRANCH", "BR", "BRNCH"], "brnch"=>["BRANCH", "BR", "BRNCH"], "bridge"=>["BRIDGE", "BRDGE", "BRG"], "brdge"=>["BRIDGE", "BRDGE", "BRG"], "brg"=>["BRIDGE", "BRDGE", "BRG"], "brook"=>["BROOK", "BRK"], "brk"=>["BROOK", "BRK"], "brooks"=>["BROOKS", "BRKS"], "brks"=>["BROOKS", "BRKS"], "burg"=>["BURG", "BG"], "bg"=>["BURG", "BG"], "burgs"=>["BURGS", "BGS"], "bgs"=>["BURGS", "BGS"], "bypass"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byp"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypa"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypas"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byps"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "camp"=>["CAMP", "CP", "CMP"], "cp"=>["CAMP", "CP", "CMP"], "cmp"=>["CAMP", "CP", "CMP"], "canyon"=>["CANYON", "CANYN", "CYN", "CNYN"], "canyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cnyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cape"=>["CAPE", "CPE"], "cpe"=>["CAPE", "CPE"], "causeway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "cswy"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "causway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "center"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cen"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "ctr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cent"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centre"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cnter"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cntr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centers"=>["CENTERS", "CTRS"], "ctrs"=>["CENTERS", "CTRS"], "circle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "cir"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circ"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circles"=>["CIRCLES", "CIRS"], "cirs"=>["CIRCLES", "CIRS"], "cliff"=>["CLIFF", "CLF"], "clf"=>["CLIFF", "CLF"], "cliffs"=>["CLIFFS", "CLFS"], "clfs"=>["CLIFFS", "CLFS"], "club"=>["CLUB", "CLB"], "clb"=>["CLUB", "CLB"], "common"=>["COMMON", "CMN"], "cmn"=>["COMMON", "CMN"], "corner"=>["CORNER", "COR"], "cor"=>["CORNER", "COR"], "corners"=>["CORNERS", "CORS"], "cors"=>["CORNERS", "CORS"], "course"=>["COURSE", "CRSE"], "crse"=>["COURSE", "CRSE"], "court"=>["COURT", "CT", "CRT"], "ct"=>["COURTS", "CTS", "CT"], "crt"=>["COURT", "CT", "CRT"], "courts"=>["COURTS", "CTS", "CT"], "cts"=>["COURTS", "CTS", "CT"], "cove"=>["COVE", "CV"], "cv"=>["COVE", "CV"], "coves"=>["COVES", "CVS"], "cvs"=>["COVES", "CVS"], "creek"=>["CREEK", "CK", "CRK", "CR"], "ck"=>["CREEK", "CK", "CRK", "CR"], "crk"=>["CREEK", "CK", "CRK", "CR"], "cr"=>["CREEK", "CK", "CRK", "CR"], "crescent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crecent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cres"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cresent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crscnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crest"=>["CREST", "CRST"], "crst"=>["CREST", "CRST"], "crossing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "xing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssng"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crossroad"=>["CROSSROAD", "XRD"], "xrd"=>["CROSSROAD", "XRD"], "curve"=>["CURVE", "CURV"], "curv"=>["CURVE", "CURV"], "dale"=>["DALE", "DL"], "dl"=>["DALE", "DL"], "dam"=>["DAM", "DM"], "dm"=>["DAM", "DM"], "divide"=>["DIVIDE", "DIV", "DV", "DVD"], "div"=>["DIVIDE", "DIV", "DV", "DVD"], "dv"=>["DIVIDE", "DIV", "DV", "DVD"], "dvd"=>["DIVIDE", "DIV", "DV", "DVD"], "drive"=>["DRIVE", "DR", "DRIV", "DRV"], "dr"=>["DRIVE", "DR", "DRIV", "DRV"], "driv"=>["DRIVE", "DR", "DRIV", "DRV"], "drv"=>["DRIVE", "DR", "DRIV", "DRV"], "drives"=>["DRIVES", "DRS"], "drs"=>["DRIVES", "DRS"], "estate"=>["ESTATE", "EST"], "est"=>["ESTATE", "EST"], "estates"=>["ESTATES", "ESTS"], "ests"=>["ESTATES", "ESTS"], "expressway"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "exp"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expy"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expr"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "express"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expw"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "extension"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "ext"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extnsn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extensions"=>["EXTENSIONS", "EXTS"], "exts"=>["EXTENSIONS", "EXTS"], "fall"=>["FALL"], "falls"=>["FALLS", "FLS"], "fls"=>["FALLS", "FLS"], "ferry"=>["FERRY", "FRY", "FRRY"], "fry"=>["FERRY", "FRY", "FRRY"], "frry"=>["FERRY", "FRY", "FRRY"], "field"=>["FIELD", "FLD"], "fld"=>["FIELD", "FLD"], "fields"=>["FIELDS", "FLDS"], "flds"=>["FIELDS", "FLDS"], "flat"=>["FLAT", "FLT"], "flt"=>["FLAT", "FLT"], "flats"=>["FLATS", "FLTS"], "flts"=>["FLATS", "FLTS"], "ford"=>["FORD", "FRD"], "frd"=>["FORD", "FRD"], "fords"=>["FORDS", "FRDS"], "frds"=>["FORDS", "FRDS"], "forest"=>["FOREST", "FRST", "FORESTS"], "frst"=>["FOREST", "FRST", "FORESTS"], "forests"=>["FOREST", "FRST", "FORESTS"], "forge"=>["FORGE", "FORG", "FRG"], "forg"=>["FORGE", "FORG", "FRG"], "frg"=>["FORGE", "FORG", "FRG"], "forges"=>["FORGES", "FRGS"], "frgs"=>["FORGES", "FRGS"], "fork"=>["FORK", "FRK"], "frk"=>["FORK", "FRK"], "forks"=>["FORKS", "FRKS"], "frks"=>["FORKS", "FRKS"], "fort"=>["FORT", "FT", "FRT"], "ft"=>["FORT", "FT", "FRT"], "frt"=>["FORT", "FT", "FRT"], "freeway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "fwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "freewy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "garden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardens"=>["GARDENS", "GDNS", "GRDNS"], "gdns"=>["GARDENS", "GDNS", "GRDNS"], "grdns"=>["GARDENS", "GDNS", "GRDNS"], "gateway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtwy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatewy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "glen"=>["GLEN", "GLN"], "gln"=>["GLEN", "GLN"], "glens"=>["GLENS", "GLNS"], "glns"=>["GLENS", "GLNS"], "green"=>["GREEN", "GRN"], "grn"=>["GREEN", "GRN"], "greens"=>["GREENS", "GRNS"], "grns"=>["GREENS", "GRNS"], "grove"=>["GROVE", "GROV", "GRV"], "grov"=>["GROVE", "GROV", "GRV"], "grv"=>["GROVE", "GROV", "GRV"], "groves"=>["GROVES", "GRVS"], "grvs"=>["GROVES", "GRVS"], "harbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harb"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hrbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbors"=>["HARBORS", "HBRS"], "hbrs"=>["HARBORS", "HBRS"], "haven"=>["HAVEN", "HVN", "HAVN"], "hvn"=>["HAVEN", "HVN", "HAVN"], "havn"=>["HAVEN", "HVN", "HAVN"], "heights"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "height"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hgts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "ht"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "highway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "highwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hill"=>["HILL", "HL"], "hl"=>["HILL", "HL"], "hills"=>["HILLS", "HLS"], "hls"=>["HILLS", "HLS"], "hollow"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hllw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hollows"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holws"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "inlet"=>["INLET", "INLT"], "inlt"=>["INLET", "INLT"], "island"=>["ISLAND", "IS", "ISLND"], "is"=>["ISLAND", "IS", "ISLND"], "islnd"=>["ISLAND", "IS", "ISLND"], "islands"=>["ISLANDS", "ISS", "ISLNDS"], "iss"=>["ISLANDS", "ISS", "ISLNDS"], "islnds"=>["ISLANDS", "ISS", "ISLNDS"], "isle"=>["ISLE", "ISLES"], "isles"=>["ISLE", "ISLES"], "junction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jct"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "juncton"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctions"=>["JUNCTIONS", "JCTNS", "JCTS"], "jctns"=>["JUNCTIONS", "JCTNS", "JCTS"], "jcts"=>["JUNCTIONS", "JCTNS", "JCTS"], "key"=>["KEY", "KY"], "ky"=>["KEY", "KY"], "keys"=>["KEYS", "KYS"], "kys"=>["KEYS", "KYS"], "knoll"=>["KNOLL", "KNL", "KNOL"], "knl"=>["KNOLL", "KNL", "KNOL"], "knol"=>["KNOLL", "KNL", "KNOL"], "knolls"=>["KNOLLS", "KNLS"], "knls"=>["KNOLLS", "KNLS"], "lake"=>["LAKE", "LK"], "lk"=>["LAKE", "LK"], "lakes"=>["LAKES", "LKS"], "lks"=>["LAKES", "LKS"], "land"=>["LAND"], "landing"=>["LANDING", "LNDG", "LNDNG"], "lndg"=>["LANDING", "LNDG", "LNDNG"], "lndng"=>["LANDING", "LNDG", "LNDNG"], "lane"=>["LANE", "LA", "LN", "LANES"], "la"=>["LANE", "LA", "LN", "LANES"], "ln"=>["LANE", "LA", "LN", "LANES"], "lanes"=>["LANE", "LA", "LN", "LANES"], "light"=>["LIGHT", "LGT"], "lgt"=>["LIGHT", "LGT"], "lights"=>["LIGHTS", "LGTS"], "lgts"=>["LIGHTS", "LGTS"], "loaf"=>["LOAF", "LF"], "lf"=>["LOAF", "LF"], "lock"=>["LOCK", "LCK"], "lck"=>["LOCK", "LCK"], "locks"=>["LOCKS", "LCKS"], "lcks"=>["LOCKS", "LCKS"], "lodge"=>["LODGE", "LDG", "LDGE", "LODG"], "ldg"=>["LODGE", "LDG", "LDGE", "LODG"], "ldge"=>["LODGE", "LDG", "LDGE", "LODG"], "lodg"=>["LODGE", "LDG", "LDGE", "LODG"], "loop"=>["LOOP", "LOOPS"], "loops"=>["LOOP", "LOOPS"], "mall"=>["MALL"], "manor"=>["MANOR", "MNR"], "mnr"=>["MANOR", "MNR"], "manors"=>["MANORS", "MNRS"], "mnrs"=>["MANORS", "MNRS"], "meadow"=>["MEADOW", "MDW"], "mdw"=>["MEADOW", "MDW"], "meadows"=>["MEADOWS", "MDWS", "MEDOWS"], "mdws"=>["MEADOWS", "MDWS", "MEDOWS"], "medows"=>["MEADOWS", "MDWS", "MEDOWS"], "mews"=>["MEWS"], "mill"=>["MILL", "ML"], "ml"=>["MILL", "ML"], "mills"=>["MILLS", "MLS"], "mls"=>["MILLS", "MLS"], "mission"=>["MISSION", "MSN", "MISSN", "MSSN"], "msn"=>["MISSION", "MSN", "MISSN", "MSSN"], "missn"=>["MISSION", "MSN", "MISSN", "MSSN"], "mssn"=>["MISSION", "MSN", "MISSN", "MSSN"], "motorway"=>["MOTORWAY", "MTWY"], "mtwy"=>["MOTORWAY", "MTWY"], "mount"=>["MOUNT", "MNT", "MT"], "mnt"=>["MOUNT", "MNT", "MT"], "mt"=>["MOUNT", "MNT", "MT"], "mountain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountains"=>["MOUNTAINS", "MNTNS", "MTNS"], "mntns"=>["MOUNTAINS", "MNTNS", "MTNS"], "mtns"=>["MOUNTAINS", "MNTNS", "MTNS"], "neck"=>["NECK", "NCK"], "nck"=>["NECK", "NCK"], "orchard"=>["ORCHARD", "ORCH", "ORCHRD"], "orch"=>["ORCHARD", "ORCH", "ORCHRD"], "orchrd"=>["ORCHARD", "ORCH", "ORCHRD"], "oval"=>["OVAL", "OVL"], "ovl"=>["OVAL", "OVL"], "overpass"=>["OVERPASS", "OPAS"], "opas"=>["OVERPASS", "OPAS"], "park"=>["PARKS", "PARK"], "pk"=>["PARK", "PK", "PRK"], "prk"=>["PARK", "PK", "PRK"], "parks"=>["PARKS", "PARK"], "parkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkwy"=>["PARKWAYS", "PKWY", "PKWYS"], "parkwy"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pky"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "parkways"=>["PARKWAYS", "PKWY", "PKWYS"], "pkwys"=>["PARKWAYS", "PKWY", "PKWYS"], "pass"=>["PASS"], "passage"=>["PASSAGE", "PSGE"], "psge"=>["PASSAGE", "PSGE"], "path"=>["PATH", "PATHS"], "paths"=>["PATH", "PATHS"], "pike"=>["PIKE", "PIKES"], "pikes"=>["PIKE", "PIKES"], "pine"=>["PINE", "PNE"], "pne"=>["PINE", "PNE"], "pines"=>["PINES", "PNES"], "pnes"=>["PINES", "PNES"], "place"=>["PLACE", "PL"], "pl"=>["PLACE", "PL"], "plain"=>["PLAIN", "PLN"], "pln"=>["PLAIN", "PLN"], "plains"=>["PLAINS", "PLAINES", "PLNS"], "plaines"=>["PLAINS", "PLAINES", "PLNS"], "plns"=>["PLAINS", "PLAINES", "PLNS"], "plaza"=>["PLAZA", "PLZ", "PLZA"], "plz"=>["PLAZA", "PLZ", "PLZA"], "plza"=>["PLAZA", "PLZ", "PLZA"], "point"=>["POINT", "PT"], "pt"=>["POINT", "PT"], "points"=>["POINTS", "PTS"], "pts"=>["POINTS", "PTS"], "port"=>["PORT", "PRT"], "prt"=>["PORT", "PRT"], "ports"=>["PORTS", "PRTS"], "prts"=>["PORTS", "PRTS"], "prairie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "pr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prarie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "radial"=>["RADIAL", "RAD", "RADL", "RADIEL"], "rad"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radl"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radiel"=>["RADIAL", "RAD", "RADL", "RADIEL"], "ramp"=>["RAMP"], "ranch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "ranches"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnchs"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rapid"=>["RAPID", "RPD"], "rpd"=>["RAPID", "RPD"], "rapids"=>["RAPIDS", "RPDS"], "rpds"=>["RAPIDS", "RPDS"], "rest"=>["REST", "RST"], "rst"=>["REST", "RST"], "ridge"=>["RIDGE", "RDG", "RDGE"], "rdg"=>["RIDGE", "RDG", "RDGE"], "rdge"=>["RIDGE", "RDG", "RDGE"], "ridges"=>["RIDGES", "RDGS"], "rdgs"=>["RIDGES", "RDGS"], "river"=>["RIVER", "RIV", "RIVR", "RVR"], "riv"=>["RIVER", "RIV", "RIVR", "RVR"], "rivr"=>["RIVER", "RIV", "RIVR", "RVR"], "rvr"=>["RIVER", "RIV", "RIVR", "RVR"], "road"=>["ROAD", "RD"], "rd"=>["ROAD", "RD"], "roads"=>["ROADS", "RDS"], "rds"=>["ROADS", "RDS"], "route"=>["ROUTE", "RTE"], "rte"=>["ROUTE", "RTE"], "row"=>["ROW"], "rue"=>["RUE"], "run"=>["RUN"], "shoal"=>["SHOAL", "SHL"], "shl"=>["SHOAL", "SHL"], "shoals"=>["SHOALS", "SHLS"], "shls"=>["SHOALS", "SHLS"], "shore"=>["SHORE", "SHOAR", "SHR"], "shoar"=>["SHORE", "SHOAR", "SHR"], "shr"=>["SHORE", "SHOAR", "SHR"], "shores"=>["SHORES", "SHOARS", "SHRS"], "shoars"=>["SHORES", "SHOARS", "SHRS"], "shrs"=>["SHORES", "SHOARS", "SHRS"], "skyway"=>["SKYWAY", "SKWY"], "skwy"=>["SKYWAY", "SKWY"], "spring"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spg"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "sprng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "springs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spgs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "sprngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spur"=>["SPURS", "SPUR"], "spurs"=>["SPURS", "SPUR"], "square"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sq"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqr"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqre"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squ"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squares"=>["SQUARES", "SQRS", "SQS"], "sqrs"=>["SQUARES", "SQRS", "SQS"], "sqs"=>["SQUARES", "SQRS", "SQS"], "station"=>["STATION", "STA", "STATN", "STN"], "sta"=>["STATION", "STA", "STATN", "STN"], "statn"=>["STATION", "STA", "STATN", "STN"], "stn"=>["STATION", "STA", "STATN", "STN"], "stravenue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stra"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strav"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strave"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "straven"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stravn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvnue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stream"=>["STREAM", "STRM", "STREME"], "strm"=>["STREAM", "STRM", "STREME"], "streme"=>["STREAM", "STRM", "STREME"], "street"=>["STREET", "ST", "STR", "STRT"], "st"=>["STREET", "ST", "STR", "STRT"], "str"=>["STREET", "ST", "STR", "STRT"], "strt"=>["STREET", "ST", "STR", "STRT"], "streets"=>["STREETS", "STS"], "sts"=>["STREETS", "STS"], "summit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "smt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumitt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "terrace"=>["TERRACE", "TER", "TERR"], "ter"=>["TERRACE", "TER", "TERR"], "terr"=>["TERRACE", "TER", "TERR"], "throughway"=>["THROUGHWAY", "TRWY"], "trwy"=>["THROUGHWAY", "TRWY"], "trace"=>["TRACE", "TRCE", "TRACES"], "trce"=>["TRACE", "TRCE", "TRACES"], "traces"=>["TRACE", "TRCE", "TRACES"], "track"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trak"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "tracks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trk"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trafficway"=>["TRAFFICWAY", "TRFY"], "trfy"=>["TRAFFICWAY", "TRFY"], "trail"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tr"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trl"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trails"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trls"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tunnel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunls"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnels"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "turnpike"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpke"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "turnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "underpass"=>["UNDERPASS", "UPAS"], "upas"=>["UNDERPASS", "UPAS"], "union"=>["UNION", "UN"], "un"=>["UNION", "UN"], "unions"=>["UNIONS", "UNS"], "uns"=>["UNIONS", "UNS"], "valley"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vally"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vlly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "valleys"=>["VALLEYS", "VLYS"], "vlys"=>["VALLEYS", "VLYS"], "viaduct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "vdct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "via"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "viadct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "view"=>["VIEW", "VW"], "vw"=>["VIEW", "VW"], "views"=>["VIEWS", "VWS"], "vws"=>["VIEWS", "VWS"], "village"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vill"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vlg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villag"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villiage"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villages"=>["VILLAGES", "VLGS"], "vlgs"=>["VILLAGES", "VLGS"], "ville"=>["VILLE", "VL"], "vl"=>["VILLE", "VL"], "vista"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vis"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vist"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vst"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vsta"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "walk"=>["WALKS", "WALK"], "walks"=>["WALKS", "WALK"], "wall"=>["WALL"], "way"=>["WAY", "WY"], "wy"=>["WAY", "WY"], "ways"=>["WAYS"], "well"=>["WELL", "WL"], "wl"=>["WELL", "WL"], "wells"=>["WELLS", "WLS"], "wls"=>["WELLS", "WLS"]})
   
    source_elem = nil
    mls_source_elem = nil
    
    if source.is_a?(Hash)
      source_elem = source[:source]
      mls_source_elem = source[:mls_source]
    else
      source_elem = source
    end
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    Delayed::Worker.logger.debug " state comparing self:  #{attrs[:state]} = #{self.state} " 
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      raw_list_price = nil
       Delayed::Worker.logger.debug "mls number exists"
       
      found = self.properties.find_by_mls_number(attrs[:mls_number])  
      if !found && !attrs[:street_name].blank? 
        street_number_str = attrs[:street_number].blank? ? "street_number is null" : "street_number = '#{attrs[:street_number]}'"
        street_dir_str = attrs[:street_direction].blank? ? "street_direction is null" : "street_direction = '#{attrs[:street_direction]}'"
        
        suff_key = attrs[:street_name].to_s.split(" ").last
        if suff_key.in?(suffixes)
          
          street_name = attrs[:street_name].to_s.gsub(suff_key,"").gsub(/\s+/,"")
          tokens = []
          suffixes[suff_key].each do |suff|
            tokens << "street_name like '#{street_name} #{suff}%'"
          end
          # q1 = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql
          # Delayed::Worker.logger.debug "dedup sql: #{q1}"
           # Delayed::Worker.logger.debug "suff query #{self.properties.where("street_number = ? and " + street_dir_str + " and (" + tokens.join(" or ")  + "   ) and zip_code = ? and (replace(regex_replace('[a-zA-Z\-]','',tax_id),' ','')  = replace(regex_replace('[a-zA-Z\-]','',?),' ','')  ) ",attrs[:street_number],attrs[:zip_code],attrs[:tax_id]).order("id desc").to_sql}"
          # founds = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem).order("id desc")
          Delayed::Worker.logger.debug "dedup 1"
          founds = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc")
        else
          # q2 = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql
          # Delayed::Worker.logger.debug "dedup sql: #{q2}"
          # Delayed::Worker.logger.debug "suff query #{self.properties.where("street_number = ? and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ? and (replace(regex_replace('[a-zA-Z\-]','',tax_id),' ','')  = replace(regex_replace('[a-zA-Z\-]','',?),' ','')  ) ",attrs[:street_number],attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],attrs[:tax_id]).order("id desc").to_sql}"
          # founds = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem).order("id desc")
          Delayed::Worker.logger.debug "dedup 2"
          founds = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc")
        end 

        if founds.length > 0
          Delayed::Worker.logger.debug "## what is found in different source: #{found}  => #{attrs[:mls_number]}"
          found = founds.first
        end 
        
      end
      
      if update_only
        
        if found # a property is found
          Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}" if found.list_price
          Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw && raw[:list_price]
          Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw && raw[:list_price]
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
        end
      else
        if found # a property is found
          Delayed::Worker.logger.debug "~~sub_category: #{found.sub_category} attr sub_cat: #{attrs[:sub_category]}"
          if !(found.sub_category && (found.sub_category == attrs[:sub_category]))
            Delayed::Worker.logger.debug "duplicate record is entering"
            # Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}"
            # Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw
            # Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw
            
            if attrs && attrs[:list_price]
              attr_list_price = attrs[:list_price].to_s.gsub(/[\\z,]/, '').to_i
            end
            
            
            # Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
            # Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
            # if raw 
              # # changed_values = {}
              # # changed_values = changed_values.with_indifferent_access
              # found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
                # true
              # else
                # false
              # end
  #            
            # end
            if attrs && (!found.auto_updatable)
              # changed_values = {}
              # changed_values = changed_values.with_indifferent_access
              found.auto_updatable =  if (found.list_price.to_i != attr_list_price)
                true
              else
                false
              end
             
            end
            found.assign_attributes(attrs)
            found.mls_source = mls_source_elem
            
            if found.status_changed?
              if found.status_was == "pending" && found.status == "active"
                found.is_back_on_market = true
                found.back_on_market = attrs[:mls_record_modified]
              else
                found.is_back_on_market = nil
                found.back_on_market = nil  
              end            
            end
  
            if found.list_price_changed?
              
              if found.list_price_was > found.list_price 
                found.price_reduction = true
                found.back_on_market = attrs[:mls_record_modified]
              else
                found.price_reduction = nil
                found.back_on_market = nil
              end
            end
          
          end
          
        else  # no property found and not in hotsheet
          Delayed::Worker.logger.debug "creating new property record in market.import()"
          
          found = self.properties.new(attrs) # create new property record
          found.mls_source = mls_source_elem
          found.auto_updatable  = true
        end
      end 
    else # no mls number exists
      if ((attrs[:tax_id] && attrs[:zip_code]) || (attrs[:street_address] && attrs[:zip_code]) || 
        (attrs[:tax_id] && attrs[:county])  || 
        (attrs[:street_address] && attrs[:county] )) # a tax account id or both street_address+zip_code exists
        
        
        # look for properties with the same properties
        history = self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        history_desc_id =  self.properties.similar_desc_id(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        bathrooms = attrs[:bathrooms]
        attrs = attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
        # Delayed::Worker.logger.debug  "similar prop #{history}"
        # Delayed::Worker.logger.debug "similar prop  #{self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).order("id desc").all}"
        
        Delayed::Worker.logger.debug "ZzzZ similar prop #{history_desc_id}"
        if history.empty? # if no property found
          # create new property
          found = self.properties.new(attrs)
          
          
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
        elsif (!history_desc_id.first.status.blank? &&  history_desc_id.first.status.in?('off'))
          update_found =  history_desc_id.first
           # create new property
          # update_found = self.properties.new(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark update lookup"
          found = pshark.lookup(found)
          
          if found
          # Delayed::Worker.logger.debug "override attributes #{found["address"]} state: #{found["state"]}"
            temp_id = update_found.id
            # Delayed::Worker.logger.debug "update found object: #{update_found}"
            update_found.destroy
            found.id = temp_id
            found.detail.id = temp_id
            # found.attributes.each do |k,v|
              # Delayed::Worker.logger.debug "attr_name  #{k}"
              # Delayed::Worker.logger.debug "update found k #{k}  value #{update_found[k]}"
              # found.send(:"#{k}=",update_found[k]) unless (k.in?(%w{id created_at}) || update_found[k].nil?)
            # end
            Delayed::Worker.logger.debug "found address value:  #{found.address}"
            Delayed::Worker.logger.debug "found id value:  #{found.id}"
          
          end
          
          
          # found.override_attributes(update_found.to_h)
          
        elsif found = history.select{|p| p.mls_number.nil? }.first
          # Delayed::Worker.logger.debug "override no mls"
          # # a property with no mls_number found
          # # then override existing property
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        elsif history.first.status.in?('active', 'pending')
          # Delayed::Worker.logger.debug "override use first"
          # # if most recent history item is active or pending
          # # override existing item
          # found = history.first
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        else
          
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
          # Delayed::Worker.logger.debug "create new prop"
          # Delayed::Worker.logger.debug "#{history}"
          # # otherwise create new property record and use the appropriate property field data from history
          # history_attrs = history.first.active_attributes(delegate: PropertyDetail, calc: false, missing: false)
          # # filter out only attributes of interest
#           
          # # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
          #  --------------------------------- commented out ----------------------------------
          # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS.map { |fld| fld.to_sym  }).select {|k, v| !v.missing? }
          # history_extra_attrs = {}
#                    
          # %w{street_address address street_name street_number state}.each { |item| 
             # history_extra_attrs = history_extra_attrs.merge(item.to_sym => history.first[item.to_sym])
          # }
          # history_extra_attrs = history_extra_attrs.merge(:status => 'off')
          # found = self.properties.new(history_attrs.merge(attrs).merge(history_extra_attrs))
          #  --------------------------------- commented out ----------------------------------
          
          # merge history attrs with user attrs (superior) and create new record
          # found = self.properties.new(history_attrs.merge(attrs))
          
          
        end
      else
         Delayed::Worker.logger.debug "do nothing"
        # do nothing
      end
    end

    if found 
      
      Delayed::Worker.logger.debug "found true"
      # validate data quality on found property
      unless found.valid?
        # reset fields with errors
        Delayed::Worker.logger.debug "found error"
        found.errors.keys.each do |key| 
          Delayed::Worker.logger.debug "error key: #{key}"
          found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
        end
      end
      found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
      Delayed::Worker.logger.debug "found new record? #{found.new_record?}"
      Delayed::Worker.logger.debug "found id #{found.id}"
      found.data_source = source_elem if found.new_record?
      
      found.status = 'off' if found.mls_number.missing?
      found.data_updated_at = DateTime.now
      found.save
      
      
      
      found
    end
  end
  
  def import7(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing

    source_elem = nil
    mls_source_elem = nil
    
    if source.is_a?(Hash)
      source_elem = source[:source]
      mls_source_elem = source[:mls_source]
    else
      source_elem = source
    end

    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    
    # p "self state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      # look for property with mls number in market database 
      founds = self.properties.where(["(mls_number = ?)  ", attrs[:mls_number]]) rescue nil; 
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      
      if founds && (founds.length > 0)# a property is found
          
        for found in founds
          
            
            # found.assign_attributes(status: attrs[:status])
            # validate data quality on found property
            unless found.valid?
              # reset fields with errors
              found.errors.keys.each do |key| 
                found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
              end
            end
            found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
            if found.status != attrs[:status]
              puts "updated id: #{found.id} found status #{found.status} status #{attrs[:status]}"
              if (attrs[:status].in?(['active']) && !attrs[:listing_date].blank?)
  
                selected_fields = attrs.select{|x| ![:listing_date,:dom,:cdom].include?(x)}
              else
                selected_fields = attrs
              end
              found.assign_attributes(selected_fields)
            end
            found.data_source = source if found.new_record?
            found.status = 'off' if found.mls_number.missing?
            found.data_updated_at = DateTime.now
            found.save
          
          
          
        end
        
        return founds
      # else
#           
        # found = self.properties.new(attrs) # create new property record
#         
        # # validate data quality on found property
        # unless found.valid?
          # found.errors.keys.each do |key| 
            # found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
          # end
        # end
        # found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
        # found.data_source = source if found.new_record?
        # found.status = 'off' if found.mls_number.missing?
        # Delayed::Worker.logger.debug "inserted id: #{found.id}"
        # found.data_updated_at = DateTime.now
        # found.save
      end     
    end
  end
  
  def import8(source, attrs, raw={}, update_only=false)

    suffixes = HashWithCaseInsensitiveAccess.new( {"alley"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "allee"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "aly"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "ally"=>["ALLEY", "ALLEE", "ALY", "ALLY"], "annex"=>["ANNEX", "ANEX", "ANX"], "anex"=>["ANNEX", "ANEX", "ANX"], "anx"=>["ANNEX", "ANEX", "ANX"], "arcade"=>["ARCADE", "ARC"], "arc"=>["ARCADE", "ARC"], "avenue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "av"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "ave"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "aven"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avenu"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avn"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "avnue"=>["AVENUE", "AV", "AVE", "AVEN", "AVENU", "AVN", "AVNUE"], "bayoo"=>["BAYOO", "BYU", "BAYOU"], "byu"=>["BAYOO", "BYU", "BAYOU"], "bayou"=>["BAYOO", "BYU", "BAYOU"], "beach"=>["BEACH", "BCH"], "bch"=>["BEACH", "BCH"], "bend"=>["BEND", "BND"], "bnd"=>["BEND", "BND"], "bluff"=>["BLUFF", "BLF", "BLUF"], "blf"=>["BLUFF", "BLF", "BLUF"], "bluf"=>["BLUFF", "BLF", "BLUF"], "bluffs"=>["BLUFFS", "BLFS"], "blfs"=>["BLUFFS", "BLFS"], "bottom"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bot"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "btm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "bottm"=>["BOTTOM", "BOT", "BTM", "BOTTM"], "boulevard"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "blvd"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boul"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "boulv"=>["BOULEVARD", "BLVD", "BOUL", "BOULV"], "branch"=>["BRANCH", "BR", "BRNCH"], "br"=>["BRANCH", "BR", "BRNCH"], "brnch"=>["BRANCH", "BR", "BRNCH"], "bridge"=>["BRIDGE", "BRDGE", "BRG"], "brdge"=>["BRIDGE", "BRDGE", "BRG"], "brg"=>["BRIDGE", "BRDGE", "BRG"], "brook"=>["BROOK", "BRK"], "brk"=>["BROOK", "BRK"], "brooks"=>["BROOKS", "BRKS"], "brks"=>["BROOKS", "BRKS"], "burg"=>["BURG", "BG"], "bg"=>["BURG", "BG"], "burgs"=>["BURGS", "BGS"], "bgs"=>["BURGS", "BGS"], "bypass"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byp"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypa"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "bypas"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "byps"=>["BYPASS", "BYP", "BYPA", "BYPAS", "BYPS"], "camp"=>["CAMP", "CP", "CMP"], "cp"=>["CAMP", "CP", "CMP"], "cmp"=>["CAMP", "CP", "CMP"], "canyon"=>["CANYON", "CANYN", "CYN", "CNYN"], "canyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cnyn"=>["CANYON", "CANYN", "CYN", "CNYN"], "cape"=>["CAPE", "CPE"], "cpe"=>["CAPE", "CPE"], "causeway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "cswy"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "causway"=>["CAUSEWAY", "CSWY", "CAUSWAY"], "center"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cen"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "ctr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cent"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centre"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cnter"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "cntr"=>["CENTER", "CEN", "CTR", "CENT", "CENTR", "CENTRE", "CNTER", "CNTR"], "centers"=>["CENTERS", "CTRS"], "ctrs"=>["CENTERS", "CTRS"], "circle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "cir"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circ"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcl"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "crcle"=>["CIRCLE", "CIR", "CIRC", "CIRCL", "CRCL", "CRCLE"], "circles"=>["CIRCLES", "CIRS"], "cirs"=>["CIRCLES", "CIRS"], "cliff"=>["CLIFF", "CLF"], "clf"=>["CLIFF", "CLF"], "cliffs"=>["CLIFFS", "CLFS"], "clfs"=>["CLIFFS", "CLFS"], "club"=>["CLUB", "CLB"], "clb"=>["CLUB", "CLB"], "common"=>["COMMON", "CMN"], "cmn"=>["COMMON", "CMN"], "corner"=>["CORNER", "COR"], "cor"=>["CORNER", "COR"], "corners"=>["CORNERS", "CORS"], "cors"=>["CORNERS", "CORS"], "course"=>["COURSE", "CRSE"], "crse"=>["COURSE", "CRSE"], "court"=>["COURT", "CT", "CRT"], "ct"=>["COURTS", "CTS", "CT"], "crt"=>["COURT", "CT", "CRT"], "courts"=>["COURTS", "CTS", "CT"], "cts"=>["COURTS", "CTS", "CT"], "cove"=>["COVE", "CV"], "cv"=>["COVE", "CV"], "coves"=>["COVES", "CVS"], "cvs"=>["COVES", "CVS"], "creek"=>["CREEK", "CK", "CRK", "CR"], "ck"=>["CREEK", "CK", "CRK", "CR"], "crk"=>["CREEK", "CK", "CRK", "CR"], "cr"=>["CREEK", "CK", "CRK", "CR"], "crescent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crecent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cres"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "cresent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crscnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsent"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crsnt"=>["CRESCENT", "CRECENT", "CRES", "CRESENT", "CRSCNT", "CRSENT", "CRSNT"], "crest"=>["CREST", "CRST"], "crst"=>["CREST", "CRST"], "crossing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "xing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssing"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crssng"=>["CROSSING", "XING", "CRSSING", "CRSSNG"], "crossroad"=>["CROSSROAD", "XRD"], "xrd"=>["CROSSROAD", "XRD"], "curve"=>["CURVE", "CURV"], "curv"=>["CURVE", "CURV"], "dale"=>["DALE", "DL"], "dl"=>["DALE", "DL"], "dam"=>["DAM", "DM"], "dm"=>["DAM", "DM"], "divide"=>["DIVIDE", "DIV", "DV", "DVD"], "div"=>["DIVIDE", "DIV", "DV", "DVD"], "dv"=>["DIVIDE", "DIV", "DV", "DVD"], "dvd"=>["DIVIDE", "DIV", "DV", "DVD"], "drive"=>["DRIVE", "DR", "DRIV", "DRV"], "dr"=>["DRIVE", "DR", "DRIV", "DRV"], "driv"=>["DRIVE", "DR", "DRIV", "DRV"], "drv"=>["DRIVE", "DR", "DRIV", "DRV"], "drives"=>["DRIVES", "DRS"], "drs"=>["DRIVES", "DRS"], "estate"=>["ESTATE", "EST"], "est"=>["ESTATE", "EST"], "estates"=>["ESTATES", "ESTS"], "ests"=>["ESTATES", "ESTS"], "expressway"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "exp"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expy"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expr"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "express"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "expw"=>["EXPRESSWAY", "EXP", "EXPY", "EXPR", "EXPRESS", "EXPW"], "extension"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "ext"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extnsn"=>["EXTENSION", "EXT", "EXTN", "EXTNSN"], "extensions"=>["EXTENSIONS", "EXTS"], "exts"=>["EXTENSIONS", "EXTS"], "fall"=>["FALL"], "falls"=>["FALLS", "FLS"], "fls"=>["FALLS", "FLS"], "ferry"=>["FERRY", "FRY", "FRRY"], "fry"=>["FERRY", "FRY", "FRRY"], "frry"=>["FERRY", "FRY", "FRRY"], "field"=>["FIELD", "FLD"], "fld"=>["FIELD", "FLD"], "fields"=>["FIELDS", "FLDS"], "flds"=>["FIELDS", "FLDS"], "flat"=>["FLAT", "FLT"], "flt"=>["FLAT", "FLT"], "flats"=>["FLATS", "FLTS"], "flts"=>["FLATS", "FLTS"], "ford"=>["FORD", "FRD"], "frd"=>["FORD", "FRD"], "fords"=>["FORDS", "FRDS"], "frds"=>["FORDS", "FRDS"], "forest"=>["FOREST", "FRST", "FORESTS"], "frst"=>["FOREST", "FRST", "FORESTS"], "forests"=>["FOREST", "FRST", "FORESTS"], "forge"=>["FORGE", "FORG", "FRG"], "forg"=>["FORGE", "FORG", "FRG"], "frg"=>["FORGE", "FORG", "FRG"], "forges"=>["FORGES", "FRGS"], "frgs"=>["FORGES", "FRGS"], "fork"=>["FORK", "FRK"], "frk"=>["FORK", "FRK"], "forks"=>["FORKS", "FRKS"], "frks"=>["FORKS", "FRKS"], "fort"=>["FORT", "FT", "FRT"], "ft"=>["FORT", "FT", "FRT"], "frt"=>["FORT", "FT", "FRT"], "freeway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "fwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "freewy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frway"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "frwy"=>["FREEWAY", "FWY", "FREEWY", "FRWAY", "FRWY"], "garden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grden"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "grdn"=>["GARDEN", "GDN", "GARDN", "GRDEN", "GRDN"], "gardens"=>["GARDENS", "GDNS", "GRDNS"], "gdns"=>["GARDENS", "GDNS", "GRDNS"], "grdns"=>["GARDENS", "GDNS", "GRDNS"], "gateway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtwy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatewy"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gatway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "gtway"=>["GATEWAY", "GTWY", "GATEWY", "GATWAY", "GTWAY"], "glen"=>["GLEN", "GLN"], "gln"=>["GLEN", "GLN"], "glens"=>["GLENS", "GLNS"], "glns"=>["GLENS", "GLNS"], "green"=>["GREEN", "GRN"], "grn"=>["GREEN", "GRN"], "greens"=>["GREENS", "GRNS"], "grns"=>["GREENS", "GRNS"], "grove"=>["GROVE", "GROV", "GRV"], "grov"=>["GROVE", "GROV", "GRV"], "grv"=>["GROVE", "GROV", "GRV"], "groves"=>["GROVES", "GRVS"], "grvs"=>["GROVES", "GRVS"], "harbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harb"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbr"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "hrbor"=>["HARBOR", "HARB", "HBR", "HARBR", "HRBOR"], "harbors"=>["HARBORS", "HBRS"], "hbrs"=>["HARBORS", "HBRS"], "haven"=>["HAVEN", "HVN", "HAVN"], "hvn"=>["HAVEN", "HVN", "HAVN"], "havn"=>["HAVEN", "HVN", "HAVN"], "heights"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "height"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "hgts"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "ht"=>["HEIGHTS", "HEIGHT", "HTS", "HGTS", "HT"], "highway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "highwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hiwy"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hway"=>["HIGHWAY", "HWY", "HIGHWY", "HIWAY", "HIWY", "HWAY"], "hill"=>["HILL", "HL"], "hl"=>["HILL", "HL"], "hills"=>["HILLS", "HLS"], "hls"=>["HILLS", "HLS"], "hollow"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hllw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holw"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "hollows"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "holws"=>["HOLLOW", "HLLW", "HOLW", "HOLLOWS", "HOLWS"], "inlet"=>["INLET", "INLT"], "inlt"=>["INLET", "INLT"], "island"=>["ISLAND", "IS", "ISLND"], "is"=>["ISLAND", "IS", "ISLND"], "islnd"=>["ISLAND", "IS", "ISLND"], "islands"=>["ISLANDS", "ISS", "ISLNDS"], "iss"=>["ISLANDS", "ISS", "ISLNDS"], "islnds"=>["ISLANDS", "ISS", "ISLNDS"], "isle"=>["ISLE", "ISLES"], "isles"=>["ISLE", "ISLES"], "junction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jct"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jction"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "jctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctn"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "juncton"=>["JUNCTION", "JCT", "JCTION", "JCTN", "JUNCTN", "JUNCTON"], "junctions"=>["JUNCTIONS", "JCTNS", "JCTS"], "jctns"=>["JUNCTIONS", "JCTNS", "JCTS"], "jcts"=>["JUNCTIONS", "JCTNS", "JCTS"], "key"=>["KEY", "KY"], "ky"=>["KEY", "KY"], "keys"=>["KEYS", "KYS"], "kys"=>["KEYS", "KYS"], "knoll"=>["KNOLL", "KNL", "KNOL"], "knl"=>["KNOLL", "KNL", "KNOL"], "knol"=>["KNOLL", "KNL", "KNOL"], "knolls"=>["KNOLLS", "KNLS"], "knls"=>["KNOLLS", "KNLS"], "lake"=>["LAKE", "LK"], "lk"=>["LAKE", "LK"], "lakes"=>["LAKES", "LKS"], "lks"=>["LAKES", "LKS"], "land"=>["LAND"], "landing"=>["LANDING", "LNDG", "LNDNG"], "lndg"=>["LANDING", "LNDG", "LNDNG"], "lndng"=>["LANDING", "LNDG", "LNDNG"], "lane"=>["LANE", "LA", "LN", "LANES"], "la"=>["LANE", "LA", "LN", "LANES"], "ln"=>["LANE", "LA", "LN", "LANES"], "lanes"=>["LANE", "LA", "LN", "LANES"], "light"=>["LIGHT", "LGT"], "lgt"=>["LIGHT", "LGT"], "lights"=>["LIGHTS", "LGTS"], "lgts"=>["LIGHTS", "LGTS"], "loaf"=>["LOAF", "LF"], "lf"=>["LOAF", "LF"], "lock"=>["LOCK", "LCK"], "lck"=>["LOCK", "LCK"], "locks"=>["LOCKS", "LCKS"], "lcks"=>["LOCKS", "LCKS"], "lodge"=>["LODGE", "LDG", "LDGE", "LODG"], "ldg"=>["LODGE", "LDG", "LDGE", "LODG"], "ldge"=>["LODGE", "LDG", "LDGE", "LODG"], "lodg"=>["LODGE", "LDG", "LDGE", "LODG"], "loop"=>["LOOP", "LOOPS"], "loops"=>["LOOP", "LOOPS"], "mall"=>["MALL"], "manor"=>["MANOR", "MNR"], "mnr"=>["MANOR", "MNR"], "manors"=>["MANORS", "MNRS"], "mnrs"=>["MANORS", "MNRS"], "meadow"=>["MEADOW", "MDW"], "mdw"=>["MEADOW", "MDW"], "meadows"=>["MEADOWS", "MDWS", "MEDOWS"], "mdws"=>["MEADOWS", "MDWS", "MEDOWS"], "medows"=>["MEADOWS", "MDWS", "MEDOWS"], "mews"=>["MEWS"], "mill"=>["MILL", "ML"], "ml"=>["MILL", "ML"], "mills"=>["MILLS", "MLS"], "mls"=>["MILLS", "MLS"], "mission"=>["MISSION", "MSN", "MISSN", "MSSN"], "msn"=>["MISSION", "MSN", "MISSN", "MSSN"], "missn"=>["MISSION", "MSN", "MISSN", "MSSN"], "mssn"=>["MISSION", "MSN", "MISSN", "MSSN"], "motorway"=>["MOTORWAY", "MTWY"], "mtwy"=>["MOTORWAY", "MTWY"], "mount"=>["MOUNT", "MNT", "MT"], "mnt"=>["MOUNT", "MNT", "MT"], "mt"=>["MOUNT", "MNT", "MT"], "mountain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntain"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mntn"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mtin"=>["MOUNTAIN", "MNTAIN", "MTN", "MNTN", "MOUNTIN", "MTIN"], "mountains"=>["MOUNTAINS", "MNTNS", "MTNS"], "mntns"=>["MOUNTAINS", "MNTNS", "MTNS"], "mtns"=>["MOUNTAINS", "MNTNS", "MTNS"], "neck"=>["NECK", "NCK"], "nck"=>["NECK", "NCK"], "orchard"=>["ORCHARD", "ORCH", "ORCHRD"], "orch"=>["ORCHARD", "ORCH", "ORCHRD"], "orchrd"=>["ORCHARD", "ORCH", "ORCHRD"], "oval"=>["OVAL", "OVL"], "ovl"=>["OVAL", "OVL"], "overpass"=>["OVERPASS", "OPAS"], "opas"=>["OVERPASS", "OPAS"], "park"=>["PARKS", "PARK"], "pk"=>["PARK", "PK", "PRK"], "prk"=>["PARK", "PK", "PRK"], "parks"=>["PARKS", "PARK"], "parkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkwy"=>["PARKWAYS", "PKWY", "PKWYS"], "parkwy"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pkway"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "pky"=>["PARKWAY", "PKWY", "PARKWY", "PKWAY", "PKY"], "parkways"=>["PARKWAYS", "PKWY", "PKWYS"], "pkwys"=>["PARKWAYS", "PKWY", "PKWYS"], "pass"=>["PASS"], "passage"=>["PASSAGE", "PSGE"], "psge"=>["PASSAGE", "PSGE"], "path"=>["PATH", "PATHS"], "paths"=>["PATH", "PATHS"], "pike"=>["PIKE", "PIKES"], "pikes"=>["PIKE", "PIKES"], "pine"=>["PINE", "PNE"], "pne"=>["PINE", "PNE"], "pines"=>["PINES", "PNES"], "pnes"=>["PINES", "PNES"], "place"=>["PLACE", "PL"], "pl"=>["PLACE", "PL"], "plain"=>["PLAIN", "PLN"], "pln"=>["PLAIN", "PLN"], "plains"=>["PLAINS", "PLAINES", "PLNS"], "plaines"=>["PLAINS", "PLAINES", "PLNS"], "plns"=>["PLAINS", "PLAINES", "PLNS"], "plaza"=>["PLAZA", "PLZ", "PLZA"], "plz"=>["PLAZA", "PLZ", "PLZA"], "plza"=>["PLAZA", "PLZ", "PLZA"], "point"=>["POINT", "PT"], "pt"=>["POINT", "PT"], "points"=>["POINTS", "PTS"], "pts"=>["POINTS", "PTS"], "port"=>["PORT", "PRT"], "prt"=>["PORT", "PRT"], "ports"=>["PORTS", "PRTS"], "prts"=>["PORTS", "PRTS"], "prairie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "pr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prarie"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "prr"=>["PRAIRIE", "PR", "PRARIE", "PRR"], "radial"=>["RADIAL", "RAD", "RADL", "RADIEL"], "rad"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radl"=>["RADIAL", "RAD", "RADL", "RADIEL"], "radiel"=>["RADIAL", "RAD", "RADL", "RADIEL"], "ramp"=>["RAMP"], "ranch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnch"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "ranches"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rnchs"=>["RANCH", "RNCH", "RANCHES", "RNCHS"], "rapid"=>["RAPID", "RPD"], "rpd"=>["RAPID", "RPD"], "rapids"=>["RAPIDS", "RPDS"], "rpds"=>["RAPIDS", "RPDS"], "rest"=>["REST", "RST"], "rst"=>["REST", "RST"], "ridge"=>["RIDGE", "RDG", "RDGE"], "rdg"=>["RIDGE", "RDG", "RDGE"], "rdge"=>["RIDGE", "RDG", "RDGE"], "ridges"=>["RIDGES", "RDGS"], "rdgs"=>["RIDGES", "RDGS"], "river"=>["RIVER", "RIV", "RIVR", "RVR"], "riv"=>["RIVER", "RIV", "RIVR", "RVR"], "rivr"=>["RIVER", "RIV", "RIVR", "RVR"], "rvr"=>["RIVER", "RIV", "RIVR", "RVR"], "road"=>["ROAD", "RD"], "rd"=>["ROAD", "RD"], "roads"=>["ROADS", "RDS"], "rds"=>["ROADS", "RDS"], "route"=>["ROUTE", "RTE"], "rte"=>["ROUTE", "RTE"], "row"=>["ROW"], "rue"=>["RUE"], "run"=>["RUN"], "shoal"=>["SHOAL", "SHL"], "shl"=>["SHOAL", "SHL"], "shoals"=>["SHOALS", "SHLS"], "shls"=>["SHOALS", "SHLS"], "shore"=>["SHORE", "SHOAR", "SHR"], "shoar"=>["SHORE", "SHOAR", "SHR"], "shr"=>["SHORE", "SHOAR", "SHR"], "shores"=>["SHORES", "SHOARS", "SHRS"], "shoars"=>["SHORES", "SHOARS", "SHRS"], "shrs"=>["SHORES", "SHOARS", "SHRS"], "skyway"=>["SKYWAY", "SKWY"], "skwy"=>["SKYWAY", "SKWY"], "spring"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spg"=>["SPRING", "SPG", "SPNG", "SPRNG"], "spng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "sprng"=>["SPRING", "SPG", "SPNG", "SPRNG"], "springs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spgs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "sprngs"=>["SPRINGS", "SPGS", "SPNGS", "SPRNGS"], "spur"=>["SPURS", "SPUR"], "spurs"=>["SPURS", "SPUR"], "square"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sq"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqr"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "sqre"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squ"=>["SQUARE", "SQ", "SQR", "SQRE", "SQU"], "squares"=>["SQUARES", "SQRS", "SQS"], "sqrs"=>["SQUARES", "SQRS", "SQS"], "sqs"=>["SQUARES", "SQRS", "SQS"], "station"=>["STATION", "STA", "STATN", "STN"], "sta"=>["STATION", "STA", "STATN", "STN"], "statn"=>["STATION", "STA", "STATN", "STN"], "stn"=>["STATION", "STA", "STATN", "STN"], "stravenue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stra"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strav"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strave"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "straven"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stravn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvn"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "strvnue"=>["STRAVENUE", "STRA", "STRAV", "STRAVE", "STRAVEN", "STRAVN", "STRVN", "STRVNUE"], "stream"=>["STREAM", "STRM", "STREME"], "strm"=>["STREAM", "STRM", "STREME"], "streme"=>["STREAM", "STRM", "STREME"], "street"=>["STREET", "ST", "STR", "STRT"], "st"=>["STREET", "ST", "STR", "STRT"], "str"=>["STREET", "ST", "STR", "STRT"], "strt"=>["STREET", "ST", "STR", "STRT"], "streets"=>["STREETS", "STS"], "sts"=>["STREETS", "STS"], "summit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "smt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumit"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "sumitt"=>["SUMMIT", "SMT", "SUMIT", "SUMITT"], "terrace"=>["TERRACE", "TER", "TERR"], "ter"=>["TERRACE", "TER", "TERR"], "terr"=>["TERRACE", "TER", "TERR"], "throughway"=>["THROUGHWAY", "TRWY"], "trwy"=>["THROUGHWAY", "TRWY"], "trace"=>["TRACE", "TRCE", "TRACES"], "trce"=>["TRACE", "TRCE", "TRACES"], "traces"=>["TRACE", "TRCE", "TRACES"], "track"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trak"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "tracks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trk"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trks"=>["TRACK", "TRAK", "TRACKS", "TRK", "TRKS"], "trafficway"=>["TRAFFICWAY", "TRFY"], "trfy"=>["TRAFFICWAY", "TRFY"], "trail"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tr"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trl"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trails"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "trls"=>["TRAIL", "TR", "TRL", "TRAILS", "TRLS"], "tunnel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunel"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunls"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnels"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "tunnl"=>["TUNNEL", "TUNEL", "TUNL", "TUNLS", "TUNNELS", "TUNNL"], "turnpike"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "tpke"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "trpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "turnpk"=>["TURNPIKE", "TPK", "TPKE", "TRNPK", "TRPK", "TURNPK"], "underpass"=>["UNDERPASS", "UPAS"], "upas"=>["UNDERPASS", "UPAS"], "union"=>["UNION", "UN"], "un"=>["UNION", "UN"], "unions"=>["UNIONS", "UNS"], "uns"=>["UNIONS", "UNS"], "valley"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vally"=>["VALLEY", "VLY", "VALLY", "VLLY"], "vlly"=>["VALLEY", "VLY", "VALLY", "VLLY"], "valleys"=>["VALLEYS", "VLYS"], "vlys"=>["VALLEYS", "VLYS"], "viaduct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "vdct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "via"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "viadct"=>["VIADUCT", "VDCT", "VIA", "VIADCT"], "view"=>["VIEW", "VW"], "vw"=>["VIEW", "VW"], "views"=>["VIEWS", "VWS"], "vws"=>["VIEWS", "VWS"], "village"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vill"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "vlg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villag"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villg"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villiage"=>["VILLAGE", "VILL", "VLG", "VILLAG", "VILLG", "VILLIAGE"], "villages"=>["VILLAGES", "VLGS"], "vlgs"=>["VILLAGES", "VLGS"], "ville"=>["VILLE", "VL"], "vl"=>["VILLE", "VL"], "vista"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vis"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vist"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vst"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "vsta"=>["VISTA", "VIS", "VIST", "VST", "VSTA"], "walk"=>["WALKS", "WALK"], "walks"=>["WALKS", "WALK"], "wall"=>["WALL"], "way"=>["WAY", "WY"], "wy"=>["WAY", "WY"], "ways"=>["WAYS"], "well"=>["WELL", "WL"], "wl"=>["WELL", "WL"], "wells"=>["WELLS", "WLS"], "wls"=>["WELLS", "WLS"]})

    source_elem = nil
    mls_source_elem = nil
    
    if source.is_a?(Hash)
      source_elem = source[:source]
      mls_source_elem = source[:mls_source]
    else
      source_elem = source
    end
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self state: #{self.state}"
    # discard records upon conditions
    Delayed::Worker.logger.debug " state comparing self:  #{attrs[:state]} = #{self.state} " 
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      raw_list_price = nil
       Delayed::Worker.logger.debug "mls number exists"
      # look for property with mls number in market database 
      found = self.properties.find_by_mls_number(attrs[:mls_number])  
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      Delayed::Worker.logger.debug "## what is found #{found}  => #{attrs[:mls_number]}"
      if !found && !attrs[:street_name].blank? 
        street_number_str = attrs[:street_number].blank? ? "street_number is null" : "street_number = '#{attrs[:street_number]}'"
        street_dir_str = attrs[:street_direction].blank? ? "street_direction is null" : "street_direction = '#{attrs[:street_direction]}'"
        
        suff_key = attrs[:street_name].to_s.split(" ").last
        if suff_key.in?(suffixes)
          
          street_name = attrs[:street_name].to_s.gsub(suff_key,"").gsub(/\s+/,"")
          tokens = []
          suffixes[suff_key].each do |suff|
            tokens << "street_name like '#{street_name} #{suff}%'"
          end
          # q1 = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql
          # Delayed::Worker.logger.debug "dedup sql: #{q1}"
           # Delayed::Worker.logger.debug "suff query #{self.properties.where("street_number = ? and " + street_dir_str + " and (" + tokens.join(" or ")  + "   ) and zip_code = ? and (replace(regex_replace('[a-zA-Z\-]','',tax_id),' ','')  = replace(regex_replace('[a-zA-Z\-]','',?),' ','')  ) ",attrs[:street_number],attrs[:zip_code],attrs[:tax_id]).order("id desc").to_sql}"
          # founds = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem).order("id desc")
          Delayed::Worker.logger.debug "dedup 1"
          Delayed::Worker.logger.debug "dedep 1 sql: #{self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql}"
          founds = self.properties.where(street_number_str + " and " + street_dir_str + " and ((" + tokens.join(" or ")  + "   )  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 ))   and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?) ",attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%', attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc")
        else
          # q2 = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql
          # Delayed::Worker.logger.debug "dedup sql: #{q2}"
          # Delayed::Worker.logger.debug "suff query #{self.properties.where("street_number = ? and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ? and (replace(regex_replace('[a-zA-Z\-]','',tax_id),' ','')  = replace(regex_replace('[a-zA-Z\-]','',?),' ','')  ) ",attrs[:street_number],attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],attrs[:tax_id]).order("id desc").to_sql}"
          # founds = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem).order("id desc")
          Delayed::Worker.logger.debug "dedup 2"
          Delayed::Worker.logger.debug "dedep 2 sql: #{self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc").to_sql}"
          founds = self.properties.where(street_number_str + "  and " + street_dir_str + " and (street_name like ?  or  (locate(street_name,?) > 0 and length(street_name)/length(?) > 0.6 )  ) and zip_code = ?  and (mls_source <> ?) and (replace(county,' ','') like ?) and (category = ?) and ((square_feet between ? and ?) OR (? = '')) and (year_built = ?)  ",attrs[:street_name]+'%',attrs[:street_name],attrs[:street_name],attrs[:zip_code],mls_source_elem,attrs[:county].to_s.gsub(' ','')[0,3]+'%',attrs[:category],(attrs[:square_feet].to_i - (attrs[:square_feet].to_i * 0.60)),(attrs[:square_feet].to_i + (attrs[:square_feet].to_i * 0.60)),attrs[:square_feet].to_s,attrs[:year_built]).order("id desc")
        end 

        if founds.length > 0
          Delayed::Worker.logger.debug "## what is found in different source: #{found}  => #{attrs[:mls_number]}"
          found = founds.first
        end 
        
      end
      
      if update_only
        
        if found # a property is found
          Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}" if found.list_price
          Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw && raw[:list_price]
          Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw && raw[:list_price]
          
          if raw && raw[:list_price]
            raw_list_price = raw[:list_price].gsub(/[\\z,]/, '').to_i
          end
          Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          if raw && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
        end
      else
        if found # a property is found
          
          
          Delayed::Worker.logger.debug "duplicate record is entering"
          # Delayed::Worker.logger.debug "org list price #{found.list_price.to_i}"
          # Delayed::Worker.logger.debug "raw list price #{raw[:list_price]}"  if raw
          # Delayed::Worker.logger.debug "raw list price (int) #{raw[:list_price].to_i}"  if raw
          
          if attrs && attrs[:list_price]
            attr_list_price = attrs[:list_price].to_s.gsub(/[\\z,]/, '').to_i
          end
          
          
          # Delayed::Worker.logger.debug "raw list price (int) #{raw_list_price}"  if raw
          # Delayed::Worker.logger.debug "lp org == lp raw? #{found.list_price.to_i == raw_list_price}"  if raw
          # if raw 
            # # changed_values = {}
            # # changed_values = changed_values.with_indifferent_access
            # found.auto_updatable =  if (found.list_price.to_i != raw_list_price)
              # true
            # else
              # false
            # end
#            
          # end
          if attrs && (!found.auto_updatable)
            # changed_values = {}
            # changed_values = changed_values.with_indifferent_access
            found.auto_updatable =  if (found.list_price.to_i != attr_list_price)
              true
            else
              false
            end
           
          end
          found.assign_attributes(attrs)
          found.mls_source = mls_source_elem

          if found.status_changed?
            if found.status_was == "pending" && found.status == "active"
              found.is_back_on_market = true
              found.back_on_market = attrs[:mls_record_modified]
            else
              found.is_back_on_market = nil
              found.back_on_market = nil  
            end            
          end

          if found.list_price_changed?
            
            if found.list_price_was > found.list_price 
              found.price_reduction = true
              found.back_on_market = attrs[:mls_record_modified]
            else
              found.price_reduction = nil
              found.back_on_market = nil
            end
          end
          
        
        else  # no property found and not in hotsheet
          Delayed::Worker.logger.debug "creating new property record in market.import()"
          
          found = self.properties.new(attrs) # create new property record
          found.mls_source = mls_source_elem
          found.auto_updatable  = true
        end
      end 
    else # no mls number exists
      if ((attrs[:tax_id] && attrs[:zip_code]) || (attrs[:street_address] && attrs[:zip_code]) || 
        (attrs[:tax_id] && attrs[:county])  || 
        (attrs[:street_address] && attrs[:county] )) # a tax account id or both street_address+zip_code exists
        
        
        # look for properties with the same properties
        history = self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        history_desc_id =  self.properties.similar_desc_id(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).all
        bathrooms = attrs[:bathrooms]
        attrs = attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
        # Delayed::Worker.logger.debug  "similar prop #{history}"
        # Delayed::Worker.logger.debug "similar prop  #{self.properties.similar(attrs[:tax_id], attrs[:street_address], attrs[:zip_code]).order("id desc").all}"
        
        Delayed::Worker.logger.debug "ZzzZ similar prop #{history_desc_id}"
        if history.empty? # if no property found
          # create new property
          found = self.properties.new(attrs)
          
          
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
        elsif (!history_desc_id.first.status.blank? &&  history_desc_id.first.status.in?('off'))
          update_found =  history_desc_id.first
           # create new property
          # update_found = self.properties.new(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark update lookup"
          found = pshark.lookup(found)
          
          if found
          # Delayed::Worker.logger.debug "override attributes #{found["address"]} state: #{found["state"]}"
            temp_id = update_found.id
            # Delayed::Worker.logger.debug "update found object: #{update_found}"
            update_found.destroy
            found.id = temp_id
            found.detail.id = temp_id
            # found.attributes.each do |k,v|
              # Delayed::Worker.logger.debug "attr_name  #{k}"
              # Delayed::Worker.logger.debug "update found k #{k}  value #{update_found[k]}"
              # found.send(:"#{k}=",update_found[k]) unless (k.in?(%w{id created_at}) || update_found[k].nil?)
            # end
            Delayed::Worker.logger.debug "found address value:  #{found.address}"
            Delayed::Worker.logger.debug "found id value:  #{found.id}"
          
          end
          
          
          # found.override_attributes(update_found.to_h)
          
        elsif found = history.select{|p| p.mls_number.nil? }.first
          # Delayed::Worker.logger.debug "override no mls"
          # # a property with no mls_number found
          # # then override existing property
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        elsif history.first.status.in?('active', 'pending')
          # Delayed::Worker.logger.debug "override use first"
          # # if most recent history item is active or pending
          # # override existing item
          # found = history.first
          # found.override_attributes(attrs)
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
        else
          
          found = self.properties.new(attrs)
          
          if bathrooms
            Delayed::Worker.logger.debug ">>override bathrooms"
            found.override_attributes(bathrooms: bathrooms)
            Delayed::Worker.logger.debug ">>set _override attrib"
            found._overrides[:bathrooms] = true
          end 
          
          # and use propertyshark to lookup missing qualities
          Delayed::Worker.logger.debug  "pshark lookup #{attrs}"
          
          found = pshark.lookup(found)
          
          # Delayed::Worker.logger.debug "create new prop"
          # Delayed::Worker.logger.debug "#{history}"
          # # otherwise create new property record and use the appropriate property field data from history
          # history_attrs = history.first.active_attributes(delegate: PropertyDetail, calc: false, missing: false)
          # # filter out only attributes of interest
#           
          # # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS).select {|k, v| !v.missing? }
          #  --------------------------------- commented out ----------------------------------
          # history_attrs = history_attrs.extract(*Property::IMPORT_FIELDS.map { |fld| fld.to_sym  }).select {|k, v| !v.missing? }
          # history_extra_attrs = {}
#                    
          # %w{street_address address street_name street_number state}.each { |item| 
             # history_extra_attrs = history_extra_attrs.merge(item.to_sym => history.first[item.to_sym])
          # }
          # history_extra_attrs = history_extra_attrs.merge(:status => 'off')
          # found = self.properties.new(history_attrs.merge(attrs).merge(history_extra_attrs))
          #  --------------------------------- commented out ----------------------------------
          
          # merge history attrs with user attrs (superior) and create new record
          # found = self.properties.new(history_attrs.merge(attrs))
          
          
        end
      else
         Delayed::Worker.logger.debug "do nothing"
        # do nothing
      end
    end

    if found 
      
      Delayed::Worker.logger.debug "found true"
      # validate data quality on found property
      unless found.valid?
        # reset fields with errors
        Delayed::Worker.logger.debug "found error"
        found.errors.keys.each do |key| 
          Delayed::Worker.logger.debug "error key: #{key}"
          found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
        end
      end
      found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
      Delayed::Worker.logger.debug "found new record? #{found.new_record?}"
      Delayed::Worker.logger.debug "found id #{found.id}"
      
      found.status = 'off' if found.mls_number.missing?
      found.data_updated_at = DateTime.now
      found.data_source = source_elem if found.new_record?
      found.save
      
      
      
      found
    end
  end
  
  def import9(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing

    source_elem = nil
    mls_source_elem = nil
    
    if source.is_a?(Hash)
      source_elem = source[:source]
      mls_source_elem = source[:mls_source]
    else
      source_elem = source
    end

    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    
    # p "self state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      # look for property with mls number in market database 
      founds = self.properties.where(["(mls_number = ?)  ", attrs[:mls_number]]) rescue nil; 
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      
      if founds && (founds.length > 0)# a property is found
          
        for found in founds
          
            
            # found.assign_attributes(status: attrs[:status])
            # validate data quality on found property
            unless found.valid?
              # reset fields with errors
              found.errors.keys.each do |key| 
                found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
              end
            end
            found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
            if found.status != attrs[:status]
              puts "updated id: #{found.id} found status #{found.status} status #{attrs[:status]}"
              # if (attrs[:status].in?(['active']) && !attrs[:listing_date].blank?)
#   
                # selected_fields = attrs.select{|x| ![:listing_date,:dom,:cdom].include?(x)}
              # else
                # selected_fields = attrs
              # end
              found.assign_attributes(attrs)
            end
            found.data_source = source if found.new_record?
            found.status = 'off' if found.mls_number.missing?
            found.data_updated_at = DateTime.now
            found.save
          
          
          
        end
        
        return founds
      # else
#           
        # found = self.properties.new(attrs) # create new property record
#         
        # # validate data quality on found property
        # unless found.valid?
          # found.errors.keys.each do |key| 
            # found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
          # end
        # end
        # found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
        # found.data_source = source if found.new_record?
        # found.status = 'off' if found.mls_number.missing?
        # Delayed::Worker.logger.debug "inserted id: #{found.id}"
        # found.data_updated_at = DateTime.now
        # found.save
      end     
    end
  end
  
  def import_geometry(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self c state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      # look for property with mls number in market database 
      founds = self.properties.where(["(mls_number = ?)", attrs[:mls_number]]) rescue nil; 
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      
      if founds && (founds.length > 0)# a property is found
               
        for found in founds
          if !found.latitude
            
            found.assign_attributes(latitude: attrs[:latitude],longitude: attrs[:longitude])
            # validate data quality on found property
            unless found.valid?
              # reset fields with errors
              found.errors.keys.each do |key| 
                found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
              end
            end
            found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
            Delayed::Worker.logger.debug "updated id: #{found.id} latitude: #{attrs[:latitude]} longitude: #{attrs[:longitude]}"
            found.data_source = source if found.new_record?
            found.status = 'off' if found.mls_number.missing?
            found.data_updated_at = DateTime.now
            found.save
          end
          
          
        end
        
        return founds
      
      end     
    end
  end
  
  def import_sale_price(source, attrs, raw={}, update_only=false)
    # ensure only attributes of interests are overriden and are not missing
    attrs = attrs.select {|k, v| !v.missing? }.with_indifferent_access
    p "self c state: #{self.state}"
    # discard records upon conditions
    return 'out-of-state' if attrs[:state].present? && attrs[:state].upcase != self.state.upcase # out of state

    if attrs[:mls_number] # a mls number exists
      # look for property with mls number in market database 
      puts "attr mls_number: #{attrs[:mls_number]}"
      founds = self.properties.where(["(mls_number = ?)", attrs[:mls_number]]) rescue nil; 
      #self.properties.find_by_mls_number(attrs[:mls_number]) 
      
      if founds && (founds.length > 0)# a property is found
               
        for found in founds
          if !found.closed_date
            
            found.assign_attributes(closed_date: attrs[:closed_date])
            # validate data quality on found property
            unless found.valid?
              # reset fields with errors
              found.errors.keys.each do |key| 
                found[key] = nil unless key.to_sym.in?(:mls_number, :tax_id, :status)
              end
            end
            found.detail._raw = (found.detail._raw || {}).merge(raw)  # merge raw data
            puts "updated id: #{found.id} sold_price: #{attrs[:sold_price]} "
            Delayed::Worker.logger.debug "updated id: #{found.id} sold_price: #{attrs[:sold_price]} "
            found.data_source = source if found.new_record?
            found.status = 'off' if found.mls_number.missing?
            found.data_updated_at = DateTime.now
            found.save
          end
          
          
        end
        
        return founds
      
      end     
    end
  end
  
  def update_search_stats
    self.search_stats = self.properties.search_stats
    self.save(validate: false)
  end

  def email_searches
    self.searches.where("email_start_on <= ?", Date.today).each do |search|
      Mailer.delay.search_results(search) if search.email_on?(Date.today)
    end
  end
  
  # def save?
    # if self.assumption_changed?
      # errors.add(:rental_comp_query, "Rental comp query can not be changed")
    # else
      # self.save  
    # end
#         
  # end

  private

    def check_mls_code_provider
      provider_class = mls_code.camelcase.constantize rescue nil
      if provider_class.nil? || provider_class.superclass != Provider
        errors.add(:mls_code, "MLS code could not be found with filename #{mls_code}.rb")
      end
    end

    def check_excel_templates
      [:level2, :level3].each do |attr|
        file = self.send(attr)
        if file.file?
          file = file.queued_for_write[:original] || file
          book = Spreadsheet.open file.path rescue nil
          # errors.add(attr, "Data sheet not found in template") if book.nil? || book.worksheet('Data').nil?
        end
      end
    end

    def check_comp_queries
      [:rental, :sale, :build].each do |category|
        attr = "#{category}_comp_query"
        begin
          query = self.send("#{attr}_for", Property.new)
          self.properties.limit(1).where(query)
        rescue Exception => ex
          errors.add(attr, "Invalid query: #{ex.message}")
        end
      end
    end

    def set_code
      self.code = (self.name.underscore.gsub(/[\Aa-z)]+/, '-') unless self.name.blank?)
    end
    
    # def check_comp
      # if assumptions_changed?
        # errors.add(:rental_comp_query, "Invalid query: rental comp query")
      # end
    # end
    
    

end

