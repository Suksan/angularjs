class Markets::UploadsController < Markets::BaseController
  before_filter :prepare_job, only: [:new, :create]

  acts_as_resource(Job) do 
    defaults route_prefix: nil
    belongs_to :market
    custom_actions collection: [:template], resource: [:export,:comps_report]
  end
  
  def new
    session.delete(:selected_tab)
    new!
  end
  
  def index
    @reload = 30000
    index!
  end

  def destroy
    resource.delete
    redirect_to collection_url
  end

  def template
    download(:csv, 'template', Property.limit(0).to_csv(Property::IMPORT_FIELDS))
  end
  
  def activate_tab
    session[:selected_tab] =   params[:selected_tab]
    render :nothing => true
  end

  def show
    ids = (resource.info[:ids] || '').split(' ')
    @properties = @market.properties.joins_proforma.where(id: ids).page(params[:page]).order_by(sort_by, sort_asc)
  end

  def create
    @upload.assign_attributes(params[:market_upload_job])
    create!
  end
  
  def export
    ids = (resource.info[:ids] || '').split(' ')
    properties = @market.properties.joins_proforma.where(id: ids)
    download(:csv, 'report', properties.to_csv4(current_user.preferences.columns_results))
  end
  
  def comps_report
    ids = (resource.info[:ids] || '').split(' ')
    properties = @market.properties.joins_proforma.where(id: ids)
    download(:csv, 'comps_report', properties.to_comps_report(Property::COMPS_REPORT_FIELDS))
  end
  
  

  private

    def prepare_job 
      @upload = MarketUploadJob.new(market_id: @market.try(:id), user_id: current_user_id)
      @upload.overrides = Array.new(3, {}) if can?(:bulk, Job)
    end

    def collection
      @uploads = @market.uploads.where(user_id: current_user_id).page(params[:page]).desc(:run_at)
    end

end
