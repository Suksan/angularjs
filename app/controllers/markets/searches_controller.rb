class Markets::SearchesController < Markets::BaseController
  SEARCH_FILEDS = ['properties.id', 'properties.market_id', 'properties.detail_id', 'properties._overrides',  'properties.beds',  'properties.category',  'properties.cdom',  'properties.city',  'properties.closed_date', 'properties.county',  'properties.deed_date', 'properties.disclosures', 'properties.dom', 'properties.expiration_date', 'properties.garage_capacity', 'properties.geo_market_area', 'properties.key_map_page',  'properties.last_sale_date',  'properties.legal_subdivision', 'properties.list_price',  'properties.listing_agent_phone', 'properties.listing_agent_name',  'properties.listing_date',  'properties.lock_box_code', 'properties.location_key',  'properties.location_name', 'properties.lot_square_feet', 'properties.mls_area_key',  'properties.mls_area_name', 'properties.mls_number',  'properties.mortgagee', 'properties.owner_estimated_loan_balance',  'properties.owner_street_address',  'properties.owner_city',  'properties.owner_name',  'properties.owner_state', 'properties.owner_zip_code',  'properties.owner_zip_4', 'properties.pending_date',  'properties.pool_private',  'properties.realtor_remarks', 'properties.rental_class', 
                  'properties.rental_type', 'properties.sale_price',  'properties.selling_for_lot_value', 'properties.single_family_type',  'properties.sold_lease_price',  'properties.square_feet', 'properties.street_direction',  'properties.street_name', 'properties.street_number', 'properties.state', 'properties.status',  'properties.subdivision', 'properties.subdivision_section_number',  'properties.tax_amount',  'properties.tax_id',  'properties.tax_market_value',  'properties.tax_rate',  'properties.tax_value', 'properties.tax_year',  'properties.termination_date',  'properties.year_built',  'properties.zip_4', 'properties.zip_code',  'properties.withdrawn_date',  'properties.sold_price',  'properties.adj_sale_price',  'properties.adj_sale_price_psf',  'properties.list_price_psf',  'properties.sale_price_psf',  'properties.bathrooms', 'properties.street_address',  'properties.zip', 'properties.city_state_zip',  'properties.address', 'properties.owner_zip', 'properties.latitude',  'properties.longitude', 'properties.zillow_id', 'properties.zillow_sale_estimate',  'properties.zillow_rental_estimate',  'properties.zillow_updated_at', 'properties.pshark_id', 'properties.pshark_updated_at', 'properties.comps_updated_at',  'properties.data_source', 'properties.data_updated_at', 'properties.created_at',  'properties.updated_at',  'properties.pshark_county', 'properties.dom_updated_at',  'properties.auto_updatable', 'properties.rep', 'properties.label', 'properties.elementary_school_rating', 'properties.middle_school_rating', 'properties.high_school_rating', 'properties.avg_school_rating']
  before_filter :load_search_stats, only: [:new, :edit, :create, :update]
  before_filter :load_properties, only: [:show, :map, :export, :add_my_prop, :set_per_page, :add_label]
  before_filter :load_property_tags, only: [:show]

  acts_as_resource do 
    defaults route_prefix: nil
    belongs_to :market
    custom_actions resource: [:map, :export, :add_my_prop, :set_per_page, :add_label]
  end

  def new
    @search = @market.searches.new(user_id: current_user_id, config_attributes: {status: 'active', category: 'single_family'})
  end
  
  def show
    
  end
    
  def create
    
    if params["delete_properties"] == "Delete properties"
           
      if params["search"]["config_attributes"]
        
        ids = params["search"]["config_attributes"]["id"]
        
        Property.delete_all(["id in (?)", ids])
        PropertyDetail.delete_all(["id in (?)", ids])
        Proforma.delete_all(["property_id in (?)", ids])
        
      end
      redirect_to url_for(action: 'show', id: 'go', q: session[:parameters]["q"]), notice: "Selected properties deleted successfully!"
    else
      puts "## params  #{params}"
      if params[:commit].nil?        
        puts "$%$ aha"
      elsif params[:commit].match(/save|create|update/i)
        create!
      else
        puts "Awesome"
        puts "~~search.config.attributes #{@search.config.attributes}"
        redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
      end
    end
  end

  def export
    Delayed::Worker.logger.debug "sort_by : #{sort_by}"
    properties = resource.results_for_user(false,current_user.id).limit(1500).order_by(sort_by, sort_asc)
    download(:csv, 'report', properties.to_csv4(current_user.preferences.columns_results,current_user.id))
  end
  
  def set_per_page
    if params[:per_page]
      puts "## set per page #{params[:per_page]}"
      session[:per_page] = params[:per_page]
      puts "## session per page #{session[:per_page]}"
      puts "### request full #{request.fullpath}"
      
      # redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
    else
      session[:per_page] = nil
    end
    
    full_path = request.original_url
    full_path.gsub!(/per_page=\d+/, "per_page=#{params[:per_page]}")
    full_path.gsub!(/&page=\d+/, "&page=1")
    full_path.gsub!(/\?page=\d+/, "?page=1")
    full_path_elements =  full_path.split('/set_per_page')
    @redirect_path = full_path_elements[0].html_safe + full_path_elements[1].html_safe
    
    
  end
  
  def add_my_prop
    puts "####params ids #{params[:ids]}"
    if params[:ids]
      ids = params[:ids].split(', ')
      properties = Property.find_all_by_id(ids)
      
      update_time = Time.now
      
      for property in properties
        
        
        force_restore = false
        
        proforma = property.proformas.find_or_initialize_by_user_id(current_user.id)
        sys_prof = property.proforma
        if (sys_prof && !proforma.new_record?) && (!sys_prof.new_record? && sys_prof.updated_at > proforma.updated_at )
          force_restore = true
        end
  
        if  force_restore
          proforma.restore(true)
          proforma.save
        else
          if proforma.new_record? || proforma.updated_at < 1.day.ago 
            proforma.restore
            proforma.save
          end
        end
        
        property.user_updated_at = update_time
        property.save
        
        my_prop = property.my_props.find_or_initialize_by_user_id(current_user.id)
        my_prop.save
      end
    end
  end
  
  def add_label
    tag_id = params[:tag_id]
    if params[:ids]
      ids = params[:ids].split(', ')
      property_tag = PropertyTag.find(tag_id)
      @tag_name = property_tag.name
      properties = Property.find_all_by_id(ids)
      
      update_time = Time.now
      
      for property in properties
        user_assigned_property = property.assigned_tags.find_or_initialize_by_user_id(current_user.id)
        user_assigned_property.assign_attributes({:tag => tag_id})
        user_assigned_property.save
                
        property.user_updated_at = update_time
        property.save
      end
    end
  end
  
  

  protected

    def member?
      defined?(@search) && @search.id.present?
    end

    def resource
      if params[:id] == 'go'
        begin
          @search = @market.searches.new(user_id: current_user_id, config_attributes: params[:q] || {}, sort_by: params[:sort_by], sort_ascending: params[:sort_asc])
          @search.id = 0
          
          @search
        rescue
          raise ActiveRecord::RecordNotFound.new unless Rails.env.development?
        end
      else
        @search = @market.searches.find(params[:id])
      end
    end

    def collection
      @searches = @market.searches.where(user_id: current_user_id).page(params[:page])
    end

    def load_search_stats
      puts "$#%$# #{@market.search_stats}"
      @stats ||= @market.search_stats
    end


    def load_properties
      puts "@@@ per_pate #{session[:per_page]}"
      puts "@@@@@@ sort_by #{sort_by}"
      
      sort = sort_asc ? 'ASC' : 'DESC'
      
      if sort_by.in?([:id,:address,:city,:state,:zip])
        sort_stmt = "properties.#{sort_by} #{sort}"
      
      else
         sort_stmt = "#{sort_by} #{sort}"
      end
      if session[:per_page]
        Delayed::Worker.logger.debug "property   #{@properties}"
        @properties ||= resource.results_for_user(false,current_user.id).page((params[:page].to_i == 0)? nil : params[:page].to_i ).per_page(session[:per_page].to_i).order(sort_stmt)    #.order_by(sort_by, sort_asc)
        # Delayed::Worker.logger.debug "property count:  #{@properties.length.to_s}"
      else
        Delayed::Worker.logger.debug "property   #{@properties}"
        @properties ||= resource.results_for_user(false,current_user.id).page((params[:page].to_i == 0)? nil : params[:page].to_i).order(sort_stmt)    #.order_by(sort_by, sort_asc)
        # Delayed::Worker.logger.debug "property count:  #{@properties.length.to_s}"
      end
    end
    # def load_properties
      # puts "@@@ per_pate #{session[:per_page]}"
      # puts "@@@@@@ sort_by #{sort_by}"
#       
      # sort = sort_asc ? 'ASC' : 'DESC'
      # if sort_by.in?([:id,:city,:state,:zip])
        # sort_stmt = "properties.#{sort_by} #{sort}"
#       
      # else
         # sort_stmt = "#{sort_by} #{sort}"
      # end
# 
#       
      # # @properties ||= resource.results.page(params[:page]).order(sort_stmt)  
      # # if session[:per_page]
        # # @properties ||= resource.results.page(params[:page]).per_page(session[:per_page]).order(sort_stmt)    #.order_by(sort_by, sort_asc)
        # # # @properties = resource.results.page(1).per_page(session[:per_page]).order(sort_stmt)  if (params[:page]) && (  session[:per_page].to_i * params[:page].to_i >  @properties.count ) 
      # # else
        # # @properties ||= resource.results.page(params[:page]).order(sort_stmt)    #.order_by(sort_by, sort_asc)
      # # end
#       
      # if session[:per_page]
        # @properties ||= resource.results
        # prop_count = @properties.count
        # per_page = session[:per_page].to_i
        # page_no = params[:page].to_i
        # if prop_count <  per_page
          # @properties = @properties.page(1).per_page(prop_count).order(sort_stmt)
        # elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
          # @properties = @properties.page(1).per_page(per_page).order(sort_stmt)
        # else
          # @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page).order(sort_stmt) 
        # end
        # # @properties = @market.properties.my_prop_for(current_user).page(params[:page]).per_page(session[:per_page]).order(sort_stmt)  #.order_by(sort_by, sort_asc)
      # else
        # @properties ||= resource.results
        # prop_count = @properties.count
        # per_page = session[:per_page].to_i
        # page_no = params[:page].to_i
        # @properties = @properties.page((page_no == 0)? nil : page_no).order(sort_stmt)    #.order_by(sort_by, sort_asc)
      # end
#       
#       
    # end
    
    def load_property_tags
     
      @property_tags ||= PropertyTag.where(["market_id = ?", @market.id]).order("seq")
    end
end