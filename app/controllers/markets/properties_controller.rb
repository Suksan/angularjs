class Markets::PropertiesController < Markets::BaseController
  #before_filter proc { recompute(false) }, except: [:index, :saved, :new, :create] #disable for now (compute too slow)
  before_filter :load_proforma, only: [:summary, :comps_rental, :comps_sale, :comps_build, :proforma, :save_proforma, :recompute]
  before_filter :load_comment, only: [:save_comment]
  before_filter :set_user_role, only: [:show,:edit,:update,:summary,:add_saved, :remove_saved, :map, :comps_rental, :comps_sale, :comps_build, :proforma, :raw, :save_proforma, :level2, :level3, :recompute, :comment, :update_rep, :update_tag, :save_comment, :update_flood, :update_near_commercial]
  
  acts_as_resource do 
    defaults route_prefix: nil
    belongs_to :market
    actions :show, :edit, :update
    custom_actions :resource => [:summary,:add_saved, :remove_saved, :map, :comps_rental, :comps_sale, :comps_build, :proforma, :raw, :save_proforma, :level2, :level3, :recompute, :comment, :save_comment, :update_rep, :update_near_commercial, :update_tag, :comps_rental_export, :comps_sale_export, :comps_build_export]
    custom_actions :collection => [:saved,:rem_myprop,:export] 
  end
  
  def opportunities
    
    hsh_map = {
      id: "id",
      street_address: "property_address_c",
      city: "property_address_city_c",
      state: "property_address_street_c",
      zip_code: "postal_code",
      beds: "bed_c",
      bathrooms: "bath_c",
      square_feet: "sq_ft_c",
      offer_price: "offer_price_c",
      offer_date: "offer_date_c",
      list_price: "list_price_c",
      closed_date: "close_date_c"
    }
    
    rslt = @market.properties.where(["id > ?", 6185694]).as_json
    
    last_result = rslt.inject([]) do |mem,item|
      hsh = {}
      hsh_map.each do |k,v|
    
        if (k.to_s == "offer_price" || k.to_s == "offer_date")
          hsh[v] = ""
        else
          hsh[v] = item[k.to_s]
        end
    
        
        
    
      end
      mem << hsh
      mem
    end
    render json: last_result
  end
  
  def rem_myprop
    if params["delete_properties"] == "Remove Property"
      if params.try(:[],"properties").try(:[],"config").try(:[],"id")    
        ids = params.try(:[],"properties").try(:[],"config").try(:[],"id")  
                  
        MyProp.delete_all(["property_id in (?) and user_id = ?", ids, current_user.id ])
        
        if session[:parameters] && session[:parameters][:page] && @market.properties.my_prop_for(current_user).page(session[:parameters][:page]).count  <= 20*(session[:parameters][:page].to_i - 1)
          redirect_to url_for(action: 'saved'), notice: "The selected properties were deleted from My Props successfully!"
        else
          p_sort_by = session[:parameters].try(:[],"sort_by")
          p_sort_asc = session[:parameters].try(:[],"sort_asc")
          redirect_to url_for(action: 'saved', page: session[:parameters][:page], sort_by: p_sort_by, sort_asc: p_sort_asc ), notice: "The selected properties were deleted from My Props successfully!"
        end
                         
      else 
       redirect_to url_for(action: 'saved', page: session[:parameters][:page]), notice: "Please select the properties you want to delete from My Props!"
      end
      
    end
  end
  
  def saved
    @title = t(:saved_properties)
    
    puts "@@@@@@ sort_by #{sort_by}"
      
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    if session[:per_page]
      @properties = @market.properties.my_prop_for(current_user)
      prop_count = @properties.count
      per_page = session[:per_page].to_i
      page_no = params[:page].to_i
      if prop_count <  per_page
        @properties = @properties.page(1).per_page(prop_count).order(sort_stmt)
      elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
        @properties = @properties.page(1).per_page(per_page).order(sort_stmt)
      else
        @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page).order(sort_stmt) 
      end
      # @properties = @market.properties.my_prop_for(current_user).page(params[:page]).per_page(session[:per_page]).order(sort_stmt)  #.order_by(sort_by, sort_asc)
    else
      @properties = @market.properties.my_prop_for(current_user)
      prop_count = @properties.count
      per_page = session[:per_page].to_i
      page_no = params[:page].to_i
      @properties = @properties.page((page_no == 0)? nil : page_no).order(sort_stmt)    #.order_by(sort_by, sort_asc)
    end

    # @properties.each do |p|
    #   p.current_user_id = current_user.id
    # end
  end
  
  
  def comps_rental_export
   
    
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    load_comps_only(:rental,sort_stmt)
    coordinate = {}
    coordinate = {latitude: @property.latitude,longitude: @property.longitude} if !@property.latitude.nil? && !@property.longitude.nil?
    download(:csv, 'report', @properties.to_csv2(current_user.preferences.columns_comps,coordinate,current_user.id))
  end
  
  def comps_sale_export
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    load_comps_only(:sale,sort_stmt)
    coordinate = {}
    coordinate = {latitude: @property.latitude,longitude: @property.longitude} if !@property.latitude.nil? && !@property.longitude.nil?
    Delayed::Worker.logger.debug "~~coordinate #{coordinate}"
    download(:csv, 'report', @properties.to_csv2(current_user.preferences.columns_comps,coordinate,current_user.id))    
  end
  
  def comps_build_export
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    load_comps_only(:build,sort_stmt)
     coordinate = {}
    coordinate = {latitude: @property.latitude,longitude: @property.longitude} if !@property.latitude.nil? && !@property.longitude.nil?
    download(:csv, 'report', @properties.to_csv2(current_user.preferences.columns_comps,coordinate,current_user.id))  
  end
   
  def export
    # properties = resource.results.limit(500).order_by(sort_by, sort_asc)
    # download(:csv, 'report', properties.to_csv2(current_user.preferences.columns_results))
    
    
 
      
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    @properties = @market.properties.my_prop_for(current_user).order(sort_stmt)
    download(:csv, 'report', @properties.to_csv3(current_user))
  end

  def show
    redirect_to summary_resource_url
  end

  def summary
    @history = @property.history.page(params[:page])
    
  end
  
  def set_per_page
    if params[:per_page]
      puts "## set per page #{params[:per_page]}"
      session[:per_page] = params[:per_page]
      puts "## session per page #{session[:per_page]}"
      puts "### request full #{request.fullpath}"
      
      # redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
    else
      session[:per_page] = nil
    end
    
    full_path = request.original_url
    Delayed::Worker.logger.debug "before full path #{full_path}"
    Delayed::Worker.logger.debug "after full path #{full_path}"
    full_path_elements = full_path.split('/set_per_page')
    @redirect_path = full_path_elements[0] + '/saved' 
    
    %w{page per_page}.each do |k|
      if ! (@redirect_path.include?('page') || @redirect_path.include?('per_page') )
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "?#{k}=1" ).html_safe  
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "?#{k}=#{params[k.to_sym]}" ).html_safe  
        end
         
      else
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "&#{k}=1".html_safe ).html_safe 
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "&#{k}=#{params[k.to_sym]}".html_safe ).html_safe 
        end
      end
    end
    
  end

  def data_set_per_page
    if params[:per_page]
      puts "## set per page #{params[:per_page]}"
      session[:data_per_page] = params[:per_page]
      puts "## session per page #{session[:per_page]}"
      puts "### request full #{request.fullpath}"
      
      # redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
    else
      session[:data_per_page] = nil
    end
    
    full_path_elements = request.original_url.split('/data_set_per_page')
    Delayed::Worker.logger.debug "##full_path #{full_path_elements[1]}"
    @redirect_path = full_path_elements[0] +  "/" + params[:data_action] 
    puts "### redirect path  #{@redirect_path }"
    
    %w{page per_page}.each do |k|
      if ! (@redirect_path.include?('page') || @redirect_path.include?('per_page') )
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "?#{k}=1" ).html_safe  
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "?#{k}=#{params[k.to_sym]}" ).html_safe  
        end
         
      else
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "&#{k}=1".html_safe ).html_safe 
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "&#{k}=#{params[k.to_sym]}".html_safe ).html_safe 
        end
      end
    end
    
  end

  def update
    
    @property.user_updated_at = Time.now
    Delayed::Worker.logger.debug "params[:property] #{params[:property]}"
    
    
    
    @property.override_attributes(params[:property])
    Delayed::Worker.logger.debug "category: #{@property.category}"
    
    # if @property.category = 'rental'
      # @property.sale_price = @property.sold_lease_price
    # else
      # @property.sale_price = @property.sold_price
    # end   
    # @property.sale_price = @property.send("sale_price_calc")
    if @property.save
      
      @property.compute
      redirect_to edit_resource_url, notice: t('flash.actions.update.notice', resource_name: Property.model_name.human)
    
    else
      
      flash[:alert] = t('flash.actions.update.alert', resource_name: Property.model_name.human)
      render 'edit'
    
    end
  end
  
  def remove_saved
    @property ||= resource
    MyProp.delete_all(["property_id = ? and user_id = ?", @property.id, current_user.id])
    redirect_to request.referer, notice: "The property has been removed from My Props successfully!"
  end
  
  def add_saved
    @property ||= resource
    
    my_prop = @property.my_props.find_or_initialize_by_user_id(current_user.id)
    my_prop.save
    
    redirect_to request.referer, notice: "The property has been saved to My Props successfully!"
  end
  
  def update_rep
     Delayed::Worker.logger.debug "~~ param rep: #{params[:values]}"
     @user_updated_at =  Time.now
     @property.override_attributes({:rep => (params[:values].blank? ?  nil : params[:values]), :user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @property.save
          format.html {redirect_to(summary_resource_url, notice: "Rep updated successfully!")}
          format.js 
        else
          format.js { render :action => "update_rep_failure", :locals => {:id => @property.rep_was} }
        end
      end 
  
  end
  
  
  def update_tag
     
     @user_updated_at = Time.now
     @user_assigned_property = @property.assigned_tags.find_or_initialize_by_user_id(current_user.id)
     @user_assigned_property.assign_attributes({:tag => (params[:tag].blank? ?  nil : params[:tag])})
     @property.assign_attributes({:user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @user_assigned_property.save && @property.save
          format.html {redirect_to(summary_resource_url, notice: "Tag updated successfully!")}
          format.js
        end
      end   
  end
  
  def update_flood
     Delayed::Worker.logger.debug "~~flood #{params[:flood].nil?}"
     @user_updated_at = Time.now
     @property.override_attributes({:flood => (params[:flood].blank? ?  nil : params[:flood]) , :user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @property.save
          format.html {redirect_to(summary_resource_url, notice: "Flood updated successfully!")}
          format.js
        end
      end   
  end
  
  def update_near_commercial
     Delayed::Worker.logger.debug "~~near_commercial #{params[:near_commercial]}"
     @user_updated_at = Time.now
     @property.override_attributes({:near_commercial => (params[:near_commercial].blank? ?  nil : params[:near_commercial]), :user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @property.save
          format.html {redirect_to(summary_resource_url, notice: "Near commercial updated successfully!")}
          format.js
        end
      end   
  end
  
  
  def comps_rental
    load_comps(:rental)
  end

  def comps_sale
    load_comps(:sale)
  end

  def comps_build
    load_comps(:build)
  end

  def recompute(force=true)
    Delayed::Worker.logger.debug "~~ force? #{force ? true : false}"
    
    destination_url = case  params[:property].try(:[],'config').try(:[],'category')
                     when "sale"
                      comps_sale_resource_url
                     when "rental"
                      comps_rental_resource_url
                     end
    
    if (params[:commit] == "Recompute")
      if force
        @property.compute(force)
        redirect_to destination_url

      elsif (@property = resource) && (@property.comps_updated_at.nil? || @property.comps_updated_at <= 1.day.ago)
        @property.compute
        redirect_to destination_url
      end
    elsif  (params[:commit] == "Save")
      if save_property
        redirect_to destination_url
      else
        
        load_comps_without_render(params[:property].try(:[],'config').try(:[],'category').to_sym)
        render :action => :comps_sale, :template => 'markets/properties/comps'
      # else
      #   Delayed::Worker.logger.debug "proforma error #{@proforma.errors.full_messages}"
      #   # comps_sale
      #   render  request.referrer # action: "comps_#{params[:property].try(:[],'config').try(:[],'category')}"
        # comps_#{params[:property].try(:[],'config').try(:[],'category')}"
      end
    else
      @proforma.restore(true)
      redirect_to destination_url, :notice => 'Fields restored successfully!'
    end
  end

  def proforma
  end
  
  def comment    
  end
  
  def save_comment
          
      if (params[:commit] && params[:commit] == "Save")
        
        @note.destroy
        
        @property.user_updated_at = Time.now
        @property.override_attributes(params[:property])  
        
        if @property.save
          @property.compute
          
          redirect_to comment_resource_url, notice: t('flash.actions.update.notice', resource_name: Property.model_name.human)
        else
          flash[:alert] = t('flash.actions.update.alert', resource_name: Property.model_name.human)
          render 'comment'
        end
      else
        
        @user_updated_at = Time.now
        @property = Property.find(@note.property_id)
        
        @property.user_updated_at = Time.now
        # @property.save
        @note.override_attributes(params[:property][:note])
        
        respond_to do |format|
          if @note.save
            format.html {redirect_to(comment_resource_url, notice: "Note created successfully!")}
            format.js
          else
            format.html {render :action => "comment"}
            format.js
          end
        end   
      end
  end
  
  def save_property
    
    
    # case current_user.id
    #   when 14
    #     pname = "six_four_three_proforma"
    #   when 20
    #     pname = "ahr_three_five_zero_proforma"
    #   when 21
    #     pname = "invitation_proforma"
    # end
    category = nil
    Delayed::Worker.logger.debug ">>>params #{params}"
    # @user_updated_at = Time.now
    # Delayed::Worker.logger.debug ">>> property_only #{params[:property].reject{ |k| k.in?([pname]) }}"
    
    # Delayed::Worker.logger.debug ">>>>client_rep: #{params[:client_rep]}"
    # Delayed::Worker.logger.debug ">>>>client_near_commercial: #{params[:client_near_commercial]}"

    # prop_hsh = {}

    # other_users = User.all.map(&:id).reject {|d| d == current_user.id  }
    # cur_reps = @property.rep ? @property.rep.split(",") : []
    # Delayed::Worker.logger.debug ">>>>other_users: #{other_users}"
    # Delayed::Worker.logger.debug ">>>>cur_reps: #{cur_reps}"
    # cur_reps_minus_self = cur_reps.reject{ |k| k == current_user.id.to_s }
    # Delayed::Worker.logger.debug ">>>>cur_reps_minus_self: #{cur_reps_minus_self}"
    # cur_reps_minus_self_str = cur_reps_minus_self.join(",")
    # prop_hsh[:rep] = [cur_reps_minus_self_str,params[:client_rep].is_a?(Array) ? params[:client_rep].try(:join,",") : params[:client_rep]].reject(&:blank?).join(",")
    # prop_hsh[:near_commercial] = params[:client_near_commercial]
    
    # prop_hsh[:flood] = params[:client_flood]


    # Delayed::Worker.logger.debug ">>>>prop hsh: #{prop_hsh}"
    
    # assigning_hash = params[:property].reject{ |k| k.in?([pname]) }.merge(prop_hsh).merge({:user_updated_at => @user_updated_at})
    # Delayed::Worker.logger.debug ">>>>assigning_hash: #{assigning_hash}"
    # @property.assign_attributes(assigning_hash)
    assigning_hash = params[:property].reject{ |k| k.in?(['proforma','config']) }.merge({:user_updated_at => @user_updated_at})
    # @property.assign_attributes(params[:property])
    @property.assign_attributes(assigning_hash)
    # @property.assign_attributes(detail_attributes: {annual_maintenance_fee: params[:property].try(:[],:annual_maintenance_fee)})
    # detail = @property.detail 
    # binding.pry
#     
    # detail.save
#     
    # client_note =  nil
  
    # if !@comment  
  
    #   client_note = @property.notes.new({:posted_by => current_user.id})  if !params[:client_notes].blank?  
    #   client_note.content = params[:client_notes]  if !params[:client_notes].blank?  
    # else

    #   client_note =  @property.notes.last
    #   client_note.posted_by = current_user.id
    # end

    # if (!client_note.blank? && !params[:client_notes].blank?)
    #   client_note.content = params[:client_notes]
    #   client_note.save
    # end
    
    # if (!client_note.blank? && !client_note.new_record? && params[:client_notes].blank?)
    #   client_note.delete      
    # end 
    
    # user_assigned_property = @property.assigned_tags.find_or_initialize_by_user_id(current_user.id)
    # user_assigned_property.assign_attributes({:tag => (params[:client_tag].blank? ?  nil : params[:client_tag])})
    @proforma.override_attributes(params[:property].try(:[],'proforma'.to_sym))  
    Delayed::Worker.logger.debug "Config ! id #{ params[:config].try(:[],'id')}"
    Delayed::Worker.logger.debug "config category: #{params[:property].try(:[],'config').try(:[],'category')}"
    if params[:property].try(:[],'config').try(:[],'category')
      category = params[:property].try(:[],'config').try(:[],'category')

    end
    if params[:property].try(:[],'config').try(:[],'id')
      user_comp_list =  params[:property].try(:[],'config').try(:[],'id').reject(&:blank?).join(",")
      Delayed::Worker.logger.debug "User comp list #{user_comp_list}"
      @proforma.override_attributes({"comparables_user_comp_list_#{category}".to_sym => user_comp_list})
    else

      @proforma.override_attributes({"comparables_user_comp_list_#{category}".to_sym => nil})
    end

    # if @proforma.save
    #   Delayed::Worker.logger.debug "successful proforma"
    # else
    #   Delaeyd::Worker.logger.debug "proforma failed: #{@proforma.errors.full_messages }"
    # end

    # true if (@property.save)
    if (@property.save)  && (@proforma.save) # && (user_assigned_property.save)
      true
    else
      false
    end
  end

  def save_proforma
    if params[:restore]
      @proforma.restore(true)
      redirect_to proforma_resource_url, :notice => 'Fields restored successfully!'
    else
      @user_updated_at = Time.now
      @property.override_attributes({:user_updated_at => @user_updated_at})
      @property.save
      
      @proforma.override_attributes(params[:proforma])  
      if @proforma.save
       
        redirect_to proforma_resource_url

      else
        render 'proforma'
      end
    end
  end

  def level2
    download(:xls, 'report', @property.to_xls(Property.active_attributes(others: :id, delegates: PropertyDetail), @market.level2.path))
  end

  def level3
    download(:xls, 'report', @property.to_xls(Property.active_attributes(others: :id, delegates: PropertyDetail), @market.level3.path))
  end
  
 

  
  protected

    def begin_of_association_chain
      @market_assignment
    end
    
    def collection
      @properties = end_of_association_chain.latest.page(params[:page])
    end

    def load_proforma
      
      Delayed::Worker.logger.debug "performa being loaded"
      force_restore = false
      @property ||= resource
      @proforma = @property.proformas.find_or_initialize_by_user_id(current_user.id)
      sys_prof = @property.proforma
      if (sys_prof && !@proforma.new_record?) && (!sys_prof.new_record? && sys_prof.updated_at > @proforma.updated_at )
        force_restore = true
      end

      if  force_restore
        # @proforma.restore(true)
        # @proforma.save
        @proforma.send(:calculate_fields) 
        @proforma.save
      else
        Delayed::Worker.logger.debug "@@ about to enter"
        if @proforma.new_record? || @proforma.updated_at < 1.day.ago
          Delayed::Worker.logger.debug "@@ into the block"
          # binding.pry
          @proforma.restore
          @proforma.save
        end
      end
      
      
    end
    
    def load_comment
      @property ||= resource
      @note = @property.notes.new({:posted_by => current_user.id})
      
    end

    def load_comps(category)
      
      load_comps_without_render(category)
      render 'comps'
    end

    def load_comps_without_render(category)
      @category = category

      params[:sort_by] =  :sale_price_psf if ! params[:sort_by].try(:to_sym)      
      params[:sort_asc] =  params[:sort_asc].blank? ? "false" :  params[:sort_asc]
      
      @subject_property = @market.properties.where(["(id in (?)) ",@property.id])
      if session[:data_per_page]
        @properties = @property.comparables(category)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        
        if per_page == 0
          @properties = @properties.page(1).per_page(prop_count)
          
        elsif prop_count <  per_page
          @properties = @properties.page(1).per_page(prop_count)
        elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
          @properties = @properties.page(1).per_page(per_page)
        else
          @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page)
        end
        
        
      else
        @properties = @property.comparables(category)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        @properties = @properties.page(1).per_page(prop_count)
      end
      Delayed::Worker.logger.debug "@properties #{@properties}"
      Delayed::Worker.logger.debug "@subject_property #{@subject_property}"
      @subject_property = @subject_property.page(1)
      
      # @comp_properties =
      @stats = @property.send("comps_#{category}_stats")
      
      # comps.try(:index,property.id)
      # @stats[:comp_grades]
      Delayed::Worker.logger.debug "stat_comps #{@stats[:comps]}"      
      
      Delayed::Worker.logger.debug "Return new comp level: return_new_#{@category}_comp_level"
      @comp_level = @assignment.definition.send("return_new_#{@category}_comp_level".to_sym)
      Delayed::Worker.logger.debug "comp level #{@comp_level}"     
      
      level_list = case @comp_level
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
      @stats_comps = @stats[:comps] ?  @stats[:comps]  : []
      @stats_comp_grades = @stats[:comp_grades] ? @stats[:comp_grades] : []
      scomp_indices = @stats_comp_grades.each_index.select{|i| @stats_comp_grades[i].in?(level_list)}
      non_scomp_indices = @stats_comp_grades.each_index.select{|i| !@stats_comp_grades[i].in?(level_list)}
      Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
      
      @selected_comps = @stats_comps.values_at(*scomp_indices)
      @selected_comp_grades = @stats_comp_grades.values_at(*scomp_indices)
      @non_selected_comps = @stats_comps.values_at(*non_scomp_indices)
      @non_selected_comp_grades = @stats_comp_grades.values_at(*non_scomp_indices)
      @comp_properties = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps])
      @comp_properties = @comp_properties.page(1).per_page(@comp_properties.count)
      
      @non_comp_properties = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps, @selected_comps])
      @non_comp_properties = @non_comp_properties.page(1).per_page(@non_comp_properties.count)
       # arr.each_index.select{|i| arr[i] == 'x'}
      Delayed::Worker.logger.debug "test comparable user comp: #{ @proforma.try("comparables_user_comp_list_#{@category}".to_sym).to_s}"
      comparables_user_comp_list = @proforma.try("comparables_user_comp_list_#{@category}".to_sym).to_s.split(",").map(&:to_i)
      Delayed::Worker.logger.debug "@comparables_user_comp_list: #@comparables_user_comp_list}"
      
      @comparables_user_comp_list = []
      
      if (comparables_user_comp_list && (comparables_user_comp_list.length > 0))
        @comparables_user_comp_list =  @comp_properties.map(&:id).inject([]) do |rst,elem|
          Delayed::Worker.logger.debug "elem #{elem.class}"
          rst << elem if elem.in?(comparables_user_comp_list) 
          rst
        end
        
        
      end
      Delayed::Worker.logger.debug "@comparables_user_comp_list after inject: #{@comparables_user_comp_list}"
      
      if @comparables_user_comp_list.length != comparables_user_comp_list.length
          
        recs = @market.properties.select("sale_price_psf").where(["(id in (?)) AND status in ('sold')",@comparables_user_comp_list]).map(&:sale_price_psf)
        Delayed::Worker.logger.debug "recs is: #{recs}"
        Delayed::Worker.logger.debug "cat is: #{category.class}"
        case category
        when :sale
          Delayed::Worker.logger.debug "sale entering"
          p_psf = (@comparables_user_comp_list ? (Math.sum(*recs) / @comparables_user_comp_list.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_sale = p_psf
          @proforma.comparables_user_avm_sale = ((p_psf.nil? || @property.square_feet.nil?) ? nil : (p_psf * @property.square_feet))
        when :rental
          Delayed::Worker.logger.debug "rental entering"
          p_psf = (@comparables_user_comp_list ? (Math.sum(*recs) / @comparables_user_comp_list.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_rental = p_psf
          @proforma.comparables_user_avm_rental = (p_psf.nil? || @property.square_feet.nil?) ? nil : p_psf * @property.square_feet
        end
        
        Delayed::Worker.logger.debug "sp_psf is: #{@proforma.comparables_user_sale_price_psf_sale}"
        Delayed::Worker.logger.debug "avm_sale is: #{@proforma.comparables_user_avm_sale}"
         
      end
      Delayed::Worker.logger.debug "after loop @comparables_user_comp_list #{@comparables_user_comp_list}"
      return nil
       # arr.each_index.select{|i| arr[i] == 'x'}
      
      
    end    
    
    def load_comps_only(category, sort_stmt)
      @category = category
      
      @properties = @property.comparables(category).order(sort_stmt)
         
    end
    
    def set_user_role
      @property ||= resource
      Delayed::Worker.logger.debug "~~ current_user.role #{current_user.role}"
      if ((!current_user.admin?)  &&  (current_user.role != 'analyst')) 
        @property.is_non_admin = true 
      end
      @property.current_user_id = current_user.id 
    end
    
    protected
    
      # def sort_by
        # params[:sort_by].try(:to_sym) || :status
      # end
#       
      # def sort_asc
        # params[:sort_asc].blank? ? true :  (params[:sort_asc] == true ? true : false)
      # end

end