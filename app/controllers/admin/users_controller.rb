class Admin::UsersController < Admin::BaseController
  layout 'application2'
  # attr_accessor :search_json
  # self.search_json = {:only => [:id], :methods => [:firstname]}
  
  acts_as_resource do
    actions :all, except: [:show]
  end
  def new
    puts "nini"
    logger.debug "nini"
  end
  def create
    @user = User.new(params[:user])
    @user.password = Devise.friendly_token.first(6)
    create! do |success, failure|
      success.html do 
        @user.send_welcome_instructions
        redirect_to admin_users_url, :notice => 'User successfully created and notified by email'
      end
    end
  end
  
  def search
    @results = User.where("firstname like ?", "%#{params[:q]}%")
    respond_to do |format|
      format.html
      # format.j @authors.collect {|author| {:id => author.id, :name => author.account_number } }
       # format.json { render :json => @results.to_json({:only => [:id], :methods => [:firstname]})}
      format.json { render :json => @results.to_json({:only => [:id], :methods => [:name]})}
    end    
  end
  
  def search_self
    @results = User.where("(firstname like ?) and (id = ?)", "%#{params[:q]}%", current_user.id)
    respond_to do |format|
      format.html
      # format.j @authors.collect {|author| {:id => author.id, :name => author.account_number } }
       # format.json { render :json => @results.to_json({:only => [:id], :methods => [:firstname]})}
      format.json { render :json => @results.to_json({:only => [:id], :methods => [:name]})}
    end    
  end

  def update
    @user = resource
    puts "params #{params}"
    @user.assign_attributes(params[:user])
    email_changed = @user.email_changed?
    update! do |success, failure|
      success.html do
        @user.send_welcome_instructions if email_changed
        redirect_to admin_users_url, :notice => ['User successfully updated', ('notified by email' if email_changed)].compact.to_sentence
      end
    end
  end

  protected

    def collection
      @users = User.page(params[:page])
    end

end
