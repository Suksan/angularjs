class Admin::ProxyUploadsController < Admin::BaseController
  before_filter :prepare_job, only: [:new, :create]
  acts_as_resource do 
    actions :all
  end
  
  def new
    new!
  end
  
  def create
    puts "params #{params}"
    @proxy_upload.proxy_list = params.try(:[],"proxy_upload_job").try(:[],"proxy_list")
    create! do |success, failure|
      success.html { redirect_to admin_proxies_url }
    end
  end
  
  private

    def prepare_job 
      @proxy_upload = ProxyUploadJob.new()
      
    end

    def collection
      @proxy_uploads = nil
    end

end