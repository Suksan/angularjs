class Admin::MarketsController < Admin::BaseController
  acts_as_resource do
    actions :all, except: [:show]
  end
  
  def update
     @market.assign_attributes(params[:market])
     if params[:confirm]
       perform_update
     else       
       if !@market.assumptions_changed?
         perform_update
       else
         flash.now[:warning] = 'With assumptions changed, you will need to recomputes all properties in this market.<br> Are you sure you want to continue saving changes?'.html_safe
         render 'edit'
       end
     end
     
     
    
  end
  
  # def update
    # update! do
#       
      # collection_url
#       
    # end
    # if @market.save?
      # edit_resource_url(@market)
    # else
      # collection_url
    # end
     
    
    # if @market.assumption_changed?
      # edit_resource_url(@market)
    # else
      # update!
      # collection_url
    # end
    
    # puts "market rental_comp_query: #{@market.rental_comp_query}"
    # puts "market rental_comp_query changed?: #{@market.rental_comp_query_changed?}"
#     
    # rental_comp_query = @market.rental_comp_query
#     
    # update! do
      # puts "market rental_comp comparison: #{rental_comp_query} vs  #{@market.rental_comp_query}"
      # if rental_comp_query !=  @market.rental_comp_query
        # edit_resource_url(@market)
      # else 
        # # @market.save
        # collection_url
      # end
    # end
    # @market = Market.new(params[:market])
    # update!
    # if @market.assumption_changed?
      # puts "aaaa"
      # redirect_to "http://www.google.co.th"
      # # redirect_to edit_resource_url(:market => params[:market] )
    # else
      # redirect_to collection_url
    # end
  # end
  
  protected

    def collection
      @markets = Market.includes(:users).page(params[:page])
    end
    
    def perform_update
      if @market.save
        redirect_to url_for(:action => :index), :notice => 'Settings saved successfully!'
      else
        flash.now[:error] = 'Settings could not be saved, check errors!'
        render 'edit'
      end
    end

end
