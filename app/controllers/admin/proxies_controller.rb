class Admin::ProxiesController < Admin::BaseController
  acts_as_resource do
  	
    actions :all
    custom_actions :collection => [:import]

  end

  protected

    def collection
      @proxies = Proxy.page(params[:page])
    end

    def import
    end


    

end