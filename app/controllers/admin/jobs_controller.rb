class Admin::JobsController < Admin::BaseController
  before_filter :load_market_and_assignment
  before_filter :load_job_types, only: [:new, :create]

  acts_as_resource(Job) do 
    defaults route_prefix: :admin
    belongs_to :market
    actions :all, except: [:edit, :update]
  end

  def create
    config = params[:market_job]
    type = config.delete(:type)
    @job = case type
      when 'import' then @import
      when 'grab' then @grab
      when 'compute' then @compute
      when 'email' then @email
    end
    @job.assign_attributes(config)
    create!
  end

  def index
    @reload = 30000
    index!
  end

  def destroy
    destroy! do
      @job.delete
      collection_url
    end
  end

  protected

    def collection
      @jobs = @market.jobs.page(params[:page]).desc(:run_at)
    end

    def load_job_types
      @import = MarketImportJob.new(market_id: @market.id)
      @grab = MarketGrabJob.new(market_id: @market.id)
      @compute = MarketComputeJob.new(market_id: @market.id)
      @email = MarketEmailJob.new(market_id: @market.id)
    end
end
