class Admin::ServicesController < Admin::BaseController
  acts_as_resource do
    actions :all, except: [:show]
  end

  protected

    def collection
      @services = Service.page(params[:page])
    end

end
