class Client::PropertiesController < Client::BaseController
  layout 'client/properties3'
  skip_before_action :verify_authenticity_token
  before_filter :load_market_and_assignment
  helper_method :load_comps
  before_filter :load_pclass, only: [:summary,:comps_rental, :comps_sale, :comps_build,:comment,:save_comment,:save, :recompute, :capex_calc, :neighborhood, :show, :show_rental_comps, :show_sale_comps, :show_subject_property, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others]
  before_filter :load_proforma, only: [:summary, :comps_rental, :comps_sale, :comps_build,:save,:recompute,:save_comment, :show, :show_rental_comps, :show_sale_comps, :show_subject_property, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others]
  before_filter :load_property, only: [:summary, :recompute,:update_tag,:update_flood,:update_near_commercial,:update_rep,:client_sale_data_set_per_page,:client_sale_data_set_per_page,:save_comment,:update,:show,:show_rental_comps, :show_sale_comps, :show_subject_property, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others]
  before_filter :load_top_comment, only: [:comps_rental, :comps_sale, :comps_build,:comment,:save_comment, :show, :show_rental_comps, :show_sale_comps, :show_subject_property, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others]
  # before_filter :load_comment, only: [:save_comment]
  before_filter :set_user_role, only: [:comps_rental, :comps_sale, :comps_build]
  SFT_USER_ID = 21
  ATFZ_USER_ID = 22
  INV_USER_ID = 23
  INV_TWO_USER_ID = 24

  acts_as_resource do 
    defaults route_prefix: :client
    belongs_to :market
    actions :all, except: [:edit, :update]
  end
  
  def opportunity
    @market.properties.where(["id = ?",params["id"]]).as_json
  end
  
  
  def opportunities
    
    logger.debug "test: #{Rails.env}"
    logger.debug "yes i do"
    hsh_map = {
      id: "id",
      street_address: "property_address_c",
      city: "property_address_city_c",
      state: "property_address_state_c",
      zip_code: "postal_code",
      beds: "bed_c",
      bathrooms: "bath_c",
      square_feet: "sq_ft_c",
      offer_price: "offer_price_c",
      offer_date: "offer_date_c",
      list_price: "list_price_c",
      closed_date: "close_date_c"
    }
    
    list = ['5690971','8001248','5693012','5694665','5695485','5695382',
'5695515','8005034','8004765','8005326','5695701','5695714',
'5695966','5695674','5695991','5601254','7583785','5661319',
'5682138','7630674','5631393','5684576','5628526','7621952',
'5683941','5684361','5684226','5686042','7638290','7636794',
'5687646','7632929','7637019','7640352','5662096','7638392',
'7628411','5688701','5690822','5692085','5692011','5690813',
'5629477','5692568','5689097','8001628','8002948','8000456','7642217']

    rslt = @market.properties.where(["mls_number in (?)",list]).as_json
    
    last_result = rslt.inject([]) do |mem,item|
      hsh = {}
      hsh_map.each do |k,v|
    
        if (k.to_s == "offer_price" || k.to_s == "offer_date")
          hsh[v] = ""
        else
          hsh[v] = item[k.to_s].to_s
        end
    
      end
      mem << hsh
      mem
    end
    
    big_result = {} 
    big_result["data"] = last_result
    big_result["draw"] = 0
    big_result["recordsTotal"] = rslt.length
    big_result["recordsFiltered"] = 0
    
    puts "yes i did"
    
    render json: big_result
  
  end
  
  def search_opportunities
    
    logger.debug "test: #{Rails.env}"
    logger.debug "yes i do"
    hsh_map = {
      id: "id",
      status: "status",
      rental_output_max_purchase_ratio: "rental_output_max_purchase_ratio",
      pool_private: "pool_private",
      comparables_avm_rental: "comparables_avm_rental",
      mls_number: "mls_number",
      tag: "tag",
      dom: "dom",
      street_address: "street_address",
      city: "city",
      zip: "zip",
      back_on_market: "back_on_market",
      tax_id: "tax_id",
      price_reduction: "price_reduction",
      subdivision: "subdivision",
      subdivision_section_number: "subdivision_section_number",
      legal_subdivision: "legal_subdivision",
      square_feet: "square_feet",
      year_built: "year_built",
      beds: "beds",
      bathrooms: "bathrooms",
      list_price: "list_price",
      sale_price: "sale_price",
      list_price_psf: "list_price_psf",
      closed_date: "closed_date",
      access_to_complex: "access_to_complex",
      comp_grade: "comp_grade",
      offer_price: "offer_price",
      mls_source: "mls_source",
      garage_capacity: "garage_capacity",
      zillow_school_1: "zillow_school_1",
      zillow_school_2: "zillow_school_2",
      zillow_school_3: "zillow_school_3",
      avg_school_rating: "avg_school_rating",
      new_style_of_building: "new_style_of_building",
      new_stories: "new_stories",
      county: "county",
      mls_area: "mls_area",
      cdom: "cdom",
      waterfront: "waterfront",
      golf_course: "golf_course",
      gated_community: "gated_community",
      rental_output_cap_rate: "rental_output_cap_rate",
      comparables_avm_sale: "comparables_avm_sale",
      comparables_cdom_sale: "comparables_cdom_sale",
      comparables_cdom_rental: "comparables_cdom_rental",
      rental_income_monthly_gross_rent: "rental_income_monthly_gross_rent",
      rental_output_max_purchase_amount: "rental_output_max_purchase_amount",
      sale_sold_count: "sale_sold_count",
      rental_sold_count: "rental_sold_count",
      latitude: "latitude",
      category: "category",
      longitude: "longitude",
      state: "state",
      lot_square_feet: "lot_square_feet",
      listing_date: "listing_date",
      offer_date: "offer_date"
    }
    
    

    # rslt = @market.properties.retrieve_for_client_user(nil,current_user.id) .as_json
    if (params[:length])
      rawp = @market.properties.retrieve_for_client_user(params[:length],current_user.id)
    else
      rawp = @market.properties.retrieve_for_client_user(nil,current_user.id)
    end
    rslt = rawp.as_json
    
    xls = rawp.prepare_client_user(current_user.id)
    
    i = 0 
    last_result = rslt.inject([]) do |mem,item|
      hsh = {}
      
      puts "rawp:#{rawp[i]}"
      hsh_map.each do |k,v|
    
        logger.debug "k:#{k}"
        
        if (k.to_s == "id")
          hsh[k] = rawp[i].id
        elsif (k.to_s == "offer_date")
          hsh[k] = ""
        elsif (rawp[i].user_client_proforma.respond_to?(k.to_s))
          
          hsh[k] = rawp[i].user_client_proforma.format(k.to_s)
        elsif (k.to_s == 'tag')
          hsh[k] = rawp[i].format_tag(current_user.id)
        else
          hsh[k] = item[k.to_s].to_s
        end
    
      end
      i += 1
      mem << hsh
      mem
    end
    
    logger.debug "json: #{last_result}"
    
    big_result = {} 
    big_result["data"] = last_result
    big_result["draw"] = 0
    big_result["recordsTotal"] = rslt.length
    big_result["recordsFiltered"] = 0
    
    puts "yes i did"
    
    render json: big_result
  
  end
  
  def rem_myprop
    
    if params["delete_properties"] == "Remove Property"
      if params.try(:[],"properties").try(:[],"config").try(:[],"id")    
        ids = params.try(:[],"properties").try(:[],"config").try(:[],"id")  
                  
        MyProp.delete_all(["property_id in (?) and user_id = ?", ids, current_user.id ])
        
        if session[:parameters] && session[:parameters][:page] && @market.properties.my_prop_for(current_user).page(session[:parameters][:page]).count  <= 20*(session[:parameters][:page].to_i - 1)
          redirect_to url_for(action: 'saved'), notice: "The selected properties were deleted from My Props successfully!"
        else
          p_sort_by = session[:parameters].try(:[],"sort_by")
          p_sort_asc = session[:parameters].try(:[],"sort_asc")
          redirect_to url_for(action: 'saved', page: session[:parameters][:page], sort_by: p_sort_by, sort_asc: p_sort_asc ), notice: "The selected properties were deleted from My Props successfully!"
        end
                         
      else 
       redirect_to url_for(action: 'saved', page: session[:parameters][:page]), notice: "Please select the properties you want to delete from My Props!"
      end
      
    end
  end
  
  def saved
    @title = t(:saved_properties)
    
    puts "@@@@@@ sort_by #{sort_by}"
      
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    if session[:per_page]
      @properties = @market.properties.client_my_prop_for(current_user)
      prop_count = @properties.count
      per_page = session[:per_page].to_i
      page_no = params[:page].to_i
      if prop_count <  per_page
        @properties = @properties.page(1).per_page(prop_count).order(sort_stmt)
      elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
        @properties = @properties.page(1).per_page(per_page).order(sort_stmt)
      else
        @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page).order(sort_stmt) 
      end
      # @properties = @market.properties.my_prop_for(current_user).page(params[:page]).per_page(session[:per_page]).order(sort_stmt)  #.order_by(sort_by, sort_asc)
    else
      @properties = @market.properties.client_my_prop_for(current_user)
      prop_count = @properties.count
      per_page = session[:per_page].to_i
      page_no = params[:page].to_i
      @properties = @properties.page((page_no == 0)? nil : page_no).order(sort_stmt)    #.order_by(sort_by, sort_asc)
    end

    # @properties.each do |p|
    #   p.current_user_id = current_user.id
    # end
  end

  def convert x
    Float(x)
    i, f = x.to_i, x.to_f
    i == f ? i : f
  rescue ArgumentError
    x
  end
  
  def show_subject_property
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: proc { |d| "L" },
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }

        
        # puts "c: #{c}"
        p = @property

        hsh = {}
        hsh_map.each do |k,v|
          
          if (k.to_s == "id")
            hsh[k] = p.id
          elsif (k.to_s == "display")
            hsh[k] = false
          
          elsif (k.to_s.in? ["gross_yield"])
            hsh[k] = ""
          elsif v.is_a?(Proc)
            hsh[k] = v.call(p)
          elsif (p.user_client_proforma.respond_to?(k.to_s))
            hsh[k] = p.user_client_proforma.format(v.to_s)
          else
            hsh[k] = p.send(v.to_sym)
          end
        end
                

          
        outp = []
        outp << hsh
        
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 1,recordsFiltered: 1}

        render json:  outer_p
      } # end json
    }


    
    
  end
  
  
  def show_rental_comps
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
        @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }

        #
        level_list = case @comp_level_rental
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
        @stats_comps_rental = @stats_rental[:comps] ?  @stats_rental[:comps]  : []
        @stats_comp_grades_rental = @stats_rental[:comp_grades] ? @stats_rental[:comp_grades] : []
        scomp_indices = @stats_comp_grades_rental.each_index.select{|i| @stats_comp_grades_rental[i].in?(level_list)}
        non_scomp_indices = @stats_comp_grades_rental.each_index.select{|i| !@stats_comp_grades_rental[i].in?(level_list)}
        Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
        
        @selected_comps_rental = @stats_comps_rental.values_at(*scomp_indices)
        @selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*scomp_indices)
        @non_selected_comps_rental = @stats_comps_rental.values_at(*non_scomp_indices)
        @non_selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*non_scomp_indices)
        @comp_properties_rental = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_rental]).pluck(:id)
        # @comp_properties_rental = @comp_properties_rental.page(1).per_page(@comp_properties_rental.count)
        
        
        
        
        
        @non_comp_properties_rental = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_rental, @selected_comps_rental]).pluck(:id)
        # @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)
        #

        outp = @stats_rental[:comps].inject([]) { |c,j|
          puts "c: #{c}"
          p = Property.find(j)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
             
            elsif (k.to_s == "display")
              if (p.id.in?(@comp_properties_rental))
                hsh[k] = true
              else
                hsh[k] = false
              end
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, latitude: @property.latitude , longitude: @property.longitude , draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end


  def show_rental_comps_qualified
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
        @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }
        #
        level_list = case @comp_level_rental
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
        @stats_comps_rental = @stats_rental[:comps] ?  @stats_rental[:comps]  : []
        @stats_comp_grades_rental = @stats_rental[:comp_grades] ? @stats_rental[:comp_grades] : []
        scomp_indices = @stats_comp_grades_rental.each_index.select{|i| @stats_comp_grades_rental[i].in?(level_list)}
        non_scomp_indices = @stats_comp_grades_rental.each_index.select{|i| !@stats_comp_grades_rental[i].in?(level_list)}
        Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
        
        @selected_comps_rental = @stats_comps_rental.values_at(*scomp_indices)
        @selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*scomp_indices)
        @non_selected_comps_rental = @stats_comps_rental.values_at(*non_scomp_indices)
        @non_selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*non_scomp_indices)
        @comp_properties_rental = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_rental]).pluck(:id)
        # @comp_properties_rental = @comp_properties_rental.page(1).per_page(@comp_properties_rental.count)
        
        
        
        
        
        @non_comp_properties_rental = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_rental, @selected_comps_rental]).pluck(:id)
        # @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)
        #

        outp = @comp_properties_rental.inject([]) { |c,j|
          puts "c: #{c}"
          p = Property.find(j)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
            elsif (k.to_s == "display")
              hsh[k] = true
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end

  def show_rental_comps_others
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
        @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }

        level_list = case @comp_level_rental
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
        @stats_comps_rental = @stats_rental[:comps] ?  @stats_rental[:comps]  : []
        @stats_comp_grades_rental = @stats_rental[:comp_grades] ? @stats_rental[:comp_grades] : []
        scomp_indices = @stats_comp_grades_rental.each_index.select{|i| @stats_comp_grades_rental[i].in?(level_list)}
        non_scomp_indices = @stats_comp_grades_rental.each_index.select{|i| !@stats_comp_grades_rental[i].in?(level_list)}
        Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
        
        @selected_comps_rental = @stats_comps_rental.values_at(*scomp_indices)
        @selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*scomp_indices)
        @non_selected_comps_rental = @stats_comps_rental.values_at(*non_scomp_indices)
        @non_selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*non_scomp_indices)
        @comp_properties_rental = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_rental]).pluck(:id)
        # @comp_properties_rental = @comp_properties_rental.page(1).per_page(@comp_properties_rental.count)
        
        
        
        
        
        @non_comp_properties_rental = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_rental, @selected_comps_rental]).pluck(:id)
        # @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)

        outp = @non_comp_properties_rental.inject([]) { |c,j|
          puts "c: #{c}"
          p = Property.find(j)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
            elsif (k.to_s == "display")
              hsh[k] = false
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end
  
  
   def show_sale_comps
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
        @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }
        
        #
        level_list = case @comp_level_sale
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
        @stats_comps_sale = @stats_sale[:comps] ?  @stats_sale[:comps]  : []
        @stats_comp_grades_sale = @stats_sale[:comp_grades] ? @stats_sale[:comp_grades] : []
        scomp_indices = @stats_comp_grades_sale.each_index.select{|i| @stats_comp_grades_sale[i].in?(level_list)}
        non_scomp_indices = @stats_comp_grades_sale.each_index.select{|i| !@stats_comp_grades_sale[i].in?(level_list)}
        Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
        
        @selected_comps_sale = @stats_comps_sale.values_at(*scomp_indices)
        @selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*scomp_indices)
        @non_selected_comps_sale = @stats_comps_sale.values_at(*non_scomp_indices)
        @non_selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*non_scomp_indices)
        @comp_properties_sale = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_sale]).pluck(:id)
        # @comp_properties_sale = @comp_properties_sale.page(1).per_page(@comp_properties_sale.count)
        
        
        
        
        
        @non_comp_properties_sale = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_sale, @selected_comps_sale]).pluck(:id)
        # @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)
        #

        outp = @stats_sale[:comps].inject([]) { |c,j|
          puts "c: #{c}"
          p = Property.find(j)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
            elsif (k.to_s == "display")
              if (p.id.in?(@comp_properties_sale))
                hsh[k] = true
              else
                hsh[k] = false
              end
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end

  def show_sale_comps_qualified
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
        @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }

        #
        level_list = case @comp_level_sale
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
        @stats_comps_sale = @stats_sale[:comps] ?  @stats_sale[:comps]  : []
        @stats_comp_grades_sale = @stats_sale[:comp_grades] ? @stats_sale[:comp_grades] : []
        scomp_indices = @stats_comp_grades_sale.each_index.select{|i| @stats_comp_grades_sale[i].in?(level_list)}
        non_scomp_indices = @stats_comp_grades_sale.each_index.select{|i| !@stats_comp_grades_sale[i].in?(level_list)}
        Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
        
        @selected_comps_sale = @stats_comps_sale.values_at(*scomp_indices)
        @selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*scomp_indices)
        @non_selected_comps_sale = @stats_comps_sale.values_at(*non_scomp_indices)
        @non_selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*non_scomp_indices)
        @comp_properties_sale = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_sale]).pluck(:id)
        # @comp_properties_sale = @comp_properties_sale.page(1).per_page(@comp_properties_sale.count)
        
        
        
        
        
        @non_comp_properties_sale = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_sale, @selected_comps_sale]).pluck(:id)
        # @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)
        #

        outp = @comp_properties_sale.inject([]) { |c,j|
          puts "c: #{c}"
          p = Property.find(j)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
            elsif (k.to_s == "display")
              hsh[k] = true
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end

  def show_sale_comps_others
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
        @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }

        #
        level_list = case @comp_level_sale
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
        @stats_comps_sale = @stats_sale[:comps] ?  @stats_sale[:comps]  : []
        @stats_comp_grades_sale = @stats_sale[:comp_grades] ? @stats_sale[:comp_grades] : []
        scomp_indices = @stats_comp_grades_sale.each_index.select{|i| @stats_comp_grades_sale[i].in?(level_list)}
        non_scomp_indices = @stats_comp_grades_sale.each_index.select{|i| !@stats_comp_grades_sale[i].in?(level_list)}
        Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
        
        @selected_comps_sale = @stats_comps_sale.values_at(*scomp_indices)
        @selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*scomp_indices)
        @non_selected_comps_sale = @stats_comps_sale.values_at(*non_scomp_indices)
        @non_selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*non_scomp_indices)
        @comp_properties_sale = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_sale]).pluck(:id)
        # @comp_properties_sale = @comp_properties_sale.page(1).per_page(@comp_properties_sale.count)
        
        
        
        
        
        @non_comp_properties_sale = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_sale, @selected_comps_sale]).pluck(:id)
        # @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)
        #

        outp = @non_comp_properties_sale.inject([]) { |c,j|
          puts "c: #{c}"
          p = Property.find(j)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
            elsif (k.to_s == "display")
              hsh[k] = false
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end


  def show_sale_comps_qualifi
    Property.set_user_id(current_user.id)
    respond_to { |format|
      format.json {
        
        @stats_sale = @property.send("comps_sale_stats")
        @stats_rental = @property.send("comps_rental_stats")

        
        hsh_map = {
          id: "id",
          mark: "mark",
          street_address: "street_address",
          city: "city",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: "gross_yield",
          list_price: "list_price",
          dom: "dom",
          beds: "beds",
          latitude: "latitude",
          longitude: "longitude",
          display: "display"
        }

        outp = @stats_sale[:comps].inject([]) { |c,k|
          puts "c: #{c}"
          p = Property.find(k)

          hsh = {}
          hsh_map.each do |k,v|
            
            if (k.to_s == "id")
              hsh[k] = p.id
            elsif (k.to_s == "display")
              hsh[k] = true
            elsif (k.to_s.in? ["mark","gross_yield"])
              hsh[k] = ""
            elsif v.is_a?(Proc)
              hsh[k] = v.call(p)
            elsif (p.user_client_proforma.respond_to?(k.to_s))
              hsh[k] = p.user_client_proforma.format(v.to_s)
            else
              hsh[k] = p.send(v.to_sym)
            end
          end

          c << hsh

          c

        }
        outer_p = nil
        outer_p = {data: outp, draw: 0,recordsTotal: 11,recordsFiltered: 11}

        render json:  outer_p
      } # end json
    }


    
    
  end



  
  def show
    puts "param id #{params[:id]}"
    @comp_level_rental = @assignment.definition.send("return_new_rental_comp_level".to_sym)
    @comp_level_sale = @assignment.definition.send("return_new_sale_comp_level".to_sym)
    @stats_sale = @property.send("comps_sale_stats")
    @stats_rental = @property.send("comps_rental_stats")
    puts "comp_level_rental #{@comp_level_rental}"
    puts "comp_level_sale #{@comp_level_sale}"
    puts "comp_level_rental_expanded_avm #{@stats_rental.format(:expanded_avm, as: :money)}"
    puts "comp_level_rental_moderate_avm #{@stats_rental.format(:moderate_avm, as: :money)}"
    puts "comp_level_rental_conservative_avm #{@stats_rental.format(:conservative_avm, as: :money)}"
    puts "comp_level_sale_expanded_avm #{@stats_sale.format(:expanded_avm, as: :money)}"
    puts "comp_level_sale_moderate_avm #{@stats_sale.format(:moderate_avm, as: :money)}"
    puts "comp_level_sale_conservative_avm #{@stats_sale.format(:conservative_avm, as: :money)}"
    


    
    respond_to do |format|
      format.json {
        hsh_map = {
          id: "id",
          status: "status",
          rental_output_max_purchase_ratio: "rental_output_max_purchase_ratio",
          pool_private: "pool_private",
          comparables_avm_rental: "comparables_avm_rental",
          mls_number: "mls_number",
          tag: "tag",
          dom: "dom",
          street_address: "street_address",
          city: "city",
          zip: "zip",
          back_on_market: "back_on_market",
          tax_id: "tax_id",
          price_reduction: "price_reduction",
          subdivision: "subdivision",
          subdivision_section_number: "subdivision_section_number",
          legal_subdivision: "legal_subdivision",
          square_feet: "square_feet",
          year_built: "year_built",
          beds: "beds",
          bathrooms: "bathrooms",
          list_price: "list_price",
          sale_price: "sale_price",
          list_price_psf: "list_price_psf",
          closed_date: "closed_date",
          access_to_complex: "access_to_complex",
          comp_grade: "comp_grade",
          offer_price: "offer_price",
          mls_source: "mls_source",
          garage_attach: "garage_attach",
          garage_capacity: "garage_capacity",
          zillow_school_1: "zillow_school_1",
          zillow_school_2: "zillow_school_2",
          zillow_school_3: "zillow_school_3",
          avg_school_rating: "avg_school_rating",
          new_style_of_building: "new_style_of_building",
          new_stories: "new_stories",
          county: "county",
          mls_area: "mls_area",
          cdom: "cdom",
          waterfront: "waterfront",
          golf_course: "golf_course",
          gated_community: "gated_community",
          rental_output_cap_rate: "rental_output_cap_rate",
          gross_yield: proc {|d| nil},
          comparables_avm_sale: "comparables_avm_sale",
          comparables_cdom_sale: "comparables_cdom_sale",
          comparables_cdom_rental: "comparables_cdom_rental",
          rental_income_monthly_gross_rent: "rental_income_monthly_gross_rent",
          rental_output_max_purchase_amount: "rental_output_max_purchase_amount",
          
          
          latitude: "latitude",
          category: "category",
          longitude: "longitude",
          state: "state",
          lot_square_feet: "lot_square_feet",
          listing_date: "listing_date",
          offer_date: "offer_date",
          
          flood: "flood",
          near_commercial: "near_commercial",
          market_id: proc  { |d| 10 },
          comp_level_rental_expanded_avm: proc { |d| "Expanded" + " (" + @stats_rental.format(:expanded_avm, as: :money) + ")"},
          comp_level_rental_moderate_avm: proc { |d| "Moderate"  + " (" + @stats_rental.format(:moderate_avm, as: :money) + ")"},
          comp_level_rental_conservative_avm: proc { |d| "Conservative"  + " (" + @stats_rental.format(:conservative_avm, as: :money) + ")"},
          comp_level_rental_avm: proc { |d|  @comp_level_rental.capitalize  + " (" + @stats_rental.format("#{@comp_level_rental}_avm".to_sym, as: :money) + ")"},
          comp_level_sale_expanded_avm: proc { |d| "Expanded"  + " (" + @stats_sale.format(:expanded_avm, as: :money) + ")"},
          comp_level_sale_moderate_avm: proc { |d| "Moderate"  + " (" + @stats_sale.format(:moderate_avm, as: :money) + ")"},
          comp_level_sale_conservative_avm: proc { |d| "Conservative"  + " (" + @stats_sale.format(:conservative_avm, as: :money) + ")"},
          comp_level_sale_avm: proc { |d| @comp_level_sale.capitalize  + " (" + @stats_sale.format("#{@comp_level_sale}_avm".to_sym, as: :money) + ")"},
          comp_level_rental: proc { |d|  @comp_level_rental },
          comp_level_sale: proc { |d|  @comp_level_sale },
          comparables_user_avm_rental: proc { |d|  @proforma.comparables_user_avm_rental.to_i },
          comparables_user_avm_sale: proc { |d|  @proforma.comparables_user_avm_sale.to_i },
          rental_avm_score: proc { |d| Random.new.rand(89..96) },
          sale_avm_score: proc { |d| Random.new.rand(89..96) },
          rental_income_user_estimated_rent: proc { |d| @proforma.rental_income_user_estimated_rent.to_i },
          sale_income_user_estimated_resale: proc { |d| @proforma.sale_income_user_estimated_resale.to_i },
          expanded_rental_price_to_list_price_mean: proc { |d| @stats_rental.format(:expanded_sale_price_to_list_price_mean, as: :percent_wo_decimal) },
          moderate_rental_price_to_list_price_mean: proc { |d| @stats_rental.format(:moderate_sale_price_to_list_price_mean, as: :percent_wo_decimal) },  
          conservative_rental_price_to_list_price_mean: proc { |d| @stats_rental.format(:conservative_sale_price_to_list_price_mean, as: :percent_wo_decimal) },
          expanded_sale_price_to_list_price_mean: proc { |d| @stats_sale.format(:expanded_sale_price_to_list_price_mean, as: :percent_wo_decimal) },
          moderate_sale_price_to_list_price_mean: proc { |d| @stats_sale.format(:moderate_sale_price_to_list_price_mean, as: :percent_wo_decimal) },  
          conservative_sale_price_to_list_price_mean: proc { |d| @stats_sale.format(:conservative_sale_price_to_list_price_mean, as: :percent_wo_decimal) },
          rental_expanded_avm_mean: proc { |d| @stats_rental.format(:expanded_avm_mean, as: :money_wo_decimal) },
          rental_moderate_avm_mean: proc { |d| @stats_rental.format(:moderate_avm_mean, as: :money_wo_decimal) },
          rental_conservative_avm_mean: proc { |d| @stats_rental.format(:conservative_avm_mean, as: :money_wo_decimal) },
          rental_expanded_avm_max: proc { |d| @stats_rental.format(:expanded_avm_max, as: :money_wo_decimal) },
          rental_moderate_avm_max: proc { |d| @stats_rental.format(:moderate_avm_max, as: :money_wo_decimal) },
          rental_conservative_avm_max: proc { |d| @stats_rental.format(:conservative_avm_max, as: :money_wo_decimal) },
          rental_expanded_sale_price_psf_mean: proc { |d| @stats_rental.format(:expanded_sale_price_psf_mean, as: :money_w_two_decimal) },
          rental_moderate_sale_price_psf_mean: proc { |d| @stats_rental.format(:moderate_sale_price_psf_mean, as: :money_w_two_decimal) },
          rental_conservative_sale_price_psf_mean: proc { |d| @stats_rental.format(:conservative_sale_price_psf_mean, as: :money_w_two_decimal) },
          rental_expanded_sale_price_psf_max: proc { |d| @stats_rental.format(:expanded_sale_price_psf_max, as: :money_w_two_decimal) },
          rental_moderate_sale_price_psf_max: proc { |d| @stats_rental.format(:moderate_sale_price_psf_max, as: :money_w_two_decimal) },
          rental_conservative_sale_price_psf_max: proc { |d| @stats_rental.format(:conservative_sale_price_psf_max, as: :money_w_two_decimal) },

          sale_expanded_avm_mean: proc { |d| @stats_sale.format(:expanded_avm_mean, as: :money_wo_decimal) },
          sale_moderate_avm_mean: proc { |d| @stats_sale.format(:moderate_avm_mean, as: :money_wo_decimal) },
          sale_conservative_avm_mean: proc { |d| @stats_sale.format(:conservative_avm_mean, as: :money_wo_decimal) },
          sale_expanded_avm_max: proc { |d| @stats_sale.format(:expanded_avm_max, as: :money_wo_decimal) },
          sale_moderate_avm_max: proc { |d| @stats_sale.format(:moderate_avm_max, as: :money_wo_decimal) },
          sale_conservative_avm_max: proc { |d| @stats_sale.format(:conservative_avm_max, as: :money_wo_decimal) },
          sale_expanded_sale_price_psf_mean: proc { |d| @stats_sale.format(:expanded_sale_price_psf_mean, as: :money_w_two_decimal) },
          sale_moderate_sale_price_psf_mean: proc { |d| @stats_sale.format(:moderate_sale_price_psf_mean, as: :money_w_two_decimal) },
          sale_conservative_sale_price_psf_mean: proc { |d| @stats_sale.format(:conservative_sale_price_psf_mean, as: :money_w_two_decimal) },
          sale_expanded_sale_price_psf_max: proc { |d| @stats_sale.format(:expanded_sale_price_psf_mean, as: :money_w_two_decimal) },
          sale_moderate_sale_price_psf_max: proc { |d| @stats_sale.format(:moderate_sale_price_psf_mean, as: :money_w_two_decimal) },
          sale_conservative_sale_price_psf_max: proc { |d| @stats_sale.format(:conservative_sale_price_psf_mean, as: :money_w_two_decimal) },

          rental_sold_sale_price_max: proc { |d| @stats_rental.format(:sold_sale_price_max, as: :money_wo_decimal)},
          rental_sold_sale_price_min: proc { |d| @stats_rental.format(:sold_sale_price_min, as: :money_wo_decimal)},
          sale_sold_sale_price_max: proc { |d| @stats_sale.format(:sold_sale_price_max, as: :money_wo_decimal)},
          sale_sold_sale_price_min: proc { |d| @stats_sale.format(:sold_sale_price_min, as: :money_wo_decimal)},
          rental_absorption_rate: proc { |d| @stats_rental.format(:absorption_rate, as: :decimal)},
          sale_absorption_rate: proc { |d| @stats_sale.format(:absorption_rate, as: :decimal)},

          rental_dom_mean:  proc {  |d| @stats_rental.format("#{@comp_level_rental}_dom_mean".to_sym)},
          sale_dom_mean:  proc {  |d| @stats_sale.format("#{@comp_level_sale}_dom_mean".to_sym)},
          rental_subject_to_comps_square_feet_mean: proc  {  |d|  n = (@property.square_feet / @stats_rental[:sold_square_feet_mean].to_s.to_d).round(2) ; n.try(:nan?) ? 0 : n},
          sale_subject_to_comps_square_feet_mean: proc  {  |d|  n = (@property.square_feet / @stats_sale[:sold_square_feet_mean].to_s.to_d).round(2) ; n.try(:nan?) ? 0 : n},
          rental_sold_count: proc { |d| @stats_rental.format(:sold_count, as: :integer)},
          rental_active_count: proc { |d| @stats_rental.format(:active_count, as: :integer).to_i + @stats_rental.format(:pending_count, as: :integer).to_i},
          rental_terminated_count: proc { |d|  @stats_rental.format(:terminated_count, as: :integer).to_i +  @stats_rental.format(:withdrawn_count, as: :integer).to_i +  @stats_rental.format(:expired_count, as: :integer).to_i},
          sale_sold_count: proc { |d| @stats_sale.format(:sold_count, as: :integer)},
          sale_active_count: proc { |d| @stats_sale.format(:active_count, as: :integer).to_i + @stats_sale.format(:pending_count, as: :integer).to_i},
          sale_terminated_count: proc { |d| @stats_sale.format(:terminated_count, as: :integer).to_i +  @stats_sale.format(:withdrawn_count, as: :integer).to_i +  @stats_sale.format(:expired_count, as: :integer).to_i},

          # rental_replacement_cost: proc { |d| (@stats_rental.format(:sold_sale_price_max, as: :money_wo_decimal).to_s.to_d * 1.25).round(2)},
          # rental_replacement_cost_percent: proc { |d| '87%'},
          # rental_peak_cost: proc { |d| (@stats_rental.format(:sold_sale_price_max, as: :money_wo_decimal).to_s.to_d * 1.25).round(2) + 35000},

          sale_replacement_cost: proc { |d| number_to_currency((@stats_sale[:sold_sale_price_max].to_s.to_d * 1.25).round(2), strip_insignificant_zeros: true)},
          sale_replacement_cost_percent: proc { |d| '87%'},
          sale_peak_cost: proc { |d| number_to_currency((@stats_sale[:sold_sale_price_max].to_s.to_d * 1.25).round(2) + 35000, strip_insignificant_zeros: true)},
          rental_comps: proc { |d| 

            @stats_rental[:comps].inject([]) { |c,k|
              puts "c: #{c}"
              p = Property.find(k)
              x = nil
              if p.latitude 
                x = Haversine.distance(p.latitude,p.longitude,@property.latitude,@property.longitude).to_miles.round(4) 
              end
              if x
                if p.status == 'sold'
                  q = {
                         c: [
                            {v: x},
                            {v: p.list_price}
                         ] 
                      }
                  c << q 
                elsif p.status == 'active'
                  q = {
                         c: [
                            {v: x},
                            {},
                            {v: p.list_price}
                         ] 
                      }
                  c << q 
                end

                
              end
              c

            }
          },
          sale_comps: proc { |d| 

            @stats_sale[:comps].inject([]) { |c,k|
              puts "c: #{c}"
              p = Property.find(k)
              x = nil
              if p.latitude 
                x = Haversine.distance(p.latitude,p.longitude,@property.latitude,@property.longitude).to_miles.round(4) 
              end
              if x
                if p.status == 'sold'
                  q = {
                         c: [
                            {v: x},
                            {v: p.list_price}
                         ] 
                      }
                  c << q 
                elsif p.status == 'active'
                  q = {
                         c: [
                            {v: x},
                            {},
                            {v: p.list_price}
                         ] 
                      }
                  c << q 
                end

                
              end
              c

            }
          },
        }
        rawp = Property.find(params[:id])
        rslt = rawp.as_json
        Property.set_user_id(current_user.id)
        
        
        i = 0 
        
        hsh = {}
        hsh_map.each do |k,v|
          puts "is Proc #{k}?: #{v.is_a?(Proc)}"

          logger.debug "k:#{k}"
          if (k.to_s == "id")
            hsh[k] = rawp.id
          elsif (k.to_s == "latitude") 
            hsh[k] = rawp.latitude
          elsif (k.to_s == "longitude") 
            hsh[k] = rawp.longitude
          elsif v.is_a?(Proc)
            hsh[k] = v.call()
          else

            if (k.to_s == "offer_date")
              hsh[k] = ""
            elsif (rawp.user_client_proforma.respond_to?(k.to_s))
              
              hsh[k] = rawp.user_client_proforma.format(k.to_s)
            elsif (k.to_s == 'tag')
              hsh[k] = rawp.format_tag(current_user.id)
            else
              hsh[k] = rawp.format(k.to_sym)
            end

          end
      
        end
        
        # big_result = {} 
        # big_result["data"] = hsh
        outp = {data: [ hsh ], draw: 0, recordsTota: 11,recordsFiltered: 11}
        
        render json:  outp
        
      }
      format.html { redirect_to summary_client_market_property_url }
    end
    # redirect_to summary_client_market_property_url
  end
  
  def update
    puts "@property: #{@property.id}"
    puts "params #{params}"
    # assigning_params = params["property"]
    assigning_params = params["property"].inject({}) do |mem,(k,v)|
      puts "k,v= #{k},#{v} #{v.blank?}"
      if v.blank?
        mem[k] = nil
      else
        mem[k] = v
      end
      mem
    end
    puts "assigning params #{assigning_params}"
    respond_to do |format|
      @property.assign_attributes(assigning_params)
      if @property.save
        format.json { head :no_content, status: :ok }
        format.xml { head :no_content, status: :ok }
      else
        format.json { render json: @property.errors, status: :unprocessable_entity }
        format.xml { render xml: @property.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def summary
    @history = @property.history.page(params[:page])
    
    @category_rental = :rental

    @subject_property_rental = @market.properties.where(["(id in (?)) ",@property.id])
      if session[:data_per_page]
        @properties = @property.comparables(@category_rental)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        
        if per_page == 0
          @properties = @properties.page(1).per_page(prop_count)
          
        elsif prop_count <  per_page
          @properties = @properties.page(1).per_page(prop_count)
        elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
          @properties = @properties.page(1).per_page(per_page)
        else
          @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page)
        end
        
        
      else
        @properties = @property.comparables(@category_rental)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        @properties = @properties.page(1).per_page(prop_count)
      end
      Delayed::Worker.logger.debug "@properties #{@properties}"
      Delayed::Worker.logger.debug "@subject_property #{@subject_property_rental}"
      @subject_property_rental = @subject_property_rental.page(1)
      
      # @comp_properties =
      @stats_rental = @property.send("comps_#{@category_rental}_stats")
      
      # comps.try(:index,property.id)
      # @stats_rental[:comp_grades]
      Delayed::Worker.logger.debug "stat_comps #{@stats_rental[:comps]}"
      
      Delayed::Worker.logger.debug "Return new comp level: #{@assignment.definition}"
      @comp_level_rental = @assignment.definition.send("return_new_#{@category_rental}_comp_level".to_sym)  
      Delayed::Worker.logger.debug "comp level #{@comp_level_rental}"     
      
      level_list = case @comp_level_rental
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
      @stats_comps_rental = @stats_rental[:comps] ?  @stats_rental[:comps]  : []
      @stats_comp_grades_rental = @stats_rental[:comp_grades] ? @stats_rental[:comp_grades] : []
      scomp_indices = @stats_comp_grades_rental.each_index.select{|i| @stats_comp_grades_rental[i].in?(level_list)}
      non_scomp_indices = @stats_comp_grades_rental.each_index.select{|i| !@stats_comp_grades_rental[i].in?(level_list)}
      Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
      
      @selected_comps_rental = @stats_comps_rental.values_at(*scomp_indices)
      @selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*scomp_indices)
      @non_selected_comps_rental = @stats_comps_rental.values_at(*non_scomp_indices)
      @non_selected_comp_grades_rental = @stats_comp_grades_rental.values_at(*non_scomp_indices)
      @comp_properties_rental = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_rental])
      @comp_properties_rental = @comp_properties_rental.page(1).per_page(@comp_properties_rental.count)
      
      
      
      
      
      @non_comp_properties_rental = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_rental, @selected_comps_rental])
      @non_comp_properties_rental = @non_comp_properties_rental.page(1).per_page(@non_comp_properties_rental.count)
       # arr.each_index.select{|i| arr[i] == 'x'}
      Delayed::Worker.logger.debug "test comparable user comp: #{ @proforma.try("comparables_user_comp_list_#{@category_rental}".to_sym).to_s}"
      comparables_user_comp_list_rental = @proforma.try("comparables_user_comp_list_#{@category_rental}".to_sym).to_s.split(",").map(&:to_i)
      Delayed::Worker.logger.debug "@comparables_user_comp_list_rental: #@comparables_user_comp_list_rental}"
      
      @comparables_user_comp_list_rental = []
      
      if (comparables_user_comp_list_rental && (comparables_user_comp_list_rental.length > 0))
        @comparables_user_comp_list_rental =  (@comp_properties_rental + @non_comp_properties_rental).map(&:id).inject([]) do |rst,elem|
          Delayed::Worker.logger.debug "elem #{elem.class}"
          rst << elem if elem.in?(comparables_user_comp_list_rental) 
          rst
        end
        
        
      end
      Delayed::Worker.logger.debug "@comparables_user_comp_list_rental after inject: #{@comparables_user_comp_list_rental}"
      
      if @comparables_user_comp_list_rental.length != comparables_user_comp_list_rental.length
          
        recs = @market.properties.select("sale_price_psf").where(["(id in (?)) AND status in ('sold')",@comparables_user_comp_list_rental]).map(&:sale_price_psf)
        
        category = @category_rental
        Delayed::Worker.logger.debug "recs is: #{recs}"        
        Delayed::Worker.logger.debug "cat is: #{category.class}"
        case category
        when :sale
          Delayed::Worker.logger.debug "sale entering"
          p_psf = (@comparables_user_comp_list_rental ? (Math.sum(*recs) / @comparables_user_comp_list_rental.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_sale = p_psf
          @proforma.comparables_user_avm_sale = ((p_psf.nil? || @property.square_feet.nil?) ? nil : (p_psf * @property.square_feet))
        when :rental
          Delayed::Worker.logger.debug "rental entering"
          p_psf = (@comparables_user_comp_list_rental ? (Math.sum(*recs) / @comparables_user_comp_list_rental.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_rental = p_psf
          @proforma.comparables_user_avm_rental = (p_psf.nil? || @property.square_feet.nil?) ? nil : p_psf * @property.square_feet
        end
        
        Delayed::Worker.logger.debug "sp_psf is: #{@proforma.comparables_user_sale_price_psf_sale}"
        Delayed::Worker.logger.debug "avm_sale is: #{@proforma.comparables_user_avm_sale}"
         
      end
      Delayed::Worker.logger.debug "after loop @comparables_user_comp_list_rental #{@comparables_user_comp_list_rental}"
    
      summary_sale
  end

  def summary_sale
    @category_sale = :sale

    @subject_property_sale = @market.properties.where(["(id in (?)) ",@property.id])
      if session[:data_per_page]
        @properties = @property.comparables(@category_sale)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        
        if per_page == 0
          @properties = @properties.page(1).per_page(prop_count)
          
        elsif prop_count <  per_page
          @properties = @properties.page(1).per_page(prop_count)
        elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
          @properties = @properties.page(1).per_page(per_page)
        else
          @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page)
        end
        
        
      else
        @properties = @property.comparables(@category_sale)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        @properties = @properties.page(1).per_page(prop_count)
      end
      Delayed::Worker.logger.debug "@properties #{@properties}"
      Delayed::Worker.logger.debug "@subject_property #{@subject_property_sale}"
      @subject_property_sale = @subject_property_sale.page(1)
      
      # @comp_properties =
      @stats_sale = @property.send("comps_#{@category_sale}_stats")
      
      # comps.try(:index,property.id)
      # @stats_sale[:comp_grades]
      Delayed::Worker.logger.debug "stat_comps #{@stats_sale[:comps]}"
      
      Delayed::Worker.logger.debug "Return new comp level: #{@assignment.definition}"
      @comp_level_sale = @assignment.definition.send("return_new_#{@category_sale}_comp_level".to_sym)  
      Delayed::Worker.logger.debug "comp level #{@comp_level_sale}"     
      
      level_list = case @comp_level_sale
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
      @stats_comps_sale = @stats_sale[:comps] ?  @stats_sale[:comps]  : []
      @stats_comp_grades_sale = @stats_sale[:comp_grades] ? @stats_sale[:comp_grades] : []
      scomp_indices = @stats_comp_grades_sale.each_index.select{|i| @stats_comp_grades_sale[i].in?(level_list)}
      non_scomp_indices = @stats_comp_grades_sale.each_index.select{|i| !@stats_comp_grades_sale[i].in?(level_list)}
      Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
      
      @selected_comps_sale = @stats_comps_sale.values_at(*scomp_indices)
      @selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*scomp_indices)
      @non_selected_comps_sale = @stats_comps_sale.values_at(*non_scomp_indices)
      @non_selected_comp_grades_sale = @stats_comp_grades_sale.values_at(*non_scomp_indices)
      @comp_properties_sale = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps_sale])
      @comp_properties_sale = @comp_properties_sale.page(1).per_page(@comp_properties_sale.count)
      
      
      
      
      
      @non_comp_properties_sale = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps_sale, @selected_comps_sale])
      @non_comp_properties_sale = @non_comp_properties_sale.page(1).per_page(@non_comp_properties_sale.count)
       # arr.each_index.select{|i| arr[i] == 'x'}
      Delayed::Worker.logger.debug "test comparable user comp: #{ @proforma.try("comparables_user_comp_list_#{@category_sale}".to_sym).to_s}"
      comparables_user_comp_list_sale = @proforma.try("comparables_user_comp_list_#{@category_sale}".to_sym).to_s.split(",").map(&:to_i)
      Delayed::Worker.logger.debug "@comparables_user_comp_list_sale: #@comparables_user_comp_list_sale}"
      
      @comparables_user_comp_list_sale = []
      
      if (comparables_user_comp_list_sale && (comparables_user_comp_list_sale.length > 0))
        @comparables_user_comp_list_sale =  (@comp_properties_sale + @non_comp_properties_sale).map(&:id).inject([]) do |rst,elem|
          Delayed::Worker.logger.debug "elem #{elem.class}"
          rst << elem if elem.in?(comparables_user_comp_list_sale) 
          rst
        end
        
        
      end
      Delayed::Worker.logger.debug "@comparables_user_comp_list_sale after inject: #{@comparables_user_comp_list_sale}"
      
      if @comparables_user_comp_list_sale.length != comparables_user_comp_list_sale.length
          
        recs = @market.properties.select("sale_price_psf").where(["(id in (?)) AND status in ('sold')",@comparables_user_comp_list_sale]).map(&:sale_price_psf)

        category = @category_sale
        Delayed::Worker.logger.debug "recs is: #{recs}"
        Delayed::Worker.logger.debug "cat is: #{category.class}"
        case category
        when :sale
          Delayed::Worker.logger.debug "sale entering"
          p_psf = (@comparables_user_comp_list_sale ? (Math.sum(*recs) / @comparables_user_comp_list_sale.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_sale = p_psf
          @proforma.comparables_user_avm_sale = ((p_psf.nil? || @property.square_feet.nil?) ? nil : (p_psf * @property.square_feet))
        when :rental
          Delayed::Worker.logger.debug "rental entering"
          p_psf = (@comparables_user_comp_list_sale ? (Math.sum(*recs) / @comparables_user_comp_list_sale.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_rental = p_psf
          @proforma.comparables_user_avm_rental = (p_psf.nil? || @property.square_feet.nil?) ? nil : p_psf * @property.square_feet
        end
        
        Delayed::Worker.logger.debug "sp_psf is: #{@proforma.comparables_user_sale_price_psf_sale}"
        Delayed::Worker.logger.debug "avm_sale is: #{@proforma.comparables_user_avm_sale}"
         
      end
      Delayed::Worker.logger.debug "after loop @comparables_user_comp_list_sale #{@comparables_user_comp_list_sale}"
  end
  
  def comment    
  end
  
  def save
    
    if params[:commit] == "Save" 
      begin
        if sub_save
          Delayed::Worker.logger.debug ">>>redirect to summary"
          redirect_to summary_client_market_property_url
        else
          Delayed::Worker.logger.debug ">>>@proforma #{@proforma.errors.full_messages}"
          Delayed::Worker.logger.debug ">>>@property #{@property.errors.full_messages}"
          Delayed::Worker.logger.debug ">>>user_assigned_property #{user_assigned_property.errors.full_messages}"
          Delayed::Worker.logger.debug ">>>redirect to second summary"
          render 'summary'
        end
      
      rescue Exception => exc
         Delayed::Worker.logger.debug ">>>exception #{exc.message}"
      end
    
    else
      
      @proforma.restore(true)
      redirect_to summary_client_market_property_url, :notice => 'Fields restored successfully!'
      
    end
  end
  
  def sub_save
    pname = nil
      
    case current_user.id
    when 21
      pname = "six_four_three_proforma"
    when 22
      pname = "ahr_three_five_zero_proforma"
    when 23
      pname = "invitation_proforma"
    when 24
      pname = "invitation_two_proforma"
    end
    
    Delayed::Worker.logger.debug ">>>params #{params}"
    @user_updated_at = Time.now
    Delayed::Worker.logger.debug ">>> property_only #{params[:property].reject{ |k| k.in?([pname,'config','sale','rental','other_sale','other_rental']) }}"
    
    Delayed::Worker.logger.debug ">>>>client_rep: #{params[:client_rep]}"
    Delayed::Worker.logger.debug ">>>>client_near_commercial: #{params[:client_near_commercial]}"

    prop_hsh = {}

    other_users = User.all.map(&:id).reject {|d| d == current_user.id  }
    cur_reps = @property.rep ? @property.rep.split(",") : []
    Delayed::Worker.logger.debug ">>>>other_users: #{other_users}"
    Delayed::Worker.logger.debug ">>>>cur_reps: #{cur_reps}"
    cur_reps_minus_self = cur_reps.reject{ |k| k == current_user.id.to_s }
    Delayed::Worker.logger.debug ">>>>cur_reps_minus_self: #{cur_reps_minus_self}"
    cur_reps_minus_self_str = cur_reps_minus_self.join(",")
    prop_hsh[:rep] = (cur_reps_minus_self_str.split(",") +   (params[:client_rep].is_a?(Array) ? params[:client_rep] : params[:client_rep].to_s.split(","))  ).reject(&:blank?).join(",")
    prop_hsh[:near_commercial] = params[:client_near_commercial]
    
    prop_hsh[:flood] = params[:client_flood]


    Delayed::Worker.logger.debug ">>>>prop hsh: #{prop_hsh}"
    
    
    
    assigning_hash = params[:property].reject{ |k| k.in?([pname,'config','sale','rental','other_sale','other_rental']) }.merge(prop_hsh).merge({:user_updated_at => @user_updated_at})
    Delayed::Worker.logger.debug ">>>>assigning_hash: #{assigning_hash}"
    @property.assign_attributes(assigning_hash)


    client_note =  nil

    if !@comment  
  
      client_note = @property.notes.new({:posted_by => current_user.id})  if !params[:client_notes].blank?  
      client_note.content = params[:client_notes]  if !params[:client_notes].blank?  
    else

      client_note =  @property.notes.last
      client_note.posted_by = current_user.id
    end

    if (!client_note.blank? && !params[:client_notes].blank?)
      client_note.content = params[:client_notes]
      client_note.save
    end
    
    if (!client_note.blank? && !client_note.new_record? && params[:client_notes].blank?)
      client_note.delete      
    end 

    
    user_assigned_property = @property.assigned_tags.find_or_initialize_by_user_id(current_user.id)
    user_assigned_property.assign_attributes({:tag => (params[:client_tag].blank? ?  nil : params[:client_tag])})
    
    
    Delayed::Worker.logger.debug ">>>proforma #{@proforma}"
    begin
      if !params[:property].try(:[],pname.to_sym).blank?
        @proforma.override_attributes(params[:property].try(:[],pname.to_sym))
      end  
      if user_assigned_property.save && @property.save && @proforma.save
        return true
  
      else
        return false
      end
    rescue Exception => exc
      raise exc
    end
    
  end
  
  def save_property
    # case current_user.id
    #   when 14
    #     pname = "six_four_three_proforma"
    #   when 20
    #     pname = "ahr_three_five_zero_proforma"
    #   when 21
    #     pname = "invitation_proforma"
    # end
    category = nil
    Delayed::Worker.logger.debug ">>>params #{params}"
    # @user_updated_at = Time.now
    # Delayed::Worker.logger.debug ">>> property_only #{params[:property].reject{ |k| k.in?([pname]) }}"
    
    # Delayed::Worker.logger.debug ">>>>client_rep: #{params[:client_rep]}"
    # Delayed::Worker.logger.debug ">>>>client_near_commercial: #{params[:client_near_commercial]}"

    # prop_hsh = {}

    # other_users = User.all.map(&:id).reject {|d| d == current_user.id  }
    # cur_reps = @property.rep ? @property.rep.split(",") : []
    # Delayed::Worker.logger.debug ">>>>other_users: #{other_users}"
    # Delayed::Worker.logger.debug ">>>>cur_reps: #{cur_reps}"
    # cur_reps_minus_self = cur_reps.reject{ |k| k == current_user.id.to_s }
    # Delayed::Worker.logger.debug ">>>>cur_reps_minus_self: #{cur_reps_minus_self}"
    # cur_reps_minus_self_str = cur_reps_minus_self.join(",")
    # prop_hsh[:rep] = [cur_reps_minus_self_str,params[:client_rep].is_a?(Array) ? params[:client_rep].try(:join,",") : params[:client_rep]].reject(&:blank?).join(",")
    # prop_hsh[:near_commercial] = params[:client_near_commercial]
    
    # prop_hsh[:flood] = params[:client_flood]


    # Delayed::Worker.logger.debug ">>>>prop hsh: #{prop_hsh}"
    
    # assigning_hash = params[:property].reject{ |k| k.in?([pname]) }.merge(prop_hsh).merge({:user_updated_at => @user_updated_at})
    # Delayed::Worker.logger.debug ">>>>assigning_hash: #{assigning_hash}"
    # @property.assign_attributes(assigning_hash)
    assigning_hash = params[:property].reject{ |k| k.in?([@pclass,'config','sale','rental','other_sale','other_rental']) }.merge({:user_updated_at => @user_updated_at})
    # @property.assign_attributes(params[:property])
    @property.assign_attributes(assigning_hash)
   
    @proforma.override_attributes(params[:property].try(:[],@pclass.to_sym))  
    # Delayed::Worker.logger.debug "Config ! id #{ params[:config].try(:[],'id')}"
    # Delayed::Worker.logger.debug "config category: #{params[:property].try(:[],'config').try(:[],'category')}"
    # if params[:property].try(:[],'config').try(:[],'category')
    #   category = params[:property].try(:[],'config').try(:[],'category')

    # end
    if params[:property].try(:[],'rental').try(:[],'id')
      user_comp_list_rental =  [params[:property].try(:[],'rental').try(:[],'id') ? params[:property].try(:[],'rental').try(:[],'id').join(",") : nil , params[:property].try(:[],'other_rental').try(:[],'id') ? params[:property].try(:[],'other_rental').try(:[],'id').join(",") : nil].reject(&:blank?).join(',')
      Delayed::Worker.logger.debug "User comp list #{user_comp_list_rental}"
      @proforma.override_attributes({"comparables_user_comp_list_rental".to_sym => user_comp_list_rental})
    else

      @proforma.override_attributes({"comparables_user_comp_list_rental".to_sym => nil})
    end

    if params[:property].try(:[],'sale').try(:[],'id')
      user_comp_list_sale =  [params[:property].try(:[],'sale').try(:[],'id') ? params[:property].try(:[],'sale').try(:[],'id').join(",") : nil ,params[:property].try(:[],'other_sale').try(:[],'id') ? params[:property].try(:[],'other_sale').try(:[],'id').join(",") : nil].reject(&:blank?).join(',')
      Delayed::Worker.logger.debug "User comp list #{user_comp_list_sale}"
      @proforma.override_attributes({"comparables_user_comp_list_sale".to_sym => user_comp_list_sale})
    else

      @proforma.override_attributes({"comparables_user_comp_list_sale".to_sym => nil})
    end

    
    if (@property.save) && (@proforma.save) # && (user_assigned_property.save)
      true
    else
      Delayed::Worker.logger.debug "Property full error: #{@property.errors.full_messages}"
      Delayed::Worker.logger.debug "Proforma full error: #{@proforma.errors.full_messages}"
      false
    end  
    
    
  end
  
  def save_comment
          
      if (params[:commit] && params[:commit] == "Save")
        Delayed::Worker.logger.debug "params: #{params}"
        # @note.destroy
        
        if sub_save
          @property.compute
          
          redirect_to comment_client_market_property_url, notice: t('flash.actions.update.notice', resource_name: Property.model_name.human)
        else
          flash[:alert] = t('flash.actions.update.alert', resource_name: Property.model_name.human)
          render 'comment'
        end
      else
        @note = @property.notes.new({:posted_by => current_user.id})
        @user_updated_at = Time.now
        @property = Property.find(@note.property_id)
        
        @property.user_updated_at = Time.now
        # @property.save
        @note.override_attributes(params[:property][:note])
        
        respond_to do |format|
          if @note.save
            format.html {redirect_to(comment_client_market_property_url, notice: "Note created successfully!")}
            format.js
          else
            format.html {render :action => "comment"}
            format.js
          end
        end   
      end
  end
  
  def request_visit
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def request_visit_form
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def schedule_visit
    # Delayed::Worker.logger.debug "Schedule visit fired" 
    p = {}
    timenow = Time.now
    p["address"] = params["property"]["address"]
    p["client_access"] = params["client_access"]
    p["request_date"] = timenow.strftime("%m/%d/%Y")
    p["request_time"] = timenow.strftime("%I:%M %p")
    p["scheduled_date"] = params["scheduled_date"]
    p["scheduled_time"] = params["scheduled_time"]
    p["username"] = params["username"]
    
    Mailer.delay.schedule_result(p) 
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def comps_rental_export
   
    
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    load_comps_only(:rental,sort_stmt)
    coordinate = {}
    coordinate = {latitude: @property.latitude,longitude: @property.longitude} if !@property.latitude.nil? && !@property.longitude.nil?
    download(:csv, 'report', @properties.to_csv2(current_user.preferences.columns_comps,coordinate,current_user.id))
  end
  
  def comps_sale_export
    sort = sort_asc ? 'ASC' : 'DESC'
    if sort_by.in?([:id,:city,:address,:state,:zip])
      sort_stmt = "properties.#{sort_by} #{sort}"
    
    else
       sort_stmt = "#{sort_by} #{sort}"
    end
    
    load_comps_only(:sale,sort_stmt)
    coordinate = {}
    coordinate = {latitude: @property.latitude,longitude: @property.longitude} if !@property.latitude.nil? && !@property.longitude.nil?
    Delayed::Worker.logger.debug "~~coordinate #{coordinate}"
    download(:csv, 'report', @properties.to_csv2(current_user.preferences.columns_comps,coordinate,current_user.id))    
  end
  
  def update_near_commercial
     Delayed::Worker.logger.debug "~~near_commercial #{params[:near_commercial]}"
     @user_updated_at = Time.now
     @property.override_attributes({:near_commercial => (params[:near_commercial].blank? ?  nil : params[:near_commercial]), :user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @property.save
          format.html {redirect_to(summary_client_market_property_url, notice: "Near commercial updated successfully!")}
          format.js
        end
      end   
  end
  
  def update_rep
     Delayed::Worker.logger.debug "~~ param rep: #{params[:values]}"
     @user_updated_at =  Time.now
     @property.override_attributes({:rep => (params[:values].blank? ?  nil : params[:values]), :user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @property.save
          format.html {redirect_to(summary_client_market_url, notice: "Rep updated successfully!")}
          format.js 
        else
          format.js { render :action => "update_rep_failure", :locals => {:id => @property.rep_was} }
        end
      end 
  
  end
  
  def update_tag
     
     @user_updated_at = Time.now
     @user_assigned_property = @property.assigned_tags.find_or_initialize_by_user_id(current_user.id)
     @user_assigned_property.assign_attributes({:tag => (params[:tag].blank? ?  nil : params[:tag])})
     @property.assign_attributes({:user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @user_assigned_property.save && @property.save
          format.html {redirect_to(summary_client_market_property_url, notice: "Tag updated successfully!")}
          format.js
        end
      end   
  end

  def update_flood
     Delayed::Worker.logger.debug "~~flood #{params[:flood].nil?}"
     @user_updated_at = Time.now
     @property.override_attributes({:flood => (params[:flood].blank? ?  nil : params[:flood]) , :user_updated_at => @user_updated_at})
          
     respond_to do |format|
        if @property.save
          format.html {redirect_to(summary_client_market_property_url, notice: "Flood updated successfully!")}
          format.js
        end
      end   
  end
  
  def comps_rental
    load_comps(self,:rental)
  end

  def comps_sale
    load_comps(self,:sale)
  end

  def comps_build
    load_comps(self,:build)
  end
  
  # def recompute(force=true)
     # Delayed::Worker.logger.debug "~~ params #{params}"
    # if (params[:commit] == "Recompute")
#       
      # Delayed::Worker.logger.debug "~~ force? #{force ? true : false}"
      # if force
        # @property.compute(force)
        # redirect_to request.referrer
      # elsif (@property = resource) && (@property.comps_updated_at.nil? || @property.comps_updated_at <= 1.day.ago)
        # @property.compute
      # end
#            
    # else  
      # if save_property
        # redirect_to request.referrer
      # else
        # redirect_to request.referrer
      # end
    # end
#     
  # end
  
  def recompute(force=true)
    Delayed::Worker.logger.debug "~~ force? #{force ? true : false}"
    
    destination_url = case  params[:property].try(:[],'config').try(:[],'category')
                     when "sale"
                      summary_client_market_property_url
                     when "rental"
                      summary_client_market_property_url
                     end
    
    if (params[:commit] == "Recompute")
      if force
        @property.compute(force)
        redirect_to summary_client_market_property_url, :notice => 'Record computed successfully!'

      elsif (@property == resource) && (@property.comps_updated_at.nil? || @property.comps_updated_at <= 1.day.ago)
        @property.compute
        redirect_to summary_client_market_property_url, :notice => 'Record computed successfully!'
      end
    elsif  (params[:commit] == "Save")
      if sub_save && save_property
        Delayed::Worker.logger.debug "~~ recompute with save "
        redirect_to summary_client_market_property_url, :notice => 'Record saved successfully!'
      else
        summary
        # Delayed::Worker.logger.debug "~~ recompute without save "
        # redirect_to summary_client_market_property_url
        # render 'summary'
        # load_comps_without_render(params[:property].try(:[],'config').try(:[],'category').to_sym)
        render :action => :summary, :template => 'client/properties/summary'

      # else
      #   Delayed::Worker.logger.debug "proforma error #{@proforma.errors.full_messages}"
      #   # comps_sale
      #   render  request.referrer # action: "comps_#{params[:property].try(:[],'config').try(:[],'category')}"
        # comps_#{params[:property].try(:[],'config').try(:[],'category')}"
      end
    else
      @proforma.restore(true)
      redirect_to summary_client_market_property_url, :notice => 'Fields restored successfully!'
    end
  end
  
  def set_per_page
    if params[:per_page]
      puts "## set per page #{params[:per_page]}"
      session[:per_page] = params[:per_page]
      puts "## session per page #{session[:per_page]}"
      puts "### request full #{request.fullpath}"
      
      # redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
    else
      session[:per_page] = nil
    end
    
    full_path = request.original_url
    Delayed::Worker.logger.debug "before full path #{full_path}"
    Delayed::Worker.logger.debug "after full path #{full_path}"
    full_path_elements = full_path.split('/set_per_page')
    @redirect_path = full_path_elements[0] + '/saved' 
    
    %w{page per_page}.each do |k|
      if ! (@redirect_path.include?('page') || @redirect_path.include?('per_page') )
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "?#{k}=1" ).html_safe  
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "?#{k}=#{params[k.to_sym]}" ).html_safe  
        end
         
      else
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "&#{k}=1".html_safe ).html_safe 
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "&#{k}=#{params[k.to_sym]}".html_safe ).html_safe 
        end
      end
    end
    
  end
  
  def client_rental_data_set_per_page
    puts "xxx params: #{params}"
    if params[:per_page]
      puts "## set per page #{params[:per_page]}"
      session[:data_per_page] = params[:per_page]
      puts "## session per page #{session[:per_page]}"
      puts "### request full #{request.fullpath}"
      
      # redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
    else
      session[:data_per_page] = nil
    end
    
    full_path_elements = request.original_url.split('/client_rental_data_set_per_page')
    Delayed::Worker.logger.debug "##full_path #{full_path_elements[1]}"
    @redirect_path = full_path_elements[0] +  "/" + params[:data_action] 

    %w{page per_page}.each do |k|
      if ! (@redirect_path.include?('page') || @redirect_path.include?('per_page') )
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "?#{k}=1" ).html_safe  
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "?#{k}=#{params[k.to_sym]}" ).html_safe  
        end
         
      else
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "&#{k}=1".html_safe ).html_safe 
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "&#{k}=#{params[k.to_sym]}".html_safe ).html_safe 
        end
      end
    end
    
  end

  def client_sale_data_set_per_page
    puts "xxx params: #{params}"
    if params[:per_page]
      puts "## set per page #{params[:per_page]}"
      session[:data_per_page] = params[:per_page]
      puts "## session per page #{session[:per_page]}"
      puts "### request full #{request.fullpath}"
      
      # redirect_to url_for(action: 'show', id: 'go', q: @search.config.attributes)
    else
      session[:data_per_page] = nil
    end
    
    full_path_elements = request.original_url.split('/client_sale_data_set_per_page')
    Delayed::Worker.logger.debug "##full_path #{full_path_elements[1]}"
    @redirect_path = full_path_elements[0] +  "/" + params[:data_action] 
    puts "### redirect path  #{@redirect_path }"

    %w{page per_page}.each do |k|
      if ! (@redirect_path.include?('page') || @redirect_path.include?('per_page') )
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "?#{k}=1" ).html_safe  
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "?#{k}=#{params[k.to_sym]}" ).html_safe  
        end
         
      else
        if params[k.to_sym] && (k == "page")
          @redirect_path =  (@redirect_path   + "&#{k}=1".html_safe ).html_safe 
        elsif params[k.to_sym]
          @redirect_path =  (@redirect_path   + "&#{k}=#{params[k.to_sym]}".html_safe ).html_safe 
        end
      end
    end
    
  end
  
  def remove_saved
    @property ||= resource
    MyProp.delete_all(["property_id = ? and user_id = ?", @property.id, current_user.id])
    redirect_to request.referer, notice: "The property has been removed from My Props successfully!"
  end
  
  def add_saved
    @property ||= resource
    
    my_prop = @property.my_props.find_or_initialize_by_user_id(current_user.id)
    my_prop.save
    
    redirect_to request.referer, notice: "The property has been saved to My Props successfully!"
  end

  protected

    def begin_of_association_chain
      @market_assignment
    end
    
    def collection
      @properties = end_of_association_chain.latest.page(params[:page])
    end

    def load_property
      
      force_restore = false
      @property ||= resource
    end

    def load_pclass
      
      
      @pclass = nil
      
      case current_user.id
      when SFT_USER_ID
        @pclass = "six_four_three_proforma"
      when ATFZ_USER_ID
        @pclass = "ahr_three_five_zero_proforma"
      when INV_USER_ID
        Delayed::Worker.logger.debug "@@ invitation proforma"
        @pclass = "invitation_proforma"
      when INV_TWO_USER_ID
        Delayed::Worker.logger.debug "@@ invitation two proforma"
        @pclass = "invitation_two_proforma"
      end      
      
      # end
    end

    def load_proforma
      
      force_restore = false
      @property ||= resource
      
      sps = nil
      
      
      begin
        
        
        sps = @pclass.classify.constantize.where(["user_id = ? and property_id = ?", current_user.id, @property.id])
        
        
        if sps  
          if sps.count == 0
            @proforma = @pclass.classify.constantize.new(user_id: current_user.id, property_id: @property.id)
          else
            @proforma = sps.first
          end
          
          load_top_comment
          # @property.six_fo.find_or_initialize_by_user_id(current_user.id)
          # sys_prof = @property.client_proforma
          # if (sys_prof && !@proforma.new_record?) && (!sys_prof.new_record? && sys_prof.updated_at > @proforma.updated_at )
            # force_restore = true
          # end
    
          # if  force_restore
            # @proforma.restore(true)
            # @proforma.save
            # @proforma.send(:calculate_fields) 
            # @proforma.save
          # else
          Delayed::Worker.logger.debug "@@ about to enter"
          if @proforma.new_record? || @proforma.updated_at < 1.day.ago
            Delayed::Worker.logger.debug "@@ into the block"
            @proforma.restore(true)
            @proforma.save
          end
        end
      rescue Exception => e
        Delayed::Worker.logger.debug "e: #{e.message}"
      end
      # end
    end
    
    def load_comps(prop,category)
      
      load_comps_without_render(category)
      render 'comps'
    end
    
    def load_comps_without_render(category)
      @category = category

      params[:sort_by] =  :sale_price_psf if ! params[:sort_by].try(:to_sym)      
      params[:sort_asc] =  params[:sort_asc].blank? ? "false" :  params[:sort_asc]
      
      @subject_property = @market.properties.where(["(id in (?)) ",@property.id])
      if session[:data_per_page]
        @properties = @property.comparables(category)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        
        if per_page == 0
          @properties = @properties.page(1).per_page(prop_count)
          
        elsif prop_count <  per_page
          @properties = @properties.page(1).per_page(prop_count)
        elsif  (prop_count.to_d/per_page.to_d).ceil < page_no
          @properties = @properties.page(1).per_page(per_page)
        else
          @properties = @properties.page((page_no == 0)? nil : page_no).per_page(per_page)
        end
        
        
      else
        @properties = @property.comparables(category)
        prop_count = @properties.count
        per_page = session[:data_per_page].to_i
        page_no = params[:page].to_i
        @properties = @properties.page(1).per_page(prop_count)
      end
      Delayed::Worker.logger.debug "@properties #{@properties}"
      Delayed::Worker.logger.debug "@subject_property #{@subject_property}"
      @subject_property = @subject_property.page(1)
      
      # @comp_properties =
      @stats = @property.send("comps_#{category}_stats")
      
      # comps.try(:index,property.id)
      # @stats[:comp_grades]
      Delayed::Worker.logger.debug "stat_comps #{@stats[:comps]}"
      
      Delayed::Worker.logger.debug "Return new comp level: #{@assignment.definition}"
      @comp_level = @assignment.definition.send("return_new_#{@category}_comp_level".to_sym)  
      Delayed::Worker.logger.debug "comp level #{@comp_level}"     
      
      level_list = case @comp_level
                    when 'expanded'
                     ['A','B','C']
                    when 'moderate'
                     ['A','B']
                    when 'conservative'
                      ['A']
                    else
                      ['A','B','C','D']
                    end
      @stats_comps = @stats[:comps] ?  @stats[:comps]  : []
      @stats_comp_grades = @stats[:comp_grades] ? @stats[:comp_grades] : []
      scomp_indices = @stats_comp_grades.each_index.select{|i| @stats_comp_grades[i].in?(level_list)}
      non_scomp_indices = @stats_comp_grades.each_index.select{|i| !@stats_comp_grades[i].in?(level_list)}
      Delayed::Worker.logger.debug "scomp_indices #{scomp_indices}"
      
      @selected_comps = @stats_comps.values_at(*scomp_indices)
      @selected_comp_grades = @stats_comp_grades.values_at(*scomp_indices)
      @non_selected_comps = @stats_comps.values_at(*non_scomp_indices)
      @non_selected_comp_grades = @stats_comp_grades.values_at(*non_scomp_indices)
      @comp_properties = @market.properties.where(["(id in (?)) AND status in ('sold')",@selected_comps])
      @comp_properties = @comp_properties.page(1).per_page(@comp_properties.count)
      
      
      
      
      
      @non_comp_properties = @market.properties.where(["(id in (?)) OR (id in (?) AND status not in ('sold')) ",@non_selected_comps, @selected_comps])
      @non_comp_properties = @non_comp_properties.page(1).per_page(@non_comp_properties.count)
       # arr.each_index.select{|i| arr[i] == 'x'}
      Delayed::Worker.logger.debug "test comparable user comp: #{ @proforma.try("comparables_user_comp_list_#{@category}".to_sym).to_s}"
      comparables_user_comp_list = @proforma.try("comparables_user_comp_list_#{@category}".to_sym).to_s.split(",").map(&:to_i)
      Delayed::Worker.logger.debug "@comparables_user_comp_list: #@comparables_user_comp_list}"
      
      @comparables_user_comp_list = []
      
      if (comparables_user_comp_list && (comparables_user_comp_list.length > 0))
        @comparables_user_comp_list =  @comp_properties.map(&:id).inject([]) do |rst,elem|
          Delayed::Worker.logger.debug "elem #{elem.class}"
          rst << elem if elem.in?(comparables_user_comp_list) 
          rst
        end
        
        
      end
      Delayed::Worker.logger.debug "@comparables_user_comp_list after inject: #{@comparables_user_comp_list}"
      
      if @comparables_user_comp_list.length != comparables_user_comp_list.length
          
        recs = @market.properties.select("sale_price_psf").where(["(id in (?)) AND status in ('sold')",@comparables_user_comp_list]).map(&:sale_price_psf)
        Delayed::Worker.logger.debug "recs is: #{recs}"
        Delayed::Worker.logger.debug "cat is: #{category.class}"
        case category
        when :sale
          Delayed::Worker.logger.debug "sale entering"
          p_psf = (@comparables_user_comp_list ? (Math.sum(*recs) / @comparables_user_comp_list.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_sale = p_psf
          @proforma.comparables_user_avm_sale = ((p_psf.nil? || @property.square_feet.nil?) ? nil : (p_psf * @property.square_feet))
        when :rental
          Delayed::Worker.logger.debug "rental entering"
          p_psf = (@comparables_user_comp_list ? (Math.sum(*recs) / @comparables_user_comp_list.length).round(2) : nil)
          @proforma.comparables_user_sale_price_psf_rental = p_psf
          @proforma.comparables_user_avm_rental = (p_psf.nil? || @property.square_feet.nil?) ? nil : p_psf * @property.square_feet
        end
        
        Delayed::Worker.logger.debug "sp_psf is: #{@proforma.comparables_user_sale_price_psf_sale}"
        Delayed::Worker.logger.debug "avm_sale is: #{@proforma.comparables_user_avm_sale}"
         
      end
      Delayed::Worker.logger.debug "after loop @comparables_user_comp_list #{@comparables_user_comp_list}"
      return nil
       # arr.each_index.select{|i| arr[i] == 'x'}
      
      
    end  
    
    def load_comps_only(category, sort_stmt)
      force_restore = false
      @property ||= resource
      
      @category = category
      
      @properties = @property.comparables(category).where(["status = ?","sold"]).order(sort_stmt)
         
    end
    
    def load_comment
      @property ||= resource
      @note = @property.notes.new({:posted_by => current_user.id})
      
    end
    
    def load_top_comment
      @property ||= resource
      @comment = @property.notes.try(:last)
    end
    
    def set_user_role
      @property ||= resource
      Delayed::Worker.logger.debug "~~ current_user.role #{current_user.role}"
      if ((!current_user.admin?)  &&  (current_user.role != 'analyst')) 
        @property.is_non_admin = true 
      end
      @property.current_user_id = current_user.id 
    end
    
    

  
end

