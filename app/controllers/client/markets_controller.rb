class Client::MarketsController < Client::BaseController
  before_filter proc { load_market_and_assignment(:id) }
  acts_as_resource do
    actions :all, except: [:show]
  end
  
  def show
    if can? :create, Search
      redirect_to new_client_market_search_url(@market)
    elsif can? :create, Job
      redirect_to new_client_market_upload_url(@market)
    end
  end
  
  def update
    
     if params[:restore]
      @assignment.restore!
      redirect_to url_for(:action => :edit), :notice => 'Settings restored successfully from market default values!'
    else
      if @assignment.update_attributes(params[:market_assignment])
        redirect_to url_for(:action => :edit), :notice => 'Settings saved successfully!'
      else
        flash.now[:error] = 'Settings could not be saved, check errors!'
        render 'edit'
      end
    end
     # @market.assign_attributes(params[:market])
     # if params[:confirm]
       # perform_update
     # else       
       # if !@market.assumptions_changed?
         # perform_update
       # else
         # flash.now[:warning] = 'With assumptions changed, you will need to recomputes all properties in this market.<br> Are you sure you want to continue saving changes?'.html_safe
         # render 'edit'
       # end
     # end
     
     
    
  end
  
  # def update
    # update! do
#       
      # collection_url
#       
    # end
    # if @market.save?
      # edit_resource_url(@market)
    # else
      # collection_url
    # end
     
    
    # if @market.assumption_changed?
      # edit_resource_url(@market)
    # else
      # update!
      # collection_url
    # end
    
    # puts "market rental_comp_query: #{@market.rental_comp_query}"
    # puts "market rental_comp_query changed?: #{@market.rental_comp_query_changed?}"
#     
    # rental_comp_query = @market.rental_comp_query
#     
    # update! do
      # puts "market rental_comp comparison: #{rental_comp_query} vs  #{@market.rental_comp_query}"
      # if rental_comp_query !=  @market.rental_comp_query
        # edit_resource_url(@market)
      # else 
        # # @market.save
        # collection_url
      # end
    # end
    # @market = Market.new(params[:market])
    # update!
    # if @market.assumption_changed?
      # puts "aaaa"
      # redirect_to "http://www.google.co.th"
      # # redirect_to edit_resource_url(:market => params[:market] )
    # else
      # redirect_to collection_url
    # end
  # end
  
  protected

    def collection
      @markets = Market.includes(:users).page(params[:page])
    end
    
    def perform_update
      
      m  = @market.save
      Delayed::Worker.logger.debug "m: #{@market.errors.full_messages}"
      if m
        redirect_to url_for(:action => :index), :notice => 'Settings saved successfully fu!'
      else
        flash.now[:error] = 'Settings could not be saved, check errors!'
        render 'edit'
      end
    end

end
