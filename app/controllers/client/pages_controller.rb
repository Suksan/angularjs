class Client::PagesController < Client::BaseController
  # before_filter :load_market_and_assignment
  before_filter :set_choices, only: [:edit_preferences, :update_preferences]
  before_filter :set_user, only: [:edit_preferences, :update_preferences, :change_password]

  def admin
    puts "gigi"
    redirect_to admin_markets_url
  end

  def client
    puts "sisi"
    redirect_to client_markets_url
  end

  def welcome
    puts "fifi"
    if !current_user.role.in?(["client"])
      redirect_to current_user.try(:markets).try(:first)
    else
      # redirect_to client_market_search_url
      redirect_to [:client,current_user.try(:markets).try(:first)]
    end
  end

  def edit_preferences
    puts "edit preferences"
    redirect_to edit_preferences_url if current_user.role != 'client'
    session[:return_to] = request.referrer
  end

  def update_preferences
    if @user.update_attributes(params[:user])
      redirect_to session[:return_to] || root_url, notice: 'Settings succesfully changed.'
    else
      flash.now[:error] = t('flash.actions.update.alert', resource_name: UserPreferences.model_name.human)
      render 'edit_preferences'
    end
  end  

  def change_password
  end

  def update_password
    @user = User.find(current_user.id)
    if @user.update_with_password(params[:user])
      # Sign in the user by passing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to root_path, :notice => 'Password succesfully changed.'
    else
      render "change_password"
    end
  end

  private

    def set_user
      @user = current_user
    end

    def set_choices
      @items = Property.active_attributes(delegates: [PropertyDetail, Proforma, ClientProforma], sort: true).inject({}) { |r, key| 
        r[key.to_s] = Property.label(key); r
      }
      
      
      @items = @items.merge({   "tag"             => "Label",
                                "comps_distance"                   => "Distance",
                                "sale_level1"                      => "Sale Comp Level 1",
                                "sale_level2"                      => "Sale Comp Level 2",
                                "sale_level3"                      => "Sale Comp Level 3",
                                "sale_level4"                      => "Sale Comp Level 4",
                                "sale_level5"                      => "Sale Comp Level 5",
                                "sale_level6"                      => "Sale Comp Level 6",
                                "sale_active_count"                => "Sale Active Count",
                                "sale_pending_count"               => "Sale Pending Count",
                                "sale_sold_count"                  => "Sale Sold Count",
                                "sale_terminated_count"            => "Sale Terminated Count",
                                "sale_withdrawn_count"             => "Sale Withdrawn Count",
                                "sale_expired_count"               => "Sale Expired Count",
                                "sale_sold_homes_rate"             => "Sale Sold Home Rate",
                                "sale_absorption_rate"             => "Sale Absorbtion Rate",
                                "sale_sold_square_feet_min"        => "Sale Sold Square Feet Min",
                                "sale_sold_square_feet_avg"        => "Sale Sold Square Feet Avg",
                                "sale_sold_square_feet_max"        => "Sale Sold Square Feet Max",
                                "sale_sold_square_feet_mean"       => "Sale Sold Square Feet Mean",
                                "sale_sold_beds_min"               => "Sale Sold Beds Min",
                                "sale_sold_beds_avg"               => "Sale Sold Beds Avg",
                                "sale_sold_beds_max"               => "Sale Sold Beds Max",
                                "sale_sold_beds_mean"              => "Sale Sold Beds Mean",
                                "sale_sold_bathrooms_min"          => "Sale Sold Bathrooms Min",
                                "sale_sold_bathrooms_avg"          => "Sale Sold Bathrooms Avg",
                                "sale_sold_bathrooms_max"          => "Sale Sold Bathrooms Max",
                                "sale_sold_bathrooms_mean"         => "Sale Sold Bathrooms Mean",
                                "sale_sold_lot_square_feet_min"    => "Sale Sold Lot Square Feet Min",
                                "sale_sold_lot_square_feet_avg"    => "Sale Sold Lot Square Feet Avg",
                                "sale_sold_lot_square_feet_max"    => "Sale Sold Lot Square Feet Max",
                                "sale_sold_lot_square_feet_mean"   => "Sale Sold Lot Square Feet Mean",
                                "sale_sold_list_price_min"         => "Sale Sold List Price Min",
                                "sale_sold_list_price_avg"         => "Sale Sold List Price Avg",
                                "sale_sold_list_price_max"         => "Sale Sold List Price Max",
                                "sale_sold_list_price_mean"        => "Sale Sold List Price Mean",
                                "sale_sold_list_price_psf_min"     => "Sale Sold List Price/SF Min",
                                "sale_sold_list_price_psf_avg"     => "Sale Sold List Price/SF Avg",
                                "sale_sold_list_price_psf_max"     => "Sale Sold List Price/SF Max",
                                "sale_sold_list_price_psf_mean"    => "Sale Sold List Price/SF Mean",
                                "sale_sold_sale_price_min"         => "Sale Sold Sale Price Min",
                                "sale_sold_sale_price_avg"         => "Sale Sold Sale Price Avg",
                                "sale_sold_sale_price_max"         => "Sale Sold Sale Price Max",
                                "sale_sold_sale_price_mean"        => "Sale Sold Sale Price Mean",
                                "sale_sold_sale_price_psf_min"     => "Sale Sold Price per Sq Ft Min",
                                "sale_sold_sale_price_psf_avg"     => "Sale Sold Price per Sq Ft Avg",
                                "sale_sold_sale_price_psf_max"     => "Sale Sold Price per Sq Ft Max",
                                "sale_sold_sale_price_psf_mean"    => "Sale Sold Price per Sq Ft Mean",
                                "sale_sold_cdom_min"               => "Sale Sold CDOM Min",
                                "sale_sold_cdom_avg"               => "Sale Sold CDOM Avg",
                                "sale_sold_cdom_max"               => "Sale Sold CDOM Max",
                                "sale_sold_cdom_mean"              => "Sale Sold CDOM Mean",
                                "sale_sold_year_built_min"         => "Sale Sold Year Built Min",
                                "sale_sold_year_built_avg"         => "Sale Sold Year Built Avg",
                                "sale_sold_year_built_max"         => "Sale Sold Year Built Max",
                                "sale_sold_year_built_mean"        => "Sale Sold Year Built Mean",
                                "rental_level1"                    => "Rental Comp Level 1",
                                "rental_level2"                    => "Rental Comp Level 2",
                                "rental_level3"                    => "Rental Comp Level 3",
                                "rental_level4"                    => "Rental Comp Level 4",
                                "rental_level5"                    => "Rental Comp Level 5",
                                "rental_level6"                    => "Rental Comp Level 6",
                                "rental_active_count"              => "Rental Active Count",
                                "rental_pending_count"             => "Rental Pending Count",
                                "rental_sold_count"                => "Rental Sold Count",
                                "rental_terminated_count"          => "Rental Terminated Count",
                                "rental_withdrawn_count"           => "Rental Withdrawn Count",
                                "rental_expired_count"             => "Rental Expired Count",
                                "rental_sold_homes_rate"           => "Rental Sold Home Rate",
                                "rental_absorption_rate"           => "Rental Absorbtion Rate",
                                "rental_sold_square_feet_min"      => "Rental Sold Square Feet Min",
                                "rental_sold_square_feet_avg"      => "Rental Sold Square Feet Avg",
                                "rental_sold_square_feet_max"      => "Rental Sold Square Feet Max",
                                "rental_sold_square_feet_mean"     => "Rental Sold Square Feet Mean",
                                "rental_sold_beds_min"             => "Rental Sold Beds Min",
                                "rental_sold_beds_avg"             => "Rental Sold Beds Avg",
                                "rental_sold_beds_max"             => "Rental Sold Beds Max",
                                "rental_sold_beds_mean"            => "Rental Sold Beds Mean",
                                "rental_sold_bathrooms_min"        => "Rental Sold Bathrooms Min",
                                "rental_sold_bathrooms_avg"        => "Rental Sold Bathrooms Avg",
                                "rental_sold_bathrooms_max"        => "Rental Sold Bathrooms Max",
                                "rental_sold_bathrooms_mean"       => "Rental Sold Bathrooms Mean",
                                "rental_sold_lot_square_feet_min"  => "Rental Sold Lot Square Feet Min",
                                "rental_sold_lot_square_feet_avg"  => "Rental Sold Lot Square Feet Avg",
                                "rental_sold_lot_square_feet_max"  => "Rental Sold Lot Square Feet Max",
                                "rental_sold_lot_square_feet_mean" => "Rental Sold Lot Square Feet Mean",
                                "rental_sold_list_price_min"       => "Rental Sold List Price Min",
                                "rental_sold_list_price_avg"       => "Rental Sold List Price Avg",
                                "rental_sold_list_price_max"       => "Rental Sold List Price Max",
                                "rental_sold_list_price_mean"      => "Rental Sold List Price Mean",
                                "rental_sold_list_price_psf_min"   => "Rental Sold List Price/SF Min",
                                "rental_sold_list_price_psf_avg"   => "Rental Sold List Price/SF Avg",
                                "rental_sold_list_price_psf_max"   => "Rental Sold List Price/SF Max",
                                "rental_sold_list_price_psf_mean"  => "Rental Sold List Price/SF Mean",
                                "rental_sold_sale_price_min"       => "Rental Sold Sale Price Min",
                                "rental_sold_sale_price_avg"       => "Rental Sold Sale Price Avg",
                                "rental_sold_sale_price_max"       => "Rental Sold Sale Price Max",
                                "rental_sold_sale_price_mean"      => "Rental Sold Sale Price Mean",
                                "rental_sold_sale_price_psf_min"   => "Rental Sold Price per Sq Ft Min",
                                "rental_sold_sale_price_psf_avg"   => "Rental Sold Price per Sq Ft Avg",
                                "rental_sold_sale_price_psf_max"   => "Rental Sold Price per Sq Ft Max",
                                "rental_sold_sale_price_psf_mean"  => "Rental Sold Price per Sq Ft Mean",
                                "rental_sold_cdom_min"             => "Rental Sold CDOM Min",
                                "rental_sold_cdom_avg"             => "Rental Sold CDOM Avg",
                                "rental_sold_cdom_max"             => "Rental Sold CDOM Max",
                                "rental_sold_cdom_mean"            => "Rental Sold CDOM Mean",
                                "rental_sold_year_built_min"       => "Rental Sold Year Built Min",
                                "rental_sold_year_built_avg"       => "Rental Sold Year Built Avg",
                                "rental_sold_year_built_max"       => "Rental Sold Year Built Max",
                                "rental_sold_year_built_mean"      => "Rental Sold Year Built Mean",
                                "build_level1"                     => "Build Comp Level 1",
                                "build_level2"                     => "Build Comp Level 2",
                                "build_level3"                     => "Build Comp Level 3",
                                "build_level4"                     => "Build Comp Level 4",
                                "build_level5"                     => "Build Comp Level 5",
                                "build_level6"                     => "Build Comp Level 6",
                                "build_active_count"               => "Build Active Count",
                                "build_pending_count"              => "Build Pending Count",
                                "build_sold_count"                 => "Build Sold Count",
                                "build_terminated_count"           => "Build Terminated Count",
                                "build_withdrawn_count"            => "Build Withdrawn Count",
                                "build_expired_count"              => "Build Expired Count",
                                "build_sold_homes_rate"            => "Build Sold Home Rate",
                                "build_absorption_rate"            => "Build Absorbtion Rate",
                                "build_sold_square_feet_min"       => "Build Sold Square Feet Min",
                                "build_sold_square_feet_avg"       => "Build Sold Square Feet Avg",
                                "build_sold_square_feet_max"       => "Build Sold Square Feet Max",
                                "build_sold_square_feet_mean"      => "Build Sold Square Feet Mean",
                                "build_sold_beds_min"              => "Build Sold Beds Min",
                                "build_sold_beds_avg"              => "Build Sold Beds Avg",
                                "build_sold_beds_max"              => "Build Sold Beds Max",
                                "build_sold_beds_mean"             => "Build Sold Beds Mean",
                                "build_sold_bathrooms_min"         => "Build Sold Bathrooms Min",
                                "build_sold_bathrooms_avg"         => "Build Sold Bathrooms Avg",
                                "build_sold_bathrooms_max"         => "Build Sold Bathrooms Max",
                                "build_sold_bathrooms_mean"        => "Build Sold Bathrooms Mean",
                                "build_sold_lot_square_feet_min"   => "Build Sold Lot Square Feet Min",
                                "build_sold_lot_square_feet_avg"   => "Build Sold Lot Square Feet Avg",
                                "build_sold_lot_square_feet_max"   => "Build Sold Lot Square Feet Max",
                                "build_sold_lot_square_feet_mean"  => "Build Sold Lot Square Feet Mean",
                                "build_sold_list_price_min"        => "Build Sold List Price Min",
                                "build_sold_list_price_avg"        => "Build Sold List Price Avg",
                                "build_sold_list_price_max"        => "Build Sold List Price Max",
                                "build_sold_list_price_mean"       => "Build Sold List Price Mean",
                                "build_sold_list_price_psf_min"    => "Build Sold List Price/SF Min",
                                "build_sold_list_price_psf_avg"    => "Build Sold List Price/SF Avg",
                                "build_sold_list_price_psf_max"    => "Build Sold List Price/SF Max",
                                "build_sold_list_price_psf_mean"   => "Build Sold List Price/SF Mean",
                                "build_sold_sale_price_min"        => "Build Sold Sale Price Min",
                                "build_sold_sale_price_avg"        => "Build Sold Sale Price Avg",
                                "build_sold_sale_price_max"        => "Build Sold Sale Price Max",
                                "build_sold_sale_price_mean"       => "Build Sold Sale Price Mean",
                                "build_sold_sale_price_psf_min"    => "Build Sold Price per Sq Ft Min",
                                "build_sold_sale_price_psf_avg"    => "Build Sold Price per Sq Ft Avg",
                                "build_sold_sale_price_psf_max"    => "Build Sold Price per Sq Ft Max",
                                "build_sold_sale_price_psf_mean"   => "Build Sold Price per Sq Ft Mean",
                                "build_sold_cdom_min"              => "Build Sold CDOM Min",
                                "build_sold_cdom_avg"              => "Build Sold CDOM Avg",
                                "build_sold_cdom_max"              => "Build Sold CDOM Max",
                                "build_sold_cdom_mean"             => "Build Sold CDOM Mean",
                                "build_sold_year_built_min"        => "Build Sold Year Built Min",
                                "build_sold_year_built_avg"        => "Build Sold Year Built Avg",
                                "build_sold_year_built_max"        => "Build Sold Year Built Max",
                                "build_sold_year_built_mean"       => "Build Sold Year Built Mean",
                                "comp_grades"                     => "Comp Grade"                                  
                               })
      puts "roices #{@items}"
      @choices = @items
      
    end
end