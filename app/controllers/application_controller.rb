class ApplicationController < ActionController::Base
  # before_filter :authenticate_user!
  
  
  
  protect_from_forgery

  helper_method :app_namespace, :member?, :collection?, :current_user_id, :sort_by, :sort_asc, :sort_by_status, :sort_asc_status, :params_without_action

  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def not_found
    puts "#nonoi"
    puts "#param #{params}"
    if !(params["path"] == "favicon") && !(params["path"] == "{{propertyImage") 
      Delayed::Worker.logger.debug "#nono"
      flash[:warning] = t('.notfound')
      redirect_to root_url
    else
      # return false
      render nothing: true
      # rescue_from ActionView::MissingTemplate, with: :not_found
    end
  end

  def current_user_id
    current_user.try(:id)
  end
  
  # def after_sign_in_path_for(resource_or_scope)
   # current_user
  # end

  protected

    def params_without_action
      (params || {}).select {|k, v| k != 'action'}
    end
    
    
    
    def sort_by
      params[:sort_by].try(:to_sym) || :id
    end

    def sort_asc
      params[:sort_asc] == "true" 
    end

    def self.acts_as_resource(resource = nil)
      # load_and_authorize_resource resource
      inherit_resources
      defaults resource_class: resource unless resource.nil?
      yield if block_given?
    end

    def member?
      !!instance_variable_get("@#{resource_instance_name}").try(:persisted?)
    end
    
    def collection?
      !!instance_variable_get("@#{resource_collection_name}")
    end

    def download(extension, filename, data)
      response.headers['Content-Disposition'] = "attachment; filename=#{filename}.#{extension}"
      response.headers['Content-Type'] = Mime::Type.lookup_by_extension(extension).to_s
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"] = "no-cache"
      response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
      blob = data.respond_to?(:string) ? data.string : data.to_s
      render text: blob, layout: false
    end
    
    def app_namespace
      # 2012.3.13 didn't work on Rails 3.0.7, cancan 1.6.7; looks promising, but needs some figuring out.
      #cns = @controller.class.to_s.split('::')
      #cns.size == 2 ? cns.shift.downcase : ""
  
      # I am sure there is a slicker way to capture the controller namespace
      # 2012.3.13 But it works!
      controller_name_segments = params[:controller].split('/')
      controller_name_segments.pop
      controller_namespace = controller_name_segments.join('/').downcase
       puts "controller.class.to_s #{controller_namespace}"
      controller_namespace
      # controller_name_segments = @controller.class.to_s.split('::')
      # controller_namespace = controller_name_segments.size > 2 ? controller_name_segments[0].downcase : ""
      # puts "controller.class.to_s #{controller_namespace}"
      # controller_namespace
    end

  private

    def load_market_and_assignment(key=:market_id)
      # binding.pry
      @assignment ||= current_user.assignments.find_by_market_id!(params[key])
      @market ||= @assignment.try(:market)
      
    end
    
    def current_ability
    # I am sure there is a slicker way to capture the controller namespace
      puts "self.class.name #{self.class.name}"
      controller_name_segments = params[:controller].split('/')
      controller_name_segments.pop
      controller_namespace = controller_name_segments.join('/').camelize
      Ability.new(current_user, controller_namespace)
    end
    
end
