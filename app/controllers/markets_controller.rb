class MarketsController < ApplicationController
  before_filter proc { load_market_and_assignment(:id) }
  acts_as_resource

  def show
    if can? :create, Search
      redirect_to new_market_search_url(@market)
    elsif can? :create, Job
      redirect_to new_market_upload_url(@market)
    end
  end

  def update
    if params[:restore]
      @assignment.restore!
      redirect_to url_for(:action => :edit), :notice => 'Settings restored successfully from market default values!'
    else
      if @assignment.update_attributes(params[:market_assignment])
        redirect_to url_for(:action => :edit), :notice => 'Settings saved successfully!'
      else
        flash.now[:error] = 'Settings could not be saved, check errors!'
        render 'edit'
      end
    end
  end

end
