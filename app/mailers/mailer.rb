class Mailer < ActionMailer::Base
  default :from => smtp_settings[:user_name]

  def welcome_instructions(record)
    @resource = record
    mail(:to => record.email, :subject => t('mailer.welcome_instructions.subject'))
  end

  def market_assignment(record)
    @resource = record
    mail(:to => record.user.email, :subject => t('mailer.market_assignment.subject'))
  end

  def search_results(record)
    @resource = record
    attachments['results.csv'] = record.results(true).limit(500).to_csv2(record.user.preferences.columns_comps,nil,nil)
    recipients = record.email_public ? record.market.users : [record.user]
    mail(:to => recipients.map(&:email), subject: "#{t('mailer.search_results.subject')}: #{record.name}" )
  end

  def service_error(record)
    @resource = record
    recipients = User.admins
    mail(:to => recipients.map(&:email), subject: t('mailer.service_error.subject'))
  end

  def upload_results(record)
    @resource = record
    mail(to: record.user.email, subject: t('mailer.upload_results.subject'))
  end
  
  def schedule_result(record)
    @resource = record
    # emails = "sarunnio@hotmail.com,sarunnio@gmail.com,mkay@greenletinv.com,visit@greenletinv.com"
    emails = "sarunnio@hotmail.com"
    subject = "#{@resource['client_access'].try(:titleize)} Visit #{@resource['scheduled_date']}@#{@resource['scheduled_time']} at #{@resource['address']}"
    mail(to: "visit@greenletinv.com", subject: subject, bcc: ["mkay@greenletinv.com","sarunnio@hotmail.com"] ) 
  end
  
end
