class MarketProviderJob < MarketBaseJob
  
  def before(job)
    super
    @provider = @market.mls_provider
    @provider.job_id = job.id
    @provider.listeners << proc { during(job) }
  end

  def during(job)
    job.errors_count = @provider.errors.inject(0) {|r, (k, v)| r += [(v.count rescue 0), 0].max ; r }
    job.results_count = @provider.ids.count
    job.info = { errors: @provider.errors, results: @provider.results, ids: @provider.ids.join(' ') }
    job.save(validate: false)
  end

end