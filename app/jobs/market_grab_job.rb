class MarketGrabJob < MarketProviderJob
  attr_accessor :days, :daily, :cleanup, :interval,  :start_time, :end_time

  # validates :interval, numericality: { greater_than: 0, only_integer: true }
  validates :daily, :inclusion => {:in => [true, false]}
  validates :cleanup, :inclusion => {:in => [true, false]}
  
  def initialize(attrs={})
    super
    self.days ||= 1
    self.daily ||= false
    self.cleanup ||= false
    self.interval ||= nil
     self.start_time ||= nil
    self.end_time ||= nil

  end

  def setup
    @market = Market.find(self.market_id)
    @provider = @market.mls_provider  
  end
  
  def perform
    
    if !self.interval.blank?
      @provider.grab_by_interval(self.interval.try(:to_i))
    elsif !self.start_time.blank?
      @provider.grab_by_specified_period(self.start_time,self.end_time) 
    else
      @provider.grab(self.days.try(:to_i))
    end

  end
  
  def download
    @provider.sync_download(self.days.try(:to_i))
  end
  
  def after(job)
    # pids = @market.properties.where("((  (id in (:ids)) and (((auto_updatable is null) or (auto_updatable = false))  and (DATE(created_at) = DATE(NOW())) OR (((auto_updatable is not null) and (list_price_changed = true))))))", ids: @provider.ids).pluck(:id) unless @provider.ids.blank?
    
    self.daily = true 
    
    pids = @market.properties.where("(  (id in (:ids)) and (auto_updatable = true) )", ids: @provider.ids).pluck(:id) unless (@provider.ids.blank? || !self.daily)
    # pids = @market.properties.where("((id in (:ids)) and (auto_updatable = true) and  (tax_id in ('9242031660000','1041510000004','0211350700010','0700004000', '0752000100025','1046910000024', '6700002021000', '1151860560015', '3843000300240','1257460020003','0830420000012','1290520010031','1312580020023')) ) ", ids: @provider.ids).pluck(:id) unless @provider.ids.blank?
    # @market.properties.where("((id in (:ids)) and ((auto_updatable is not null) and (auto_updatable = true)))", ids: @provider.ids)
    # @provider.ids.select do |id|
      # p = Property.find(id)
      # p
    # end
    Delayed::Worker.logger.debug "@provider.ids: #{pids}"
    
    Delayed::Worker.logger.debug "@time: #{Time.now}"
    MarketComputeJob.new(market_id: @market.id, ids: pids, daily: true, cleanup: self.cleanup).program(priority: -1) 
    # MarketComputeJob.new(market_id: @market.id, ids: pids, daily: true, cleanup: self.cleanup).program(priority: -1) if (self.daily && !Rails.env.development?)
  end

end