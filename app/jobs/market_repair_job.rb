class MarketRepairJob < MarketProviderJob

  def perform
    @provider.import(self.files)
  end

end