class MarketDomUpdateJob < MarketProviderJob
  attr_accessor :days, :rows_affected
 
  validates :days, numericality: { greater_than: 0, only_integer: true }

  def initialize(attrs={})
    super
    self.days ||= 1
  end

  def perform
    p "job being performed"
    @provider.add_dom(self.days.try(:to_i))
    # Delayed::Worker.logger.debug "row affect at job caller: #{n}"
    # self.rows_affected = n
  end

  def during(job)
    job.errors_count = @provider.errors.inject(0) {|r, (k, v)| r += [(v.count rescue 0), 0].max ; r }
    Delayed::Worker.logger.debug "row affect after job caller: #{@provider.rows_affected}"
    job.results_count.nil? ? job.results_count = @provider.rows_affected: job.results_count += @provider.rows_affected
    job.info = { errors: @provider.errors, results: @provider.results, ids: @provider.ids.join(' ') }
    job.save(validate: false)
  end
end