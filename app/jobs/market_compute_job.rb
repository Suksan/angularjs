class MarketComputeJob < MarketBaseJob
  
  attr_accessor :limit, :ids, :active_pending, :not_active_pending, :results, :force,  :error_items , 
  :active, :active_start_id, :active_end_id, :general_start_id, :general_end_id, :daily, :finish_time,
  :weekly, :cleanup, :general_start_date, :general_end_date, :duration, :missing_zillows, :active_single_family, :custom_ids,
  :rental_type, :total_ids
  validates :limit, numericality: { greater_than: 0, only_integer: true, allow_blank: true }
  validates :active_start_id, numericality: { greater_than: 0, only_integer: true, allow_blank: true }
  validates :active_end_id, numericality: { greater_than: 0, only_integer: true, allow_blank: true }
  validates :general_start_id, numericality: { greater_than: 0, only_integer: true, allow_blank: true }
  validates :general_end_id, numericality: { greater_than: 0, only_integer: true, allow_blank: true }
  validate :consistency
  validate :active_start_consistency
  validate :active_end_consistency
  validate :general_start_consistency
  validate :general_end_consistency
 

  # def perform
    # todo = {}
# 
    # #>> id case
    # if !self.custom_ids.blank?
      # id_ary = custom_ids.gsub(/\s+/,"").split(",").map(&:to_i)
      # self.force = true
      # todo[:custom_ids]    = @market.properties.where(["id IN (?)", id_ary]).pluck(:id)
    # end
#     
    # todo[:ids]    = @market.properties.where(["id IN (?)", self.ids]).pluck(:id) unless self.ids.blank?
#     
    # sqft_not_null = "square_feet IS NOT NULL AND " if @market.id != 5
#     
    # #>> weekly case
    # if !self.weekly.blank?
      # never_get_computed = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NULL) ", ['sold', 'off']]).pluck(:id) 
#       
      # older_than_one_week = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NOT NULL AND comps_updated_at <= ?) ", ['sold', 'off'], 1.week.ago]).order("comps_updated_at asc").pluck(:id) 
#    
      # todo[:weekly] = never_get_computed + older_than_one_week
    # end
#     
    # #>> active or pending case
    # if !self.general_start_id.blank? && self.general_start_date.blank?   
      # Delayed::Worker.logger.debug "st id not null - st date null 1"
      # todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?) AND (id between ? and ?)", ['active','pending'], self.force ? DateTime.now : 1.week.ago, general_start_id, general_end_id]).pluck(:id) unless self.active_pending.blank?
#    
    # end
#     
    # #>> others case
    # if !self.general_start_id.blank? && self.general_start_date.blank?
      # todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?) AND (id between ? and ?)", ['active', 'pending'], self.force ? DateTime.now : 1.month.ago, general_start_id, general_end_id]).pluck(:id) unless self.not_active_pending.blank?
#     
    # end
#     
    # #>> active or pending case
    # if self.general_start_id.blank? && !self.general_start_date.blank?
      # Delayed::Worker.logger.debug "##general_start_date #{general_start_date} - #{general_end_date}"
      # todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?)) ", ['active', 'pending'], Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default])  + 1.day ]).pluck(:id) unless self.active_pending.blank?
#       
#     
    # end
#     
    # if self.general_start_id.blank? &&  !self.general_start_date.blank?
     # todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?))", ['active', 'pending'],  Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default])  + 1.day ]).pluck(:id) unless self.not_active_pending.blank?
#     
    # end
# #     
    # if !self.general_start_id.blank? && !self.general_start_date.blank?
#      
      # todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?))  AND (id between ? and ?)", ['active', 'pending'], Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default]) + 1.day , general_start_id, general_end_id]).pluck(:id) unless self.active_pending.blank?
#      
#    
    # end
#     
    # if !self.general_start_id.blank? && !self.general_start_date.blank?
     # todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?))  AND (id between ? and ?)", ['active', 'pending'],  Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default])  + 1.day, general_start_id, general_end_id ]).pluck(:id) unless self.not_active_pending.blank?
#     
#      
    # end
#     
    # if !self.general_start_id.blank? && !self.rental_type.blank?
#        
       # todo[:rental_type] = @market.properties.limit(self.limit).where(["#{sqft_not_null} category = 'rental'  AND (comps_updated_at IS NULL OR comps_updated_at <= ?) AND (id between ? and ?)", self.force ? DateTime.now : 1.week.ago, general_start_id, general_end_id]).pluck(:id)
    # end
#     
    # if self.general_start_id.blank? && self.general_start_date.blank?
#       
      # todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?) ", ['active', 'pending'], self.force ? DateTime.now : 1.week.ago]).pluck(:id) unless self.active_pending.blank?
#       
    # end
#     
    # if self.general_start_id.blank? && self.general_start_date.blank?
#       
       # todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?)", ['active', 'pending'], self.force ? DateTime.now : 1.month.ago]).pluck(:id) unless self.not_active_pending.blank?
#       
    # end
#     
    # todo[:active] = @market.properties.limit(self.limit).where(["#{sqft_not_null} (status = (?)) AND (comps_updated_at IS NULL) AND (id between ? and ?)", 'active', active_start_id, active_end_id]).pluck(:id) unless self.active.blank?
    # Delayed::Worker.logger.debug "** active_single_family #{@market.properties.where(["(status = ?) and (category = ?)", 'active','single_family']).count}"
#     
    # if !self.active_single_family.blank?
       # self.force = true
       # todo[:active_single_family] = @market.properties.limit(self.limit).where(["(status = ?) and (category = ?)", 'active','single_family']).pluck(:id)
    # end
#     
#    
#     
    # if !self.missing_zillows.blank?
      # self.force = true
      # todo[:missing_zillows] = @market.properties.where(["((zillow_sale_estimate is not null) and (zillow_rental_estimate is not null)) and ((zillow_tax_amount is null)  or (appreciation_rate is null)) and (status = ?)", 'active']).pluck(:id)
    # end
#      
#     
    # self.results = {}
    # self.error_items = {}
#     
    # Delayed::Worker.logger.debug "$todo #{todo}"
    # Delayed::Worker.logger.debug "ids #{ids}"
#     
    # todo.each do |category, ids|
      # Delayed::Worker.logger.debug "category:> #{category} - timed #{Time.new}" 
      # self.results[category] = []
      # self.error_items[category] = []
      # i = 0
      # @market.properties.where(id: ids).find_each do |p|
#         
          # # if (i >= 0) && (i < 6)
            # if finish_time
              # timenow = Time.now
              # Delayed::Worker.logger.debug "compute interrupted at break time, #{timenow}" if timenow >= finish_time 
              # break if timenow >= finish_time 
            # end
#             
            # p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
            # Delayed::Worker.logger.debug "force #{self.force}"
            # if p.compute(self.force ? true : false )
              # Delayed::Worker.logger.debug "computed successfully"
              # self.results[category] << p.id
            # else
              # Delayed::Worker.logger.debug "compute failed"
              # self.error_items[category] << p.id
            # end
#           
          # # end
          # i += 1
#         
      # end
    # end
#     
    # if self.daily
      # n = @market.properties.update_all  ["auto_updatable = null"],["id in (:ids)",ids: self.ids ]  unless self.ids.blank?
      # Delayed::Worker.logger.debug "afftected auto updatable = #{n}"
    # end
#     
    # # @market.update_search_stats
  # end
  
  def perform
    todo = {}

    #>> id case
    if !self.custom_ids.blank?
      id_ary = custom_ids.gsub(/\s+/,"").split(",").map(&:to_i)
      self.force = true
      todo[:custom_ids]    = @market.properties.where(["id IN (?)", id_ary]).pluck(:id)
    end
    
    todo[:ids]    = @market.properties.where(["id IN (?)", self.ids]).pluck(:id) unless self.ids.blank?
    
    sqft_not_null = "square_feet IS NOT NULL AND " if @market.id != 5
    
    #>> weekly case
    if !self.weekly.blank?
      never_get_computed = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NULL) ", ['sold', 'off']]).pluck(:id) 
      
      older_than_one_week = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NOT NULL AND comps_updated_at <= ?) ", ['sold', 'off'], 1.week.ago]).order("comps_updated_at asc").pluck(:id) 
   
      todo[:weekly] = never_get_computed + older_than_one_week
    end
    
    #>> active or pending case
    if !self.general_start_id.blank? && self.general_start_date.blank?   
      Delayed::Worker.logger.debug "st id not null - st date null 1"
      todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?) AND (id between ? and ?)", ['active','pending'], self.force ? DateTime.now : 1.week.ago, general_start_id, general_end_id]).pluck(:id) unless self.active_pending.blank?
   
    end
    
    #>> others case
    if !self.general_start_id.blank? && self.general_start_date.blank?
      todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?) AND (id between ? and ?)", ['active', 'pending'], self.force ? DateTime.now : 1.month.ago, general_start_id, general_end_id]).pluck(:id) unless self.not_active_pending.blank?
    
    end
    
    #>> active or pending case
    if self.general_start_id.blank? && !self.general_start_date.blank?
      Delayed::Worker.logger.debug "##general_start_date #{general_start_date} - #{general_end_date}"
      todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?)) ", ['active', 'pending'], Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default])  + 1.day ]).pluck(:id) unless self.active_pending.blank?
      
    
    end
    
    if self.general_start_id.blank? &&  !self.general_start_date.blank?
     todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?))", ['active', 'pending'],  Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default])  + 1.day ]).pluck(:id) unless self.not_active_pending.blank?
    
    end
#     
    if !self.general_start_id.blank? && !self.general_start_date.blank?
     
      todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?))  AND (id between ? and ?)", ['active', 'pending'], Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default]) + 1.day , general_start_id, general_end_id]).pluck(:id) unless self.active_pending.blank?
     
   
    end
    
    if !self.general_start_id.blank? && !self.general_start_date.blank?
     todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND ((comps_updated_at >= ?) and (comps_updated_at <= ?))  AND (id between ? and ?)", ['active', 'pending'],  Date.strptime(general_start_date, Date::DATE_FORMATS[:default]), Date.strptime(general_end_date, Date::DATE_FORMATS[:default])  + 1.day, general_start_id, general_end_id ]).pluck(:id) unless self.not_active_pending.blank?
    
     
    end
    
    if !self.general_start_id.blank? && !self.rental_type.blank?
       
       todo[:rental_type] = @market.properties.limit(self.limit).where(["#{sqft_not_null} category = 'rental'  AND (comps_updated_at IS NULL OR comps_updated_at <= ?) AND (id between ? and ?)", self.force ? DateTime.now : 1.week.ago, general_start_id, general_end_id]).pluck(:id)
    end
    
    if self.general_start_id.blank? && self.general_start_date.blank?
      
      todo[:active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?) ", ['active', 'pending'], self.force ? DateTime.now : 1.week.ago]).pluck(:id) unless self.active_pending.blank?
      
    end
    
    if self.general_start_id.blank? && self.general_start_date.blank?
      
       todo[:not_active_pending] = @market.properties.limit(self.limit).where(["#{sqft_not_null} status NOT IN (?) AND (comps_updated_at IS NULL OR comps_updated_at <= ?)", ['active', 'pending'], self.force ? DateTime.now : 1.month.ago]).pluck(:id) unless self.not_active_pending.blank?
      
    end
    
    todo[:active] = @market.properties.limit(self.limit).where(["#{sqft_not_null} (status = (?)) AND (comps_updated_at IS NULL) AND (id between ? and ?)", 'active', active_start_id, active_end_id]).pluck(:id) unless self.active.blank?
    Delayed::Worker.logger.debug "** active_single_family #{@market.properties.where(["(status = ?) and (category = ?)", 'active','single_family']).count}"
    
    if !self.active_single_family.blank?
       self.force = true
       todo[:active_single_family] = @market.properties.limit(self.limit).where(["(status = ?) and (category = ?)", 'active','single_family']).pluck(:id)
    end
    
   
    
    if !self.missing_zillows.blank?
      self.force = true
      todo[:missing_zillows] = @market.properties.where(["((zillow_sale_estimate is not null) and (zillow_rental_estimate is not null)) and ((zillow_tax_amount is null)  or (appreciation_rate is null)) and (status = ?)", 'active']).pluck(:id)
    end
     
    
    self.results = {}
    self.error_items = {}
    
    Delayed::Worker.logger.debug "$todo #{todo}"
    Delayed::Worker.logger.debug "ids #{ids}"
    
    rd = RedisCli.new()
    
    if todo.nil? || (todo == {})
      hsh = {:market_id => @market.id,:custom_ids => "", 
                :jid => self.jid}
            # hsh = hsh.merge({:is_last => true,:total => ids.count}) if is_last
            rd.lpushq('MarketJobUpdateWorker','queue:default01', hsh)
    end
    
    todo.each do |category, ids|
      Delayed::Worker.logger.debug "category:> #{category} - timed #{Time.new}" 
      self.results[category] = []
      self.error_items[category] = []
      i = 0
      is_last = false
      if ids && ids.count > 0
      # @market.properties.where(id: ids).find_each do |p|
        divisor = 0.0
        
        if (category == :weekly) || (category == 'weekly')
          divisor = 1.0
        elsif (category != :custom_ids)
          case 
            # when ids.count >= 50
              # divisor  = 8.0
            # when ids.count >= 40  
              # divisor  = 3.0
            when ids.count >= 10  
              divisor  = 5.0
            else
              divisor = 5.0
          end  
        else
          divisor = 2.0
        end
        max_items = (ids.count / divisor).ceil
        # sls = ids.each_slice(max_items).to_a
        sls = ids.in_groups(divisor.to_i,false).reject do |d| d == [] end
        sls_text = (sls.inject([]) { |x,d| x << d.last; x }).join(",") if sls && sls.count > 0
        
        jhsh = {'processed' => 0,'total' => ids.count,'last_items' => sls_text  }
        rd.mapped_hmset("job:#{self.jid}",jhsh)
        
        if sls && sls.count > 0 
          sls.each do |sl|
            
              # if (i >= 0) && (i < 6)
            if finish_time
              timenow = Time.now
              Delayed::Worker.logger.debug "finish time, #{finish_time}"
              Delayed::Worker.logger.debug "compute interrupted at break time, #{timenow}" if timenow >= finish_time 
              break if timenow >= finish_time 
            end
              
            
            Delayed::Worker.logger.debug "force #{self.force}"
            Delayed::Worker.logger.debug "job_id #{self.jid}"
            Delayed::Worker.logger.debug "job_id #{self.jid}"
              
            # if i == (ids.count - 1)
              # is_last = true
            # end
              
            
            Delayed::Worker.logger.debug "sl #{sl}"
            hsh = {:market_id => @market.id,:custom_ids => sl.reject(&:blank?).join(","), 
                :jid => self.jid}
            # hsh = hsh.merge({:is_last => true,:total => ids.count}) if is_last
            if (category == :weekly) || (category == 'weekly')
              hsh = {:market_id => @market.id,:custom_ids => sl.reject(&:blank?).join(","), 
                :jid => self.jid,:weekly => "true"}
              Delayed::Worker.logger.debug ">>priority low"
              new_hash = hsh.merge({finish_time: finish_time.to_s})
              Delayed::Worker.logger.debug ">>new hash #{new_hash}"
              rd.lpushq('MarketCompsWorker','queue:slow', new_hash)
            else
              chosen_queue = ''
              # if (Sidekiq::Queue['fast01'].busy  && Sidekiq::Queue['fast01'].busy > 0)
#               
              # end
              fast_items = rd.lrange("limit_fetch:busy:fast01",0,-1) 
              fast_items2 = rd.lrange("limit_fetch:busy:fast02",0,-1)
              if ((!fast_items) || (fast_items && fast_items.count < 6))
                chosen_queue = 'fast01'
              
              elsif ((fast_items && fast_items.count >= 6) && ((!fast_items2) || (fast_items2 && fast_items2.count < 6)))
                chosen_queue = 'fast02'
              else
                chosen_queue = 'default01'
              end 
              Delayed::Worker.logger.debug ">>priority default"
              Delayed::Worker.logger.debug ">>chosen queue: #{chosen_queue}"
              if category == :custom_ids
                rd.lpushq('MarketNewCompsWorker',"queue:#{chosen_queue}",hsh)
              else
                rd.lpushq('MarketCompsWorker',"queue:#{chosen_queue}",hsh)
              end
            end
            
            
            
            
              
              # if p.compute(self.force ? true : false )
                # Delayed::Worker.logger.debug "computed successfully"
                # self.results[category] << p.id
              # else
                # Delayed::Worker.logger.debug "compute failed"
                # self.error_items[category] << p.id
              # end
            
            # end
            i += 1
          end
          self.total_ids = i
        else
          self.total_ids = 0  
        end
      # end
      else
        self.total_ids = 0 
        
        
      end
    end
    
    if self.daily
      n = @market.properties.update_all  ["auto_updatable = null"],["id in (:ids)",ids: self.ids ]  unless self.ids.blank?
      Delayed::Worker.logger.debug "afftected auto updatable = #{n}"
    end
    
    # @market.update_search_stats
  end
  
  def after(job)
     
                    
    # Delayed::Worker.logger.debug "error count: #{self.errors rescue nil}"
    self.error_items.each { |k,v|
      Delayed::Worker.logger.debug "k = #{k}   v = #{v}"
    }
    job.results_count = self.results.inject(0) {|r, (k,v)| r+= v.count; r} rescue nil
    job.errors_count = self.error_items.inject(0) {|r, (k,v)| r+= v.count unless v.blank?; r} rescue nil
    
    job.info = { results: self.results.inject({}) {|r, (cat, ids)| 
        r[cat] = ids.join(' ') 
        # rd = RedisCli.new()
        # rd.lpush('MarketComputesWorker',{:market_id => @market.id,:custom_ids => ids.join(',')})
        r 
      } , errors: self.error_items.inject({}) {|r, (cat, ids)| 
        r[cat] = ids.join(' ') unless ids.blank?
        r 
      }  
    }

    super
    
    
    
    
    # n = self.market.properties.on_market.update_all ["cdom = cdom+ DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL dom DAY) - 1) )
                          # , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          # ["status in ('active','pending') and ((listing_date is not null) and ((cdom is not null) and (dom is not null)))"]
    # MarketEmailJob.new(market_id: @market.id, limit: 1000, weekly: true).program(priority: -1)
  end
  
  def clean_up
    restart_cron_job = @market.restart_cron_jobs.build(name: "x")
    restart_cron_job.save
  end

  private

    def consistency
      self.errors.add(:limit, "You must select at least one option") if self.limit.blank? && self.ids.blank? && self.active_pending.blank? && self.not_active_pending.blank?  && self.active.blank? && self.custom_ids.blank? && self.active_single_family.blank?
    end
    
    def active_start_consistency
      self.errors.add(:active_start_id, "You must specify a starting ID") if self.active_start_id.blank? && !self.active.blank?
    end 
    
    def active_end_consistency
      self.errors.add(:active_end_id, "You must specify an ending ID") if self.active_end_id.blank? &&  !self.active.blank?
    end 
    
    def general_start_consistency
      self.errors.add(:general_start_id, "You must specify a starting ID") if self.general_start_id.blank? && !self.force.blank?
    end 
    
    def general_end_consistency
      self.errors.add(:general_end_id, "You must specify an ending ID") if self.general_end_id.blank? &&  !self.force.blank?
    end 
end