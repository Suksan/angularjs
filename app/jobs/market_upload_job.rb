class MarketUploadJob < MarketProviderJob
  require 'fileutils'

  attr_accessor :source, :overrides

  validates :user_id, :source, presence: true
  validate :consistency

  def initialize(attrs={})
    super
    self.overrides ||= [{}]
  end

  def overrides=(values)
    @overrides = values.inject([]) do |r, value|
      value = value.reject {|k, v| v.missing? }
      r << value
      r
    end
  end

  def perform
    overrides = self.overrides.select {|value| !value.empty? }
    p overrides
    p "performing upload"
    
    @provider.upload(self.source, self.files, overrides)
    # p "provider #{@provider}"
    # @market.properties.where(id: @provider.ids).find_each {|p| p.compute }
    # Delayed::Worker.logger.debug("provider ids: #{@provider.ids}")
    
    # allprops = Property.where(["id is not null"])
      # allprops.each do |ap|
        # Delayed::Worker.logger.debug "ap: #{ap.id}"
      # end
    
    @market.update_search_stats
    
     
  end

  def after(job)
    super
    
    Delayed::Worker.logger.debug "@provider.ids: #{@provider.ids}"
    MarketComputeJob.new(market_id: @market.id, ids: @provider.ids).program(priority: 0)
    
    Mailer.delay.upload_results(job)
  end

  private

    def consistency
      overrides = self.overrides.reject(&:empty?)
      if self.files.empty? && overrides.empty?
        self.errors[:files] = "You must at lease enter one row or select a file"
      end
    end

end