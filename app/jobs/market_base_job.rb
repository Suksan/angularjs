class MarketBaseJob < ActiveModel::Base
  require 'fileutils'
  
  attr_accessor :market_id, :user_id, :files, :jid

  validates :market_id, presence: true

  def initialize(args={})
    # binding.pry
    super
    self.files ||= []
  end

  def files=(values)
    @files = [*values].map do |f|
      FileUtils.cp(f.tempfile, Rails.root.join('tmp'))
      {name: f.original_filename, path: Rails.root.join('tmp', File.basename(f.tempfile)) } 
    end
  end

  def enqueue(job)
    job.user_id = self.user_id
    job.market_id = self.market_id
    job.name = self.class.name.underscore
    
  end

  def before(job)
    @market = job.market
    self.jid = job.id
  end

  def success(job)
    if self.respond_to?(:files)
      for file in self.files
        FileUtils.rm(file[:path])
      end
    end
  end

  def error(job, exception)
  end

  def after(job)
  end

  def save(options={})
    if valid?
      self.program
      true
    else
      false
    end
  end

  def program(options={})    
    Delayed::Job.enqueue self, options.merge(:queue => 'greenlight')
  end

end