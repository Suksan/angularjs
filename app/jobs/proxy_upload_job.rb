class ProxyUploadJob < ActiveModel::Base
  DAILY_LIMIT = 1500
  attr_accessor :source, :overrides, :proxy_list
  validate :consistency
  
  def initialize(attrs={})
    
    
  end

  def proxy_list=(values)
    @proxy_list = values
  end

  def perform
       
     
  end
  
  def after(job)
    
    
    Delayed::Worker.logger.debug "job done!!"
    
  end

  
  
  def save(options={})
    if valid?
      self.program
      true
    else
      false
    end
  end

  def program(options={})
    lines = []
    lines = @proxy_list.split("\r\n")
    Delayed::Worker.logger.debug ("$$$ lines #{lines}")
    ActiveRecord::Base.connection.execute("TRUNCATE proxies")
    for line in lines
        str_line = line.strip
        ary_line = str_line.split(":")
        
        proxy = Proxy.new(ip: ary_line[0],
                    port: ary_line[1],
                    login: ary_line[2],
                    password: ary_line[3],
                    daily_limit: DAILY_LIMIT,
                    daily_calls: 0
          )
        proxy.save
    end
    # Delayed::Job.enqueue self, options
  end
  
  private

    def consistency
      
      if self.proxy_list.blank?
        
        errors.add("proxy_list", "You must at least enter one proxy entry")
      end
      
      lines = []
      lines = @proxy_list.split("\r\n")
      i = 0
      for line in lines
        
        if !line.match /^([0-9a-z\.]*)\:([0-9a-z]*)\:([0-9a-z]*)\:([0-9a-z]*)$/i
          errors.add("proxy_list", "Line #{(i+1).to_s} contains invalid entry format")
          
          break
        end
        
        i += 1
      end
    end
   
end