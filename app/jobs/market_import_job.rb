class MarketImportJob < MarketProviderJob

  validates :files, presence: true

  def perform
    @provider.import(self.files)
  end

end