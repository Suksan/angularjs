class MarketEmailJob < MarketBaseJob
  
  attr_accessor :limit, :weekly
  validates :limit, numericality: { greater_than: 0, only_integer: true, allow_blank: true }
  
  def perform
    @market.email_searches
  end

end