class OsmGeocodeJob < MarketBaseJob
  attr_accessor :results, :error_items, :limit, :listeners, :ids, :start_id, :end_id, :status, :time_sold
  
  def initialize(args={})
    
    super
    
    self.listeners = []
  end
  
  def before(job)
    super
    
    self.listeners << proc { during(job) }
  end
    
  def perform
    
     todo = {}
     
     self.results = {}
     self.error_items = {}
     self.ids = []
    # @provider.import(files)
     Delayed::Worker.logger.debug "~~ market id #{@market.id}"
     
     Delayed::Worker.logger.debug "~~ parameters start_id: #{self.start_id} end_id: #{self.end_id} status: #{self.status} time_sold: #{self.time_sold}"
     if !(@market.id == 3)
       if (self.start_id && !self.status)
        todo[:ids] = @market.properties.where("(id between ? and ?) and (latitude IS NULL)", self.start_id, self.end_id).map(&:id)
       elsif (self.start_id && self.status  && !self.time_sold)
        todo[:ids] = @market.properties.where("(id between ? and ?) and (latitude IS NULL) and (status = ?) ", self.start_id, self.end_id, self.status).limit(self.limit).map(&:id)
       elsif (self.start_id && self.status  && self.time_sold)
        todo[:ids] = @market.properties.where("(id between ? and ?) and (latitude IS NULL) and (status = ?)  and (closed_date >= ?) ", self.start_id, self.end_id, self.status,self.time_sold.month.ago).limit(self.limit).map(&:id)
       elsif !self.start_id && !self.status && !self.time_sold
        todo[:ids] = @market.properties.where("latitude IS NULL").limit(self.limit).map(&:id)
       elsif (!self.start_id && self.status && !self.time_sold)
        # Delayed::Worker.logger.debug "~~sql #{@market.client_properties.where("(status = ?) and (latitude IS NULL)",self.status).limit(self.limit).to_sql}"
        todo[:ids] = @market.properties.where("(status = ?) and (latitude IS NULL)",self.status).limit(self.limit).map(&:id)
       elsif (!self.start_id && self.status && self.time_sold)
        # Delayed::Worker.logger.debug "~~ sql: #{@market.client_properties.where("(id between ? and ?) and (status = ?) and (closed_date >= ?) and (latitude IS  NULL)", self.start_id, self.end_id,self.status,self.time_sold.month.ago).limit(self.limit).to_sql}" 
        todo[:ids] = @market.properties.where("(status = ?) and (closed_date >= ?) and (latitude IS  NULL)",self.status,self.time_sold.month.ago).limit(self.limit).map(&:id)
       end
     else
       if (self.start_id && !self.status)
        todo[:ids] = @market.properties.where("(id between ? and ?) and (latitude IS NULL) and (year_built <= 2012)", self.start_id, self.end_id).map(&:id)
       elsif (self.start_id && self.status  && !self.time_sold)
        todo[:ids] = @market.properties.where("(id between ? and ?) and (latitude IS NULL) and (status = ?)  and (year_built <= 2012) ", self.start_id, self.end_id, self.status).limit(self.limit).map(&:id)
       elsif (self.start_id && self.status  && self.time_sold)
        todo[:ids] = @market.properties.where("(id between ? and ?) and (latitude IS NULL) and (status = ?)  and (closed_date >= ?) and (year_built <= 2012) ", self.start_id, self.end_id, self.status,self.time_sold.month.ago).limit(self.limit).map(&:id)
       elsif !self.start_id && !self.status && !self.time_sold
        todo[:ids] = @market.properties.where("latitude IS NULL and (year_built <= 2012)").limit(self.limit).map(&:id)
       elsif (!self.start_id && self.status && !self.time_sold)
        # Delayed::Worker.logger.debug "~~sql #{@market.client_properties.where("(status = ?) and (latitude IS NULL)",self.status).limit(self.limit).to_sql}"
        todo[:ids] = @market.properties.where("(status = ?) and (latitude IS NULL) and (year_built <= 2012)",self.status).limit(self.limit).map(&:id)
       elsif (!self.start_id && self.status && self.time_sold)
        # Delayed::Worker.logger.debug "~~ sql: #{@market.client_properties.where("(id between ? and ?) and (status = ?) and (closed_date >= ?) and (latitude IS  NULL)", self.start_id, self.end_id,self.status,self.time_sold.month.ago).limit(self.limit).to_sql}" 
        todo[:ids] = @market.properties.where("(status = ?) and (closed_date >= ?) and (latitude IS  NULL)  and (year_built <= 2012)",self.status,self.time_sold.month.ago).limit(self.limit).map(&:id)
       end
           
     end 
     
     t1 = Time.new
     todo.each do |category, ids|
        
        Delayed::Worker.logger.debug "category:> #{category} - timed #{Time.new}" 
        self.results[category] = []
        self.error_items[category] = []
        @market.properties.where(id: ids).find_each do |p|
          Delayed::Worker.logger.debug "~~ property id #{p.id}"
          if p.osm_geocode
            p.save(validate: false)
            self.ids << p.id
            self.results[category] << p.id
          else
            self.error_items[category] << p.id
          end
          
          notify_listeners
          
        end
       
     end
     
     t2 = Time.new
     Delayed::Worker.logger.debug "~~started at #{t1}" 
     Delayed::Worker.logger.debug "~~stopped at #{t2}" 
     Delayed::Worker.logger.debug "~~time elapse #{t2 - t1}"
     
  end
  
  def notify_listeners
    self.listeners.each {|listener| listener.call() }
  end
  
  def during(job)
    job.errors_count = self.errors.inject(0) {|r, (k, v)| r += [(v.count rescue 0), 0].max ; r }
    job.results_count = self.ids.count
    job.info = { results: self.results.inject({}) {|r, (cat, ids)| r[cat] = ids.join(' '); r } }
    job.save(validate: false)
  end
  
  
   def after(job)
    # Delayed::Worker.logger.debug "error count: #{self.errors rescue nil}"
    self.error_items.each { |k,v|
      Delayed::Worker.logger.debug "k = #{k}   v = #{v}"
    }
    job.results_count = self.results.inject(0) {|r, (k,v)| r+= v.count; r} rescue nil
    job.errors_count = self.error_items.inject(0) {|r, (k,v)| r+= v.count unless v.blank?; r} rescue nil
    
    job.info = { results: self.results.inject({}) {|r, (cat, ids)| r[cat] = ids.join(' '); r } , errors: self.error_items.inject({}) {|r, (cat, ids)| r[cat] = ids.join(' ') unless ids.blank?; r }  }

    super
    
    
    
    
    # n = self.market.properties.on_market.update_all ["cdom = cdom+ DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL dom DAY) - 1) )
                          # , dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?",  DateTime.now], 
                          # ["status in ('active','pending') and ((listing_date is not null) and ((cdom is not null) and (dom is not null)))"]
    # MarketEmailJob.new(market_id: @market.id, limit: 1000, weekly: true).program(priority: -1)
  end

end