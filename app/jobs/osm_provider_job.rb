class OsmProviderJob < MarketBaseJob
  
  def before(job)
    super
    @provider = "osm".camelcase.constantize.new(Market.find(1))
    @provider.listeners << proc { during(job) }
  end

  def during(job)
    job.errors_count = @provider.errors.inject(0) {|r, (k, v)| r += [(v.count rescue 0), 0].max ; r }
    job.results_count = @provider.ids.count
    job.info = { errors: @provider.errors, results: @provider.results, file_names: @provider.file_names.join(' ') }
    job.save(validate: false)
  end

end