Web::Application.routes.draw do
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do 
    
    namespace :admin do
      resources :markets do
        resources :jobs
      end
      resources :users do
        collection do
          get :search
          get :search_self
        end
      end 
      resources :services
      resources :proxy_uploads
      resources :proxies do
        collection do
          get :import
        end
      end
    end
    match :admin, :via => [:get], to: 'pages#admin'

    namespace :client do
      [:edit_preferences, :update_preferences, :change_password, :update_password].each do |action|
        match action, :via => [:get], to: "pages##{action}"
      end
      
      resources :markets do   
        resources :uploads do
          collection do
            get :template          
          end
          member do
            get :activate_tab, :export , :comps_report 
          end
        end    
        resources :properties do
          collection do 
            get :comps_rental_export,:comps_sale_export,:comps_build_export,:saved,:set_per_page,:opportunities,:search_opportunities
            post :rem_myprop
          end
          member do 
            get :summary,:client_sale_data_set_per_page,:client_rental_data_set_per_page,:comps_rental,:comps_sale,:comps_build,
            :map,:comment,:add_saved, :remove_saved, :capex_calc, :request_visit, :request_visit_form, :neighborhood, :show_rental_comps, :show_rental_comps_qualified, :show_rental_comps_others, :show_sale_comps_qualified, :show_sale_comps_others, :show_sale_comps, :show_subject_property
            post :recompute,:save,:update_flood,:update_tag,:update_near_commercial,:update_rep,:save_comment,:schedule_visit         
          end
        end
        resources :searches do
          member do
            get :map, :export, :set_per_page
            post :add_my_prop, :add_label
          end
        end
      end
      
    end
    match :client, :via => [:get], to: 'pages#client'

    resources :markets do 
      scope :module => :markets do
        resources :searches do
          member do
            get :map, :export, :set_per_page
            post :add_my_prop, :add_label
          end
        end
        resources :uploads do
          collection do
            get :template          
          end
          member do
            get :activate_tab, :export , :comps_report 
          end
        end
        resources :properties do 
          collection do
            get :saved, :export, :set_per_page, :comps_sale_export, :comps_rental_export, :comps_build_export
            post :rem_myprop
          end
          member do 
            get :summary, :add_saved, :remove_saved, :map, :comps_rental, :comps_sale, :comps_build, :proforma, :raw, :level2, :level3, :comment, :data_set_per_page
            post :save_proforma, :recompute, :save_comment, :update_rep, :update_tag, :update_flood, :update_near_commercial
          end
        end
      end
    end

    #resources :users

    devise_for :users

    [:edit_preferences, :update_preferences, :change_password, :update_password].each do |action|
      match action, :via => [:get], to: "pages##{action}"
    end

    # root :to => "pages#welcome"
    # root 'home#index'
    # get ':path', to: 'home#index', format: false
    match '*path', :via => [:get], :to => "application#not_found"
  end

end