#ENV['RAILS_ENV'] = 'staging'
ENV['RAILS_ENV'] = @environment

require File.expand_path(File.dirname(__FILE__) + "/environment")

#set :environment,'production'


DAY_MAPPING = {
    Mon: :monday,
    Tue: :tuesday,
    Wed: :wednesday,
    Thu: :thursday,
    Fri: :friday,
    Sat: :saturday,
    Sun: :sunday,
}

MarketCronJob.all.each do |d|

  begin
    time = Time.strptime("#{d.start_hour.to_s.rjust(2,'0')}:#{d.start_min.to_s.rjust(2,'0')} #{d.mer_indicator}", "%I:%M %P").strftime("%I:%M %P")
    duration = "#{d.duration_hour.to_s.rjust(2,'0')}:#{d.duration_min.to_s.rjust(2,'0')}"

    Delayed::Worker.logger.debug "#{time} - #{duration}"
    every DAY_MAPPING[d.day_of_week.to_sym], :at => "#{time}" do
      rake "jobs:compute['#{d.market_id}','#{time}','#{duration}']"
    end
  rescue Exception => ex
    puts "#{e.message}"
  end
end

every 1.day, :at => '10:00 pm' do
   rake "dom:add[1]"
end

every 1.day, :at => '06:30 am' do
  rake "jobs:add[3,true]"
end

every 1.day, :at => '01:30 pm' do
  rake "jobs:add[3,true]"
end

every 1.day, :at => '06:30 pm' do
  rake "jobs:add[3,true]"
end

every 1.day, :at => '04:30 am' do
  rake "jobs:rsync_by_interval[7,true,1440]"
end

every 1.day, :at => '00:30 am' do
  rake "jobs:rsync_by_interval[6,true,1440]"
end

every 1.day, :at => '10:00 am' do
  rake "jobs:zillow_top_up"
end

every 1.day, :at => '01:40 pm' do
  rake "jobs:zillow_top_up"
end

every 1.day, :at => '04:00 pm' do
  rake "jobs:zillow_top_up"
end
# every 1.day, :at => '03:30 am' do
  # rake "jobs:rsync[5,true]"
# end
#
# every 1.day, :at => '11:00 am' do
  # rake "jobs:rsync[5,true]"
# end
#
# every 1.day, :at => '03:30 pm' do
  # rake "jobs:rsync[5,true]"
# end

every 1.day, :at => '02:00 am' do
  rake "jobs:rsync_by_interval[10,true,480]"
end


######### Dallas Grab Jobs begins #########

every 1.day, :at => '00:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '00:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '00:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '01:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '01:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '01:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '02:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '02:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '02:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '03:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '03:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '03:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '04:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '04:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '04:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '05:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '05:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '05:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '06:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '06:20 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '06:40 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '07:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '07:20 am' do
   rake "jobs:rsync_by_interval[3,true,20]"
 end

every 1.day, :at => '07:40 am' do
   rake "jobs:rsync_by_interval[3,true,20]"
end

###### speed zpone begin #####

every 1.day, :at => '08:00 am' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '08:10 am' do
  rake "jobs:rsync_by_interval[3,true,10]"
end

every 1.day, :at => '08:20 am' do
  rake "jobs:rsync_by_interval[3,true,10]"
end

every 1.day, :at => '08:30 am' do
  rake "jobs:rsync_by_interval[3,true,10]"
end

every 1.day, :at => '08:40 am' do
  rake "jobs:rsync_by_interval[3,true,10]"
end

every 1.day, :at => '08:50 am' do
  rake "jobs:rsync_by_interval[3,true,10]"
end

####### Dallas: speed zone begins ##########

for i in 9..18
  [0].each do |mel|
    if i > 11
      if i == 12
        puts "hh#{i} mm#{mel} pm"
        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} pm" do
          rake "jobs:rsync_by_interval[3,true,60]"
        end
      else
        chel = i % 12
        puts "hh#{chel} mm#{mel} pm"
        every 1.day, :at => "#{chel.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} pm" do
          rake "jobs:rsync_by_interval[3,true,60]"
        end
      end
    else
      if i == 9 && mel == 0
        puts "hh#{i} mm#{mel} am"
        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} am" do
          rake "jobs:rsync_by_interval[3,true,10]"
        end
      else
        puts "hh#{i} mm#{mel} am"
        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} am" do
          rake "jobs:rsync_by_interval[3,true,60]"
        end
      end
    end
   end
end

####### Orlando: speed zone begins ##########

for i in 8..20
  [0,5,10,15,20,25,30,35,40,45,50,55].each do |mel|
    if i > 11
      if i == 12
        puts "hh#{i} mm#{mel} pm"
        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} pm" do
          rake "jobs:rsync_by_interval[6,true,5]"  
        end
      else
        chel = i % 12
        puts "hh#{chel} mm#{mel} pm"
        every 1.day, :at => "#{chel.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} pm" do
          rake "jobs:rsync_by_interval[6,true,5]"  
        end
      end
    else
      if i == 8 && mel == 0
        puts "hh#{i} mm#{mel} am"
        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} am" do
          rake "jobs:rsync_by_interval[6,true,450]"  
        end
      else
        puts "hh#{i} mm#{mel} am"
        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:#{mel.to_s.rjust(2,'0')} am" do
          rake "jobs:rsync_by_interval[6,true,5]"  
        end
      end
    end
   end
end

# every 1.day, :at => '07:00 pm' do
#   rake "jobs:rsync_by_interval[3,true,10]"
# end

# every 1.day, :at => '07:10 pm' do
#   rake "jobs:rsync_by_interval[3,true,10]"
# end

# every 1.day, :at => '07:30 am' do
#   rake "jobs:rsync_by_interval[3,true,10]"
# end

# every 1.day, :at => '07:38 am' do
#   rake "jobs:rsync_by_interval[3,true,10]"
# end

###### speed zpone end #####

every 1.day, :at => '07:00 pm' do
  rake "jobs:rsync_by_interval[3,true,10]"
end

every 1.day, :at => '07:20 pm' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '07:40 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '08:00 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '08:20 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '08:40 pm' do
  rake "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '09:00 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '09:20 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '09:40 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '10:00 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '10:20 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '10:40 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '11:00 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '11:20 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

every 1.day, :at => '11:40 pm' do
  rake  "jobs:rsync_by_interval[3,true,20]"
end

######### Dallas Grab Jobs ends #########

every 1.day, :at => '08:00 am' do
  rake "jobs:rsync[2,true]"
end

every 1.day, :at => '07:30 pm' do
  rake "jobs:rsync[2,true]"
end

# every 1.day, :at => '02:00 am' do
  # rake "jobs:rsync[6,true]"
# end
#
# every 1.day, :at => '00:30 am' do
  # rake "jobs:rsync[4,true]"
# end


####### Atlanta 2: speed zone begins ##########
hours = [7,9,11,13,15,17,18]
for i in hours

    if i > 11
      if i == 18

        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:00 pm" do
          rake "jobs:rsync_by_interval[10,true,60]"
        end
      else
        chel = i % 12

        every 1.day, :at => "#{chel.to_s.rjust(2,'0')}:00 pm" do
          rake "jobs:rsync_by_interval[10,true,120]"
        end
      end
    else
      if i == 7

        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:00 am" do
          rake "jobs:rsync_by_interval[10,true,300]"
        end
      else

        every 1.day, :at => "#{i.to_s.rjust(2,'0')}:00 am" do
          rake "jobs:rsync_by_interval[10,true,120]"
        end
      end
    end

end


every 1.day, :at => '3:00 am' do
  rake "jobs:mail"
end


every 1.day, :at => '06:53 am' do
  command "cd #{Whenever.path} && truncate -s 0 log/staging.log"
end

every 1.day, :at => '06:53 am' do
  command "cd #{Whenever.path} && truncate -s 0 log/dj.log"
end

every 1.day, :at => '06:53 pm' do
  command "cd #{Whenever.path} && truncate -s 0 log/staging.log"
end

every 1.day, :at => '06:53 pm' do
  command "cd #{Whenever.path} && truncate -s 0 log/dj.log"
end

every :reboot do
   command "cd #{Whenever.path} && RAILS_ENV=staging script/delayed_job start -n 8"
end