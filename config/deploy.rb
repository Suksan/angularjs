# see https://github.com/capistrano/capistrano/wiki/2.x-Multistage-Extension
require 'capistrano/ext/multistage'
require 'bundler/capistrano'
require 'delayed/recipes'

set :application, "greenlight"
set :user, "greenlight"
set :stages, %w(production staging)
set :default_stage, "staging"
set :use_sudo, false
set :scm, :git
set :repository, "ssh://git@bitbucket.org/sarunnio/greenlight-rails4.git"
set :deploy_to, "/var/#{application}"
set :deploy_via, :remote_cache
set :default_shell, "bash -l"
set :delayed_job_args, "-n 3"
# set :whenever_command, "bundle exec whenever"
# set :whenever_environment, defer { stage }
# require "whenever/capistrano" 

desc "open SSH console"
task :ssh do
  system "ssh root@#{find_servers.first}"
end

desc "retrieve logs"
task :logs do
  system "scp #{user}@#{find_servers.first}:#{current_path}/log/* log/"
end

namespace :delayed_job do
  task :setup do
    run "cd #{current_path}; chmod +x script/delayed_job"
  end
end

namespace :deploy do
  desc "hard deploy"
  task :hard do
    update
    run "cd #{current_path} && #{rake} RAILS_ENV=#{rails_env} db:#{ENV['db'] || 'recreate'}"
    restart
  end

  desc "Reset and seed db"
  task :seed do
    run "cd #{current_path} && bundle exec rake RAILS_ENV=#{rails_env} db:seed"
  end

  desc "Start the Thin processes"
  task :start do
    run "service thin start"
  end

  desc "Stop the Thin processes"
  task :stop do
    run "service thin stop"
  end

  desc "Restart the Thin processes"
  task :restart do
    run "service thin restart"
  end
  
  task :clean_up do
    run "cd #{current_path} && truncate -s 0 log/#{rails_env}.log"
  end
end

after "deploy",         "deploy:cleanup"
# after "deploy:update",  "deploy:migrate"
after "deploy:update",  "delayed_job:setup"
after "deploy:stop",    "delayed_job:stop"
after "deploy:start",   "delayed_job:start"
after "deploy:restart", "deploy:clean_up", "delayed_job:restart"