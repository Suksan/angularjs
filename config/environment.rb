# fast_require loading for boosting speed 
# see https://github.com/rdp/faster_require
#require File.join(File.dirname(__FILE__), 'boot')
#require File.dirname(__FILE__) + "/../lib/faster_require-0.9.3/lib/faster_require" 

# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Web::Application.initialize!
#Web::Application.config.action_controller.session_store = :active_record_store

url = RedisCli.new.url
Delayed::Worker.logger.debug "redis url: #{url}"
redis = Redis.new(:url => url)
$lock = RemoteLock.new(RemoteLock::Adapters::Redis.new(redis))
