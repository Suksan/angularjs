module CanCan
  class ControllerResource
    def load_collection?
      if resource_base.respond_to? :proxy_association
        resource_base.proxy_association.klass.respond_to?(:accessible_by) && !current_ability.has_block?(authorization_action, resource_class)
      else
        resource_base.respond_to?(:accessible_by) && !current_ability.has_block?(authorization_action, resource_class)
      end
    end
  end
end