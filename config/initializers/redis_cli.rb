# RedisWrapper::Request.default_options[:host]  = 'redis://localhost'
# RedisWrapper::Request.default_options[:port]  = '6379'
# RedisWrapper::Request.default_options[:db]    = ''
# RedisWrapper::Request.default_options[:queue] = 'queue:default'


RedisCli.configure do |config|
  config.host    = 'redis://localhost'       
  config.port    = '6379'       
  config.db      = '1'       
  config.queue   = 'queue:default'      
  puts "setting completed"          
end