include ActionView::Helpers::NumberHelper

# ActiveConfig.debug = true
ActiveConfig.type_config do |c|
  # defaults
  
  # defaults
  Date::DATE_FORMATS[:default] = '%m/%d/%Y'
  c.date nil,
    formatter:  proc {|v| puts "p v= #{v} v class= #{v.class.name}"; v.is_a?(Date) ?  v.strftime(Date::DATE_FORMATS[:default])  : Date.parse(v,"%Y-%m-%d").strftime(Date::DATE_FORMATS[:default]) },
    parser:     proc {|s| Date.strptime(s.gsub(/[^\d]+/, '/'), Date::DATE_FORMATS[:default]) },
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, template.content_tag(:i, nil, class: 'icon-th'), :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge_with_class(:class => 'date input-small align-right', 'data-date-format' => 'mm/dd/yyyy'))) }}  
  c.datetime nil,
    formatter:  proc {|v| v.strftime('%m/%d/%Y %H:%M') }
  c.delimited_string nil,
    formatter:  proc {|v| v.split(",").reject(&:blank?).join(", ") }
  c.time nil,
    formatter:  proc {|v| v.strftime('%H:%M') }
  c.text nil,
    input: proc { @builder.text_area(attribute_name, input_html_options.merge_with_class(rows: 3)) }
  c.string  nil,
    input: proc { @builder.text_field(attribute_name, input_html_options) }
  c.boolean nil,
    formatter:  proc {|v|  v ? 'Yes' : 'No'},
    parser:     proc {|s| s.downcase.match(/^(1|y|true|yes)$/).present? },
    setup:      proc {|c,a,o| 
      o.delete(:presence)
      c.validates a, inclusion: { in: [true, false], allow_blank: true } 
    }
  c.boolean_two nil,
     input:      proc { @builder.select(attribute_name,[["Yes","1"],["No","0"]],{  :include_blank => true , :selected => if !@builder.object.send(attribute_name).nil? then @builder.object.send(attribute_name) ? "1" : "0" else "" end },{style: "width: 120px"})  },
    formatter:  proc {|v| Delayed::Worker.logger.debug "v: #{v}" ; v ? '1' : '0'},
    parser:     proc {|s| s.downcase.match(/^(1|y|true|yes)$/).present? },
    setup:      proc {|c,a,o| 
      o.delete(:presence)
      c.validates a, inclusion: { in: [true, false], allow_blank: true } 
    }
  c.decimal nil, 
    parser:     proc {|s| Float(s.gsub(/[^\d.]+/, '')).to_d }, 
    migrate:    { precision: 30, scale: 15 },
    formatter:  proc {|v| number_with_precision(v, precision: 1) },
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, nil, :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge_with_class(:class => 'input-small numeric align-right'))) } },
    setup:      proc {|c,a,o|   
      c.validates a, numericality: true, allow_blank: true 
      c.validates a, :inclusion => { :in => o[:in], :allow_blank => true, :message => "must be in range #{o[:in].format(:first, :as => o[:as])} <> #{o[:in].format(:last, :as => o[:as])}"} if o[:in]
    },
    align_right: true
  c.decimal_w_two_decimal nil, 
    parser:     proc {|s| Float(s.gsub(/[^\d.]+/, '')).to_d }, 
    migrate:    { precision: 30, scale: 15 },
    formatter:  proc {|v| number_with_precision(v, precision: 2) },
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, nil, :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge_with_class(:class => 'input-small numeric align-right'))) } },
    setup:      proc {|c,a,o|   
      c.validates a, numericality: true, allow_blank: true 
      c.validates a, :inclusion => { :in => o[:in], :allow_blank => true, :message => "must be in range #{o[:in].format(:first, :as => o[:as])} <> #{o[:in].format(:last, :as => o[:as])}"} if o[:in]
    },
    align_right: true
  c.integer nil,
    parser:     proc {|s| Integer(s.gsub(/[^\d.]+/, '')) },
    formatter:  proc {|v| number_with_precision(v, precision: 0) }, 
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, nil, :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge_with_class(:class => 'input-small numeric integer align-right'))) } },
    setup:      proc {|c,a,o| 
      c.validates a, numericality: { :only_integer => true }, allow_blank: true
      c.validates a, :inclusion => { :in => o[:in], :allow_blank => true, :message => "must be in range #{o[:in].format(:first, :as => o[:as])} <> #{o[:in].format(:last, :as => o[:as])}"} if o[:in]
    },
    align_right: true
  c.file nil,
    input:      FileInput,#{errors_on_attribute: proc { object.errors[attribute_name] + object.errors["#{attribute_name}_content_type"] }},
    migrate:    proc {|t,a| t.attachment a},
    setup:      proc {|c,a,o| 
      c.has_attached_file a
      c.validates_attachment a, content_type: { content_type: o[:content_type] }
    }
  # custom
  c.money :decimal,
    migrate:    { precision: 30, scale: 10 }, 
    formatter:  proc {|v| number_to_currency(v, precision: 0) },
    input:      proc { Rails.logger.debug "Yeah"; template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, '$', :class => 'add-on').concat(@builder.text_field(attribute_name,  input_html_options.merge_with_class( :class => 'input-small numeric align-right'))) }}
  c.money_wo_decimal :decimal,
    migrate:    { precision: 30, scale: 10 }, 
    formatter:  proc {|v| number_to_currency(v, precision: 0) },
    input:      proc { Rails.logger.debug "Geah"; template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, '$', :class => 'add-on').concat(@builder.text_field(attribute_name,  input_html_options.merge_with_class(:value => (number_with_precision(@builder.object.send(attribute_name), :precision => 0) ), :class => 'input-small numeric align-right'))) }}
  c.money_w_two_decimal :decimal,
    migrate:    { precision: 30, scale: 10 }, 
    formatter:  proc {|v| number_to_currency(v, precision: 2) },
    input:      proc { Rails.logger.debug "Geah"; template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, '$', :class => 'add-on').concat(@builder.text_field(attribute_name,  input_html_options.merge_with_class(:value => (number_with_precision(@builder.object.send(attribute_name), :precision => 0) ), :class => 'input-small numeric align-right'))) }}
  
  c.percent :decimal,
    parser:     proc {|s| Float(s.gsub(/[^\d.]+/, '')).to_d / BigDecimal('100') }, 
    setup:      proc {|c,a,o| 
      c.send(:define_method, "#{a}_percent=") { |val| self[a] = (Float(val).to_d / 100.0.to_d) rescue nil }
      c.send(:define_method, "#{a}_percent")  { Float(self[a]).to_d * 100.to_d rescue nil }
      # c.send(:define_method, "#{a}_percent_changed?") { self.send("#{a}_changed?") }
      # c.send(:define_method, "#{a}_percent_was") { BigDecimal(self.send("#{a}_was") * 100) rescue nil }
    },
    migrate:    { precision: 30, scale: 20 },
    formatter:  proc { |v| number_to_percentage(v * 100, :precision => 1) }, 
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, '%', :class => 'add-on').concat(@builder.text_field("#{attribute_name}_percent", input_html_options.merge_with_class(:class => 'input-small numeric align-right'))) } }
  c.percent_w_two_decimal :decimal,
    parser:     proc {|s| Float(s.gsub(/[^\d.]+/, '')).to_d / BigDecimal('100') }, 
    setup:      proc {|c,a,o| 
      c.send(:define_method, "#{a}_percent=") { |val| self[a] = (Float(val).to_d / 100.0.to_d) rescue nil }
      c.send(:define_method, "#{a}_percent")  { Float(self[a]).to_d * 100.to_d rescue nil }
      # c.send(:define_method, "#{a}_percent_changed?") { self.send("#{a}_changed?") }
      # c.send(:define_method, "#{a}_percent_was") { BigDecimal(self.send("#{a}_was") * 100) rescue nil }
    },
    migrate:    { precision: 30, scale: 20 },
    formatter:  proc { |v| number_to_percentage(v * 100, :precision => 2) }, 
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, '%', :class => 'add-on').concat(@builder.text_field("#{attribute_name}_percent", input_html_options.merge_with_class(:class => 'input-small numeric align-right'))) } }  
  c.percent_wo_decimal :decimal,
    parser:     proc {|s| Float(s.gsub(/[^\d.]+/, '')).to_d / BigDecimal('100') }, 
    setup:      proc {|c,a,o| 
      c.send(:define_method, "#{a}_percent=") { |val| self[a] = (Float(val).to_d / 100.0.to_d) rescue nil }
      c.send(:define_method, "#{a}_percent")  { Float(self[a]).to_d * 100.to_d rescue nil }
      # c.send(:define_method, "#{a}_percent_changed?") { self.send("#{a}_changed?") }
      # c.send(:define_method, "#{a}_percent_was") { BigDecimal(self.send("#{a}_was") * 100) rescue nil }
    },
    migrate:    { precision: 30, scale: 20 },
    formatter:  proc { |v| number_to_percentage((v.to_d.round(2) * 100), :precision => 0) }, 
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, '%', :class => 'add-on').concat(@builder.text_field("#{attribute_name}_percent", input_html_options.merge_with_class(:class => 'input-small numeric align-right'))) } }  
  c.enum :string, 
    formatter:  proc { |v,c,a| I18n.t(v, scope: [:activerecord, :enums, c.name.underscore, a]) },
    setup:      proc {|c,a,o| 
      c.const_set(a.pluralize.upcase, o[:in]) 
      c.validates a, :inclusion => { :in => o[:in], :allow_blank => true }
    },
    input_html_options: proc {|c,a| {:as => :select, :collection => c.const_get(a.to_s.pluralize.upcase), label_method: proc{|r| c.format(a, r) }, input_html: {class: 'input-medium' } } }
  c.password :string, 
    input:      SimpleForm::Inputs::PasswordInput,
    formatter:  proc {|v| v.gsub(/./, '*') }
  c.phone :string,  
    parser:     proc {|s| s.gsub(/[^\d]+/, '')},
    formatter:  proc {|v| number_to_phone(v, :area_code => true) }
  c.hour :integer,  
    
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, nil, :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge({ type: "number", min: '0', max: '12', class: 'input-small numeric integer align-right' }))) }},
    formatter:  proc {|v| number_with_precision(v, precision: 0) }  
  c.hour_twenty_four :integer,  
    
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, nil, :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge({ type: "number", min: '0', max: '24', class: 'input-small numeric integer align-right' }))) }},
    formatter:  proc {|v| number_with_precision(v, precision: 0) }  
  c.minute :integer,  
    input:      proc { template.content_tag(:div, :class => 'input-prepend') { template.content_tag(:span, nil, :class => 'add-on').concat(@builder.text_field(attribute_name, input_html_options.merge({ type: "number", min: '0', max: '60', class: 'input-small numeric integer align-right' }))) }},
    formatter:  proc {|v| number_with_precision(v, precision: 0) }
  c.year :integer,
    formatter:  proc {|v| number_with_precision(v, precision: 0) }
  c.serialize :text,
    migrate:    proc {|t,a| t.text a}, #, limit: 4294967295 },
    setup:      proc {|c,a,o|
      # puts "%%%%% #{a}"
      o[:with] ||= HashWithIndifferentAccess
      o[:default] = o[:with].new
      c.serialize a, o[:with]
      c.send(:define_method, "#{a}=") {|hash| hash = hash.with_indifferent_access if hash.is_a?(Hash); super(hash) }
      if o[:with] < ActiveModel::Base
        c.send(:define_method, "#{a}_attributes=") {|attrs| self[a].assign_attributes(attrs) }
        c.send(:define_method, "#{a}_attributes") { self[a].attributes } 
      end
    }
  c.user :string,
  # pre_member = User.find_by_id(value); 
    input: proc { puts "input_html_options #{input_html_options}";  @builder.text_field(attribute_name, input_html_options.merge({ :style => "width: 100px;"}))          },
    setup:  proc {|c,a,o|
      
    },
    definition_class: "User"
     # User.find_by_id(self.send(attribute_name));
     # input: proc {  pre_member = User.all; options = {}; options.merge({"data-pre" =>  pre_member.to_json(:only => [:id], :methods => [:name])}) unless pre_member.nil?  ; @builder.text_field(attribute_name, input_html_options.merge(options).merge({ :style => "width: 100px;"}))          }
  # "data-pre" => @book.authors.map(&:attributes).to_json
  c.tag :integer,
    input: proc { puts "input_html_options #{input_html_options}";  @builder.text_field(attribute_name, input_html_options.merge({ :style => "width: 100px;"}))          },
    definition_class: "PropertyTag"
  c.meridian_indicator :enum,
    setup:      proc {|c,a,o| o[:in] = %w{am pm} },
    formatter:  proc {|v| I18n.t(v, scope: :meridian_indicators)}
  c.state :enum,
    setup:      proc {|c,a,o| o[:in] = %w{AL AK AZ AR CA CO CT DE DC FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA PR RI SC SD TN TX UT VT VA WA WV WI WY} },
    formatter:  proc {|v| I18n.t(v, scope: :states)}
  c.loan :enum,
    setup:      proc {|c,a,o| o[:in] = %w{short short2 long bridge} },
    formatter:  proc {|v| I18n.t(v, scope: :loans)}
  c.comp_level :enum,
    setup:      proc {|c,a,o| o[:in] = %w{conservative moderate expanded liberal} },
    formatter:  proc {|v| I18n.t(v, scope: :comp_levels)}
  c.query :text,
    input: QueryInput,
    setup:      proc {|c,a,o|
      
      c.send(:define_method, "#{a}_for") do |record| 
        values = []
        query = self.send(a).gsub(/\#\{[a-z0-9_]+\}/) do |s| 
          values << record.send(s[2..-2]); '?'
        end
        values.unshift(query)
      end
    }
end
