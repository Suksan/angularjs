# Delayed::Worker.sleep_delay = 60
Delayed::Worker.max_run_time = 24.hours
# Delayed::Worker.read_ahead = 10
# Delayed::Worker.delay_jobs = !Rails.env.test?
Delayed::Worker.max_attempts = 0
Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.backend = Job
# Delayed::Worker.logger = Logger.new('/dev/null') # 
Delayed::Worker.logger = Logger.new(File.join(Rails.root, 'log', 'dj.log'))
# 
Delayed::Worker.logger.level = Logger::DEBUG