class ActiveConfig
  class_attribute :types, :on_type_ready, :debug

  def self.debug?
    !!self.debug
  end

  def self.type_config(*args)
    # puts "### initial type_config"
    raise "type_config already defined" if self.types.present?
    raise "wrong usage" if args.length > 0 || !block_given?

    self.types = HashWithIndifferentAccess.new 
    yield GlobalConfigBuilder.new
  end

  def self.has_type?(type)
    raise_if_types_undefined

    self.types.has_key?(type)
  end

  def self.find(name, option, optionOnly=true)
    self.find_all(name, option, optionOnly).first
  end

  def self.find_all(name, option, optionOnly=true)
    raise_if_types_undefined
    # puts "### #{name}"
    # puts "#RRR# #{option}"
    result = []
    config = self.types[name]
    # puts "$$$$ #{config}" if config 
    while (config)
      result << (optionOnly ? config[option] : config) if config[option] || (!optionOnly && config[:as].blank?)
      config = self.types[config[:as]]
    end
    # puts ">>>> #{result}"
    result
  end

  private

    def self.raise_if_types_undefined
      raise "ActiveConfig.type_config must be defined first" if self.types.nil?
      # return if self.types.nil?
    end


  class TypeDefinition
    attr_accessor :name, :as, :options 

    def initialize(name, as, options)
      self.name = name
      self.as = as
      self.options = options
    end

  end

  class GlobalConfigBuilder

    def method_missing(method_id, *args, &block)
      # Rails.logger.debug "$@$ Global method missing #{method_id}"
      p "ActiveConfig - processing type #{method_id}, args: #{args.length}" if ActiveConfig.debug?
      test = method_id.to_s.match(/^([a-z_]+)$/)
      raise "wrong usage" if block_given? || args.length > 2 || test.nil?
      # raise "type #{name} already defined" if types.has_key(name)

      name = test[1]
      as = args.first unless args.first.is_a?(Hash)
      # raise "unknown parent type #{as} for #{name}" if !ActiveConfig.types.has_key?(as)
      
      options = (args.last.is_a?(Hash) ? args.last : {}).merge(:as => as, :type => name)
      
      ActiveConfig.types[name] = options.with_indifferent_access #TypeDefinition.new(name, as, options)
      ActiveConfig.on_type_ready[name.to_sym] if ActiveConfig.on_type_ready.is_a?(Proc)
    end

  end

  class AttrDefinition

  end

  class AttrConfigBuilder

    attr_accessor :klass

    def initialize(klass)
      self.klass = klass
    end

    def _delegate(model_class, destination)
      # puts "#model class #{model_class}"
      keys = model_class.active_attributes
      # puts "keys #{model_class}"
      klass.delegate(*keys.map {|key|
        [key, "#{key}=", "#{key}_changed?", "#{key}_was"]
      }.flatten, to: destination)
      klass.active_delegates << model_class
    end

    def method_missing(method_id, *args, &block)
     
      p "ActiveConfig - processing #{klass.name}.#{method_id}, args: #{args.length}" if ActiveConfig.debug?
      test = method_id.to_s.match(/^([a-z0-9_]+)(!{0,1})$/)
      raise "wrong usage" if block_given? || args.length > 2 || test.nil?

      attr = test[1]
      raise "attribute :#{attr} already defined" if klass.active_config.has_key?(attr)

      options = args.last.is_a?(Hash) ? args.last : {}
      type, default = args.first.is_a?(Hash) ? args.first.first : [args.first, nil]

      raise "unknown type '#{type}' for #{klass.name}.#{attr}" if type && !ActiveConfig.has_type?(type.to_sym)
      #
      options[:transient] = false unless options[:transient] == true
      options[:as] = type || 'string'
      options[:default] = default
      options[:presence] = true unless test[2].blank?
      options[:model] = klass

      setups = ActiveConfig.find_all(options[:as], :setup)
      setups.each {|s| s[klass, attr, options] }

      calc = options[:calc]
      klass.send(:define_method, "#{attr}_calc", proc { calc[self] }) if calc && calc.is_a?(Proc)

      klass.send(:attr_accessor, attr) if options[:transient]

      klass.active_config[attr] = options.with_indifferent_access

      [:acceptance, :confirmation, :exclusion, :format, :inclusion, :length, :numericality, :presence, :uniqueness].each do |key|
        if options[key]
          klass.validates attr, key => options[key], :allow_blank => true
        end
      end

    end
  
  end
    
end

### CONTROLLER UTILS #####################################################################################

class ActionController::Base

  alias :std_redirect_to :redirect_to
  def redirect_to(*args)
    options = args.extract_options!
    for key in [:error, :alert, :notice, :success]
      flash[key] = options.delete(key) if options.has_key?(key)
    end
    flash.keep
    std_redirect_to *args
  end
end

### ARRAY UTILS ###########################################################################################

class Array

  def sum!
    compact.inject(:+).to_d
  end

  def avg!
    sum! / compact.length rescue nil
  end

end

### HASH UTILS ############################################################################################

require "active_support/hash_with_indifferent_access"

class HashWithCaseInsensitiveAccess < HashWithIndifferentAccess
  # This method shouldn't need an override, but my tests say otherwise.
  def [](key)
    super convert_key(key)
  end

  protected

  def convert_key(key)
    key.respond_to?(:downcase) ? key.downcase : key
  end  
end

class Hash
  CLASS_PROC = proc do |key, first, second| 
    if key.to_sym == :class
      [first, second].compact.join(' ')
    elsif first.is_a?(Hash) && second.is_a?(Hash)
      first.merge_with_class(second)
    else
      second
    end
  end

  def merge_with_class!(other_hash)
    merge!(other_hash, &CLASS_PROC)
  end

  def merge_with_class(other_hash)
    merge(other_hash, &CLASS_PROC)
  end
  
  def with_case_insensitive_access
    HashWithCaseInsensitiveAccess.new(self)
  end

  def send(method, *args)
    if args.length == 0 && !self.respond_to?(method)
      self[method]
    else
      super
    end
  end
end

### OBJECT EXTENSION ######################################################################################

class Proc

  def [](*args)
    args = args.slice(0, self.arity) if self.arity >= 0
    call(*args)
  end

end

class Object

  def missing?
    self.nil? || self == ''
  end

  def self.label(method, options={})
    I18n.t(method)
  end
  
  def self.label2(method, options={})
    method
  end

  def format(method, options={})
    
    # puts "%%%% # send method: #{self.class}"
    value = self.send(method) rescue nil
    # puts "$%^$ # value #{value}"
    # puts "option as #{options[:as]} def_class:  #{options[:definition_class]}"
    
    # puts "%##% option is Hash? #{options.is_a?(Hash)}"
    
    if options.is_a?(Hash)
      if options[:as] == :user
        puts "##%% user value #{method}"
        
        reps = self.send(method).split(",") rescue []
        user_list = []
        
        reps.each { |d|
          user = User.find_by_id(d) rescue nil
          user_full = [user.firstname, user.lastname].compact.join(' ') rescue nil
          user_list << user_full
        }
        value = user_list.join(", ") rescue nil
        
      
      
      end
      


      # if method == 
      #   value = self.send(:comps_sale_stats)["level1"] rescue nil
      # elsif method == "sale_level2"
      #   value = self.send(:comps_sale_stats)["level2"] rescue nil
      # elsif method == "sale_level3"
      #   value = self.send(:comps_sale_stats)["level3"] rescue nil
      # end
      
    end
    
    # if method.to_s == "school_district"
      # sd = SchoolDistrict.find_by_id(self.send(method)) rescue nil
      # value = sd.school_district_name rescue nil
    # end
    
    if value.missing? 
      if method.to_s.include?("_")
        
        method_call = method.to_s[method.to_s.index("_") + 1 , method.to_s.length - method.to_s.index("_") - 1 ]
        # puts "#$$ method call #{method_call}"
        case method 

          when  "sale_level1"                    , 
                "sale_level2"                    , 
                "sale_level3"                    ,
                "sale_level4"                    ,
                "sale_level5"                    ,
                "sale_level6"                    
                 value = self.send(:comps_sale_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:money, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "sale_active_count"              ,
                "sale_pending_count"             ,
                "sale_sold_count"                ,
                "sale_terminated_count"          ,
                "sale_withdrawn_count"           ,
                "sale_expired_count"             ,
                "sale_sold_square_feet_min"      ,
                "sale_sold_square_feet_avg"      ,
                "sale_sold_square_feet_min"      ,
                "sale_sold_square_feet_avg"      ,
                "sale_sold_square_feet_max"      ,
                "sale_sold_square_feet_mean"     ,
                "sale_sold_beds_min"             ,
                "sale_sold_beds_avg"             ,
                "sale_sold_beds_max"             ,
                "sale_sold_beds_mean"            ,
                "sale_sold_lot_square_feet_min"  ,
                "sale_sold_lot_square_feet_avg"  ,
                "sale_sold_lot_square_feet_max"  ,
                "sale_sold_lot_square_feet_mean" ,
                "sale_sold_cdom_min"             ,
                "sale_sold_cdom_avg"             ,
                "sale_sold_cdom_max"             ,
                "sale_sold_cdom_mean"            ,
                "sale_sold_year_built_min"       ,
                "sale_sold_year_built_avg"       ,
                "sale_sold_year_built_max"       ,
                "sale_sold_year_built_mean"

              value = self.send(:comps_sale_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:integer, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "sale_sold_homes_rate"           ,
                "sale_absorbtion_rate"           
              value = self.send(:comps_sale_stats)[method_call] rescue nil
              value = number_to_percentage((value ? value : nil), :precision => 1)
              value = '-' if value.missing?
              return value
          when  "sale_sold_bathrooms_min"        ,
                "sale_sold_bathrooms_avg"        ,
                "sale_sold_bathrooms_max"        ,
                "sale_sold_bathrooms_mean"                

                
               value = self.send(:comps_sale_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:decimal, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "sale_sold_list_price_min"       ,
                "sale_sold_list_price_avg"       ,
                "sale_sold_list_price_max"       ,
                "sale_sold_list_price_mean"      ,
                "sale_sold_list_price_psf_min"   ,
                "sale_sold_list_price_psf_avg"   ,
                "sale_sold_list_price_psf_max"   ,
                "sale_sold_list_price_psf_mean"  ,
                "sale_sold_sale_price_min"       ,
                "sale_sold_sale_price_avg"       ,
                "sale_sold_sale_price_max"       ,
                "sale_sold_sale_price_mean"      ,
                "sale_sold_sale_price_psf_min"   ,
                "sale_sold_sale_price_psf_avg"   ,
                "sale_sold_sale_price_psf_max"   ,
                "sale_sold_sale_price_psf_mean" 
                 value = self.send(:comps_sale_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:money_w_two_decimal, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value 
          when  "rental_level1"                    , 
                "rental_level2"                    , 
                "rental_level3"                    ,
                "rental_level4"                    ,
                "rental_level5"                    ,
                "rental_level6"                    
                 value = self.send(:comps_rental_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:money, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "rental_active_count"              ,
                "rental_pending_count"             ,
                "rental_sold_count"                ,
                "rental_terminated_count"          ,
                "rental_withdrawn_count"           ,
                "rental_expired_count"             ,
                "rental_sold_square_feet_min"      ,
                "rental_sold_square_feet_avg"      ,
                "rental_sold_square_feet_min"      ,
                "rental_sold_square_feet_avg"      ,
                "rental_sold_square_feet_max"      ,
                "rental_sold_square_feet_mean"     ,
                "rental_sold_beds_min"             ,
                "rental_sold_beds_avg"             ,
                "rental_sold_beds_max"             ,
                "rental_sold_beds_mean"            ,
                "rental_sold_lot_square_feet_min"  ,
                "rental_sold_lot_square_feet_avg"  ,
                "rental_sold_lot_square_feet_max"  ,
                "rental_sold_lot_square_feet_mean" ,
                "rental_sold_cdom_min"             ,
                "rental_sold_cdom_avg"             ,
                "rental_sold_cdom_max"             ,
                "rental_sold_cdom_mean"            ,
                "rental_sold_year_built_min"       ,
                "rental_sold_year_built_avg"       ,
                "rental_sold_year_built_max"       ,
                "rental_sold_year_built_mean"
              value = self.send(:comps_rental_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:integer, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "rental_sold_homes_rate"           ,
                "rental_absorbtion_rate"           
              value = self.send(:comps_rental_stats)[method_call] rescue nil
              value = number_to_percentage((value ? value : nil), :precision => 1)
              value = '-' if value.missing?
              return value
          when  "rental_sold_bathrooms_min"        ,
                "rental_sold_bathrooms_avg"        ,
                "rental_sold_bathrooms_max"        ,
                "rental_sold_bathrooms_mean"       
                                

                
               value = self.send(:comps_rental_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:decimal, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
           when "rental_sold_list_price_min"       ,
                "rental_sold_list_price_avg"       ,
                "rental_sold_list_price_max"       ,
                "rental_sold_list_price_mean"      ,
                "rental_sold_list_price_psf_min"   ,
                "rental_sold_list_price_psf_avg"   ,
                "rental_sold_list_price_psf_max"   ,
                "rental_sold_list_price_psf_mean"  ,
                "rental_sold_sale_price_min"       ,
                "rental_sold_sale_price_avg"       ,
                "rental_sold_sale_price_max"       ,
                "rental_sold_sale_price_mean"      ,
                "rental_sold_sale_price_psf_min"   ,
                "rental_sold_sale_price_psf_avg"   ,
                "rental_sold_sale_price_psf_max"   ,
                "rental_sold_sale_price_psf_mean" 
                 value = self.send(:comps_rental_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:money_w_two_decimal, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value 
           when "buildbuild_level1"                    , 
                "build_level2"                    , 
                "build_level3"                    ,
                "build_level4"                    ,
                "build_level5"                    ,
                "build_level6"                    
                 value = self.send(:comps_build_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:money, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "build_active_count"              ,
                "build_pending_count"             ,
                "build_sold_count"                ,
                "build_terminated_count"          ,
                "build_withdrawn_count"           ,
                "build_expired_count"             ,
                "build_sold_square_feet_min"      ,
                "build_sold_square_feet_avg"      ,
                "build_sold_square_feet_min"      ,
                "build_sold_square_feet_avg"      ,
                "build_sold_square_feet_max"      ,
                "build_sold_square_feet_mean"     ,
                "build_sold_beds_min"             ,
                "build_sold_beds_avg"             ,
                "build_sold_beds_max"             ,
                "build_sold_beds_mean"            ,
                "build_sold_lot_square_feet_min"  ,
                "build_sold_lot_square_feet_avg"  ,
                "build_sold_lot_square_feet_max"  ,
                "build_sold_lot_square_feet_mean" ,
                "build_sold_cdom_min"             ,
                "build_sold_cdom_avg"             ,
                "build_sold_cdom_max"             ,
                "build_sold_cdom_mean"            ,
                "build_sold_year_built_min"       ,
                "build_sold_year_built_avg"       ,
                "build_sold_year_built_max"       ,
                "build_sold_year_built_mean"
              value = self.send(:comps_build_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:integer, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
          when  "build_sold_homes_rate"           ,
                "build_absorbtion_rate"           
              value = self.send(:comps_build_stats)[method_call] rescue nil
               value = number_to_percentage((value ? value : nil), :precision => 1)
              value = '-' if value.missing?
              return value
          when  "build_sold_bathrooms_min"        ,
                "build_sold_bathrooms_avg"        ,
                "build_sold_bathrooms_max"        ,
                "build_sold_bathrooms_mean"       ,
                               

                
               value = self.send(:comps_build_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:decimal, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
              return value
           when "build_sold_list_price_min"       ,
                "build_sold_list_price_avg"       ,
                "build_sold_list_price_max"       ,
                "build_sold_list_price_mean"      ,
                "build_sold_list_price_psf_min"   ,
                "build_sold_list_price_psf_avg"   ,
                "build_sold_list_price_psf_max"   ,
                "build_sold_list_price_psf_mean"  ,
                "build_sold_sale_price_min"       ,
                "build_sold_sale_price_avg"       ,
                "build_sold_sale_price_max"       ,
                "build_sold_sale_price_mean"      ,
                "build_sold_sale_price_psf_min"   ,
                "build_sold_sale_price_psf_avg"   ,
                "build_sold_sale_price_psf_max"   ,
                "build_sold_sale_price_psf_mean"   
                
                 value = self.send(:comps_build_stats)[method_call] rescue nil
              formatter = ActiveConfig.find(:money_w_two_decimal, :formatter)
              value = formatter[value]
              value = '-' if value.missing?
        end

      end
    else
      
      if method.to_s == 'tag'
        puts "tag"
        tag = PropertyTag.find_by_id(self.send()) rescue nil
        value = tag.name rescue nil
      end
      
    end
    
    if options.is_a?(Hash)
      empty_value = (options && options[:custom_empty_value]) ?  options[:custom_empty_value].to_s : '-'
    else
      empty_value = '-'
    end 
    return empty_value if value.missing? # TODO format_missing
    
    formatter = ActiveConfig.find(options[:as], :formatter)
    # puts "%^&*formatter #{formatter}"
    formatter ? formatter[value, self.class, method, self] : value.to_s
  end
  
 
  
  def format_tag(user_id)
     Property.assign_user_id(user_id)
                
     tag = PropertyTag.find_by_id(self.send(:user_assigned_tag).send(:tag)) rescue nil
     value = tag.name rescue nil
    
  end

  def extract(*keys)
    result = {}
    keys.each {|key| result[key] = self.send(key) }
    result
  end

end

### ACTIVE MODEL ##########################################################################################

class ActiveModel::Base
  extend ActiveModel::Naming
  extend ActiveModel::Translation
  include ActiveModel::Validations
  include ActiveModel::Conversion
  include ActiveModel::Serialization

  def initialize(attrs = {})
    assign_attributes(attrs)
  end
  
  

  def assign_attributes(attrs = {})
    attrs.each do |name, value| 
      value = nil if value.is_a?(Array) && value.length == 1 && value.first.missing?
      value = nil if value.is_a?(String) && (m = value.match(/^([0-9.,]+);([0-9.,]+)$/)) && (m[1] == m[2])
      send("#{name}=", value.blank? ? nil : value)
    end
  end

  def attributes
    Hash[self.instance_variables.map { |v|
      value = self.instance_variable_get("#{v}")
      [v.to_s[1..-1], value] unless value.missing?
    }.compact]
  end

  def persisted?
    false
  end

  def self.dump(object)
    YAML.dump(object.attributes)
  end

  def self.load(yaml)
    attrs = YAML.load(yaml) unless yaml.blank?
    self.new(attrs || {})
  end

  def self.accepts_tokens_for(*attr_names)
    attr_names.each do |attr|
      self.send(:define_method, "#{attr}_tokens")  { self.send(attr).try(:join, ',') }
      self.send(:define_method, "#{attr}_tokens=") {|tokens| self.send("#{attr}=", tokens.try(:split, ',')) }
    end
  end

end

### LOCALIZATIONS ########################################################################################

module I18n
  class MissingTranslation
    def message
      keys.last.to_s.humanize
    end

    def html_message
      ERB::Util.html_escape(message)
    end
  end
end

### ACTIVE RECORD #########################################################################################

class ActiveRecord::Base
  class_attribute :active_config, :active_delegates, :all_attributes_with_delegates

  after_initialize :after_initialize
  before_save :prepare

  def format(attr, options={})
    options[:as] ||= self.class.attr_config(attr, :as)
    super(attr, options)
  end

  def self.human_attribute_name(attribute_key_name, options = {})
    label(attribute_key_name, :default, options)
  end

  def human_enum_name(attribute)
    value = self[attribute]
    value.missing? ? nil : I18n.t("activerecord.enums.#{self.class.name.underscore}.#{attribute}.#{value}")
  end

  def self.label(attr, type=nil, options={})
    options[:default] ||= Rails.env.development? ? [attr, type].compact.join(" / ") : attr.to_s.humanize
    type ||= :default
    config = attr_config(attr)
    model = self
    model = config[:model] if config
    # Delayed::Worker.logger.debug "activerecord.attributes.#{model.model_name.underscore}.#{attr}.#{type}"
    I18n.translate("activerecord.attributes.#{model.model_name.underscore}.#{attr}.#{type}", options)
  end

  def self.format(attr, value, options={})
    options[:as] ||= self.attr_config(attr, :as)
    {attr => value}.format(attr, options)
  end

  def override_attributes(attrs)
    puts ">>>>attrs #{attrs}"
    assign_attributes(attrs, skip_overrides: true)
    if self.respond_to?(:_overrides)
      attrs.keys.each do |attr|
        value = self.send(attr)
        if m = attr.to_s.match(/(.+)_percent$/)
          attr = m[1]
        end
        if self.send("#{attr}_changed?") && !(value === self.send("#{attr}_was"))
          if value.missing?
            self._overrides.delete(attr.to_sym) 
          else
            self._overrides[attr.to_sym] = true
          end
        end
      end
    end
  end

  def assign_attributes(new_attributes, options = {})
   puts "new_attributes: #{new_attributes}"
    if self.respond_to?(:_overrides) && options.delete(:skip_overrides).blank?
      
      new_attributes.delete_if do |attr, value|
        #$$Delayed::Worker.logger.debug "**each attr= #{attr}"
        if m = attr.to_s.match(/(.+)_percent$/)
          #$$Delayed::Worker.logger.debug "**each attr matched =#{attr}"
          attr = m[1]
        end
        self._overrides[attr.to_sym] 
      end
    end
    super
  end

  def active_attributes(options={})
    # puts "get in instance" if ("#{self}" == "Property")
    
    self.class.active_attributes(options).inject({}) do |r, attr|
      value = self.send(attr) 
      # Delayed::Worker.logger.debug "attr: #{attr}"
      r[attr] = value if (options[:missing].nil? || !!options[:missing] == value.missing?)
      r
    end
  end

  def self.active_attributes(options={})
    
    # puts "get in class" if ("#{self}" == "Property")
    result = self.active_config.inject([]) do |r, (attr, config)|
      # puts "#****# options as #{attr} #{options[:as]}" 
      r << attr.to_sym if attr.first != '_' && (options[:calc].nil? || !!options[:calc] == !!config[:calc])
      r
    end
    [*options.delete(:delegates)].compact.each do |delegate|
      # puts "$%^& options #{options}"
      result += delegate.active_attributes(options)
    end
    result += [*options[:others]] if options[:others]
    result = result.map(&:to_s) if options[:symbols] == false
    result.sort! if options[:sort]
    result
  end

  def self.parse(attr, value)
    Rails.logger.debug "$A$ self parse"
    Delayed::Worker.logger.debug "$A$ self parse"
    puts "$A$ self parse"
    return value if !value.is_a?(String)
    value = value.strip
    return nil if value.missing?
    
    config = self.attr_config(attr)
    #$$Delayed::Worker.logger.debug "config in self.attr_config #{config}"
    p attr if config.nil?
    parser = config[:parser] || ActiveConfig.find(config[:as], :parser)
    parser ? parser[value] : value rescue nil
  end


  
  # TODO attr_protected
  def self.attr_config(*args)
    
    # Rails.logger.debug "$A$ attr config args: #{args}"
    if args.length.in?(1,2) && !block_given?
      if self.active_config
        for delegate in self.active_delegates
          config = delegate.active_config[args.first]
          break unless config.nil?
        end
        
        # Rails.logger.debug "$A$ entered attr_config: #{config}"
        args.length == 2 && config ? config[args.last] : config
      end
    elsif args.length == 0 && block_given?
      # raise "attr_config already defined" if self.active_config.present?
      # puts caller
      # puts "$A$ block passed attr_config: #{self}" if ("#{self}" == "Property")
      self.active_config = HashWithIndifferentAccess.new
      self.attr_protected  #TODO allow config
      self.active_delegates = [self]
      # puts "# active delegates #{self.active_delegates}" 
      self.scope :asc, proc {|attr| order("#{attr} ASC")}
      self.scope :desc, proc {|attr| order("#{attr} DESC")}
      
      # puts "## gehara"
      yield ActiveConfig::AttrConfigBuilder.new(self)
      
      self.all_attributes_with_delegates = active_attributes(delegates: self.active_delegates, others: [:id, :created_at, :updated_at])
      # puts "$A$ end: #{self} block" if ("#{self}" == "Property")
    else
      raise "wrong usage"
    end
  end

  def self.migrate(table)
    self.active_config.each do |attr, config| 
      table.send(config[:as], attr) unless config[:transient]
    end
  end

  def self.distinct(attr, label_attr=nil)
    if label_attr
      where("#{attr} IS NOT NULL and #{attr} <> ''").order(attr).select("DISTINCT #{attr}, #{label_attr}").inject({}) do |h, r|
        h[r[attr]] = r[label_attr] || r[attr] 
        h
      end
    else
      where("#{attr} IS NOT NULL and #{attr} <> ''").order(attr).pluck("DISTINCT #{attr}")
    end
  end
  
  def self.distinct_order_unsigned(attr, label_attr=nil)
    if label_attr
      where("#{attr} IS NOT NULL and #{attr} <> ''").order("cast(#{attr} as unsigned)").select("DISTINCT #{attr}, #{label_attr}").inject({}) do |h, r|
        h[r[attr]] = r[label_attr] || r[attr] 
        h
      end
    else
      where("#{attr} IS NOT NULL and #{attr} <> ''").order("cast(#{attr} as unsigned)").pluck("DISTINCT #{attr}")
    end
  end

  private

    def after_initialize
      if self.class.active_config && self.new_record?
        self.class.active_config.each do |attr, options|
          default = options[:default]
          if self[attr].nil? && !default.nil?
            self[attr] = default
          end
        end
      end
    end

    def prepare
      # Rails.logger.debug "^^# prepare 1"
      inflect_strings
      calculate_fields
    end

    def inflect_strings
      if self.class.active_config
        self.class.active_config.each do |attr, options|
          if options.has_key?(:inflect)
            self[attr] = self[attr].try(options[:inflect])
          end
        end
      end
    end

    def calculate_fields
      
      if self.class.active_config
        self.class.active_attributes(calc: true).each do |attr|
          # Rails.logger.debug "^^ attr #{attr}"
          unless self.respond_to?(:_overrides) && self._overrides[attr.to_sym]
            timestamp = DateTime.now
            value = self.send("#{attr}_calc") rescue nil
            value = nil if value.is_a?(BigDecimal) && value.infinite?
            # binding.pry if (attr.to_sym == :seller_finance_output_hold_weeks)
            begin
              self[attr] = (value unless value.missing?)
            rescue Exception => e
              p "calcualte_fileds error message: #{e.message} attr: #{attr}"
            end
            p "[#{((DateTime.now - timestamp)*24*60*60*1000).to_i}] calculated :#{attr}" if ActiveConfig.debug?
          end
        end
      end
      # raise Exception
    end
end

### MIGRATIONS ############################################################################################

class ActiveRecord::ConnectionAdapters::TableDefinition

  def decimal(*args)
    method_missing(:decimal, *args)
  end

  def file(*args)
    method_missing(:file, *args)
  end

  def method_missing(symbol, *args)
    if args.length.in?(1,2)
      config = ActiveConfig.find(symbol, :migrate, false)
      if config
        migrate = config[:migrate] || {}
        if migrate.is_a?(Hash)
          options = (migrate).merge(args.extract_options!)
          p "column(#{args.first}, #{config[:as] || config[:type]}, #{options})" if ActiveConfig.debug?
          return column(args.first, config[:as] || config[:type], options)
        elsif migrate.is_a?(Proc)
          return migrate[self, args.first]
        end
      end 
    end
    super
  end
end

### ACTIONVIEW UTILS ######################################################################################

module ActionView::Helpers::TagHelper
  class DefinitionsBuilder
    attr_accessor :object, :template, :form, :options

    def initialize(object, template, form, options)
      self.object = object
      self.template = template
      self.form = form
      self.options = options
    end

    def row(*args)
      options = self.options.merge(args.extract_options!)
      puts "options:  #{options}"
      options[:columns] ||= 2
      options[:item_class] ||= 'align-right'
      options[:bordered_bottom] ||= ''
      template.content_tag(:tr) do
        attrs = args.flatten
        attrs.unshift(attrs.last) if attrs.length == options[:columns]  - 1
        editable = options[:editable]
        # puts "editable #{editable}"
        Delayed::Worker.logger.debug "see attrib #{attrs}"
        label_attr = attrs.shift
        puts ">>label attr #{label_attr}"
        if options[:fixed_label]
           label = template.label_tag(label_attr, options[:fixed_label], class: '') 
        else
          if options[:label]
            label = template.label_tag(label_attr, self.object.class.label(options[:label], options[:label_type]), class: '')
          else
            label = template.label_tag(label_attr, self.object.class.label(label_attr, options[:label_type]), class: '')
          end
        end
        output = template.content_tag(:td, label, class: ['align-left title', options[:title_class],options[:bordered_bottom], ('valign-middle' if editable)].compact.join(' '))
        attrs = Array.new(options[:columns]-2) << label_attr if attrs.empty?
        puts "attr count >> #{attrs.count}"
        attr_count = attrs.count
        i = 0
        for attr in attrs
          unless attr.nil?
            if options[:definition_class]
              item =  self.object.format(attr, as: "string")
            else  
              if  options[:as]
                item = (editable ? form.input(attr, label: false, as: options[:as]) : self.object.format(attr, as: options[:as])) 
              else
                item = (editable ? form.input(attr, label: false) : self.object.format(attr, as: options[:as]))
              end
            end
            editable = false if editable == true
          end
          puts ">>last item width #{options[:last_item_width]}"
          output << template.content_tag(:td, item, class: ["item", options[:item_class],options[:bordered_bottom], ('valign-middle' if options[:editable]), ( 'last-item-width'  if (options[:last_item_width] && (i == (attr_count - 1))))].compact.join(' '))
          i += 1
        end
        puts ">>output<< #{attr}" if attr == "sale_output_purchase_expense"
        output
      end
    end
    
    def row2(*args)
      options = self.options.merge(args.extract_options!)
      puts "options:  #{options}"
      options[:columns] ||= 2
      options[:item_class] ||= 'align-right'
      options[:bordered_bottom] ||= ''
      template.content_tag(:tr) do
        attrs = args.flatten
        attrs.unshift(attrs.last) if attrs.length == options[:columns]  - 1
        editable = options[:editable]
        # puts "editable #{editable}"
        # puts "see attrib #{attrs}"
        label_attr = attrs.shift
        puts ">>label attr #{label_attr}"
        if options[:fixed_label]
           label = template.label_tag(label_attr, self.object.class.label2(options[:fixed_label], nil), class: '') 
        else
          if options[:label]
            label = template.label_tag(label_attr, self.object.class.label2(options[:label], options[:label_type]), class: '')
          else
            label = template.label_tag(label_attr, self.object.class.label2(label_attr, options[:label_type]), class: '')
          end
        end
        output = template.content_tag(:td, label, class: ['align-left title', options[:title_class],options[:bordered_bottom], ('valign-middle' if editable)].compact.join(' '))
        attrs = Array.new(options[:columns]-2) << label_attr if attrs.empty?
        puts "attr count >> #{attrs.count}"
        attr_count = attrs.count
        i = 0
        for attr in attrs
          unless attr.nil?
            if options[:definition_class]
              item =  self.object.format(attr, as: "string")
            else  
              if  options[:as]
                item = (editable ? form.input(attr, label: false, as: options[:as]) : self.object.format(attr, as: options[:as])) 
              else
                item = (editable ? form.input(attr, label: false) : self.object.format(attr, as: options[:as]))
              end
            end
            editable = false if editable == true
          end
          puts ">>last item width #{options[:last_item_width]}"
          output << template.content_tag(:td, item, class: ["item", options[:item_class],options[:bordered_bottom], ('valign-middle' if options[:editable]), ( 'last-item-width'  if (options[:last_item_width] && (i == (attr_count - 1))))].compact.join(' '))
          i += 1
        end
        puts ">>output<< #{attr}" if attr == "sale_output_purchase_expense"
        output
      end
    end
    
    def row3(*args)
      options = self.options.merge(args.extract_options!)
      puts "options:  #{options}"
      options[:columns] ||= 2
      options[:item_class] ||= 'align-right'
      options[:bordered_bottom] ||= ''
      options[:border_left_color] ||= 'no-border-left'
      template.content_tag(:tr) do
        attrs = args.flatten
        attrs.unshift(attrs.last) if attrs.length == options[:columns]  - 1
        editable = options[:editable]
        # puts "editable #{editable}"
        # puts "see attrib #{attrs}"
        label_attr = attrs.shift
        puts ">>label attr #{label_attr}"
        if options[:fixed_label]
           label = template.label_tag(label_attr, self.object.class.label2(options[:fixed_label], nil), class: '') 
        else
          if options[:label]
            label = template.label_tag(label_attr, self.object.class.label2(options[:label], options[:label_type]), class: '')
          else
            label = template.label_tag(label_attr, self.object.class.label2(label_attr, options[:label_type]), class: '')
          end
        end
        # output = template.content_tag(:td, label, class: ['align-left title', options[:title_class],options[:bordered_bottom], ('valign-middle' if editable)].compact.join(' '))
        attrs = Array.new(options[:columns]-2) << label_attr if attrs.empty?
        puts "attr count >> #{attrs.count}"
        attr_count = attrs.count
        i = 0
        for attr in attrs
          unless attr.nil?
            if options[:definition_class]
              item =  self.object.format(attr, as: "string")
            else  
              if  options[:as]
                item = (editable ? form.input(attr, label: false, as: options[:as]) : self.object.format(attr, as: options[:as])) 
              else
                item = (editable ? form.input(attr, label: false) : self.object.format(attr, as: options[:as]))
              end
            end
            editable = false if editable == true
          end
          puts ">>last item width #{options[:last_item_width]}"
          output = template.content_tag(:td, item, class: ["item", options[:item_class],options[:bordered_bottom], ('valign-middle' if options[:editable]), ( 'last-item-width'  if (options[:last_item_width] && (i == (attr_count - 1))))].compact.join(' '), style: 'border-left: 0px #FFF' )
          i += 1
        end
        puts ">>output<< #{attr}" if attr == "sale_output_purchase_expense"
        output
      end
    end
    
    def row4(*args)
      options = self.options.merge(args.extract_options!)
      puts "options:  #{options}"
      options[:columns] ||= 2
      options[:item_class] ||= 'align-right'
      options[:bordered_bottom] ||= ''
      options[:border_left_color] ||= 'no-border-left'
      template.content_tag(:tr) do
        attrs = args.flatten
        attrs.unshift(attrs.last) if attrs.length == options[:columns]  - 1
        editable = options[:editable]
        # puts "editable #{editable}"
        # puts "see attrib #{attrs}"
        label_attr = attrs.shift
        puts ">>label attr #{label_attr}"
        if options[:fixed_label]
           label = template.label_tag(label_attr, self.object.class.label2(options[:fixed_label], nil), class: '') 
        else
          if options[:label]
            label = template.label_tag(label_attr, self.object.class.label2(options[:label], options[:label_type]), class: '')
          else
            label = template.label_tag(label_attr, self.object.class.label2(label_attr, options[:label_type]), class: '')
          end
        end
        # output = template.content_tag(:td, label, class: ['align-left title', options[:title_class],options[:bordered_bottom], ('valign-middle' if editable)].compact.join(' '))
        attrs = Array.new(options[:columns]-2) << label_attr if attrs.empty?
        puts "attr count >> #{attrs.count}"
        attr_count = attrs.count
        i = 0
        for attr in attrs
          unless attr.nil?
            if options[:definition_class]
              item =  self.object.format(attr, as: "string")
            else  
              if  options[:as]
                item = (editable ? form.input(attr, label: false, as: options[:as], input_html: {:readonly => true}) : self.object.format(attr, as: options[:as])) 
              else
                item = (editable ? form.input(attr, label: false) : self.object.format(attr, as: options[:as]))
              end
            end
            editable = false if editable == true
          end
          puts ">>last item width #{options[:last_item_width]}"
          output = template.content_tag(:td, item, class: ["item", options[:item_class],options[:bordered_bottom], ('valign-middle' if options[:editable]), ( 'last-item-width'  if (options[:last_item_width] && (i == (attr_count - 1))))].compact.join(' '), style: 'border-left: 0px #FFF' )
          i += 1
        end
        puts ">>output<< #{attr}" if attr == "sale_output_purchase_expense"
        output
      end
    end

    def rows(*args)
      options = args.extract_options!
      attrs = args.flatten
      output = ""
      attrs.each do |attr|
        output << row(attr, options.dup)
      end
      output.html_safe
    end
  end

  def definitions_for(object, options={}, &block)
    builder = DefinitionsBuilder.new(object, self, options.delete(:form), options)
    caption = options.delete(:caption)
    if options[:style]
      content_tag(:table, class: 'table table-definitions', style: options[:style]) do
        output = ""
        output << content_tag(:caption, caption) if caption
        output << capture(builder, &block) if block_given?
        output.html_safe
      end
    else
      content_tag(:table, class: 'table table-definitions') do
        output = ""
        output << content_tag(:caption, caption) if caption
        output << capture(builder, &block) if block_given?
        output.html_safe
      end

    end
  end
  
  def definitions_for2(object, options={}, &block)
    builder = DefinitionsBuilder.new(object, self, options.delete(:form), options)
    caption = options.delete(:caption)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      output = ""
      output << content_tag(:caption, caption) if caption
      output << capture(builder, &block) if block_given?
      output.html_safe
    end
  end
  
  def definitions_for3(object, options={}, &block)
    builder = DefinitionsBuilder.new(object, self, options.delete(:form), options)
    caption = options.delete(:caption)
    content_tag(:table, class: 'table table-definitions', style: 'margin-bottom: 0px') do
      output = ""
      output << content_tag(:caption, caption) if caption
      output << capture(builder, &block) if block_given?
      output.html_safe
    end
  end
end

module ActionView
  class Base
    alias t_without_symbol t
    def t(key, options = {})
      key = ".#{key}" if key.is_a?(Symbol)
      t_without_symbol(key, options)
    end
  end
end

### SIMPLE FORM EXTENSION ################################################################################

module SimpleForm
  class FormBuilder < ActionView::Helpers::FormBuilder

    def definitions(options={}, &block)
      puts "~~options #{options}"
      template.definitions_for(self.object, options.merge(form: self), &block)
    end
    
    def definitions2(options={}, &block)
      puts "~~options #{options}"
      template.definitions_for3(self.object, options.merge(form: self), &block)
    end

    def self.add_type(type)
      input_class = ActiveConfig.find(type, :input)
      if input_class.is_a?(Proc)
        input_class = Object.const_set("#{type.to_s.camelcase}Input", Class.new(SimpleForm::Inputs::Base) { 
          define_method :input, input_class
        })
      end
      map_type type, :to => input_class if input_class
    end
    
    def textarea(attribute_name, options={}, &block)
      options = @defaults.deep_dup.deep_merge(options) if @defaults

      #
      model_class = options[:model] || self.object.class
      if model_class.respond_to?(:active_config)
        overriden = self.object.respond_to?(:_overrides) && self.object._overrides.has_key?(attribute_name)
        options[:input_html] ||= {}
        options[:input_html].merge_with_class!(class: :overriden) if overriden
        as = model_class.attr_config(attribute_name, :as)
        if as
          html_options = ActiveConfig.find(as, :input_html_options)
          type_options = html_options ? html_options[model_class, attribute_name] : {}
          options = {as: as}.merge(type_options).merge_with_class(options)
        end
        options[:label] = model_class.label(attribute_name) if options[:label].missing?
      end
      #

      input = find_input(attribute_name, options, &block)

      chosen =
        if name = options[:wrapper] || find_wrapper_mapping(input.input_type)
          name.respond_to?(:render) ? name : SimpleForm.wrapper(name)
        else
          wrapper
        end

      chosen.render input
    end
    
    # TODO proper override
    def input_no_blank(attribute_name, options={}, &block)
      options = @defaults.deep_dup.deep_merge(options) if @defaults

      #
      model_class = options[:model] || self.object.class
      if model_class.respond_to?(:active_config)
        overriden = self.object.respond_to?(:_overrides) && self.object._overrides.has_key?(attribute_name)
        options[:input_html] ||= {}
        options[:input_html].merge_with_class!(class: :overriden) if overriden
        as = model_class.attr_config(attribute_name, :as)
        if as
          html_options = ActiveConfig.find(as, :input_html_options)
          type_options = html_options ? html_options[model_class, attribute_name] : {}
          options = {as: as}.merge(type_options).merge_with_class(options)
        end
        options[:label] = model_class.label(attribute_name) if options[:label].missing?
      end
      #

      input = find_input(attribute_name, options, &block)

      chosen =
        if name = options[:wrapper] || find_wrapper_mapping(input.input_type)
          name.respond_to?(:render) ? name : SimpleForm.wrapper(name)
        else
          wrapper
        end
      input.input_options.merge!({include_blank: false})
      Delayed::Worker.logger.debug "~~ render input #{input.input_options}" if attribute_name == :near_commercial
      chosen.render input
    end
    
    # TODO proper override
    def input(attribute_name, options={}, &block)
      options = @defaults.deep_dup.deep_merge(options) if @defaults

      #
      model_class = options[:model] || self.object.class
      if model_class.respond_to?(:active_config)
        overriden = self.object.respond_to?(:_overrides) && self.object._overrides.has_key?(attribute_name)
        options[:input_html] ||= {}
        options[:input_html].merge_with_class!(class: :overriden) if overriden
        as = model_class.attr_config(attribute_name, :as)
        if as
          html_options = ActiveConfig.find(as, :input_html_options)
          type_options = html_options ? html_options[model_class, attribute_name] : {}
          options = {as: as}.merge(type_options).merge_with_class(options)
        end
        options[:label] = model_class.label(attribute_name) if options[:label].missing?
        
      end
      #

      input = find_input(attribute_name, options, &block)

      chosen =
        if name = options[:wrapper] || find_wrapper_mapping(input.input_type)
          name.respond_to?(:render) ? name : SimpleForm.wrapper(name)
        else
          wrapper
        end
      Delayed::Worker.logger.debug "~~ render input #{input.input_options}" if attribute_name == :near_commercial
      chosen.render input
    end
    
     # TODO proper override
    def input_no_label(attribute_name, options={}, &block)
      options = @defaults.deep_dup.deep_merge(options) if @defaults

      #
      model_class = options[:model] || self.object.class
      if model_class.respond_to?(:active_config)
        overriden = self.object.respond_to?(:_overrides) && self.object._overrides.has_key?(attribute_name)
        options[:input_html] ||= {}
        options[:input_html].merge_with_class!(class: :overriden) if overriden
        options[:input_html] = options[:input_html].merge({class: "input-medium-large"}) 
        as = model_class.attr_config(attribute_name, :as)
        if as
          html_options = ActiveConfig.find(as, :input_html_options)
          type_options = html_options ? html_options[model_class, attribute_name] : {}
          options = {as: as}.merge(type_options).merge_with_class(options)
        end
        options[:label] = ""
      end
      #

      input = find_input(attribute_name, options, &block)
    
      chosen =
        if name = options[:wrapper] || find_wrapper_mapping(input.input_type)
          Delayed::Worker.logger.debug "option 1"
          name.respond_to?(:render) ? name : SimpleForm.wrapper(name)
        else
          Delayed::Worker.logger.debug "option 2"
          wrapper.options.merge!(wrapper: false)
          wrapper
        end
      Delayed::Worker.logger.debug "wrapper #{chosen.options}"
      input.input_options.merge!({label: false,inline_label: true})
      Delayed::Worker.logger.debug "~~ render input #{input.input_options}" if attribute_name == :rep
      chosen.render input
    end

    def multiple_input(attribute_name, options={}, &block)

      options[:input_html] ||= {}
      options[:input_html][:multiple] = true
      # if options[:collection].blank?
      #   options[:collection] ||= []
      #   # options[:as] = :select
      #   options[:input_html][:disabled] = :disabled 
      # end
      puts "%%^&f attribute name: #{attribute_name}"
      input(attribute_name, options, &block)
    end
    
    def single_input(attribute_name, options={}, &block)

      options[:input_html] ||= {}
      options[:input_html][:multiple] = false
      options[:input_html][:include_blank] = false
      # if options[:collection].blank?
      #   options[:collection] ||= []
      #   # options[:as] = :select
      #   options[:input_html][:disabled] = :disabled 
      # end
      puts "%%^&f attribute name: #{attribute_name}"
      input_no_blank(attribute_name, options, &block)
    end
    
    def special_input(attribute_name, options={}, &block)

      options[:input_html] ||= {}
      options[:input_html][:multiple] = true
      options[:input_html][:include_blank] = false
      # if options[:collection].blank?
      #   options[:collection] ||= []
      #   # options[:as] = :select
      #   options[:input_html][:disabled] = :disabled 
      # end
      puts "%%^&f attribute name: #{attribute_name}"
      input_no_label(attribute_name, options, &block)
    end

    def slider(attribute_name, from, to, step, options={})
      if from.nil? || to.nil? || from == to
        label(attribute_name).concat(template.content_tag(:i, 'Not enough data', class: 'small muted'))
      else
        input(attribute_name, input_html: { 
          class: 'slider', 
          "data-options" => options.merge(from: from, to: to, step: step).to_json 
        })
      end
    end
  end
end

### WHEN READY ############################################################################################

ActiveConfig.on_type_ready = Proc.new do |type|
  SimpleForm::FormBuilder.add_type(type)
end
