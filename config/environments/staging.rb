Web::Application.configure do
  HOST = "45.35.251.155:81"
  #SUBURI = "/greenlight"
   # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = true

  # Log error messages when you accidentally call methods on nil.
  # config.whiny_nils = true
  config.eager_load = false

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false

  # Compress JavaScripts and CSS
  config.assets.compress = true
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = true


  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default_url_options = { :host => HOST }

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :stderr

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  #config.active_record.auto_explain_threshold_in_seconds = 0.5

  config.assets.precompile += %w( html5.js properties.js searches.js client_properties.js)
  # Expands the lines which load the assets
  config.assets.debug = true
  config.log_level = :debug
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5

  # allow assets to be prefixed with sub-uri directory, requires thin --prefix /sub-uri
  #ENV["RAILS_RELATIVE_URL_ROOT"] = SUBURI
  #config.action_controller.relative_url_root = SUBURI
  #config.assets.prefix = "#{SUBURI}/assets"
  #config.log_level = :warn
end