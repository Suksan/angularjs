server '45.35.251.155', :app, :web, :db, :primary => true
set :rails_env, 'staging'
set :application, "greenlight"
set :deploy_to, "/var/greenlight"
set :branch, "rel_2.0_skit"
