require 'CSV'
require 'capybara/poltergeist'
require 'capybara/dsl'
include Capybara::DSL
require 'spreadsheet'

File.delete("file.xls") if File.exist?("file.xls")

Spreadsheet.client_encoding = 'UTF-8'

book = Spreadsheet::Workbook.new
sheet1 = book.create_worksheet :name => "staffs"

Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, :js_errors => false, :timeout => 60)
    #Capybara::Poltergeist::Driver.new(app, :js_errors => false, :debug => true)
end
Capybara.default_driver = :poltergeist
Capybara.javascript_driver = :poltergeist
Capybara.default_wait_time = 20
Capybara.ignore_hidden_elements = false

session = Capybara::Session.new(:poltergeist)

session.visit "http://sabor.connectmls.com/"
session.fill_in "userid", :with => "473731"
session.fill_in "password", :with => "sanant804"
session.click_button "Sign In"
m_server = session.current_url.to_s.match(/(http:\/\/[\w]+.connectmls.com)/)
home_url = session.current_url

k = 0
l = 0
(57..65).each { |n|

  if n.to_s.length < 2 
    m = "0#{n}"
  else
    m = "#{n}"
  end



  (0..9).each { |o|
    (0..9).each { |q|
      p = "#{m}#{o}#{q}"
      # sheet1 = book.create_worksheet :name => p
      # File.delete("file.csv")
      session.visit home_url
      session.find("nobr", :text => "Search").hover
      session.find("a", :text => "Member").click
      session.within_frame('main'){
        session.within_frame('workspace'){
          session.fill_in 'USERID', :with => "#{p}"
        }
      }

      session.within_frame('main'){
        session.within_frame('workspace'){
          session.find(:xpath,"//input[@id='searchButtonBottom']").click
        }
      }


      session.within_frame('main'){
        session.within_frame('workspace'){
          doc = Nokogiri::HTML(session.html.gsub("\n","").gsub("\t",""))
          ary = []
          doc.xpath("//table[@class='gridview']//tr[@height='20']").each { |d|
            i = 0
            hsh = {}
            number,last,first,member_id,email,office_id,office_name,city,office_phone,type = nil,nil,nil,nil,nil,nil,nil,nil,nil,nil
            d.children.each { |t|
        	     case i
                when 0
                  number = l + 1
                when 2
                  last = t.text.strip
                when 3
                  first = t.text.strip
                when  5   
                  member_id = t.text.strip
                when 6 
                  m_email =  t.to_s.match(/href="javascript:%20var%20card='\.+([\/\.\da-zA-Z\?=&;]+)'/)
                  puts "m_email #{m_email}"
                  
                  # puts m_server
                  if m_email && m_email.length > 0
                    my_email = m_email[1]
                  end
                  email_link = "#{m_server}#{my_email}"
                  puts "#{email_link}"
                  if email_link.include? "bcard"
                    session.visit  email_link.gsub(/&amp;/,'&')
                    sleep 2.5/2.0
                    begin
                      email = session.find(:xpath,"//tbody//a[contains(@href,'@')]").text
                    rescue
                      email = nil
                    end
                  else
                    email = 'N/A'
                  end
                when 7 
                  office_id = t.text.strip
                when 8 
                  office_name = t.text.strip
                when 9 
                  city = t.text.strip
                when 10 
                  office_phone = t.text.strip
                when 12 
                  type = t.text.strip

              end
              # print "#{i} #{t.text.strip}\r\n"
              
                      i += 1
            }

            l += 1

            hsh["last"] = last
            hsh["first"] = first
            hsh["member_id"] = member_id
            hsh["email"] = email
            hsh["office_id"] = office_id
            hsh["office_name"] = office_name
            hsh["city"] = city
            hsh["office_phone"] = office_phone
            hsh["type"]  = type

            ary << hsh
            puts "#{number} #{first} #{last} #{member_id} #{email} #{office_id} #{office_name} #{city} #{office_phone} #{type}"

          }

          columns = %w{first last member_id email office_id office_name city office_phone type}
          
          # if File.exist?("file.xls")
          #   book  = Spreadsheet.open('file.xls') 
          #   sheet1 =  book.worksheet 0 
          # else
          #   book = Spreadsheet::Workbook.new
            
          # end

          
          
          j = 0 
          ary.each  do |ar|

            columns.each do |d|
              sheet1.row(k).push  ar[d] 
            end
            
            j += 1
            k += 1
          end

          

          # mode =  File.exist?("file.csv") ? "a+" : "wb"
          # CSV.open("file.csv", mode)  do |csv|
          #   # csv << columns
          #   ary.each do |ar|
          #     csv << columns.map do |d|   ar[d]   end
          #   end
          # end
          
        
        }
      }

    }

  }
  book.write 'file.xls'  
}

