angular.module('SimbaNicApp').controller('trucksController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {

    $scope.listView = function() {

        jQuery('.page-head').show();
        $scope.showListView = true;
        $scope.showMapView = false;
        $scope.renderopportunitiesMap = false;
    }

   
    // $scope.listView();

    // var query = "";

    // var trucksView = $state.current.data.trucksView ? $state.current.data.trucksView : 'active';

    // $scope.$on('$viewContentLoaded', function() {
    //     // initialize core components
    //     App.initAjax();
    // });

    // $scope.getDatetime = function() {
    //     return new Date().getTime();
    // };
    // $scope.trucksViewModals = function() {
    //     $ngBootbox.customDialog({
    //         templateUrl: 'assets/app/components/trucks/detailview/trucksView.html?timestamp=' + new Date().getTime(),
    //         size: 'large',
    //         closeButton: false,
    //         className: 'simbanic_trucks_dialog',
    //         scope: $scope
    //     });
    // }


    $scope.trucksViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/trucks/detailview/trucksView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_trucks_dialog',
            scope: $scope
        });
    }
    $scope.truckAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/trucks/trucksAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_trucks_dialog',
            scope: $scope
        });
    }


});

SimbaNicApp.controller('trucksViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getTruck(id) {
         // alert("get location");
         CgseqService.getTruck(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load truck data: ' + error.message;
            });
    }
    $scope.editTruck = function (db) {
        // alert('yes');
       
        $params = $.param({
            "id": db.id,
            "licence_number": db.licence_number,
            "make": db.make,
            "model": db.model,
            "colour": db.colour,
            "type": db.type,
            "year": db.year,
            "mileage": db.mileage

        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveTruck(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getTruck($scope.selected_id);


});
SimbaNicApp.controller('truckAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initTruckAdd(id) {
          // alert("get truck" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            licence_number : "",
            make : "",
            model : "",
            colour : "",
            type : "",
            year : "",
            mileage : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editTruck = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.truck_obj = {id: record.id, licence_number: record.licence_number, make: record.make, model: record.model, colour: record.colour, type: record.type, year: record.year, mileage: record.mileage};

        $params = $.param({
            "id": $scope.truck_obj.id,
            "licence_number": $scope.truck_obj.licence_number,
            "make": $scope.truck_obj.make,
            "model": $scope.truck_obj.model,
            "colour": $scope.truck_obj.colour,
            "type": $scope.truck_obj.type,
            "year": $scope.truck_obj.year,
            "mileage": $scope.truck_obj.mileage

        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddTruck($scope.truck_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initTruckAdd($scope.selected_id);


});