/* Setup Login controller */
angular.module('SimbaNicApp').controller('loginController', ['$rootScope', '$http', '$scope', 'settings', 'AuthService', '$window', function($rootScope, $http, $scope, settings, AuthService, $window) {

	$scope.loginForm = {};
	$scope.loginForm.userName = "";
	$scope.loginForm.password = "";

	$scope.forgetForm = {};
	$scope.forgetForm.userName = "";
	$scope.forgetForm.userEmail = "";

	//AuthService.checkLocalStorage();
	if (AuthService.getStatus() == true) 
	{
		$window.location.href = '#/dashboard.html';
		$window.location.reload();
	}

	$scope.simbaLogin = function(){
		
		if(jQuery('.login-form').valid())
		{
			$rootScope.simbaLoadingShow();

	    	var params = {
	    		user_auth:{
			        user_name: $scope.loginForm.userName,
			        password: $scope.loginForm.password,
			        encryption: 'PLAIN'
			    },
			    application: 'RestGreenlet'
			}
			var json = JSON.stringify(params);

		    var loginParam = jQuery.param({
		    	method : "login",
			    input_type : "JSON",
			    response_type : "JSON",
			    rest_data : json
		    });

			$http({
			    url: settings.apiURL + 'service/v4_1/rest.php',
			    method: 'POST',
			    dataType: 'JSON',
			    data: loginParam,
			    headers: {
			        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
			    },
			}).success(function(response){

				alert(JSON.stringify(response));
				$rootScope.simbaLoadingHide();
				if(response.id)
				{
					console.log("Success");
					AuthService.setStatus(true);
					AuthService.setSimbaTimeOut(false);
					AuthService.setCurrentUser(response);
					AuthService.setUserId(response.name_value_list.user_id.value);
					AuthService.setSessionId(response.id);

					$rootScope.loginBean = {};
				    $rootScope.loginBean.loggedIn = AuthService.getStatus();
				    $rootScope.loginBean.sessionId = AuthService.getSessionId();
				    $rootScope.loginBean.currentUser = AuthService
				            .getCurrentUser();
		            $rootScope.loginBean.simbaTimeOut = AuthService
				            .getSimbaTimeOut();
		            
					$window.location.href = '#/dashboard.html';
				}
				else
				{
					alert('Please Enter Correct Username and Password');
				}
			}).error(function(error){
			    
			});
		}
	}

	$scope.simbaForget = function() {
		
		if(jQuery('.forget-form').valid())
		{
		 //    var forgetParam = jQuery.param({
		 //    	method : "forget_password",
			//     input_type : "JSON",
			//     response_type : "JSON",
			//     user_email : $scope.forgetForm.userEmail,
			//     username: $scope.forgetForm.userName,
		 //    });

			// $http({
			//     url: settings.apiURL + 'service/v4_1/rest.php',
			//     method: 'POST',
			//     dataType: 'JSON',
			//     data: forgetParam,
			//     headers: {
			//         'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
			//     },
			// }).success(function(response){
			// 	if(response.success)
			// 	{
			// 		alert('Please check your email');
			// 	}
			// 	else
			// 	{
			// 		alert('Please Enter Correct Username and Email Address');
			// 	}
			// }).error(function(error){
			    
			// });
		}
	}

    $scope.$on('$viewContentLoaded', function() {   
    	// initialize core components
    	App.initAjax();

    	// set default layout mode
    	$rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
    });

}]);
