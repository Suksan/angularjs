/**
 * Authentication Service
 */

SimbaNicApp.factory('AuthService',
	function($http, $localStorage, settings, $window, $rootScope) {

		var storage = $localStorage.$default({
			loggedIn : false,
			simbaTimeOut: false,
			sessionId: 0,
			currentUser : '',
			userRole : '',
			selectedMenuItem : '',
			companyId : 0,
			userId : 0,
			loginTimeStamp : new Date().getTime()
		});
		return {
			getStatus : function() {
				return storage.loggedIn;
			},
			setStatus : function(value) {
				storage.loggedIn = value;
			},
			getSessionId : function() {
				return storage.sessionId;
			},
			setSessionId : function(value) {
				storage.sessionId = value;
			},
			setSimbaTimeOut : function(value) {
				storage.simbaTimeOut = value;
			},
			getSimbaTimeOut : function() {
				return storage.simbaTimeOut;
			},
			getCurrentUser : function() {
				return storage.currentUser;
			},
			setCurrentUser : function(loggedInUser) {
				storage.currentUser = loggedInUser;
			},
			
			getTimeStamp : function() {
				storage.loginTimeStamp;
			},
			setSelectedMenuItem : function(menuItem) {
				storage.selectedMenuItem = menuItem;
			},
			getSelectedMenuItem : function() {
				return storage.selectedMenuItem;
			},
			setUserId : function(userId) {
				storage.userId = userId;
			},
			getUserId : function() {
				return storage.userId;
			},
			checkLocalStorage : function() {
				// console.log('Diff::'+ (new Date().getTime() -
				// storage.loginTimeStamp)/(1000*60));
				if ((new Date().getTime() - storage.loginTimeStamp) / (1000 * 60) > 20) {
					storage.currentUser = '';
					storage.loggedIn = false;
					storage.loginTimeStamp = new Date().getTime();
				}
			},
			checkAuthentication : function(){
				if ($rootScope.loginBean.simbaTimeOut) {
					$window.location.href = '#/lock.html';
				}
				else if(storage.loggedIn == false || storage.loggedIn != true){
					$window.location.href = '#/login.html';
				}
			},
		};

	});

SimbaNicApp.factory('Auth', function($rootScope, $http, $localStorage, settings, $window, AuthService) {

	var simbaAuth = {};

	simbaAuth.checkAuthentication = function() {
		if ($rootScope.loginBean.simbaTimeOut) {
			$window.location.href = '#/lock.html';
			return false;
		}
		
		var param = {
			session: AuthService.getSessionId()
		}
		var restParam = JSON.stringify(param);

	    var paramData = jQuery.param({
	    	method : "seamless_login",
		    input_type : "JSON",
		    response_type : "JSON",
		    rest_data : restParam	
	    });

		var request = $http({
		    url: settings.apiURL + 'service/v4_1/rest.php',
		    method: 'POST',
		    dataType: 'JSON',
		    data: paramData,
		    headers: {
		        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
		    },
		});
		return request;
	}

	return simbaAuth;

});