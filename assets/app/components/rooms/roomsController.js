angular.module('SimbaNicApp').controller('RoomsController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

     $scope.roomViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/rooms/detailview/roomView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_room_dialog',
            scope: $scope
        });
    }
    $scope.roomAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/rooms/roomAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_room_dialog',
            scope: $scope
        });
    }
});
SimbaNicApp.controller('roomViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getRoom(id) {
         // alert("get location");
         CgseqService.getRoom(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load room data: ' + error.message;
            });
    }
    $scope.editRoom = function (db) {
        // alert('yes');
       
        $params = $.param({
            "id": db.id,
            "name": db.name,
            "location_id": db.location_id,
            "capacity": db.capacity

        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveRoom(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getRoom($scope.selected_id);


});
SimbaNicApp.controller('roomAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initRoomAdd(id) {
          // alert("get room" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            name : "",
            location_id : "",
            capacity : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editRoom = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.room_obj = {id: record.id, name: record.name, location_id: record.location_id, capacity: record.capacity};

        $params = $.param({
            "id": $scope.room_obj.id,
            "name": $scope.room_obj.name,
            "location_id": $scope.room_obj.location_id,
            "capacity": $scope.room_obj.capacity

        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddRoom($scope.room_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initRoomAdd($scope.selected_id);


});