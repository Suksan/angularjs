angular.module('SimbaNicApp').controller('dashboardController', function($rootScope, $scope, $http, $timeout, Auth, $window, SimbaNicAPI, closingsFunctions, AuthService) {
	
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        App.initAjax();
    });

    $rootScope.getDeviceWidth = $window.innerWidth;
    $scope.count = {};
    $scope.tasks = {};

    angular.element($window).bind('resize', function(){
        $rootScope.$apply(function(){
            $rootScope.getDeviceWidth = $window.innerWidth;
        })
    });

    $scope.getEntriesCount = function(module_name, query) {
    	$rootScope.simbaLoadingShow();
    	SimbaNicAPI.getEntriesCount(module_name, query)
        	.success(function(response) {
        		if(typeof(response.result_count) === 'undefined')
                {
                    $scope.getEntriesCount(module_name);
                }
                else
                {
                	$scope.count[module_name] = response.result_count;
                	$rootScope.simbaLoadingHide();
                }
        	});
    }

    $scope.getEntriesCount('Opportunities', "(opportunity_status_c = 'offer_make_offer' OR opportunity_status_c = 'offer_out_for_signature' OR opportunity_status_c = 'offer_signed_ready_to_submit' OR opportunity_status_c = 'offer_sent_to_seller') OR (opportunity_status_c = 'closing_contingencies' OR opportunity_status_c = 'closing_inspection' OR opportunity_status_c = 'closing_post_constingencies')");
    $scope.getEntriesCount('Tasks', "tasks.status != 'Completed' AND tasks.assigned_user_id = '" + AuthService.getUserId() + "'");

    $scope.getEntryList = function(unique_name, module_name, query, order_by, offset, select_fields, link_name_to_fields_array, max_results) {
        $rootScope.simbaLoadingShow();
        
        SimbaNicAPI.getEntryList(module_name, query, order_by, offset, select_fields, link_name_to_fields_array, max_results)
            .success(function(response) {
                if(typeof(response.data) === 'undefined')
                {
                    $scope.getEntryList(module_name);
                }
                else
                {
                    $scope[unique_name] = response.data;
                    $rootScope.simbaLoadingHide();
                }
            });
    }

    $scope.dashletOffers = function() {
        $scope.dashletOffer = true;
        //$scope.dashletClosing = false;
    };

    $scope.dashletClosings = function() {
        //$scope.dashletOffer = false;
        $scope.dashletClosing = true;
    };

    var closings_query = "(opportunity_status_c = 'closing_contingencies' OR opportunity_status_c = 'closing_inspection' OR opportunity_status_c = 'closing_post_constingencies') OR (opportunity_status_c = 'closed_purchased') OR (opportunity_status_c = 'closing_cancel_contract')";

    var offers_query = "(opportunity_status_c = 'offer_make_offer' OR opportunity_status_c = 'offer_out_for_signature' OR opportunity_status_c = 'offer_signed_ready_to_submit' OR opportunity_status_c = 'offer_sent_to_seller') OR (opportunity_status_c = 'offer_lost_deal')";

    //$scope.getEntryList('Closings', 'Opportunities', closings_query, 'date_entered DESC', 0, ["id", "opportunity_status_c", "property_address_c", "property_address_city_c", "property_address_state_c", "property_address_postalcode_c", "close_date_c"], 20);

    //$scope.getEntryList('Offers', 'Opportunities', offers_query, 'date_entered DESC', 0, ["id", "opportunity_status_c", "property_address_c", "property_address_city_c", "property_address_state_c", "property_address_postalcode_c", "close_date_c"], 20);

    //$scope.getEntryList('Tasks', 'Tasks', "tasks.status != 'Completed' AND tasks.assigned_user_id = '" + AuthService.getUserId() + "'", 'date_due ASC', 0, ["id", "name", "status", "property_address_c", "property_address_city_c", "property_address_state_c", "property_address_postalcode_c"], 20);

    $scope.taskComplete = function(task_id){

        if($scope.tasks[task_id])
        { 
            if(confirm("Are you sure you want to Confirm this task?"))
            {
                $rootScope.simbaLoadingShow();

                var name_value_list = [
                    {
                        "name": "id",
                        "value": task_id,
                    },
                    {
                        "name": "status",
                        "value": "Completed"
                    }
                ];

                SimbaNicAPI.save('Tasks', name_value_list)
                    .success(function(response) {
                        scope.listFilterBy();
                        $rootScope.simbaLoadingHide();
                })
            }
       }
    }

    $scope.getClosingsFunctionName = function(status) {
        var status_name = closingsFunctions.getClosingsStatus(status);
        if(status_name == "Won" || status_name == "Inspect" || status_name == "Closing" || status_name == "Offer")
        {
            return 'Active';
        }
        return status_name;

    }

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    
});
