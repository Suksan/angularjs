angular.module('SimbaNicApp').controller('CalendarController', function($rootScope, $scope, $http, $timeout, Auth, AuthService, $stateParams, settings, $filter, $location, SimbaNicAPI, uiCalendarConfig, $log, $window) {

    $scope.events = [];
    $scope.filterQuery = "";
    $scope.filter1 = 'incomplete';
    $scope.filter2 = 'my_tasks';

    $scope.reloadRemoteEvents = function(reset) {

        if (typeof(reset)==='undefined') reset = false;

        if(reset)
        {
            $scope.events = [];
        }

        if($scope.filterStatusSelected == "")
        {
            $scope.events = [];
            $scope.renderCalender();
            return false;
        }

        $rootScope.simbaLoadingShow();

        SimbaNicAPI.getEntryList('Tasks', $scope.filterQuery, '', 0, ["id", "name", "date_due", "status"], [], 50)
        .success(function(response) {
            
            if(response.data)
            {
                var count = 0;
                var eventsLength = response.data.length;

                angular.forEach(response.data, function(data) {
                    
                    var date_due = data.date_due;
                    if(date_due)
                    {
                        var start_date = new Date(date_due.replace(/-/g,"/"));
                        var event_class_name = "";
                        var task_status = data.status;
                        if(task_status != 'Completed')
                        {
                            event_class_name = 'incomplete';
                        }
                        else
                        {
                            event_class_name = 'completed';
                        }

                        $scope.events.push({
                            id: data.id,
                            title: data.name,
                            start: start_date,
                            url: '#/Tasks/' + data.id + '?return_action=Calendar',
                            stick: true,
                            className: event_class_name,
                        });
                    }
                    count = count + 1;
                    if(eventsLength == count)
                    {
                        $scope.renderCalender();
                        $rootScope.simbaLoadingHide();
                    }
                });
            }
        });
    }

    $scope.getFilterQuery = function() {

        var query = "";

        if($scope.filter1 == "incomplete")
        {
            if(query)
            {
                query = query + " AND tasks.status != 'Completed' ";
            }
            else
            {
                query = " tasks.status != 'Completed' ";
            }
        }
        else if($scope.filter1 == "complete")
        {
            if(query)
            {
                query = query + " AND tasks.status = 'Completed' ";
            }
            else
            {
                query = " tasks.status = 'Completed' ";
            }
        }
        else if($scope.filter1 == "all_status")
        {
            if(query)
            {
                query = query + " AND tasks.status != '' ";
            }
            else
            {
                query = " tasks.status != '' ";
            }
        }

        if($scope.filter2 == "all_user_tasks")
        {
            if(query)
            {
                query = query + " AND tasks.assigned_user_id != 0 ";
            }
            else
            {
                query = " tasks.assigned_user_id != 0 ";
            }
        }
        else if($scope.filter2 == "my_tasks")
        {
            if(query)
            {
                query = query + " AND tasks.assigned_user_id = '" + AuthService.getUserId() + "' ";
            }
            else
            {
                query = " tasks.assigned_user_id = '" + AuthService.getUserId() + "'";
            }
        }
        
        $scope.filterQuery = query;
        console.log($scope.filterQuery);
    };

    $scope.listFilterBy = function(status) {
        $scope.getFilterQuery();
        $scope.reloadRemoteEvents(true);
    };

    $scope.getFilterQuery();
    $scope.reloadRemoteEvents();

    $scope.changeView = function (view, element) {
        //alert("Change");
    };

    $scope.uiConfig = {
        calendar:{
            height: $window.innerHeight >= 1024 ? $window.innerHeight - jQuery('.page-header').outerHeight(true) - jQuery('.page-head').outerHeight(true) - jQuery('.page-breadcrumb').outerHeight(true) - 200 : 450,
            editable: false,
            stick: true,
            header:{
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            viewRender: function(view, element) {
                $log.debug("View Changed: ", view.visStart, view.visEnd, view.start, view.end);
            }
            
        }
    };

    $scope.renderCalender = function() {
        $timeout(function() {
            uiCalendarConfig.calendars['simbaCalendar'].fullCalendar('removeEvents');
            uiCalendarConfig.calendars['simbaCalendar'].fullCalendar('addEventSource', $scope.events);
            //uiCalendarConfig.calendars['simbaCalendar'].fullCalendar('renderEvent', $scope.events, true);
        });
    };
    console.log($scope.events);
    $scope.eventSources = [$scope.events];



    /*$scope.options = {
        weekOffset: 1,
        daysOfTheWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        constraints: {
            startDate: moment().subtract(1, 'months').format('YYYY-MM-16'),
            endDate: moment().add(2, 'months').format('YYYY-MM-16')
        }
    };

    $scope.today = function(){
        $scope.clndr.setMonth(moment().format('MM') - 1);
        $scope.clndr.setYear(moment().format('YYYY'));
    }

    $scope.events = [
        { date: moment().add(3, 'days').format(), title: "Happy days", timeStart: "10:00", timeEnd: "11:00" },
        { date: moment().subtract(5, 'days').format(), title: "Good old days", timeStart: "10:00", timeEnd: "11:00" },
        { date: moment().subtract(5, 'days').format(), title: "And some more", timeStart: "10:00", timeEnd: "11:00" }
    ];

    $scope.showEvents = function(events) {
        
        if(events != null)
        {
            console.log(events);
            events.map(function(event) {
                $scope.selectedDay = event.date;
                jQuery('#clndr_events').addClass('events_visible events_over');
            }).join("\n");
            
        }
    };

    $scope.closeEvents = function() {
        jQuery('#clndr_events').removeClass('events_visible events_over');
    };
    */

});
