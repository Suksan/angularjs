SimbaNicApp.directive('ngDetailViewContact', function(settings, AuthService, $rootScope, $state, $filter, $stateParams, $http, $window, Upload, $injector, $location, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var $validationProvider = $injector.get('$validation');
            
            angular.extend($validationProvider, {
                validCallback: function (element){
                    $(element).parents('.form-group:first').removeClass('has-error');
                },
                invalidCallback: function (element) {
                    $(element).parents('.form-group:first').addClass('has-error');
                }
            });

        	scope.edit = function() {
                scope.isCreate = false;
                scope.contactDetail = scope.contact;
        		scope.editView = true;
        	}

            scope.create = function() {
                scope.isCreate = true;
                scope.contactDetail = scope.contact;
                scope.contact = {};
                scope.editView = true;
                if($location.search().name)
                {
                    scope.contact.first_name = $location.search().name;
                }
            }

            if($location.search().create)
            {
                scope.create();
            }

            scope.cancel = function() {
                if($location.search().guid)
                {
                    $window.location.href = '#/Closings/' + $location.search().guid;
                    return false;
                }
                scope.isCreate = false;
                scope.contact = scope.contactDetail;
                scope.editView = false;
            }

            scope.upload = function (file, guid) {
                
                $rootScope.simbaLoadingShow();
                var param = {
                    session: AuthService.getSessionId(),
                    module: 'Contacts',
                    id: guid,
                    field_name: 'photo',
                    file: file
                }
                var restJson = JSON.stringify(param);

                var restData = jQuery.param({
                    method : "simba_file_upload",
                    input_type : "JSON",
                    response_type : "JSON",
                    rest_data : restJson
                });
                
                Upload.upload({
                    url: settings.apiURL + 'custom/service/v4_1_custom/rest.php',
                    data: {
                        method : "simba_file_upload",
                        rest_data: restJson,
                        input_type : "JSON",
                        response_type : "JSON",
                        photo: file
                    },
                }).then(function (resp) {
                    $rootScope.simbaLoadingHide();

                    if(scope.isCreate)
                    {
                        $window.location.href = '#/Contacts/' + guid;
                        //$window.location.reload();
                        scope.isCreate = false;
                    }
                    if($location.search().guid)
                    {
                        $window.location.href = '#/Closings/' + $location.search().guid;
                        return false;
                    }
                    
                    scope.contact.photo = file.name;
                    scope.getContactProfileURL();
                    jQuery("div[data-id='"+ scope.contact.id +"'] img").attr('src', scope.contact.photoURL);
                    jQuery("div[data-id='"+ scope.contact.id +"'] .simba_contact_name").html(scope.contact.first_name + " " + scope.contact.last_name);
                    scope.editView = false;
                    
                }, function (resp) {
                    //console.log('Error status: ' + resp.status);
                }, function (evt) {
                    //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
            };

        	scope.save = function() {

                if(!$validationProvider.checkValid)
                {
                    return false;
                }

                scope.isSaveBtnDisabled = true;
                scope.isCancelBtnDisabled = true;
            
                $rootScope.simbaLoadingShow();
                
        		var param = {
                    session: AuthService.getSessionId(),
                    module_name: 'Contacts',
                    name_value_list: [
                        [
                            {
                                "name": "id",
                                "value": scope.contact.id,
                            },
                            {
                                "name": "first_name",
                                "value": scope.contact.first_name
                            },
                            {
                                "name": "last_name",
                                "value": scope.contact.last_name
                            },
                            {
                                "name": "company_name_c",
                                "value": scope.contact.company_name_c
                            },
                            {
                                "name": "phone_work",
                                "value": scope.contact.phone_work
                            },
                            {
                                "name": "phone_mobile",
                                "value": scope.contact.phone_mobile
                            },
                            {
                                "name": "title",
                                "value": scope.contact.title
                            },
                            {
                                "name": "department",
                                "value": scope.contact.department
                            },
                            {
                                "name": "phone_fax",
                                "value": scope.contact.phone_fax
                            },
                            {
                                "name": "email1",
                                "value": scope.contact.email1
                            },
                            {
                                "name": "primary_address_street",
                                "value": scope.contact.primary_address_street
                            },
                            {
                                "name": "primary_address_city",
                                "value": scope.contact.primary_address_city
                            },
                            {
                                "name": "primary_address_postalcode",
                                "value": scope.contact.primary_address_postalcode
                            },
                            {
                                "name": "alt_address_street",
                                "value": scope.contact.alt_address_street
                            },
                            {
                                "name": "alt_address_city",
                                "value": scope.contact.alt_address_city
                            },
                            {
                                "name": "alt_address_postalcode",
                                "value": scope.contact.alt_address_postalcode
                            },
                            {
                                "name": "description",
                                "value": scope.contact.description
                            },
                        ],
                    ],
                }
                var restJson = JSON.stringify(param);

                var restData = jQuery.param({
                    method : "set_entries",
                    input_type : "JSON",
                    response_type : "JSON",
                    rest_data : restJson 
                });

                $http({
                    url: settings.apiURL + 'service/v4_1/rest.php',
                    method: 'POST',
                    dataType: 'JSON',
                    data: restData,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8 multipart/form-data'
                    },
                }).success(function(response) {

                    scope.contact.id = response.ids[0];

                    if($stateParams.guid == 0)
                    {
                        if($location.search().guid)
                        {
                            $rootScope.simbaLoadingShow();
                            SimbaNicAPI.setRelationship('Opportunities', $location.search().guid, 'contacts', [scope.contact.id])
                                .success(function(response) {

                                    $rootScope.simbaLoadingHide();

                                });
                        }
                    }

                    if(scope.contact.image)
                    {
                        scope.upload(scope.contact.image, response.ids[0]);
                    }
                    else
                    {
                        if(scope.isCreate)
                        {
                            $window.location.href = '#/Contacts/' + scope.contact.id;
                        }
                        if($location.search().guid)
                        {
                            $window.location.href = '#/Closings/' + $location.search().guid;
                            return false;
                        }
                        scope.editView = false;
                        jQuery("div[data-id='"+ scope.contact.id +"'] .simba_contact_name").html(scope.contact.first_name + " " + scope.contact.last_name);
                        $rootScope.simbaLoadingHide();
                        scope.isCreate = false;
                    }

                    scope.isSaveBtnDisabled = false;
                    scope.isCancelBtnDisabled = false;
                    

                }).error(function(error) {
                    
                });
        	}

            $('[data-toggle="confirmation"]').confirmation({
                btnOkClass: 'btn-success',
                btnCancelClass: 'btn-danger',
                singleton: true,
                popout: true,
                container: 'body'
            });

            $('#contact_delete_btn').on('confirmed.bs.confirmation', function () {

                var param = {
                    session: AuthService.getSessionId(),
                    module_name: 'Contacts',
                    name_value_list: [
                        [
                            {
                                "name": "id",
                                "value": $stateParams.guid,
                            },
                            {
                                "name": "deleted",
                                "value": "1"
                            }
                        ],
                    ],
                }
                var restJson = JSON.stringify(param);

                var restData = jQuery.param({
                    method : "set_entries",
                    input_type : "JSON",
                    response_type : "JSON",
                    rest_data : restJson 
                });

                $http({
                    url: settings.apiURL + 'service/v4_1/rest.php',
                    method: 'POST',
                    dataType: 'JSON',
                    data: restData,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
                    },
                }).success(function(response){
                    $window.location.href = '#/Contacts';
                }).error(function(error) {
                    scope.error = error;
                });

            });
        }
    }
});