angular.module('SimbaNicApp').controller('ContactListViewController', function($rootScope, $scope, $http, $timeout, Auth, AuthService, $injector) {

    var $validationProvider = $injector.get('$validation');
	
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    $scope.broadcast = function(name) {
        $scope.$broadcast(name);
    }

    $scope.save = function() {
        if(!$validationProvider.checkValid)
        {
            return false;
        }
        $scope.$broadcast('save');
    }

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
});
