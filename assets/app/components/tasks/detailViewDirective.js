SimbaNicApp.directive('ngDetailViewTask', function(settings, AuthService, $rootScope, $state, $filter, $stateParams, $http, $window, $injector, $location, SimbaNicAPI) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var $validationProvider = $injector.get('$validation');
            
            angular.extend($validationProvider, {
                validCallback: function (element){
                    $(element).parents('.form-group:first').removeClass('has-error');
                },
                invalidCallback: function (element) {
                    $(element).parents('.form-group:first').addClass('has-error');
                }
            });

            scope.delete = function(id) {
                
                $rootScope.simbaLoadingShow();

                SimbaNicAPI.delete('Tasks', id)
                    .success(function(response) {
                        $rootScope.simbaLoadingHide();
                        $window.location.href = '#/Tasks';
                    }).error(function(error) {
                        scope.delete(id);
                    });
            }

        	scope.edit = function() {
        		scope.editView = true;
        	}

            scope.cancel = function() {
                if($location.search().guid)
                {
                    $window.location.href = '#/Closings/' + $location.search().guid;
                    return false;
                }
                else if($location.search().create)
                {
                    $window.location.href = '#/Tasks';
                    return false;
                }
                scope.editView = false;
                scope.createView = false;
            }

        	scope.save = function() {

                var regex = /^([0]\d|[1][0-2]):([0-5]\d)\s?(?:AM|PM)$/i;

                if(!$validationProvider.checkValid)
                {
                    return false;
                }

                $rootScope.simbaLoadingShow();

                var date_due;

                if(scope.task.date_due)
                {
                    var date = scope.formatDate(scope.task.date_due);
                    date_due = $filter('date')(date, 'yyyy-MM-dd');
                    if(scope.task.date_due_time)
                    {
                        date_due = date_due + ' ' + scope.convertAmPmTo24Hour(scope.task.date_due_time) + ':00';
                    }
                    
                }
                else
                {
                    date_due = "";
                }

                var name_value_list = [
                    {
                        "name": "id",
                        "value": $stateParams.guid,
                    },
                    {
                        "name": "name",
                        "value": scope.task.name
                    },
                    {
                        "name": "description",
                        "value": scope.task.description
                    },
                    {
                        "name": "date_due",
                        "value": date_due
                    },
                ];

                if($stateParams.guid == 0)
                {
                    var AssignObj = {};
                    AssignObj['name'] = 'assigned_user_id';
                    AssignObj['value'] = AuthService.getUserId();
                    name_value_list.push(AssignObj);
                }
                
                if(scope.task.parent_id)
                {
                    var AssignObj = {};
                    AssignObj['name'] = 'parent_id';
                    AssignObj['value'] = scope.task.parent_id;
                    name_value_list.push(AssignObj);
                    var AssignObj = {};
                    AssignObj['name'] = 'parent_type';
                    AssignObj['value'] = 'Opportunities';
                    name_value_list.push(AssignObj);
                }

                SimbaNicAPI.save('Tasks', name_value_list)
                    .success(function(response) {

                        if(response.id)
                        {
                            scope.task.id = response.id;
                            
                            if(scope.noteFiles)
                            {
                                var count = 0;
                                var notesLength = scope.noteFiles.length;

                                angular.forEach(scope.noteFiles, function(file) {
                                    
                                    count = count + 1;
                                    if(notesLength == count)
                                    {
                                        scope.upload(file, response.id, true);
                                    }
                                    else
                                    {
                                        scope.upload(file, response.id, false);
                                    }
                                });
                            }
                            else
                            {
                                if($location.search().guid)
                                {
                                    $window.location.href = '#/Closings/' + $location.search().guid;
                                    return false;
                                }
                                else if(scope.createView)
                                {
                                    $window.location.href = '#/Tasks/' + response.id;
                                    return false;
                                }
                                else
                                {
                                    $rootScope.simbaLoadingHide();
                                    scope.editView = false;
                                }
                            }
                        }
                });
        	}

            scope.selectedProperty = function(selected) {
                if (selected) {
                    scope.task.parent_id = selected.originalObject.id;
                }
            }

            scope.statusChange = function(status)
            {
                $rootScope.simbaLoadingShow();

                var name_value_list = [
                    {
                        "name": "id",
                        "value": $stateParams.guid,
                    },
                    {
                        "name": "status",
                        "value": status
                    }
                ];

                SimbaNicAPI.save('Tasks', name_value_list)
                    .success(function(response) {
                        $rootScope.simbaLoadingHide();
                        scope.task.status = status;
                    }).error(function(error) {
                        alert('Something went wrong');
                    });
            }


            $('#status_incomplete_btn').on('confirmed.bs.confirmation', function (){
                scope.statusChange('In Progress');
            });

            $('#status_complete_btn').on('confirmed.bs.confirmation', function (){
                scope.statusChange('Completed');
            });
        }
    }
});

SimbaNicApp.directive('simbaCreateTaskNoteConfirmation', function($timeout, $rootScope){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            jQuery('[data-simba-toggle=confirmation]').confirmation({
                btnOkClass: 'btn-success',
                btnCancelClass: 'btn-danger',
                singleton: true,
                container: 'body'
            });

            jQuery('[data-simba-toggle=confirmation]').on('confirmed.bs.confirmation', function () {
                $rootScope.simbaLoadingShow();
                scope.removeNoteItem(jQuery(this).attr("data-index"));
                setTimeout(function(){
                    $rootScope.simbaLoadingHide();
                }, 5000);
            });
        }
    }
})

SimbaNicApp.directive('dueDatePicker', function($timeout){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.datepicker({
                format: "mm/dd/yyyy",
                autoclose: true,
                startDate: new Date(),
            });

            scope.dueDatePicker = function() {
                $timeout(function() {
                    elem.triggerHandler('focus');
                }, 100);
            };
        }
    }
})

SimbaNicApp.directive('dueTimePicker', function($timeout){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            var date_due_time = scope.task.date_due_time;
            
            elem.timepicker({
                autoclose: true,
                showSeconds: false,
                minuteStep: 30,
                defaultTime: false,
                showMeridian: true,
            });
            
            if(date_due_time)
            {
                //elem.timepicker('setTime', date_due_time);
            }

            elem.timepicker().on('show.timepicker', function(e) {
                if (typeof(scope.task.date_due_time) === 'undefined')
                {
                    elem.timepicker('setTime', '12:00 PM');
                }
                else
                {
                    elem.timepicker('setTime', scope.task.date_due_time);
                }
            });

        }
    }
})

SimbaNicApp.directive('ngSubpanelNotes', function(settings, $stateParams, AuthService, $rootScope, SimbaNicAPI, $location) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            if($location.search().create)
            {
                return false;
            }

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                responsive: {
                    
                },

                "order": [
                    [0, 'asc']
                ],

                "processing": true,
                "serverSide": true,

                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "method": "POST",
                    "data": function ( d ) {
                        d.method = "get_simba_relationships";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = JSON.stringify({
                            session: AuthService.getSessionId(),
                            module_name: 'Tasks',
                            module_id: $stateParams.guid,
                            link_field_name: 'notes',
                            related_module_query: "",
                            related_fields: ["id", "filename"],
                            related_module_link_name_to_fields_array: [],
                            deleted: '0',
                            order_by: d.columns[d.order[0]['column']]['name'] + " " + d.order[0]['dir'],
                            offset: d.start,
                            limit: d.length,
                        });
                    },
                },

                columnDefs: [ {
                    'orderable': false,
                    'targets': [1]
                }, ],
                
                "lengthMenu": $rootScope.simbaDataTable.subpanelLengthMenu,
                
                "pageLength": $rootScope.simbaDataTable.subpanelPageLength,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "width": "80%",
                        "name": "filename",
                        "render" : function (data, type, row){
                            var fileURL = settings.crmURL + "/index.php?entryPoint=download&id="+data.id + "&type=Notes";
                            return "<a href='" + fileURL + "' target='_blank'>"+ data.filename +"</a>";
                        }, 
                    },
                    { 
                        "data": null,
                        "width": "20%",
                        "render" : function (data, type, row){
                            return "<button class='btn btn-outline red-mint btn-small notes_delete_btn' \
                                    data-toggle='note-confirmation' \
                                    data-id = '" + data.id + "'\
                                    >Remove</button>";
                        },
                        "targets": 0
                    },
                ],
                "fnDrawCallback":function(){

                    $('[data-toggle="note-confirmation"]').confirmation({
                        btnOkClass: 'btn-success',
                        btnCancelClass: 'btn-danger',
                        singleton: true,
                        popout: true,
                        container: 'body',
                    });
                    

                    $('[data-toggle="note-confirmation"]').on('confirmed.bs.confirmation', function () {

                        $rootScope.simbaLoadingShow();

                        SimbaNicAPI.delete('Notes', jQuery(this).attr("data-id")).success(function(response){
                            scope.reloadNotes();
                            $rootScope.simbaLoadingHide();
                        }).error(function(error) {
                            
                        });
                    });

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#subpanel_notes_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#subpanel_notes_wrapper .dataTables_length').css("display", "block");
                        $('#subpanel_notes_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                        $('#subpanel_notes_wrapper .dataTables_paginate').css("display", "none");
                        $('#subpanel_notes_wrapper .dataTables_length').css("display", "none");
                        $('#subpanel_notes_wrapper .dataTables_filter').css("display", "none");
                     }
                }
                
            });

            scope.reloadNotes = function() {
                elem.dataTable()._fnAjaxUpdate();
            };
        }
    };
});