angular.module('SimbaNicApp').controller('OpportunityDetailViewController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions,CgseqService, $filter) {

    $scope.isLike = true;
    $scope.isReject = false;
    $scope.isSave = false;
    $scope.simbaFirstTabDisplay = true;
    $scope.simbaSecondTabDisplay = false;
    $scope.simbaNeighborhoodTab = false;
    $scope.compsRentalTrends = false;
    $scope.compsSaleTrends = false;
    $scope.database ;
    $scope.player = 'deram';

    $scope.properyLike = function() {
        $scope.isLike = !$scope.isLike;
        if($scope.isReject)
        {
            $scope.isReject = !$scope.isReject;
        }
    }
    $scope.properyReject = function() {
        $scope.isReject = !$scope.isReject;
        if($scope.isLike)
        {
            $scope.isLike = !$scope.isLike;
        }
    }

    $scope.residential_marker_img = settings.siteURL + settings.imgPath + '/residential_places.png';
    
    $scope.formatPrice = function(price) {
        return $filter('megaNumber')(price);
    }

    NgMap.getMap().then(function(map) {
        $scope.map = map;
        $scope.map.center = "5311 Maple Street";
    });

    $scope.gotoTab = function(tabName) {
        $scope.simbaFirstTabDisplay = true;
        $scope.simbaSecondTabDisplay = false;
        $scope.simbaNeighborhoodTab = false;
        jQuery('.simba_first_tab li.first').addClass('active');
        jQuery('.simba_second_tab li').removeClass('active');

        $anchorScroll.yOffset = 85;
        $location.hash('opportunity_' + tabName);
        $anchorScroll();
    };

     $scope.nextTab = function(tabName) {
        $scope.simbaFirstTabDisplay = false;
        $scope.simbaSecondTabDisplay = true;
        if(tabName == "neighborhood_data")
        {
            $scope.simbaNeighborhoodTab = true;
        }
        jQuery('.simba_first_tab li').removeClass('active');
          if(tabName == "rehab_estimator")
        {
            $timeout(function() {
                jQuery('.simba_second_tab li.opportunity_rehab_estimator').addClass('active');
                jQuery('.scroll-to-top').trigger('click');
            }, 10);
            //jQuery('.simba_second_tab li.opportunity_rehab_estimator').addClass('active');
        }
    }
    
    
    $scope.getRandomSpan = function(min, max){
	  // return Math.floor((Math.random()*6)+1);
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}

    $scope.rentalMapProperties = function() {

        var _rentalPins = [];

        // _rentalPins.push({"id":5976872,"mark":"","street_address":"231 Weatherstone Pointe Dr","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1500.0","dom":33,"beds":3,"latitude":"34.0920119","longitude":"-84.46813600000002","display":true});
        // _rentalPins.push({"id":4281164,"mark":"","street_address":"223 Claremore Dr","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1200.0","dom":92,"beds":3,"latitude":null,"longitude":null,"display":true});
        // _rentalPins.push({"id":5838655,"mark":"","street_address":"160 Weatherstone Dr","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1530.0","dom":25,"beds":3,"latitude":"34.09194","longitude":"-84.4721129","display":true});
        // _rentalPins.push({"id":5884192,"mark":"","street_address":"347 Weatherstone Pl","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1350.0","dom":25,"beds":3,"latitude":"34.089865","longitude":"-84.4752139","display":true});
        // _rentalPins.push({"id":5903503,"mark":"","street_address":"335 Weatherstone Pl","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1425.0","dom":3,"beds":3,"latitude":"34.090758","longitude":"-84.474868","display":true});
        // _rentalPins.push({"id":5915182,"mark":"","street_address":"311 Weatherstone Pl","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1395.0","dom":71,"beds":3,"latitude":"34.088916","longitude":"-84.47304299999999","display":true});
        // _rentalPins.push({"id":5935794,"mark":"","street_address":"335 Weatherstone Pl","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1425.0","dom":10,"beds":3,"latitude":"34.090758","longitude":"-84.474868","display":true});
        // _rentalPins.push({"id":5984721,"mark":"","street_address":"204 Weatherstone Xing","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1395.0","dom":15,"beds":3,"latitude":"34.0912999","longitude":"-84.4729119","display":true});
        // _rentalPins.push({"id":6407445,"mark":"","street_address":"804 Cataya Cv","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1500.0","dom":10,"beds":3,"latitude":null,"longitude":null,"display":true});
        // _rentalPins.push({"id":6410653,"mark":"","street_address":"319 Weatherstone Pl","city":"Woodstock","rental_output_cap_rate":null,"gross_yield":"","list_price":"1395.0","dom":49,"beds":3,"latitude":"34.089443","longitude":"-84.473658","display":true});
      

        $scope.rentalMapInfo = {
            center: new google.maps.LatLng(34.089865, -84.4721129),
            zoom: 14,
            pins: _rentalPins,
        };

        // Always follow Best Practices
        // You can retrieve google co-ordinates from below service.
        /*$http.get('http://maps.google.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false').success(function(mapData) {
            //angular.extend($scope, mapData);
        });

        var bounds = new google.maps.LatLngBounds();

        angular.forEach($scope.rentalMapInfo.pins, function(value, key) {
            var latlng = new google.maps.LatLng(value.lat, value.lng);
            bounds.extend(latlng);
        });

        NgMap.getMap({ id:'rentalMap' }).then(function(map) {
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            $scope.map = map;
            google.maps.event.trigger($scope.map, 'resize');
        });*/
    }

    $scope.saleMapProperties = function() {

        var _salePins = [];
        // alert("sale map");
        // _salePins.push({ "id": "1","title": "A","address": "286 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "105000","dom": "2558","beds": "4", "lat":37.5082, "lng":-122.267332, "display": true});
        // _salePins.push({ "id": "2","title": "B","address": "310 Elm St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "106000","dom": "2558","beds": "4", "lat":37.50921953, "lng": -122.2661137, "display": true});
        // _salePins.push({ "id": "3","title": "C","address": "144 Elm st","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "107000","dom": "2558","beds": "4", "lat":37.5102, "lng": -122.2671, "display": true});
        // _salePins.push({ "id": "4","title": "D","address": "260 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "108000","dom": "2558","beds": "4", "lat":37.508319, "lng": -122.268319, "display": true});
        // _salePins.push({ "id": "5","title": "E","address": "281 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "109000","dom": "2558","beds": "4", "lat":37.507945, "lng": -122.267611, "display": true});
        // _salePins.push({ "id": "6","title": "F","address": "90 Cedar St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "110000","dom": "2558","beds": "4", "lat":37.509717, "lng": -122.270479, "display": true});
        // _salePins.push({ "id": "7","title": "G","address": "351 Cedar St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "111000","dom": "2558","beds": "4", "lat":37.506130, "lng": -122.267869, "display": true});
        // _salePins.push({ "id": "8","title": "H","address": "378 EI Camino Real","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "112000","dom": "2558","beds": "4", "lat":37.509235, "lng": -122.263499, "display": true});
        // _salePins.push({ "id": "9","title": "I","address": "1007 Bookland Ave","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "113000","dom": "2558","beds": "4", "lat":37.509524, "lng": -122.273726, "display": true});
        // _salePins.push({ "id": "10","title": "J","address": "287 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "114000","dom": "2558","beds": "4", "lat":37.508447, "lng": -122.266570, "display": true});
        // _salePins.push({ "id": "11","title": "K","address": "1242 Magnolia Ave","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "115000","dom": "2558","beds": "4", "lat":37.508864, "lng": -122.265079, "display": true});
        // _salePins.push({ "id": "12","title": "L","address": "199 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "101000","dom": "2558","beds": "4", "lat":37.508813, "lng": -122.268233, "display": false});
        // _salePins.push({ "id": "13","title": "M","address": "277 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "102000","dom": "2558","beds": "4", "lat":37.508030, "lng": -122.267525, "display": false});
        // _salePins.push({ "id": "14","title": "N","address": "290 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "103000","dom": "2558","beds": "4", "lat":37.508081, "lng": -122.267274, "display": false});
        // _salePins.push({ "id": "15","title": "O","address": "282 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "104000","dom": "2558","beds": "4", "lat":37.508353, "lng": -122.267611, "display": false});

        getSaleMapPins();


        // $scope.saleMapInfo = {
        //     center: new google.maps.LatLng(34.089865, -84.4721129),
        //     zoom: 15,
        //     pins: _salePins,
        // };

        /*var bounds = new google.maps.LatLngBounds();

        angular.forEach($scope.saleMapInfo.pins, function(value, key) {
            var latlng = new google.maps.LatLng(value.lat, value.lng);
            bounds.extend(latlng);
        });

        NgMap.getMap({ id:'saleMap' }).then(function(map) {
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            $scope.map = map;
            google.maps.event.trigger($scope.map, 'resize');
        });*/
    }
    
    
    

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
        // Here, Please Put oppoertunity title dynamic (5305)
        if($location.search().trigger)
        {
            $timeout((function() {
                jQuery('#simbanic_overview_photos img.overview_photos_0').trigger('click');
            }), 2000);
        }
        
    });

    $scope.updateBook = function(database) {
        // alert(database.flood);
        var flood_val = null;
        var near_commercial_val = null;

        if (angular.isUndefined(database.flood) || database.flood === null ) {
           // alert('1');
            flood_val = null;
        } else {
           // alert('2');
           switch(database.flood) {
               case 'High':
                   flood_val = 'high';
                   break;
               case 'Moderate-low':
                   flood_val = 'moderate-low';
                   break;
               case 'Undetermined' :
               	   flood_val = 'undetermined';
                   break;
           }
            // flood_val = database.flood;
        }
         if (angular.isUndefined(database.near_commercial) || database.near_commercial === null ) {
            near_commercial_val = null;
        } else {
           switch(database.near_commercial) {
               case 'Yes':
                   near_commercial_val = '1';
                   break;
               case 'No':
                   near_commercial_val = '0-low';
                   break;
           }
            // near_commercial_val = database.near_commercial;
        }

        
        $params = $.param({
          "property": {
            "flood": flood_val,
            "near_commercial": near_commercial_val      
          }       
        }) 
        
        CgseqService.saveBooks($stateParams.guid,$params);
     }
    
     var debounceSaveUpdates = function(newVal, oldVal) {
        console.debug('new value' + ' ' + newVal + ' old value' + ' ' + oldVal);
        if (newVal != oldVal) {
          if ($scope.timeout) {
            $timeout.cancel($scope.timeout)
          }
          $scope.timeout = $timeout($scope.updateBook, 2000, true, $scope.database);  // 1000 = 1 second
    
        }
    };

    // $scope.getSeq = function(response) {
//          
       // $scope.database = CgseqService.getInfo();
       // // alert('database:' + $scope.database);
    // };
//     
    // $scope.getSeq();
    
    function getRentalMapPins() {
    	 CgseqService.getRentalMapInfo($stateParams.guid)
            .then(function (response) {
                // alert(JSON.stringify(response.data.data));
                // alert(JSON.stringify(response.data));
                $scope.rentalMapInfo.pins = response.data.data;

                
                
               
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }

    function getSaleMapPins() {
         CgseqService.getSaleMapInfo($stateParams.guid)
            .then(function (response) {
                // alert(JSON.stringify(response.data.data));
                // $scope.saleMapInfo.pins = response.data.data;


 // var _salePins = [];
 //        // alert("sale map");
 //        _salePins.push({ "id": "1","title": "A","address": "286 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "105000","dom": "2558","beds": "4", "lat":37.5082, "lng":-122.267332, "display": true});
 //        _salePins.push({ "id": "2","title": "B","address": "310 Elm St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "106000","dom": "2558","beds": "4", "lat":37.50921953, "lng": -122.2661137, "display": true});
 //        _salePins.push({ "id": "3","title": "C","address": "144 Elm st","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "107000","dom": "2558","beds": "4", "lat":37.5102, "lng": -122.2671, "display": true});
 //        _salePins.push({ "id": "4","title": "D","address": "260 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "108000","dom": "2558","beds": "4", "lat":37.508319, "lng": -122.268319, "display": true});
 //        _salePins.push({ "id": "5","title": "E","address": "281 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "109000","dom": "2558","beds": "4", "lat":37.507945, "lng": -122.267611, "display": true});
 //        _salePins.push({ "id": "6","title": "F","address": "90 Cedar St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "110000","dom": "2558","beds": "4", "lat":37.509717, "lng": -122.270479, "display": true});
 //        _salePins.push({ "id": "7","title": "G","address": "351 Cedar St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "111000","dom": "2558","beds": "4", "lat":37.506130, "lng": -122.267869, "display": true});
 //        _salePins.push({ "id": "8","title": "H","address": "378 EI Camino Real","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "112000","dom": "2558","beds": "4", "lat":37.509235, "lng": -122.263499, "display": true});
 //        _salePins.push({ "id": "9","title": "I","address": "1007 Bookland Ave","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "113000","dom": "2558","beds": "4", "lat":37.509524, "lng": -122.273726, "display": true});
 //        _salePins.push({ "id": "10","title": "J","address": "287 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "114000","dom": "2558","beds": "4", "lat":37.508447, "lng": -122.266570, "display": true});
 //        _salePins.push({ "id": "11","title": "K","address": "1242 Magnolia Ave","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "115000","dom": "2558","beds": "4", "lat":37.508864, "lng": -122.265079, "display": true});
 //        _salePins.push({ "id": "12","title": "L","address": "199 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "101000","dom": "2558","beds": "4", "lat":37.508813, "lng": -122.268233, "display": false});
 //        _salePins.push({ "id": "13","title": "M","address": "277 Fir St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "102000","dom": "2558","beds": "4", "lat":37.508030, "lng": -122.267525, "display": false});
 //        _salePins.push({ "id": "14","title": "N","address": "290 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "103000","dom": "2558","beds": "4", "lat":37.508081, "lng": -122.267274, "display": false});
 //        _salePins.push({ "id": "15","title": "O","address": "282 Chestnut St","city": "San Carlos","cap": "41","gross_yield": "2016/01/10","list": "104000","dom": "2558","beds": "4", "lat":37.508353, "lng": -122.267611, "display": false});

                $scope.saleMapInfo = {
                    center: new google.maps.LatLng($scope.database.latitude, $scope.database.longitude),
                    zoom: 15,
                    pins: response.data.data,
                };
                
               
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
    function getCustomer() {
         CgseqService.getInfo($stateParams.guid)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                $state.current.data.pageTitle =  "Opportunity" + " | " + $scope.database.street_address;
                $scope.rentalTrendsChart.data.rows = $scope.database.rental_comps;
                $scope.saleTrendsChart.data.rows = $scope.database.sale_comps;
                // if ($scope.database.street_address) {
                $scope.flood_options = [{
                    value: 'High',
                    name: 'High'
                    }, {
                    value: 'Moderate-low',
                    name: 'Moderate-low'
                    }, {
                    value: 'Undetermined',
                    name: 'Undetermined'
                    }];
                    
                
                
                $scope.near_commercial_options = [{
                    value: 'Yes',
                    name: 'Yes'
                    }, {
                    value: 'No',
                    name: 'No'
                    }];
                    
                    
                $scope.comp_level_rental_avm_options = [{
                    value: $scope.database.comp_level_rental_expanded_avm ,
                    name:  $scope.database.comp_level_rental_expanded_avm 
                    }, {
                    value: $scope.database.comp_level_rental_moderate_avm ,
                    name:  $scope.database.comp_level_rental_moderate_avm 
                    }, {
                    value: $scope.database.comp_level_rental_conservative_avm ,
                    name:  $scope.database.comp_level_rental_conservative_avm 
                    }
                    ];
                    
               $scope.comp_level_sale_avm_options = [{
                    value: $scope.database.comp_level_sale_expanded_avm ,
                    name: $scope.database.comp_level_sale_expanded_avm 
                    }, {
                    value: $scope.database.comp_level_sale_moderate_avm ,
                    name: $scope.database.comp_level_sale_moderate_avm 
                    }, {
                    value: $scope.database.comp_level_sale_conservative_avm ,
                    name: $scope.database.comp_level_sale_conservative_avm 
                    }
                    ];
                
                $scope.rentalMapInfo = {
                    center: new google.maps.LatLng($scope.database.latitude, $scope.database.longitude),
                    zoom: 14,
                    pins: $scope.rentalMapInfo.pins
                };

                $scope.saleMapInfo = {
                    center: new google.maps.LatLng($scope.database.latitude, $scope.database.longitude),
                    zoom: 14,
                    pins: $scope.rentalMapInfo.pins
                };
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
    }
    
  
      
    
    
    getCustomer();
    getRentalMapPins();
    getSaleMapPins();

    $scope.rentalMapProperties();

    $scope.simbaTab = function(tabname) {
        $scope[tabname] = true;
        if(tabname == 'rental')
        {
            $scope.renderSaleMap = false;
            $scope.renderRentalMap = true;
        }
        else if(tabname == 'sale')
        {
            $scope.renderRentalMap = false;
            $scope.renderSaleMap = true;
        }
    }

    $scope.stopMediaVideos = function(){
        angular.forEach($scope.propertyVideos, function(value, key) {

            var property_video = jQuery("#property_video_" + key).attr("src");
            jQuery("#property_video_" + key).attr("src","");
            jQuery("#property_video_" + key).attr("src", property_video);
        });
    }

    $scope.overviewMedia = function() {
        $scope.hasOverviewMedia = true;
        $scope.hasOverviewPhotos = false;
        $scope.hasoverviewMap = false;
        $scope.stopMediaVideos();
    }
    $scope.overviewPhotos = function() {
        $scope.hasOverviewPhotos = true;
        $scope.hasOverviewMedia = false;
        $scope.hasoverviewMap = false;
        $scope.stopMediaVideos();
    }

    $scope.maps = [];

    $scope.$on('mapInitialized', function(evt, evtMap) {
        $scope.maps.push(evtMap);
        angular.forEach($scope.maps, function(index) {
            google.maps.event.trigger(index, 'resize');
        });
    });

    $scope.overviewMap = function() {
        $scope.hasoverviewMap = true;
        $scope.renderOverviewMap = true;
        $scope.hasOverviewPhotos = false;
        $scope.hasOverviewMedia = false;
        $scope.stopMediaVideos();

        var _overviewPins = [];

        _overviewPins.push({ "id": "1","address": "3602 Hidalgo Dr, Dallas, TX, 75220","city": "Dallas", "lat": 32.871041, "lng": -96.858554});

        $scope.overviewMapInfo = {
            center: [32.871041, -96.858554],
            zoom: 15,
            pins: _overviewPins,
        };

        NgMap.getMap({id: 'overviewMap'}).then(function(map) {
            $scope.map = map;
            $scope.map.center = new google.maps.LatLng(32.871041, -96.858554);
            $scope.map.zoom = 15;
            $scope.map.lat = 32.871041;
            $scope.map.lng = -96.858554;
        });

        $timeout(function(){
            angular.forEach($scope.maps, function(index) {
                google.maps.event.trigger(index, 'resize');
            });
        }, 500);
    }

    $scope.showPropertyDetail = function(e, property, status) {

        $scope.mapPopup();

        /*$scope['simba'+ status + 'Property'] = property;
        $scope.map.showInfoWindow('simba' + status + 'InfoWindow', property.id);*/
    };

    $scope.mapPopup = function() {
        $scope.bootboxDialog = true;

        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/detailview/bootbox.html',
            size: 'large',
            closeButton: false,
            className: 'simbanic_opp_map_bootbox',
            scope: $scope
        });
    }

    $scope.hidePropertyDetail = function(e, property) {
        //$scope.map.hideInfoWindow('simbaInfoWindow');
    };
    
    // Always follow best practices
    // Property images object
    
    $scope.propertyImages = [
        {
            title: 'Greenlet 1',
            src: settings.siteURL + settings.imgPath + '/property/24396815_1.jpg?timestamp=' + new Date().getTime(),
            class: '',
        },
        {
            title: 'Greenlet 2',
            src: settings.imgPath + '/property/24396815_2.jpg',
            class: '',
        },
        {
            title: 'Greenlet 3',
            src: settings.imgPath + '/property/24396815_3.jpg',
            class: '',
        },
        {
            title: 'Greenlet 4',
            src: settings.imgPath + '/property/24396815_4.jpg',
            class: '',
        },
        {
            title: 'Greenlet 5',
            src: settings.imgPath + '/property/24396815_5.jpg',
            class: '',
        },
        {
            title: 'Greenlet 6',
            src: settings.imgPath + '/property/24396815_6.jpg',
            class: '',
        },
        {
            title: 'Greenlet 7',
            src: settings.imgPath + '/property/24396815_7.jpg',
            class: '',
        },
        {
            title: 'Greenlet 8',
            src: settings.imgPath + '/property/24396815_8.jpg',
            class: '',
        },
        {
            title: 'Greenlet 9',
            src: settings.imgPath + '/property/24396815_9.jpg',
            class: '',
        },
    ];


    $scope.propertyVideos = [
        {
            title: 'Greenlet 1',
            imgSrc: settings.siteURL + settings.imgPath + '/property/24396815_1.jpg',
            src: 'https://www.youtube.com/embed/BQdOsEw0HgM',
            videoId: 'BQdOsEw0HgM',
        },
        {
            title: 'Greenlet 2',
            imgSrc: settings.imgPath + '/property/24396815_7.jpg',
            src: 'https://www.youtube.com/embed/60e4k_AXGtU',
            videoId: '60e4k_AXGtU',
        },
        {
            title: 'Greenlet 1',
            imgSrc: settings.imgPath + '/property/24396815_9.jpg',
            src: 'https://www.youtube.com/embed/XFPLSUZBCB8',
            videoId: 'XFPLSUZBCB8',
        },
        {
            title: 'Greenlet 2',
            imgSrc: settings.imgPath + '/property/24396815_5.jpg',
            src: 'https://www.youtube.com/embed/cUwisuft34Q',
            videoId: 'cUwisuft34Q',
        },
        {
            title: 'Greenlet 1',
            imgSrc: settings.imgPath + '/property/24396815_3.jpg',
            src: 'https://www.youtube.com/embed/zb0rLKxXL8s',
            videoId: 'zb0rLKxXL8s',
        },
        {
            title: 'Greenlet 2',
            imgSrc: settings.imgPath + '/property/24396815_2.jpg',
            src: 'https://www.youtube.com/embed/twwbyMMvqtA',
            videoId: 'twwbyMMvqtA',
        }
    ];

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    // Marker Status Means Add or Remove, Status = Rental OR Sale
    $scope.markerIntoMap = function(markerStatus, status, address) {

        var searchValue = { "street_address": address };
        var display = false;

        if(markerStatus == 'add')
        {
            console.debug('add');
            display = true;
        }
        else if(markerStatus == 'remove')
        {
            console.debug('remove');
            display = false;
        }

        if(status == 'rental')
        {
            console.debug('rental');
        	// alert('removeing');
            var index = _.findIndex($scope.rentalMapInfo.pins, searchValue);
            // alert(JSON.stringify(searchValue));
            $scope.rentalMapInfo.pins[index]['display'] = display;
        }
        else if(status == 'sale')
        {
            console.debug('sale');
            var index = _.findIndex($scope.saleMapInfo.pins, searchValue);
            $scope.saleMapInfo.pins[index]['display'] = display;
        }

        if(markerStatus == 'add')
        {
            console.debug('marker add');
            jQuery("button[data-address='"+ address +"'].simba_add_map").removeClass('default');
            jQuery("button[data-address='"+ address +"'].simba_add_map").addClass('green_dark');
            
            jQuery("button[data-address='"+ address +"'].simba_remove_map").removeClass('red');
            jQuery("button[data-address='"+ address +"'].simba_remove_map").addClass('default');
        }
        else if(markerStatus == 'remove')
        {
            console.debug('marker remove');
            jQuery("button[data-address='"+ address +"'].simba_add_map").removeClass('green_dark');
            jQuery("button[data-address='"+ address +"'].simba_add_map").addClass('default');
            
            jQuery("button[data-address='"+ address +"'].simba_remove_map").removeClass('default');
            jQuery("button[data-address='"+ address +"'].simba_remove_map").addClass('red');
        }        
    }

    $('.simba_add_map').live('click', function () {
        
        $rootScope.simbaLoadingShow();

        var address = $(this).attr('data-address');
        var status = $(this).attr('data-status');

        $scope.markerIntoMap('add', status, address)

        setTimeout(function(){
            $rootScope.simbaLoadingHide();
            
            var bounds = new google.maps.LatLngBounds();
            // angular.forEach($scope.rentalMapInfo.pins, function(value, key) {
	            // // var latlng = new google.maps.LatLng(value.lat, value.lng);
	            // // bounds.extend(latlng);
	           // bounds.extend(value.getPosition());
	        // });
	        
	        angular.forEach($scope.rentalMapInfo.pins, function(value,key) {
			    // bounds.extend(new google.maps.LatLng(place.getLatitude(), place.getLongitude()));
			     // alert(JSON.stringify(value));
			     
			     var latlng = new google.maps.LatLng(value.latitude, value.longitude);
			     
			     if (value.display === true) {
	                bounds.extend(latlng);
	            }
			});
            
            NgMap.getMap({ id:'rentalMap' }).then(function(map) {
            	
            	// alert("map: " + JSON.stringify(map));
	            map.setCenter(bounds.getCenter());
	            map.fitBounds(bounds);
	            
	            // map.setZoom(map.getZoom()-1);
	             map.setZoom(map.getZoom()-1);

			    // set a minimum zoom
			    // if you got only 1 marker or all markers are on the same address map will be zoomed too much.
			    if(map.getZoom()> 15){
			        map.setZoom(15);
			    }
			    $scope.map = map;
			    
			    google.maps.event.trigger($scope.map, 'resize');
	            // google.maps.event.trigger($scope.map, 'resize');
	        });
            
        }, 6000);
        
    });

    $('.simba_remove_map').live('click', function () {
        // alert('remove');
        $rootScope.simbaLoadingShow();

        var address = $(this).attr('data-address');
        var status = $(this).attr('data-status');
		// alert('address: ' + address + ', ' + 'status: ' + status);
        $scope.markerIntoMap('remove', status, address)

        setTimeout(function(){
            $rootScope.simbaLoadingHide();
            
            
            
            
            
        }, 6000);
        
        
    });

    $('.simba_popup_map').live('click', function () {
        
        $scope.mapPopup();
    });

    $scope.opportunitiesVisit = function() {
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/detailview/visit.html?timestamp=' + new Date().getTime(),
            size: 'large',
            closeButton: false,
            className: 'simbanic_opportunities_visit_dialog',
            scope: $scope
        });
    }

    $scope.opportunitiesEvaluator = function() {
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/detailview/evaluator.html?timestamp=' + new Date().getTime(),
            size: 'large',
            closeButton: false,
            className: 'simbanic_opportunities_evaluator_dialog',
            scope: $scope
        });
    }

    $scope.opportunitiesFinance = function() {
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/detailview/finance.html?timestamp=' + new Date().getTime(),
            size: 'large',
            closeButton: false,
            className: 'simbanic_opportunities_finance_dialog',
            scope: $scope
        });
    }

    $scope.opportunitiesOffer = function() {
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/detailview/offer.html?timestamp=' + new Date().getTime(),
            size: 'large',
            closeButton: false,
            className: 'simbanic_opportunities_offer_dialog',
            scope: $scope
        });
    }

    $scope.rentalTrendsChart = {};
    $scope.rentalTrendsChart.type = "ScatterChart";
    $scope.rentalTrendsChart.data = 
    {
        "cols": 
        [
            {type: "number", label: "X"},
            {type: "number", label: "Sold"},
            {type: "number", label: "Active"},
        ],
        "rows": 
        [
            
        ]
    };

    $scope.rentalTrendsChart.options = {
        title: '',
        //titleTextStyle: { fontSize: '70px', fontName: 'Open Sans'},
        legend: {
            position: "top",
            alignment: "end"
        },
        colors: ['blue', 'red'],
        chartArea: {
            top: 30,
            left: 70,
            bottom: 40,
            width:"100%",
            height: "100%"
        },
        titleX: 'Distance', 
        titleY: 'Price',
        vAxes: {
            ticks: [0,1300,1400,1500,1600,1700,1800,1900,2000],
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            //title: 'Distance',
            ticks: [0,.25,.50,.75,1,1.25,1.5],
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        
    };
    
    
    $scope.saleTrendsChart = {};
    $scope.saleTrendsChart.type = "ScatterChart";
    $scope.saleTrendsChart.data = 
    {
        "cols": 
        [
            {type: "number", label: "X"},
            {type: "number", label: "Sold"},
            {type: "number", label: "Active"},
        ],
        "rows": 
        [
            
        ]
    };

    $scope.saleTrendsChart.options = {
        title: '',
        //titleTextStyle: { fontSize: '70px', fontName: 'Open Sans'},
        legend: {
            position: "top",
            alignment: "end"
        },
        colors: ['blue', 'red'],
        chartArea: {
            top: 30,
            left: 70,
            bottom: 40,
            width:"100%",
            height: "100%"
        },
        titleX: 'Distance', 
        titleY: 'Price',
        vAxes: {
            ticks: [0,1300,1400,1500,1600,1700,1800,1900,2000],
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            //title: 'Distance',
            ticks: [0,.25,.50,.75,1,1.25,1.5],
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        
    };

    $scope.gotoTrends = function(tabName) {
        $scope.compsRentalTrends = false;
        $scope.compsSaleTrends = false;
        if(tabName == "rental")
        {
            $scope.compsRentalTrends = true;
        }
        else if(tabName == "sale")
        {
            $scope.compsSaleTrends = true;
        }
    }

});

angular.module('SimbaNicApp').controller('overviewMapController', function($scope, NgMap) {
    
    NgMap.getMap({ id:'overviewMap' }).then(function(map) {
        $scope.map = map;
        $scope.map.center = [32.871041, -96.858554];
        $scope.map.zoom = 15;
        google.maps.event.trigger($scope.map, 'resize');
    });
});

angular.module('SimbaNicApp').controller('saleMapController', function($scope, NgMap) {

    NgMap.getMap({ id:'saleMap' }).then(function(map) {
        $scope.map = map;
        google.maps.event.trigger($scope.map, 'resize');
    });
});

angular.module('SimbaNicApp').controller('rentalMapController', function($scope, NgMap) {

    NgMap.getMap({ id:'rentalMap' }).then(function(map) {
        $scope.map = map;
    });
});

SimbaNicApp.controller('TabController', ['$scope', function($scope) {
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
}]);

SimbaNicApp.controller('opportunitiesVisitController', function($rootScope, $scope, $injector, $ngBootbox) {

    $scope.visitForm = {};

    var $validationProvider = $injector.get('$validation');
            
    angular.extend($validationProvider, {
        validCallback: function (element) {
            $(element).parents('.form-group:first').removeClass('has-error');
        },
        invalidCallback: function (element) {
            $(element).parents('.form-group:first').addClass('has-error');
        }
    });

    $scope.save = function() {

        if(!$validationProvider.checkValid)
        {
            return false;
        }

        if (typeof($scope.visitForm.earliest_time) === 'undefined') return false;
        if (typeof($scope.visitForm.latest_time) === 'undefined') return false;
        var earliest_time = $scope.convertAmPmTo24Hour($scope.visitForm.earliest_time);
        var latest_time = $scope.convertAmPmTo24Hour($scope.visitForm.latest_time);

        if(latest_time < earliest_time)
        {
            $scope.earliestTimeError = true;
            return false;
        }
        else
        {
            $scope.earliestTimeError = false;
        }
        
        $ngBootbox.hideAll();
    }

    $scope.latestTimeChange = function() {

        if($scope.visitForm.latest_time)
        {
            if (typeof($scope.visitForm.earliest_time) === 'undefined') return false;
            if (typeof($scope.visitForm.latest_time) === 'undefined') return false;
            var earliest_time = $scope.convertAmPmTo24Hour($scope.visitForm.earliest_time);
            var latest_time = $scope.convertAmPmTo24Hour($scope.visitForm.latest_time);
            if(latest_time < earliest_time)
            {
                $scope.earliestTimeError = true;
                return false;
            }
            else
            {
                $scope.earliestTimeError = false;
            }
        }
    };

    $scope.convertAmPmTo24Hour = function(time_value) {

        var time = time_value;
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if(AMPM == "PM" && hours<12) hours = hours+12;
        if(AMPM == "AM" && hours==12) hours = hours-12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if(hours<10) sHours = "0" + sHours;
        if(minutes<10) sMinutes = "0" + sMinutes;
        return sHours + ":" + sMinutes;
    }

});

SimbaNicApp.controller('opportunitiesEvaluatorController', function($rootScope, $scope, $injector, $ngBootbox) {

    $scope.evaluatorForm = {};

    var $validationProvider = $injector.get('$validation');
            
    angular.extend($validationProvider, {
        validCallback: function (element){
            $(element).parents('.form-group:first').removeClass('has-error');
        },
        invalidCallback: function (element) {
            $(element).parents('.form-group:first').addClass('has-error');
        }
    });

    $scope.save = function() {

        if(!$validationProvider.checkValid)
        {
            return false;
        }
        
        $ngBootbox.hideAll();

    }

});

SimbaNicApp.controller('opportunitiesFinancialController', function($rootScope, $scope, $injector, $ngBootbox, $filter, $anchorScroll, $location, $window) {

    
    $scope.finance = [];
    $scope.finance.offerForm = {};
    $scope.finance.offerForm.rent_avm = "";
    $scope.finance.offerForm.rehab_grade = "A";
    $scope.finance.offerForm.mortgage_type = "";
    $scope.finance.offerForm.financing = "no";
    $scope.finance.offerForm.sale_avm = "";
    $scope.finance.offerForm.terms_yrs = "";

});

SimbaNicApp.controller('opportunitiesOfferController', function($rootScope, $scope, $injector, $ngBootbox, $filter, $anchorScroll, $location, $window) {

    $scope.make_an_offer = true;
    $scope.review_an_offer = false;
    $scope.offerForm = {};
       var closeDate = new Date();
    var offerExpiryDate = new Date();
    var close_date = closeDate.addDays(20);
    var offer_expiry_date = offerExpiryDate.addDays(30);
    
    close_date = $filter('date')(close_date, "MM/dd/yyyy");
    offer_expiry_date = $filter('date')(offer_expiry_date, "MM/dd/yyyy");
    
    //$scope.offerForm.close_date = close_date;
    //$scope.offerForm.offer_expires = offer_expiry_date;
    $scope.offerForm.option_period = "5";
    $scope.offerForm.terms_yrs = "";
    $scope.offerForm.loan_type = "";
    $scope.offerForm.buyer_name = "Test Buyer Name";
    $scope.offerForm.escrow_company = "Test Escrow Company";

     $scope.offerForm.survey = "no";
    $scope.offerForm.option_inspect_period = "yes";
    $scope.offerForm.title_payer = "seller";
    $scope.offerForm.closing_costs = "no";
    $scope.offerForm.lease_back = "no";
    $scope.offerForm.financing = "no";
    $scope.offerForm.loan_type = "conventional";
    $scope.offerForm.terms_yrs = "5";
    $scope.offerForm.special_terms = "no";
    $scope.offerForm.negotiation_notes = "no";
    $scope.offerForm.appraisal = "no";
    $scope.displayNewBuyer = false;
    $scope.displayNewEscrowCompany = false;

    $scope.changeOfferPrice = function() {
        $scope.offerForm.earnest = $scope.offerForm.offer_price ? (($scope.offerForm.offer_price/100)) : '';
    }

    $scope.surveys = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showSurvey = function() {
        var selected = $filter('filter')($scope.surveys, { value: $scope.offerForm.survey });
        return ($scope.offerForm.survey && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.option_inspect_periods = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showOptionInspectPeriod = function() {
        var selected = $filter('filter')($scope.option_inspect_periods, { value: $scope.offerForm.option_inspect_period });
        return ($scope.offerForm.option_inspect_period && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.option_periods = [
        {value: 1, text: 1},
        {value: 2, text: 2},
        {value: 3, text: 3},
        {value: 4, text: 4},
        {value: 5, text: 5},
        {value: 6, text: 6},
        {value: 7, text: 7},
        {value: 8, text: 8},
        {value: 9, text: 9},
        {value: 10, text: 10},
        {value: 11, text: 11},
        {value: 12, text: 12},
        {value: 13, text: 13},
        {value: 14, text: 14},
        {value: 15, text: 15},
    ];
    $scope.showOptionPeriod = function() {
        var selected = $filter('filter')($scope.option_periods, { value: $scope.offerForm.option_period });
        return ($scope.offerForm.option_period && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.title_payers = [
        {value: 'seller', text: 'Seller'},
        {value: 'buyer', text: 'Buyer'},
        {value: 'split', text: 'Split'}
    ];
    $scope.showTitlePayer = function() {
        var selected = $filter('filter')($scope.title_payers, { value: $scope.offerForm.title_payer });
        return ($scope.offerForm.title_payer && selected.length) ? selected[0].text : 'Not set';
    };


    $scope.closing_costses = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showClosingCost = function() {
        var selected = $filter('filter')($scope.closing_costses, { value: $scope.offerForm.closing_costs });
        return ($scope.offerForm.closing_costs && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.lease_backs = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showLeaseBack = function() {
        var selected = $filter('filter')($scope.lease_backs, { value: $scope.offerForm.lease_back });
        return ($scope.offerForm.lease_back && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.financings = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showFinancing = function() {
        var selected = $filter('filter')($scope.financings, { value: $scope.offerForm.financing });
        return ($scope.offerForm.financing && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.loan_types = [
        {value: 'conventional', text: 'Conventional'},
        {value: 'fha', text: 'FHA'},
        {value: 'va', text: 'VA'},
        {value: 'hard_money', text: 'Hard Money'},
        {value: 'owner_finance_request', text: 'Owner Finance Request'},
    ];
    $scope.showLoanType = function() {
        var selected = $filter('filter')($scope.loan_types, { value: $scope.offerForm.loan_type });
        return ($scope.offerForm.loan_type && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.appraisals = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showAppraisal = function() {
        var selected = $filter('filter')($scope.appraisals, { value: $scope.offerForm.appraisal });
        return ($scope.offerForm.appraisal && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.special_termses = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showSpecialTerms = function() {
        var selected = $filter('filter')($scope.special_termses, { value: $scope.offerForm.special_terms });
        return ($scope.offerForm.special_terms && selected.length) ? selected[0].text : 'Not set';
    };

    $scope.negotiation_noteses = [
        {value: 'yes', text: 'Yes'},
        {value: 'no', text: 'No'}
    ];
    $scope.showNegotiationNotes = function() {
        var selected = $filter('filter')($scope.negotiation_noteses, { value: $scope.offerForm.negotiation_notes });
        return ($scope.offerForm.negotiation_notes && selected.length) ? selected[0].text : 'Not set';
    };

    var $validationProvider = $injector.get('$validation');
            
    angular.extend($validationProvider, {
        validCallback: function (element) {
            $(element).parents('.form-group:first').removeClass('has-error');
        },
        invalidCallback: function (element) {
            $(element).parents('.form-group:first').addClass('has-error');
        }
    });

    $scope.save = function() {

        if(!$validationProvider.checkValid)
        {
            return false;
        }

         if(jQuery('#opportunityOfferForm').valid())
        {
            $scope.make_an_offer = false;
            $scope.review_an_offer = true;
            $('.modal').animate({scrollTop:0}, 500, 'swing');
        }



        
        
        //$ngBootbox.hideAll();
    }

    $scope.backMakeOffer = function() {
        $scope.make_an_offer = true;
        $scope.review_an_offer = false;
    }

    $scope.checkEditableCurrency = function(data) {
        var regex  = /^\d+(?:\.\d{0,2})$/;
        if(!isNaN(parseFloat(data)) && isFinite(data))
        {
            
        }
        else
        {
            return "Please specify a valid value";
        }
    }

    $scope.checkEditableText = function(data) {
        if (!data) {
            return "Required Field";
        }
    }

    $scope.toggleNewBuyer = function() {
        $scope.displayNewBuyer = !$scope.displayNewBuyer;
    }

    $scope.toggleNewEscrowCompany = function() {
        $scope.displayNewEscrowCompany = !$scope.displayNewEscrowCompany;
    }


});

SimbaNicApp.controller('opportunitiesNeighborhoodController', function($rootScope, $scope) {

    // Age
    $scope.neighborhoodAge = {};
    $scope.neighborhoodAge.type = "PieChart";
    $scope.neighborhoodAge.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Under 5"},
                    {v: 42.8},
                ]
            },
            {
                c: [
                    {v: "18-29"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "45-64"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "5-17"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "30-44"},
                    {v: 24}
                ]
            },
            {
                c: [
                    {v: "65+"},
                    {v: 8.3},
                ]
            },
            
            
        ]
    };
    $scope.neighborhoodAge.options = {
        //pieHole: 0.4,
        
        chartArea: {
            top: 10,
            width:"100%", 
            height: "90%"
        },
        legend: {
            position: "none",
        },
        colors: ['#3266CC', '#DC3812', '#FE9900', '#109619', '#990099', '#FF00DD']
    }
    console.log($scope.neighborhoodAge);

    // Education
    $scope.neighborhoodEducationHighSchool = {};
    $scope.neighborhoodEducationHighSchool.type = "BarChart";
    $scope.neighborhoodEducationHighSchool.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"},
            {role: "annotation", type: "string"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Here"},
                    {v: 89.2},
                    {v: '#4286F5'},
                    {v: "89.2%"},
                ]
            },
            {
                c: [
                    {v: "USA"},
                    {v: 78.4},
                    {v: '#DC4439'},
                    {v: "78.4%"},
                ]
            },
        ]
    };
    $scope.neighborhoodEducationHighSchool.options = {
        'title': 'High School Degree',
        titleTextStyle: { fontSize: '50px', fontName: 'Open Sans'},
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"80%", 
            height: "50%"
        },
        annotations: {
            alwaysOutside: true,
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [1,30,70,100]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };


    $scope.neighborhoodEducationBachelors = {};   
    $scope.neighborhoodEducationBachelors.type = "BarChart";
    $scope.neighborhoodEducationBachelors.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"},
            {role: "annotation", type: "string"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Here"},
                    {v: 89.2},
                    {v: '#4286F5'},
                    {v: "89.2%"},
                ]
            },
            {
                c: [
                    {v: "USA"},
                    {v: 78.4},
                    {v: '#DC4439'},
                    {v: "78.4%"},
                ]
            },
        ]
    };
    $scope.neighborhoodEducationBachelors.options = {
        title: 'Bachelors Degree',
        titleTextStyle: { fontSize: '50px', fontName: 'Open Sans'},
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"80%", 
            height: "50%"
        },
        annotations: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [1,30,70,100]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };

    $scope.neighborhoodEducationAdvancedCollege = {};
    $scope.neighborhoodEducationAdvancedCollege.type = "BarChart";
    $scope.neighborhoodEducationAdvancedCollege.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"},
            {role: "annotation", type: "string"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Here"},
                    {v: 89.2},
                    {v: '#4286F5'},
                    {v: "89.2%"},
                ]
            },
            {
                c: [
                    {v: "USA"},
                    {v: 78.4},
                    {v: '#DC4439'},
                    {v: "78.4%"},
                ]
            },
        ]
    };
    $scope.neighborhoodEducationAdvancedCollege.options = {
        title: 'Advanced College Degree',
        titleTextStyle: { 
            fontSize: '70px', 
            fontName: 'Open Sans',
        },
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"80%", 
            height: "50%"
        },
        annotations: {
            alwaysOutside: true,
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [1,30,70,100]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };


    // Marital
    $scope.neighborhoodMarital = {};
    $scope.neighborhoodMarital.type = "PieChart";
    $scope.neighborhoodMarital.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Single"},
                    {v: 45.8},
                ]
            },
            {
                c: [
                    {v: "Divorced"},
                    {v: 16.6},
                ]
            },
            {
                c: [
                    {v: "Married"},
                    {v: 8.3},
                ]
            },
            
            {
                c: [
                    {v: "Windowed"},
                    {v: 29.2},
                ]
            },
        ]
    };
    $scope.neighborhoodMarital.options = {
        pieHole: 0.4,
        legend: {
            position: "none"
        },
        chartArea: {
            top: 10,
            width:"100%", 
            height: "90%"
        },
        colors: ['#3266CC', '#FE9900', '#109619', '#990099'],
    }

    // Income
    $scope.neighborhoodPerCapita = {};
    $scope.neighborhoodPerCapita.type = "BarChart";
    $scope.neighborhoodPerCapita.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"},
            {role: "annotation", type: "string"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Here"},
                    {v: 46224},
                    {v: '#4286F5'},
                    {v: "$46,224"}
                ]
            },
            {
                c: [
                    {v: "USA"},
                    {v: 28155},
                    {v: '#DC4439'},
                    {v: "$28,155"}
                ]
            },
        ]
    };
    $scope.neighborhoodPerCapita.options = {
        'title': 'Per Capita Income',
        titleTextStyle: { fontSize: '50px', fontName: 'Open Sans'},
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"80%", 
            height: "50%"
        },
        annotations: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [10000,20000,30000,40000, 50000]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };

    //
    $scope.neighborhoodMedianHousehold = {};
    $scope.neighborhoodMedianHousehold.type = "BarChart";
    $scope.neighborhoodMedianHousehold.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"},
            {role: "annotation", type: "string"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Here"},
                    {v: 82170},
                    {v: '#4286F5'},
                    {v: "$82,170"}
                ]
            },
            {
                c: [
                    {v: "USA"},
                    {v: 53046},
                    {v: '#DC4439'},
                    {v: "$53,046"}
                ]
            },
        ]
    };
    $scope.neighborhoodMedianHousehold.options = {
        'title': 'Median Household Income',
        titleTextStyle: { fontSize: '50px', fontName: 'Open Sans'},
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"80%", 
            height: "50%"
        },
        annotations: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [30000, 50000, 70000, 90000]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };

    // Occupation
    $scope.neighborhoodOccupation = {};
    $scope.neighborhoodOccupation.type = "PieChart";
    $scope.neighborhoodOccupation.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Professionals"},
                    {v: 45.8},
                ]
            },
            {
                c: [
                    {v: "Office Support"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "Farmers"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "Sales & Service"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "Factory & Laborers"},
                    {v: 29.2},
                ]
            },
        ]
    };
    $scope.neighborhoodOccupation.options = {
        pieHole: 0.4,
        legend: { 
            position: 'none' 
        },
        chartArea: {
            top: 10,
            width:"100%", 
            height: "90%"
        },
        colors: ["#3266CC", "#DC3812", "#FE9900", "#119619", "#990099"]
    }


    // UnEmployment
    $scope.neighborhoodUnEmployment = {
        "type": "LineChart",
        "displayed": false,
        "data": {
            "cols": [
                {
                    "id": "month",
                    "label": "Month",
                    "type": "string",
                    "role": "domain"
                },
                {
                    "id": "here-id",
                    "label": "Here",
                    "type": "number"
                },
                {
                    "role": "annotation",
                    "type": "string"
                },
                {
                    "id": "usa-id",
                    "label": "USA",
                    "type": "number"
                },
                {
                    "role": "annotation",
                    "type": "string"
                }
            ],
            "rows": [
                {
                    "c": [
                        {
                            "v": "JAN"
                        },
                        {
                            "v": 6.5,
                        },
                        {},
                        {
                            "v": 3.5,
                        },
                    ]
                },
                {
                    "c": [
                        {
                            "v": "FEB"
                        },
                        {
                            "v": 7.6,
                        },
                        {},
                        {
                            "v": 3.9,
                        },
                    ]
                },
                {
                    "c": [
                        {
                            "v": "MAR"
                        },
                        {
                            "v": 5.4,
                        },
                        {},
                        {
                            "v": 5.8,
                        },
                    ]
                },
                {
                    "c": [
                        {
                            "v": "APR"
                        },
                        {
                            "v": 4,
                        },
                        {},
                        {
                            "v": 6.5,
                        },
                    ]
                },
                {
                    "c": [
                        {
                            "v": "MAY"
                        },
                        {
                            "v": 5,
                        },
                        {},
                        {
                            "v": 5.5,
                        },
                    ]
                },
                {
                    "c": [
                        {
                            "v": "JUN"
                        },
                        {
                            "v": 6,
                        },
                        {
                            "v": "4.2%",
                        },
                        {
                            "v": 4,
                        },
                        {
                            "v": "5.1%",
                        },
                    ]
                },
            ]
        },
        "options": {
            "title": "",
            "isStacked": "true",
            "fill": 20,
            "displayExactValues": true,
            "legend": { 
                position: 'bottom' 
            },
            curveType: 'function',
            annotations: {
                textStyle: {
                    fontName: 'Open Sans',
                    fontSize: 14,
                },
                
            },
            chartArea: {
                top: 20,
                width:"90%", 
                height: "75%"
            },
            vAxis: {
                ticks: [1, 3, 5, 7, 9]
            },

        },
        "formatters": {},
        "view": {}
    }


    // HomeOwnerShip
    $scope.neighborhoodHomeOwnerShip = {};
    $scope.neighborhoodHomeOwnerShip.type = "PieChart";
    $scope.neighborhoodHomeOwnerShip.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Owners"},
                    {v: 45.8},
                ]
            },
            {
                c: [
                    {v: "Seasonal"},
                    {v: 16.6},
                ]
            },
            {
                c: [
                    {v: "Renters"},
                    {v: 8.3},
                ]
            },
            
            {
                c: [
                    {v: "Vacant"},
                    {v: 29.2},
                ]
            },
        ]
    };
    $scope.neighborhoodHomeOwnerShip.options = {
        pieHole: 0.4,
        legend: {
            position: "none"
        },
        chartArea: {
            top: 10,
            width:"100%", 
            height: "90%"
        },
        colors: ["#3266CC", "#FE9900", "#139519", "#990099"]
    }

    // Commute
    $scope.neighborhoodCommute = {};
    $scope.neighborhoodCommute.type = "BarChart";
    $scope.neighborhoodCommute.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"},
            {role: "annotation", type: "string"},
        ], 
        "rows": 
        [
            {
                c: [
                    {v: ">15 min"},
                    {v: 34.9},
                    {v: '#109619'},
                    {v: "34.9%"}
                ]
            },
            {
                c: [
                    {v: "15-30 min"},
                    {v: 23.7},
                    {v: '#109619'},
                    {v: "23.7%"}
                ]
            },
            {
                c: [
                    {v: "30-45 min"},
                    {v: 25.3},
                    {v: '#109619'},
                    {v: "25.3%"}
                ]
            },
            {
                c: [
                    {v: "45-60 min"},
                    {v: 13.7},
                    {v: '#109619'},
                    {v: "13.7%"}
                ]
            },
            {
                c: [
                    {v: "60 min+"},
                    {v: 2.3},
                    {v: '#109619'},
                    {v: "2.3%"}
                ]
            },
        ]
    };
    $scope.neighborhoodCommute.options = {
        'title': '',
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"60%", 
            height: "50%"
        },
        annotations: {
            alwaysOutside: true,
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [0,10,20,30,40]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };

    // HouseHold
    $scope.neighborhoodHouseHold = {};
    $scope.neighborhoodHouseHold.type = "PieChart";
    $scope.neighborhoodHouseHold.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "One Person"},
                    {v: 45.8},
                ]
            },
            {
                c: [
                    {v: "Married"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "Sam Sex"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "Married with Child"},
                    {v: 8.3},
                ]
            },
            {
                c: [
                    {v: "Single Parent"},
                    {v: 29.2},
                ]
            },
        ]
    };
    $scope.neighborhoodHouseHold.options = {
        legend: {
            position: "none",
        },
        chartArea: {
            top: 10,
            width:"100%", 
            height: "90%"
        },
        colors: ["#3266CC", "#DB3912", "#FE9900", "#0D9912", "#990099"]
    }

    // Gender
    $scope.neighborhoodGender = {};
    $scope.neighborhoodGender.type = "PieChart";
    $scope.neighborhoodGender.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "style", type: "string"}
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Male"},
                    {v: 45.8},
                    {v:'color: #000000'}
                ]
            },
            {
                c: [
                    {v: "Female"},
                    {v: 54.2},
                    {v:'color: #000000'}
                ]
            },
        ]
    };
    $scope.neighborhoodGender.options = {
        pieHole: 0.4,
        legend: {
            position: "bottom"
        },
        chartArea: {
            top: 10,
            width:"100%", 
            height: "80%"
        },
        colors: ['#3266CC', '#990099']
    }

    // Migration
    $scope.neighborhoodMigration = {};
    $scope.neighborhoodMigration.type = "BarChart";
    $scope.neighborhoodMigration.data = 
    {
        "cols": 
        [
            {type: "string"},
            {type: "number"},
            {role: "annotation", type: "string"},
        ], 
        "rows": 
        [
            {
                c: [
                    {v: "Moved Last Year"},
                    {v: 21.3},
                    {v: "21.3%"},
                ]
            },
            {
                c: [
                    {v: "Born Out of State"},
                    {v: 71.5},
                    {v: "71.5%"},
                ]
            },
            {
                c: [
                    {v: "Foreign Born"},
                    {v: 14.1},
                    {v: "14.1%"},
                ]
            },
        ]
    };
    $scope.neighborhoodMigration.options = {
        'title': '',
        legend: {
            position: "none"
        },
        chartArea: {
            top: 30,
            width:"60%", 
            height: "50%"
        },
        annotations: {
            alwaysOutside: true,
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        },
        hAxis: {
            ticks: [0,20,40,60,80,100]
        },
        vAxes: {
            textStyle: {
                fontName: 'Open Sans',
                fontSize: 14,
            }
        }
    };

});

SimbaNicApp.controller('opportunitiesRehabEstimatorController', function($rootScope, $scope) {

    $scope.rehabExteriorEstimator = [
        {
            id: 1,
            name: "Change Locks",
            qty: 0,
            rate: 300,
            total: 0
        },
        {
            id: 2,
            name: "Trash removal",
            qty: 0,
            rate: 500,
            total: 0
        },
        {
            id: 3,
            name: "Yard Cleanup",
            qty: 0,
            rate: 150,
            total: 0
        },
        {
            id: 4,
            name: "Lanscape-Planting",
            qty: 0,
            rate: 400,
            total: 0
        },
        {
            id: 5,
            name: "Tree Trimming",
            qty: 0,
            rate: 250,
            total: 0
        },
        {
            id: 6,
            name: "Roof",
            qty: 0,
            rate: 2500,
            total: 0
        },
        {
            id: 7,
            name: "Stucco & Brick Repair",
            qty: 0,
            rate: 1000,
            total: 0
        },
        {
            id: 8,
            name: "Pool Renovation",
            qty: 0,
            rate: 1500,
            total: 0
        },
        {
            id: 9,
            name: "Exterior Painting(sqft)",
            qty: 0,
            rate: 1,
            total: 0
        },
        {
            id: 10,
            qty: 0,
            name: "",
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'

        },
        {
            id: 11,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        }
    ];

    $scope.getExteriorRowTotal = function(index) {
        $scope.rehabExteriorEstimator[index]['total'] = $scope.rehabExteriorEstimator[index]['qty'] * $scope.rehabExteriorEstimator[index]['rate'];
    }

    $scope.getExteriorTotal = function() {
        var total = 0;
        for(var i = 0; i < $scope.rehabExteriorEstimator.length; i++)
        {
            if($scope.rehabExteriorEstimator[i]['total'])
            {
                total += parseFloat($scope.rehabExteriorEstimator[i]['total']);    
            }
        }
        return total;
    }


    $scope.rehabInteriorEstimator = [
        {
            id: 1,
            name: "Finish Cleaning",
            qty: 0,
            rate: 300,
            total: 0
        },
        {
            id: 2,
            name: "Drywall Repair(wall)",
            qty: 0,
            rate: 100,
            total: 0
        },
        {
            id: 3,
            name: "Painiting Interior(sqft)",
            qty: 0,
            rate: 1.50,
            total: 0
        },
        {
            id: 4,
            name: "Fan & Light Fixtures",
            qty: 0,
            rate: 75,
            total: 0,
        },
        {
            id: 5,
            name: "Replace Outlets and Switches",
            qty: 0,
            rate: 450,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 6,
            name: "Carpet Replacement(sqft)",
            qty: 0,
            rate: 1.40,
            total: 0
        },
        {
            id: 7,
            name: "Floors (sqft)",
            qty: 0,
            rate: 5,
            total: 0
        },
        {
            id: 8,
            name: "Kitchen Remodel",
            qty: 0,
            rate: 3500,
            total: 0
        },
        {
            id: 9,
            name: "Bathroom 1 Remodel",
            qty: 0,
            rate: 500,
            total: 0
        },
        {
            id: 10,
            name: "Bathroom 2 Remodel",
            qty: 0,
            rate: 500,
            total: 0
        },
        {
            id: 11,
            name: "Bathroom 3 Remodel",
            qty: 0,
            rate: 500,
            total: 0
        },
        {
            id: 12,
            name: "Garage Floor Paint",
            qty: 0,
            rate: 750,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 13,
            qty: 0,
            name: "",
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        },
        {
            id: 14,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        }
    ];

    $scope.getInteriorRowTotal = function(index) {

        $scope.rehabInteriorEstimator[index]['total'] = $scope.rehabInteriorEstimator[index]['qty'] * $scope.rehabInteriorEstimator[index]['rate'];
        
    }

    $scope.getInteriorTotal = function() {
        var total = 0;
        for(var i = 0; i < $scope.rehabInteriorEstimator.length; i++)
        {
            if($scope.rehabInteriorEstimator[i]['total'])
            {
                total += parseFloat($scope.rehabInteriorEstimator[i]['total']);    
            }
        }
        return total;
    }

    $scope.rehabAppliancesEstimator = [
        {
            id: 1,
            name: "Refrigerator",
            qty: 0,
            rate: 1000,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 2,
            name: "Dishwasher",
            qty: 0,
            rate: 425,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 3,
            name: "Microwave",
            qty: 0,
            rate: 250,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 4,
            name: "Garage Door",
            qty: 0,
            rate: 750,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 5,
            name: "Stove/Range",
            qty: 0,
            rate: 650,
            total: 0,
            qty_type: 'checkbox',
        },
        {
            id: 6,
            name: "Blinds",
            qty: 0,
            rate: 75,
            total: 0
        },
        {
            id: 7,
            name: "Screens",
            qty: 0,
            rate: 100,
            total: 0
        },
        {
            id: 8,
            name: "Garage Door Opener",
            qty: 0,
            rate: 125,
            total: 0
        },
        {
            id: 9,
            name: "HVAC Replace",
            qty: 0,
            rate: 3000,
            total: 0,
            qty_type: 'checkbox'
        },
        {
            id: 10,
            name: "HVAC Service",
            qty: 0,
            rate: 250,
            total: 0
        },
        {
            id: 11,
            name: "Water Heater",
            qty: 0,
            rate: 500,
            total: 0
        },
        {
            id: 12,
            qty: 0,
            name: "",
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        },
        {
            id: 13,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        }
    ];

    $scope.getAppliancesRowTotal = function(index) {
        $scope.rehabAppliancesEstimator[index]['total'] = $scope.rehabAppliancesEstimator[index]['qty'] * $scope.rehabAppliancesEstimator[index]['rate'];
    }

    $scope.getAppliancesTotal = function() {
        var total = 0;
        for(var i = 0; i < $scope.rehabAppliancesEstimator.length; i++)
        {
            if($scope.rehabAppliancesEstimator[i]['total'])
            {
                total += parseFloat($scope.rehabAppliancesEstimator[i]['total']);    
            }
            
        }
        return total;
    }

    $scope.rehabOtherEstimator = [
        {
            id: 1,
            qty: 0,
            name: "",
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        },
        {
            id: 2,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        },
        {
            id: 3,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        },
        {
            id: 4,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        },
        {
            id: 5,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        }


    ];

    $scope.getOtherRowTotal = function(index) {
        $scope.rehabOtherEstimator[index]['total'] = $scope.rehabOtherEstimator[index]['qty'] * $scope.rehabOtherEstimator[index]['rate'];
    }

    $scope.getOtherTotal = function() {
        var total = 0;
        for(var i = 0; i < $scope.rehabOtherEstimator.length; i++)
        {
            if($scope.rehabOtherEstimator[i]['total'] != 0)
            {
                if($scope.rehabOtherEstimator[i]['total'])
                {
                    total += parseFloat($scope.rehabOtherEstimator[i]['total']);    
                }
            }
        }
        return total;
    }

    $scope.addOtherRow = function() {
        var next_id = $scope.rehabOtherEstimator.length + 1;
        $scope.rehabOtherEstimator.push({
            id: next_id,
            name: "",
            qty: 0,
            rate: 0,
            total: 0,
            qty_input: false,
            name_type: 'text'
        });
    }

});