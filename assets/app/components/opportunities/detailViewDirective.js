SimbaNicApp.directive('ngDetailviewOpportunity', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            jQuery('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        }
    }
});

SimbaNicApp.directive('dataCounter', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.counterUp({
                delay: 10,
                time: 1000
            });
        }
    }
});

SimbaNicApp.directive('simbaSlickFor', function($timeout, settings, $location) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            $timeout((function() {
                elem.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    adaptiveHeight: true,
                    asNavFor: '.simba-slick-nav'
                });
                elem.slickLightbox({
                    src: 'src',
                    itemSelector: '.simba_property_image img'
                });
                elem[0].slick.refresh();
            }), 100);
        }
    }
});

SimbaNicApp.directive('simbaSlickNav', function($timeout) {
    return {
        restrict: 'C',
        link: function(scope, elem, attrs) {
            $timeout((function() {
                elem.slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    asNavFor: '.simba-slick-for',
                    dots: true,
                    //centerMode: true,
                    focusOnSelect: true,
                    responsive: [
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                arrows: true,
                                dots: false
                            }
                        },
                    ],
                    prevArrow: '<button class="btn simba-slick-prev" type="button"><i class="fa fa-chevron-left font-hg"></i></button>',
                    nextArrow: '<button class="btn simba-slick-next" type="button"><i class="fa fa-chevron-right font-hg"></i></button>'
                });
                elem[0].slick.refresh();
            }), 100);
        }
    }
});

SimbaNicApp.directive('simbaVideoSliderFor', function($timeout) {
    return {
        restrict: 'C',
        link: function(scope, elem, attrs) {
            $timeout((function() {

                elem.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    adaptiveHeight: true,
                    asNavFor: '.simba-video-slider-nav'
                });
            }), 100);
        }
    }
});

SimbaNicApp.directive('simbaVideoSliderNav', function($timeout, $rootScope) {
    return {
        restrict: 'C',
        link: function(scope, elem, attrs) {
            $timeout((function() {
                elem.slick({
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    asNavFor: '.simba-video-slider-for',
                    dots: true,
                    arrows: true,
                    //centerMode: true,
                    focusOnSelect: true,
                    prevArrow: '<button class="btn simba-slick-prev" type="button"><i class="fa fa-chevron-left font-hg"></i></button>',
                    nextArrow: '<button class="btn simba-slick-next" type="button"><i class="fa fa-chevron-right font-hg"></i></button>'
                });
                
            }), 100);

            elem.on('beforeChange', function(event, slick, currentSlide, nextSlide){

                var property_video = jQuery("#property_video_" + currentSlide).attr("src");
                jQuery("#property_video_" + currentSlide).attr("src","");
                jQuery("#property_video_" + currentSlide).attr("src", property_video);

            });
        }
    }
});

SimbaNicApp.directive('simbaPopupSlick', function($timeout, settings) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            $timeout((function() {
                elem.slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    fade: true,
                    adaptiveHeight: true,
                });
                jQuery('.simba_opportunity_map_bootbox .slick-next').trigger('click');
                
            }), 100);
        }
    }
});

// SimbaNicApp.directive('closeDatePicker', function($timeout, $parse){
SimbaNicApp.directive('offerExpiresDatePicker', function($timeout, $parse, $filter){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            var offerExpiryDate = new Date();
           //var offer_expiry_date = offerExpiryDate.addDays(30);
           var offer_expiry_date = new Date($filter('addWeekdays')(moment(), 30));

            elem.datepicker({
                orientation: "right",
                autoclose: true,
                 todayHighlight: true,
                setDate: offer_expiry_date,
                startDate: new Date(),
                //endDate: new Date(new Date().getTime()+(30*24*60*60*1000)), // 30 Days
            });
            
            elem.datepicker('setDate', offer_expiry_date);
            elem.datepicker('update');

            scope.offerExpiresDatePicker = function() {
           
                elem.triggerHandler('focus');
            };

        }
    }
})


SimbaNicApp.directive('closeDatePicker', function($timeout, $parse, $filter){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var closeDate = new Date();
            //var close_date = closeDate.addDays(20);
            
            var close_date = new Date($filter('addWeekdays')(moment(), 20));
 

            elem.datepicker({
                orientation: "right",
                autoclose: true,
                todayHighlight: true,
                 startDate: new Date(),
                //endDate: new Date(new Date().getTime()+(20*24*60*60*1000)), // 20 Days
            });

            elem.datepicker('setDate', close_date);
            elem.datepicker('update');

            scope.closeDatePicker = function() {
                elem.triggerHandler('focus');
            };

        }
    }
})

SimbaNicApp.directive('visitDatePicker', function($timeout, $parse){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            parsed = $parse(attrs.datepicker);

            elem.datepicker({
                orientation: "right",
                autoclose: true,
                startDate: new Date(),
            });

            scope.visitDatePicker = function() {
                elem.triggerHandler('focus');
            };

        }
    }
})

SimbaNicApp.directive('earliestTimePicker', function($timeout, $filter){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.timepicker({
                autoclose: true,
                showSeconds: false,
                minuteStep: 30,
                defaultTime: false,
                //defaultTime: $filter('date')(new Date(new Date().getTime() + 30*60000), 'hh:mm a')

            });

            elem.timepicker().on('show.timepicker', function(e) {
                if (typeof(scope.visitForm.earliest_time) === 'undefined')
                {
                    elem.timepicker('setTime', '12:00 PM');
                }
            });

            scope.earliestTimePicker = function() {
                $timeout(function() {
                    elem.triggerHandler('click');
                }, 100);
            };
        }
    }
})

SimbaNicApp.directive('latestTimePicker', function($timeout, $filter){
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.timepicker({
                autoclose: true,
                showSeconds: false,
                minuteStep: 30,
                defaultTime: false,
                //defaultTime: $filter('date')(new Date(new Date().getTime() + 60*60000), 'hh:mm a'),
            });

            elem.timepicker().on('show.timepicker', function(e) {
                if (typeof(scope.visitForm.latest_time) === 'undefined')
                {
                    elem.timepicker('setTime', '01:00 PM');    
                }
            });

            scope.latestTimePicker = function() {
                $timeout(function() {
                    elem.triggerHandler('click');
                }, 100);
            };

        }
    }
})

SimbaNicApp.directive('ngSubjectProperty', function(settings, $rootScope, $state, $http, $stateParams, $window) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                
                // Put Web API HERE.
                "processing": false,
                "serverSide": true,
                "ajax": {
                    // "url": settings.siteURL + "assets/app/components/opportunities/subjectproperty.json",
                    "url": settings.siteURL + 'client/markets/10/properties/' + $stateParams.guid + '/show_subject_property.json',

                },
                
                scrollX: true,
                
                autoWidth: false,

                "paginate": false,
                "paging": false,
                "info": false,
                "searching": false,
                "ordering": false,
                
                //use this variable when ajax url caLL
                "columns": [
                    {
                        "data": null,
                        "render" : function (data,type,row) {
                            
                            return '<div class="font-green">Subject Property</div>';
                        },
                    },
                    {
                        "data": null,
                        "render" : function (data,type,row) {
                            return '<a class="simba_popup_map">' + data.street_address + '</a>';
                        }
                    },
                    { 
                        "data": "city",
                    },
                    { 
                        "data": "rental_output_cap_rate",
                    },
                    { 
                        "data": "gross_yield",
                    },
                    { 
                        "data": "list_price",
                    },
                    { 
                        "data": "dom",
                    },
                    { 
                        "data": "beds",
                    },
                ],

                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width", "145px");
                    $(nRow).children('td:eq(1)').css("min-width", "220px");
                    $(nRow).children('td:eq(2)').css("min-width", "120px");
                    $(nRow).children('td:eq(3)').css("min-width", "100px");
                    $(nRow).children('td:eq(4)').css("min-width", "140px");
                    $(nRow).children('td:eq(5)').css("min-width", "90px");
                    $(nRow).children('td:eq(6)').css("min-width", "70px");
                    $(nRow).children('td:eq(7)').css("min-width", "70px");
                    
                    return nRow;
                },

                "fnInitComplete": function(){

                },
                "fnDrawCallback":function() {

                    jQuery('.subject_property').parent('.dataTables_scrollBody').addClass('overflow_x_scroll');

                }
            });
        
        }
    };
});

SimbaNicApp.directive('ngComparableProperties', function(settings, $rootScope, $state, $http, $stateParams, $window) {

    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            console.debug("yes yes " + $stateParams.guid);
            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                },{
                    'orderable': false,
                    'targets': [1]
                }, {
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [2, 'asc']
                ],
                // Put Web API HERE.
                "processing": true,
                "serverSide": true,
                "ajax": {
                    // "url": settings.siteURL + "assets/app/components/opportunities/propertyservice.json",
                    "url": settings.siteURL + "client/markets/10/properties/" +  $stateParams.guid +  "/show_rental_comps_qualified.json",

                },
                
                "lengthMenu": [
                    [20, 50, 100],
                    [20, 50, 100]
                ],
                
                scrollY: 300,
                scrollX: true,
                
                "pageLength": 20,
                autoWidth: false,

                //"paginate": false,
                
                //use this variable when ajax url caLL
                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            // alert(settings.siteURL + "client/markets/10/properties/" + data.id +  "/show_comps.json");
                            var add_class = 'default';
                            var remove_class = 'red';
                            if(data.display)
                            {
                                add_class = 'green_dark';
                                remove_class = 'default';
                            }

                            return '<div class="mt-action-buttons ">\
                                <div class="btn-group btn-group-circle">\
                                    <button ng-simba-spinner-bar data-address ="'+ data.street_address +'" data-status="rental" class="btn simba_add_map '+ add_class +' btn-xs margin-right-0" type="button" style="width: 70px;"><i class="fa fa-plus"></i> Add</button>\
                                    <button ng-simba-spinner-bar data-address ="'+ data.street_address +'" data-status="rental" class="btn simba_remove_map '+ remove_class +' btn-xs " type="button" style="width: 70px;"><i class="fa fa-minus"></i> Remove</button>\
                                </div>\
                            </div>';
                        },
                    },
                    {
                        "data": null,
                        "render" : function (data,type,row) {
                            return '<a class="simba_popup_map">' + data.street_address + '</a>';
                        }
                    },
                    { 
                        "data": "city",
                    },
                    { 
                        "data": "rental_output_cap_rate",
                    },
                    { 
                        "data": "gross_yield",
                    },
                    { 
                        "data": "list_price",
                    },
                    { 
                        "data": "dom",
                    },
                    { 
                        "data": "beds",
                    },
                ],

                //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width", "145px");
                    $(nRow).children('td:eq(1)').css("min-width", "220px");
                    $(nRow).children('td:eq(2)').css("min-width", "120px");
                    $(nRow).children('td:eq(3)').css("min-width", "100px");
                    $(nRow).children('td:eq(4)').css("min-width", "140px");
                    $(nRow).children('td:eq(5)').css("min-width", "90px");
                    $(nRow).children('td:eq(6)').css("min-width", "70px");
                    $(nRow).children('td:eq(7)').css("min-width", "70px");
                    
                    return nRow;
                },

                "fnInitComplete": function(){

                },
                "fnDrawCallback":function() {

                    jQuery('#comparable_properties').parent('.dataTables_scrollBody').addClass('overflow_scroll');

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#comparable_properties_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#comparable_properties_wrapper .dataTables_length').css("display", "block");
                        $('#comparable_properties_wrapper .dataTables_filter').css("display", "none");                       
                    } 
                    else 
                    {
                        $('#comparable_properties_wrapper .dataTables_paginate').css("display", "none");
                        $('#comparable_properties_wrapper .dataTables_length').css("display", "none");
                        $('#comparable_properties_wrapper .dataTables_filter').css("display", "none");
                    }
                }
            });
        
        }
    };
});

SimbaNicApp.directive('ngRelatedProperties', function(settings, $rootScope, $state, $filter, $http ,$stateParams) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                },
                {
                    'orderable': false,
                    'targets': [1]
                }, {
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [2, 'asc']
                ],
                "processing": true,
                "serverSide": true,
                "ajax": {
                        // "url": settings.siteURL + "assets/app/components/opportunities/relatedproperties.json",
                        "url": settings.siteURL + "client/markets/10/properties/" +  $stateParams.guid +  "/show_rental_comps_others.json",
                        "data": function ( d ) {
                            
                        },
                    },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],

                scrollY: 150,
                scrollX: true,
                
                "pageLength": 20,
                autoWidth: false,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            var add_class = 'default';
                            var remove_class = 'red';
                            if(data.display)
                            {
                                add_class = 'green_dark';
                                remove_class = 'default';
                            }

                            return '<div class="mt-action-buttons ">\
                                <div class="btn-group btn-group-circle">\
                                    <button data-address ="'+ data.street_address +'" data-status="rental" data-status="rental" class="btn simba_add_map '+ add_class +' btn-xs margin-right-0" type="button" style="width: 70px;"><i class="fa fa-plus"></i> Add</button>\
                                    <button data-address ="'+ data.street_address +'" data-status="rental" data-status="rental" class="btn simba_remove_map '+ remove_class +' btn-xs " type="button" style="width: 70px;"><i class="fa fa-minus"></i> Remove</button>\
                                </div>\
                            </div>';
                        },
                        "width": "20%",
                    },
                    {
                        "data": null,
                        "render" : function (data,type,row) {
                            return '<a class="simba_popup_map">' + data.street_address + '</a>';
                        }
                    },
                    { 
                        "data": "city",
                        "width": "10%",
                    },
                    { 
                        "data": "rental_output_cap_rate",
                        "width": "5%",
                    },
                    { 
                        "data": "gross_yield",
                        "width": "15%",
                    },
                    { 
                        "data": "list_price",
                        "width": "10%",
                    },
                    { 
                        "data": "dom",
                        "width": "5%",
                    },
                    { 
                        "data": "beds",
                        "width": "5%",
                    },
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width", "145px");
                    $(nRow).children('td:eq(1)').css("min-width", "220px");
                    $(nRow).children('td:eq(2)').css("min-width", "120px");
                    $(nRow).children('td:eq(3)').css("min-width", "100px");
                    $(nRow).children('td:eq(4)').css("min-width", "140px");
                    $(nRow).children('td:eq(5)').css("min-width", "90px");
                    $(nRow).children('td:eq(6)').css("min-width", "70px");
                    $(nRow).children('td:eq(7)').css("min-width", "70px");
                    
                    return nRow;
                },
                "fnDrawCallback":function() {

                    jQuery('#related_properties').parent('.dataTables_scrollBody').addClass('overflow_scroll');

                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#related_properties_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#related_properties_wrapper .dataTables_length').css("display", "block");
                        $('#related_properties_wrapper .dataTables_filter').css("display", "none");                       
                    } 
                    else 
                    {
                        $('#related_properties_wrapper .dataTables_paginate').css("display", "none");
                        $('#related_properties_wrapper .dataTables_length').css("display", "none");
                        $('#related_properties_wrapper .dataTables_filter').css("display", "none");
                    }
                }
            });
        }
    };
});

SimbaNicApp.directive('ngSaleComparableProperties', function(settings, $rootScope, $state, $http, $stateParams, $window) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var oTable = elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                },{
                    'orderable': false,
                    'targets': [1]
                }, {
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [2, 'asc']
                ],
                // Put Web API HERE.
                "processing": true,
                "serverSide": true,
                "ajax": {
                     // "url": settings.siteURL + "client/markets/10/properties/" +  $stateParams.guid +  "/show_sale_comps.json",
                     "url": settings.siteURL + "client/markets/10/properties/" +  $stateParams.guid +  "/show_sale_comps_qualified.json",
                },
                
                "lengthMenu": [
                    [20, 50, 100],
                    [20, 50, 100]
                ],
                
                scrollY: 300,
                scrollX: true,
                
                "pageLength": 20,
                autoWidth: false,

                //"paginate": false,
                
                //use this variable when ajax url caLL
                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            var add_class = 'default';
                            var remove_class = 'red';
                            if(data.display)
                            {
                                add_class = 'green_dark';
                                remove_class = 'default';
                            }

                            return '<div class="mt-action-buttons ">\
                                <div class="btn-group btn-group-circle">\
                                    <button ng-simba-spinner-bar data-address ="'+ data.street_address +'" data-status="sale"  class="btn simba_add_map '+ add_class +' btn-xs margin-right-0" type="button" style="width: 70px;"><i class="fa fa-plus"></i> Add</button>\
                                    <button ng-simba-spinner-bar data-address ="'+ data.street_address +'" data-status="sale" class="btn simba_remove_map '+ remove_class +' btn-xs " type="button" style="width: 70px;"><i class="fa fa-minus"></i> Remove</button>\
                                </div>\
                            </div>';
                        },
                    },
                    {
                        "data": null,
                        "render" : function (data,type,row) {
                            return '<a class="simba_popup_map">' + data.street_address + '</a>';
                        }
                    },
                    { 
                        "data": "city",
                    },
                    { 
                        "data": "rental_output_cap_rate",
                    },
                    { 
                        "data": "gross_yield",
                    },
                    { 
                        "data": "list_price",
                    },
                    { 
                        "data": "dom",
                    },
                    { 
                        "data": "beds",
                    },
                ],

                "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width", "145px");
                    $(nRow).children('td:eq(1)').css("min-width", "220px");
                    $(nRow).children('td:eq(2)').css("min-width", "120px");
                    $(nRow).children('td:eq(3)').css("min-width", "100px");
                    $(nRow).children('td:eq(4)').css("min-width", "140px");
                    $(nRow).children('td:eq(5)').css("min-width", "90px");
                    $(nRow).children('td:eq(6)').css("min-width", "70px");
                    $(nRow).children('td:eq(7)').css("min-width", "70px");
                    
                    return nRow;
                },

                "fnInitComplete": function(){

                },
                "fnDrawCallback":function() {

                    jQuery('#sale_comparable_properties').parent('.dataTables_scrollBody').addClass('overflow_scroll');

                    if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#sale_comparable_properties_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#sale_comparable_properties_wrapper .dataTables_length').css("display", "block");
                        $('#sale_comparable_properties_wrapper .dataTables_filter').css("display", "none");                       
                    } 
                    else 
                    {
                        $('#sale_comparable_properties_wrapper .dataTables_paginate').css("display", "none");
                        $('#sale_comparable_properties_wrapper .dataTables_length').css("display", "none");
                        $('#sale_comparable_properties_wrapper .dataTables_filter').css("display", "none");
                    }
                }

            });

        }
    };
});



SimbaNicApp.directive('ngSaleRelatedProperties', function(settings, $rootScope, $state, $filter, $http, $stateParams) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            elem.dataTable({
            
                "language": $rootScope.simbaDataTable.language,

                buttons: [
                    
                ],

                "columnDefs": [{
                    'orderable': false,
                    'targets': [0]
                },
                {
                    'orderable': false,
                    'targets': [1]
                }, {
                    "searchable": false,
                    "targets": [0]
                }],

                "order": [
                    [2, 'asc']
                ],
                "processing": true,
                "serverSide": true,
                "ajax": {
                        // "url": settings.siteURL + "assets/app/components/opportunities/saleRelatedProperties.json",
                        "url": settings.siteURL + "client/markets/10/properties/" +  $stateParams.guid +  "/show_sale_comps_others.json",
                        "data": function ( d ) {
                            
                        },
                    },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],

                scrollY: 150,
                scrollX: true,
                
                "pageLength": 20,
                autoWidth: false,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            var add_class = 'default';
                            var remove_class = 'red';
                            if(data.display)
                            {
                                add_class = 'green_dark';
                                remove_class = 'default';
                            }

                            return '<div class="mt-action-buttons ">\
                                <div class="btn-group btn-group-circle">\
                                    <button data-address ="'+ data.street_address +'" data-status="sale" class="btn simba_add_map '+ add_class +' btn-xs margin-right-0" type="button" style="width: 70px;"><i class="fa fa-plus"></i> Add</button>\
                                    <button data-address ="'+ data.street_address +'" data-status="sale" class="btn simba_remove_map '+ remove_class +' btn-xs " type="button" style="width: 70px;"><i class="fa fa-minus"></i> Remove</button>\
                                </div>\
                            </div>';
                        },
                        "width": "20%",
                    },
                    {
                        "data": null,
                        "render" : function (data,type,row) {
                            return '<a class="simba_popup_map">' + data.street_address + '</a>';
                        }
                    },
                    { 
                        "data": "city",
                        "width": "10%",
                    },
                    { 
                        "data": "rental_output_cap_rate",
                        "width": "5%",
                    },
                    { 
                        "data": "gross_yield",
                        "width": "15%",
                    },
                    { 
                        "data": "list_price",
                        "width": "10%",
                    },
                    { 
                        "data": "dom",
                        "width": "5%",
                    },
                    { 
                        "data": "beds",
                        "width": "5%",
                    },
                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                   
                    $(nRow).children('td:eq(0)').css("min-width", "145px");
                    $(nRow).children('td:eq(1)').css("min-width", "220px");
                    $(nRow).children('td:eq(2)').css("min-width", "120px");
                    $(nRow).children('td:eq(3)').css("min-width", "100px");
                    $(nRow).children('td:eq(4)').css("min-width", "140px");
                    $(nRow).children('td:eq(5)').css("min-width", "90px");
                    $(nRow).children('td:eq(6)').css("min-width", "70px");
                    $(nRow).children('td:eq(7)').css("min-width", "70px");
                    
                    return nRow;
                },
                "fnDrawCallback":function() {

                    jQuery('#sale_related_properties').parent('.dataTables_scrollBody').addClass('overflow_scroll');

                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                        $('#sale_related_properties_wrapper .dataTables_paginate').css("display", "block"); 
                        $('#sale_related_properties_wrapper .dataTables_length').css("display", "block");
                        $('#sale_related_properties_wrapper .dataTables_filter').css("display", "none");                       
                    }
                    else 
                    {
                        $('#sale_related_properties_wrapper .dataTables_paginate').css("display", "none");
                        $('#sale_related_properties_wrapper .dataTables_length').css("display", "none");
                        $('#sale_related_properties_wrapper .dataTables_filter').css("display", "none");
                    }
                }
            });
        }
    };

});

SimbaNicApp.directive('simbaOpportunityOfferForm', function(settings) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            elem.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    offer_price: {
                        required: true,
                        currency: ["$", false]
                    },
                    /*max_offer: {
                        required: true,
                        currency: ["$", false]
                    },*/
                    close_date: {
                        required: true,
                        date : true,
                    },
                    earnest: {
                        required: true,
                        currency: ["$", false]
                    },
                    offer_expires: {
                        required: true,
                        date : true,
                    },
                    option_amount: {
                        required: function(element){
                            return scope.offerForm.option_inspect_period == "yes";
                        },
                        currency: ["$", false]
                    },
                    closing_support: {
                        required: function(element){
                            return scope.offerForm.closing_costs == "yes";
                        },
                        currency: ["$", false]
                    },
                    loan: {
                        required: function(element){
                            return scope.offerForm.financing == "yes";
                        }
                    },
                    loan_amount: {
                        required: function(element){
                            return scope.offerForm.financing == "yes";
                        },
                        currency: ["$", false]
                    },
                    loan_type: {
                        required: function(element){
                            return scope.offerForm.financing == "yes";
                        }
                    },
                    interest_rate: {
                        required: function(element){
                            return scope.offerForm.financing == "yes";
                        }
                    },
                    terms_yrs: {
                        required: function(element){
                            return scope.offerForm.financing == "yes";
                        }
                    },
                    appraisal: {
                        required: function(element){
                            return scope.offerForm.financing == "yes";
                        }
                    },
                    special_term_notes: {
                        required: function(element){
                            return scope.offerForm.special_terms == "yes";
                        }
                    },
                    private_negotiation_notes: {
                        required: function(element){
                            return scope.offerForm.negotiation_notes == "yes";
                        }
                    },
                },

                messages: {
                    
                },

                invalidHandler: function(event, validator) { //display error alert on form submit   
                    jQuery('.alert-danger', elem).show();
                },

                highlight: function(element) { // hightlight error inputs
                    jQuery(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function(error, element) {
                    error.insertAfter(element.closest('.form-control'));
                    error.insertAfter(element.closest('.md-radio-inline'));
                },

                submitHandler: function(form) {
                    //form.submit(); // form validation success, call ajax form submit
                }
            });

            jQuery('.simba-opportunity-offer-form input').keypress(function(e) {
                if (e.which == 13) {
                    if (elem.validate().form()) {
                        elem.submit(); //form validation success, call ajax form submit
                    }
                    return false;
                }
            });

            jQuery.extend(jQuery.validator.messages, {
                required: "Required Field",
            });

            scope.offerFormSubmit = function() {
                if(elem.validate().errorList.length != 0)
                {
                    if (elem.validate().form()) {
                    }
                    return false;    
                }
            }
        }
    };
});