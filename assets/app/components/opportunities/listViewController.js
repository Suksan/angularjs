angular.module('SimbaNicApp').controller('opportunityListViewController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {

    $scope.listView = function() {

        jQuery('.page-head').show();
        $scope.showListView = true;
        $scope.showMapView = false;
        $scope.renderopportunitiesMap = false;
    }

    $scope.mapView = function() {
        
        jQuery('.page-head').hide();
        $scope.showMapView = true;
        $scope.showListView = false;
        $scope.renderopportunitiesMap = true;
        $scope.templateMapViewUrl = "assets/app/components/opportunities/listView/mapView.html?timeStamp="+ new Date().getTime();
        $scope.templateMapViewMapUrl = "assets/app/components/opportunities/listView/map.html?timeStamp="+ new Date().getTime();
    }

    $scope.listView();

    var query = "";

    var opportunityView = $state.current.data.opportunityView ? $state.current.data.opportunityView : 'active';

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    $scope.getDatetime = function() {
        return new Date().getTime();
    };
     $scope.binViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/detailview/binView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_bin_dialog',
            scope: $scope
        });
    }
    $scope.binAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/opportunities/binAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_bin_dialog',
            scope: $scope
        });
    }

});

SimbaNicApp.controller('binViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getListview(id) {
         // alert("get location");
         CgseqService.getListview(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load bin data: ' + error.message;
            });
    }
    $scope.editBin = function (db) {
        // alert('yes');
       
        $params = $.param({
            "id": db.id,
            "tag_id": db.tag_id,
            "barcode_id": db.barcode_id,
            "model": db.model,
            "weight": db.weight,
            "dimension": db.dimension,
            "load_status": db.load_status,
            "status": db.status
        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveBin(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getListview($scope.selected_id);


});
SimbaNicApp.controller('binAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initBinAdd(id) {
          // alert("get bin" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            tag_id : "",
            barcode_id : "",
            model : "",
            weight : "",
            dimension : "",
            load_status : "",
            status : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editBin = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.bin_obj = {id: record.id, tag_id: record.tag_id, barcode_id: record.barcode_id, model: record.model, weight: record.weight, dimension: record.dimension, load_status: record.load_status, status: record.status};

        $params = $.param({
            "id": $scope.bin_obj.id,
            "tag_id": $scope.bin_obj.tag_id,
            "barcode_id": $scope.bin_obj.barcode_id,
            "model": $scope.bin_obj.model,
            "weight": $scope.bin_obj.weight,
            "dimension": $scope.bin_obj.dimension,
            "load_status": $scope.bin_obj.load_status,
            "status": $scope.bin_obj.status

        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddBin($scope.bin_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initBinAdd($scope.selected_id);


});




angular.module('SimbaNicApp').controller('opportunityMapViewController', function($rootScope, $scope, $window, NgMap, settings, $http) {

    $scope.map = {};

    var bounds = new google.maps.LatLngBounds();
    var geocoder = new google.maps.Geocoder();

    $scope.deviceWidth = $window.innerWidth;

    $scope.refreshMap = function() {

        NgMap.getMap({ id:'opportunitiesMapView' }).then(function(map) {

            map.setCenter(bounds.getCenter());

            var listener = google.maps.event.addListener(map, "bounds_changed", function() {
                if (map.getZoom() > 16) {
                    
                    map.setZoom(15);
                }
                google.maps.event.removeListener(listener); 
            });

            map.fitBounds(bounds);
            $scope.map = map;
            google.maps.event.trigger(map, 'resize');
        });

        var element = angular.element('[scroll-to-fixed]');
        element.trigger('update');
    }

    $scope.initClosingMapData = function(){

        $scope.opportunitiesMapInfo = {
            center: [bounds.getCenter().lat(), bounds.getCenter().lng()],
            zoom: 11,
            pins: $scope.opportunitiesPins,
            height: $window.innerHeight - angular.element('[simba-page-container]').offset().top - 150 + "px",
        };
    }

    $scope.mapHeight = $window.innerHeight - angular.element('[simba-page-container]').offset().top - 150 + "px";
    console.log($scope.mapHeight);

    $scope.refreshMapBound = function() {

        var count = 0;
        var pinsLength = $scope.opportunitiesPins.length;
        // alert('pinsLength:' + pinsLength);

        angular.forEach($scope.opportunitiesPins, function(value, key) {

            var address = value.street_address + ', ' + value.city + ', ' + value.state + ', USA';

            $rootScope.simbaLoadingShow();

            $http({
                url: settings.googleGeocodeApiURL + '?key='+ settings.googleApiKey + '&address=' + address,
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
                },
            }).success(function(response) {

                $rootScope.simbaLoadingHide();
                
                if (response.status == "OK") {

                    //console.log(response.results[0].geometry.location.lat);
                    //console.log(response.results[0].geometry.location.lng);

                    var latlng = new google.maps.LatLng(response.results[0].geometry.location.lat, response.results[0].geometry.location.lng);
                    bounds.extend(latlng);

                    $scope.opportunitiesPins[key]['position'] = [response.results[0].geometry.location.lat, response.results[0].geometry.location.lng];
                }

                count = count + 1;
                if(pinsLength == count)
                {
                    $scope.refreshMap();
                    $scope.initClosingMapData();
                }
            });

            /*geocoder.geocode({ 'address': address }, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    
                    var latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                    bounds.extend(latlng);

                    $scope.opportunitiesPins[key]['position'] = [results[0].geometry.location.lat(), results[0].geometry.location.lng()];
                }
            });*/

        });

        
    }


    $scope.scrollToFixedOptions = {
        marginTop: $('.page-header').outerHeight(true) + 10,
        bottom: -10
    };
    
    // $scope.opportunitiesPins = [{"id":"5821862","property_address_c":"6107 Queens River Dr","property_address_city_c":"Mableton","property_address_state_c":"GA","postal_code":"30126","bed_c":"5","bath_c":"3.5","sq_ft_c":"3061","offer_price_c":"","offer_date_c":"","list_price_c":"340000.0","close_date_c":""},{"id":"5975056","property_address_c":"5411 Pleasant Woods Dr","property_address_city_c":"Flowery Branch","property_address_state_c":"GA","postal_code":"30542","bed_c":"5","bath_c":"2.5","sq_ft_c":"2459","offer_price_c":"","offer_date_c":"","list_price_c":"219900.0","close_date_c":"06/09/2016"},{"id":"6154169","property_address_c":"65 Farmington Dr","property_address_city_c":"Woodstock","property_address_state_c":"GA","postal_code":"30188","bed_c":"3","bath_c":"2.5","sq_ft_c":"2438","offer_price_c":"","offer_date_c":"","list_price_c":"194900.0","close_date_c":""},{"id":"6276625","property_address_c":"6335 Polar Cir","property_address_city_c":"Flowery Branch","property_address_state_c":"GA","postal_code":"30542","bed_c":"4","bath_c":"3.5","sq_ft_c":"1876","offer_price_c":"","offer_date_c":"","list_price_c":"175000.0","close_date_c":""},{"id":"6324554","property_address_c":"1821 Blackwater Trce","property_address_city_c":"Marietta","property_address_state_c":"GA","postal_code":"30066","bed_c":"3","bath_c":"2.0","sq_ft_c":"2314","offer_price_c":"","offer_date_c":"","list_price_c":"160000.0","close_date_c":"05/19/2016"},{"id":"4491856","property_address_c":"385 Darter Run","property_address_city_c":"Kennesaw","property_address_state_c":"GA","postal_code":"30144","bed_c":"4","bath_c":"3.0","sq_ft_c":"1896","offer_price_c":"","offer_date_c":"","list_price_c":"174777.0","close_date_c":"06/08/2016"},{"id":"6133477","property_address_c":"750 Parkside Dr","property_address_city_c":"Woodstock","property_address_state_c":"GA","postal_code":"30188","bed_c":"4","bath_c":"3.0","sq_ft_c":"2519","offer_price_c":"","offer_date_c":"","list_price_c":"240000.0","close_date_c":""},{"id":"6382563","property_address_c":"2251 Ingram Rd","property_address_city_c":"Duluth","property_address_state_c":"GA","postal_code":"30096","bed_c":"4","bath_c":"2.5","sq_ft_c":"1779","offer_price_c":"","offer_date_c":"","list_price_c":"159900.0","close_date_c":"06/07/2016"},{"id":"5929237","property_address_c":"357 Grenier Ter","property_address_city_c":"Lawrenceville","property_address_state_c":"GA","postal_code":"30045","bed_c":"3","bath_c":"2.5","sq_ft_c":"2082","offer_price_c":"","offer_date_c":"","list_price_c":"169900.0","close_date_c":""},{"id":"5109875","property_address_c":"1063 Bailing Rd","property_address_city_c":"Lawrenceville","property_address_state_c":"GA","postal_code":"30043","bed_c":"4","bath_c":"3.5","sq_ft_c":"2538","offer_price_c":"","offer_date_c":"","list_price_c":"169000.0","close_date_c":""},{"id":"6426623","property_address_c":"2345 Bretdale Rd","property_address_city_c":"Duluth","property_address_state_c":"GA","postal_code":"30096","bed_c":"4","bath_c":"2.5","sq_ft_c":"1644","offer_price_c":"","offer_date_c":"","list_price_c":"179900.0","close_date_c":""},{"id":"6430955","property_address_c":"150 Weatherstone Dr","property_address_city_c":"Woodstock","property_address_state_c":"GA","postal_code":"30188","bed_c":"3","bath_c":"2.5","sq_ft_c":"1736","offer_price_c":"","offer_date_c":"","list_price_c":"199800.0","close_date_c":"06/10/2016"},{"id":"6431998","property_address_c":"2837 Riders Ct","property_address_city_c":"Dacula","property_address_state_c":"GA","postal_code":"30019","bed_c":"4","bath_c":"3.0","sq_ft_c":"2696","offer_price_c":"","offer_date_c":"","list_price_c":"170000.0","close_date_c":""},{"id":"6432013","property_address_c":"440 Cotton Gin Ln","property_address_city_c":"Lawrenceville","property_address_state_c":"GA","postal_code":"30045","bed_c":"4","bath_c":"3.0","sq_ft_c":"2525","offer_price_c":"","offer_date_c":"","list_price_c":"189900.0","close_date_c":""},{"id":"6432507","property_address_c":"402 Sam Cobb Ct","property_address_city_c":"Woodstock","property_address_state_c":"GA","postal_code":"30188","bed_c":"3","bath_c":"2.5","sq_ft_c":"2228","offer_price_c":"","offer_date_c":"","list_price_c":"184900.0","close_date_c":""},{"id":"6437446","property_address_c":"4171 S Arbor Cir","property_address_city_c":"Marietta","property_address_state_c":"GA","postal_code":"30066","bed_c":"4","bath_c":"2.5","sq_ft_c":"2519","offer_price_c":"","offer_date_c":"","list_price_c":"290000.0","close_date_c":""},{"id":"6027620","property_address_c":"3775 Laurel Brook Way","property_address_city_c":"Snellville","property_address_state_c":"GA","postal_code":"30039","bed_c":"4","bath_c":"2.5","sq_ft_c":"2167","offer_price_c":"","offer_date_c":"","list_price_c":"164900.0","close_date_c":""},{"id":"6215289","property_address_c":"3854 Laurel Dr","property_address_city_c":"Acworth","property_address_state_c":"GA","postal_code":"30101","bed_c":"4","bath_c":"2.5","sq_ft_c":"2158","offer_price_c":"","offer_date_c":"","list_price_c":"194500.0","close_date_c":""},{"id":"6269819","property_address_c":"329 Ridgewood Trl","property_address_city_c":"Canton","property_address_state_c":"GA","postal_code":"30115","bed_c":"4","bath_c":"2.5","sq_ft_c":"2263","offer_price_c":"","offer_date_c":"","list_price_c":"265000.0","close_date_c":"06/09/2016"},{"id":"6306686","property_address_c":"3145 Windsor Gate Run","property_address_city_c":"Duluth","property_address_state_c":"GA","postal_code":"30096","bed_c":"3","bath_c":"2.5","sq_ft_c":"1728","offer_price_c":"","offer_date_c":"","list_price_c":"180000.0","close_date_c":"06/20/2016"},{"id":"6372732","property_address_c":"3850 Jim Owens Rd","property_address_city_c":"Kennesaw","property_address_state_c":"GA","postal_code":"30152","bed_c":"3","bath_c":"3.5","sq_ft_c":"1720","offer_price_c":"","offer_date_c":"","list_price_c":"140000.0","close_date_c":"06/13/2016"},{"id":"6374086","property_address_c":"3990 Village Main St","property_address_city_c":"Loganville","property_address_state_c":"GA","postal_code":"30052","bed_c":"3","bath_c":"2.0","sq_ft_c":"2118","offer_price_c":"","offer_date_c":"","list_price_c":"165000.0","close_date_c":""},{"id":"4500371","property_address_c":"1768 Apple Blvd","property_address_city_c":"Marietta","property_address_state_c":"GA","postal_code":"30066","bed_c":"4","bath_c":"2.5","sq_ft_c":"1896","offer_price_c":"","offer_date_c":"","list_price_c":"185000.0","close_date_c":"07/14/2016"},{"id":"6408669","property_address_c":"113 Hidden Creek Dr","property_address_city_c":"Canton","property_address_state_c":"GA","postal_code":"30114","bed_c":"4","bath_c":"2.5","sq_ft_c":"2420","offer_price_c":"","offer_date_c":"","list_price_c":"153000.0","close_date_c":"06/21/2016"},{"id":"6415833","property_address_c":"4479 S Springwood","property_address_city_c":"Smyrna","property_address_state_c":"GA","postal_code":"30082","bed_c":"4","bath_c":"2.0","sq_ft_c":"1946","offer_price_c":"","offer_date_c":"","list_price_c":"144900.0","close_date_c":""},{"id":"6418466","property_address_c":"2416 Wingfield Dr","property_address_city_c":"Austell","property_address_state_c":"GA","postal_code":"30106","bed_c":"3","bath_c":"2.5","sq_ft_c":"2203","offer_price_c":"","offer_date_c":"","list_price_c":"169900.0","close_date_c":"07/07/2016"},{"id":"6438784","property_address_c":"914 River Rock Dr","property_address_city_c":"Woodstock","property_address_state_c":"GA","postal_code":"30188","bed_c":"3","bath_c":"2.0","sq_ft_c":"1624","offer_price_c":"","offer_date_c":"","list_price_c":"163000.0","close_date_c":"06/24/2016"},{"id":"6429263","property_address_c":"3417 Willgrove","property_address_city_c":"Duluth","property_address_state_c":"GA","postal_code":"30096","bed_c":"4","bath_c":"2.5","sq_ft_c":"1923","offer_price_c":"","offer_date_c":"","list_price_c":"199900.0","close_date_c":"07/07/2016"},{"id":"6432633","property_address_c":"3546 Cherry Blossom Ln","property_address_city_c":"Loganville","property_address_state_c":"GA","postal_code":"30052","bed_c":"4","bath_c":"2.5","sq_ft_c":"2118","offer_price_c":"","offer_date_c":"","list_price_c":"159900.0","close_date_c":"07/29/2016"}]
    
    // alert('http');
    $http({ method: 'GET', url: '/client/markets/10/properties/search_opportunities' , headers:{ 'Content-Type': 'application/json'}}).
     success(function (data) {
        // alert(JSON.stringify(data.data));
        $scope.opportunitiesPins = data.data;
        // alert($scope.opportunitiesPins); 
        $scope.refreshMapBound();
    // ...
      }).
      error(function (data) {
        alert('error');
        // ...
      });
    
     // alert('sco');
     // $http({
                // url: 'http://localhost:3000/client/markets/10/properties/opportunities',
                // method: 'GET',
                // headers: {
                    // 'X-API-TOKEN': token,
                    // 'X-API-CREDENCIAL': credencial,
                    // 'X-API-ORIGEM': origem
                // }
            // }).success(function(response) {
//             
    // });
    // $scope.opportunitiesPins = [];
    // alert($http);
    // $http.get('http://localhost:3000/client/markets/10/properties/opportunities').
        // success(function(data) {
            // $scope.opportunitiesPins = data;
        // });
        
    
    
    
    

    $scope.showPropertyDetail = function(e, property, scroll_Top) {

        $scope.simbaProperty = property;

        NgMap.getMap({ id:'opportunitiesMapView' }).then(function(map) {
            $scope.map = map;
            $scope.map.showInfoWindow ('simbaInfoWindow', property.id);
            if($scope.map.markers)
            {
                angular.forEach($scope.map.markers, function(value, key) {
                    $scope.map.markers[key].setIcon("");
                });

                $scope.map.markers[property.id].setIcon(settings.siteURL + "assets/img/green_marker.png");
            }
        });

        jQuery('.map_bg_active').removeClass('bg-green');
        jQuery('.' + property.id + ' > .map_bg_active').addClass('bg-green');

        if(scroll_Top)
        {
            $window.scrollTo(0, $("." + property.id).offset().top - 200);
        }
    };

    angular.element($window).bind('resize', function(){
        //$scope.refreshMap();
    });
    
});