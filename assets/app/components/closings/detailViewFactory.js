SimbaNicApp.factory('closingDetail', function($http, settings) {
	var promise = null;
	return {
        getInfo: function(){
            if (promise) 
			{
				return promise;
			}
			else
			{
				var restClosingData = {
					session: '',
			 		module_name: 'Opportunities',
			 		ids: ['f21d7a55-3976-269f-ee4e-568a857c9dbd'],
			 		deleted: '0',
			 		select_fields: [],
			 		link_name_to_fields_array: [],
			 		order_by: '',
			     	offset: 0,
			 		limit: 2,
				}
				var restClosingDataJson = JSON.stringify(restClosingData);

			    var closingData = jQuery.param({
			    	method : "get_simba_entries",
				    input_type : "JSON",
				    response_type : "JSON",
				    rest_data : restClosingDataJson	
			    });

				$http({
				    url: settings.apiURL + 'service/v4_1/rest.php',
				    method: 'POST',
				    dataType: 'JSON',
				    data: closingData,
				    headers: {
				        'Content-Type': 'application/x-www-form-urlencoded charset=UTF-8'
				    },
				}).success(function(response){
				    return response.data;
				}).error(function(error){
				    return error;
				});
			}
        }  
    }
	
});