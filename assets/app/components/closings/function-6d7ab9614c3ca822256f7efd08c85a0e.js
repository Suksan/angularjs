SimbaNicApp.factory('closingsFunctions', function($http, settings) {
	var promise = null;
	return {
        getClosingsStatus: function(status) {
        	
        	//offer_make_offer
        	//offer_out_for_signature
        	//offer_sent_to_seller
        	//offer_lost_deal
        	//closing_under_contract
        	//closing_cancel_contract
        	//closed_purchased
        	//pre_offer_field_service
        	//offer_signed_ready_to_submit
        	//closing_contingencies
        	//closing_inspection
        	//closing_post_constingencies(Clear to close)

        	if(status == 'offer_make_offer' || status == 'offer_out_for_signature' || status == 'offer_signed_ready_to_submit' || status == 'offer_sent_to_seller')
        	{
        		return "Offer";
        	}
        	else if(status == 'closing_contingencies')
        	{
        		return "Won";
        	}
        	else if(status == 'closing_inspection')
        	{
        		return "Inspect";
        	}
        	else if(status == 'closing_post_constingencies')
        	{
        		return "Closing";
        	}
        	else if(status == 'closed_purchased')
        	{
        		return "Purchased";
        	}
        	else if(status == 'offer_lost_deal')
        	{
        		return "Lost";
        	}
            else if(status == 'closing_cancel_contract')
            {
                return "Cancelled";
            }
        	else
        	{
        		return "";
        	}
        }
    }
});
