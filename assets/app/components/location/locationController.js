angular.module('SimbaNicApp').controller('LocationController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

     $scope.locationViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/location/detailview/locationView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_location_dialog',
            scope: $scope
        });
    }
    $scope.locationAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/location/locationAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_location_dialog',
            scope: $scope
        });
    }

});
SimbaNicApp.controller('locationViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getLocation(id) {
         // alert("get location");
         CgseqService.getLocation(id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load Location data: ' + error.message;
            });
    }
    $scope.editLocation = function (db) {
       
        $params = $.param({
            "id": db.id,
            "name": db.name,
            "type": db.type,
            "address": db.address,
            "latitude": db.latitude,
            "longitude": db.longitude

        })
        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();
       

        CgseqService.saveLocation(db.id, $params).success(function(response) {
            alert('Record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getLocation($scope.selected_id);


});
SimbaNicApp.controller('locationAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initLocationAdd(id) {
          // alert("get location" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            name : "",
            type : "",
            address : "",
            latitude : "",
            longitude : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editLocation = function () {
        var record =  $scope.newRecord;
        // alert('yes');

        $scope.location_obj = {id: record.id, name: record.name,type: record.type, address: record.address,latitude: record.latitude, longitude: record.longitude};

        $params = $.param({
            "id": $scope.location_obj.id,
            "name": $scope.location_obj.name,
            "type": $scope.location_obj.type,
            "address": $scope.location_obj.address,
            "latitude": $scope.location_obj.latitude,
            "longitude": $scope.location_obj.longitude

        });

        // alert($params);
        alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();
        

        CgseqService.saveAddLocation($scope.location_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initLocationAdd($scope.selected_id);


});