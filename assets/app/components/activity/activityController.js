angular.module('SimbaNicApp').controller('ActivityController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {
	
	// $scope.simbaLoading = true;

    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.activityViewModals = function(id) {
        // alert("yes");
         // alert(id);
        $scope.selected_id = id

        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/activity/detailview/activityView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_activity_dialog',
            scope: $scope
        });
    }

    $scope.activityAddModals = function(id) {
        // alert("yes");
         // alert(id);
        $scope.selected_id = id

        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/activity/activityAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_activity_dialog',
            scope: $scope
        });
    }
});
SimbaNicApp.controller('activityViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function getActivity(id) {
         // alert("get activity" + id);
         CgseqService.getActivity(id)
            .then(function (response) {

                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                 
                
                // $scope.flood = {
                //     value: $scope.database.flood
                //   };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load truck data: ' + error.message;
            });
    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editActivity = function (db) {
        // alert('yes');
        $scope.activity_obj = {id: db.id, name: db.name};

        $params = $.param({
            "id": db.id,
            "name": db.name

        })

        alert('Updated Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveActivity(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    getActivity($scope.selected_id);


});

SimbaNicApp.controller('activityAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initActivityAdd(id) {
          // alert("get activity" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            name : ""
        };

        // $scope.newRecord.id = "xx";
         // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editActivity = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.activity_obj = {id: record.id, name: record.name};

        $params = $.param({
            "id": $scope.activity_obj.id,
            "name": $scope.activity_obj.name

        });

         // alert($params);
         alert('Insert Successfully');
        bootbox.hideAll();
        window.location.reload();

        CgseqService.saveAddActivity($scope.activity_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initActivityAdd($scope.selected_id);


});