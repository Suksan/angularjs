angular.module('SimbaNicApp').controller('historyController', function($rootScope, $scope, $http, $timeout, settings, $state, $stateParams, Auth, NgMap, $anchorScroll, $location, $ngBootbox, $window, SimbaNicFunctions, $filter, CgseqService) {

    $scope.listView = function() {

        jQuery('.page-head').show();
        $scope.showListView = true;
        $scope.showMapView = false;
        $scope.renderopportunitiesMap = false;
    }

    $scope.historyViewModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/history/detailview/historyView.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_history_dialog',
            scope: $scope
        });
    }
    $scope.historyAddModals = function(id) {
        // alert("yes");
        // alert(num);
        $scope.selected_id = id
        $ngBootbox.customDialog({
            templateUrl: 'assets/app/components/history/historyAdd.html?id=' + id,
            size: 'large',
            closeButton: false,
            className: 'simbanic_history_dialog',
            scope: $scope
        });
    }


});

SimbaNicApp.controller('historyViewController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService, $stateParams) {

    

    function getHistory() {

        // alert('stateParam: '  + $stateParams.id);
           // alert(id);
         CgseqService.getHistory( $stateParams.id)
            .then(function (response) {
                // alert("able");
                // console.log('success');
                $scope.database = response.data.data[0];
                // alert($scope.database);
                
                // $scope.flood = {
                    // value: $scope.database.flood
                  // };
                    
                // }
            }, function (error) {
                // console.log('error');
                // alert("unable");
                $scope.status = 'Unable to load history data: ' + error.message;
            });
    }
    $scope.editHistory = function (db) {
        alert('yes');
       
        $params = $.param({
            "id": db.id,
            "tag_id": db.tag_id,
            "load_status": db.load_status,
            "current_location": db.current_location,
            "station": db.station

        })

        CgseqService.saveHistory(db.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    getHistory();


});
SimbaNicApp.controller('historyAddController', function($rootScope, $scope, $injector, $ngBootbox, CgseqService) {

    

    function initHistoryAdd(id) {
          // alert("get truck" + id);
           
        // var $scope.newRecord = {};
        // $scope.newRecord.id;
        // $scope.newRecord.name;
        $scope.newRecord ={
            id : "",
            licence_number : "",
            make : "",
            model : "",
            colour : "",
            type : "",
            year : "",
            mileage : ""
        };

        // $scope.newRecord.id = "xx";
        // alert($scope.newRecord.id);

    }
    //Edit a person
            // $scope.editActivity = function (id)
    $scope.editHistory = function () {
        var record =  $scope.newRecord;
        // alert('yes');
        $scope.history_obj = {id: record.id, licence_number: record.licence_number, make: record.make, model: record.model, colour: record.colour, type: record.type, year: record.year, mileage: record.mileage};

        $params = $.param({
            "id": $scope.history_obj.id,
            "licence_number": $scope.history_obj.licence_number,
            "make": $scope.history_obj.make,
            "model": $scope.history_obj.model,
            "colour": $scope.history_obj.colour,
            "type": $scope.history_obj.type,
            "year": $scope.history_obj.year,
            "mileage": $scope.history_obj.mileage

        });

        alert($params);

        CgseqService.saveAddHistory($scope.history_obj.id, $params).success(function(response) {
            alert('record updated');
        }).error(function(error) {

        });

        // $http({
        //     method: 'POST',
        //     url: 'http://localhost:8080/api/activities/update/' + id,
        //     data: JSON.stringify($scope.activity_obj),
        //     headers: { 'Content-Type': 'application/JSON' }
        // }).
        // success(function (data) {
        //     //Showing Success message
        //     $scope.status = "The Person Updated Successfully!!!";
        //     //Loading people to the $scope
        //     GetPersons();
        //     //Displaying save button
        //     $scope.DisplaySave = true;
        //     //Hiding Update button
        //     $scope.DisplayUpdate = false;
        // })
        // .error(function (error) {
        //     //Showing error message
        //     $scope.status = 'Unable to update a person: ' + error.message;
        // });

        // alert(JSON.stringify($scope.activity_obj));


   }
    
  
      
    
    
    

    initHistoryAdd($scope.selected_id);


});