/**
 * Header Controller
 */

SimbaNicApp.controller('HeaderController', [ '$rootScope','$scope',
		'$http', '$window', '$location',
		function($rootScope, $scope, $http, $window,$location) {

			function logout() {
				var logout_path =  $location.host() == '45.35.251.155' ? 'http://45.35.251.155:82/' : '/';
				// alert ($location.host());
				// alert("sign out");

        		$http({method: 'DELETE', url: '/users/sign_out.json', data: {}});
				
				$window.location.href = logout_path;
			}

			$scope.logout = function() {
				var logout_path =  $location.host() == '45.35.251.155' ? 'http://45.35.251.155:82/' : '/';
				// alert ($location.host());

				
				// alert("sign out");

		        
		        $http({method: 'DELETE', url: '/users/sign_out.json', data: {}});
				
				$window.location.href = logout_path;
			}
		
			/*$scope.$on('IdleTimeout', function() {
				logout();
				$rootScope.logoutApp();
			});*/

} ]);