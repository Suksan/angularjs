SimbaNicApp.directive('simbaLoginForm', function(settings) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            elem.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                    remember: {
                        required: false
                    }
                },

                messages: {
                    username: {
                        required: "Username is required."
                    },
                    password: {
                        required: "Password is required."
                    }
                },

                invalidHandler: function(event, validator) { //display error alert on form submit   
                    jQuery('.alert-danger', elem).show();
                },

                highlight: function(element) { // hightlight error inputs
                    jQuery(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function(error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function(form) {
                    //form.submit(); // form validation success, call ajax form submit
                }
            });

            jQuery('.login-form input').keypress(function(e) {
                if (e.which == 13) {
                    if (elem.validate().form()) {
                        elem.submit(); //form validation success, call ajax form submit
                    }
                    return false;
                }
            });
        }
    };
});

SimbaNicApp.directive('simbaForgetForm', function(settings) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            elem.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    username: {
                        required: true,
                    },
                    user_email: {
                        required: true,
                        email: true
                    }
                },

                messages: {
                    username: {
                        required: "UserName is required.",
                    },
                    user_email: {
                        required: "Email is required."
                    }
                },

                invalidHandler: function(event, validator) { //display error alert on form submit   

                },

                highlight: function(element) { // hightlight error inputs
                    jQuery(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function(error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function(form) {
                    //form.submit();
                }
            });

            jQuery('.forget-form input').keypress(function(e) {
                if (e.which == 13) {
                    if (elem.validate().form()) {
                        jQuery('.forget-form').submit();
                    }
                    return false;
                }
            });

            jQuery('#forget-password').click(function() {
                jQuery('.login-form').hide();
                elem.show();
            });

            jQuery('#back-btn').click(function() {
                jQuery('.login-form').show();
                elem.hide();
            });
        }
    };
});