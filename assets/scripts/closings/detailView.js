/*new GMaps({
    div: '#gmap_basic',
    lat: -12.043333,
    lng: -77.028333
});*/

SimbaNicApp.directive('ngGmap', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            var map = new GMaps({
                div: '#gmap_basic',
                lat: -12.043333,
                lng: -77.028333
            });
        }
    };
});

SimbaNicApp.directive('ngSubpanelDocuments', function(settings, $stateParams, AuthService) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            var restDocumentsData = {
                session: AuthService.getSessionId(),
                module_name: 'Opportunities',
                module_id: $stateParams.guid,
                link_field_name: 'documents',
                deleted: '0',
                related_fields: ["name", "filename", "template_type", "date_entered", "status_id", "active_date", "category_id", "created_by_name"],
                related_module_link_name_to_fields_array: [],
                related_module_query: '',
                order_by: '',
                offset: 0,
                limit: -1,
            }
            var restDocumentsDataJson = JSON.stringify(restDocumentsData);

            var oTable = elem.dataTable({
            
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "Nothing here yet",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "_MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       type: 'column'
                    }
                },

                "order": [
                    [1, 'asc']
                ],

                "ajax": {
                        "url": settings.apiURL + "/service/v4_1/rest.php",
                        "data": function ( d ) {
                            d.method = "get_simba_relationships";
                            d.input_type = "JSON";
                            d.response_type = "JSON";
                            d.rest_data = restDocumentsDataJson
                        },
                    },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],
                
                "pageLength": 5,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                columnDefs: [ {
                    className: 'control',
                    orderable: false,
                    targets:   0
                } ],

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row){
                            return "";
                        },
                    },
                    { 
                        "data": null,
                        "width": "40%",
                        "render" : function (data,type,row){
                            return "<a href='#' target='_blank'>"+ data.name +"</a>";
                        }, 
                    },
                    { "data": "filename" },
                    { "data": "template_type" },
                    { "data": "date_entered" },
                    { "data": "status_id" },
                    { "data": "active_date",
                        "width": "30%" },
                    { "data": "created_by_name",
                    "width": "30%" },
                ],
                "fnDrawCallback":function(){
                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                             $('#subpanel_documents_wrapper .dataTables_paginate').css("display", "block"); 
                             $('#subpanel_documents_wrapper .dataTables_length').css("display", "block");
                             $('#subpanel_documents_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                             $('#subpanel_documents_wrapper .dataTables_paginate').css("display", "none");
                             $('#subpanel_documents_wrapper .dataTables_length').css("display", "none");
                             $('#subpanel_documents_wrapper .dataTables_filter').css("display", "none");
                     }
                }
                
            });
        }
    };
});

SimbaNicApp.directive('ngSubpanelContacts', function(settings, $stateParams, AuthService) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            
            var restContactsData = {
                session: AuthService.getSessionId(),
                module_name: 'Opportunities',
                module_id: $stateParams.guid,
                link_field_name: 'contacts',
                deleted: '0',
                related_fields: ["name", "company_name_c", "phone_work", "phone_mobile", "title", "primary_address_street",  "description"],
                related_module_link_name_to_fields_array: [{"name":"email_addresses","value":["id","email_address","opt_out","primary_address"]}],
                related_module_query: '',
                order_by: '',
                offset: 0,
                limit: -1,
            }
            var restContactsDataJson = JSON.stringify(restContactsData);

            elem.dataTable({
            
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "Nothing here yet",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "_MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       type: 'column'
                    }
                },

                "order": [
                    [1, 'asc']
                ],

                "ajax": {
                    "url": settings.apiURL + "/service/v4_1/rest.php",
                    "data": function ( d ) {
                        d.method = "get_simba_relationships";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = restContactsDataJson
                    },
                },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],
                
                "pageLength": 5,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                columnDefs: [ {
                    className: 'control',
                    orderable: false,
                    targets:   0
                } ],

                "columns": [
                    {
                        "data": null,
                        "render" : function (data,type,row){
                            return "";
                        },
                    },
                    {
                        "data": null,
                        "width": "30%", 
                        "render" : function (data,type,row){
                            return "<a href='#' target='_blank'>"+ data.name +"</a>";
                        },

                    },
                    { "data": "company_name_c",
                        "width": "20%", 
                    },
                    { "data": "phone_work",
                        "width": "20%", 
                    },
                    { "data": null,
                      "render" : function (data,type,row){
                        return "";
                        },
                        "width": "30%",
                    },
                    { "data": "phone_mobile" },
                    { "data": "title" },
                    { "data": "description" },
                ],
                "fnDrawCallback":function(){
                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                             $('#subpanel_contacts_wrapper .dataTables_paginate').css("display", "block"); 
                             $('#subpanel_contacts_wrapper .dataTables_length').css("display", "block");
                             $('#subpanel_contacts_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                             $('#subpanel_contacts_wrapper .dataTables_paginate').css("display", "none");
                             $('#subpanel_contacts_wrapper .dataTables_length').css("display", "none");
                             $('#subpanel_contacts_wrapper .dataTables_filter').css("display", "none");
                     }
                }

            });
        }
    };
});


SimbaNicApp.directive('ngSubpanelHistory', function(settings, $stateParams, AuthService) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            
            var restHistoryData = {
                session: AuthService.getSessionId(),
                module_name: 'Opportunities',
                id: $stateParams.guid,
            }
            var restHistoryDataJson = JSON.stringify(restHistoryData);

            elem.dataTable({
            
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "Nothing here yet",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "_MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },

                buttons: [
                    
                ],

                responsive: {
                    details: {
                       
                    }
                },

                "order": [
                    [0, 'asc']
                ],

                "ajax": {
                    "url": settings.apiURL + "/custom/service/v4_1_custom/rest.php",
                    "data": function ( d ) {
                        d.method = "get_simba_activities";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = restHistoryDataJson
                    },
                },
                
                "lengthMenu": [
                    [5, 10, 15, 20],
                    [5, 10, 15, 20]
                ],
                
                "pageLength": 5,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { 
                        "data": null,
                        "render" : function (data,type,row) {
                            if(!data.description)
                            {
                                data.description = "";
                            }
                            return '<div class="general-item-list">\
                                            <div class="item">\
                                                <div class="item-head">\
                                                    <div class="item-details">\
                                                        <img class="item-pic rounded" src="public/assets/img/avatar9.jpg">\
                                                        <a href="#" class="item-name primary-link"> ' 
                                                        + data.name +
                                                        '</a>\
                                                        <span class="item-label">3 hrs ago</span>\
                                                    </div>\
                                                    <span class="item-status">\
                                                        <span class="badge badge-empty badge-success"></span> ' + data.status + '</span>\
                                                </div>\
                                                <div class="item-body"> '
                                                    + data.description + '\
                                                </div>\
                                            </div>';
                        },
                        "width": "100%",
                    },
                ],
                "fnDrawCallback":function(){
                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                             $('#subpanel_history_wrapper .dataTables_paginate').css("display", "block"); 
                             $('#subpanel_history_wrapper .dataTables_length').css("display", "block");
                             $('#subpanel_history_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                             $('#subpanel_history_wrapper .dataTables_paginate').css("display", "none");
                             $('#subpanel_history_wrapper .dataTables_length').css("display", "none");
                             $('#subpanel_history_wrapper .dataTables_filter').css("display", "none");
                     }
                }

            });
        }
    };
});


SimbaNicApp.directive('ngSubpanelTasks', function(settings, $stateParams, AuthService) {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {

            var restTasksData = {
                session: AuthService.getSessionId(),
                module_name: 'Opportunities',
                module_id: $stateParams.guid,
                link_field_name: 'tasks',
                deleted: '0',
                related_fields: ["name", "status", "date_due", "assigned_user_name"],
                related_module_link_name_to_fields_array: [],
                related_module_query: '',
                order_by: '',
                offset: 0,
                limit: -1,
            }
            var restTasksDataJson = JSON.stringify(restTasksData);

            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
                oTable.fnDraw();
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnDraw();
            }

            var table = elem;

            var oTable = table.dataTable({

                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "Nothing here yet",
                    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                    "infoEmpty": "No entries found",
                    "infoFiltered": "(filtered1 from _MAX_ total entries)",
                    "lengthMenu": "_MENU_ entries",
                    "search": "Search:",
                    "zeroRecords": "No matching records found"
                },

                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],

                "pageLength": 5,

                "language": {
                    "lengthMenu": " _MENU_ records"
                },

                "ajax": {
                    "url": settings.apiURL + "/service/v4_1/rest.php",
                    "data": function ( d ) {
                        d.method = "get_simba_relationships";
                        d.input_type = "JSON";
                        d.response_type = "JSON";
                        d.rest_data = restTasksDataJson
                    },
                },
                "bFilter": false,
                
                "columnDefs": [{
                    'orderable': true,
                    'targets': [0]
                }, {
                    "searchable": true,
                    "targets": [0]
                }],
                "order": [
                    [1, "asc"]
                ],
                
                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "columns": [
                    { "data": null,
                        "render" : function (data, type, row){
                            return "<label><div class='checker'><span><input type='checkbox'></span></div>";
                        },
                        "width": "5%"
                    },
                    { "data": "name",
                        "width": "40%" },
                    { "data": "status",
                        "width": "30%" },
                    { "data": null,
                        "render" : function (data, type, row){

                            var dueDate = data.date_due.split(" ");
                            var date = new Date(dueDate[0]);
                            return date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
                        },
                        "width": "25%"
                    },
                ],
                "fnDrawCallback":function(){
                   if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1)  {
                             $('#subpanel_tasks_wrapper .dataTables_paginate').css("display", "block"); 
                             $('#subpanel_tasks_wrapper .dataTables_length').css("display", "block");
                             $('#subpanel_tasks_wrapper .dataTables_filter').css("display", "block");                       
                     } else {
                             $('#subpanel_tasks_wrapper .dataTables_paginate').css("display", "none");
                             $('#subpanel_tasks_wrapper .dataTables_length').css("display", "none");
                             $('#subpanel_tasks_wrapper .dataTables_filter').css("display", "none");
                     }
                }
                
            });

            var nEditing = null;
            var nNew = false;

            table.on('click', '.delete', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
                alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            table.on('click', '.cancel', function (e) {
                e.preventDefault();
                if (nNew) {
                    oTable.fnDeleteRow(nEditing);
                    nEditing = null;
                    nNew = false;
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            table.on('click', '.edit', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                    alert("Updated! Do not forget to do some ajax to sync with backend :)");
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });


            jQuery('#subpanel_tasks_wrapper .row:nth-child(2) div:nth-child(2)').html("<a class='pull-right' href='#/Tasks'>See All Tasks <i class='fa fa-arrow-circle-o-right'></i></a>");
        }
    };
});
