/*!
 * angular-clndr 0.3.0
 * 10KB, http://10kb.nl/
 * License: MIT
 */

(function(){var a,b;b=angular.module("tien.clndr",[]),a=function(){var a,b;return b={clndr:"=tienClndrObject",events:"=tienClndrEvents",options:"=?tienClndrOptions"},a=["$scope","$element","$attrs","$transclude",function(a,b,c,d){return d(function(c,d){var e,f;return b.append(c),a.$watch("events",function(b){return a.clndr.setEvents(angular.copy(b||[]))}),f=function(a){return angular.extend(d,a)},e=angular.extend(a.options||{},{render:f}),a.clndr=angular.element("<div/>").clndr(e)})}],{restrict:"E",replace:!0,transclude:!0,scope:b,controller:a}},b.directive("tienClndr",a)}).call(this);
