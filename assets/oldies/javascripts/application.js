(function() {
  var $body, anchor, cleanArray, first, url;

  $body = $('body');

  window.onbeforeunload = function(e) {};

  cleanArray = function(actual) {
    var i, newArray;
    newArray = new Array();
    i = 0;
    while (i < actual.length) {
      if (actual[i]) {
        newArray.push(actual[i]);
      }
      i++;
    }
    return newArray;
  };

  url = document.location.toString();

  if (url.match('#')) {
    anchor = url.split('#')[1];
    first = anchor.split('_')[0];
    if (first) {
      $(".nav-tabs a[href=#" + first + "]").tab('show');
    }
    $(".nav-tabs a[href=#" + anchor + "]").tab('show');
  } else {
    $('.nav-tabs a:first').tab('show');
  }

  $('.nav-tabs a').on('shown', function(e) {
    var scr;
    scr = document.body.scrollTop;
    window.location.hash = e.target.hash;
    return document.body.scrollTop = scr;
  });

  if ($body.data('reload')) {
    setTimeout('location.reload(true)', $body.data('reload'));
  }

  $('form').submit(function(e) {
    $(this).attr('action', $(this).attr('action') + document.location.hash);
    return true;
  });

  $('form[data-observe=true]').change(function(event) {
    return self.changes = true;
  }).submit(function(event) {
    self.changes = false;
    return true;
  });

  $('form[data-autosubmit=true]').change(function(event) {
    return $(this).submit();
  });

  $('#rep_inverse').change(function() {
    var first_option, i, opt, option, target;
    target = $(this).next().next().next().find('select');
    target.val(null);
    target.trigger('change');
    opt = {};
    i = 0;
    option = void 0;
    first_option = $("#search_config_attributes_rep option:first").val();
    $("#search_config_attributes_rep option").each(function() {
      option = void 0;
      option = $(this);
      if (first_option === "0") {
        if (i === 0) {
          opt["-999"] = option.text();
        } else {
          opt["-" + option.val()] = option.text();
        }
      } else {
        if (i === 0) {
          opt["0"] = option.text();
        } else {
          opt[option.val().replace("-", "")] = option.text();
        }
      }
      i++;
    });
    $('#search_config_attributes_rep').empty();
    return $.each(opt, function(val, text) {
      $('#search_config_attributes_rep').append($("<option></option>").val(val).html(text));
    });
  });

  $('#tag_inverse').change(function() {
    var first_option, i, opt, option, target;
    target = $(this).next().next().next().find('select');
    target.val(null);
    target.trigger('change');
    opt = {};
    i = 0;
    option = void 0;
    first_option = $("#search_config_attributes_tag option:first").val();
    $("#search_config_attributes_tag option").each(function() {
      option = void 0;
      option = $(this);
      if (first_option === "0") {
        if (i === 0) {
          opt["-999"] = option.text();
        } else {
          opt["-" + option.val()] = option.text();
        }
      } else {
        if (i === 0) {
          opt["0"] = option.text();
        } else {
          opt[option.val().replace("-", "")] = option.text();
        }
      }
      i++;
    });
    $('#search_config_attributes_tag').empty();
    return $.each(opt, function(val, text) {
      $('#search_config_attributes_tag').append($("<option></option>").val(val).html(text));
    });
  });

  $('#flood_inverse').change(function() {
    var opt, target;
    target = $(this).next().next().next().find('select');
    if ($(this).is(':checked')) {
      opt = {
        '-1': 'Empty',
        '-high': 'High',
        '-moderate-low': 'Moderate-low',
        '-undetermined': 'Undetermined'
      };
    } else {
      opt = {
        '0': 'Empty',
        'high': 'High',
        'moderate-low': 'Moderate-low',
        'undetermined': 'Undetermined'
      };
    }
    $('#search_config_attributes_flood').empty();
    return $.each(opt, function(val, text) {
      $('#search_config_attributes_flood').append($("<option></option>").val(val).html(text));
    });
  });

  $('#near_commercial_inverse').change(function() {
    var opt, target;
    target = $(this).next().next().next().find('select');
    if ($(this).is(':checked')) {
      opt = {
        '-1': 'Empty',
        '-a': 'Yes',
        '-b': 'No'
      };
    } else {
      opt = {
        '0': 'Empty',
        'a': 'Yes',
        'b': 'No'
      };
    }
    $('#search_config_attributes_near_commercial').empty();
    return $.each(opt, function(val, text) {
      $('#search_config_attributes_near_commercial').append($("<option></option>").val(val).html(text));
    });
  });

  $('#mls_area_key_inverse').change(function() {
    var first_option, i, opt, option, target;
    target = $(this).next().next().next().find('select');
    target.val(null);
    target.trigger('change');
    opt = {};
    i = 0;
    option = void 0;
    first_option = $("#search_config_attributes_mls_area_key option:first").val();
    $("#search_config_attributes_mls_area_key option").each(function() {
      option = void 0;
      option = $(this);
      if (first_option === "0") {
        if (i === 0) {
          opt["-999"] = option.text();
        } else {
          opt["-" + option.val()] = option.text();
        }
      } else {
        if (i === 0) {
          opt["0"] = option.text();
        } else {
          opt[option.val().replace("-", "")] = option.text();
        }
      }
      i++;
    });
    $('#search_config_attributes_mls_area_key').empty();
    return $.each(opt, function(val, text) {
      $('#search_config_attributes_mls_area_key').append($("<option></option>").val(val).html(text));
    });
  });

  $('#geo_market_area_inverse').change(function() {
    var first_option, i, opt, option, target;
    target = $(this).next().next().next().find('select');
    target.val(null);
    target.trigger('change');
    opt = {};
    i = 0;
    option = void 0;
    first_option = $("#search_config_attributes_geo_market_area option:first").val();
    $("#search_config_attributes_geo_market_area option").each(function() {
      option = void 0;
      option = $(this);
      if (first_option === "0") {
        if (i === 0) {
          opt["-999"] = option.text();
        } else {
          opt["-" + option.val()] = option.text();
        }
      } else {
        if (i === 0) {
          opt["0"] = option.text();
        } else {
          opt[option.val().replace("-", "")] = option.text();
        }
      }
      i++;
    });
    $('#search_config_attributes_geo_market_area').empty();
    return $.each(opt, function(val, text) {
      $('#search_config_attributes_geo_market_area').append($("<option></option>").val(val).html(text));
    });
  });

  $('tr td a[data-toggle=delete]').live('click', function() {
    return $(this).closest('tr').fadeOut().remove();
  });

  $('[data-toggle=clear]').each(function() {
    var source, target;
    source = $(this);
    target = $(this).next().next().find('select');
    target.on('change', function() {
      return source.toggle(target.val() !== null).addClass('pull-right');
    });
    target.trigger('change');
    return source.live('click touchstart', function() {
      target.val(null);
      return target.trigger('change');
    });
  });

  $('a[data-toggle=add]').live('click', function() {
    var row_html, target;
    target = $($(this).data('target'));
    row_html = target.find('tr.hidden').html();
    return target.append($('<tr>').html(row_html));
  });

  $('input[type=text].date').each(function() {
    return $(this).datepicker();
  });

  $('input[type=text].slider').each(function() {
    var options;
    options = $(this).data('options');
    if (!$(this).val()) {
      $(this).val('0;0');
    }
    $(this).slider(options);
    return $(this).next().css({
      'margin-top': '10px'
    });
  });

  $('a.reset.query').live('click touchstart', function() {
    return $(this).prev().val($(this).data('original'));
  });

  $('input[type=text].active_id_togglible').each(function() {
    if (!$('input[type=checkbox]#market_job_active').attr('checked')) {
      return $("input[type=text].active_id_togglible").attr("disabled", "disabled");
    }
  });

  $('input[type=text].general_id_togglible').each(function() {
    if (!$('input[type=checkbox]#market_job_force').attr('checked')) {
      return $("input[type=text].general_id_togglible").attr("disabled", "disabled");
    }
  });

  $('input[type=text].general_date_togglible').each(function() {
    if (!$('input[type=checkbox]#market_job_duration').attr('checked')) {
      return $("input[type=text].general_date_togglible").attr("disabled", "disabled");
    }
  });

  $('input[type=checkbox]#market_job_active').live('click', function() {
    if ($(this).attr('checked')) {
      return $("input[type=text].active_id_togglible").removeAttr("disabled");
    } else {
      $("input[type=text].active_id_togglible").attr("disabled", "disabled");
      return $("input[type=text].active_id_togglible").val("");
    }
  });

  $('input[type=checkbox]#market_job_force').live('click', function() {
    if ($(this).attr('checked')) {
      return $("input[type=text].general_id_togglible").removeAttr("disabled");
    } else {
      $("input[type=text].general_id_togglible").attr("disabled", "disabled");
      return $("input[type=text].general_id_togglible").val("");
    }
  });

  $('input[type=checkbox]#market_job_duration').live('click', function() {
    if ($(this).attr('checked')) {
      return $("input[type=text].general_date_togglible").removeAttr("disabled");
    } else {
      $("input[type=text].general_date_togglible").attr("disabled", "disabled");
      return $("input[type=text].general_date_togglible").val("");
    }
  });

  $('select.autocomplete').each(function() {
    return $(this).select2();
  });

  $('input[type=text].transferable').each(function() {
    var element, k, mapping, tags, v;
    element = $(this).hide();
    mapping = element.data('collection');
    tags = [];
    for (k in mapping) {
      v = mapping[k];
      tags.push({
        id: k,
        text: v
      });
    }
    element.select2({
      width: '100%',
      allowClear: true,
      tags: tags,
      createSearchChoice: function(term, data) {
        return [];
      },
      formatSelection: function(item) {
        return mapping[item.id];
      }
    });
    element.on('change', function() {
      var last_token, tokens;
      tokens = element.select2("val");
      last_token = tokens[tokens.length - 1];
      if (mapping[last_token]) {
        return $(this).attr({
          value: tokens
        });
      }
    });
    return element.select2("container").find("ul.select2-choices").sortable({
      containment: 'parent',
      start: function() {
        return element.select2("onSortStart");
      },
      update: function() {
        return element.select2("onSortEnd");
      }
    });
  });

  $('#comment_list').each(function() {
    return $(this).scrollTop($(this)[0].scrollHeight);
  });

  $('input[type=text].user').each(function() {
    var pre;
    pre = $(this).data('pre');
    if ($(this).attr('admin')) {
      return $(this).tokenInput("/admin/users/search.json", {
        theme: 'facebook',
        hintText: 'Type in a name',
        tokenLimit: 1,
        prePopulate: pre ? [pre] : null
      });
    } else {
      return $(this).tokenInput("/admin/users/search_self.json", {
        theme: 'facebook',
        hintText: 'Type in a name',
        tokenLimit: 1,
        prePopulate: pre ? [pre] : null
      });
    }
  });

  $("input[type=text].user").change(function() {
    var property_id, property_market_id, rep;
    rep = $(this).val();
    property_id = $("#property_id").val();
    property_market_id = $("#property_market_id").val();
    return $.ajax({
      type: "post",
      url: "/markets/" + property_market_id + "/properties/" + property_id + "/update_rep",
      data: {
        rep: rep
      },
      dataType: "script"
    });
  });

  $("select#tag").change(function() {
    var app_namespace, app_root, property_id, property_market_id, tag;
    tag = $(this).val();
    property_id = $("#property_id").val();
    property_market_id = $("#property_market_id").val();
    if ($("select#tag").attr('app_root')) {
      app_root = $("select#tag").attr('app_root');
    } else {
      app_root = "";
    }
    if ($("select#tag").attr('app_namespace')) {
      app_namespace = $("select#tag").attr('app_namespace');
    } else {
      app_namespace = "";
    }
    return $.ajax({
      type: "post",
      url: app_root + app_namespace + "/markets/" + property_market_id + "/properties/" + property_id + "/update_tag",
      data: {
        tag: tag
      },
      dataType: "script"
    });
  });

  $("select#flood").change(function() {
    var app_namespace, app_root, flood, property_id, property_market_id;
    flood = $(this).val();
    property_id = $("#property_id").val();
    property_market_id = $("#property_market_id").val();
    if ($("select#flood").attr('app_root')) {
      app_root = $("select#flood").attr('app_root');
    } else {
      app_root = "";
    }
    if ($("select#flood").attr('app_namespace')) {
      app_namespace = $("select#flood").attr('app_namespace');
    } else {
      app_namespace = "";
    }
    return $.ajax({
      type: "post",
      url: app_root + app_namespace + "/markets/" + property_market_id + "/properties/" + property_id + "/update_flood",
      data: {
        flood: flood
      },
      dataType: "script"
    });
  });

  $("select#near_commercial").change(function() {
    var app_namespace, app_root, near_commercial, property_id, property_market_id;
    near_commercial = $(this).val();
    property_id = $("#property_id").val();
    property_market_id = $("#property_market_id").val();
    if ($("select#near_commercial").attr('app_root')) {
      app_root = $("select#near_commercial").attr('app_root');
    } else {
      app_root = "";
    }
    if ($("select#near_commercial").attr('app_namespace')) {
      app_namespace = $("select#near_commercial").attr('app_namespace');
    } else {
      app_namespace = "";
    }
    return $.ajax({
      type: "post",
      url: app_root + app_namespace + "/markets/" + property_market_id + "/properties/" + property_id + "/update_near_commercial",
      data: {
        near_commercial: near_commercial
      },
      dataType: "script"
    });
  });

  $("select#per_page").change(function() {
    var app_root, market_id, per_page, res, search_id, str;
    str = window.location.href;
    str = str.replace(/&per_page=\d+|per_page=\d+/, "");
    res = str.split("?");
    per_page = $(this).val();
    if ($("select#per_page").attr('data-app-root')) {
      app_root = $("select#per_page").attr('data-app-root');
    } else {
      app_root = "";
    }
    market_id = $("select#per_page").attr('data-market-id');
    search_id = $("select#per_page").attr('data-search-id');
    return $.ajax({
      type: "get",
      url: res[0] + '/set_per_page?' + res[1],
      data: {
        per_page: per_page
      },
      dataType: "script"
    });
  });

  $("select#saved_per_page").change(function() {
    var app_root, market_id, per_page, res, str;
    str = window.location.href;
    str = str.replace(/per_page=\d+|&amp;per_page=\d+/, "");
    res = str.split("/saved");
    per_page = $(this).val();
    if ($("select#saved_per_page").attr('data-app-root')) {
      app_root = $("select#saved_per_page").attr('data-app-root');
    } else {
      app_root = "";
    }
    market_id = $("select#saved_per_page").attr('data-market-id');
    return $.ajax({
      type: "get",
      url: res[0] + '/set_per_page' + res[1],
      data: {
        per_page: per_page
      },
      dataType: "script"
    });
  });

  $("select#data_per_page").change(function() {
    var app_root, data_action, market_id, page, per_page, res, str;
    str = window.location.href;
    data_action = $("select#data_per_page").attr('data-action');
    str = str.replace(/per_page=\d+|&amp;per_page=\d+/, "");
    res = str.split("/" + data_action);
    per_page = $(this).val();
    if ($("select#data_per_page").attr('data-app-root')) {
      app_root = $("select#data_per_page").attr('data-app-root');
    } else {
      app_root = "";
    }
    market_id = $("select#data_per_page").attr('data-market-id');
    page = $("select#data_per_page").attr('data-page');
    alert(res[0] + '/data_set_per_page' + res[1]);
    return $.ajax({
      type: "get",
      url: res[0] + '/data_set_per_page' + res[1],
      data: {
        per_page: per_page,
        data_action: data_action,
        page: page
      },
      dataType: "script"
    });
  });

  $("select#client_rental_data_per_page").change(function() {
    var app_root, data_action, market_id, page, per_page, res, str;
    str = window.location.href;
    data_action = $("select#client_rental_data_per_page").attr('data-action');
    str = str.replace(/per_page=\d+|&amp;per_page=\d+/, "");
    res = str.split("/" + data_action);
    per_page = $(this).val();
    if ($("select#client_rental_data_per_page").attr('data-app-root')) {
      app_root = $("select#client_rental_data_per_page").attr('data-app-root');
    } else {
      app_root = "";
    }
    market_id = $("select#client_rental_data_per_page").attr('data-market-id');
    page = $("select#client_rental_data_per_page").attr('data-page');
    alert(res[0] + '/client_rental_data_set_per_page' + res[1]);
    return $.ajax({
      type: "get",
      url: res[0] + '/client_rental_data_set_per_page' + res[1],
      data: {
        per_page: per_page,
        data_action: data_action,
        page: page
      },
      dataType: "script"
    });
  });

  $("select#client_sale_data_per_page").change(function() {
    var app_root, data_action, market_id, page, per_page, res, str;
    str = window.location.href;
    data_action = $("select#client_sale_data_per_page").attr('data-action');
    str = str.replace(/per_page=\d+|&amp;per_page=\d+/, "");
    res = str.split("/" + data_action);
    per_page = $(this).val();
    if ($("select#client_sale_data_per_page").attr('data-app-root')) {
      app_root = $("select#client_sale_data_per_page").attr('data-app-root');
    } else {
      app_root = "";
    }
    market_id = $("select#client_sale_data_per_page").attr('data-market-id');
    page = $("select#client_sale_data_per_page").attr('data-page');
    alert(res[0] + '/client_sale_data_set_per_page' + res[1]);
    return $.ajax({
      type: "get",
      url: res[0] + '/client_sale_data_set_per_page' + res[1],
      data: {
        per_page: per_page,
        data_action: data_action,
        page: page
      },
      dataType: "script"
    });
  });

  $('a.add-my-prop').click(function() {
    var app_namespace, app_root, id_list, ids, market_id, opts, search_id, spinner;
    if ($("a.add-my-prop").attr('data-app-root')) {
      app_root = $("a.add-my-prop").attr('data-app-root');
    } else {
      app_root = "";
    }
    if ((app_root === 'null')) {
      app_root = "";
    }
    if ($("a.add-my-prop").attr('data-app-namespace')) {
      app_namespace = $("a.add-my-prop").attr('data-app-namespace');
    } else {
      app_namespace = "";
    }
    market_id = $("a.add-my-prop").attr('data-market-id');
    search_id = $("a.add-my-prop").attr('data-search-id');
    ids = [];
    $('input:checkbox[id*="search_config_attributes"]').each(function(i) {
      if ($(this).prop("checked")) {
        return ids[i] = $(this).val();
      }
    });
    id_list = cleanArray(ids).join(", ");
    opts = {
      lines: 12,
      length: 7,
      width: 5,
      radius: 10,
      color: "#000",
      speed: 1,
      trail: 100,
      shadow: true
    };
    spinner = new Spinner(opts).spin(document.getElementById("spinnerContainer"));
    return $.ajax({
      type: "post",
      url: app_root + app_namespace + "/markets/" + market_id + "/searches/" + search_id + "/add_my_prop/",
      beforeSend: function() {
        $("#spinnerContainer").show();
        return spinner.spin(document.getElementById("spinnerContainer"));
      },
      complete: function() {
        spinner.stop();
        return $("#spinnerContainer").hide();
      },
      data: {
        ids: id_list
      },
      dataType: "script"
    });
  });

  $('a.add-label').click(function() {
    var app_namespace, app_root, id_list, ids, market_id, opts, search_id, spinner, tag_id;
    if ($("a.add-label").attr('data-app-root')) {
      app_root = $("a.add-label").attr('data-app-root');
    } else {
      app_root = "";
    }
    if ((app_root === 'null')) {
      app_root = "";
    }
    if ($("a.add-my-prop").attr('data-app-namespace')) {
      app_namespace = $("a.add-my-prop").attr('data-app-namespace');
    } else {
      app_namespace = "";
    }
    market_id = $(this).attr('data-market-id');
    search_id = $(this).attr('data-search-id');
    tag_id = $(this).attr('data-tag-id');
    ids = [];
    $('input:checkbox[id*="search_config_attributes"]').each(function(i) {
      if ($(this).prop("checked")) {
        return ids[i] = $(this).val();
      }
    });
    id_list = cleanArray(ids).join(", ");
    opts = {
      lines: 12,
      length: 7,
      width: 5,
      radius: 10,
      color: "#000",
      speed: 1,
      trail: 100,
      shadow: true
    };
    spinner = new Spinner(opts).spin(document.getElementById("spinnerContainer"));
    return $.ajax({
      type: "post",
      url: app_root + app_namespace + "/markets/" + market_id + "/searches/" + search_id + "/add_label/",
      beforeSend: function() {
        $("#spinnerContainer").show();
        return spinner.spin(document.getElementById("spinnerContainer"));
      },
      complete: function() {
        spinner.stop();
        return $("#spinnerContainer").hide();
      },
      data: {
        ids: id_list,
        tag_id: tag_id
      },
      dataType: "script"
    });
  });

  $(document).ready(function() {
    $(document).on("submit", ".contact_submit_form", function(e) {
      var old_annual_maintenance_fee, old_client_flood, old_client_near_commercial, old_client_notes, old_client_rep, old_client_tag, old_pool_private, old_tax_amount, old_tax_rate_percent;
      old_tax_amount = $(".contact_associated_form #property_tax_amount").val();
      old_tax_rate_percent = $(".contact_associated_form #property_tax_rate_percent").val();
      old_annual_maintenance_fee = $(".contact_associated_form #property_annual_maintenance_fee").val();
      old_client_rep = $(".contact_associated_form #client_rep").val();
      old_client_flood = $(".contact_associated_form #client_flood").val();
      old_client_near_commercial = $(".contact_associated_form #client_near_commercial").val();
      old_client_tag = $(".contact_associated_form #client_tag").val();
      old_client_notes = $(".contact_associated_form #client_notes").val();
      old_pool_private = $(".contact_associated_form #property_pool_private").is(':checked');
      $(".contact_submit_form #property_tax_amount").val(old_tax_amount);
      $(".contact_submit_form #property_tax_rate_percent").val(old_tax_rate_percent);
      $(".contact_submit_form #property_annual_maintenance_fee").val(old_annual_maintenance_fee);
      $(".contact_submit_form #client_rep").val(old_client_rep);
      $(".contact_submit_form #client_flood").val(old_client_flood);
      $(".contact_submit_form #client_near_commercial").val(old_client_near_commercial);
      $(".contact_submit_form #client_tag").val(old_client_tag);
      $(".contact_submit_form #client_notes").val(old_client_notes);
      $(".contact_submit_form #property_pool_private").val(old_pool_private != null ? old_pool_private : {
        1: 0
      });
    });
    $(".up,.down").live("click", function() {
      var delay_click, delay_leave, ogColor, row, txt_seq;
      row = $(this).parents("tr:first");
      txt_seq = row.find('.integer.input-small');
      if ($(this).is(".up")) {
        if (row.prev().length) {
          row.prev().find('.integer.input-small').val(parseInt(row.prev().find('.integer.input-small').val(), 10) + 1);
          row.insertBefore(row.prev());
          txt_seq.val(parseInt(txt_seq.val(), 10) - 1);
        }
      } else {
        if (row.next().length) {
          row.next().find('.integer.input-small').val(parseInt(row.next().find('.integer.input-small').val(), 10) - 1);
          row.insertAfter(row.next());
          txt_seq.val(parseInt(txt_seq.val(), 10) + 1);
        }
      }
      delay_click = 400;
      delay_leave = 600;
      ogColor = row.css('backgroundColor');
      return row.animate({
        backgroundColor: '#DDDDDD'
      }, delay_click, function() {
        return row.animate({
          backgroundColor: ogColor
        }, delay_leave);
      });
    });
    $("select#rep").multiselect({
      selectedList: 20,
      minWidth: 140,
      header: false,
      noneSelectedText: " ",
      position: {
        my: 'left top',
        at: 'left bottom'
      }
    });
    $("button.ui-multiselect").css('min-height', '25px');
    $("select#rep").bind("change", function() {
      var app_namespace, app_root, property_id, property_market_id, reps, values, values2;
      values = $("select#rep").multiselect("getChecked").map(function() {
        return this.value;
      }).get();
      values2 = $("select#rep").val();
      reps = values.toString();
      property_id = $("#property_id").val();
      property_market_id = $("#property_market_id").val();
      if ($("select#rep").attr('app_root')) {
        app_root = $("select#rep").attr('app_root');
      } else {
        app_root = "";
      }
      if ($("select#rep").attr('app_namespace')) {
        app_namespace = $("select#rep").attr('app_namespace');
      } else {
        app_namespace = "";
      }
      return $.ajax({
        type: "post",
        url: app_root + app_namespace + "/markets/" + property_market_id + "/properties/" + property_id + "/update_rep",
        data: {
          values: reps
        },
        dataType: "script"
      });
    });
    return $("select#client_rep").multiselect({
      selectedList: 20,
      minWidth: 140,
      header: false,
      noneSelectedText: " ",
      position: {
        my: 'left top',
        at: 'left bottom'
      }
    });
  });

  $("#tags").on("cocoon:after-insert", function(e, insertedItem) {
    var max_seq;
    if (insertedItem.prev().length) {
      max_seq = insertedItem.prev().find('.integer.input-small').val();
    } else {
      max_seq = 0;
    }
    return insertedItem.find('.integer.input-small').val(parseInt(max_seq, 10) + 1);
  });

  $("#market_properties_rem_myprop").submit(function(event) {
    var len;
    len = $('input[id*="properties_config_id"]:checked').length;
    if ((len > 0) && (confirm("Are you sure you want to delete the selected?"))) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  });

  $(document).on('ajax:success', '.ajax_link', function(e) {
    $('#my-modal input[type=text].date').each(function() {
      return $(this).datepicker();
    });
    $('#ui-datepicker-div').appendTo($('#my-modal'));
    $('#my-modal .btn.btn-primary').click(function() {
      $('#my-modal').modal('hide');
      return $(this).closest("form").submit();
    });
    return $('#my-modal .btn.btn-close').click(function() {
      return $('#my-modal').modal('hide');
    });
  });

}).call(this);
