namespace :proxies do
  
  DAILY_LIMIT = 1500

  desc "Populate proxies"
  task :add => :environment do |t, args|
    begin
      ActiveRecord::Base.connection.execute("TRUNCATE proxies")
      file = File.new("proxies.txt",'r')
      while (line = file.gets)
        str_line = line.strip
        ary_line = str_line.split(":")
        
        proxy = Proxy.new(ip: ary_line[0],
                    port: ary_line[1],
                    login: ary_line[2],
                    password: ary_line[3],
                    daily_limit: DAILY_LIMIT,
                    daily_calls: 0
          )
        proxy.save
      end
      file.close
    rescue Exception => ex
      raise ex
    end
  end
  
  desc "Benchmark proxies"
  task :benchmark => :environment do |t, args|
    p = Property.find(2339211)
    @agent = Mechanize.new
    @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    @agent.user_agent = "ruby/net::http"
    @agent.pluggable_parser.default = Mechanize::Page
       
    
   
    test_items =  Proxy.where(["1=1"]).order("id asc")
    test_items.each { |prox|
      
      
      @agent.set_proxy(prox.ip,prox.port,prox.login,prox.password)
    
      
      a43 = 0.0
      a21 = 0.0
    
      begin
        a1 = Time.new
        attrs = lookup3(p,@agent)
        raise Exception.new if !(attrs && attrs.length > 0)
        # mapquest_geocode(p,@agent)
        a2 = Time.new
        a21 = a2 - a1
        a3 = Time.new
        attrs = lookup3(p,@agent)
        raise Exception.new if !(attrs && attrs.length > 0)
        # mapquest_geocode(p,@agent)
        a4 = Time.new
        a43 = a4 - a3
        
        avg = (a43 + a21)/2
        puts "#{prox.ip},#{avg}"
      
      rescue Exception => e
        puts "error class: #{e.class} message: #{e.message}"
        puts "#{prox.ip},error"
      end
    
      
      
    
    }
  end
  
  def lookup3(prop,ag)  #prop_address,zid,zillow_url
    
    attrs = {}
    
    elementary_school = nil
    middle_school= nil
    high_school= nil
    elementary_school_rating= nil
    middle_school_rating= nil
    high_school_rating= nil
    tax_amount = nil
    appreciation = nil
    zillow_sale_estimate = nil
    zillow_rental_estimate = nil
     
    tries = 0
     
    begin
      
      
     
      # page = agent.get 'http://www.zillow.com/'
      # form = page.form_with(:name => "formSearchBar")
      # form.citystatezip = prop_address #442040
      # puts "form submit"
      # page = form.submit
#       
      # href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
      # puts ">>>( href #{href}"
      
      # if !href
      
      href = prop.zillow_url
      # end
      
      # page = agent.get 'http://www.zillow.com/'
      # form = page.form_with(:name => "formSearchBar")
      # form.citystatezip = prop_address #442040
      # puts "form submit"
      # page = form.submit
#       
      # href = page.at("h1[text()*='Your search at']").parent.at("span[itemprop='streetAddress']").parent["href"] rescue nil
      
      if href
        zillow_id = nil
        
        phref = href
        # puts ">>> href #{phref}"
        page = ag.get phref
       
        
        m = href.match(/(\d+)_zpid\/$/).captures rescue nil
        # puts "$$% zillow get #{zillow_id}"
        
        if !prop.zillow_id 
          if m && m.length > 0
           zillow_id = m[0] rescue nil
          end
        else
          zillow_id = prop.zillow_id
        end
        
        # puts "$$$% xpath #{page.parser.xpath("//a[contains(@href,'-school-')]/following-sibling::span[1]").count}"
        page.parser.xpath("//a[contains(@href,'-school-')]").each do |scl|
          scl_item = scl.parent
          # puts "scl_item = #{scl_item}"
          scl_desc = scl.text
          # puts "scl_desc = #{scl_desc}"
          if scl_desc.match(/Elementary/)
            elementary_school ||= scl_desc.gsub("Elementary","").strip
            # puts "elementary_school = #{elementary_school}"
            elementary_school_rating ||= scl_item.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
             # puts "elementary_school_rating = #{elementary_school_rating}"
          elsif scl_desc.match(/Middle/) || scl_desc.match(/Junior High/) || scl_desc.match(/Intermediate/)
            middle_school ||= scl_desc.gsub(/Middle|Intermediate|Junior High/,"").strip
             # puts "middle_school = #{middle_school}"
            middle_school_rating ||= scl_item.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
             # puts "middle_school_rating = #{middle_school_rating}"
          elsif scl_desc.match(/High/) && !scl_desc.match(/Junior High/)
            high_school ||= scl_desc.gsub(/High/,"").strip
             # puts "high_school = #{high_school}"
            high_school_rating ||= scl_item.parent.first_element_child.first_element_child.children[0].text.to_i rescue nil
            # puts "high_school_rating = #{high_school_rating}"
          end
        end
        
        appre_catch = page.at("//div[@class='track-ga-event']").text.scan(/[0-9\.]+%/) rescue []
        # puts "~~appre catch length #{appre_catch.length}"
        if appre_catch.length > 0
          appreciation = appre_catch[0].gsub(/%/,"").to_d / 100.to_d
        end
        # puts " scrape sale est: #{page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text}"
        zillow_sale_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Zestimate')]").next_element.text).amount.to_i  rescue nil
        zillow_rental_estimate = Money.parse(page.at("//table[@id='zestimateTable']/tbody/tr/th[starts-with(text(),'Rent Zestimate')]").next_element.text).amount.to_i rescue nil
        # puts "@@@@ zillow_sale_estimate & zillow_rental_estimate #{zillow_sale_estimate} : #{zillow_rental_estimate}"
        # puts "page.body: #{page.body}"
        
        m = page.body.match(/(^asyncLoader.load\().+?AjaxRender.htm(.+?)","divId.+:"home-tax-history","jsModule":"z-expando-table"}\)/)
        
        if m && m.length 
          
          yhref = "http://www.zillow.com/AjaxRender.htm#{m[2]}"
          zhref = "#{yhref}"
          Delayed::Worker.logger.debug "yhref #{zhref}"
          
          
          page = ag.get zhref
         
          html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","'").gsub(/^ \{ 'html': '/,"").gsub(/'\s+}\s+$/,""))
          
          Delayed::Worker.logger.debug "#### tax amount html #{html_doc}"
          
          begin
            tax_amount = html_doc.at("//tbody/*[1]").first_element_child.next.text rescue nil
            
            if tax_amount
              tax_money =  Money.parse(tax_amount) 
              tax_amount = tax_money.amount.to_i
            end
          
          rescue
            
          end
          
          
        end
        
        # puts "###% zillow id #{zillow_id}"
        # puts "### walk score 'http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}'}"
       
        wshref = "http://www.zillow.com/walkscore/GetWalkscore.htm?zpid=#{zillow_id}"
        qhref = "#{wshref}"
        
        Delayed::Worker.logger.debug "qhref #{qhref}"
        
        # proxy = Proxy.find_available
        page = ag.get qhref
        # proxy.increment_calls
        # proxy.rem_error_count
        
        html_doc = Nokogiri::HTML(page.body.gsub("\\","").gsub("\"","").gsub(/^{ text : /,"").gsub(/\s+}$/,""))
        
        Delayed::Worker.logger.debug "#### walk score #{html_doc}"
        
        walk_score = html_doc.at("//div[@id='walkscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
        walk_score = nil if walk_score == 0
        transit_score = html_doc.at("//div[@id='transitscore-tip_toggleArea']").next_element.at("span[@class='ws-value']").text.to_i rescue nil
        transit_score = nil if transit_score == 0
        # puts "#%%%%% walk score #{walk_score}"
        # puts "#%%%%% transit score #{transit_score}"
        
        
        # puts "#%%% out #{elementary_school} #{elementary_school_rating}"
        attrs = { 
                  :walk_score => walk_score,
                  :transit_score => transit_score,
                  :school_updated_at => Time.now,
                  
               }
         attrs = attrs.merge({:elementary_school => elementary_school}) if elementary_school
         attrs = attrs.merge({:middle_school => middle_school}) if middle_school
         attrs = attrs.merge({:high_school => high_school}) if high_school
         attrs = attrs.merge({:elementary_school_rating => elementary_school_rating}) if elementary_school_rating
         attrs = attrs.merge({:middle_school_rating => middle_school_rating}) if middle_school_rating
         attrs = attrs.merge({:high_school_rating => high_school_rating}) if high_school_rating
         attrs = attrs.merge({:zillow_tax_amount => tax_amount})  if tax_amount
         attrs = attrs.merge({:appreciation_rate => appreciation}) if appreciation
         attrs = attrs.merge({:zillow_sale_estimate => zillow_sale_estimate}) if !prop.zillow_sale_estimate
         attrs = attrs.merge({:zillow_rental_estimate => zillow_rental_estimate}) if !prop.zillow_rental_estimate
             # puts "##** returning attrs #{attrs}"
         
              
      else
        return nil
      end
      
      attrs
      
    rescue Timeout::Error, Net::HTTP::Persistent::Error, Mechanize::ResponseCodeError => e 
      puts "specific exc #{e.class}"
      raise e
    rescue Exception => e
      puts "general exc #{e.class}"
      raise e
    end
    
  end

  def mapquest_geocode(prop,ag)
    begin 
      agent = ag
         
      Delayed::Worker.logger.debug "~~ mapquest geocode ~~"
      service = Service.find_available(:mapquest_geocoding)

      if service
        Delayed::Worker.logger.debug "~~ setting proxy"
        proxy = Proxy.find_available if !Rails.env.development?
        # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?
        
        Delayed::Worker.logger.debug "~~ on search"
        
        
        page = agent.get("http://www.mapquestapi.com/geocoding/v1/address?&key=#{service.key}",
            location: prop.address)

        service.increment_calls
        service.try(:rem_error_count)
        proxy.increment_calls if !Rails.env.development?
        proxy.rem_error_count if !Rails.env.development?  
        
        Delayed::Worker.logger.debug "~~ on search #{page.body}"
        
        json = page.body
        dec = ActiveSupport::JSON.decode json
        
        lat = dec.try(:[],"results").try(:[],0).try(:[],"locations").try(:[],0).try(:[],"latLng").try(:[],"lat") rescue nil
        long = dec.try(:[],"results").try(:[],0).try(:[],"locations").try(:[],0).try(:[],"latLng").try(:[],"lng") rescue nil
        Delayed::Worker.logger.debug "obtained lat: #{lat} long: #{long}"

        prop.latitude = lat
        prop.longitude = long
        
        prop.save(validate: false)
      end
     
     rescue Mechanize::ResponseCodeError => e

      Delayed::Worker.logger.debug "Error"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)

      case e.message
        when /403/ then 
          Service.find_available(:mapquest_geocoding)    
          service.try(:increment_calls)  
          service.try(:mark_error)
          service.try(:add_error_count)  
        when /400/ then 
          proxy = Proxy.find_available
          proxy.try(:increment_calls)
          proxy.try(:mark_error)
          proxy.try(:add_error_count)
      end
      raise e 
            
     rescue Exception =>  e 
      
      Delayed::Worker.logger.debug "Error"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)
      raise e    
     end
  end
  
  
  def google_geocode(prop,ag)
    begin
      agent = ag

      Delayed::Worker.logger.debug "~~ mapquest geocode ~~"



      Delayed::Worker.logger.debug "~~ setting proxy"
      proxy = Proxy.find_available if !Rails.env.development?
      # agent.set_proxy(proxy.ip,proxy.port,proxy.login,proxy.password) if !Rails.env.development?

      Delayed::Worker.logger.debug "~~ on search"


      # page = agent.get("http://www.mapquestapi.com/geocoding/v1/address?&key=#{service.key}",
          # location: prop.address)
      page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{prop.address}")


      proxy.increment_calls if !Rails.env.development?
      proxy.rem_error_count if !Rails.env.development?

      Delayed::Worker.logger.debug "~~ on search #{page.body}"

      json = page.body
            dec = ActiveSupport::JSON.decode json

      lat = dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat") rescue nil
      long = dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng") rescue nil

      Delayed::Worker.logger.debug "obtained lat: #{lat} long: #{long}"

      prop.latitude = lat
      prop.longitude = long

      prop.save(validate: false)

      return true if !(lat.blank? && long.blank)
     rescue Mechanize::ResponseCodeError => e

      Delayed::Worker.logger.debug "Error"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)

      case e.message
        when /403/ then

        when /400/ then

      end
      raise e

     rescue Exception =>  e

      Delayed::Worker.logger.debug "Error"
      proxy = Proxy.find_available
      proxy.try(:increment_calls)
      raise e
     end
  end

  
end
