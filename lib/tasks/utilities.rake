namespace :utilities do

  
  desc "parallel processing"
  task :ppro,[:market_id,:category,:statuses] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
      args.with_defaults(:status => "")
      statuses = args.statuses.split(" ")
      
      
      puts "#{market_id}   #{category}  #{statuses}"
      
      market = Market.find(market_id)
      statuses.each do |status|
        ids = {}
        ids[status] = []
        
        puts "nohup bundle exec rake jobs:sic[#{market.id},#{category},#{status}] RAILS_ENV=staging"
      end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
end