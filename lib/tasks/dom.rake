namespace :dom do
  
  task :reconcile => :environment do
    # property = Property.first(:order => "RAND()")
    # puts "property is: #{property.tax_id}"
    p "converting start time --- #{Time.new.inspect}" 
    
      
    Property.update_all ["cdom = cdom+ DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL dom DAY) - 1) ), 
                        dom = DATEDIFF( NOW(), listing_date) + 1, dom_updated_at = ?", DateTime.now ],
                        ["status in ('active','pending') 
                        and ((listing_date is not null) and ((cdom is not null) and (dom is not null)))"]
                         
    # Property.update_all "dom = DATEDIFF( NOW(), listing_date) + 1", "status in ('active'
                         # ,'pending') and ((listing_date is not null) and (cdom is not null and dom is not null))"             
                         
      # query = "UPDATE properties SET cdom = cdom+ DATEDIFF(NOW() , (DATE_ADD(listing_date, INTERVAL dom DAY) - 1) ),
            # dom = DATEDIFF( NOW(), listing_date) + 1 WHERE status in ('active'
            # ,'pending') and ((listing_date is not null) and (cdom is not null) and (dom is not null))" 
    # ActiveRecord::Base.connection.execute(query);
  
    p "converting end time --- #{Time.new.inspect}" 
  end
  
  task :add, [:days] => :environment do
    days = Integer(args.days) rescue 1
    Market.all.each do |market|
      MarketDomUpdateJob.new(market_id: market.id, days: days).program(priority: -1) 
    end
  end
  
  task :up, [:market_id] => :environment do |t, args|
    market_id = Integer(args.market_id)
    Delayed::Worker.logger.debug "market id: #{market_id}"
    MarketDomUpdateJob.new(market_id: market_id).program(priority: -1)    
  end
  
  
  
end 