require 'capybara/poltergeist'

namespace :jobs do

  PROX_1 = '23.95.193.93'
PROX_2 = '192.230.49.225'
PROX_3 = '23.94.87.170'
PROX_4 = '23.95.193.32'
PROX_5 = '192.69.254.216'
PROX_6 = '23.94.87.197'

  desc "Queue daily jobs"
  task :add, [:days,:daily,:cleanup] => :environment do |t, args|
    days = Integer(args.days) rescue 1
    args.with_defaults(:daily => false)
    daily = args.daily
    args.with_defaults(:cleanup => false)
    cleanup = args.cleanup
    Market.all.each do |market|
      if market.id.in?([1])
        MarketGrabJob.new(market_id: market.id, days: days, daily: daily, cleanup: cleanup).program(priority: -1)
      end
      # MarketComputeJob.new(market_id: market.id).program()
      # MarketEmailSearchesJob.new(market_id: market.id, limit: 1000, weekly: true).program()
    end
  end
  
   task :addh, [:start_day,:end_day,:daily,:cleanup] => :environment do |t, args|
    start_day = Integer(args.start_day) rescue 1
    end_day = Integer(args.end_day) rescue 1
    args.with_defaults(:daily => false)
    daily = args.daily
    args.with_defaults(:cleanup => false)
    cleanup = args.cleanup
    st_time =  "#{Time.new}"
    market = Market.find(1)
    
    
    end_day.downto(start_day) { |i| 
      market_grab_job = MarketGrabJob.new(market_id: market.id, days: i, daily: daily, cleanup: cleanup)
      p "market grab job: #{market_grab_job}"
      market_grab_job.setup
      market_grab_job.download
    }
    en_time =  "#{Time.new}"
    
    Delayed::Worker.logger.debug "start: #{st_time} => finish: #{en_time}"
    
  end
  
  desc "rsync"
  task :rsync, [:market_id,:daily,:cleanup] => :environment do |t, args|
    market_id = args.market_id
    args.with_defaults(:daily => false)
    daily = args.daily
    args.with_defaults(:cleanup => false)
    cleanup = args.cleanup
    
    MarketGrabJob.new(market_id: market_id, daily: daily, cleanup: cleanup).program(priority: -1)
      # MarketComputeJob.new(market_id: market.id).program()
      # MarketEmailSearchesJob.new(market_id: market.id, limit: 1000, weekly: true).program()
    
  end

  desc "rsync by interval"
  task :rsync_by_interval, [:market_id,:daily,:mins] => :environment do |t, args|
    Delayed::Worker.logger.debug "setting parameter by interval"
    market_id = args.market_id
    args.with_defaults(:daily => false)
    daily = args.daily
    args.with_defaults(:cleanup => false)
    cleanup = args.cleanup
    args.with_defaults(:mins => nil)
    mins = args.mins
    
    MarketGrabJob.new(market_id: market_id, daily: daily, interval: mins).program(priority: -1)
      # MarketComputeJob.new(market_id: market.id).program()
      # MarketEmailSearchesJob.new(market_id: market.id, limit: 1000, weekly: true).program()
    
  end
  
  desc "rsync by specified period"
  task :rsync_by_specified_period, [:market_id,:start_time,:end_time] => :environment do |t, args|
    Delayed::Worker.logger.debug "setting parameter by interval"
    market_id = args.market_id
    
    end_time = args.end_time
    
    # provider = "mfrmls".camelcase.constantize.new(Market.find(6))
    # provider.grab_by_specified_period(start_time,end_time)  
    MarketGrabJob.new(market_id: market_id, start_time: start_time, end_time: end_time).program(priority: -1)
      # MarketComputeJob.new(market_id: market.id).program()
      # MarketEmailSearchesJob.new(market_id: market.id, limit: 1000, weekly: true).program()
    
  end

  desc "RETS Sync"
  task :sync_by_specified_period,[:market_id,:start_time,:end_time] => :environment do |t, args|
    
    market_id = args.market_id
    
    market = Market.find(market_id)
    provider  = market.mls_code.camelcase.constantize.new(market)
 

    start_time = args.start_time
    end_time = args.end_time
    # idx = Integer(args.idx)
    # provider = "gamls_two".camelcase.constantize.new(Market.find(10))
    # provider.sync_rets(nil)
    provider.grab_by_specified_period(start_time,end_time)
    # provider.correct_by_specified_period(start_time,end_time,idx) 
    
    
  end
  
  desc "Queue daily mail"
  task :mail => :environment do |t, args|
    
    Market.all.each do |market|
      MarketEmailJob.new(market_id: market.id, limit: 1000, weekly: true).program(-1) 
      # MarketComputeJob.new(market_id: market.id).program()
      # MarketEmailSearchesJob.new(market_id: market.id, limit: 1000, weekly: true).program()
    end
  end
  
  desc "Zillow Top up"
  task :zillow_top_up => :environment do |t, args|
    
    zillow = Zillow.new
    zillow.top_up
  end
  
  
  
  
  desc "Compute weekly distributed jobs"
  task :compute, [:market_id,:start_time,:duration] => :environment do |t, args|
    begin
      start_time = args.start_time
      duration = args.duration
      market_id = Integer(args.market_id)
      time = Time.strptime(start_time, "%I:%M %P")
      duration_hour, duration_min  =  duration.split(":").map(&:to_i)
      finish_time = time + duration_hour.hours + duration_min.minutes
      
      # to be assigned priority "1", as indicating weekly job
      # the priority tends to be lower than daily and on-demand job
      MarketComputeJob.new(market_id: market_id, weekly: true, finish_time: finish_time).program(priority: 1)
      
    rescue Exception => ex
      raise ex
    end
  end
  
  desc "Repair Job"
  task :repair => :environment do |t, args|

   
    files  = Dir.glob('/repair/*')
    file_list = files.inject([]) do |result,h|

      file = {}
      file[:name] = h.match(/\/(.+\.txt)$/).captures[0]
      file[:path] = h
      result << file
    end

    provider = "har".camelcase.constantize.new(Market.find(1))
    provider.repair(file_list)
  end
  
  
  desc "Rectify Job"
  task :rectify => :environment do |t, args|

   
    files  = Dir.glob('/rectify/*')
    file_list = files.inject([]) do |result,h|

      file = {}
      file[:name] = h.match(/\/(.+\.txt)$/).captures[0]
      file[:path] = h
      result << file
    end

    provider = "gamls_two".camelcase.constantize.new(Market.find(10))
    provider.repair(file_list)
  end
  
  desc "repair geometry"
  task :repair_geometry => :environment do |t, args|


    files  = Dir.glob('/Users/apple/Downloads/repair/*')
    file_list = files.inject([]) do |result,h|

      file = {}
      file[:name] = h.match(/\/(.+\.txt)$/).captures[0]
      file[:path] = h
      result << file
    end

    provider = "ntreis".camelcase.constantize.new(Market.find(3))
    provider.repair_geometry(file_list)
  end
  
  desc "Test Import"
  task :tim => :environment do |t, args|


    files  = Dir.glob('/Users/apple/Downloads/Dallas MLS 2.13.14/Dallas MLS/Residential/*')
    file_list = files.inject([]) do |result,h|

      file = {}
      file[:name] = h.match(/\/(.+\.txt)$/).captures[0]
      file[:path] = h
      result << file
    end

    provider = "ntreis".camelcase.constantize.new(Market.find(3))
    provider.import(file_list)
  end
  
  desc "RETS Sync"
  task :sync => :environment do |t, args|
    
    market = Market.find(10)
    provider  = market.mls_code.camelcase.constantize.new(market)
    
    provider.grab_by_interval(120)
    
  end
  
  desc "RETS Sync"
  task :sync2 => :environment do |t, args|
    
    provider = "gamls_two".camelcase.constantize.new(Market.find(10))
    provider.test_by_interval(600)
    
  end

  desc "RETS Sync Test"
  task :syte => :environment do |t,args|
    market = Market.find(6)
    mfr = market.mls_code.camelcase.constantize.new(market)
    mfr.grab_by_interval(600)
  end
  
  desc "RETS Sync"
  task :fetch_special => :environment do |t, args|
    
    provider = "har".camelcase.constantize.new(Market.find(1))
    provider.fetch_special
    
    
  end

  desc "Retrieve Rets"
  task :retrieve => :environment do |t, args|
    
    provider = "ntreis".camelcase.constantize.new(Market.find(3))
    provider.retrieve_rets(nil)
  end
  
   desc "RETS metadata"
  task :metadata => :environment do |t, args|
    
    provider = "ntreis".camelcase.constantize.new(Market.find(3))
    provider.get_metadata
  end
  
  desc "Sync sale price"
  task :ssp => :environment do |t, args|
    
    provider = "gamls".camelcase.constantize.new(Market.find(5))
    provider.get_metadata()
  end
  
   desc "Geocoding job"
  task :osm_geocode,[:market_id,:start_id,:end_id,:status,:time_sold,:limit] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:start_id => nil)
      start_id = args.start_id
      args.with_defaults(:end_id => nil)
      end_id = args.end_id
      args.with_defaults(:status => nil)
      status = args.status
      args.with_defaults(:time_sold => nil)
      time_sold = Integer(args.time_sold) rescue nil
      args.with_defaults(:limit => nil)
      limit = args.limit
      # to be assigned priority "1", as indicating weekly job
      # the priority tends to be lower than daily and on-demand job
      OsmGeocodeJob.new(market_id: market_id, start_id: start_id, end_id: end_id,status: status,time_sold: time_sold,limit: limit).program(priority: 1)
      
    rescue Exception => ex
      raise ex
    end
  end
  
  desc "adhoc comp"
  task :adhoc_compute,[:market_id,:category,:status,:start_id,:end_id] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
      args.with_defaults(:status => nil)
      status = args.status
      args.with_defaults(:start_id => nil)
      start_id = args.start_id
      args.with_defaults(:end_id => nil)
      end_id = args.end_id
      

      market = Market.find(market_id)
      # ids = {}
      # ids = market.properties.where([" category = ? AND status IN (?) AND (id between ? and ?) AND  (comps_updated_at is null or comps_updated_at <= '2014-11-15 11:35:00')", category, status,  start_id, end_id]).pluck(:id)
      ids = market.properties.where([" category = ? AND status IN (?) AND (id between ? and ?) AND  (latitude is null)", category, status,  start_id, end_id]).pluck(:id) 
      # ids = market.properties.where(["status='sold' and market_id = 6 and zillow_id is  null and id >= 1924865 and id <= 1941943"]).pluck(:id)
      
      
      # 1774865 1804864
      # 1804865 1834864
      # 1834865 1864864
      # 1864865 1894864
      # 1894865 1924864
      # 1924865 1941943
      
      if ids && ids.length > 0
        market = Market.find(market_id)
        market.properties.where(id: ids).find_each do |p|
          
          p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
          
          if p.compute2(true)
            Delayed::Worker.logger.debug "computed successfully"
            
          else
            Delayed::Worker.logger.debug "compute failed"
            
          end
           
          
        end
      end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  
  desc "Silent comping"
  task :sicomp,[:market_id,:category,:start_id,:end_id] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
     
      args.with_defaults(:start_id => nil)
      start_id = args.start_id
      args.with_defaults(:end_id => nil)
      end_id = args.end_id
      

      market = Market.find(market_id)
      # ids = {}
            ids = market.properties.where(["status not in ('sold','off') AND (id between ? and ?) ",  start_id, end_id]).pluck(:id) 
      # ids = market.properties.where(["category = ? AND status IN (?) AND (id between ? and ?) AND (comps_updated_at is null)", category, status,  start_id, end_id]).pluck(:id) 
      # ids = market.properties.where(["status='sold' and market_id = 6 and zillow_id is  null and id >= 1924865 and id <= 1941943"]).pluck(:id)
      
      
      # 1774865 1804864
      # 1804865 1834864
      # 1834865 1864864
      # 1864865 1894864
      # 1894865 1924864
      # 1924865 1941943
      
      if ids && ids.length > 0
        market = Market.find(market_id)
        market.properties.where(id: ids).find_each do |p|
          
          p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
          
          if p.compute2(true)
            Delayed::Worker.logger.debug "computed successfully"
            
          else
            Delayed::Worker.logger.debug "compute failed"
            
          end
           
          
        end
      end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  desc "new silent comp"
  task :sic,[:market_id,:category,:status,:start_id,:end_id] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
      args.with_defaults(:status => nil)
      status = args.status
      args.with_defaults(:start_id => nil)
      start_id = args.start_id
      args.with_defaults(:end_id => nil)
      end_id = args.end_id
      

      market = Market.find(market_id)
      # ids = {}
      
      # ids = market.properties.joins("INNER JOIN proformas ON properties.id = proformas.property_id and ifnull(proformas.user_id,0) = 0").where("not (ifnull(comparables_avm_rental,-1)  = -1) and ifnull(rental_output_cap_rate,-1) = -1 and category = ? and status = ? AND (properties.id between ? and ?)) ",category,status,start_id,end_id).pluck("properties.id")
      # ids = market.properties.where(["category = ? AND status IN (?) AND (id between ? and ?)  and (zillow_updated_at > '2014-08-08')", category, status,  start_id, end_id]).pluck(:id) 

     
      
      ids = '5827473,5821862,5975056,6154169,6158951,6276625,6306517,6324554,4491856,6382563,5929237,5109875,6426623,6430955,6431998,6432013,6432507,6433336,6437446,6027620,6269819,6374086,4500371,6408669,6415833,6418466,6438784,6429263,6431057,6432633'.split(",")
       # ids = '5827473,5821862,5975056,6154169,6276625,6324554,4491856,6133477,6382563,5929237,5109875,6426623,6430955,6431998,6432013,6432507,6437446,6027620,6215289,6269819,6306686,6372732,6374086,4500371,6408669,6415833,6418466,6438784,6429263,6432633'.split(",")
      if ids && ids.length > 0
        market = Market.find(market_id)
        market.properties.where(id: ids).order("id desc").find_each do |p|
          
          p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
          
          if p.compute(true)
            Delayed::Worker.logger.debug "computed successfully"
            
          else
            Delayed::Worker.logger.debug "compute failed"
            
          end
           
          
        end
      end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  
  desc "zillow geocode"
  task :zg,[:market_id,:category,:status,:start_id,:end_id] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
      args.with_defaults(:status => nil)
      status = args.status
      args.with_defaults(:start_id => nil)
      start_id = args.start_id
      args.with_defaults(:end_id => nil)
      end_id = args.end_id
      

      market = Market.find(market_id)
      # ids = {}
      ids = market.properties.where(["category = ? AND status IN (?) AND (id between ? and ?) AND (zillow_id is null)", category, status,  start_id, end_id]).pluck(:id) 
      # ids = market.properties.where(["status='sold' and market_id = 6 and zillow_id is  null and id >= 1924865 and id <= 1941943"]).pluck(:id)
      
      
      # 1774865 1804864
      # 1804865 1834864
      # 1834865 1864864
      # 1864865 1894864
      # 1894865 1924864
      # 1924865 1941943
      
      if ids && ids.length > 0
        market = Market.find(market_id)
        market.properties.where(id: ids).find_each do |p|
          
          p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
          
          if p.compute3(true)
            Delayed::Worker.logger.debug "computed successfully"
            
          else
            Delayed::Worker.logger.debug "compute failed"
            
          end
           
          
        end
      end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  desc "geocode"
  task :gc,[:market_id,:category,:status,:start_id,:end_id] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
      args.with_defaults(:status => nil)
      status = args.status
      args.with_defaults(:start_id => nil)
      start_id = args.start_id
      args.with_defaults(:end_id => nil)
      end_id = args.end_id
      

      market = Market.find(market_id)
      # ids = {}
      ids = market.properties.where(["category = ? AND status IN (?) AND (id between ? and ?) AND (zillow_id is null)", category, status,  start_id, end_id]).pluck(:id) 
      # ids = market.properties.where(["status='sold' and market_id = 6 and zillow_id is  null and id >= 1924865 and id <= 1941943"]).pluck(:id)
      
      
      # 1774865 1804864
      # 1804865 1834864
      # 1834865 1864864
      # 1864865 1894864
      # 1894865 1924864
      # 1924865 1941943
      
      if ids && ids.length > 0
        market = Market.find(market_id)
        market.properties.where(id: ids).find_each do |p|
          
          p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
          
          if p.compute4(true)
            Delayed::Worker.logger.debug "computed successfully"
            
          else
            Delayed::Worker.logger.debug "compute failed"
            
          end
           
          
        end
      end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end


  desc "zillow"
  task :zillow => :environment do |t, args|
    begin
      export_file = 'file3.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "staffs"

      #-------------------------------------

      workbook = Roo::Excel.new('tn.xls')

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      ary = []
      ((workbook.first_row + 1)..workbook.last_row).each do |row|
        hsh = {}
        street_number = workbook.row(row)[0].to_i.to_s
        street_name = workbook.row(row)[1]
        city = workbook.row(row)[2]
        state = workbook.row(row)[3]
        zip_code = workbook.row(row)[4].to_i.to_s
        puts "#{street_number} #{street_name}, #{city}, #{state} #{zip_code}"

        p = Property.new(street_number: street_number, street_name: street_name, city: city, state: state, zip_code: zip_code)

        if p.compute5(true)
          Delayed::Worker.logger.debug "computed successfully"          
        else
          Delayed::Worker.logger.debug "compute failed"          
        end

        hsh["street_number"] = p.street_number
        hsh["street_name"] = p.street_name
        hsh["city"] = p.city
        hsh["state"] = p.state
        hsh["zip_code"] = p.zip_code
        hsh["zillow_sale_estimate"] = p.zillow_sale_estimate
        hsh["zillow_rental_estimate"] = p.zillow_rental_estimate
        hsh["elementary_school"] = p.elementary_school
        hsh["elementary_school_rating"] = p.elementary_school_rating
        hsh["middle_school"] = p.middle_school
        hsh["middle_school_rating"] = p.middle_school_rating
        hsh["high_school"] = p.high_school
        hsh["high_school_rating"] = p.high_school_rating

        # ary << hsh

        columns = %w{street_number street_name city state zip_code zillow_sale_estimate zillow_rental_estimate elementary_school elementary_school_rating middle_school middle_school_rating high_school high_school_rating}
        
        columns.each do |d|
          sheet1.row(i).push hsh[d] 
        end
        
        # break if i >= 20
        i += 1
      end

      book.write export_file 
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  
  desc "zillow"
  task :zpid => :environment do |t, args|
    begin
      # sts = ['1916 EXCALIBUR DR, Orlando, FL 32822','903 PRINCETON DRIVE, Clermont, FL 34711','903 PRINCETON DRIVE, Clermont, FL 34711','889 WOODGATE TRL, Longwood, FL 32750','2496 PINE CHASE CIR, Saint Cloud, FL 34769','1878 TILBURG AVE, Deltona, FL 32725','9906 KENDAL DR, Orlando, FL 32817','2967 RED OAK DR, Kissimmee, FL 34744','618 RED MULBERRY DR, Deltona, FL 32725','51 TIMBERCREEK PINES CIR, Winter Garden, FL 34787','11020 SYLVAN POND CIR, Orlando, FL 32825','1470 OLD MILL DR, Deltona, FL 32725','1470 OLD MILL DR, Deltona, FL 32725','5450 COUNTY FAIR CT, Oviedo, FL 32765','752 SWAYING PALM DR, Apopka, FL 32712','1923 SEATONHAM ST, Winter Garden, FL 34787','1201 CARSON AVE, Kissimmee, FL 34744','2210 HEATHEROAK DR, Apopka, FL 32703','4307 STONEFIELD DR, Orlando, FL 32826','206 YALE DR, Sanford, FL 32771','605 S DILLINGHAM AVE, Kissimmee, FL 34741','794 PRINCETON DR, Clermont, FL 34711','794 PRINCETON DR, Clermont, FL 34711','1922 ALGONQUIN AVE, Deltona, FL 32725','904 GRAND CAYMAN CT, Orlando, FL 32835','16 N NORMANDALE AVE, Orlando, FL 32835','5457 ENDICOTT PL, Oviedo, FL 32765','5457 ENDICOTT PL, Oviedo, FL 32765','2972 OAKTREE DR, Kissimmee, FL 34744','1040 ROYAL MARQUIS CIR, Ocoee, FL 34761','555 CHAUTAUQUA DR, Mount Dora, FL 32757','306 COUNTRY LANDING BLVD, Apopka, FL 32703','3420 BUFFAM PL, Casselberry, FL 32707','1642 PORTCASTLE CIR, Winter Garden, FL 34787','1409 AUBURN GREEN LOOP, Winter Park, FL 32792','941 SOUTHRIDGE TRL, Altamonte Springs, FL 32714','2939 TWIN OAKS DR, Kissimmee, FL 34744','7212 ALSTON CT, Orlando, FL 32835','1363 WOODBINE AVE, Deltona, FL 32725','130 STEPHEN AVE, Oviedo, FL 32765','518 KAYS LANDING DR, Sanford, FL 32771','4810 WATERSIDE POINTE CIR, Orlando, FL 32829','2227 CERBERUS DR, Apopka, FL 32712','2227 CERBERUS DR, Apopka, FL 32712','2931 BLAKELY DR, Orlando, FL 32835','1457 SAXON BLVD, Deltona, FL 32725','1043 BURNETT ST, Oviedo, FL 32765','334 OAK PARK PL, Casselberry, FL 32707','2052 CROSSHAIR CIR, Orlando, FL 32837','2052 CROSSHAIR CIR, Orlando, FL 32837','611 S ANCHOR DR, Deltona, FL 32725','1225 UPSTREET CT, Orlando, FL 32837','628 NEUMANN VILLAGE CT, Ocoee, FL 34761','2598 SHEILA DR, Apopka, FL 32712','2598 SHEILA DR, Apopka, FL 32712','3101 CRYSTAL CREEK BLVD, Orlando, FL 32837','285 COVENT GARDENS PL, Deltona, FL 32725','198 VELVETEEN PL, Chuluota, FL 32766','53 COLUMBINE TRL, Debary, FL 32713','2125 KAYLAS CT, Orlando, FL 32817','3601 GRUBBS ST, Mount Dora, FL 32757','3601 GRUBBS ST, Mount Dora, FL 32757','685 ASHLAND CT, Deltona, FL 32725','9135 WOODBREEZE BLVD, Windermere, FL 34786','1366 JECENIA BLOSSOM DR, Apopka, FL 32712','431 GLEN ABBEY LN, Debary, FL 32713','1166 NAOMI LN, Sanford, FL 32773','420 E CITRUS ST, Altamonte Springs, FL 32701','1214 STILLWATER AVE, Deltona, FL 32725','1056 COASTAL CIR, Ocoee, FL 34761','724 HAVEN OAK CT, Apopka, FL 32703','2401 PALMETTO RIDGE CIR, Apopka, FL 32712','617 PICKFAIR TER, Lake Mary, FL 32746','635 NICOLE MARIE ST, Apopka, FL 32712','2584 VINEYARD CIR, Sanford, FL 32771','1307 LYDIA DR, Deltona, FL 32725','203 N CERVIDAE DR, Apopka, FL 32703','11233 CARABELEE CIR, Orlando, FL 32825','2442 OAK HOLLOW DR, Kissimmee, FL 34744','1931 RIVER PARK BLVD, Orlando, FL 32817','628 WHITEMARSH AVENUE, Deltona, FL 32725','540 BIRCH COURT, Altamonte Springs, FL 32714','110 ARDSDALE COURT, Longwood, FL 32750','2325 FITZPATRICK TERRACE, Deltona, FL 32725','1305 HEATHER LAKE DR, Orlando, FL 32824','2212 EH POUNDS DR, Ocoee, FL 34761','1978 S OLD MILL DR, Deltona, FL 32725','4618 TIFFANY WOODS CIR, Oviedo, FL 32765','1939 PERIDOT CIR, Kissimmee, FL 34743','10314 TARABY CT, Orlando, FL 32817','1555 APRIL AVE, Deltona, FL 32725','4422 WATERSIDE POINTE CIR, Orlando, FL 32829','9444 TELFER RUN, Orlando, FL 32817','104 LAKESIDE CIR, Sanford, FL 32773','3198 FAIRFIELD DR, Kissimmee, FL 34743','2225 WHISPERING MAPLE DR, Orlando, FL 32837','4550 SIMMONS RD, Orlando, FL 32812','4550 SIMMONS RD, Orlando, FL 32812','211 BRIGHTVIEW DR, Lake Mary, FL 32746','3137 HOLLAND DR, Orlando, FL 32825','905 ABBY TER, Deltona, FL 32725','905 ABBY TER, Deltona, FL 32725','1742 DELAFIELD DR, Winter Garden, FL 34787','1215 HONEY RD, Apopka, FL 32712','1215 HONEY RD, Apopka, FL 32712','2625 ALAMOSA PL, Lake Mary, FL 32746','1228 LAKE LUCERNE CIR, Winter Springs, FL 32708','381 SAN SEBASTIAN PRADO, Altamonte Springs, FL 32714','4190 BEAR GULLY ROAD, Winter Park, FL 32792','2088 KEEL WAY, Kissimmee, FL 34744','1744 VALE DR, Clermont, FL 34711','2516 ASCOT COURT, Kissimmee, FL 34744','871 N ATMORE CIRCLE, Deltona, FL 32725','1726 GULF WINDS COURT, Apopka, FL 32712','11739 CLAIR PLACE, Clermont, FL 34711','1440 BADGER TERRACE, Deltona, FL 32725','225 GEMWOOD COURT, Kissimmee, FL 34743','263 BLACKWATER PLACE, Longwood, FL 32750','2456 PINE CHASE CIRCLE, Saint Cloud, FL 34769','704 CASTILLO PLACE, Saint Cloud, FL 34769','807 S SANFORD AVENUE, Sanford, FL 32771','12712 WOODBURY GLEN DRIVE, Orlando, FL 32828','3425 SUGAR MILL ROAD, Kissimmee, FL 34741','508 LUCILE WAY, Orlando, FL 32835','30230 HUNSTAUNTON DRIVE, Sorrento, FL 32776','1201 AZORA DRIVE, Deltona, FL 32725','1634 E NORMANDY BOULEVARD, Deltona, FL 32725','698 N APACHE CIRCLE, Deltona, FL 32725','879 ABBY TERRACE, Deltona, FL 32725','301 HERONWOOD CIRCLE, Deltona, FL 32725','1022 W HARBOR DRIVE, Deltona, FL 32725','1430 CORONET DRIVE, Deltona, FL 32725','9236 PALM TREE DRIVE, Windermere, FL 34786','653 BROOKFIELD LOOP, Lake Mary, FL 32746','521 HOWARD AVENUE, Altamonte Springs, FL 32701','1671 ANORADA BOULEVARD, Kissimmee, FL 34744','3409 WINDY WOOD DRIVE, Orlando, FL 32812','7900 DUNSTABLE CIRCLE, Orlando, FL 32817','8011 VIA BONITA STREET, Sanford, FL 32771','12511 EL VIENTO ROAD, Clermont, FL 34711','452 PALM SPRINGS DRIVE, Longwood, FL 32750','1020 FRANCIS STREET, Altamonte Springs, FL 32701','1033 SUMMER LAKES DRIVE, Orlando, FL 32835','1418 BEAR LAKE ROAD, Apopka, FL 32703','14610 MUSKET FIRE LANE, Orlando, FL 32837','332 RINGWOOD CIRCLE, Winter Springs, FL 32708','941 BUCKSAW PLACE, Longwood, FL 32750','111 HAMLIN T LANE, Altamonte Springs, FL 32714','3515 CURTIS DRIVE, Apopka, FL 32703','691 SHADY COURT, Altamonte Springs, FL 32701','646 N DIVISION STREET, Oviedo, FL 32765','3379 MONIKA CIRCLE, Orlando, FL 32812','1498 STONE TRAIL, Enterprise, FL 32725','900 DOLPHIN AVENUE, Kissimmee, FL 34744','1564 LAWNDALE CIRCLE, Winter Park, FL 32792','1582 GOLFSIDE VILLAGE BOULEVARD, Apopka, FL 32712','1460 LAKE FRANCIS DRIVE, Apopka, FL 32712','9440 W PAULINDA AVENUE, Apopka, FL 32703','120 WINDSONG COURT, Lake Mary, FL 32746','2158 SUSSEX ROAD, Winter Park, FL 32792','125 HOLLOW BRANCH ROAD, Apopka, FL 32703','584 SAXON BOULEVARD, Deltona, FL 32725','583 MENDOZA DRIVE, Orlando, FL 32825','1002 GROVE AVENUE, Altamonte Springs, FL 32701','11648 ENGLISH STREET, Orlando, FL 32817','904 LOGENBERRY TRAIL, Winter Springs, FL 32708','1710 FINLEY AVENUE, Apopka, FL 32703','2650 GOLFRIDGE LANE, Apopka, FL 32712','102 WINDMILL WAY, Longwood, FL 32750','2003 LAKE PICKETT ROAD, Orlando, FL 32826','104 HALLMARK COURT, Lake Mary, FL 32746','117 MORNING GLORY DRIVE, Lake Mary, FL 32746','11719 LAKE CLAIR CIRCLE, Clermont, FL 34711','1961 BEAR VIEW DRIVE, Apopka, FL 32703','111 HATTAWAY DRIVE, Altamonte Springs, FL 32701','14533 BRAY ROAD, Orlando, FL 32832','1344 N MARCY DRIVE, Longwood, FL 32750','952 MARCH HARE COURT, Winter Springs, FL 32708','1115 W WINGED FOOT CIRCLE, Winter Springs, FL 32708','1359 N MARCY DRIVE, Longwood, FL 32750','3065 NICHOLSON DRIVE, Winter Park, FL 32792','1207 ROYAL OAK DRIVE, Winter Springs, FL 32708','50 SWEETWATER CREEK CIRCLE, Oviedo, FL 32765','3237 BLACK PINE AVENUE, Winter Park, FL 32792','3915 STONEHAVEN ROAD, Orlando, FL 32817','2986 LOWERY DRIVE, Oviedo, FL 32765','4713 DEER ROAD, Orlando, FL 32812','127 GUADALAJARA DRIVE, Kissimmee, FL 34743','1321 ARDEN, Longwood, FL 32750','32 BASS LAKE, Debary, FL 32713','1085 DEAN STREET, Saint Cloud, FL 34771','908 RED OAK COURT, Winter Springs, FL 32708','8049 SWEETGUM LOOP, Orlando, FL 32835','8658 SAVORY DRIVE, Orlando, FL 32825','32 S LAKE CORTEZ DRIVE, Apopka, FL 32703','249 E LAKE AVENUE, Longwood, FL 32750','1559 SUGARWOOD CIRCLE, Winter Park, FL 32792','4692 STURBRIDGE CIRCLE, Orlando, FL 32812','613 HEATHER BRITE CIRCLE, Apopka, FL 32712','1317 VIA VILLA NOVA, Winter Springs, FL 32708','807 TOLEDO DRIVE, Altamonte Springs, FL 32714','376 GOLDSTONE COURT, Lake Mary, FL 32746','1663 SPICEWOOD LANE, Casselberry, FL 32707','7624 WAUNATTA COURT, Winter Park, FL 32792','7682 GLENMOOR LANE, Winter Park, FL 32792','837 HAULOVER DRIVE, Altamonte Springs, FL 32714','23958 OAK TREE DRIVE, Sorrento, FL 32776','1928 PROCTOR AVENUE, Orlando, FL 32817','296 SANDPIPER DRIVE, Casselberry, FL 32707','10146 BRIDLEWOOD AVENUE, Orlando, FL 32825','2349 PINE STREET, Oviedo, FL 32765','126 SACKETT ROAD, Debary, FL 32713','1509 JAGUAR CIRCLE, Apopka, FL 32712','23 N WINTER PARK DRIVE, Casselberry, FL 32707','630 TOMLINSON TERRACE, Lake Mary, FL 32746','1602 AUGUSTA WAY, Casselberry, FL 32707','2805 ORANOLE WAY, Apopka, FL 32703','411 W CITRUS STREET, Altamonte Springs, FL 32714','1867 SHADYHILL TERRACE, Winter Park, FL 32792','1138 DAPPLED ELM LANE, Winter Springs, FL 32708','680 ROYAL PALM DRIVE, Kissimmee, FL 34743','1232 WOODRIDGE COURT, Altamonte Springs, FL 32714','2982 LOWERY DRIVE, Oviedo, FL 32765','308 HAWAII WOODS COURT, Orlando, FL 32824','325 LOCH LOMOND AVENUE, Longwood, FL 32750','803 CALABRIA DRIVE, Altamonte Springs, FL 32714','670 GEORGIA AVENUE, Longwood, FL 32750','178 SOUTHCOT DRIVE, Casselberry, FL 32707','816 PINE MEADOWS ROAD, Orlando, FL 32825','3603 GATEWOOD DRIVE, Orlando, FL 32812','904 LITTLE BEND ROAD, Altamonte Springs, FL 32714','109 CARDAMON DRIVE, Orlando, FL 32825','3001 NICHOLSON DRIVE, Winter Park, FL 32792','114 MADEIRA AVENUE, Orlando, FL 32825','2304 JUSTY WAY, Orlando, FL 32817','113 SHOMATE DRIVE, Longwood, FL 32750','1252 HIBISCUS LANE, Apopka, FL 32703','1514 MEADOWLARK STREET, Longwood, FL 32750','549 OAKHURST STREET, Altamonte Springs, FL 32701','714 E LACY CIRCLE, Deltona, FL 32725','510 WATER WAY COURT, Ocoee, FL 34761','1024 NIN STREET, Orlando, FL 32835','830 N JERICO DRIVE, Casselberry, FL 32707','1555 JAN LAN BOULEVARD, Saint Cloud, FL 34772','2982 HEATHERSIDE AVENUE, Orlando, FL 32822','102 RAPSCALLION DRIVE, Orlando, FL 32828','8056 ELSEE DRIVE, Orlando, FL 32822','3311 HEARTWOOD AVENUE, Winter Park, FL 32792','1452 ACORN COURT, Kissimmee, FL 34744','401 NAVARRE WAY, Altamonte Springs, FL 32714','460 PALM SPRINGS DRIVE, Longwood, FL 32750','1837 FAYETTEVILLE AVENUE, Deltona, FL 32725','1662 TREMONT LANE, Winter Park, FL 32792','648 RIDGEWOOD STREET, Altamonte Springs, FL 32701','1139 N OLD MILL DRIVE, Deltona, FL 32725','562 BITTERWOOD COURT, Kissimmee, FL 34743','401 GLEN ABBEY LANE, Debary, FL 32713','1179 E NORMANDY BOULEVARD, Deltona, FL 32725','625 HAYMAN COURT, Debary, FL 32713','7100 GREEN NEEDLE DRIVE, Winter Park, FL 32792','112 MISTYWOOD COURT, Kissimmee, FL 34743','2722 RAMSEY DRIVE, Apopka, FL 32703','1357 PARK DRIVE, Casselberry, FL 32707','532 FLORAL DRIVE, Kissimmee, FL 34743','118 DONNA CIRCLE, Sanford, FL 32773','400 W WASHINGTON STREET, Minneola, FL 34715','4907 RED BAY DRIVE, Orlando, FL 32829','1132 MADURA DRIVE, Deltona, FL 32725','744 MEADOWLARK COURT, Longwood, FL 32750','201 EILEEN AVENUE, Altamonte Springs, FL 32714','601 ANTOINETTE STREET, Deltona, FL 32725','940 MARQUETTE COURT, Deltona, FL 32725','1319 HOLLY SPRINGS CIRCLE, Orlando, FL 32825','1432 OUTER COURT, Kissimmee, FL 34744','553 CLEMSON DRIVE, Altamonte Springs, FL 32714','2552 CANTERCLUB TRAIL, Apopka, FL 32712','189 W 3RD STREET, Chuluota, FL 32766','1412 WESTDALE AVENUE, Winter Park, FL 32792','1568 RANDOLPH STREET, Deltona, FL 32725','3720 VERAVAL AVENUE, Orlando, FL 32817','211 DOSTER DRIVE, Casselberry, FL 32707','1004 TOMES COURT, Orlando, FL 32825','1014 BELVEDERE DRIVE, Deltona, FL 32725','336 CANOE TRAIL LANE, Orlando, FL 32825','424 LA PAZ DRIVE, Kissimmee, FL 34743','212 BAYOU VISTA STREET, Debary, FL 32713','327 E STORY ROAD, Winter Garden, FL 34787','517 MOCKINGBIRD LANE, Altamonte Springs, FL 32714','212 DELESPINE DRIVE, Debary, FL 32713','851 WOODGATE TRAIL, Longwood, FL 32750','2057 AMBERGRIS, Orlando, FL 32822','7724 PINE HAWK LANE, Orlando, FL 32822','549 NARDELLO DRIVE, Deltona, FL 32725','2313 MILLBANK DRIVE, Orlando, FL 32837','301 LATONIA STREET, Kissimmee, FL 34741','1109 S ELM AVENUE, Sanford, FL 32771','8103 BRITT DRIVE, Orlando, FL 32822','31051 DEAL DRIVE, Sorrento, FL 32776','138 W CEDARWOOD CIRCLE, Kissimmee, FL 34743','1564 SUNBIRD TERRACE, Deltona, FL 32725','2617 CORAL AVENUE, Kissimmee, FL 34741','409 MONROE, Apopka, FL 32703','1519 HENRY STREET, Kissimmee, FL 34741','1860 SAXON BOULEVARD, Deltona, FL 32725','1384 RURAL HALL STREET, Deltona, FL 32725','7628 RUTHWIND COURT, Orlando, FL 32822','1640 FRUITLAND DRIVE, Deltona, FL 32725','7421 HOUSTON E COURT, Winter Park, FL 32792','1859 E COOPER DRIVE, Deltona, FL 32725','655 DAVID STREET, Winter Springs, FL 32708','805 ASPENWOOD CIRCLE, Kissimmee, FL 34743','1700 DIXIE BELLE DRIVE, Orlando, FL 32812','1169 S BRICKELL DRIVE, Deltona, FL 32725','445 ALEXANDER AVENUE, Deltona, FL 32725','1743 LONGLEAF DRIVE, Saint Cloud, FL 34769','13978 IVORY GARDENIA AVENUE, Windermere, FL 34786','12962 GARRIDAN AVENUE, Windermere, FL 34786','13808 CEPHEUS DRIVE, Orlando, FL 32828','313 MIRASOL LANE, Orlando, FL 32828','2104 LILIPETAL COURT, Sanford, FL 32771','2023 CHICKADEE DRIVE, Apopka, FL 32703','1706 ALAMBRA CIRCLE, Apopka, FL 32703','11317 SCENIC VISTA DRIVE, Clermont, FL 34711','10971 INSIDE LOOP, Orlando, FL 32825','2014 PIRIE PLACE, Saint Cloud, FL 34769','24144 PLYMOUTH HOLLOW CIRCLE, Sorrento, FL 32776','2342 AVELLINO AVENUE, Saint Cloud, FL 34771','2978 CAMINO REAL S DRIVE, Kissimmee, FL 34744','3496 MEADOW BREEZE LOOP, Ocoee, FL 34761','1061 FLAGSTONE COURT, Apopka, FL 32703','1849 DUNN COVE DRIVE, Apopka, FL 32703','1118 FOX TRAIL AVENUE, Minneola, FL 34715','5236 LEMON TWIST LANE, Windermere, FL 34786','4980 SPIRAL WAY, Saint Cloud, FL 34771','2780 PYTHAGORAS CIRCLE, Ocoee, FL 34761','9882 LENOX STREET, Clermont, FL 34711','1839 ALAMBRA CIRCLE, Apopka, FL 32703','10334 CALLE DE FLORES DRIVE, Clermont, FL 34711','2038 PIRIE PLACE, Saint Cloud, FL 34769','354 MIRASOL LANE, Orlando, FL 32828','3172 ROLLING HILLS LANE, Apopka, FL 32712','12938 GARRIDAN AVENUE, Windermere, FL 34786','4770 SALAMANDER STREET, Saint Cloud, FL 34772','2829 LAFAYETTE TRACE DRIVE, Saint Cloud, FL 34772','2177 WHITE BIRD WAY, Apopka, FL 32703','1941 CANDLENUT CIRCLE, Apopka, FL 32712','1427 BARN OWL LOOP, Sanford, FL 32773','431 SHIRLEY DRIVE, Apopka, FL 32712','520 CODY ALLEN COURT, Saint Cloud, FL 34769','3252 TOSCANA DRIVE, Saint Cloud, FL 34772','2728 STANWOOD DRIVE, Kissimmee, FL 34743','24031 PLYMOUTH HOLLOW CIRCLE, Sorrento, FL 32776','2773 SHEARWATER STREET, Clermont, FL 34711','663 NEWHALL LANE, Debary, FL 32713','671 NEWHALL LANE, Debary, FL 32713','3036 SANDSTONE CIRCLE, Saint Cloud, FL 34772','1816 DUNN COVE DRIVE, Apopka, FL 32703','622 ALABAMA WOODS LANE, Orlando, FL 32824','3705 RYEGRASS STREET, Clermont, FL 34714','2720 DOLFINO COURT, Saint Cloud, FL 34772','26250 BILTMORE STREET, Sorrento, FL 32776','850 KAZAROS CIRCLE, Ocoee, FL 34761','16 BURGUST STREET, Apopka, FL 32712','591 NEWHALL LANE, Debary, FL 32713','4852 GRAND VISTA LANE, Saint Cloud, FL 34771','659 NEWHALL LANE, Debary, FL 32713','10965 INSIDE LOOP, Orlando, FL 32825','16338 SAINT AUGUSTINE STREET, Clermont, FL 34714','16555 CORNER LAKE DRIVE, Orlando, FL 32820','4899 MEADOW DRIVE, Saint Cloud, FL 34772','2680 DOLFINO COURT, Saint Cloud, FL 34772','2335 AVELLINO AVENUE, Saint Cloud, FL 34771','1331 CREEKVIEW COURT, Saint Cloud, FL 34772','2765 LAKESHORE BOULEVARD, Saint Cloud, FL 34769','599 NEWHALL LANE, Debary, FL 32713','2118 PORTCASTLE CIRCLE, Winter Garden, FL 34787','814 WINDROSE DRIVE, Orlando, FL 32824','1859 RIBBON FALLS PARKWAY, Orlando, FL 32824','277 MAGICAL WAY, Kissimmee, FL 34744','124 COLONIAL PINE LANE, Minneola, FL 34715','435 ROSALIA DRIVE, Sanford, FL 32771','2759 BARCLAY LANE, Kissimmee, FL 34743','2448 VINEYARD CIRCLE, Sanford, FL 32771','3820 RYEGRASS STREET, Clermont, FL 34714','4903 STONE ACRES CIRCLE, Saint Cloud, FL 34771','579 NEWHALL LANE, Debary, FL 32713','1781 SCRUB JAY ROAD, Apopka, FL 32703','2640 JUDGE LOOP, Kissimmee, FL 34743','3014 SANDSTONE CIRCLE, Saint Cloud, FL 34772','2061 BIG BUCK DRIVE, Saint Cloud, FL 34772','672 NEWHALL LANE, Debary, FL 32713','1024 FLAGSTONE COURT, Apopka, FL 32703','2932 BONITA CIRCLE, Kissimmee, FL 34744','915 W STORY ROAD, Winter Garden, FL 34787','415 ROSALIA DRIVE, Sanford, FL 32771','10052 PENTRIDGE ROAD, Orlando, FL 32829','910 HADDOCK DRIVE, Clermont, FL 34711','3847 SPIRITED CIRCLE, Saint Cloud, FL 34772','3311 PEKIN STREET, Saint Cloud, FL 34772','3926 CEDAR HAMMOCK TRAIL, Saint Cloud, FL 34772','11322 WISHING WELL LANE, Clermont, FL 34711','4854 TERRA SOLE PLACE, Saint Cloud, FL 34771','3523 HARLEQUIN DRIVE, Saint Cloud, FL 34772','1352 S PRAIRIE CIRCLE, Deltona, FL 32725','223 CLARA VISTA STREET, Debary, FL 32713','219 CLARA VISTA STREET, Debary, FL 32713','217 CLARA VISTA STREET, Debary, FL 32713','1378 FREEPORT DRIVE, Deltona, FL 32725','16723 CITRUS PARKWAY, Clermont, FL 34714','1575 PIER STREET, Clermont, FL 34711','2954 MARBELLA DRIVE, Kissimmee, FL 34744','1705 SOUTHERN OAK LOOP, Minneola, FL 34715','1954 S OLD MILL DRIVE, Deltona, FL 32725','3258 OLIVERA WAY, Saint Cloud, FL 34772','16350 SAINT AUGUSTINE STREET, Clermont, FL 34714','3828 RYEGRASS STREET, Clermont, FL 34714','952 GRAPEWOOD STREET, Deltona, FL 32725','1713 PORTCASTLE CIRCLE, Winter Garden, FL 34787','10214 YONAOMI CIRCLE, Clermont, FL 34711','16736 CITRUS PARKWAY, Clermont, FL 34714','25006 CHIPSHOT COURT, Sorrento, FL 32776','3620 HOLDEN DRIVE, Saint Cloud, FL 34772','221 CLARA VISTA STREET, Debary, FL 32713','142 VERONA ROAD, Debary, FL 32713','2857 SHELBURNE WAY, Saint Cloud, FL 34772','1265 HERNDON AVENUE, Deltona, FL 32725','850 CAMPELLO STREET, Altamonte Springs, FL 32701','2150 TYBEE ROAD, Saint Cloud, FL 34769','201 MAGICAL WAY, Kissimmee, FL 34744','3053 BOXELDER COURT, Deltona, FL 32725','2539 POINSETTA AVENUE, Sanford, FL 32773','2535 POINSETTA AVENUE, Sanford, FL 32773','2148 LILIPETAL COURT, Sanford, FL 32771','2037 KELSO AVENUE, Deltona, FL 32725','421 BELLA ROSA CIRCLE, Sanford, FL 32771','349 FAIRFIELD DRIVE, Sanford, FL 32771','3144 LAUREN COURT, Deltona, FL 32725','285 W MAGNOLIA STREET, Clermont, FL 34711','959 SPRINGS LANDING DRIVE, Deltona, FL 32725','1195 W HANCOCK DRIVE, Deltona, FL 32725','1388 DANDELION DRIVE, Deltona, FL 32725','1034 BELVEDERE DRIVE, Deltona, FL 32725','1734 JOYNER DRIVE, Deltona, FL 32725','2691 PROVIDENCE BOULEVARD, Deltona, FL 32725','443 FAIRFIELD DRIVE, Sanford, FL 32771','13927 ORANGE CREST DRIVE, Windermere, FL 34786','6344 EARTHGOLD DRIVE, Windermere, FL 34786','11373 ARBORSIDE BEND WAY, Windermere, FL 34786','448 SOTHEBY WAY, Debary, FL 32713','13718 COLINA COURT, Clermont, FL 34711','4911 CORTO DRIVE, Orlando, FL 32837','4062 SUNNY DAY WAY, Kissimmee, FL 34744','151 VELVETEEN PLACE, Chuluota, FL 32766','13248 OLD DOCK ROAD, Orlando, FL 32828','4598 CONROY CLUB DRIVE, Orlando, FL 32835','1861 PARKGLEN CIRCLE, Apopka, FL 32712','1246 GREEN VISTA CIRCLE, Apopka, FL 32712','4889 FELLS COVE AVENUE, Kissimmee, FL 34744','3337 BENNINGTON COURT, Winter Park, FL 32792','1115 PRINCETON DRIVE, Clermont, FL 34711','209 WEKIVA POINTE CIRCLE, Apopka, FL 32712','427 LAKE AMBERLEIGH DRIVE, Winter Garden, FL 34787','2033 CORNER SCHOOL DRIVE, Orlando, FL 32820','2719 GRAPEVINE CREST, Ocoee, FL 34761','10838 VINEYARD COURT, Clermont, FL 34711','2817 OCONNELL DRIVE, Kissimmee, FL 34741','476 MISTY OAKS RUN, Casselberry, FL 32707','11038 WINDCHIME CIRCLE, Clermont, FL 34711','199 SAVANNAH PARK LOOP, Casselberry, FL 32707','2555 ROLLINS AVENUE, Clermont, FL 34711','16944 CORNER BAY COURT, Orlando, FL 32820','3205 WATERBRIDGE COURT, Kissimmee, FL 34744','12649 CYNTHIA LANE, Clermont, FL 34715','16548 ARROWHEAD TRAIL, Clermont, FL 34711','13721 COLINA COURT, Clermont, FL 34711','3529 MAPLE RIDGE LOOP, Kissimmee, FL 34741','14444 ESTRELLA DRIVE, Orlando, FL 32837','428 DOMINISH ESTATES DRIVE, Apopka, FL 32712','238 TIMBERCREEK PINES CIRCLE, Winter Garden, FL 34787','221 TIMBERCREEK PINES CIRCLE, Winter Garden, FL 34787','727 HACIENDA CIRCLE, Kissimmee, FL 34741','5164 FILMORE PLACE, Sanford, FL 32773','12014 PRAIRIE MEADOWS DRIVE, Orlando, FL 32837','1679 ROSEWOOD DRIVE, Clermont, FL 34711','1701 MELANIE DRIVE, Orlando, FL 32825','10082 SHADOW CREEK DRIVE, Orlando, FL 32832','18050 THORNHILL GRAND CIRCLE, Orlando, FL 32820','1842 WINGED ELM PLACE, Winter Garden, FL 34787','210 VELVETEEN PLACE, Chuluota, FL 32766','9510 MEADOW CREST LANE, Clermont, FL 34711','2204 JULIANNA COURT, Saint Cloud, FL 34769','12545 LAKE DENISE BOULEVARD, Clermont, FL 34711','1828 VALE DRIVE, Clermont, FL 34711','6043 FROGGATT STREET, Orlando, FL 32835','146 CROWN COLONY WAY, Sanford, FL 32771','360 MISTY OAKS RUN, Casselberry, FL 32707','1737 CORNERVIEW LANE, Orlando, FL 32820','3689 ROCHELLE LANE, Apopka, FL 32712','1923 BIG CYPRESS DRIVE, Saint Cloud, FL 34771','1924 KNOLLCREST DRIVE, Clermont, FL 34711','1531 LALIQUE LANE, Orlando, FL 32828','12623 LAKE DENISE BOULEVARD, Clermont, FL 34711','2925 MAGNOLIA BLOSSOM CIRCLE, Clermont, FL 34711','1152 ORANGE GROVE LANE, Apopka, FL 32712']
      # sts = ['2634 QUARTERDECK COURT, Kissimmee, FL 34743','8867 HASTINGS BEACH BOULEVARD, Orlando, FL 32829','6405 DIANE COURT, Saint Cloud, FL 34771','10241 MASON LOOP, Clermont, FL 34711','10066 SHADOW CREEK DRIVE, Orlando, FL 32832','104 SEQUOIA VALLEY COURT, Minneola, FL 34715','14344 SAPPHIRE BAY CIRCLE, Orlando, FL 32828','5817 AUTUMN CHASE CIRCLE, Sanford, FL 32773','275 VELVETEEN PLACE, Chuluota, FL 32766','1319 AGUACATE COURT, Orlando, FL 32837','510 NEWHALL LANE, Debary, FL 32713','216 BELGIAN WAY, Sanford, FL 32773','2720 NOVA DRIVE, Apopka, FL 32703','415 CAREY WAY, Orlando, FL 32825','1000 CORNELL AVENUE, Clermont, FL 34711','355 E LAKE AVENUE, Longwood, FL 32750','913 ANCHORAGE LANE, Kissimmee, FL 34744','12520 WOODBURY COVE DRIVE, Orlando, FL 32828','10538 VIA LUGANO COURT, Clermont, FL 34711','910 DON WILSON AVENUE, Apopka, FL 32712','808 BRISTOL FOREST WAY, Orlando, FL 32828','1163 LEBANON COURT, Sanford, FL 32771','1312 SENECA FALLS DRIVE, Orlando, FL 32828','2835 BICKLEY DRIVE, Apopka, FL 32712','504 VICKS LANDING DRIVE, Apopka, FL 32712','10438 MOSS ROSE WAY, Orlando, FL 32832','1741 ELDORADO COURT, Saint Cloud, FL 34771','1844 OXTON COURT, Ocoee, FL 34761','12633 DOUGLAS FIR COURT, Clermont, FL 34711','17937 SAXONY LANE, Orlando, FL 32820','5587 PATS POINT, Winter Park, FL 32792','2221 EAGLES LANDING WAY, Kissimmee, FL 34744','14696 KRISTENRIGHT LANE, Orlando, FL 32826','154 LONDON FOG WAY, Sanford, FL 32771','972 CORNELL AVENUE, Clermont, FL 34711','1192 GREEN VISTA CIRCLE, Apopka, FL 32712','1519 LALIQUE LANE, Orlando, FL 32828','1718 AMERICUS MINOR DRIVE, Winter Garden, FL 34787','518 BABBLING BROOK COURT, Orlando, FL 32825','11425 ROUSE RUN CIRCLE, Orlando, FL 32817','2290 CALEDONIAN STREET, Clermont, FL 34711','1280 OCKLAWAHA DRIVE, Orlando, FL 32828','1505 BRAYTON CIRCLE, Deltona, FL 32725','701 PADDINGTON PLACE, Deltona, FL 32725','803 PRINCETON DRIVE, Clermont, FL 34711','606 NEUMANN VILLAGE COURT, Ocoee, FL 34761','33008 WINDY OAK STREET, Sorrento, FL 32776','11521 ROUSE RUN CIRCLE, Orlando, FL 32817','1509 MORNING STAR DRIVE, Clermont, FL 34714','12007 STILL MEADOW DRIVE, Clermont, FL 34711','2534 DELBARTON AVENUE, Deltona, FL 32725','2122 VICTORIA FALLS DRIVE, Orlando, FL 32824','14637 HUNTCLIFF PARK WAY, Orlando, FL 32824','2045 CORNER MEADOW CIRCLE, Orlando, FL 32820','621 SKYRIDGE ROAD, Clermont, FL 34711','825 LAKE DOE BOULEVARD, Apopka, FL 32703','2982 MARBELLA DRIVE, Kissimmee, FL 34744','1719 SOUTHERN OAK LOOP, Minneola, FL 34715','14655 LADY VICTORIA BOULEVARD, Orlando, FL 32826','507 DOMINISH ESTATES DRIVE, Apopka, FL 32712','1705 BRIDGEPEBBLE ROAD, Winter Garden, FL 34787','3219 COUNTRYSIDE VIEW DRIVE, Saint Cloud, FL 34772','14156 ECON WOODS LANE, Orlando, FL 32826','1451 AGATHA DRIVE, Deltona, FL 32725','1180 SHERBROOK DRIVE, Deltona, FL 32725','642 WHISPER SOUND STREET, Minneola, FL 34715','30650 PGA DRIVE, Sorrento, FL 32776','1917 SOUTHERN OAK LOOP, Minneola, FL 34715','2011 STONE ABBEY BOULEVARD, Orlando, FL 32828','905 ANCHORAGE LANE, Kissimmee, FL 34744','7872 NIAGARA FALLS COURT, Orlando, FL 32825','15328 TORPOINT ROAD, Winter Garden, FL 34787','30620 PGA DRIVE, Sorrento, FL 32776','3218 HUNTERS CHASE LOOP, Kissimmee, FL 34743','1291 DARNABY WAY, Orlando, FL 32824','11604 ROPER BOULEVARD, Clermont, FL 34711','1200 GIOVANNI STREET, Deltona, FL 32725','11718 FOXGLOVE DRIVE, Clermont, FL 34711','2552 CROWN RIDGE CIRCLE, Kissimmee, FL 34744','1955 VILLA ANGELO BOULEVARD, Saint Cloud, FL 34769','710 WHISPER SOUND STREET, Minneola, FL 34715','1386 LYDIA DRIVE, Deltona, FL 32725','10231 CODY LANE, Orlando, FL 32825','733 CHARING PLACE, Deltona, FL 32725','1384 WILDBERRY LANE, Deltona, FL 32725','1507 MERRIMAC LANE, Deltona, FL 32725','1401 EDISON TERRACE, Deltona, FL 32725','1255 ANDERSON STREET, Deltona, FL 32725','516 S DISSTON AVENUE, Minneola, FL 34715','1152 ABELINE DRIVE, Deltona, FL 32725','1075 FORT SMITH BOULEVARD, Deltona, FL 32725','705 PADDINGTON PLACE, Deltona, FL 32725','2743 HOWLAND BOULEVARD, Deltona, FL 32725','10714 CEDAR FOREST CIRCLE, Clermont, FL 34711','2150 N NORMANDY BOULEVARD, Deltona, FL 32725','30719 PGA DRIVE, Sorrento, FL 32776','222 ACACIA ROAD, Debary, FL 32713','541 ANTOINETTE STREET, Deltona, FL 32725','1808 E COOPER DRIVE, Deltona, FL 32725','1874 W COOPER DRIVE, Deltona, FL 32725','2857 SMITHFIELD DRIVE, Orlando, FL 32837','9211 COUNTY ROAD 561, Clermont, FL 34711','901 COPPERFIELD TERRACE, Casselberry, FL 32707','1415 STONE TRAIL, Enterprise, FL 32725','4456 STEED TERRACE, Winter Park, FL 32792','866 MOONLIT LANE, Casselberry, FL 32707','9431 LAKESHORE DRIVE, Clermont, FL 34711','3701 SEMINOLE DRIVE, Orlando, FL 32812','1191 FREEDOM LANE, Winter Springs, FL 32708','155 RAYMOND OAKS COURT, Altamonte Springs, FL 32701','4503 SANDHURST DRIVE, Orlando, FL 32817','10013 IAN STREET, Orlando, FL 32825','14849 GREEN VALLEY BOULEVARD, Clermont, FL 34711','903 LAUREL LEAF COURT, Apopka, FL 32712','2757 HERON LANDING COURT, Orlando, FL 32837','1011 LONG BRANCH LANE, Oviedo, FL 32765','5101 BELLTHORN DRIVE, Orlando, FL 32837','411 AUGUSTINE COURT, Oviedo, FL 32765','3900 LAKE MIRAGE BOULEVARD, Orlando, FL 32817','842 LANCER CIRCLE, Ocoee, FL 34761','14526 QUAIL TRAIL CIRCLE, Orlando, FL 32837','8636 OTTER CREEK COURT, Orlando, FL 32829','2125 MARTINGALE PLACE, Oviedo, FL 32765','3153 TIMUCUA CIRCLE, Orlando, FL 32837','1035 CROSS CUT WAY, Longwood, FL 32750','3020 OLD CANOE CREEK ROAD, Saint Cloud, FL 34772','352 BALOGH PLACE, Longwood, FL 32750','10312 CARLSON CIRCLE, Clermont, FL 34711','13938 LAKE MARY JANE ROAD, Orlando, FL 32832','13750 SUNSHOWERS CIRCLE, Orlando, FL 32828','12311 BOHANNON BOULEVARD, Orlando, FL 32824','411 GLEN ABBEY LANE, Debary, FL 32713','2045 BRIDGEVIEW CIRCLE, Orlando, FL 32824','5449 BAYTOWNE PLACE, Oviedo, FL 32765','1219 FOXFORREST CIRCLE, Apopka, FL 32712','1726 RIVEREDGE ROAD, Oviedo, FL 32766','375 W HIGHBANKS ROAD, Debary, FL 32713','2937 SOMERSWORTH COURT, Orlando, FL 32835','1315 OLYMPIA PARK CIRCLE, Ocoee, FL 34761','1667 STERLING SILVER BOULEVARD, Deltona, FL 32725','2237 JESSICA LANE, Kissimmee, FL 34744','31114 GOLDEN LANE, Sorrento, FL 32776','4539 CONWAY LANDING DRIVE, Orlando, FL 32812','13047 PRAIRIE MEADOWS DRIVE, Orlando, FL 32837','11627 SIR WINSTON WAY, Orlando, FL 32824','342 INGLENOOK CIRCLE, Winter Springs, FL 32708','131 WESTMORELAND CIRCLE, Kissimmee, FL 34744','1747 SWEETWATER WEST CIRCLE, Apopka, FL 32712','4631 SUNTREE BOULEVARD, Orlando, FL 32817','1755 MYRTLE STREET, Sanford, FL 32773','705 HALLOWELL CIRCLE, Orlando, FL 32828','2204 WYNDAM WAY, Kissimmee, FL 34743','621 PICKFAIR TERRACE, Lake Mary, FL 32746','441 DEER POINTE CIRCLE, Casselberry, FL 32707','1538 COLUSO DRIVE, Winter Garden, FL 34787','3236 HARPERS FERRY COURT, Orlando, FL 32837','339 OAK LEAF CIRCLE, Lake Mary, FL 32746','26 HOLLOW PINE DRIVE, Debary, FL 32713','2270 ALCLOBE CIRCLE, Ocoee, FL 34761','1196 TROTWOOD BOULEVARD, Winter Springs, FL 32708','2509 WEKIVA WALK WAY, Apopka, FL 32703','1711 IMPERIAL PALM DRIVE, Apopka, FL 32712','1137 CROWN ISLE CIRCLE, Apopka, FL 32712','556 LAKE MCCOY DRIVE, Apopka, FL 32712','140 STILLBROOK TRAIL, Enterprise, FL 32725','2964 DIVISION STREET, Oviedo, FL 32765','4942 CASA VISTA DRIVE, Orlando, FL 32837','1440 GALENA TERRACE, Deltona, FL 32725','384 HINSDALE DRIVE, Debary, FL 32713','2828 COPPER RIDGE COURT, Lake Mary, FL 32746','965 WELLINGTON AVENUE, Oviedo, FL 32765','1000 PIEDMONT OAKS DRIVE, Apopka, FL 32703','8081 WELLSMERE CIRCLE, Orlando, FL 32835','2608 QUARTERDECK COURT, Kissimmee, FL 34743','3878 AIDEN PLACE, Apopka, FL 32703','1910 PINE GROVE ROAD, Saint Cloud, FL 34771','3010 EAGLE LAKE DRIVE, Orlando, FL 32837','14713 DAY LILY COURT, Orlando, FL 32824','1005 WHITE SPRINGS LANE, Winter Springs, FL 32708','2609 CEDAR BLUFF LANE, Ocoee, FL 34761','9331 TELFER RUN, Orlando, FL 32817','234 BRIGHTVIEW DRIVE, Lake Mary, FL 32746','753 CREPE MYRTLE CIRCLE, Apopka, FL 32712','2315 RUNNING HORSE TRAIL, Saint Cloud, FL 34771','10439 CARLSON CIRCLE, Clermont, FL 34711','2815 PINE AVENUE, Apopka, FL 32703','2068 CASSINGHAM CIRCLE, Ocoee, FL 34761','367 FOXHILL DRIVE, Debary, FL 32713','4779 DUNBARTON DRIVE, Orlando, FL 32817','249 BUENA VISTA STREET, Debary, FL 32713','1462 LUND AVENUE, Kissimmee, FL 34744','3225 SAWGRASS CREEK CIRCLE, Saint Cloud, FL 34772','442 FOXHILL DRIVE, Debary, FL 32713','438 FOXHILL DRIVE, Debary, FL 32713','1536 REGAL COURT, Kissimmee, FL 34744','3827 PEACE PIPE DRIVE, Orlando, FL 32829','603 QUEENSBRIDGE DRIVE, Lake Mary, FL 32746','2310 FLAMINGO LAKES DRIVE, Kissimmee, FL 34743','10312 CASTILLO COURT, Clermont, FL 34711','11740 CRESCENT PINES BOULEVARD, Clermont, FL 34711','5372 ROCKING HORSE PLACE, Oviedo, FL 32765','2728 EAGLE LAKE DRIVE, Orlando, FL 32837','1484 COUNTRY VILLA COURT, Apopka, FL 32703','12715 WOODBURY OAKS DRIVE, Orlando, FL 32828','285 COUNTRY LANDING BOULEVARD, Apopka, FL 32703','2144 MAJESTIC WOODS BOULEVARD, Apopka, FL 32712','4148 EAGLE FEATHER DRIVE, Orlando, FL 32829','4461 DRAYTON LANE, Oviedo, FL 32765','701 S ENDEAVOUR DRIVE, Winter Springs, FL 32708','741 CREPE MYRTLE CIRCLE, Apopka, FL 32712','271 HIDDEN SPRINGS CIRCLE, Kissimmee, FL 34743','707 MEADOWSIDE COURT, Orlando, FL 32825','10519 HUNTRIDGE ROAD, Orlando, FL 32825','2337 ROANOKE COURT, Lake Mary, FL 32746','9718 SYLVA COURT, Orlando, FL 32817','4742 LAKE SHARP DRIVE, Orlando, FL 32817','4418 WEEPING WILLOW CIRCLE, Casselberry, FL 32707','1915 NEEDHAM ROAD, Apopka, FL 32712','1131 RIDGE ROAD, Longwood, FL 32750','157 CORALWOOD CIRCLE, Kissimmee, FL 34743','1604 OAK HOLLOW ROAD, Clermont, FL 34711','2298 JESSICA LANE, Kissimmee, FL 34744','2401 ISLAND CLUB WAY, Orlando, FL 32822','759 VERONICA CIRCLE, Ocoee, FL 34761','4278 FOX HOLLOW CIRCLE, Casselberry, FL 32707','4530 SADDLE CREEK PLACE, Orlando, FL 32829','2980 MONICA TERRACE, Kissimmee, FL 34744','157 E 9TH STREET, Chuluota, FL 32766','11125 SYLVAN POND CIRCLE, Orlando, FL 32825','1636 SALMON STREET, Saint Cloud, FL 34771','2300 ALCLOBE CIRCLE, Ocoee, FL 34761','5171 MICHIGAN AVENUE, Sanford, FL 32771','2010 PALMETTO PINE LANE, Orlando, FL 32826','19 RANCH TRAIL ROAD, Debary, FL 32713','328 MISTY MEADOW DRIVE, Ocoee, FL 34761','1677 W AKRON DRIVE, Deltona, FL 32725','2413 WINFIELD DRIVE, Kissimmee, FL 34743','52 GRACIE ROAD, Debary, FL 32713','11626 PURPLE LILAC CIRCLE, Orlando, FL 32837','835 WOODFIELD COURT, Kissimmee, FL 34744','8001 ELMSTONE CIRCLE, Orlando, FL 32822','11653 WATER RIDGE LANE, Clermont, FL 34711','980 DINERO DRIVE, Winter Springs, FL 32708','2026 RIVER PARK BOULEVARD, Orlando, FL 32817','625 VIRGINIA WOODS LANE, Orlando, FL 32824','2575 TANDORI CIRCLE, Orlando, FL 32837','807 MCLEAN COURT, Orlando, FL 32825','1606 FOXCREEK LANE, Apopka, FL 32703','3138 HOLLAND DRIVE, Orlando, FL 32825','4246 SHADES CREST LANE, Sanford, FL 32773','12125 ROMERO STREET, Orlando, FL 32837','7718 FORT SUMTER DRIVE, Orlando, FL 32822','1015 APOPKA WOODS LANE, Orlando, FL 32824','3819 WINDWAY COURT, Orlando, FL 32817','1521 TRUMBULL STREET, Kissimmee, FL 34744','1508 FERGASON AVENUE, Deltona, FL 32725','3143 CAYMAN WAY, Orlando, FL 32812','1901 LEE WOOD COURT, Saint Cloud, FL 34772','7305 VICTORIA CIRCLE, Orlando, FL 32835','345 SAXON BOULEVARD, Deltona, FL 32725','9564 CHANDON DRIVE, Orlando, FL 32825','14533 POINTE EAST TRAIL, Clermont, FL 34711','8644 CLAIBORNE COURT, Orlando, FL 32825','298 HERONWOOD CIRCLE, Deltona, FL 32725','3621 FOX HOLLOW DRIVE, Orlando, FL 32829','2009 WILLIAMS AVENUE, Sanford, FL 32771','6525 GROSVENOR LANE, Orlando, FL 32835','9560 BRACKIN STREET, Orlando, FL 32825','11826 CLAIR PLACE, Clermont, FL 34711','1360 INDIAN ROCK COURT, Deltona, FL 32725','602 WHITTINGHAM PLACE, Lake Mary, FL 32746','176 GOLFSIDE CIRCLE, Sanford, FL 32773','10715 BROWN TROUT CIRCLE, Orlando, FL 32825','950 SYLVIA DRIVE, Deltona, FL 32725','2494 PINE CHASE CIRCLE, Saint Cloud, FL 34769','4011 CABAN COURT, Orlando, FL 32822','2342 CARRIAGE RUN ROAD, Kissimmee, FL 34741','139 HONEYWOOD DRIVE, Kissimmee, FL 34743','1902 E BARLINGTON DRIVE, Deltona, FL 32725','1231 ANDERSON STREET, Deltona, FL 32725','426 ROCKFORD STREET, Deltona, FL 32725','130 PINEWOOD CIRCLE, Kissimmee, FL 34743','602 ARMADILLO DRIVE, Deltona, FL 32725','1041 ALAHAMBRA STREET, Deltona, FL 32725','1183 WINDY WAY, Apopka, FL 32703','1985 E 4TH STREET, Sanford, FL 32771','7325 VICTORIA CIRCLE, Orlando, FL 32835','10731 LAKE RALPH DRIVE, Clermont, FL 34711','2550 IVYDALE DRIVE, Deltona, FL 32725','1420 ELKCAM BOULEVARD, Deltona, FL 32725','312 COUNTRY LANDING BOULEVARD, Apopka, FL 32703','139 N CERVIDAE DRIVE, Apopka, FL 32703','135 LAKE ADA CIRCLE, Sanford, FL 32773','30836 SUNNINGDALE STREET, Sorrento, FL 32776','2514 GINGER MILL BOULEVARD, Orlando, FL 32837','1201 ROMA CT, Orlando, FL 32825','550 LYNN ST, Oviedo, FL 32765','2040 S OLD MILL DR, Deltona, FL 32725','2040 S OLD MILL DR, Deltona, FL 32725','3034 SANDSTONE CIR, Saint Cloud, FL 34772','839 NEPTUNE POINTE LN, Kissimmee, FL 34744','2335 ASHINGTON PARK DR, Apopka, FL 32703','9270 VENEZIA PLANTATION DR, Orlando, FL 32829','297 SKY VALLEY ST, Clermont, FL 34711','2549 EUSTACE DR, Deltona, FL 32725','4912 E ST BRIDES CIR, Orlando, FL 32812','114 WHEATFIELD CIR, Sanford, FL 32771','2480 DEER CREEK BLVD, Saint Cloud, FL 34772','2625 EAGLES NEST CT, Orlando, FL 32837','9325 LARETTE DR, Orlando, FL 32817','1055 LIZA ST, Saint Cloud, FL 34771','324 OLD MILL RD, Enterprise, FL 32725','502 CANARY ISLAND CT, Orlando, FL 32828','321 CONCH KEY WAY, Sanford, FL 32771','13137 LOS ANGELES WOODS LN, Orlando, FL 32824','1261 HANCOCK CIR, Saint Cloud, FL 34769','1522 STONEYWOOD WAY, Apopka, FL 32712','1522 STONEYWOOD WAY, Apopka, FL 32712','2021 ALLSPICE AVE, Orlando, FL 32837','3206 MATTSON DR, Orlando, FL 32825','2342 AUDLEY ST, Oviedo, FL 32765','16408 CAJU RD, Clermont, FL 34711','1013 E RIVIERA BLVD, Oviedo, FL 32765','907 FEATHER DR, Deltona, FL 32725','820 KAZAROS CIR, Ocoee, FL 34761','200 BIRDIEWOOD CT, Debary, FL 32713','12308 ABBERTON CT, Orlando, FL 32837','11250 PINEWOOD COVE LN, Orlando, FL 32817','1525 RANDOLPH ST, Deltona, FL 32725','2526 BULLION LOOP, Sanford, FL 32771','409 NEWHEARTH CIR, Winter Garden, FL 34787','409 NEWHEARTH CIR, Winter Garden, FL 34787','2037 WEKIVA RESERVE BLVD, Apopka, FL 32703','2662 NEWBOLT DR, Orlando, FL 32817','3012 SANGRIA ST, Kissimmee, FL 34744','4263 VIXEN CT, Oviedo, FL 32765','3008 SANGRIA ST, Kissimmee, FL 34744','13011 DEVONSHIRE RD, Orlando, FL 32832','218 IOWA WOODS W CIR, Orlando, FL 32824','1480 S SEAGATE DR, Deltona, FL 32725','128 BRISTOL FOREST TRL, Sanford, FL 32771','9413 CANDICE CT, Orlando, FL 32832','10036 LETHBRIDGE DR, Orlando, FL 32829','105 HOLLOWAY CT, Sanford, FL 32771','105 HOLLOWAY CT, Sanford, FL 32771','106 DOMINION CT, Sanford, FL 32771','12957 DOWNSTREAM CIR, Orlando, FL 32828','325 ALEMANDER AVE, Debary, FL 32713','1243 CREEK WOODS CIR, Saint Cloud, FL 34772','618 N GOODRICH DR, Deltona, FL 32725','1409 COURT ST, Sanford, FL 32771','3924 BENSON PARK BLVD, Orlando, FL 32829','1339 AUGUSTA NATIONAL BLVD, Winter Springs, FL 32708','1021 ELKCAM BLVD, Deltona, FL 32725','10536 CHERRY OAK CIR, Orlando, FL 32817','2216 GRAND TREE CT, Lake Mary, FL 32746','1045 HAMPSHIRE CT, Winter Park, FL 32792','2290 LAKEVIEW AVE, Clermont, FL 34711','3914 CEDAR HAMMOCK TRL, Saint Cloud, FL 34772','289 PINE TRACE DR, Deltona, FL 32725','13191 LUXBURY LOOP, Orlando, FL 32837','1904 NICHOLAS PL, Saint Cloud, FL 34771','1115 ROYALTON RD, Orlando, FL 32825','408 WOODBURY PINES CIR, Orlando, FL 32828','531 SPINNAKER DR, Orlando, FL 32835','100 TOLLGATE TRAIL, Longwood, FL 32750','3162 TOURAINE AVENUE, Orlando, FL 32812','2215 ORCHARD DRIVE, Apopka, FL 32712','6355 JUDITH COURT, Saint Cloud, FL 34771','111 ORIENTA DRIVE, Altamonte Springs, FL 32701','614 E HELM WAY, Casselberry, FL 32707','451 MARIGOLD ROAD, Casselberry, FL 32707','1291 PUNTA GORDA CIRCLE, Winter Springs, FL 32708','791 E BIRCHWOOD CIRCLE, Kissimmee, FL 34743','1937 DUPONT COURT, Deltona, FL 32725','878 CLOVERLEAF BOULEVARD, Deltona, FL 32725','213 N ALDERWOOD STREET, Winter Springs, FL 32708','5127 OAK HILL DRIVE, Winter Park, FL 32792','653 LA SALLE DRIVE, Altamonte Springs, FL 32714','290 WETUMPA COURT, Casselberry, FL 32707','125 CLOVER LANE, Longwood, FL 32750','1849 CONCORD DR, Apopka, FL 32703','2468 MONTE CRISTO WAY, Sanford, FL 32771','1581 SILHOUETTE DRIVE, Clermont, FL 34711','908 GIRARD DRIVE, Orlando, FL 32824','2025 VICTORIA FALLS DRIVE, Orlando, FL 32824','385 COVENTRY ESTATES BOULEVARD, Deltona, FL 32725','2930 SPRINGLAKE VILLAGE BOULEVARD, Kissimmee, FL 34744','870 MAYBROOK DRIVE, Deltona, FL 32725','1124 DEER LAKE CIRCLE, Apopka, FL 32712','2723 CRANES COVE DRIVE, Kissimmee, FL 34741','2026 CASSINGHAM CIRCLE, Ocoee, FL 34761','1015 COVINGTON ST, Oviedo, FL 32765','4339 ROCKY RIDGE PL, Sanford, FL 32773','821 ABBOTT AVE, Deltona, FL 32725','821 ABBOTT AVE, Deltona, FL 32725','2409 OAK RUN BLVD, Kissimmee, FL 34744','2409 OAK RUN BLVD, Kissimmee, FL 34744','2720 SHEILA DR, Apopka, FL 32712','14825 CEDAR BRANCH WAY, Orlando, FL 32824','2111 ADAMS RIDGE RD, Apopka, FL 32703','46 S CERVIDAE DR, Apopka, FL 32703','882 STRATTON ST, Deltona, FL 32725','518 TIBERON COVE RD, Longwood, FL 32750','622 OAK MANOR CIR, Orlando, FL 32825','409 CLOVERLEAF BLVD, Deltona, FL 32725','2811 LATTER DAY CT, Saint Cloud, FL 34772','2343 FITZPATRICK TER, Deltona, FL 32725','1677 12TH ST, Clermont, FL 34711','3165 LAKE GEORGE COVE DR, Orlando, FL 32812','541 FLAGLER ST, Deltona, FL 32725','1298 FORT SMITH BLVD, Deltona, FL 32725','13239 CANNA LILY DR, Orlando, FL 32824','381 W DE CARLO DR, Deltona, FL 32725','552 BISON CIR, Apopka, FL 32712','5582 PATS PT, Winter Park, FL 32792','2698 SUNBURY ST, Clermont, FL 34711','619 WEKIVA CREST DR, Apopka, FL 32712','638 APPLEGATE TER, Deltona, FL 32725','1036 N HARBOR DR, Deltona, FL 32725','3909 GYPSUM LN, Orlando, FL 32826','3909 GYPSUM LN, Orlando, FL 32826','582 HARDWOOD PL, Lake Mary, FL 32746','3083 WOOLRIDGE DR, Orlando, FL 32837','14321 GOLDEN RAIN TREE BLVD, Orlando, FL 32828','2669 SEDGEFIELD AVE, Deltona, FL 32725','2824 OAK SHORE RD, Oviedo, FL 32766','115 RACHEL LIN LN, Saint Cloud, FL 34771','16829 GOLD STAR CT, Clermont, FL 34714','14080 BRADBURY RD, Orlando, FL 32828','764 GALWAY BLVD, Apopka, FL 32703','4719 FORT KNOX CT, Orlando, FL 32822','4719 FORT KNOX CT, Orlando, FL 32822','143 KNIGHTS HOLLOW DR, Apopka, FL 32712','1165 TROTWOOD BLVD, Winter Springs, FL 32708','1165 TROTWOOD BLVD, Winter Springs, FL 32708','2349 CILANTRO DR, Orlando, FL 32837','2117 TURMERIC AVE, Orlando, FL 32837','1638 LAWNDALE CIR, Winter Park, FL 32792','3978 CEDAR HAMMOCK TRL, Saint Cloud, FL 34772','3978 CEDAR HAMMOCK TRL, Saint Cloud, FL 34772','1713 COUNTRY TERRACE LN, Apopka, FL 32703','1706 SANFORD AVE, Sanford, FL 32771','167 Portstewart Drive, Orlando, FL 32828','8353 Lake Crowell Circle, Orlando, FL 32836','3712 Manteo Circle, Orlando, FL 32837','618 Crownclover Avenue, Orlando, FL 32828','2631 Runyon Circle, Orlando, FL 32837','2627 VERONA TRL, WINTER PARK, FL 32789','4464 TUSCANY ISLAND CT, WINTER PARK, FL 32792','359 REMINGTON DR, OVIEDO, FL 32765','1336 N MARCY DR, LONGWOOD, FL 32750','1363 N MARCY DR, LONGWOOD, FL 32750','1368 SAN DIEGO CT, WINTER SPRINGS, FL 32708','4057 GALLAGHER LOOP, CASSELBERRY, FL 32707','515 MANDAN CT, LAKE MARY, FL 32746','2750 RUNNING SPRINGS, LOOP OVIEDO, FL 32765','1671 MARKHAM WOODS RD, LONGWOOD, FL 32779','854 SHRIVER CIR, LAKE MARY, FL 32746','2761 TEAK PL, LAKE MARY, FL 32746','3063 MANDOLIN DR, KISSIMMEE, FL 34744','2956 LEBA LN, SAINT CLOUD, FL 34772','3837 SHOREVIEW DR, KISSIMMEE, FL 34744','10802 ARBOR VIEW BLVD, ORLANDO, FL 32825','1340 PELHAM RD, WINTER PARK, FL 32789','418 COVERED BRIDGE DR, OCOEE, FL 34761','2724 MUSCATELLO ST, ORLANDO, FL 32837','16957 DEER OAK LN, ORLANDO, FL 32828','115 W SPRUCE ST, ORLANDO, FL 32804','14033 DEEP LAKE DR, ORLANDO, FL 32826','2141 SETTLERS TRL, ORLANDO, FL 32837','13528 HERON CAY CT, ORLANDO, FL 32837','9307 LAKE FISCHER BLVD, GOTHA, FL 34734','8556 SUMMERVILLE PL, ORLANDO, FL 32819','313 LA CREEK CT, DEBARY, FL 32713','1005 FOXHOUND WY, LONGWOOD, FL 32750','4551 BOND LN, OVIEDO, FL 32765','5110 OTTERS DEN TRL, SANFORD, FL 32771','992 SLEEPING ROCK CT, WINTER SPRINGS, FL 32708','927 ARABIAN AVE, WINTER SPRINGS, FL 32708','2711 PEARL CT, KISSIMMEE, FL 34743','1440 WESTCHESTER AVE, WINTER PARK, FL 32789','1213 GUNNISON AVE, ORLANDO, FL 32804','492 SADDELL BAY LOOP, OCOEE, FL 34761','1014 PALMER ST, ORLANDO, FL 32801','12720 DALLINGTON TER, WINTER GARDEN, FL 34787','380 MISTY OAKS RUN, CASSELBERRY, FL 32707','2141 CARPATHIAN DR, APOPKA, FL 32712','9030 FLAT ROCK LN, ORLANDO, FL 32832','2001 MOUNTLEIGH TRL, ORLANDO, FL 32824','1415 SUN MEADOW DR, ORLANDO, FL 32824','1358 JECENIA BLOSSOM DR, APOPKA, FL 32712','15336 MONTESINO DR, ORLANDO, FL 32828','6266 Golden Dewdrop Trail, Windermere, FL 34786']
      # sts = ['992 W CHARING CROSS CIR, Lake Mary, FL 32746','2037 WEKIVA RESERVE BLVD, Apopka, FL 32703','2179 Hunley Ave, Apopka, FL 32703','1152 Alston Bay Blvd, Apopka, FL 32703','988 Alston Bay Blvd, Apopka, FL 32703','3716 Moon Dancer Pl, Saint Cloud, FL 34772','1971 Valley Forge Dr, Saint Cloud, FL 34769','2268 Victoria Falls Drive, Orlando, FL 32824','3522 WOODLEY PARK PL, Oviedo, FL 32765','445 ORANGE STREET, Altamonte Springs, FL 32701','3252 NIGHT BREEZE LANE, Lake Mary, FL 32746','14507 PINE CONE TRAIL, Clermont, FL 34711','14717 DAY LILY COURT, Orlando, FL 32824','1130 HARBOUR VIEW CIRCLE, Longwood, FL 32750','641 OAK HOLLOW WAY, Altamonte Springs, FL 32714','10214 CINCHWOOD LANE, Orlando, FL 32836','10214 CINCHWOOD LANE, Orlando, FL 32836','10214 CINCHWOOD LANE, Orlando, FL 32836','1855 EAGLES REST DRIVE, Apopka, FL 32712','210 SADLER AVENUE, Oakland, FL 34760','13739 DORNOCH DRIVE, Orlando, FL 32828','230 LAKEBREEZE CIRCLE, Lake Mary, FL 32746','397 FAIRFIELD DRIVE, Sanford, FL 32771','17536 WOODCREST WAY, Clermont, FL 34714','7761 INDIAN RIDGE TRAIL, Kissimmee, FL 34747','7761 INDIAN RIDGE TRAIL, Kissimmee, FL 34747','2893 BREEZY MEADOW ROAD, Apopka, FL 32712','1841 CENTENNIAL AVENUE, Saint Cloud, FL 34769','5006 TANGERINE AVENUE, Winter Park, FL 32792','2119 JUSTICE LANE, Saint Cloud, FL 34769','180 BROADMOOR AVENUE, Lake Mary, FL 32746','180 BROADMOOR AVENUE, Lake Mary, FL 32746','7266 CARILLON AVENUE, Cocoa, FL 32927','4244 ANDOVER CAY BOULEVARD, Orlando, FL 32825','700 LAKEVIEW AVENUE, Winter Garden, FL 34787','3225 INDIAN TRAIL, Eustis, FL 32726','5050 CAPE HATTERAS DRIVE, Clermont, FL 34714','11044 DAWNVIEW LANE, Orlando, FL 32825','10313 STRATFORD POINTE AVENUE, Orlando, FL 32832','1141 DUNCAN DRIVE, Winter Springs, FL 32708','1220 PORTMOOR WAY, Winter Garden, FL 34787','13032 COG HILL WAY, Orlando, FL 32828','13032 COG HILL WAY, Orlando, FL 32828','14919 LAKE AZURE DRIVE, Orlando, FL 32824','2618 SAN SIMEON WAY, Kissimmee, FL 34741','550 PRESTON ROAD, Longwood, FL 32750','10601 VISTA DEL SOL CIRCLE, Clermont, FL 34711','1341 EUSTIS ROAD, Eustis, FL 32726','2472 ROSECRANS COURT, Apopka, FL 32712','648 CROWNCLOVER AVENUE, Orlando, FL 32828','13350 LOBLOLLY LANE, Clermont, FL 34711','13350 LOBLOLLY LANE, Clermont, FL 34711','331 FERDINAND DRIVE, Longwood, FL 32750','13544 EYAS ROAD, Orlando, FL 32837','13544 EYAS ROAD, Orlando, FL 32837','14511 MANDOLIN DRIVE, Orlando, FL 32837','648 RANDON TERRACE, Lake Mary, FL 32746','712 LANCEWOOD DRIVE, Winter Springs, FL 32708','632 FALCON COURT, Winter Springs, FL 32708','860 Brightwater Circle, Maitland, FL 32751','14126 FOX GLOVE ST, Winter Garden, FL 34787','1358 SHELTER ROCK RD, Orlando, FL 32835','1324 VALLEY PINE CIR, Apopka, FL 32712','2341 WICKDALE CT, Ocoee, FL 34761','2465 ORSOTA CIR, Ocoee, FL 34761','13212 OVERSTREET RD, Windermere, FL 34786','596 LEXINGTON PKWY, Apopka, FL 32712','2832 BREEZY MEADOW DRIVE, Apopka, FL 32712','2623 SHEILA DR, Apopka, FL 32712','1805 Grasmere Drive, Apopka, FL 32703','13527 MADISON DOCK RD, Orlando, FL 32828','1041 FOGGY BROOK PL, Longwood, FL 32750','2396 WESTWOOD DR, Longwood, FL 32779','276 LINBERRY LN, Ocoee, FL 34761','3690 ROCHELLE LN, Apopka, FL 32712','261 LANCER OAK DR, Apopka, FL 32712','2018 SAWGRASS DR, Apopka, FL 32712','2255 PELINION ST, Apopka, FL 32712','2424 CEDAR KNOLL DR, Apopka, FL 32712','2093 JUDITH PL, Longwood, FL 32779','2026 CASCADES COVE DR, Orlando, FL 32820','2774 UNIVERSITY ACRES DR, Orlando, FL 32817','2851 GRASSMOOR LOOP, Apopka, FL 32712','1466 Westbury Drive, Clermont, FL 34711','3510 Foxcroft Circle, Oviedo, FL 32765','2869 Picadilly Circle, Kissimmee, FL 34747','130 Thornbury Drive, Kissimmee, FL 34744','5846 Cheshire Cove Terrace, Orlando, FL 32829','2608 Florence Drive, Kissimmee, FL 34744','4017 Lillian Hall Lane, Orlando, FL 32812','10131 Malpas Point, Orlando, FL 32832','548 Fern Lake Drive, Orlando, FL 32825','8648 Vista Harbor Court, Orlando, FL 32836','1710 Lochshyre Loop, Ocoee, FL 34761','14330 Paradise Tree Drive, Orlando, FL 32828','8046 Solitaire Court, Orlando, FL 32836','1201 Shelter Rock Road, Orlando, FL 32835','536 Belhaven Falls Drive, Ocoee, FL 34761','6351 Point Hancock Drive, Winter Garden, FL 34787','30424 Jubilee Way, Sorrento, FL 32776','15612 Firelight Drive, Winter Garden, FL 34787','664 Cimarosa Court, Ocoee, FL 34761','1477 Pine Marsh Loop, Saint Cloud, FL 34771','2716 Kingston Ridge Drive, Clermont, FL 34711','3501 Starcatcher Street, Saint Cloud, FL 34772','4042 Evander Drive, Orlando, FL 32812','9404 Whispering Meadows Lane, Orlando, FL 32825','3226 Open Meadow Loop, Oviedo, FL 32766','3441 Glen Village Court, Orlando, FL 32822','2935 Red Oak Drive, Kissimmee, FL 34744','13442 Budworth Circle, Orlando, FL 32832','1133 Mosaic Drive, Celebration, FL 34747','294 Wescliff Drive, Ocoee, FL 34761','606 Water Oak Lane, Longwood, FL 32779','4205 Whitemoor Court, Clermont, FL 34711','10785 Willow Ridge Loop, Orlando, FL 32825','12622 Scottish Pine Lane, Clermont, FL 34711','5472 Rishley Run Way, Mount Dora, FL 32757','13914 Eylewood Drive, Winter Garden, FL 34787','12831 Cragside Lane, Windermere, FL 34786','787 High Grove Park Court, Oviedo, FL 32765','8511 Peconic Drive, Orlando, FL 32835','4658 Forrestal Avenue, Orlando, FL 32806','5115 Rishley Run Way, Mount Dora, FL 32757','1201 Providence Boulevard, Kissimmee, FL 34744','501 Terrace Cove Way, Orlando, FL 32828','6628 Bouganvillea Crescent Drive, Orlando, FL 32809','4598 S Hampton Drive, Orlando, FL 32812','2137 Firestone Court, Oviedo, FL 32765','6254 Buford Street, Orlando, FL 32835','1702 Baxter Avenue, Orlando, FL 32806','16112 Birchwood Way, Orlando, FL 32828','12784 Waterhaven Circle, Orlando, FL 32828','13959 Marine Drive, Orlando, FL 32832','4212 Trentonian Court, Orlando, FL 32812','2446 Ridgemoor Drive, Orlando, FL 32828','2207 Summer Raye Court, Saint Cloud, FL 34772','2125 Sunset Terrace Drive, Orlando, FL 32825','359 Remington Drive, Oviedo, FL 32765','2852 Magnolia Blossom Circle, Clermont, FL 34711','1066 New Castle Lane, Oviedo, FL 32765','825 Grey Heron Place, Chuluota, FL 32766','4131 Bell Tower Court, Belle Isle, FL 32812','605 S Dillingham Avenue, Kissimmee, FL 34741','643 Green Meadow, Maitland, FL 32751','2802 Ashbridge Street, Orlando, FL 32825','3851 Wood Thrush Drive, Kissimmee, FL 34744','2711 Estep Court, Ocoee, FL 34761','10059 Cypress Knee Circle, Orlando, FL 32825','10708 Fallow Trail, Orlando, FL 32817','13750 Via Roma Circle, Clermont, FL 34711','4999 Disston Drive, Saint Cloud, FL 34771','9310 Whittingham Drive, Orlando, FL 32817','10021 Cove Lake Drive, Orlando, FL 32836','14522 Quail Trail Circle, Orlando, FL 32837','10632 Crystal Springs Court, Orlando, FL 32825','1014 Harbor Hill Street, Winter Garden, FL 34787','1000 Horton Court, Oviedo, FL 32765','2484 Prairie View Drive, Winter Garden, FL 34787','10342 Oakview Pointe Terrace, Gotha, FL 34734','14245 Paradise Tree Drive, Orlando, FL 32828','4020 Serena Lane, Clermont, FL 34711','5100 Appenine Loop, Saint Cloud, FL 34771','10402 Vista Pines Loop, Clermont, FL 34711','2130 White Eagle Street, Clermont, FL 34714','11885 Zelkova Lane, Orlando, FL 32827','15516 Charter Oaks Trail, Clermont, FL 34711','225 Wild Pine Point, Orlando, FL 32828','1774 Derby Glen Drive, Orlando, FL 32837','671 First Cape Coral Drive, Winter Garden, FL 34787','2710 Valiant Drive, Clermont, FL 34711','9530 Osprey Landing Drive, Orlando, FL 32832','1408 Lexi Davis Street, Orlando, FL 32828','5718 Sweetheart Court, Saint Cloud, FL 34772','10730 Fairhaven Way, Orlando, FL 32825','3523 Wading Heron Terrace, Oviedo, FL 32766','3139 Pizzaro Place, Clermont, FL 34715','30106 Jutland Court, Mount Dora, FL 32757','1413 Resolute Street, Kissimmee, FL 34747','828 Crooked Branch Drive, Clermont, FL 34711','1433 Pine Marsh Loop, Saint Cloud, FL 34771','4752 Coppola Drive, Mount Dora, FL 32757','1410 Sean Court, Saint Cloud, FL 34772','2410 Country Pond Court, Saint Cloud, FL 34771','926 Pawstand Road, Celebration, FL 34747','2103 Hammock Moss Drive, Orlando, FL 32820','2557 Runyon Circle, Orlando, FL 32837','12125 Dyson Court, Orlando, FL 32821','14009 Fairwinds Court, Orlando, FL 32824','14761 Grand Cove Drive, Orlando, FL 32837','9127 North Bay Boulevard, Orlando, FL 32819','1532 Cedar Lake Drive, Orlando, FL 32824','5430 Birchbend Loop, Oviedo, FL 32765','9037 Notchwood Court, Orlando, FL 32825','14203 Whooping Crane Lane, Orlando, FL 32824','3741 Linsbury Street, Orlando, FL 32817','15349 Markham Drive, Clermont, FL 34714','5327 Brady Lane, Orlando, FL 32814','11800 Gray Rock Trail, Windermere, FL 34786','10924 Eliotti Street, Orlando, FL 32832','3018 Ash Park Point, Winter Park, FL 32792','8530 Sidon Street, Orlando, FL 32817','4961 Brightmour Circle, Orlando, FL 32837','2474 Orsota Circle, Ocoee, FL 34761','4630 Woodlands Village Drive, Orlando, FL 32835','11063 Summerspring Lakes Drive, Orlando, FL 32825','7030 Maverick Trail, Saint Cloud, FL 34771','7987 Sapphire Lane, Orlando, FL 32822','2107 Continental Street, Saint Cloud, FL 34769','3700 Brookmyra Drive, Orlando, FL 32837','2387 Sabastian Street, Mount Dora, FL 32757','9152 Mossy Oak Lane, Clermont, FL 34711','1931 Lochshyre Loop, Ocoee, FL 34761','3430 Turningwind Lane, Winter Garden, FL 34787','3441 Diamond Leaf Lane, Oviedo, FL 32766','10929 Arrowtree Boulevard, Clermont, FL 34715','10037 Richardson Court, Orlando, FL 32825','1003 Green Branch Court, Oviedo, FL 32765','2930 Marjorie Road, Saint Cloud, FL 34772','819 Hallowell Circle, Orlando, FL 32828','2841 Ithaca Court, Orlando, FL 32826','7808 Turkey Oak Lane, Kissimmee, FL 34747','14565 Braddock Oak Drive, Orlando, FL 32837','6011 Cornelia Avenue, Orlando, FL 32807','13436 Texas Woods Circle, Orlando, FL 32824','5888 Lake Melrose Drive, Orlando, FL 32829','12723 Winding Woods Lane, Orlando, FL 32832','13115 Social Lane, Winter Garden, FL 34787','559 Horseman Drive, Oviedo, FL 32765','445 Ridge Forest Court, Sanford, FL 32771','712 Seneca Trail, Saint Cloud, FL 34772','1848 Blue Fox Court, Orlando, FL 32825','2256 Windcrest Lake Circle, Orlando, FL 32824','687 Woodland Creek Boulevard, Kissimmee, FL 34744','777 Kyle Court, Chuluota, FL 32766','17714 Sterling Pond Lane, Orlando, FL 32820','13843 Old Dock Road, Orlando, FL 32828','4015 Bounce Drive, Orlando, FL 32812','8135 Fort Thomas Way, Orlando, FL 32822','10935 Lanesboro Court, Orlando, FL 32825','1013 Vinsetta Circle, Winter Garden, FL 34787','3215 Hurd Avenue, Orlando, FL 32812','32312 Avington Road, Sorrento, FL 32776','973 Tramells Trail, Kissimmee, FL 34744','2260 Blackjack Oak Street, Ocoee, FL 34761','8240 Prestbury Drive, Orlando, FL 32832','12096 Alder Branch Loop, Orlando, FL 32824','3800 Blue Dasher Drive, Kissimmee, FL 34744','1961 Windcrest Lake Circle, Orlando, FL 32824','10615 Mere Parkway, Orlando, FL 32832','1931 River Park Boulevard, Orlando, FL 32817','13251 Moonflower Court, Clermont, FL 34711','1946 Pantheon Drive, Winter Garden, FL 34787','1932 Pantheon Drive, Winter Garden, FL 34787','10182 Pointview Court, Orlando, FL 32836','2046 Solar Drive, Winter Garden, FL 34787','1599 Anorada Boulevard, Kissimmee, FL 34744','2707 Kingston Ridge Drive, Clermont, FL 34711','1126 New Castle Court, Oviedo, FL 32765','2574 Jenscot Road, Saint Cloud, FL 34771','2044 Solar Drive, Winter Garden, FL 34787','4611 Cypress Forest Lane, Saint Cloud, FL 34772','1428 Brook Hollow Drive, Orlando, FL 32824','4618 Tiffany Woods Circle, Oviedo, FL 32765','2049 Solar Drive, Winter Garden, FL 34787','1070 Lascala Drive, Windermere, FL 34786','9127 Shadowbrook Trail, Orlando, FL 32825','1446 Silver Cove Drive, Clermont, FL 34714','3888 Shoreview Drive, Kissimmee, FL 34744','1757 Auburn Lakes Drive, Rockledge, FL 32955','1693 Strathmore Circle, Mount Dora, FL 32757','5297 Mill Stream Drive, Saint Cloud, FL 34771','15333 Heron Hideaway Circle, Winter Garden, FL 34787','2304 Cornhill Drive, Orlando, FL 32837','13542 Tenbury Wells Way, Winter Garden, FL 34787','1934 Pantheon Drive, Winter Garden, FL 34787','20907 Sullivan Ranch Boulevard, Mount Dora, FL 32757','2201 Deata Court, Saint Cloud, FL 34772','1930 Pantheon Drive, Winter Garden, FL 34787','2412 Oakington Street, Winter Garden, FL 34787','2788 Cullens Court, Ocoee, FL 34761','1313 Horizon Creek Court, Orlando, FL 32828','7409 Fairgrove Avenue, Windermere, FL 34786','6965 Corley Avenue, Windermere, FL 34786','2781 Kissimmee Bay Circle, Kissimmee, FL 34744','16345 Myers Court, Clermont, FL 34711','6735 Edgeworth Drive, Orlando, FL 32819','9862 Heron Pointe Drive, Orlando, FL 32832','6540 Dover Cove Drive, Orlando, FL 32822','13548 Kitty Fork Road, Orlando, FL 32828','1742 Delafield Drive, Winter Garden, FL 34787','2044 Pirie Place, Saint Cloud, FL 34769','1210 Epson Oaks Way, Orlando, FL 32837','3810 Doune Way, Clermont, FL 34711','12833 Hyland Lane, Clermont, FL 34711','1627 Whitney Isles Drive, Windermere, FL 34786','1744 Vale Drive, Clermont, FL 34711','516 Egret Place Drive, Winter Garden, FL 34787','14525 Spotted Sandpiper Boulevard, Winter Garden, FL 34787','2224 Fairglenn Way, Winter Park, FL 32792','4613 Barbados Loop, Clermont, FL 34711','9252 Lake Hickory Nut Drive, Winter Garden, FL 34787','839 Neptune Pointe Lane, Kissimmee, FL 34744','2632 Belmont Place, Kissimmee, FL 34744','7490 Betty Street, Winter Park, FL 32792','201 Pond Road, Mount Dora, FL 32757','393 Wekiva Cove Road, Longwood, FL 32779','1848 Osman Avenue, Orlando, FL 32806','2455 Mockingbird Avenue, Saint Cloud, FL 34771','2531 Long Iron Court, Longwood, FL 32779','410 Sweet Bay Drive, Longwood, FL 32779','3666 Limestone Street, Clermont, FL 34711','3517 Forest Ridge Lane, Kissimmee, FL 34741','8606 Villanova Street, Orlando, FL 32817','124 Seville Pointe Avenue, Orlando, FL 32807','10318 Jane Eyre Drive, Orlando, FL 32825','14009 Chicora Crossing Boulevard, Orlando, FL 32828','6612 Duncaster Street, Windermere, FL 34786','1925 Peach Tree Boulevard, Saint Cloud, FL 34769','15550 Camp Dubois Crescent, Winter Garden, FL 34787','3238 Stonewyck Street, Orlando, FL 32824','6971 Corley Avenue, Windermere, FL 34786','10137 Hatton Circle, Orlando, FL 32832','1709 Snaresbrook Way, Orlando, FL 32837','700 Grey Heron Place, Chuluota, FL 32766','3220 Stonewyck Street, Orlando, FL 32824','5692 Merlin Way, Saint Cloud, FL 34772','10533 Bruun Place, Orlando, FL 32825','102 Riverbend Boulevard, Longwood, FL 32779','14544 Pointe East Trail, Clermont, FL 34711','303 Willowbay Ridge Street, Sanford, FL 32771','16524 Magnolia Terrace, Montverde, FL 34756','2600 Pickett Downs Drive, Chuluota, FL 32766','3116 Rawcliffe Road, Clermont, FL 34714','3044 Lake Jean Drive, Orlando, FL 32817','3425 Quail Nest Court, Orlando, FL 32829','1766 Thetford Circle, Orlando, FL 32824','12137 Mangloe Court, Orlando, FL 32837','7636 Lake Angelina Drive, Mount Dora, FL 32757','2379 Sweetwater Boulevard, Saint Cloud, FL 34772','7714 Glynde Hill Drive, Orlando, FL 32835','1619 Mistflower Lane, Winter Garden, FL 34787','15312 Chahtah Court, Orlando, FL 32828','2480 Deer Creek Boulevard, Saint Cloud, FL 34772','2904 Sunset Retreat Court, Kissimmee, FL 34747','7289 Mardell Court, Orlando, FL 32835','4722 Coppola Drive, Mount Dora, FL 32757','2943 Sunset Vista Boulevard, Kissimmee, FL 34747','2205 Wintermere Pointe Drive, Winter Garden, FL 34787','13852 Red Mangrove Drive, Orlando, FL 32828','3501 Mcallister Way, Orlando, FL 32837','13217 Briar Forest Court, Orlando, FL 32828','8471 Lake Waverly Lane, Orlando, FL 32829','9845 Bennington Chase Drive, Orlando, FL 32829','105 Albrighton Drive, Longwood, FL 32779','1200 Pine Harbor Point Circle, Orlando, FL 32806','1914 Big Cypress Drive, Saint Cloud, FL 34771','10531 Satinwood Circle, Orlando, FL 32825','16018 Yelloweyed Drive, Clermont, FL 34714','3028 Bransbury Court, Kissimmee, FL 34747','17029 Florence View Drive, Montverde, FL 34756','855 Whispering Cypress Lane, Orlando, FL 32824','3200 Wax Myrtle Court, Kissimmee, FL 34744','5007 Cawthon Way, Saint Cloud, FL 34771','4880 Manchester Drive, Rockledge, FL 32955','15000 Ozzi Street, Winter Garden, FL 34787','2561 Carrickton Circle, Orlando, FL 32824','5170 Log Wagon Road, Ocoee, FL 34761','13825 Vista Del Lago Boulevard, Clermont, FL 34711','1013 Belvoir Drive, Kissimmee, FL 34744','2354 Dryburgh Court, Orlando, FL 32828','3231 Open Meadow Loop, Oviedo, FL 32766','8052 Laurel Ridge Drive, Mount Dora, FL 32757','5183 Brightmour Circle, Orlando, FL 32837','5406 Baybrook Avenue, Orlando, FL 32819','5201 Wildflower Road, Orlando, FL 32821','5356 Mellow Palm Way, Winter Park, FL 32792','12149 Rebeccas Run Drive, Winter Garden, FL 34787','9108 Outlook Rock Trail, Windermere, FL 34786','2116 Justice Lane, Saint Cloud, FL 34769','723 Sandy Bar Drive, Winter Garden, FL 34787','30323 Tokara Terrace, Mount Dora, FL 32757','2838 Scenic Lane, Kissimmee, FL 34744','14807 Tanja King Boulevard, Orlando, FL 32828','6220 Beldon, Mount Dora, FL 32757','14424 Brushwood Way, Winter Garden, FL 34787','8760 Bay Ridge Boulevard, Orlando, FL 32819','3206 Mattson Drive, Orlando, FL 32825','10003 Baywater Breeze Drive, Orlando, FL 32827','515 Charleswood Avenue, Orlando, FL 32825','506 Divine Circle, Orlando, FL 32828','2016 Pine Bluff Avenue, Orlando, FL 32806','3232 Stonewyck Street, Orlando, FL 32824','6920 Ranchero Court, Saint Cloud, FL 34771','981 Jade Forest Avenue, Orlando, FL 32828','1013 E Riviera Boulevard, Oviedo, FL 32765','2863 North Pointe Boulevard, Kissimmee, FL 34744','1721 Stafford Drive, Belle Isle, FL 32809','7921 Fernleaf Drive, Orlando, FL 32836','848 Pecori Terrace, Ocoee, FL 34761','820 Kazaros Circle, Ocoee, FL 34761','1139 Anne Elisa Circle, Saint Cloud, FL 34772','3716 Heirloom Rose Place, Oviedo, FL 32766','5129 Tiber Way, Saint Cloud, FL 34771','5123 Tiber Way, Saint Cloud, FL 34771','4151 Conway Place Circle, Orlando, FL 32812','3110 Mandolin Drive, Kissimmee, FL 34744','18149 Cadence Street, Orlando, FL 32820','2352 Blackjack Oak Street, Ocoee, FL 34761','779 Lancer Circle, Ocoee, FL 34761','14545 Broadhaven Boulevard, Orlando, FL 32828','131 Sura Boulevard, Orlando, FL 32809','8324 Foxworth Circle, Orlando, FL 32819','3057 Bloomsbury Drive, Kissimmee, FL 34747','3832 Valencia Grove Lane, Orlando, FL 32817','3139 Queen Alexandria Drive, Kissimmee, FL 34744','2091 Terrace Boulevard, Longwood, FL 32779','21944 Belgian Court, Mount Dora, FL 32757','377 Fieldstream West Boulevard, Orlando, FL 32825','1305 Mactavandash Drive, Oviedo, FL 32765','670 Seneca Trail, Saint Cloud, FL 34772','8146 Jailene Drive, Windermere, FL 34786','2292 Stanford Court, Clermont, FL 34711','3525 Windansea Court, Clermont, FL 34711','11515 Sandy Hill Drive, Orlando, FL 32821','11731 Barletta Drive, Orlando, FL 32827','9338 Palm Tree Drive, Windermere, FL 34786','1537 Balsam Willow Trail, Orlando, FL 32825','2718 Hilmer Court, Orlando, FL 32806','14813 Perdido Drive, Orlando, FL 32828','16054 River Pointe Court, Orlando, FL 32828','5018 Hook Hollow Circle, Orlando, FL 32837','1273 Martin Boulevard, Orlando, FL 32825','9709 Crenshaw Circle, Clermont, FL 34711','12278 Gray Birch Circle, Orlando, FL 32832','8067 Roaring Creek Court, Kissimmee, FL 34747','3669 Rollingbrook Street, Clermont, FL 34711','8026 Rural Retreat Court, Orlando, FL 32819','5171 Preserve Boulevard, Saint Cloud, FL 34772','720 Parkmont Place, Winter Garden, FL 34787','3341 Hillmont Circle, Orlando, FL 32817','7906 Jailene Drive, Windermere, FL 34786','7431 Winding Lake Circle, Oviedo, FL 32765','13129 Baneberry Court, Clermont, FL 34711','2890 Strand Circle, Oviedo, FL 32765','763 Crystal Bay Lane, Orlando, FL 32828','1830 Morgans Mill Circle, Orlando, FL 32825','9680 Loblolly Pine Circle, Orlando, FL 32827','10118 Garden Rose Court, Orlando, FL 32825','15422 Greater Groves Boulevard, Clermont, FL 34714','641 Longmeadow Circle, Longwood, FL 32779','4542 Simmons Road, Orlando, FL 32812','6889 Scythe Avenue, Orlando, FL 32812','3005 Mystic Cove Drive, Orlando, FL 32812','10209 Cypress Trail Drive, Orlando, FL 32825','4400 Aruba Boulevard, Clermont, FL 34711','3214 Stonewyck Street, Orlando, FL 32824','3540 Vestavia Way, Longwood, FL 32779','8123 Laughing Gull Street, Winter Garden, FL 34787','13319 Falcon Pointe Drive, Orlando, FL 32837','3230 Palatka Street, Orlando, FL 32824','1243 Creek Woods Circle, Saint Cloud, FL 34772','1336 Golfside Drive, Winter Park, FL 32792','14652 Greater Pines Boulevard, Clermont, FL 34711','678 Winding Lake Drive, Clermont, FL 34711','12767 Holdenbury Lane, Windermere, FL 34786','3124 Stonewyck Street, Orlando, FL 32824','1433 Lexi Davis Street, Orlando, FL 32828','1023 Trout Creek Court, Oviedo, FL 32765','3924 Benson Park Boulevard, Orlando, FL 32829','4918 Terrapin Boulevard, Saint Cloud, FL 34771','3615 Aughton Court, Orlando, FL 32812','322 Arbor Pointe Avenue, Minneola, FL 34715','157 E Lake Brantley Drive, Longwood, FL 32779','3512 Tern Hollow Drive, Orlando, FL 32828','7900 Tumbleweed Court, Orlando, FL 32822','1466 Amanda Road, Kissimmee, FL 34744','3536 Mccormick Woods Drive, Ocoee, FL 34761','7433 Betty Street, Winter Park, FL 32792','825 Palmetto Terrace, Oviedo, FL 32765','10465 Stapeley Drive, Orlando, FL 32832','11326 Wishing Well Lane, Clermont, FL 34711','16532 Deer Chase Loop, Orlando, FL 32828','3520 Hollow Oak Run, Oviedo, FL 32766','1001 Pine Lane, Saint Cloud, FL 34771','3400 Canvas Street, Kissimmee, FL 34741','5269 Hammock Circle, Saint Cloud, FL 34771','811 Carrigan Avenue, Oviedo, FL 32765','2333 Avellino Avenue, Saint Cloud, FL 34771','1115 Royalton Road, Orlando, FL 32825','3513 Pintail Lane, Saint Cloud, FL 34772','531 Crownclover Avenue, Orlando, FL 32828','2983 Sabel Oak Place, Oviedo, FL 32765','3960 Liberty Hill Drive, Clermont, FL 34711','13258 Long Pine Trail, Clermont, FL 34711','9931 Sublette Avenue, Orlando, FL 32836','1115 Robert Ridge Court, Kissimmee, FL 34747','187 Osprey Lakes Circle, Chuluota, FL 32766','14837 Porter Road, Winter Garden, FL 34787','6647 Beth Road, Orlando, FL 32824','2305 Zaltana Circle, Minneola, FL 34715','825 Kingsbridge Drive, Oviedo, FL 32765','13931 Vista Del Lago Boulevard, Clermont, FL 34711','15703 Montesino Drive, Orlando, FL 32828','1028 Lascala Drive, Windermere, FL 34786','16014 Oak Spring Drive, Orlando, FL 32828','1620 Anna Catherine Drive, Orlando, FL 32828','14020 Fairway Willow Lane, Winter Garden, FL 34787','126 S Normandale Avenue, Orlando, FL 32835','5657 Pitch Pine Drive, Orlando, FL 32819','2831 Maria Isabel Avenue, Ocoee, FL 34761','2447 Osprey Woods Circle, Orlando, FL 32820','1639 Anna Catherine Drive, Orlando, FL 32828','2030 Sunset Terrace Drive, Orlando, FL 32825','8080 Indian Creek Boulevard, Kissimmee, FL 34747','2679 Willow Glen Circle, Kissimmee, FL 34744','907 Oak Chase Drive, Orlando, FL 32828','1571 Cheri Court, Kissimmee, FL 34744','1730 Connecticut Avenue, Saint Cloud, FL 34769','145 Summer Place Loop, Clermont, FL 34714','13619 Guildhall Circle, Orlando, FL 32828','12643 Cragside Lane, Windermere, FL 34786','17525 Silver Creek Court, Clermont, FL 34714','6321 Hidden Valley Court, Orlando, FL 32819','2813 Monticello Way, Kissimmee, FL 34741','1473 Amanda Road, Kissimmee, FL 34744','2868 Aloma Lake Run, Oviedo, FL 32765','962 Forest Hill Drive, Minneola, FL 34715','2881 Citron Drive, Longwood, FL 32779','3925 Whittington Drive, Orlando, FL 32817','15639 Carriage Hill Court, Clermont, FL 34711','14901 White Magnolia Court, Orlando, FL 32824','2769 Monticello Way, Kissimmee, FL 34741','1326 Blackwater Pond Drive, Orlando, FL 32828','2835 Kemper Avenue, Orlando, FL 32814','10029 Cheshunt Drive, Orlando, FL 32817','14863 Royal Poinciana Drive, Orlando, FL 32828','1710 Billingshurst Court, Orlando, FL 32825','4964 Southfork Ranch Drive, Orlando, FL 32812','1721 Cheltenborough Drive, Orlando, FL 32835','732 Pond Pine Court, Orlando, FL 32825','512 Carey Way, Orlando, FL 32825','1898 Gregory Road, Orlando, FL 32825','812 Rosemere Circle, Orlando, FL 32835','2782 Roccella Court, Kissimmee, FL 34747','1562 Setting Sun Court, Clermont, FL 34711','13239 Canna Lily Drive, Orlando, FL 32824','307 South Oakland Avenue, Minneola, FL 34715','10418 Lago Louisa Court, Clermont, FL 34711','2788 Kissimmee Bay Circle, Kissimmee, FL 34744','137 Thornbury Drive, Kissimmee, FL 34744','1007 Shady Maple Circle, Ocoee, FL 34761','473 Turnstone Way, Orlando, FL 32828','3508 Vinning Court, Kissimmee, FL 34741','672 Carrigan Woods Trail, Oviedo, FL 32765','217 Magical Way, Kissimmee, FL 34744','16045 Magnolia Hill Street, Clermont, FL 34714','1222 Molona Street, Reunion, FL 34747','837 Clifton Hills Street, Orlando, FL 32828','6029 Silk Oak Drive, Orlando, FL 32819','4125 Scarlet Branch Road, Orlando, FL 32824','13406 Meadow Bay Loop, Orlando, FL 32824','2032 Kaylas Court, Orlando, FL 32817','1030 Elizabeth Ridge Court, Kissimmee, FL 34747','5043 Tempic Drive, Mount Dora, FL 32757','2705 Portchester Court, Kissimmee, FL 34744','34145 Fortunado Street, Sorrento, FL 32776','10283 Kristen Park Drive, Orlando, FL 32832','116 Milinda Lane, Oviedo, FL 32765','5204 Hammock Pointe Court, Saint Cloud, FL 34771','8966 Vickroy Terrace, Oviedo, FL 32765','12924 Lower River Boulevard, Orlando, FL 32828','3323 Shrike Hill Court, Clermont, FL 34714','4737 Blue Major Drive, Windermere, FL 34786','32807 Wolfs Trail, Sorrento, FL 32776','1213 Burning Tree Lane, Winter Park, FL 32792','7116 Calm Cove Court, Windermere, FL 34786','10637 Deergrass Lane, Orlando, FL 32821','921 La Terraza Lane, Kissimmee, FL 34744','34038 Fortunado Street, Sorrento, FL 32776','113 Red Bay Drive, Longwood, FL 32779','1011 Lascala Drive, Windermere, FL 34786','811 Brandy Oaks Loop, Winter Garden, FL 34787','1457 Resolute Street, Kissimmee, FL 34747','9605 Buoy Court, Orlando, FL 32817','14080 Bradbury Road, Orlando, FL 32828','6357 Sedona Leaf Court, Windermere, FL 34786','13817 Eagles Glen Court, Orlando, FL 32837','4719 Fort Knox Court, Orlando, FL 32822','321 Giovani Boulevard, Clermont, FL 34715','1772 Leather Fern Drive, Ocoee, FL 34761','4070 La Salle Avenue, Saint Cloud, FL 34772','532 Wekiva Cove Road, Longwood, FL 32779','7631 Redwood Country Road, Orlando, FL 32835','501 Rebecca Drive, Saint Cloud, FL 34769','13338 Phoenix Drive, Orlando, FL 32828','7050 Buckhorn Trail, Saint Cloud, FL 34771','1119 Lake Legro Court, Orlando, FL 32835','4747 Cains Wren Trail, Sanford, FL 32771','1014 FAIRCLOTH CT, Oviedo, FL 32765','3137 HOLLAND DR, Orlando, FL 32825','2521 TEAK COURT, Kissimmee, FL 34743','754 Woodside Road, Maitland, FL 32751','954 LONGWOOD CLUB PLACE, Longwood, FL 32750','1923 SEATONHAM STREET, Winter Garden, FL 34787','1040 ROYAL MARQUIS CIRCLE, Ocoee, FL 34761','3456 BUFFAM PLACE, Casselberry, FL 32707','1342 JECENIA BLOSSOM DRIVE, Apopka, FL 32712','559 HORSEMAN DRIVE, Oviedo, FL 32765','4671 CYPRESS RIVER ROAD, Saint Cloud, FL 34772','1825 MORGANS MILL CIRCLE, Orlando, FL 32825','23503 VALDERAMA LANE, Sorrento, FL 32776','13511 TETHERLINE TRL, Orlando, FL 32837','11845 SHELTERING PINE DRIVE, Orlando, FL 32836','2100 RICKOVER PLACE, Winter Garden, FL 34787','2306 TREYMORE DRIVE, Orlando, FL 32825','6559 LAKE PEMBROKE PLACE, Orlando, FL 32829','1937 DERBY GLEN DRIVE, Orlando, FL 32837','482 ORIONVISTA WAY, Oakland, FL 34787','13122 MOSS PARK RIDGE DRIVE, Orlando, FL 32832','1412 FOXFORREST CIRCLE, Apopka, FL 32712','550 LYNN STREET, Oviedo, FL 32765','10500 EASTPARK WOODS DRIVE, Orlando, FL 32832','1187 LAKE FRANCIS DRIVE, Apopka, FL 32712','1430 COLUSO DRIVE, Winter Garden, FL 34787','902 MOSSHART LANE, Orlando, FL 32825']
      # sts = ['100 CLOVERDALE CT, Hendersonville, Tn 37075','1005 Matheus Drive, Murfreesboro, Tn 37128','1006 Sky Valley Trail, Smyrna, Tn 37167','1007 Lassiter Drive, Goodlettsville, Tn 37072','1012 Valley Dr, Goodlettsville, Tn 37072','1013 ARCADIA COURT, ANTIOCH, Tn 37013','1017 Treasure Reef, Madison, Tn 37115','1018 Cornwall Court, Murfreesboro, Tn ','102 Grapevine Road, Hendersonville, Tn 37075','102 Marcus Circle, Murfreesboro, Tn 37130','102 Skylark Drive, Murfreesboro, Tn ','1024 Hammack Court, Nashville, Tn 37214','103 Lisa Circle, Lebanon, Tn 37087','1036 Blue Mountain Lane, Antioch, Tn 37013','104 Susan Drive, Hendersonville, Tn ','1048 Campbell Ave, Gallatin, Tn 37066','105 Cataract Drive, Murfreesboro, Tn 37129','105 Elizer Court, Hendersonville, Tn 37075','105 Guinevere Court, Murfreesboro, Tn ','106 Four Season Dr, Murfreesboro, Tn 37129','107 Timberview Circle, LaVergne, Tn 37086','108 Crystal Court, Nashville, Tn ','108 Schooner Ct, Hermitage, Tn ','108 Sunset, Mount Juliet, Tn 37122','108 Woodcraft Court, Nashville, Tn ','109 Hunters Chase Drive, Smyrna, Tn 37167','109 Painter Drive, Antioch, Tn 37013','1095 Gray Bill Drive, Gallatin, Tn 37066','1104 Brittany Park Lane, Antioch, Tn 37013','1106 Gardendale Drive, Murfreesboro, Tn 37130','1107 Jerico Road, Murfreesboro, Tn 37130','1108 Woburn Way, Antioch, Tn 37013','1109 Geneil Lane, LaVergne, Tn 37086','111 Bentree Drive, Hendersonville, Tn 37075','111 Deer Ridge Lane, Hendersonville, Tn ','111 Sandhill Rd, LaVergne, Tn 37086','1110 Mary Evelyn Court, Nashville, Tn ','1111 Alandee, Nashville, Tn 37214','111111 DELETED, LaVergne, Tn 37086','1113 Shallowbrook Trl S, Antioch, Tn 37013','1114 McDonough Circle, Thompsons Station, Tn 37179','1115 Gardendale Dr, Murfreesboro, Tn 37130','112 Haney Court, Murfreesboro, Tn 37128','113 Lindy Murff Court, Antioch, Tn ','114 Scotch Street, Hendersonville, Tn 37075','115 Bradford Circle, Hendersonville, Tn 37075','115 Cartwright Parkway, Goodlettsville, Tn ','115 Summerlake Place, Hendersonville, Tn 37075','1175 Bothwell Place, Gallatin, Tn 37066','118 Ivanhoe Court, Murfreesboro, Tn 37127','1193 sioux terrace, Madison, Tn 37115','1200 Timber Valley Drive, Nashville, Tn ','1204 Lasalle Court, Smyrna, Tn ','1205 Timber Valley Drive, Nashville, Tn 37214','121 Lakeside Park Drive, Hendersonville, Tn 37075','121 WILLIAMS VALLEY COURT, MADISON, Tn ','121 Williams Valley Ct, Madison, Tn 37115','122 Stillhouse Road, Hendersonville, Tn ','1220 Alandee Street, Nashville, Tn ','1222 Westridge Court, Antioch, Tn 37013','1229 Langwood Court, Gallatin, Tn 37066','1233 Shannon Ln, LaVergne, Tn 37086','124 Trace Drive, Goodlettsville, Tn 37072','125 Matheus Court, Murfreesboro, Tn ','1264 Dalmally Dr, Murfreesboro, Tn 37128','130 Agee Circle East, Hendersonville, Tn 37075','1303 Amberwood Circle, Murfreesboro, Tn ','1304 Springwood Ct, LaVergne, Tn 37086','1306 Saint hubbins drive, LAVERGNE, Tn 37086','131 Two Valley Road, Hendersonville, Tn 37075','1313 Rainglen Cove, Antioch, Tn 37013','1317 Bundy Drive, Smyrna, Tn 37167','1318 Georgetown, Old Hickory, Tn 37138','134 Ben Allen Rd, Nashville, Tn 37207','1340 Cutoff Rd, Murfreesboro, Tn 37129','1342 Nicole Ln, LaVergne, Tn 37086','1345 Shearron Court, Murfreesboro, Tn 37130','136 Dennis Rd, Hendersonville, Tn ','1401 Woodchimes Ct, Hermitage, Tn 37076','1402 Ardee, LaVergne, Tn 37086','143 Lane Drive, Mount Juliet, Tn 37122','1433 Middleborough Court, Murfreesboro, Tn 37130','1436 Pawnee Trail, Madison, Tn 37115','1438 Middleborough Court, Murfreesboro, Tn 37130','1438 Middleborough Ct, Murfreesboro, Tn 37130','1440 Middleborough Court, Murfreesboro, Tn 37130','146 Hickory Heights Drive, Hendersonville, Tn 37075','1505 Elm Run Court, Nashville, Tn ','1505 Heritage View Boulevard, Madison, Tn ','1507 CARDINAL LANE, Mount Juliet, Tn ','1512 Brookchase Court, Antioch, Tn 37013','1523 Journey Drive, Murfreesboro, Tn 37130','1533 Passage Drive, Murfreesboro, Tn 37130','1542 River Rock Boulevard, Murfreesboro, Tn ','1550 James Gafford Drive, LaVergne, Tn 37086','1570 Ridgemont Drive, LaVergne, Tn 37086','1615 Pacer Ct, Spring Hill, Tn 37174','1619 Allston Drive, Murfreesboro, Tn 37128','163 Lakeside Park Drive, Hendersonville, Tn 37075','1637 O Green Cv, Murfreesboro, Tn 37129','1701 Pine Crest Court, Mount Juliet, Tn 37122','1704 Carrington Court, Old Hickory, Tn 37138','1712 Jimmy Cove, LaVergne, Tn 37086','1728 Westerwald Drive, Murfreesboro, Tn 37127','1736 Londonview Place, Antioch, Tn 37013','1742 Freiburg Drive, Spring Hill, Tn 37174','1761 Heritage Glen Dr, Madison, Tn 37115','1772 Londonview Place, Antioch, Tn 37013','1805 Lakewood Village Drive, Antioch, Tn ','1821 Baslia Lane, Spring Hill, Tn 37174','1825 Annalee Drive, Antioch, Tn 37013','1834 Turnstone Court, Murfreesboro, Tn ','185 Center St, LaVergne, Tn 37086','1877 Brookmeadow Ln, Hermitage, Tn 37076','1880 Portway Rd, Spring Hill, Tn 37174','1903 Sean Cove, LaVergne, Tn ','1906 Venada Trail, Murfreesboro, Tn 37128','1913 Deep Woods Trail, Nashville, Tn 37214','1999 Waterford Dr, Old Hickory, Tn 37138','2003 Eagle Ct, Spring Hill, Tn 37174','2009 Remington Park Road, Hermitage, Tn 37076','202 Berrywood Drive, Smyrna, Tn 37167','202 Bluewater Court, LaVergne, Tn ','202 Fairfield Drive, Smyrna, Tn 37167','2022 Gardendale Drive, Murfreesboro, Tn 37130','203 Sierra Drive, Murfreesboro, Tn 37129','2031 Gardendale Drive, Murfreesboro, Tn ','204 IVY DRIVE, HENDERSONVILLE, Tn 37075','2041 Williams Valley Drive, Madison, Tn ','205 Mercury Drive, LaVergne, Tn 37086','205 Turtle Rock, Murfreesboro, Tn 37129','208 Orchard Valley Drive, Smyrna, Tn ','211 Deerlake Court, LaVergne, Tn 37086','2112 Caladonia Way, Smyrna, Tn 37167','212 Hedgeway, Gallatin, Tn 37066','2127 Ponty Pool Drive, Mount Juliet, Tn 37122','2157 Aberdeen, Murfreesboro, Tn 37130','216 Broadmoor Drive, Nashville, Tn 37207','2164 Spring Hill Circle, Spring Hill, Tn 37174','219 Cherry Hill Drive, Hendersonville, Tn 37075','2210 Burr Court, Murfreesboro, Tn 37129','223 Oak Point Ln, Mount Juliet, Tn ','2235 Brick Way, Murfreesboro, Tn 37130','225 Loxley Lane, Murfreesboro, Tn 37127','227 Shrewsbury Dr, Murfreesboro, Tn 37129','2283 Nonaville Rd, Mount Juliet, Tn 37122','229 LAKE FOREST DRIVE, LAVERGNE, Tn 37086','229 Slayton Drive, Madison, Tn 37115','2333 Cooper Terrace, Nashville, Tn 37216','2349 Hockett Drive, Nashville, Tn 37218','2420 S Bellah Court, Murfreesboro, Tn ','2427 Grover Street, Nashville, Tn 37207','246 Sunny Acre Drive, Mount Juliet, Tn ','2513 Gold Valley Drive, Murfreesboro, Tn 37130','2527 Lone Oak Circle, Murfreesboro, Tn 37128','2543 Medford Campbell Boulevard, Murfreesboro, Tn 37130','2550 Bethwood Drive, Nashville, Tn ','2600 Ravine Drive, Nashville, Tn 37217','2601 Mesa Dr, Nashville, Tn 37217','2623 Apostle Lane, Murfreesboro, Tn ','2688 Pine Ridge Rd, Nashville, Tn ','2704 Old Smith Springs Road, Nashville, Tn 37217','2711 Oxford Hall Ave, Murfreesboro, Tn 37128','2731 Roscommon Dr, Murfreesboro, Tn 37128','2740 Welshcrest Dr, Antioch, Tn 37013','2743 Mollys Court, Spring Hill, Tn ','2748 Windwalker Court, Murfreesboro, Tn 37128','2751 Oxford Hall Ave, Murfreesboro, Tn 37128','2808 Candlewicke Drive, Spring Hill, Tn ','2815 Creekbend Dr, Nashville, Tn 37207','2816 Penn Meade Drive, Nashville, Tn ','2818 Rachel Lane, Thompson Station, Tn 37179','2840 Cochran Trace Dr, Spring Hill, Tn 37174','2849 Paddle Wheel Drive, Nashville, Tn 37214','2864 Meadow Glen, Mount Juliet, Tn 37122','290 Indian Park Dr, Murfreesboro, Tn ','290 Old Nashville Hwy, La Vergne, Tn ','2903 WESTCHESTER COURT, Old Hickory, Tn 37138','2904 Torrence Trl, Spring Hill, Tn 37174','2907 Checkers Court, Spring Hill, Tn 37174','2914 Kedzie Drive, Murfreesboro, Tn 37130','2921 Katonka Ct, Antioch, Tn 37013','2941 Windemere Drive, Murfreesboro, Tn ','2982 Bridle Dr, Murfreesboro, Tn 37129','301 Canberra Ct, Hermitage, Tn 37076','3021 Holsted Drive, Murfreesboro, Tn 37128','3022 Barfield Road, Murfreesboro, Tn 37128','3022 Roellen Road, Murfreesboro, Tn ','3027 Cliffside Dr, Christiana, Tn 37037','3036 Ewngdale Drive, Nashville, Tn ','3036 PENN MEADE WAY, Nashville, Tn 37214','304 Sherry Cove, LaVergne, Tn ','3042 Weybridge Drive, Murfreesboro, Tn ','305 Clearlake Drive, LaVergne, Tn ','305 Country Village Dr, Smyrna, Tn 37167','305 Short Court, Smyrna, Tn 37167','3063 ACE WINTERMEYER DR., LaVergne, Tn 37086','307 Liberty Lane, LaVergne, Tn 37086','3073 Reelfoot Drive, Nashville, Tn 37214','308 Griffin Place, Hermitage, Tn 37076','308 Swanridge Court, Smyrna, Tn ','3105 Kennebeck Place, Antioch, Tn 37013','3107 Priest Woods Dr, Nashville, Tn ','3109 Windsail Trail, Antioch, Tn 37013','3121 Barksdale Harbor Drive, Nashville, Tn 37214','3142 E Lake Drive, Nashville, Tn 37214','317 Cedar Park Circle, LaVergne, Tn ','3213 Crowe Drive, Nashville, Tn ','323 Donna Drive, Hendersonville, Tn 37075','3255 Priest Woods Dr, Nashville, Tn 37214','327 Merlin Dr, Murfreesboro, Tn 37127','3273 Anderson Road, Antioch, Tn ','329 Stewart Springs Drive, Smyrna, Tn 37167','3300 Towne Village Road, Antioch, Tn 37013','3305 Penn Meade Way, Nashville, Tn ','3308 Towne Village Road, Antioch, Tn 37013','3340 Fletz Circle, Murfreesboro, Tn ','3348 Calais Circle, Antioch, Tn ','3440 White Pine Drive, Nashville, Tn 37214','3444 Daisy Trail, Antioch, Tn ','3505 Cobble St, Nashville, Tn 37211','3512 Roundwood Forest Drive, Antioch, Tn 37013','3524 Roundwood Forest Dr, Antioch, Tn 37013','355 Spring Valley Drive, Mount Juliet, Tn 37122','359 Clearlake Drive, La Vergne, Tn 37086','3605 Jerry Anderson Drive, Murfreesboro, Tn 37128','3717 Willow Creek Road, Nashville, Tn ','3874 Nestledown Drive, Murfreesboro, Tn 37129','400 Silver Springs Lane, Mount Juliet, Tn 37122','4007 Cadence Drive, Spring Hill, Tn ','4009 Affirmed Drive, Mount Juliet, Tn ','4009 Cadence Dr, Spring Hill, Tn 37174','401 Breckinridge Dr, Smyrna, Tn 37167','401 Sunset Dr, Mount Juliet, Tn 37122','4016 Deer Run Trace, Spring Hill, Tn 37174','402 Newbary Court, Franklin, Tn 37069','403 Indian Hills Ct, Gallatin, Tn 37066','403 SILVER SPRINGS LANE, Mount Juliet, Tn 37122','4037 Moss Rd, Antioch, Tn 37013','404 Woodsman Court, Nashville, Tn 37214','4044 LANCASHIRE DRIVE, Antioch, Tn ','405 Wooded Valley Ct, LaVergne, Tn 37086','4050 Lake Parkway, Hermitage, Tn 37076','407 Shadetree Ct, Nashville, Tn 37207','408 Lake Forest Drive, LaVergne, Tn ','411 SUNSET DRIVE, Mount Juliet, Tn 37122','421 Scotts Creek Trail, Hermitage, Tn 37076','4232 Chesney Glen Drive, Hermitage, Tn ','4233 Sandstone Drive, Antioch, Tn 37013','424 Dorchester Pl, Gallatin, Tn 37066','425 Whispering Drive, Spring Hill, Tn 37174','426 Tampa Drive, Nashville, Tn 37211','4273 Rachel Donelson Pass, Hermitage, Tn ','4300 Baton Rouge Drive, Hermitage, Tn 37076','4409 SANDPIPER LANE, ANTIOCH, Tn ','4424 Lavergne Couchville Pike, Antioch, Tn 37013','4425 Frost Street, Nashville, Tn 37214','444 Jessie Drive, Nashville, Tn ','4516 Hunters Green Cir, Antioch, Tn 37013','4517 Highland Ridge Drive, Antioch, Tn ','4549 Raccoon Trail, Hermitage, Tn ','4587 Artelia Drive, Antioch, Tn 37013','4620 Raccoon Trail, Hermitage, Tn 37076','468 Cedar Park Circle, LaVergne, Tn 37086','4717 Phoenix Dr, Hermitage, Tn ','4728 Greystone St, Antioch, Tn 37013','4728 Greystone Street, Antioch, Tn 37013','4729 Ashley Way, Hermitage, Tn 37076','480 Avery Valley Drive, Smyrna, Tn 37167','4816 Cape Hope Pass, Hermitage, Tn 37076','4824 Leesa Ann Lane, Hermitage, Tn 37076','4920 Oak Barrel Dr, Antioch, Tn 37013','4932 Alexis Dr, Antioch, Tn 37013','4951 Laura Jeanne Blvd, Murfreesboro, Tn 37129','4953 Laura Jeanne Bolvard, Murfreesboro, Tn 37129','501 Chinquapin Pl, Smyrna, Tn 37167','5026 Nina Marie Avenue, Murfreesboro, Tn 37129','505 Connie Ct, Smyrna, Tn ','505 Suzanne Court, Mount Juliet, Tn ','509 Dutchmans Drive, Hermitage, Tn ','5104 Patience Drive, Smyrna, Tn 37167','5104 Roxborough Drive, Hermitage, Tn 37076','5130 Jerickia Court, Murfreesboro, Tn 37129','5139 Hunters Point Ln, Hermitage, Tn 37076','5169 Preserve Blvd, Antioch, Tn 37013','520 RHODES CIRCLE, Gallatin, Tn 37066','521 HIGHLAND VIEW DR., Hermitage, Tn 37076','529 Jasmine Drive, Nashville, Tn 37211','603 N. Greenwood EXT, Lebanon, Tn 37087','604 Lake Terrace Drive, Nashville, Tn 37217','604 Whirlaway Drive, Antioch, Tn 37013','608 Blake Moore Drive, LaVergne, Tn 37086','608 Cayla Court, Smyrna, Tn ','608 Hidden Hill Dr, Hermitage, Tn 37076','609 CREEKFRONT DRIVE, Mount Juliet, Tn ','6121 Terry Drive, Nashville, Tn 37209','613 Denver Dr, Hermitage, Tn 37076','614 Federal Court, Murfreesboro, Tn 37129','6200 Tuckaleechee Lane, Antioch, Tn 37013','6204 Rocky Top Drive, Antioch, Tn 37013','628 Meadowview Dr, Mount Juliet, Tn ','632 Granwood Blvd, Old Hickory, Tn 37138','6441 Kari Drive, Murfreesboro, Tn ','6507 Forrest Lane, Murfreesboro, Tn ','6559 Kari Drive, Murfreesboro, Tn 37129','6736 Forrest Lane, Murfreesboro, Tn 37129','690 Holland Ridge Drive, LaVergne, Tn 37086','6980 Calderwood Drive, Antioch, Tn 37013','701 Clear Circle, Smyrna, Tn ','704 Alisa Cove, LaVergne, Tn ','705 Meade Drive, Spring Hill, Tn 37174','706 Judge Mason Way, LaVergne, Tn 37086','709 Pinwheel Court, Antioch, Tn ','7096 S Hampton Boulevard, Antioch, Tn 37013','7101 S Hampton Blvd, Antioch, Tn ','711 Kennington North, Nashville, Tn ','7152 Legacy Dr, Antioch, Tn 37013','716 William Howard Place, Nashville, Tn 37209','721 Woodcraft Drive, Nashville, Tn 37214','731 Hogan Drive, Murfreesboro, Tn 37128','7316 Ole Nottingham Drive, Antioch, Tn 37013','733 Woodcraft, Nashville, Tn 37214','738 Middleton Lane, Murfreesboro, Tn ','7481 Maggie Dr, Cane Ridge, Tn 37013','7545 W Winchester Drive, Antioch, Tn ','779 Curd Road, Mount Juliet, Tn 37122','801 Lake Terrace, Nashville, Tn ','805 Pebble Beach Circle, Mount Juliet, Tn 37122','814 General Forrest Drive, Smyrna, Tn 37167','817 Chilhowee Court, Smyrna, Tn 37167','8192 RAMSTONE WAY, ANTIOCH, Tn 37013','820 Netherlands Drive, Hermitage, Tn ','820 Stone Flower Ct, Antioch, Tn 37013','823 Buckhaven Drive, Smyrna, Tn 37167','823 Netherlands Drive, Hermitage, Tn ','825 Stoner Mill Lane, Hermitage, Tn 37076','8276 Ramstone Way, Antioch, Tn ','839 Netherlands Dr, Hermitage, Tn ','839 Netherlands Drive, Hermitage, Tn ','8604 Wayne St, Murfreesboro, Tn 37129','894 Lavergne Lane, LaVergne, Tn 37086','902 Bellwood Dr, Mount Juliet, Tn 37122','907 Lance King Court, LaVergne, Tn ','910 Emmett Ave, Nashville, Tn 37206','911 Tapoco Ct, Smyrna, Tn 37167','913 Tom Hailey Boulevard, LaVergne, Tn ','916 N 2ND Street, Nashville, Tn 37207','943 Strand Fleet Drive, Antioch, Tn 37013','969 Norwalk Dr, Nashville, Tn 37214','03 Cedar Bend Lane, LaVergne, Tn 37086','104 Camas Ct, Christiana, Tn 37037','109 Louann Lane, Hendersonville, Tn ','1090 Williamson Road, Goodlettsville, Tn 37072','1102 Williamson Road, Goodlettsville, Tn 37072','113 New Haven Court, Nashville, Tn 37214','127 Riverwood Dr, LaVergne, Tn 37086','134 Hillsdale Drive, Hendersonville, Tn 37075','138 Keenan Ln, Mount Juliet, Tn 37122','1411 Ashlawn Dr, Murfreesboro, Tn 37128','1412 Chutney Court, Antioch, Tn 37013','1517 Rochester Drive, Murfreesboro, Tn 37130','1547 John Galt Drive, Lebanon, Tn 37087','1605 Ogletree Court, La Vergne, Tn 37086','1724 Greenhard Court, ANTIOCH, Tn 37013','1740 Woodland Pointe Drive, Nashville, Tn 37214','1834 Turnstone Ct, Murfreesboro, Tn 37128','1996 Waterford Dr, Old Hickory, Tn 37138','202 Sycamore Drive, Smyrna, Tn ','207 Taylor Drive, LaVergne, Tn ','2105 Argo Place, Nashville, Tn 37211','217 S Valley Road, Hendersonville, Tn ','2204 Crescent Valley Lane, Hermitage, Tn 37076','225 Ash Drive, Franklin, Tn 37064','2331 Chandler Pl, Murfreesboro, Tn 37130','249 Walnut Grove Road, Christiana, Tn 37037','2600 Sailboat Court, Nashville, Tn 37217','2728 Aston Woods Lane, Thompsons Station, Tn ','30 Jay Street, Nashville, Tn 37210','3009 Sunnyview Drive, Nashville, Tn 37207','3046 High Rigger Drive, Nashville, Tn 37217','3152 Kinwood Dr, Antioch, Tn 37013','3269 Luann Drive, Antioch, Tn 37013','3308 E Lake Drive, Nashville, Tn 37214','3343 Oak Trees Court, Antioch, Tn ','3462 Clegg Drive, Spring Hill, Tn 37174','3540 Anderson Road, Nashville, Tn 37217','3657 Rutherford Drive, Spring Hill, Tn 37174','3858 Priest Lake Drive, Nashville, Tn 37217','4752 Reischa Drive, Antioch, Tn 37013','483 Belinda Parkway, Mount Juliet, Tn 37122','4865 Indian Summer Drive, Nashville, Tn 37207','509 Rockwood Drive, Hermitage, Tn 37076','512 Allen Drive, Franklin, Tn 37064','6044 Panama Drive, Hermitage, Tn 37076','6301 Watsonwood Court, Murfreesboro, Tn 37129','7117 Lone Eagle Court, Murfreesboro, Tn 37128','7434 Powells Chapel Road, Murfreesboro, Tn ','7500 W Winchester Drive, Antioch, Tn ','808 Clearspring Ct, Smyrna, Tn 37167','912 E Cynthia Trail, Goodlettsville, Tn 37072','912 Hammack Drive, Nashville, Tn 37214','923 Strand Fleet Drive, Antioch, Tn 37013','926 B N 6Th Street, Nashville, Tn 37207','7208 Autumn Crossing Way, Brentwood, Tn 37027','106 Sorrel Court, Hendersonville, Tn 37075','259 Keswick Grove Lane, FRANKLIN, Tn 37067','6391 Fischer Court, Brentwood, Tn 37027','105 Harkness Court, Smyrna, Tn 37167','1108 Poplar Hollow Rd., La Vergne, Tn 37086','128 Windward Drive, Mount Juliet, Tn 37122','421 Coles Ferry Road, Gallatin, Tn 37066','4873 Everest Drive, Old Hickory, Tn 37138','118 Cloverdale Court, Hendersonville, Tn 37075','1800 Cottage Grove Way, Antioch, Tn 37013','239 Indian Park Drive, Murfreesboro, Tn 37128','3107 Wellington Place, Murfreesboro, Tn 37128','391 Tapestry Place Unit 101, Gallatin, Tn 37066','5320 Skip Jack Drive, Antioch, Tn 37013','917 Crystal Bear Trail, Murfreesboro, Tn 37218','1009 Stonemark Trail, LaVergne, Tn 37086','110 Meadow Street, Old Hickory, Tn 37138','1171 Osborne Lane, Murfreesboro, Tn 37130','1408 Ardee Drive, LaVergne, Tn 37086','1413 Wakonda Court, Murfreesboro, Tn 37130','1434 Tuffnell Drive, LaVergne, Tn 37086','1621 Rice Hill Court, Antioch, Tn 37013','184 Center Street, LaVergne, Tn 37086','2005 Lassiter Drive, Goodlettsville, Tn 37072','2130 Spring Hill Circle, Spring Hill, Tn 37174','233 Lake Forest Drive, LaVergne, Tn 37086','2406 Hillmont Drive, Murfreesboro, Tn 37129','243 Lake Forest Drive, LaVergne, Tn 37086','3621 Arcadia Circle, Antioch, Tn 37013','4277 Valley Grove Drive, Hermitage, Tn 37076','428 Brownstone Street, Old Hickory, Tn 37138','431 Nyu Place, Murfreesboro, Tn 37128','432 Cedar Park Circle, LaVergne, Tn 37086','434 Cedar Park Circle, LaVergne, Tn 37086','4913 Alexis Drive, Cane Ridge, Tn 37013','510 Debbie Drive, Hermitage, Tn 37076','515 Cedar Park Circle, LaVergne, Tn 37086','637 Wildwood Drive, Smyrna, Tn 37167','7015 Westfork Drive, Smyrna, Tn 37167','7815 Rainey Drive, Antioch, Tn 37013','805 Park Court South, LaVergne, Tn 37086','912 Wildrose Court, Smyrna, Tn 37167','1015 Compton Road, Murfreesboro, Tn 37130','108 Butleigh Court, Goodlettsville, Tn 37072','1115 Fitzpatrick Road, Nashville, Tn 37214','113 Elissa Drive, Hendersonville, Tn 37075','121 Lee Court, Gallatin, Tn 37066','154 Dreville Drive, LaVergne, Tn 37086','167 Timberlake Drive, Hendersonville, Tn 37075','1894 Brookmeadow Lane, Hermitage, Tn 37076','2017 Abingdon Drive, LaVergne, Tn 37086','2283 Joann Drive, Spring Hill, Tn 37174','250 Lake Forest Drive, LaVergne, Tn 37086','2610 Foxfire Court, Murfreesboro, Tn 37130','3016 Towne Valley Road, Antioch, Tn ','3208 Country Meadow Road, Antioch, Tn 37013','415 Hill Road, Nashville, Tn 37220','4837 Terragon Trail, Antioch, Tn 37013','501 Aerie Landing, Antioch, Tn 37013','520 Bellmore Place, Nashville, Tn 37209','664 Holland Ridge Drive, LaVergne, Tn 37086','1540 Mount Mitchell Court, Antioch, Tn 37013','2809 PADDLE WHEEL DR., NASHVILLE, Tn 37214','8 CHARLESMEADE DR, JACKSON, Tn 38305','1849 Pallas Drive, Madison, Tn 37115','2920 Pipkin Hills Drive, Spring Hill, Tn 37174','2972 Kinwood Drive, Antioch, Tn 37013','337 Clearlake Drive, LaVergne, Tn ','110 Long Rifle Road, Smyrna, Tn 37167','118 SUNSET CIRCLE, Mount Juliet, Tn 37122','1217 Eastwoods Drive, Murfreesboro, Tn 37130','1311 Teakwood Court, Murfreesboro, Tn ','1424 Timber Valley Drive, Nashville, Tn 37214','1708 Realtree Way, Antioch, Tn 37013','3429 Towneship Road, Antioch, Tn 37013','604 Dusk Court, Nashville, Tn 37221','641 Belgium Drive, Hermitage, Tn 37076','1006 Seven Oaks Blvd, Smyrna, Tn 37167','1007 Cheryl Lane, LaVergne, Tn 37086','1017 Rolling Meadow Drive, Mount Juliet, Tn ','102 Wortham Court, LaVergne, Tn ','106 Cavalier Drive, Hendersonville, Tn 37075','1080 Blue Mountain Lane, Antioch, Tn 37013','1110 Mahogany Trail, Murfreesboro, Tn 37130','1112 Shawnee Trace, Madison, Tn ','1115 Gardendale Drive, Murfreesboro, Tn 37130','1135 Mahogany Trail, Murfreesboro, Tn ','1136 Eastwoods Drive, Murfreesboro, Tn ','1144 Brittany Park Ln, Antioch, Tn 37013','116 Firelight Court, Antioch, Tn 37013','117 Forest Lane, LaVergne, Tn 37086','1209 Elisha Court, LaVergne, Tn 37086','1232 Jacksons Hill Road, Hermitage, Tn ','1276 Saint Andrews Drive, Murfreesboro, Tn ','1283 Tiree Dr, Murfreesboro, Tn 37128','131 Agee Cir E, Hendersonville, Tn 37075','1329 Tonya Drive, LaVergne, Tn 37086','1445 Westview Drive, Murfreesboro, Tn 37128','1508 JAMES GAFFORD DRIVE, LAVERGNE, Tn 37086','1519 Mohawk Trail, Madison, Tn 37115','1520 Joe Pyron, Madison, Tn 37115','158 Homestead Place, Hendersonville, Tn 37075','1615 Bill Eller Drive, LaVergne, Tn 37086','1624 Saint Andrews Drive, Murfreesboro, Tn ','1645 Comanche Run, Madison, Tn 37115','1734 Lucille Lane, Murfreesboro, Tn 37129','1805 Rory Cove, LaVergne, Tn 37086','1816 Hickory Lawn Court, Antioch, Tn 37013','1844 Cottage Grove Way, Antioch, Tn 37013','1917 Portview Drive, Spring Hill, Tn ','224 Mary Joe Martin Drive, LaVergne, Tn 37086','227 Meigs Drive, Murfreesboro, Tn 37128','230 Quiet Lane, LaVergne, Tn 37086','2322 Briar Bend Dr, Murfreesboro, Tn 37128','2414 Hillmont Drive, Murfreesboro, Tn 37129','2472 Saddlewood Ct, Murfreesboro, Tn 37128','2507 Maybrook Court, Murfreesboro, Tn ','2607 Edge O Lake Drive, Nashville, Tn 37217','2624 Gold Valley Drive, Murfreesboro, Tn 37130','2709 Calais Ct, Murfreesboro, Tn 37127','2719 Rocking Horse Lane, Murfreesboro, Tn 37130','279 Indian Park Drive, Murfreesboro, Tn 37128','2804 Halifax Ct, Old Hickory, Tn 37138','2812 Pinnacle Court, Spring Hill, Tn ','2813 Hydes Ferry Rd, Nashville, Tn 37218','3006 Henderson Lane, Murfreesboro, Tn 37130','3022 Tuckers Place, LaVergne, Tn ','303 Hunters Chase Drive, Smyrna, Tn ','304 University Drive, Gallatin, Tn 37066','3041 Delta Queen Dr, Nashville, Tn 37214','322 Elizabeth Ct, Smyrna, Tn 37167','3408 Hamberton Circle, Murfreesboro, Tn 37128','3501 Seasons Drive, Antioch, Tn 37013','3512 Grace Falls Dr, Antioch, Tn 37013','3514 Whitebud Lane, Murfreesboro, Tn 37128','4009 Pepperwood Drive, Antioch, Tn ','401 Wildwood Dr, Smyrna, Tn 37167','402 Rodney Street, Gallatin, Tn 37066','4112 Pepperwood Drive, Antioch, Tn 37013','417 Belinda Parkway, Mount Juliet, Tn 37122','4530 Baton Rouge Drive, Hermitage, Tn ','504 Fairhaven Court, Nashville, Tn ','5067 Boyd Drive, Mufreesboro, Tn 37129','508 Summit Way, Mount Juliet, Tn 37122','5188 Preserve Boulevard, Antioch, Tn 37013','5201 Alyadar Drive, Hermitage, Tn ','529 Summit Way, Mount Juliet, Tn 37122','616 Aideen Place, Madison, Tn 37115','622 McKean Drive, Smyrna, Tn ','627 Walton Ferry Road, Hendersonville, Tn 37075','652 Rocky Mountain Parkway, Antioch, Tn 37013','701 Lake Terrace Drive, Nashville, Tn 37217','709 Winchester Pl, Antioch, Tn 37013','720 Stewart Valley Drive, Smyrna, Tn ','781 Woodcrest Court, LaVergne, Tn 37086','800 Southwoods Drive, Smyrna, Tn 37167','805 Troy Cove, LaVergne, Tn 37086','8328 TRADING POST COURT, NASHVILLE, Tn 37221','921 Birchmill Point, Antioch, Tn 37013','150 Elm St, LaVergne, Tn 37086','1604 Timber Run, Nashville, Tn 37214','204 Redondo Court North, Hendersonville, Tn 37075','2383 Paddock Drive, Murfreesboro, Tn 37128','2712 Sewanee Place, Murfreesboro, Tn 37128','5072 English Village Drive, Nashville, Tn 37211','2007 Golden Court, Spring Hill, Tn 37174','2815 Maple Circle, Thompsons Station, Tn 37179','102 Raspberry Lane, Smyrna, Tn ','103 Peartree Drive, Hendersonville, Tn 37075','104 Valley View Court, Hendersonville, Tn 37075','107 Happy Street, Lebanon, Tn ','108 Jesse Brown Drive, Goodlettsville, Tn 37072','109 Natchez Court S, LaVergne, Tn ','109 Smith Springs Court, Nashville, Tn 37217','110 Longview Drive, Goodlettsville, Tn 37072','1106 Pyburns Place, LaVergne, Tn ','1120 Richard Lee Circle, Hermitage, Tn 37076','1155 Flat Stone Drive, Gallatin, Tn 37066','1168 Cynthia Lane, 37174, Tn 37174','1175 Fitzpatrick Road, Nashville, Tn 37214','118 Camino Circle, Hendersonville, Tn 37075','1205 Red Oak Court, Murfreesboro, Tn 37130','126 Edgewood Drive, Hendersonville, Tn 37075','1276 Blairfield Drive, Antioch, Tn 37013','1311 Owasa Trail, Murfreesboro, Tn 37130','132 Branford Drive, Smyrna, Tn 37167','132 Two Valley Rd, Hendersonville, Tn 37075','1406 Pagosa Court, Murfreesboro, Tn 37130','1421 Suffolk Court, LaVergne, Tn 37086','163 East Drive, Hendersonville, Tn 37075','1702 Erin Trace, LaVergne, Tn 37086','1718 Luton Drive, LaVergne, Tn 37086','1737 Stone Hollow Court, Hermitage, Tn ','174 Evergreen Circle, Hendersonville, Tn 37075','1838 Pecan Ridge Drive, Murfreesboro, Tn 37128','1913 Wax Wing Court, Mount Juliet, Tn 37122','2025 Barretts Ridge Drive, Murfreesboro, Tn ','2114 Erin Lane, Mount Juliet, Tn 37122','214 Brentmeade Drive, Murfreesboro, Tn 37130','217 Manatee Court, Antioch, Tn ','2227 Dewey Drive, Spring Hill, Tn 37174','2231 Dewey Drive, Spring Hill, Tn 37174','2306 Centertree Drive, Murfreesboro, Tn 37128','2328 Franco Road, Madison, Tn 37115','2602 Matchstick Place, Spring Hill, Tn ','2619 Apostle Lane, Murfreesboro, Tn 37129','2636 Dakota Way, Murfreesboro, Tn 37130','2688 Pine Ridge Road, Nashville, Tn 37207','279 Indian Park Drive, Murfreesboro, Tn 37128','2826 Castlerea Drive, Murfreesboro, Tn 37128','2860 Paddle Wheel Drive, Nashville, Tn 37214','2900 Wills Court, Spring Hill, Tn 37174','294 Townes Drive, Nashville, Tn 37211','2941 Bridle Drive, Murfreesboro, Tn ','3002 Burnt Pine Drive, Smyrna, Tn 37167','307 Davids Way, LaVergne, Tn ','3102 Barretts Ridge Drive, Murfreesboro, Tn ','3117 Kerrybrook Drive, Murfreesboro, Tn ','3120 Justin Towne Court, Antioch, Tn 37013','3128 Roundwood Forest Lane, Antioch, Tn 37013','3137 E Lake Drive, Nashville, Tn ','314 Sierra Drive, Murfreesboro, Tn 37129','3304 Monoco Drive, Spring Hill, Tn 37174','3518 Florence Road, Murfreesboro, Tn 37129','3846 Florence Road, Murfreesboro, Tn ','3877 Manson Pike, Murfreesboro, Tn 37129','3924 Pepperwood Drive, Antioch, Tn 37013','4025 Apache Trail, Antioch, Tn 37013','406 Anthony Branch Drive, Mount Juliet, Tn 37122','414 Rachels Court West, Hermitage, Tn ','422 Bonnawood, Hermitage, Tn 37076','4256 Chesney Glen Drive, Hermitage, Tn 37076','507 Morris Dr, Murfreesboro, Tn ','510 Gammon Lane, LaVergne, Tn ','511 Hidden Hill Drive, Hermitage, Tn 37076','523 MILESDALE LANE, SMYRNA, Tn 37167','619 Creekfront Drive, Mount Juliet, Tn 37122','636 Frankfort Drive, Hermitage, Tn 37076','639 Belinda Pkwy, Mount Juliet, Tn ','689 Whispering Breeze, Mount Juliet, Tn ','808 Trailside Circle, Antioch, Tn 37013','810 Holder Court, Nashville, Tn ','8309 Ramstone Way, Antioch, Tn ','9018 Nathanael Drive, LaVergne, Tn ','9018 Nevada Avenue, Smyrna, Tn 37167','534 Joe B Jackson Parkway, Murfreesboro, Tn 37127','538 Sandy Dr, Mount Juliet, Tn 37122','538 Summit Way, Mount Juliet, Tn ','540 Scotts Creek Trail, Hermitage, Tn 37076','5432 Village Way, Nashville, Tn ','5500 Escalade Drive, Mount Juliet, Tn 37122','5501 Fairhaven Drive, Nashville, Tn ','552 Highpoint Drive, Smyrna, Tn 37167','565 Mill Station Drive, Nashville, Tn 37207','576 Noel Drive, Mount Juliet, Tn 37122','5804 Foxview Drive, Antioch, Tn 37013','5906 Colchester Drive, Hermitage, Tn 37076','5913 New Hope Court, Hermitage, Tn 37076','5986 Monroe Crossing, Antioch, Tn 37013','601 Jean Street, Gallatin, Tn 37066']
      # sts = ['108 Schooner Ct, Hermitage','2527 Lone Oak, Murfreesboro, Tn 37128']
      # sts = ['108 Schooner Ct, Hermitage','111111 DELETED, LaVergne, Tn 37086','2527 Lone Oak, Murfreesboro, Tn 37128','2688 Pine Ridge, Nashville','4232 Chesney Glen Drive, Hermitage','4273 Rachel Donelson Pass, Hermitage','4549 Raccoon Trail, Hermitage','4717 Phoenix Dr, Hermitage ','509 Dutchmans Drive, Hermitage','711 Kennington North, Nashville','801 Lake Terrace, TN','820 Netherlands Drive, Hermitage','823 Netherlands Drive, Hermitage','839 Netherlands Dr, Hermitage','839 Netherlands Drive, Hermitage','03 Cedar Bend Lane, LaVergne, Tn 37086','4752 Reischa Drive, Tn 37013','917 Crystal Bear, TN','1232 Jacksons Hill, TN ','4530 Baton Rouge, TN ','5201 Alyadar Drive, TN ','1737 Stone Hollow Court, TN','414 Rachels Court, TN']
      # sts = ['313 ALPINE STREET','2508 SUGARSAND CT','2593 Breezy Meadow Road','1736 S WOODBURY CT','2832 BREEZY MEADOW','1342 JECENIA BLOSSOM DR','520 AZALEA BLOOM DR','2665 HASKILL HILL RD','2006 BEARDSLEY DR','1240 MAJESTIC OAK DR','4019 KNOTT DR','3952 LONG BRANCH LN','2525 EMERALD TREE LN','541 HEBRIDES COURT','2524 EMERALD TREE LN','4513 Barbados Loop','4431 Aruba Boulevard','3210 SAINT CROIX LANE','365 MARCO COURT','4556 Barbados Loop','3425 GLOSSY LEAF LN','12833 HYLAND LN','610 FIORELLA CT','437 KETTERING ROAD','829 W CHARING CROSS CIR','2695 Cahill Way','2206 BARKWOOD COURT','2950 Egrets Landing Drive','900 Beresford Way','2762 Snow Goose Lane','508 QUEENSBRIDGE DRIVE','512 W SPRINGTREE WAY','105 WHITLEY BAY LN','111 Bilsdale Court','243 E Hornbeam Drive','500 S SWEETWATER COVE BLVD','122 Roosevelt Place','2344 Falmouth Road','720 BUCHER RD','2071 GOLDWATER CT','3816 Mount Carmel Ln','2337 Deercroft Dr','1260 Olde Bailey Ln','2687 Deercroft Drive','2257 Deercroft Drive','710 Parnell Avenue','2010 SOUTHERN OAK LOOP','371 Pebble Ct','643 Woods Landing Drive','736 Arbor Pointe Avenue','20837 SULLIVAN RANCH BOULEVARD','30131 Tokara Terrace','2633 GREYWALL AVE','1393 PLUMGRASS CIRCLE','2648 PALASTRO WAY','11467 BROWN QUAIL CT','10362 Little Econ Street','1506 SUN MEADOW DR','12602 LYNCHBURG CT','5101 BELLTHORN DR','9730 MOSS ROSE WY','14511 Mandolin Drive','9156 LAKE AVON DR','2103 HAMMOCK MOSS DR','1063 Battery Pointe Drive','5223 ADAIR OAK DRIVE','8213 Villa Drive','5215 LOS PALMA VISTA DRIVE','1043 BURNETT STREET','559 HORSEMAN DR','2862 SWEETSPIRE CIR','875 Brookview Lane','989 Riviera Pointe Dr','981 Riviera Point Dr','1913 Maury Road','6007 COAL CT','105 HADLEY DR','3155 Burdock Ave','2301 Stratford Pointe','2544 Ventura Way','2852 Glasbern Circle','1062 Bainbury Lane','234 Sedgewood Circle','1566 Litchfield Drive','1024 Sedgewood Cir','1180 Olde Bailey Ln','2512 Glasbern Circle','4041 Joslin Way','1530 Corbett Ln','1020 Olde Bailey Ln','1247 Alaqua Way','2758 Madrigal Ln','3174 Burdock Ave','14455 YELLOW BUTTERFLY','5345 LEMON TWIST LANE','1456 Whitney Isles Dr.','11538 VIA LUCERNA CIR','222 TRADITIONS DR','14434 WHITTRIDGE DR','2661 Tierra Circle','2616 Verde Lane','1564 Lawndale Circle']
      sts = ['313 ALPINE STREET, Altamonte Springs, FL 32701','2508 SUGARSAND CT, APOPKA, FL 32712','2593 Breezy Meadow Road, Apopka, FL 32712','1736 S WOODBURY CT, Apopka, FL 32712','2832 BREEZY MEADOW, Apopka, FL 32712','1342 JECENIA BLOSSOM DR, Apopka, FL 32712','520 AZALEA BLOOM DR, Apopka, FL 32712','2665 HASKILL HILL RD, Apopka, FL 32712','2006 BEARDSLEY DR, Apopka, FL 32703','1240 MAJESTIC OAK DR, Apopka, FL 32712','4019 KNOTT DR, Apopka, FL 32712','3952 LONG BRANCH LN, Apopka, FL 32712','2525 EMERALD TREE LN, Apopka, FL 32712','541 HEBRIDES COURT, Apopka, FL 32712','2524 EMERALD TREE LN, Apopka, FL 32712','4513 Barbados Loop, Clermont, FL 34711','4431 Aruba Boulevard, Clermont, FL 34711','3210 SAINT CROIX LANE, Clermont, FL 34711','365 MARCO COURT, Clermont, FL 34715','4556 Barbados Loop, Clermont, FL 34711','3425 GLOSSY LEAF LN, Clermont, FL 34711','12833 HYLAND LN, Clermont, FL 34711','610 FIORELLA CT, Debary, FL 32713','437 KETTERING ROAD, Deltona, FL 32725','829 W CHARING CROSS CIR, LAKE MARY, FL 32746','2695 Cahill Way, Lake Mary, FL 32746','2206 BARKWOOD COURT, Lake Mary, FL 32746','2950 Egrets Landing Drive, Lake Mary, FL 32746','900 Beresford Way, Lake Mary, FL 32746','2762 Snow Goose Lane, Lake Mary, FL 32746','508 QUEENSBRIDGE DRIVE, Lake Mary, FL 32746','512 W SPRINGTREE WAY, Lake Mary, FL 32746','105 WHITLEY BAY LN, LONGWOOD, FL 32779','111 Bilsdale Court, Longwood, FL 32779','243 E Hornbeam Drive, Longwood, FL 32779','500 S SWEETWATER COVE BLVD, Longwood, FL 32779','122 Roosevelt Place, Maitland, FL 32751','2344 Falmouth Road, Maitland, FL 32751','720 BUCHER RD, MAITLAND, FL 32751','2071 GOLDWATER CT, Maitland, FL 32751','3816 Mount Carmel Ln, Melbourne, FL 32901','2337 Deercroft Dr, Melbourne, FL 32940','1260 Olde Bailey Ln, Melbourne, FL 32904','2687 Deercroft Drive, Melbourne, FL 32940','2257 Deercroft Drive, Melbourne, FL 32940','710 Parnell Avenue, Melbourne, FL 32901','2010 SOUTHERN OAK LOOP, Minneola, FL 34715','371 Pebble Ct, Minneola, FL 34715','643 Woods Landing Drive, Minneola, FL 34715','736 Arbor Pointe Avenue, Minneola, FL 34715','20837 SULLIVAN RANCH BOULEVARD, Mount Dora, FL 32757','30131 Tokara Terrace, Mount Dora, FL 32757','2633 GREYWALL AVE, Ocoee, FL 34761','1393 PLUMGRASS CIRCLE, Ocoee, FL 34761','2648 PALASTRO WAY, Ocoee, FL 34761','11467 BROWN QUAIL CT, ORLANDO, FL 32817','10362 Little Econ Street, Orlando, FL 32825','1506 SUN MEADOW DR, ORLANDO, FL 32824','12602 LYNCHBURG CT, ORLANDO, FL 32837','5101 BELLTHORN DR, Orlando, FL 32837','9730 MOSS ROSE WY, ORLANDO, FL 32832','14511 Mandolin Drive, Orlando, FL 32837','9156 LAKE AVON DR, ORLANDO, FL 32829','2103 HAMMOCK MOSS DR, Orlando, FL 32820','1063 Battery Pointe Drive, Orlando, FL 32828','5223 ADAIR OAK DRIVE, Orlando, FL 32829','8213 Villa Drive, Orlando, FL 32836','5215 LOS PALMA VISTA DRIVE, Orlando, FL 32837','1043 BURNETT STREET, Oviedo, FL 32765','559 HORSEMAN DR, Oviedo, FL 32765','2862 SWEETSPIRE CIR, Oviedo, FL 32766','875 Brookview Lane, Rockledge, FL 32955','989 Riviera Pointe Dr, Rockledge, FL 32955','981 Riviera Point Dr, Rockledge, FL 32955','1913 Maury Road, Saint Cloud, FL 34771','6007 COAL CT, Saint Cloud, FL 34772','105 HADLEY DR, SANFORD, FL 32771','3155 Burdock Ave, West Melbourne, FL 32904','2301 Stratford Pointe, West Melbourne, FL 32904','2544 Ventura Way, West Melbourne, FL 32904','2852 Glasbern Circle, West Melbourne, FL 32904','1062 Bainbury Lane, West Melbourne, FL 32904','234 Sedgewood Circle, West Melbourne, FL 32904','1566 Litchfield Drive, West Melbourne, FL 32904','1024 Sedgewood Cir, West Melbourne, FL 32904','1180 Olde Bailey Ln, West Melbourne, FL 32904','2512 Glasbern Circle, West Melbourne, FL 32904','4041 Joslin Way, West Melbourne, FL 32904','1530 Corbett Ln, West Melbourne, FL 32904','1020 Olde Bailey Ln, West Melbourne, FL 32904','1247 Alaqua Way, West Melbourne, FL 32904','2758 Madrigal Ln, West Melbourne, FL 32904','3174 Burdock Ave, West Melbourne, FL 32904','14455 YELLOW BUTTERFLY, Windermere, FL 34786','5345 LEMON TWIST LANE, Windermere, FL 34786','1456 Whitney Isles Dr., Windermere, FL 34786','11538 VIA LUCERNA CIR, Windermere, FL 34786','222 TRADITIONS DR, Winter Garden, FL 34787','14434 WHITTRIDGE DR, Winter Garden, FL 34787','2661 Tierra Circle, Winter Park, FL 32792','2616 Verde Lane, Winter Park, FL 32792','1564 Lawndale Circle, Winter Park, FL 32792']
      sts.each do |d|
        Property.search_zpid(d)
      end
      
    rescue Exception => ex
      puts " error #{ex.message}"
    end
  end
  
  desc "zillow"
  task :zil_match => :environment do |t, args|
    begin
      # sts = ['1916 EXCALIBUR DR, Orlando, FL 32822','903 PRINCETON DRIVE, Clermont, FL 34711','903 PRINCETON DRIVE, Clermont, FL 34711','889 WOODGATE TRL, Longwood, FL 32750','2496 PINE CHASE CIR, Saint Cloud, FL 34769','1878 TILBURG AVE, Deltona, FL 32725','9906 KENDAL DR, Orlando, FL 32817','2967 RED OAK DR, Kissimmee, FL 34744','618 RED MULBERRY DR, Deltona, FL 32725','51 TIMBERCREEK PINES CIR, Winter Garden, FL 34787','11020 SYLVAN POND CIR, Orlando, FL 32825','1470 OLD MILL DR, Deltona, FL 32725','1470 OLD MILL DR, Deltona, FL 32725','5450 COUNTY FAIR CT, Oviedo, FL 32765','752 SWAYING PALM DR, Apopka, FL 32712','1923 SEATONHAM ST, Winter Garden, FL 34787','1201 CARSON AVE, Kissimmee, FL 34744','2210 HEATHEROAK DR, Apopka, FL 32703','4307 STONEFIELD DR, Orlando, FL 32826','206 YALE DR, Sanford, FL 32771','605 S DILLINGHAM AVE, Kissimmee, FL 34741','794 PRINCETON DR, Clermont, FL 34711','794 PRINCETON DR, Clermont, FL 34711','1922 ALGONQUIN AVE, Deltona, FL 32725','904 GRAND CAYMAN CT, Orlando, FL 32835','16 N NORMANDALE AVE, Orlando, FL 32835','5457 ENDICOTT PL, Oviedo, FL 32765','5457 ENDICOTT PL, Oviedo, FL 32765','2972 OAKTREE DR, Kissimmee, FL 34744','1040 ROYAL MARQUIS CIR, Ocoee, FL 34761','555 CHAUTAUQUA DR, Mount Dora, FL 32757','306 COUNTRY LANDING BLVD, Apopka, FL 32703','3420 BUFFAM PL, Casselberry, FL 32707','1642 PORTCASTLE CIR, Winter Garden, FL 34787','1409 AUBURN GREEN LOOP, Winter Park, FL 32792','941 SOUTHRIDGE TRL, Altamonte Springs, FL 32714','2939 TWIN OAKS DR, Kissimmee, FL 34744','7212 ALSTON CT, Orlando, FL 32835','1363 WOODBINE AVE, Deltona, FL 32725','130 STEPHEN AVE, Oviedo, FL 32765','518 KAYS LANDING DR, Sanford, FL 32771','4810 WATERSIDE POINTE CIR, Orlando, FL 32829','2227 CERBERUS DR, Apopka, FL 32712','2227 CERBERUS DR, Apopka, FL 32712','2931 BLAKELY DR, Orlando, FL 32835','1457 SAXON BLVD, Deltona, FL 32725','1043 BURNETT ST, Oviedo, FL 32765','334 OAK PARK PL, Casselberry, FL 32707','2052 CROSSHAIR CIR, Orlando, FL 32837','2052 CROSSHAIR CIR, Orlando, FL 32837','611 S ANCHOR DR, Deltona, FL 32725','1225 UPSTREET CT, Orlando, FL 32837','628 NEUMANN VILLAGE CT, Ocoee, FL 34761','2598 SHEILA DR, Apopka, FL 32712','2598 SHEILA DR, Apopka, FL 32712','3101 CRYSTAL CREEK BLVD, Orlando, FL 32837','285 COVENT GARDENS PL, Deltona, FL 32725','198 VELVETEEN PL, Chuluota, FL 32766','53 COLUMBINE TRL, Debary, FL 32713','2125 KAYLAS CT, Orlando, FL 32817','3601 GRUBBS ST, Mount Dora, FL 32757','3601 GRUBBS ST, Mount Dora, FL 32757','685 ASHLAND CT, Deltona, FL 32725','9135 WOODBREEZE BLVD, Windermere, FL 34786','1366 JECENIA BLOSSOM DR, Apopka, FL 32712','431 GLEN ABBEY LN, Debary, FL 32713','1166 NAOMI LN, Sanford, FL 32773','420 E CITRUS ST, Altamonte Springs, FL 32701','1214 STILLWATER AVE, Deltona, FL 32725','1056 COASTAL CIR, Ocoee, FL 34761','724 HAVEN OAK CT, Apopka, FL 32703','2401 PALMETTO RIDGE CIR, Apopka, FL 32712','617 PICKFAIR TER, Lake Mary, FL 32746','635 NICOLE MARIE ST, Apopka, FL 32712','2584 VINEYARD CIR, Sanford, FL 32771','1307 LYDIA DR, Deltona, FL 32725','203 N CERVIDAE DR, Apopka, FL 32703','11233 CARABELEE CIR, Orlando, FL 32825','2442 OAK HOLLOW DR, Kissimmee, FL 34744','1931 RIVER PARK BLVD, Orlando, FL 32817','628 WHITEMARSH AVENUE, Deltona, FL 32725','540 BIRCH COURT, Altamonte Springs, FL 32714','110 ARDSDALE COURT, Longwood, FL 32750','2325 FITZPATRICK TERRACE, Deltona, FL 32725','1305 HEATHER LAKE DR, Orlando, FL 32824','2212 EH POUNDS DR, Ocoee, FL 34761','1978 S OLD MILL DR, Deltona, FL 32725','4618 TIFFANY WOODS CIR, Oviedo, FL 32765','1939 PERIDOT CIR, Kissimmee, FL 34743','10314 TARABY CT, Orlando, FL 32817','1555 APRIL AVE, Deltona, FL 32725','4422 WATERSIDE POINTE CIR, Orlando, FL 32829','9444 TELFER RUN, Orlando, FL 32817','104 LAKESIDE CIR, Sanford, FL 32773','3198 FAIRFIELD DR, Kissimmee, FL 34743','2225 WHISPERING MAPLE DR, Orlando, FL 32837','4550 SIMMONS RD, Orlando, FL 32812','4550 SIMMONS RD, Orlando, FL 32812','211 BRIGHTVIEW DR, Lake Mary, FL 32746','3137 HOLLAND DR, Orlando, FL 32825','905 ABBY TER, Deltona, FL 32725','905 ABBY TER, Deltona, FL 32725','1742 DELAFIELD DR, Winter Garden, FL 34787','1215 HONEY RD, Apopka, FL 32712','1215 HONEY RD, Apopka, FL 32712','2625 ALAMOSA PL, Lake Mary, FL 32746','1228 LAKE LUCERNE CIR, Winter Springs, FL 32708','381 SAN SEBASTIAN PRADO, Altamonte Springs, FL 32714','4190 BEAR GULLY ROAD, Winter Park, FL 32792','2088 KEEL WAY, Kissimmee, FL 34744','1744 VALE DR, Clermont, FL 34711','2516 ASCOT COURT, Kissimmee, FL 34744','871 N ATMORE CIRCLE, Deltona, FL 32725','1726 GULF WINDS COURT, Apopka, FL 32712','11739 CLAIR PLACE, Clermont, FL 34711','1440 BADGER TERRACE, Deltona, FL 32725','225 GEMWOOD COURT, Kissimmee, FL 34743','263 BLACKWATER PLACE, Longwood, FL 32750','2456 PINE CHASE CIRCLE, Saint Cloud, FL 34769','704 CASTILLO PLACE, Saint Cloud, FL 34769','807 S SANFORD AVENUE, Sanford, FL 32771','12712 WOODBURY GLEN DRIVE, Orlando, FL 32828','3425 SUGAR MILL ROAD, Kissimmee, FL 34741','508 LUCILE WAY, Orlando, FL 32835','30230 HUNSTAUNTON DRIVE, Sorrento, FL 32776','1201 AZORA DRIVE, Deltona, FL 32725','1634 E NORMANDY BOULEVARD, Deltona, FL 32725','698 N APACHE CIRCLE, Deltona, FL 32725','879 ABBY TERRACE, Deltona, FL 32725','301 HERONWOOD CIRCLE, Deltona, FL 32725','1022 W HARBOR DRIVE, Deltona, FL 32725','1430 CORONET DRIVE, Deltona, FL 32725','9236 PALM TREE DRIVE, Windermere, FL 34786','653 BROOKFIELD LOOP, Lake Mary, FL 32746','521 HOWARD AVENUE, Altamonte Springs, FL 32701','1671 ANORADA BOULEVARD, Kissimmee, FL 34744','3409 WINDY WOOD DRIVE, Orlando, FL 32812','7900 DUNSTABLE CIRCLE, Orlando, FL 32817','8011 VIA BONITA STREET, Sanford, FL 32771','12511 EL VIENTO ROAD, Clermont, FL 34711','452 PALM SPRINGS DRIVE, Longwood, FL 32750','1020 FRANCIS STREET, Altamonte Springs, FL 32701','1033 SUMMER LAKES DRIVE, Orlando, FL 32835','1418 BEAR LAKE ROAD, Apopka, FL 32703','14610 MUSKET FIRE LANE, Orlando, FL 32837','332 RINGWOOD CIRCLE, Winter Springs, FL 32708','941 BUCKSAW PLACE, Longwood, FL 32750','111 HAMLIN T LANE, Altamonte Springs, FL 32714','3515 CURTIS DRIVE, Apopka, FL 32703','691 SHADY COURT, Altamonte Springs, FL 32701','646 N DIVISION STREET, Oviedo, FL 32765','3379 MONIKA CIRCLE, Orlando, FL 32812','1498 STONE TRAIL, Enterprise, FL 32725','900 DOLPHIN AVENUE, Kissimmee, FL 34744','1564 LAWNDALE CIRCLE, Winter Park, FL 32792','1582 GOLFSIDE VILLAGE BOULEVARD, Apopka, FL 32712','1460 LAKE FRANCIS DRIVE, Apopka, FL 32712','9440 W PAULINDA AVENUE, Apopka, FL 32703','120 WINDSONG COURT, Lake Mary, FL 32746','2158 SUSSEX ROAD, Winter Park, FL 32792','125 HOLLOW BRANCH ROAD, Apopka, FL 32703','584 SAXON BOULEVARD, Deltona, FL 32725','583 MENDOZA DRIVE, Orlando, FL 32825','1002 GROVE AVENUE, Altamonte Springs, FL 32701','11648 ENGLISH STREET, Orlando, FL 32817','904 LOGENBERRY TRAIL, Winter Springs, FL 32708','1710 FINLEY AVENUE, Apopka, FL 32703','2650 GOLFRIDGE LANE, Apopka, FL 32712','102 WINDMILL WAY, Longwood, FL 32750','2003 LAKE PICKETT ROAD, Orlando, FL 32826','104 HALLMARK COURT, Lake Mary, FL 32746','117 MORNING GLORY DRIVE, Lake Mary, FL 32746','11719 LAKE CLAIR CIRCLE, Clermont, FL 34711','1961 BEAR VIEW DRIVE, Apopka, FL 32703','111 HATTAWAY DRIVE, Altamonte Springs, FL 32701','14533 BRAY ROAD, Orlando, FL 32832','1344 N MARCY DRIVE, Longwood, FL 32750','952 MARCH HARE COURT, Winter Springs, FL 32708','1115 W WINGED FOOT CIRCLE, Winter Springs, FL 32708','1359 N MARCY DRIVE, Longwood, FL 32750','3065 NICHOLSON DRIVE, Winter Park, FL 32792','1207 ROYAL OAK DRIVE, Winter Springs, FL 32708','50 SWEETWATER CREEK CIRCLE, Oviedo, FL 32765','3237 BLACK PINE AVENUE, Winter Park, FL 32792','3915 STONEHAVEN ROAD, Orlando, FL 32817','2986 LOWERY DRIVE, Oviedo, FL 32765','4713 DEER ROAD, Orlando, FL 32812','127 GUADALAJARA DRIVE, Kissimmee, FL 34743','1321 ARDEN, Longwood, FL 32750','32 BASS LAKE, Debary, FL 32713','1085 DEAN STREET, Saint Cloud, FL 34771','908 RED OAK COURT, Winter Springs, FL 32708','8049 SWEETGUM LOOP, Orlando, FL 32835','8658 SAVORY DRIVE, Orlando, FL 32825','32 S LAKE CORTEZ DRIVE, Apopka, FL 32703','249 E LAKE AVENUE, Longwood, FL 32750','1559 SUGARWOOD CIRCLE, Winter Park, FL 32792','4692 STURBRIDGE CIRCLE, Orlando, FL 32812','613 HEATHER BRITE CIRCLE, Apopka, FL 32712','1317 VIA VILLA NOVA, Winter Springs, FL 32708','807 TOLEDO DRIVE, Altamonte Springs, FL 32714','376 GOLDSTONE COURT, Lake Mary, FL 32746','1663 SPICEWOOD LANE, Casselberry, FL 32707','7624 WAUNATTA COURT, Winter Park, FL 32792','7682 GLENMOOR LANE, Winter Park, FL 32792','837 HAULOVER DRIVE, Altamonte Springs, FL 32714','23958 OAK TREE DRIVE, Sorrento, FL 32776','1928 PROCTOR AVENUE, Orlando, FL 32817','296 SANDPIPER DRIVE, Casselberry, FL 32707','10146 BRIDLEWOOD AVENUE, Orlando, FL 32825','2349 PINE STREET, Oviedo, FL 32765','126 SACKETT ROAD, Debary, FL 32713','1509 JAGUAR CIRCLE, Apopka, FL 32712','23 N WINTER PARK DRIVE, Casselberry, FL 32707','630 TOMLINSON TERRACE, Lake Mary, FL 32746','1602 AUGUSTA WAY, Casselberry, FL 32707','2805 ORANOLE WAY, Apopka, FL 32703','411 W CITRUS STREET, Altamonte Springs, FL 32714','1867 SHADYHILL TERRACE, Winter Park, FL 32792','1138 DAPPLED ELM LANE, Winter Springs, FL 32708','680 ROYAL PALM DRIVE, Kissimmee, FL 34743','1232 WOODRIDGE COURT, Altamonte Springs, FL 32714','2982 LOWERY DRIVE, Oviedo, FL 32765','308 HAWAII WOODS COURT, Orlando, FL 32824','325 LOCH LOMOND AVENUE, Longwood, FL 32750','803 CALABRIA DRIVE, Altamonte Springs, FL 32714','670 GEORGIA AVENUE, Longwood, FL 32750','178 SOUTHCOT DRIVE, Casselberry, FL 32707','816 PINE MEADOWS ROAD, Orlando, FL 32825','3603 GATEWOOD DRIVE, Orlando, FL 32812','904 LITTLE BEND ROAD, Altamonte Springs, FL 32714','109 CARDAMON DRIVE, Orlando, FL 32825','3001 NICHOLSON DRIVE, Winter Park, FL 32792','114 MADEIRA AVENUE, Orlando, FL 32825','2304 JUSTY WAY, Orlando, FL 32817','113 SHOMATE DRIVE, Longwood, FL 32750','1252 HIBISCUS LANE, Apopka, FL 32703','1514 MEADOWLARK STREET, Longwood, FL 32750','549 OAKHURST STREET, Altamonte Springs, FL 32701','714 E LACY CIRCLE, Deltona, FL 32725','510 WATER WAY COURT, Ocoee, FL 34761','1024 NIN STREET, Orlando, FL 32835','830 N JERICO DRIVE, Casselberry, FL 32707','1555 JAN LAN BOULEVARD, Saint Cloud, FL 34772','2982 HEATHERSIDE AVENUE, Orlando, FL 32822','102 RAPSCALLION DRIVE, Orlando, FL 32828','8056 ELSEE DRIVE, Orlando, FL 32822','3311 HEARTWOOD AVENUE, Winter Park, FL 32792','1452 ACORN COURT, Kissimmee, FL 34744','401 NAVARRE WAY, Altamonte Springs, FL 32714','460 PALM SPRINGS DRIVE, Longwood, FL 32750','1837 FAYETTEVILLE AVENUE, Deltona, FL 32725','1662 TREMONT LANE, Winter Park, FL 32792','648 RIDGEWOOD STREET, Altamonte Springs, FL 32701','1139 N OLD MILL DRIVE, Deltona, FL 32725','562 BITTERWOOD COURT, Kissimmee, FL 34743','401 GLEN ABBEY LANE, Debary, FL 32713','1179 E NORMANDY BOULEVARD, Deltona, FL 32725','625 HAYMAN COURT, Debary, FL 32713','7100 GREEN NEEDLE DRIVE, Winter Park, FL 32792','112 MISTYWOOD COURT, Kissimmee, FL 34743','2722 RAMSEY DRIVE, Apopka, FL 32703','1357 PARK DRIVE, Casselberry, FL 32707','532 FLORAL DRIVE, Kissimmee, FL 34743','118 DONNA CIRCLE, Sanford, FL 32773','400 W WASHINGTON STREET, Minneola, FL 34715','4907 RED BAY DRIVE, Orlando, FL 32829','1132 MADURA DRIVE, Deltona, FL 32725','744 MEADOWLARK COURT, Longwood, FL 32750','201 EILEEN AVENUE, Altamonte Springs, FL 32714','601 ANTOINETTE STREET, Deltona, FL 32725','940 MARQUETTE COURT, Deltona, FL 32725','1319 HOLLY SPRINGS CIRCLE, Orlando, FL 32825','1432 OUTER COURT, Kissimmee, FL 34744','553 CLEMSON DRIVE, Altamonte Springs, FL 32714','2552 CANTERCLUB TRAIL, Apopka, FL 32712','189 W 3RD STREET, Chuluota, FL 32766','1412 WESTDALE AVENUE, Winter Park, FL 32792','1568 RANDOLPH STREET, Deltona, FL 32725','3720 VERAVAL AVENUE, Orlando, FL 32817','211 DOSTER DRIVE, Casselberry, FL 32707','1004 TOMES COURT, Orlando, FL 32825','1014 BELVEDERE DRIVE, Deltona, FL 32725','336 CANOE TRAIL LANE, Orlando, FL 32825','424 LA PAZ DRIVE, Kissimmee, FL 34743','212 BAYOU VISTA STREET, Debary, FL 32713','327 E STORY ROAD, Winter Garden, FL 34787','517 MOCKINGBIRD LANE, Altamonte Springs, FL 32714','212 DELESPINE DRIVE, Debary, FL 32713','851 WOODGATE TRAIL, Longwood, FL 32750','2057 AMBERGRIS, Orlando, FL 32822','7724 PINE HAWK LANE, Orlando, FL 32822','549 NARDELLO DRIVE, Deltona, FL 32725','2313 MILLBANK DRIVE, Orlando, FL 32837','301 LATONIA STREET, Kissimmee, FL 34741','1109 S ELM AVENUE, Sanford, FL 32771','8103 BRITT DRIVE, Orlando, FL 32822','31051 DEAL DRIVE, Sorrento, FL 32776','138 W CEDARWOOD CIRCLE, Kissimmee, FL 34743','1564 SUNBIRD TERRACE, Deltona, FL 32725','2617 CORAL AVENUE, Kissimmee, FL 34741','409 MONROE, Apopka, FL 32703','1519 HENRY STREET, Kissimmee, FL 34741','1860 SAXON BOULEVARD, Deltona, FL 32725','1384 RURAL HALL STREET, Deltona, FL 32725','7628 RUTHWIND COURT, Orlando, FL 32822','1640 FRUITLAND DRIVE, Deltona, FL 32725','7421 HOUSTON E COURT, Winter Park, FL 32792','1859 E COOPER DRIVE, Deltona, FL 32725','655 DAVID STREET, Winter Springs, FL 32708','805 ASPENWOOD CIRCLE, Kissimmee, FL 34743','1700 DIXIE BELLE DRIVE, Orlando, FL 32812','1169 S BRICKELL DRIVE, Deltona, FL 32725','445 ALEXANDER AVENUE, Deltona, FL 32725','1743 LONGLEAF DRIVE, Saint Cloud, FL 34769','13978 IVORY GARDENIA AVENUE, Windermere, FL 34786','12962 GARRIDAN AVENUE, Windermere, FL 34786','13808 CEPHEUS DRIVE, Orlando, FL 32828','313 MIRASOL LANE, Orlando, FL 32828','2104 LILIPETAL COURT, Sanford, FL 32771','2023 CHICKADEE DRIVE, Apopka, FL 32703','1706 ALAMBRA CIRCLE, Apopka, FL 32703','11317 SCENIC VISTA DRIVE, Clermont, FL 34711','10971 INSIDE LOOP, Orlando, FL 32825','2014 PIRIE PLACE, Saint Cloud, FL 34769','24144 PLYMOUTH HOLLOW CIRCLE, Sorrento, FL 32776','2342 AVELLINO AVENUE, Saint Cloud, FL 34771','2978 CAMINO REAL S DRIVE, Kissimmee, FL 34744','3496 MEADOW BREEZE LOOP, Ocoee, FL 34761','1061 FLAGSTONE COURT, Apopka, FL 32703','1849 DUNN COVE DRIVE, Apopka, FL 32703','1118 FOX TRAIL AVENUE, Minneola, FL 34715','5236 LEMON TWIST LANE, Windermere, FL 34786','4980 SPIRAL WAY, Saint Cloud, FL 34771','2780 PYTHAGORAS CIRCLE, Ocoee, FL 34761','9882 LENOX STREET, Clermont, FL 34711','1839 ALAMBRA CIRCLE, Apopka, FL 32703','10334 CALLE DE FLORES DRIVE, Clermont, FL 34711','2038 PIRIE PLACE, Saint Cloud, FL 34769','354 MIRASOL LANE, Orlando, FL 32828','3172 ROLLING HILLS LANE, Apopka, FL 32712','12938 GARRIDAN AVENUE, Windermere, FL 34786','4770 SALAMANDER STREET, Saint Cloud, FL 34772','2829 LAFAYETTE TRACE DRIVE, Saint Cloud, FL 34772','2177 WHITE BIRD WAY, Apopka, FL 32703','1941 CANDLENUT CIRCLE, Apopka, FL 32712','1427 BARN OWL LOOP, Sanford, FL 32773','431 SHIRLEY DRIVE, Apopka, FL 32712','520 CODY ALLEN COURT, Saint Cloud, FL 34769','3252 TOSCANA DRIVE, Saint Cloud, FL 34772','2728 STANWOOD DRIVE, Kissimmee, FL 34743','24031 PLYMOUTH HOLLOW CIRCLE, Sorrento, FL 32776','2773 SHEARWATER STREET, Clermont, FL 34711','663 NEWHALL LANE, Debary, FL 32713','671 NEWHALL LANE, Debary, FL 32713','3036 SANDSTONE CIRCLE, Saint Cloud, FL 34772','1816 DUNN COVE DRIVE, Apopka, FL 32703','622 ALABAMA WOODS LANE, Orlando, FL 32824','3705 RYEGRASS STREET, Clermont, FL 34714','2720 DOLFINO COURT, Saint Cloud, FL 34772','26250 BILTMORE STREET, Sorrento, FL 32776','850 KAZAROS CIRCLE, Ocoee, FL 34761','16 BURGUST STREET, Apopka, FL 32712','591 NEWHALL LANE, Debary, FL 32713','4852 GRAND VISTA LANE, Saint Cloud, FL 34771','659 NEWHALL LANE, Debary, FL 32713','10965 INSIDE LOOP, Orlando, FL 32825','16338 SAINT AUGUSTINE STREET, Clermont, FL 34714','16555 CORNER LAKE DRIVE, Orlando, FL 32820','4899 MEADOW DRIVE, Saint Cloud, FL 34772','2680 DOLFINO COURT, Saint Cloud, FL 34772','2335 AVELLINO AVENUE, Saint Cloud, FL 34771','1331 CREEKVIEW COURT, Saint Cloud, FL 34772','2765 LAKESHORE BOULEVARD, Saint Cloud, FL 34769','599 NEWHALL LANE, Debary, FL 32713','2118 PORTCASTLE CIRCLE, Winter Garden, FL 34787','814 WINDROSE DRIVE, Orlando, FL 32824','1859 RIBBON FALLS PARKWAY, Orlando, FL 32824','277 MAGICAL WAY, Kissimmee, FL 34744','124 COLONIAL PINE LANE, Minneola, FL 34715','435 ROSALIA DRIVE, Sanford, FL 32771','2759 BARCLAY LANE, Kissimmee, FL 34743','2448 VINEYARD CIRCLE, Sanford, FL 32771','3820 RYEGRASS STREET, Clermont, FL 34714','4903 STONE ACRES CIRCLE, Saint Cloud, FL 34771','579 NEWHALL LANE, Debary, FL 32713','1781 SCRUB JAY ROAD, Apopka, FL 32703','2640 JUDGE LOOP, Kissimmee, FL 34743','3014 SANDSTONE CIRCLE, Saint Cloud, FL 34772','2061 BIG BUCK DRIVE, Saint Cloud, FL 34772','672 NEWHALL LANE, Debary, FL 32713','1024 FLAGSTONE COURT, Apopka, FL 32703','2932 BONITA CIRCLE, Kissimmee, FL 34744','915 W STORY ROAD, Winter Garden, FL 34787','415 ROSALIA DRIVE, Sanford, FL 32771','10052 PENTRIDGE ROAD, Orlando, FL 32829','910 HADDOCK DRIVE, Clermont, FL 34711','3847 SPIRITED CIRCLE, Saint Cloud, FL 34772','3311 PEKIN STREET, Saint Cloud, FL 34772','3926 CEDAR HAMMOCK TRAIL, Saint Cloud, FL 34772','11322 WISHING WELL LANE, Clermont, FL 34711','4854 TERRA SOLE PLACE, Saint Cloud, FL 34771','3523 HARLEQUIN DRIVE, Saint Cloud, FL 34772','1352 S PRAIRIE CIRCLE, Deltona, FL 32725','223 CLARA VISTA STREET, Debary, FL 32713','219 CLARA VISTA STREET, Debary, FL 32713','217 CLARA VISTA STREET, Debary, FL 32713','1378 FREEPORT DRIVE, Deltona, FL 32725','16723 CITRUS PARKWAY, Clermont, FL 34714','1575 PIER STREET, Clermont, FL 34711','2954 MARBELLA DRIVE, Kissimmee, FL 34744','1705 SOUTHERN OAK LOOP, Minneola, FL 34715','1954 S OLD MILL DRIVE, Deltona, FL 32725','3258 OLIVERA WAY, Saint Cloud, FL 34772','16350 SAINT AUGUSTINE STREET, Clermont, FL 34714','3828 RYEGRASS STREET, Clermont, FL 34714','952 GRAPEWOOD STREET, Deltona, FL 32725','1713 PORTCASTLE CIRCLE, Winter Garden, FL 34787','10214 YONAOMI CIRCLE, Clermont, FL 34711','16736 CITRUS PARKWAY, Clermont, FL 34714','25006 CHIPSHOT COURT, Sorrento, FL 32776','3620 HOLDEN DRIVE, Saint Cloud, FL 34772','221 CLARA VISTA STREET, Debary, FL 32713','142 VERONA ROAD, Debary, FL 32713','2857 SHELBURNE WAY, Saint Cloud, FL 34772','1265 HERNDON AVENUE, Deltona, FL 32725','850 CAMPELLO STREET, Altamonte Springs, FL 32701','2150 TYBEE ROAD, Saint Cloud, FL 34769','201 MAGICAL WAY, Kissimmee, FL 34744','3053 BOXELDER COURT, Deltona, FL 32725','2539 POINSETTA AVENUE, Sanford, FL 32773','2535 POINSETTA AVENUE, Sanford, FL 32773','2148 LILIPETAL COURT, Sanford, FL 32771','2037 KELSO AVENUE, Deltona, FL 32725','421 BELLA ROSA CIRCLE, Sanford, FL 32771','349 FAIRFIELD DRIVE, Sanford, FL 32771','3144 LAUREN COURT, Deltona, FL 32725','285 W MAGNOLIA STREET, Clermont, FL 34711','959 SPRINGS LANDING DRIVE, Deltona, FL 32725','1195 W HANCOCK DRIVE, Deltona, FL 32725','1388 DANDELION DRIVE, Deltona, FL 32725','1034 BELVEDERE DRIVE, Deltona, FL 32725','1734 JOYNER DRIVE, Deltona, FL 32725','2691 PROVIDENCE BOULEVARD, Deltona, FL 32725','443 FAIRFIELD DRIVE, Sanford, FL 32771','13927 ORANGE CREST DRIVE, Windermere, FL 34786','6344 EARTHGOLD DRIVE, Windermere, FL 34786','11373 ARBORSIDE BEND WAY, Windermere, FL 34786','448 SOTHEBY WAY, Debary, FL 32713','13718 COLINA COURT, Clermont, FL 34711','4911 CORTO DRIVE, Orlando, FL 32837','4062 SUNNY DAY WAY, Kissimmee, FL 34744','151 VELVETEEN PLACE, Chuluota, FL 32766','13248 OLD DOCK ROAD, Orlando, FL 32828','4598 CONROY CLUB DRIVE, Orlando, FL 32835','1861 PARKGLEN CIRCLE, Apopka, FL 32712','1246 GREEN VISTA CIRCLE, Apopka, FL 32712','4889 FELLS COVE AVENUE, Kissimmee, FL 34744','3337 BENNINGTON COURT, Winter Park, FL 32792','1115 PRINCETON DRIVE, Clermont, FL 34711','209 WEKIVA POINTE CIRCLE, Apopka, FL 32712','427 LAKE AMBERLEIGH DRIVE, Winter Garden, FL 34787','2033 CORNER SCHOOL DRIVE, Orlando, FL 32820','2719 GRAPEVINE CREST, Ocoee, FL 34761','10838 VINEYARD COURT, Clermont, FL 34711','2817 OCONNELL DRIVE, Kissimmee, FL 34741','476 MISTY OAKS RUN, Casselberry, FL 32707','11038 WINDCHIME CIRCLE, Clermont, FL 34711','199 SAVANNAH PARK LOOP, Casselberry, FL 32707','2555 ROLLINS AVENUE, Clermont, FL 34711','16944 CORNER BAY COURT, Orlando, FL 32820','3205 WATERBRIDGE COURT, Kissimmee, FL 34744','12649 CYNTHIA LANE, Clermont, FL 34715','16548 ARROWHEAD TRAIL, Clermont, FL 34711','13721 COLINA COURT, Clermont, FL 34711','3529 MAPLE RIDGE LOOP, Kissimmee, FL 34741','14444 ESTRELLA DRIVE, Orlando, FL 32837','428 DOMINISH ESTATES DRIVE, Apopka, FL 32712','238 TIMBERCREEK PINES CIRCLE, Winter Garden, FL 34787','221 TIMBERCREEK PINES CIRCLE, Winter Garden, FL 34787','727 HACIENDA CIRCLE, Kissimmee, FL 34741','5164 FILMORE PLACE, Sanford, FL 32773','12014 PRAIRIE MEADOWS DRIVE, Orlando, FL 32837','1679 ROSEWOOD DRIVE, Clermont, FL 34711','1701 MELANIE DRIVE, Orlando, FL 32825','10082 SHADOW CREEK DRIVE, Orlando, FL 32832','18050 THORNHILL GRAND CIRCLE, Orlando, FL 32820','1842 WINGED ELM PLACE, Winter Garden, FL 34787','210 VELVETEEN PLACE, Chuluota, FL 32766','9510 MEADOW CREST LANE, Clermont, FL 34711','2204 JULIANNA COURT, Saint Cloud, FL 34769','12545 LAKE DENISE BOULEVARD, Clermont, FL 34711','1828 VALE DRIVE, Clermont, FL 34711','6043 FROGGATT STREET, Orlando, FL 32835','146 CROWN COLONY WAY, Sanford, FL 32771','360 MISTY OAKS RUN, Casselberry, FL 32707','1737 CORNERVIEW LANE, Orlando, FL 32820','3689 ROCHELLE LANE, Apopka, FL 32712','1923 BIG CYPRESS DRIVE, Saint Cloud, FL 34771','1924 KNOLLCREST DRIVE, Clermont, FL 34711','1531 LALIQUE LANE, Orlando, FL 32828','12623 LAKE DENISE BOULEVARD, Clermont, FL 34711','2925 MAGNOLIA BLOSSOM CIRCLE, Clermont, FL 34711','1152 ORANGE GROVE LANE, Apopka, FL 32712']
      # sts = ['2634 QUARTERDECK COURT, Kissimmee, FL 34743','8867 HASTINGS BEACH BOULEVARD, Orlando, FL 32829','6405 DIANE COURT, Saint Cloud, FL 34771','10241 MASON LOOP, Clermont, FL 34711','10066 SHADOW CREEK DRIVE, Orlando, FL 32832','104 SEQUOIA VALLEY COURT, Minneola, FL 34715','14344 SAPPHIRE BAY CIRCLE, Orlando, FL 32828','5817 AUTUMN CHASE CIRCLE, Sanford, FL 32773','275 VELVETEEN PLACE, Chuluota, FL 32766','1319 AGUACATE COURT, Orlando, FL 32837','510 NEWHALL LANE, Debary, FL 32713','216 BELGIAN WAY, Sanford, FL 32773','2720 NOVA DRIVE, Apopka, FL 32703','415 CAREY WAY, Orlando, FL 32825','1000 CORNELL AVENUE, Clermont, FL 34711','355 E LAKE AVENUE, Longwood, FL 32750','913 ANCHORAGE LANE, Kissimmee, FL 34744','12520 WOODBURY COVE DRIVE, Orlando, FL 32828','10538 VIA LUGANO COURT, Clermont, FL 34711','910 DON WILSON AVENUE, Apopka, FL 32712','808 BRISTOL FOREST WAY, Orlando, FL 32828','1163 LEBANON COURT, Sanford, FL 32771','1312 SENECA FALLS DRIVE, Orlando, FL 32828','2835 BICKLEY DRIVE, Apopka, FL 32712','504 VICKS LANDING DRIVE, Apopka, FL 32712','10438 MOSS ROSE WAY, Orlando, FL 32832','1741 ELDORADO COURT, Saint Cloud, FL 34771','1844 OXTON COURT, Ocoee, FL 34761','12633 DOUGLAS FIR COURT, Clermont, FL 34711','17937 SAXONY LANE, Orlando, FL 32820','5587 PATS POINT, Winter Park, FL 32792','2221 EAGLES LANDING WAY, Kissimmee, FL 34744','14696 KRISTENRIGHT LANE, Orlando, FL 32826','154 LONDON FOG WAY, Sanford, FL 32771','972 CORNELL AVENUE, Clermont, FL 34711','1192 GREEN VISTA CIRCLE, Apopka, FL 32712','1519 LALIQUE LANE, Orlando, FL 32828','1718 AMERICUS MINOR DRIVE, Winter Garden, FL 34787','518 BABBLING BROOK COURT, Orlando, FL 32825','11425 ROUSE RUN CIRCLE, Orlando, FL 32817','2290 CALEDONIAN STREET, Clermont, FL 34711','1280 OCKLAWAHA DRIVE, Orlando, FL 32828','1505 BRAYTON CIRCLE, Deltona, FL 32725','701 PADDINGTON PLACE, Deltona, FL 32725','803 PRINCETON DRIVE, Clermont, FL 34711','606 NEUMANN VILLAGE COURT, Ocoee, FL 34761','33008 WINDY OAK STREET, Sorrento, FL 32776','11521 ROUSE RUN CIRCLE, Orlando, FL 32817','1509 MORNING STAR DRIVE, Clermont, FL 34714','12007 STILL MEADOW DRIVE, Clermont, FL 34711','2534 DELBARTON AVENUE, Deltona, FL 32725','2122 VICTORIA FALLS DRIVE, Orlando, FL 32824','14637 HUNTCLIFF PARK WAY, Orlando, FL 32824','2045 CORNER MEADOW CIRCLE, Orlando, FL 32820','621 SKYRIDGE ROAD, Clermont, FL 34711','825 LAKE DOE BOULEVARD, Apopka, FL 32703','2982 MARBELLA DRIVE, Kissimmee, FL 34744','1719 SOUTHERN OAK LOOP, Minneola, FL 34715','14655 LADY VICTORIA BOULEVARD, Orlando, FL 32826','507 DOMINISH ESTATES DRIVE, Apopka, FL 32712','1705 BRIDGEPEBBLE ROAD, Winter Garden, FL 34787','3219 COUNTRYSIDE VIEW DRIVE, Saint Cloud, FL 34772','14156 ECON WOODS LANE, Orlando, FL 32826','1451 AGATHA DRIVE, Deltona, FL 32725','1180 SHERBROOK DRIVE, Deltona, FL 32725','642 WHISPER SOUND STREET, Minneola, FL 34715','30650 PGA DRIVE, Sorrento, FL 32776','1917 SOUTHERN OAK LOOP, Minneola, FL 34715','2011 STONE ABBEY BOULEVARD, Orlando, FL 32828','905 ANCHORAGE LANE, Kissimmee, FL 34744','7872 NIAGARA FALLS COURT, Orlando, FL 32825','15328 TORPOINT ROAD, Winter Garden, FL 34787','30620 PGA DRIVE, Sorrento, FL 32776','3218 HUNTERS CHASE LOOP, Kissimmee, FL 34743','1291 DARNABY WAY, Orlando, FL 32824','11604 ROPER BOULEVARD, Clermont, FL 34711','1200 GIOVANNI STREET, Deltona, FL 32725','11718 FOXGLOVE DRIVE, Clermont, FL 34711','2552 CROWN RIDGE CIRCLE, Kissimmee, FL 34744','1955 VILLA ANGELO BOULEVARD, Saint Cloud, FL 34769','710 WHISPER SOUND STREET, Minneola, FL 34715','1386 LYDIA DRIVE, Deltona, FL 32725','10231 CODY LANE, Orlando, FL 32825','733 CHARING PLACE, Deltona, FL 32725','1384 WILDBERRY LANE, Deltona, FL 32725','1507 MERRIMAC LANE, Deltona, FL 32725','1401 EDISON TERRACE, Deltona, FL 32725','1255 ANDERSON STREET, Deltona, FL 32725','516 S DISSTON AVENUE, Minneola, FL 34715','1152 ABELINE DRIVE, Deltona, FL 32725','1075 FORT SMITH BOULEVARD, Deltona, FL 32725','705 PADDINGTON PLACE, Deltona, FL 32725','2743 HOWLAND BOULEVARD, Deltona, FL 32725','10714 CEDAR FOREST CIRCLE, Clermont, FL 34711','2150 N NORMANDY BOULEVARD, Deltona, FL 32725','30719 PGA DRIVE, Sorrento, FL 32776','222 ACACIA ROAD, Debary, FL 32713','541 ANTOINETTE STREET, Deltona, FL 32725','1808 E COOPER DRIVE, Deltona, FL 32725','1874 W COOPER DRIVE, Deltona, FL 32725','2857 SMITHFIELD DRIVE, Orlando, FL 32837','9211 COUNTY ROAD 561, Clermont, FL 34711','901 COPPERFIELD TERRACE, Casselberry, FL 32707','1415 STONE TRAIL, Enterprise, FL 32725','4456 STEED TERRACE, Winter Park, FL 32792','866 MOONLIT LANE, Casselberry, FL 32707','9431 LAKESHORE DRIVE, Clermont, FL 34711','3701 SEMINOLE DRIVE, Orlando, FL 32812','1191 FREEDOM LANE, Winter Springs, FL 32708','155 RAYMOND OAKS COURT, Altamonte Springs, FL 32701','4503 SANDHURST DRIVE, Orlando, FL 32817','10013 IAN STREET, Orlando, FL 32825','14849 GREEN VALLEY BOULEVARD, Clermont, FL 34711','903 LAUREL LEAF COURT, Apopka, FL 32712','2757 HERON LANDING COURT, Orlando, FL 32837','1011 LONG BRANCH LANE, Oviedo, FL 32765','5101 BELLTHORN DRIVE, Orlando, FL 32837','411 AUGUSTINE COURT, Oviedo, FL 32765','3900 LAKE MIRAGE BOULEVARD, Orlando, FL 32817','842 LANCER CIRCLE, Ocoee, FL 34761','14526 QUAIL TRAIL CIRCLE, Orlando, FL 32837','8636 OTTER CREEK COURT, Orlando, FL 32829','2125 MARTINGALE PLACE, Oviedo, FL 32765','3153 TIMUCUA CIRCLE, Orlando, FL 32837','1035 CROSS CUT WAY, Longwood, FL 32750','3020 OLD CANOE CREEK ROAD, Saint Cloud, FL 34772','352 BALOGH PLACE, Longwood, FL 32750','10312 CARLSON CIRCLE, Clermont, FL 34711','13938 LAKE MARY JANE ROAD, Orlando, FL 32832','13750 SUNSHOWERS CIRCLE, Orlando, FL 32828','12311 BOHANNON BOULEVARD, Orlando, FL 32824','411 GLEN ABBEY LANE, Debary, FL 32713','2045 BRIDGEVIEW CIRCLE, Orlando, FL 32824','5449 BAYTOWNE PLACE, Oviedo, FL 32765','1219 FOXFORREST CIRCLE, Apopka, FL 32712','1726 RIVEREDGE ROAD, Oviedo, FL 32766','375 W HIGHBANKS ROAD, Debary, FL 32713','2937 SOMERSWORTH COURT, Orlando, FL 32835','1315 OLYMPIA PARK CIRCLE, Ocoee, FL 34761','1667 STERLING SILVER BOULEVARD, Deltona, FL 32725','2237 JESSICA LANE, Kissimmee, FL 34744','31114 GOLDEN LANE, Sorrento, FL 32776','4539 CONWAY LANDING DRIVE, Orlando, FL 32812','13047 PRAIRIE MEADOWS DRIVE, Orlando, FL 32837','11627 SIR WINSTON WAY, Orlando, FL 32824','342 INGLENOOK CIRCLE, Winter Springs, FL 32708','131 WESTMORELAND CIRCLE, Kissimmee, FL 34744','1747 SWEETWATER WEST CIRCLE, Apopka, FL 32712','4631 SUNTREE BOULEVARD, Orlando, FL 32817','1755 MYRTLE STREET, Sanford, FL 32773','705 HALLOWELL CIRCLE, Orlando, FL 32828','2204 WYNDAM WAY, Kissimmee, FL 34743','621 PICKFAIR TERRACE, Lake Mary, FL 32746','441 DEER POINTE CIRCLE, Casselberry, FL 32707','1538 COLUSO DRIVE, Winter Garden, FL 34787','3236 HARPERS FERRY COURT, Orlando, FL 32837','339 OAK LEAF CIRCLE, Lake Mary, FL 32746','26 HOLLOW PINE DRIVE, Debary, FL 32713','2270 ALCLOBE CIRCLE, Ocoee, FL 34761','1196 TROTWOOD BOULEVARD, Winter Springs, FL 32708','2509 WEKIVA WALK WAY, Apopka, FL 32703','1711 IMPERIAL PALM DRIVE, Apopka, FL 32712','1137 CROWN ISLE CIRCLE, Apopka, FL 32712','556 LAKE MCCOY DRIVE, Apopka, FL 32712','140 STILLBROOK TRAIL, Enterprise, FL 32725','2964 DIVISION STREET, Oviedo, FL 32765','4942 CASA VISTA DRIVE, Orlando, FL 32837','1440 GALENA TERRACE, Deltona, FL 32725','384 HINSDALE DRIVE, Debary, FL 32713','2828 COPPER RIDGE COURT, Lake Mary, FL 32746','965 WELLINGTON AVENUE, Oviedo, FL 32765','1000 PIEDMONT OAKS DRIVE, Apopka, FL 32703','8081 WELLSMERE CIRCLE, Orlando, FL 32835','2608 QUARTERDECK COURT, Kissimmee, FL 34743','3878 AIDEN PLACE, Apopka, FL 32703','1910 PINE GROVE ROAD, Saint Cloud, FL 34771','3010 EAGLE LAKE DRIVE, Orlando, FL 32837','14713 DAY LILY COURT, Orlando, FL 32824','1005 WHITE SPRINGS LANE, Winter Springs, FL 32708','2609 CEDAR BLUFF LANE, Ocoee, FL 34761','9331 TELFER RUN, Orlando, FL 32817','234 BRIGHTVIEW DRIVE, Lake Mary, FL 32746','753 CREPE MYRTLE CIRCLE, Apopka, FL 32712','2315 RUNNING HORSE TRAIL, Saint Cloud, FL 34771','10439 CARLSON CIRCLE, Clermont, FL 34711','2815 PINE AVENUE, Apopka, FL 32703','2068 CASSINGHAM CIRCLE, Ocoee, FL 34761','367 FOXHILL DRIVE, Debary, FL 32713','4779 DUNBARTON DRIVE, Orlando, FL 32817','249 BUENA VISTA STREET, Debary, FL 32713','1462 LUND AVENUE, Kissimmee, FL 34744','3225 SAWGRASS CREEK CIRCLE, Saint Cloud, FL 34772','442 FOXHILL DRIVE, Debary, FL 32713','438 FOXHILL DRIVE, Debary, FL 32713','1536 REGAL COURT, Kissimmee, FL 34744','3827 PEACE PIPE DRIVE, Orlando, FL 32829','603 QUEENSBRIDGE DRIVE, Lake Mary, FL 32746','2310 FLAMINGO LAKES DRIVE, Kissimmee, FL 34743','10312 CASTILLO COURT, Clermont, FL 34711','11740 CRESCENT PINES BOULEVARD, Clermont, FL 34711','5372 ROCKING HORSE PLACE, Oviedo, FL 32765','2728 EAGLE LAKE DRIVE, Orlando, FL 32837','1484 COUNTRY VILLA COURT, Apopka, FL 32703','12715 WOODBURY OAKS DRIVE, Orlando, FL 32828','285 COUNTRY LANDING BOULEVARD, Apopka, FL 32703','2144 MAJESTIC WOODS BOULEVARD, Apopka, FL 32712','4148 EAGLE FEATHER DRIVE, Orlando, FL 32829','4461 DRAYTON LANE, Oviedo, FL 32765','701 S ENDEAVOUR DRIVE, Winter Springs, FL 32708','741 CREPE MYRTLE CIRCLE, Apopka, FL 32712','271 HIDDEN SPRINGS CIRCLE, Kissimmee, FL 34743','707 MEADOWSIDE COURT, Orlando, FL 32825','10519 HUNTRIDGE ROAD, Orlando, FL 32825','2337 ROANOKE COURT, Lake Mary, FL 32746','9718 SYLVA COURT, Orlando, FL 32817','4742 LAKE SHARP DRIVE, Orlando, FL 32817','4418 WEEPING WILLOW CIRCLE, Casselberry, FL 32707','1915 NEEDHAM ROAD, Apopka, FL 32712','1131 RIDGE ROAD, Longwood, FL 32750','157 CORALWOOD CIRCLE, Kissimmee, FL 34743','1604 OAK HOLLOW ROAD, Clermont, FL 34711','2298 JESSICA LANE, Kissimmee, FL 34744','2401 ISLAND CLUB WAY, Orlando, FL 32822','759 VERONICA CIRCLE, Ocoee, FL 34761','4278 FOX HOLLOW CIRCLE, Casselberry, FL 32707','4530 SADDLE CREEK PLACE, Orlando, FL 32829','2980 MONICA TERRACE, Kissimmee, FL 34744','157 E 9TH STREET, Chuluota, FL 32766','11125 SYLVAN POND CIRCLE, Orlando, FL 32825','1636 SALMON STREET, Saint Cloud, FL 34771','2300 ALCLOBE CIRCLE, Ocoee, FL 34761','5171 MICHIGAN AVENUE, Sanford, FL 32771','2010 PALMETTO PINE LANE, Orlando, FL 32826','19 RANCH TRAIL ROAD, Debary, FL 32713','328 MISTY MEADOW DRIVE, Ocoee, FL 34761','1677 W AKRON DRIVE, Deltona, FL 32725','2413 WINFIELD DRIVE, Kissimmee, FL 34743','52 GRACIE ROAD, Debary, FL 32713','11626 PURPLE LILAC CIRCLE, Orlando, FL 32837','835 WOODFIELD COURT, Kissimmee, FL 34744','8001 ELMSTONE CIRCLE, Orlando, FL 32822','11653 WATER RIDGE LANE, Clermont, FL 34711','980 DINERO DRIVE, Winter Springs, FL 32708','2026 RIVER PARK BOULEVARD, Orlando, FL 32817','625 VIRGINIA WOODS LANE, Orlando, FL 32824','2575 TANDORI CIRCLE, Orlando, FL 32837','807 MCLEAN COURT, Orlando, FL 32825','1606 FOXCREEK LANE, Apopka, FL 32703','3138 HOLLAND DRIVE, Orlando, FL 32825','4246 SHADES CREST LANE, Sanford, FL 32773','12125 ROMERO STREET, Orlando, FL 32837','7718 FORT SUMTER DRIVE, Orlando, FL 32822','1015 APOPKA WOODS LANE, Orlando, FL 32824','3819 WINDWAY COURT, Orlando, FL 32817','1521 TRUMBULL STREET, Kissimmee, FL 34744','1508 FERGASON AVENUE, Deltona, FL 32725','3143 CAYMAN WAY, Orlando, FL 32812','1901 LEE WOOD COURT, Saint Cloud, FL 34772','7305 VICTORIA CIRCLE, Orlando, FL 32835','345 SAXON BOULEVARD, Deltona, FL 32725','9564 CHANDON DRIVE, Orlando, FL 32825','14533 POINTE EAST TRAIL, Clermont, FL 34711','8644 CLAIBORNE COURT, Orlando, FL 32825','298 HERONWOOD CIRCLE, Deltona, FL 32725','3621 FOX HOLLOW DRIVE, Orlando, FL 32829','2009 WILLIAMS AVENUE, Sanford, FL 32771','6525 GROSVENOR LANE, Orlando, FL 32835','9560 BRACKIN STREET, Orlando, FL 32825','11826 CLAIR PLACE, Clermont, FL 34711','1360 INDIAN ROCK COURT, Deltona, FL 32725','602 WHITTINGHAM PLACE, Lake Mary, FL 32746','176 GOLFSIDE CIRCLE, Sanford, FL 32773','10715 BROWN TROUT CIRCLE, Orlando, FL 32825','950 SYLVIA DRIVE, Deltona, FL 32725','2494 PINE CHASE CIRCLE, Saint Cloud, FL 34769','4011 CABAN COURT, Orlando, FL 32822','2342 CARRIAGE RUN ROAD, Kissimmee, FL 34741','139 HONEYWOOD DRIVE, Kissimmee, FL 34743','1902 E BARLINGTON DRIVE, Deltona, FL 32725','1231 ANDERSON STREET, Deltona, FL 32725','426 ROCKFORD STREET, Deltona, FL 32725','130 PINEWOOD CIRCLE, Kissimmee, FL 34743','602 ARMADILLO DRIVE, Deltona, FL 32725','1041 ALAHAMBRA STREET, Deltona, FL 32725','1183 WINDY WAY, Apopka, FL 32703','1985 E 4TH STREET, Sanford, FL 32771','7325 VICTORIA CIRCLE, Orlando, FL 32835','10731 LAKE RALPH DRIVE, Clermont, FL 34711','2550 IVYDALE DRIVE, Deltona, FL 32725','1420 ELKCAM BOULEVARD, Deltona, FL 32725','312 COUNTRY LANDING BOULEVARD, Apopka, FL 32703','139 N CERVIDAE DRIVE, Apopka, FL 32703','135 LAKE ADA CIRCLE, Sanford, FL 32773','30836 SUNNINGDALE STREET, Sorrento, FL 32776','2514 GINGER MILL BOULEVARD, Orlando, FL 32837','1201 ROMA CT, Orlando, FL 32825','550 LYNN ST, Oviedo, FL 32765','2040 S OLD MILL DR, Deltona, FL 32725','2040 S OLD MILL DR, Deltona, FL 32725','3034 SANDSTONE CIR, Saint Cloud, FL 34772','839 NEPTUNE POINTE LN, Kissimmee, FL 34744','2335 ASHINGTON PARK DR, Apopka, FL 32703','9270 VENEZIA PLANTATION DR, Orlando, FL 32829','297 SKY VALLEY ST, Clermont, FL 34711','2549 EUSTACE DR, Deltona, FL 32725','4912 E ST BRIDES CIR, Orlando, FL 32812','114 WHEATFIELD CIR, Sanford, FL 32771','2480 DEER CREEK BLVD, Saint Cloud, FL 34772','2625 EAGLES NEST CT, Orlando, FL 32837','9325 LARETTE DR, Orlando, FL 32817','1055 LIZA ST, Saint Cloud, FL 34771','324 OLD MILL RD, Enterprise, FL 32725','502 CANARY ISLAND CT, Orlando, FL 32828','321 CONCH KEY WAY, Sanford, FL 32771','13137 LOS ANGELES WOODS LN, Orlando, FL 32824','1261 HANCOCK CIR, Saint Cloud, FL 34769','1522 STONEYWOOD WAY, Apopka, FL 32712','1522 STONEYWOOD WAY, Apopka, FL 32712','2021 ALLSPICE AVE, Orlando, FL 32837','3206 MATTSON DR, Orlando, FL 32825','2342 AUDLEY ST, Oviedo, FL 32765','16408 CAJU RD, Clermont, FL 34711','1013 E RIVIERA BLVD, Oviedo, FL 32765','907 FEATHER DR, Deltona, FL 32725','820 KAZAROS CIR, Ocoee, FL 34761','200 BIRDIEWOOD CT, Debary, FL 32713','12308 ABBERTON CT, Orlando, FL 32837','11250 PINEWOOD COVE LN, Orlando, FL 32817','1525 RANDOLPH ST, Deltona, FL 32725','2526 BULLION LOOP, Sanford, FL 32771','409 NEWHEARTH CIR, Winter Garden, FL 34787','409 NEWHEARTH CIR, Winter Garden, FL 34787','2037 WEKIVA RESERVE BLVD, Apopka, FL 32703','2662 NEWBOLT DR, Orlando, FL 32817','3012 SANGRIA ST, Kissimmee, FL 34744','4263 VIXEN CT, Oviedo, FL 32765','3008 SANGRIA ST, Kissimmee, FL 34744','13011 DEVONSHIRE RD, Orlando, FL 32832','218 IOWA WOODS W CIR, Orlando, FL 32824','1480 S SEAGATE DR, Deltona, FL 32725','128 BRISTOL FOREST TRL, Sanford, FL 32771','9413 CANDICE CT, Orlando, FL 32832','10036 LETHBRIDGE DR, Orlando, FL 32829','105 HOLLOWAY CT, Sanford, FL 32771','105 HOLLOWAY CT, Sanford, FL 32771','106 DOMINION CT, Sanford, FL 32771','12957 DOWNSTREAM CIR, Orlando, FL 32828','325 ALEMANDER AVE, Debary, FL 32713','1243 CREEK WOODS CIR, Saint Cloud, FL 34772','618 N GOODRICH DR, Deltona, FL 32725','1409 COURT ST, Sanford, FL 32771','3924 BENSON PARK BLVD, Orlando, FL 32829','1339 AUGUSTA NATIONAL BLVD, Winter Springs, FL 32708','1021 ELKCAM BLVD, Deltona, FL 32725','10536 CHERRY OAK CIR, Orlando, FL 32817','2216 GRAND TREE CT, Lake Mary, FL 32746','1045 HAMPSHIRE CT, Winter Park, FL 32792','2290 LAKEVIEW AVE, Clermont, FL 34711','3914 CEDAR HAMMOCK TRL, Saint Cloud, FL 34772','289 PINE TRACE DR, Deltona, FL 32725','13191 LUXBURY LOOP, Orlando, FL 32837','1904 NICHOLAS PL, Saint Cloud, FL 34771','1115 ROYALTON RD, Orlando, FL 32825','408 WOODBURY PINES CIR, Orlando, FL 32828','531 SPINNAKER DR, Orlando, FL 32835','100 TOLLGATE TRAIL, Longwood, FL 32750','3162 TOURAINE AVENUE, Orlando, FL 32812','2215 ORCHARD DRIVE, Apopka, FL 32712','6355 JUDITH COURT, Saint Cloud, FL 34771','111 ORIENTA DRIVE, Altamonte Springs, FL 32701','614 E HELM WAY, Casselberry, FL 32707','451 MARIGOLD ROAD, Casselberry, FL 32707','1291 PUNTA GORDA CIRCLE, Winter Springs, FL 32708','791 E BIRCHWOOD CIRCLE, Kissimmee, FL 34743','1937 DUPONT COURT, Deltona, FL 32725','878 CLOVERLEAF BOULEVARD, Deltona, FL 32725','213 N ALDERWOOD STREET, Winter Springs, FL 32708','5127 OAK HILL DRIVE, Winter Park, FL 32792','653 LA SALLE DRIVE, Altamonte Springs, FL 32714','290 WETUMPA COURT, Casselberry, FL 32707','125 CLOVER LANE, Longwood, FL 32750','1849 CONCORD DR, Apopka, FL 32703','2468 MONTE CRISTO WAY, Sanford, FL 32771','1581 SILHOUETTE DRIVE, Clermont, FL 34711','908 GIRARD DRIVE, Orlando, FL 32824','2025 VICTORIA FALLS DRIVE, Orlando, FL 32824','385 COVENTRY ESTATES BOULEVARD, Deltona, FL 32725','2930 SPRINGLAKE VILLAGE BOULEVARD, Kissimmee, FL 34744','870 MAYBROOK DRIVE, Deltona, FL 32725','1124 DEER LAKE CIRCLE, Apopka, FL 32712','2723 CRANES COVE DRIVE, Kissimmee, FL 34741','2026 CASSINGHAM CIRCLE, Ocoee, FL 34761','1015 COVINGTON ST, Oviedo, FL 32765','4339 ROCKY RIDGE PL, Sanford, FL 32773','821 ABBOTT AVE, Deltona, FL 32725','821 ABBOTT AVE, Deltona, FL 32725','2409 OAK RUN BLVD, Kissimmee, FL 34744','2409 OAK RUN BLVD, Kissimmee, FL 34744','2720 SHEILA DR, Apopka, FL 32712','14825 CEDAR BRANCH WAY, Orlando, FL 32824','2111 ADAMS RIDGE RD, Apopka, FL 32703','46 S CERVIDAE DR, Apopka, FL 32703','882 STRATTON ST, Deltona, FL 32725','518 TIBERON COVE RD, Longwood, FL 32750','622 OAK MANOR CIR, Orlando, FL 32825','409 CLOVERLEAF BLVD, Deltona, FL 32725','2811 LATTER DAY CT, Saint Cloud, FL 34772','2343 FITZPATRICK TER, Deltona, FL 32725','1677 12TH ST, Clermont, FL 34711','3165 LAKE GEORGE COVE DR, Orlando, FL 32812','541 FLAGLER ST, Deltona, FL 32725','1298 FORT SMITH BLVD, Deltona, FL 32725','13239 CANNA LILY DR, Orlando, FL 32824','381 W DE CARLO DR, Deltona, FL 32725','552 BISON CIR, Apopka, FL 32712','5582 PATS PT, Winter Park, FL 32792','2698 SUNBURY ST, Clermont, FL 34711','619 WEKIVA CREST DR, Apopka, FL 32712','638 APPLEGATE TER, Deltona, FL 32725','1036 N HARBOR DR, Deltona, FL 32725','3909 GYPSUM LN, Orlando, FL 32826','3909 GYPSUM LN, Orlando, FL 32826','582 HARDWOOD PL, Lake Mary, FL 32746','3083 WOOLRIDGE DR, Orlando, FL 32837','14321 GOLDEN RAIN TREE BLVD, Orlando, FL 32828','2669 SEDGEFIELD AVE, Deltona, FL 32725','2824 OAK SHORE RD, Oviedo, FL 32766','115 RACHEL LIN LN, Saint Cloud, FL 34771','16829 GOLD STAR CT, Clermont, FL 34714','14080 BRADBURY RD, Orlando, FL 32828','764 GALWAY BLVD, Apopka, FL 32703','4719 FORT KNOX CT, Orlando, FL 32822','4719 FORT KNOX CT, Orlando, FL 32822','143 KNIGHTS HOLLOW DR, Apopka, FL 32712','1165 TROTWOOD BLVD, Winter Springs, FL 32708','1165 TROTWOOD BLVD, Winter Springs, FL 32708','2349 CILANTRO DR, Orlando, FL 32837','2117 TURMERIC AVE, Orlando, FL 32837','1638 LAWNDALE CIR, Winter Park, FL 32792','3978 CEDAR HAMMOCK TRL, Saint Cloud, FL 34772','3978 CEDAR HAMMOCK TRL, Saint Cloud, FL 34772','1713 COUNTRY TERRACE LN, Apopka, FL 32703','1706 SANFORD AVE, Sanford, FL 32771','167 Portstewart Drive, Orlando, FL 32828','8353 Lake Crowell Circle, Orlando, FL 32836','3712 Manteo Circle, Orlando, FL 32837','618 Crownclover Avenue, Orlando, FL 32828','2631 Runyon Circle, Orlando, FL 32837','2627 VERONA TRL, WINTER PARK, FL 32789','4464 TUSCANY ISLAND CT, WINTER PARK, FL 32792','359 REMINGTON DR, OVIEDO, FL 32765','1336 N MARCY DR, LONGWOOD, FL 32750','1363 N MARCY DR, LONGWOOD, FL 32750','1368 SAN DIEGO CT, WINTER SPRINGS, FL 32708','4057 GALLAGHER LOOP, CASSELBERRY, FL 32707','515 MANDAN CT, LAKE MARY, FL 32746','2750 RUNNING SPRINGS, LOOP OVIEDO, FL 32765','1671 MARKHAM WOODS RD, LONGWOOD, FL 32779','854 SHRIVER CIR, LAKE MARY, FL 32746','2761 TEAK PL, LAKE MARY, FL 32746','3063 MANDOLIN DR, KISSIMMEE, FL 34744','2956 LEBA LN, SAINT CLOUD, FL 34772','3837 SHOREVIEW DR, KISSIMMEE, FL 34744','10802 ARBOR VIEW BLVD, ORLANDO, FL 32825','1340 PELHAM RD, WINTER PARK, FL 32789','418 COVERED BRIDGE DR, OCOEE, FL 34761','2724 MUSCATELLO ST, ORLANDO, FL 32837','16957 DEER OAK LN, ORLANDO, FL 32828','115 W SPRUCE ST, ORLANDO, FL 32804','14033 DEEP LAKE DR, ORLANDO, FL 32826','2141 SETTLERS TRL, ORLANDO, FL 32837','13528 HERON CAY CT, ORLANDO, FL 32837','9307 LAKE FISCHER BLVD, GOTHA, FL 34734','8556 SUMMERVILLE PL, ORLANDO, FL 32819','313 LA CREEK CT, DEBARY, FL 32713','1005 FOXHOUND WY, LONGWOOD, FL 32750','4551 BOND LN, OVIEDO, FL 32765','5110 OTTERS DEN TRL, SANFORD, FL 32771','992 SLEEPING ROCK CT, WINTER SPRINGS, FL 32708','927 ARABIAN AVE, WINTER SPRINGS, FL 32708','2711 PEARL CT, KISSIMMEE, FL 34743','1440 WESTCHESTER AVE, WINTER PARK, FL 32789','1213 GUNNISON AVE, ORLANDO, FL 32804','492 SADDELL BAY LOOP, OCOEE, FL 34761','1014 PALMER ST, ORLANDO, FL 32801','12720 DALLINGTON TER, WINTER GARDEN, FL 34787','380 MISTY OAKS RUN, CASSELBERRY, FL 32707','2141 CARPATHIAN DR, APOPKA, FL 32712','9030 FLAT ROCK LN, ORLANDO, FL 32832','2001 MOUNTLEIGH TRL, ORLANDO, FL 32824','1415 SUN MEADOW DR, ORLANDO, FL 32824','1358 JECENIA BLOSSOM DR, APOPKA, FL 32712','15336 MONTESINO DR, ORLANDO, FL 32828','6266 Golden Dewdrop Trail, Windermere, FL 34786']
      # sts = ['992 W CHARING CROSS CIR, Lake Mary, FL 32746','2037 WEKIVA RESERVE BLVD, Apopka, FL 32703','2179 Hunley Ave, Apopka, FL 32703','1152 Alston Bay Blvd, Apopka, FL 32703','988 Alston Bay Blvd, Apopka, FL 32703','3716 Moon Dancer Pl, Saint Cloud, FL 34772','1971 Valley Forge Dr, Saint Cloud, FL 34769','2268 Victoria Falls Drive, Orlando, FL 32824','3522 WOODLEY PARK PL, Oviedo, FL 32765','445 ORANGE STREET, Altamonte Springs, FL 32701','3252 NIGHT BREEZE LANE, Lake Mary, FL 32746','14507 PINE CONE TRAIL, Clermont, FL 34711','14717 DAY LILY COURT, Orlando, FL 32824','1130 HARBOUR VIEW CIRCLE, Longwood, FL 32750','641 OAK HOLLOW WAY, Altamonte Springs, FL 32714','10214 CINCHWOOD LANE, Orlando, FL 32836','10214 CINCHWOOD LANE, Orlando, FL 32836','10214 CINCHWOOD LANE, Orlando, FL 32836','1855 EAGLES REST DRIVE, Apopka, FL 32712','210 SADLER AVENUE, Oakland, FL 34760','13739 DORNOCH DRIVE, Orlando, FL 32828','230 LAKEBREEZE CIRCLE, Lake Mary, FL 32746','397 FAIRFIELD DRIVE, Sanford, FL 32771','17536 WOODCREST WAY, Clermont, FL 34714','7761 INDIAN RIDGE TRAIL, Kissimmee, FL 34747','7761 INDIAN RIDGE TRAIL, Kissimmee, FL 34747','2893 BREEZY MEADOW ROAD, Apopka, FL 32712','1841 CENTENNIAL AVENUE, Saint Cloud, FL 34769','5006 TANGERINE AVENUE, Winter Park, FL 32792','2119 JUSTICE LANE, Saint Cloud, FL 34769','180 BROADMOOR AVENUE, Lake Mary, FL 32746','180 BROADMOOR AVENUE, Lake Mary, FL 32746','7266 CARILLON AVENUE, Cocoa, FL 32927','4244 ANDOVER CAY BOULEVARD, Orlando, FL 32825','700 LAKEVIEW AVENUE, Winter Garden, FL 34787','3225 INDIAN TRAIL, Eustis, FL 32726','5050 CAPE HATTERAS DRIVE, Clermont, FL 34714','11044 DAWNVIEW LANE, Orlando, FL 32825','10313 STRATFORD POINTE AVENUE, Orlando, FL 32832','1141 DUNCAN DRIVE, Winter Springs, FL 32708','1220 PORTMOOR WAY, Winter Garden, FL 34787','13032 COG HILL WAY, Orlando, FL 32828','13032 COG HILL WAY, Orlando, FL 32828','14919 LAKE AZURE DRIVE, Orlando, FL 32824','2618 SAN SIMEON WAY, Kissimmee, FL 34741','550 PRESTON ROAD, Longwood, FL 32750','10601 VISTA DEL SOL CIRCLE, Clermont, FL 34711','1341 EUSTIS ROAD, Eustis, FL 32726','2472 ROSECRANS COURT, Apopka, FL 32712','648 CROWNCLOVER AVENUE, Orlando, FL 32828','13350 LOBLOLLY LANE, Clermont, FL 34711','13350 LOBLOLLY LANE, Clermont, FL 34711','331 FERDINAND DRIVE, Longwood, FL 32750','13544 EYAS ROAD, Orlando, FL 32837','13544 EYAS ROAD, Orlando, FL 32837','14511 MANDOLIN DRIVE, Orlando, FL 32837','648 RANDON TERRACE, Lake Mary, FL 32746','712 LANCEWOOD DRIVE, Winter Springs, FL 32708','632 FALCON COURT, Winter Springs, FL 32708','860 Brightwater Circle, Maitland, FL 32751','14126 FOX GLOVE ST, Winter Garden, FL 34787','1358 SHELTER ROCK RD, Orlando, FL 32835','1324 VALLEY PINE CIR, Apopka, FL 32712','2341 WICKDALE CT, Ocoee, FL 34761','2465 ORSOTA CIR, Ocoee, FL 34761','13212 OVERSTREET RD, Windermere, FL 34786','596 LEXINGTON PKWY, Apopka, FL 32712','2832 BREEZY MEADOW DRIVE, Apopka, FL 32712','2623 SHEILA DR, Apopka, FL 32712','1805 Grasmere Drive, Apopka, FL 32703','13527 MADISON DOCK RD, Orlando, FL 32828','1041 FOGGY BROOK PL, Longwood, FL 32750','2396 WESTWOOD DR, Longwood, FL 32779','276 LINBERRY LN, Ocoee, FL 34761','3690 ROCHELLE LN, Apopka, FL 32712','261 LANCER OAK DR, Apopka, FL 32712','2018 SAWGRASS DR, Apopka, FL 32712','2255 PELINION ST, Apopka, FL 32712','2424 CEDAR KNOLL DR, Apopka, FL 32712','2093 JUDITH PL, Longwood, FL 32779','2026 CASCADES COVE DR, Orlando, FL 32820','2774 UNIVERSITY ACRES DR, Orlando, FL 32817','2851 GRASSMOOR LOOP, Apopka, FL 32712','1466 Westbury Drive, Clermont, FL 34711','3510 Foxcroft Circle, Oviedo, FL 32765','2869 Picadilly Circle, Kissimmee, FL 34747','130 Thornbury Drive, Kissimmee, FL 34744','5846 Cheshire Cove Terrace, Orlando, FL 32829','2608 Florence Drive, Kissimmee, FL 34744','4017 Lillian Hall Lane, Orlando, FL 32812','10131 Malpas Point, Orlando, FL 32832','548 Fern Lake Drive, Orlando, FL 32825','8648 Vista Harbor Court, Orlando, FL 32836','1710 Lochshyre Loop, Ocoee, FL 34761','14330 Paradise Tree Drive, Orlando, FL 32828','8046 Solitaire Court, Orlando, FL 32836','1201 Shelter Rock Road, Orlando, FL 32835','536 Belhaven Falls Drive, Ocoee, FL 34761','6351 Point Hancock Drive, Winter Garden, FL 34787','30424 Jubilee Way, Sorrento, FL 32776','15612 Firelight Drive, Winter Garden, FL 34787','664 Cimarosa Court, Ocoee, FL 34761','1477 Pine Marsh Loop, Saint Cloud, FL 34771','2716 Kingston Ridge Drive, Clermont, FL 34711','3501 Starcatcher Street, Saint Cloud, FL 34772','4042 Evander Drive, Orlando, FL 32812','9404 Whispering Meadows Lane, Orlando, FL 32825','3226 Open Meadow Loop, Oviedo, FL 32766','3441 Glen Village Court, Orlando, FL 32822','2935 Red Oak Drive, Kissimmee, FL 34744','13442 Budworth Circle, Orlando, FL 32832','1133 Mosaic Drive, Celebration, FL 34747','294 Wescliff Drive, Ocoee, FL 34761','606 Water Oak Lane, Longwood, FL 32779','4205 Whitemoor Court, Clermont, FL 34711','10785 Willow Ridge Loop, Orlando, FL 32825','12622 Scottish Pine Lane, Clermont, FL 34711','5472 Rishley Run Way, Mount Dora, FL 32757','13914 Eylewood Drive, Winter Garden, FL 34787','12831 Cragside Lane, Windermere, FL 34786','787 High Grove Park Court, Oviedo, FL 32765','8511 Peconic Drive, Orlando, FL 32835','4658 Forrestal Avenue, Orlando, FL 32806','5115 Rishley Run Way, Mount Dora, FL 32757','1201 Providence Boulevard, Kissimmee, FL 34744','501 Terrace Cove Way, Orlando, FL 32828','6628 Bouganvillea Crescent Drive, Orlando, FL 32809','4598 S Hampton Drive, Orlando, FL 32812','2137 Firestone Court, Oviedo, FL 32765','6254 Buford Street, Orlando, FL 32835','1702 Baxter Avenue, Orlando, FL 32806','16112 Birchwood Way, Orlando, FL 32828','12784 Waterhaven Circle, Orlando, FL 32828','13959 Marine Drive, Orlando, FL 32832','4212 Trentonian Court, Orlando, FL 32812','2446 Ridgemoor Drive, Orlando, FL 32828','2207 Summer Raye Court, Saint Cloud, FL 34772','2125 Sunset Terrace Drive, Orlando, FL 32825','359 Remington Drive, Oviedo, FL 32765','2852 Magnolia Blossom Circle, Clermont, FL 34711','1066 New Castle Lane, Oviedo, FL 32765','825 Grey Heron Place, Chuluota, FL 32766','4131 Bell Tower Court, Belle Isle, FL 32812','605 S Dillingham Avenue, Kissimmee, FL 34741','643 Green Meadow, Maitland, FL 32751','2802 Ashbridge Street, Orlando, FL 32825','3851 Wood Thrush Drive, Kissimmee, FL 34744','2711 Estep Court, Ocoee, FL 34761','10059 Cypress Knee Circle, Orlando, FL 32825','10708 Fallow Trail, Orlando, FL 32817','13750 Via Roma Circle, Clermont, FL 34711','4999 Disston Drive, Saint Cloud, FL 34771','9310 Whittingham Drive, Orlando, FL 32817','10021 Cove Lake Drive, Orlando, FL 32836','14522 Quail Trail Circle, Orlando, FL 32837','10632 Crystal Springs Court, Orlando, FL 32825','1014 Harbor Hill Street, Winter Garden, FL 34787','1000 Horton Court, Oviedo, FL 32765','2484 Prairie View Drive, Winter Garden, FL 34787','10342 Oakview Pointe Terrace, Gotha, FL 34734','14245 Paradise Tree Drive, Orlando, FL 32828','4020 Serena Lane, Clermont, FL 34711','5100 Appenine Loop, Saint Cloud, FL 34771','10402 Vista Pines Loop, Clermont, FL 34711','2130 White Eagle Street, Clermont, FL 34714','11885 Zelkova Lane, Orlando, FL 32827','15516 Charter Oaks Trail, Clermont, FL 34711','225 Wild Pine Point, Orlando, FL 32828','1774 Derby Glen Drive, Orlando, FL 32837','671 First Cape Coral Drive, Winter Garden, FL 34787','2710 Valiant Drive, Clermont, FL 34711','9530 Osprey Landing Drive, Orlando, FL 32832','1408 Lexi Davis Street, Orlando, FL 32828','5718 Sweetheart Court, Saint Cloud, FL 34772','10730 Fairhaven Way, Orlando, FL 32825','3523 Wading Heron Terrace, Oviedo, FL 32766','3139 Pizzaro Place, Clermont, FL 34715','30106 Jutland Court, Mount Dora, FL 32757','1413 Resolute Street, Kissimmee, FL 34747','828 Crooked Branch Drive, Clermont, FL 34711','1433 Pine Marsh Loop, Saint Cloud, FL 34771','4752 Coppola Drive, Mount Dora, FL 32757','1410 Sean Court, Saint Cloud, FL 34772','2410 Country Pond Court, Saint Cloud, FL 34771','926 Pawstand Road, Celebration, FL 34747','2103 Hammock Moss Drive, Orlando, FL 32820','2557 Runyon Circle, Orlando, FL 32837','12125 Dyson Court, Orlando, FL 32821','14009 Fairwinds Court, Orlando, FL 32824','14761 Grand Cove Drive, Orlando, FL 32837','9127 North Bay Boulevard, Orlando, FL 32819','1532 Cedar Lake Drive, Orlando, FL 32824','5430 Birchbend Loop, Oviedo, FL 32765','9037 Notchwood Court, Orlando, FL 32825','14203 Whooping Crane Lane, Orlando, FL 32824','3741 Linsbury Street, Orlando, FL 32817','15349 Markham Drive, Clermont, FL 34714','5327 Brady Lane, Orlando, FL 32814','11800 Gray Rock Trail, Windermere, FL 34786','10924 Eliotti Street, Orlando, FL 32832','3018 Ash Park Point, Winter Park, FL 32792','8530 Sidon Street, Orlando, FL 32817','4961 Brightmour Circle, Orlando, FL 32837','2474 Orsota Circle, Ocoee, FL 34761','4630 Woodlands Village Drive, Orlando, FL 32835','11063 Summerspring Lakes Drive, Orlando, FL 32825','7030 Maverick Trail, Saint Cloud, FL 34771','7987 Sapphire Lane, Orlando, FL 32822','2107 Continental Street, Saint Cloud, FL 34769','3700 Brookmyra Drive, Orlando, FL 32837','2387 Sabastian Street, Mount Dora, FL 32757','9152 Mossy Oak Lane, Clermont, FL 34711','1931 Lochshyre Loop, Ocoee, FL 34761','3430 Turningwind Lane, Winter Garden, FL 34787','3441 Diamond Leaf Lane, Oviedo, FL 32766','10929 Arrowtree Boulevard, Clermont, FL 34715','10037 Richardson Court, Orlando, FL 32825','1003 Green Branch Court, Oviedo, FL 32765','2930 Marjorie Road, Saint Cloud, FL 34772','819 Hallowell Circle, Orlando, FL 32828','2841 Ithaca Court, Orlando, FL 32826','7808 Turkey Oak Lane, Kissimmee, FL 34747','14565 Braddock Oak Drive, Orlando, FL 32837','6011 Cornelia Avenue, Orlando, FL 32807','13436 Texas Woods Circle, Orlando, FL 32824','5888 Lake Melrose Drive, Orlando, FL 32829','12723 Winding Woods Lane, Orlando, FL 32832','13115 Social Lane, Winter Garden, FL 34787','559 Horseman Drive, Oviedo, FL 32765','445 Ridge Forest Court, Sanford, FL 32771','712 Seneca Trail, Saint Cloud, FL 34772','1848 Blue Fox Court, Orlando, FL 32825','2256 Windcrest Lake Circle, Orlando, FL 32824','687 Woodland Creek Boulevard, Kissimmee, FL 34744','777 Kyle Court, Chuluota, FL 32766','17714 Sterling Pond Lane, Orlando, FL 32820','13843 Old Dock Road, Orlando, FL 32828','4015 Bounce Drive, Orlando, FL 32812','8135 Fort Thomas Way, Orlando, FL 32822','10935 Lanesboro Court, Orlando, FL 32825','1013 Vinsetta Circle, Winter Garden, FL 34787','3215 Hurd Avenue, Orlando, FL 32812','32312 Avington Road, Sorrento, FL 32776','973 Tramells Trail, Kissimmee, FL 34744','2260 Blackjack Oak Street, Ocoee, FL 34761','8240 Prestbury Drive, Orlando, FL 32832','12096 Alder Branch Loop, Orlando, FL 32824','3800 Blue Dasher Drive, Kissimmee, FL 34744','1961 Windcrest Lake Circle, Orlando, FL 32824','10615 Mere Parkway, Orlando, FL 32832','1931 River Park Boulevard, Orlando, FL 32817','13251 Moonflower Court, Clermont, FL 34711','1946 Pantheon Drive, Winter Garden, FL 34787','1932 Pantheon Drive, Winter Garden, FL 34787','10182 Pointview Court, Orlando, FL 32836','2046 Solar Drive, Winter Garden, FL 34787','1599 Anorada Boulevard, Kissimmee, FL 34744','2707 Kingston Ridge Drive, Clermont, FL 34711','1126 New Castle Court, Oviedo, FL 32765','2574 Jenscot Road, Saint Cloud, FL 34771','2044 Solar Drive, Winter Garden, FL 34787','4611 Cypress Forest Lane, Saint Cloud, FL 34772','1428 Brook Hollow Drive, Orlando, FL 32824','4618 Tiffany Woods Circle, Oviedo, FL 32765','2049 Solar Drive, Winter Garden, FL 34787','1070 Lascala Drive, Windermere, FL 34786','9127 Shadowbrook Trail, Orlando, FL 32825','1446 Silver Cove Drive, Clermont, FL 34714','3888 Shoreview Drive, Kissimmee, FL 34744','1757 Auburn Lakes Drive, Rockledge, FL 32955','1693 Strathmore Circle, Mount Dora, FL 32757','5297 Mill Stream Drive, Saint Cloud, FL 34771','15333 Heron Hideaway Circle, Winter Garden, FL 34787','2304 Cornhill Drive, Orlando, FL 32837','13542 Tenbury Wells Way, Winter Garden, FL 34787','1934 Pantheon Drive, Winter Garden, FL 34787','20907 Sullivan Ranch Boulevard, Mount Dora, FL 32757','2201 Deata Court, Saint Cloud, FL 34772','1930 Pantheon Drive, Winter Garden, FL 34787','2412 Oakington Street, Winter Garden, FL 34787','2788 Cullens Court, Ocoee, FL 34761','1313 Horizon Creek Court, Orlando, FL 32828','7409 Fairgrove Avenue, Windermere, FL 34786','6965 Corley Avenue, Windermere, FL 34786','2781 Kissimmee Bay Circle, Kissimmee, FL 34744','16345 Myers Court, Clermont, FL 34711','6735 Edgeworth Drive, Orlando, FL 32819','9862 Heron Pointe Drive, Orlando, FL 32832','6540 Dover Cove Drive, Orlando, FL 32822','13548 Kitty Fork Road, Orlando, FL 32828','1742 Delafield Drive, Winter Garden, FL 34787','2044 Pirie Place, Saint Cloud, FL 34769','1210 Epson Oaks Way, Orlando, FL 32837','3810 Doune Way, Clermont, FL 34711','12833 Hyland Lane, Clermont, FL 34711','1627 Whitney Isles Drive, Windermere, FL 34786','1744 Vale Drive, Clermont, FL 34711','516 Egret Place Drive, Winter Garden, FL 34787','14525 Spotted Sandpiper Boulevard, Winter Garden, FL 34787','2224 Fairglenn Way, Winter Park, FL 32792','4613 Barbados Loop, Clermont, FL 34711','9252 Lake Hickory Nut Drive, Winter Garden, FL 34787','839 Neptune Pointe Lane, Kissimmee, FL 34744','2632 Belmont Place, Kissimmee, FL 34744','7490 Betty Street, Winter Park, FL 32792','201 Pond Road, Mount Dora, FL 32757','393 Wekiva Cove Road, Longwood, FL 32779','1848 Osman Avenue, Orlando, FL 32806','2455 Mockingbird Avenue, Saint Cloud, FL 34771','2531 Long Iron Court, Longwood, FL 32779','410 Sweet Bay Drive, Longwood, FL 32779','3666 Limestone Street, Clermont, FL 34711','3517 Forest Ridge Lane, Kissimmee, FL 34741','8606 Villanova Street, Orlando, FL 32817','124 Seville Pointe Avenue, Orlando, FL 32807','10318 Jane Eyre Drive, Orlando, FL 32825','14009 Chicora Crossing Boulevard, Orlando, FL 32828','6612 Duncaster Street, Windermere, FL 34786','1925 Peach Tree Boulevard, Saint Cloud, FL 34769','15550 Camp Dubois Crescent, Winter Garden, FL 34787','3238 Stonewyck Street, Orlando, FL 32824','6971 Corley Avenue, Windermere, FL 34786','10137 Hatton Circle, Orlando, FL 32832','1709 Snaresbrook Way, Orlando, FL 32837','700 Grey Heron Place, Chuluota, FL 32766','3220 Stonewyck Street, Orlando, FL 32824','5692 Merlin Way, Saint Cloud, FL 34772','10533 Bruun Place, Orlando, FL 32825','102 Riverbend Boulevard, Longwood, FL 32779','14544 Pointe East Trail, Clermont, FL 34711','303 Willowbay Ridge Street, Sanford, FL 32771','16524 Magnolia Terrace, Montverde, FL 34756','2600 Pickett Downs Drive, Chuluota, FL 32766','3116 Rawcliffe Road, Clermont, FL 34714','3044 Lake Jean Drive, Orlando, FL 32817','3425 Quail Nest Court, Orlando, FL 32829','1766 Thetford Circle, Orlando, FL 32824','12137 Mangloe Court, Orlando, FL 32837','7636 Lake Angelina Drive, Mount Dora, FL 32757','2379 Sweetwater Boulevard, Saint Cloud, FL 34772','7714 Glynde Hill Drive, Orlando, FL 32835','1619 Mistflower Lane, Winter Garden, FL 34787','15312 Chahtah Court, Orlando, FL 32828','2480 Deer Creek Boulevard, Saint Cloud, FL 34772','2904 Sunset Retreat Court, Kissimmee, FL 34747','7289 Mardell Court, Orlando, FL 32835','4722 Coppola Drive, Mount Dora, FL 32757','2943 Sunset Vista Boulevard, Kissimmee, FL 34747','2205 Wintermere Pointe Drive, Winter Garden, FL 34787','13852 Red Mangrove Drive, Orlando, FL 32828','3501 Mcallister Way, Orlando, FL 32837','13217 Briar Forest Court, Orlando, FL 32828','8471 Lake Waverly Lane, Orlando, FL 32829','9845 Bennington Chase Drive, Orlando, FL 32829','105 Albrighton Drive, Longwood, FL 32779','1200 Pine Harbor Point Circle, Orlando, FL 32806','1914 Big Cypress Drive, Saint Cloud, FL 34771','10531 Satinwood Circle, Orlando, FL 32825','16018 Yelloweyed Drive, Clermont, FL 34714','3028 Bransbury Court, Kissimmee, FL 34747','17029 Florence View Drive, Montverde, FL 34756','855 Whispering Cypress Lane, Orlando, FL 32824','3200 Wax Myrtle Court, Kissimmee, FL 34744','5007 Cawthon Way, Saint Cloud, FL 34771','4880 Manchester Drive, Rockledge, FL 32955','15000 Ozzi Street, Winter Garden, FL 34787','2561 Carrickton Circle, Orlando, FL 32824','5170 Log Wagon Road, Ocoee, FL 34761','13825 Vista Del Lago Boulevard, Clermont, FL 34711','1013 Belvoir Drive, Kissimmee, FL 34744','2354 Dryburgh Court, Orlando, FL 32828','3231 Open Meadow Loop, Oviedo, FL 32766','8052 Laurel Ridge Drive, Mount Dora, FL 32757','5183 Brightmour Circle, Orlando, FL 32837','5406 Baybrook Avenue, Orlando, FL 32819','5201 Wildflower Road, Orlando, FL 32821','5356 Mellow Palm Way, Winter Park, FL 32792','12149 Rebeccas Run Drive, Winter Garden, FL 34787','9108 Outlook Rock Trail, Windermere, FL 34786','2116 Justice Lane, Saint Cloud, FL 34769','723 Sandy Bar Drive, Winter Garden, FL 34787','30323 Tokara Terrace, Mount Dora, FL 32757','2838 Scenic Lane, Kissimmee, FL 34744','14807 Tanja King Boulevard, Orlando, FL 32828','6220 Beldon, Mount Dora, FL 32757','14424 Brushwood Way, Winter Garden, FL 34787','8760 Bay Ridge Boulevard, Orlando, FL 32819','3206 Mattson Drive, Orlando, FL 32825','10003 Baywater Breeze Drive, Orlando, FL 32827','515 Charleswood Avenue, Orlando, FL 32825','506 Divine Circle, Orlando, FL 32828','2016 Pine Bluff Avenue, Orlando, FL 32806','3232 Stonewyck Street, Orlando, FL 32824','6920 Ranchero Court, Saint Cloud, FL 34771','981 Jade Forest Avenue, Orlando, FL 32828','1013 E Riviera Boulevard, Oviedo, FL 32765','2863 North Pointe Boulevard, Kissimmee, FL 34744','1721 Stafford Drive, Belle Isle, FL 32809','7921 Fernleaf Drive, Orlando, FL 32836','848 Pecori Terrace, Ocoee, FL 34761','820 Kazaros Circle, Ocoee, FL 34761','1139 Anne Elisa Circle, Saint Cloud, FL 34772','3716 Heirloom Rose Place, Oviedo, FL 32766','5129 Tiber Way, Saint Cloud, FL 34771','5123 Tiber Way, Saint Cloud, FL 34771','4151 Conway Place Circle, Orlando, FL 32812','3110 Mandolin Drive, Kissimmee, FL 34744','18149 Cadence Street, Orlando, FL 32820','2352 Blackjack Oak Street, Ocoee, FL 34761','779 Lancer Circle, Ocoee, FL 34761','14545 Broadhaven Boulevard, Orlando, FL 32828','131 Sura Boulevard, Orlando, FL 32809','8324 Foxworth Circle, Orlando, FL 32819','3057 Bloomsbury Drive, Kissimmee, FL 34747','3832 Valencia Grove Lane, Orlando, FL 32817','3139 Queen Alexandria Drive, Kissimmee, FL 34744','2091 Terrace Boulevard, Longwood, FL 32779','21944 Belgian Court, Mount Dora, FL 32757','377 Fieldstream West Boulevard, Orlando, FL 32825','1305 Mactavandash Drive, Oviedo, FL 32765','670 Seneca Trail, Saint Cloud, FL 34772','8146 Jailene Drive, Windermere, FL 34786','2292 Stanford Court, Clermont, FL 34711','3525 Windansea Court, Clermont, FL 34711','11515 Sandy Hill Drive, Orlando, FL 32821','11731 Barletta Drive, Orlando, FL 32827','9338 Palm Tree Drive, Windermere, FL 34786','1537 Balsam Willow Trail, Orlando, FL 32825','2718 Hilmer Court, Orlando, FL 32806','14813 Perdido Drive, Orlando, FL 32828','16054 River Pointe Court, Orlando, FL 32828','5018 Hook Hollow Circle, Orlando, FL 32837','1273 Martin Boulevard, Orlando, FL 32825','9709 Crenshaw Circle, Clermont, FL 34711','12278 Gray Birch Circle, Orlando, FL 32832','8067 Roaring Creek Court, Kissimmee, FL 34747','3669 Rollingbrook Street, Clermont, FL 34711','8026 Rural Retreat Court, Orlando, FL 32819','5171 Preserve Boulevard, Saint Cloud, FL 34772','720 Parkmont Place, Winter Garden, FL 34787','3341 Hillmont Circle, Orlando, FL 32817','7906 Jailene Drive, Windermere, FL 34786','7431 Winding Lake Circle, Oviedo, FL 32765','13129 Baneberry Court, Clermont, FL 34711','2890 Strand Circle, Oviedo, FL 32765','763 Crystal Bay Lane, Orlando, FL 32828','1830 Morgans Mill Circle, Orlando, FL 32825','9680 Loblolly Pine Circle, Orlando, FL 32827','10118 Garden Rose Court, Orlando, FL 32825','15422 Greater Groves Boulevard, Clermont, FL 34714','641 Longmeadow Circle, Longwood, FL 32779','4542 Simmons Road, Orlando, FL 32812','6889 Scythe Avenue, Orlando, FL 32812','3005 Mystic Cove Drive, Orlando, FL 32812','10209 Cypress Trail Drive, Orlando, FL 32825','4400 Aruba Boulevard, Clermont, FL 34711','3214 Stonewyck Street, Orlando, FL 32824','3540 Vestavia Way, Longwood, FL 32779','8123 Laughing Gull Street, Winter Garden, FL 34787','13319 Falcon Pointe Drive, Orlando, FL 32837','3230 Palatka Street, Orlando, FL 32824','1243 Creek Woods Circle, Saint Cloud, FL 34772','1336 Golfside Drive, Winter Park, FL 32792','14652 Greater Pines Boulevard, Clermont, FL 34711','678 Winding Lake Drive, Clermont, FL 34711','12767 Holdenbury Lane, Windermere, FL 34786','3124 Stonewyck Street, Orlando, FL 32824','1433 Lexi Davis Street, Orlando, FL 32828','1023 Trout Creek Court, Oviedo, FL 32765','3924 Benson Park Boulevard, Orlando, FL 32829','4918 Terrapin Boulevard, Saint Cloud, FL 34771','3615 Aughton Court, Orlando, FL 32812','322 Arbor Pointe Avenue, Minneola, FL 34715','157 E Lake Brantley Drive, Longwood, FL 32779','3512 Tern Hollow Drive, Orlando, FL 32828','7900 Tumbleweed Court, Orlando, FL 32822','1466 Amanda Road, Kissimmee, FL 34744','3536 Mccormick Woods Drive, Ocoee, FL 34761','7433 Betty Street, Winter Park, FL 32792','825 Palmetto Terrace, Oviedo, FL 32765','10465 Stapeley Drive, Orlando, FL 32832','11326 Wishing Well Lane, Clermont, FL 34711','16532 Deer Chase Loop, Orlando, FL 32828','3520 Hollow Oak Run, Oviedo, FL 32766','1001 Pine Lane, Saint Cloud, FL 34771','3400 Canvas Street, Kissimmee, FL 34741','5269 Hammock Circle, Saint Cloud, FL 34771','811 Carrigan Avenue, Oviedo, FL 32765','2333 Avellino Avenue, Saint Cloud, FL 34771','1115 Royalton Road, Orlando, FL 32825','3513 Pintail Lane, Saint Cloud, FL 34772','531 Crownclover Avenue, Orlando, FL 32828','2983 Sabel Oak Place, Oviedo, FL 32765','3960 Liberty Hill Drive, Clermont, FL 34711','13258 Long Pine Trail, Clermont, FL 34711','9931 Sublette Avenue, Orlando, FL 32836','1115 Robert Ridge Court, Kissimmee, FL 34747','187 Osprey Lakes Circle, Chuluota, FL 32766','14837 Porter Road, Winter Garden, FL 34787','6647 Beth Road, Orlando, FL 32824','2305 Zaltana Circle, Minneola, FL 34715','825 Kingsbridge Drive, Oviedo, FL 32765','13931 Vista Del Lago Boulevard, Clermont, FL 34711','15703 Montesino Drive, Orlando, FL 32828','1028 Lascala Drive, Windermere, FL 34786','16014 Oak Spring Drive, Orlando, FL 32828','1620 Anna Catherine Drive, Orlando, FL 32828','14020 Fairway Willow Lane, Winter Garden, FL 34787','126 S Normandale Avenue, Orlando, FL 32835','5657 Pitch Pine Drive, Orlando, FL 32819','2831 Maria Isabel Avenue, Ocoee, FL 34761','2447 Osprey Woods Circle, Orlando, FL 32820','1639 Anna Catherine Drive, Orlando, FL 32828','2030 Sunset Terrace Drive, Orlando, FL 32825','8080 Indian Creek Boulevard, Kissimmee, FL 34747','2679 Willow Glen Circle, Kissimmee, FL 34744','907 Oak Chase Drive, Orlando, FL 32828','1571 Cheri Court, Kissimmee, FL 34744','1730 Connecticut Avenue, Saint Cloud, FL 34769','145 Summer Place Loop, Clermont, FL 34714','13619 Guildhall Circle, Orlando, FL 32828','12643 Cragside Lane, Windermere, FL 34786','17525 Silver Creek Court, Clermont, FL 34714','6321 Hidden Valley Court, Orlando, FL 32819','2813 Monticello Way, Kissimmee, FL 34741','1473 Amanda Road, Kissimmee, FL 34744','2868 Aloma Lake Run, Oviedo, FL 32765','962 Forest Hill Drive, Minneola, FL 34715','2881 Citron Drive, Longwood, FL 32779','3925 Whittington Drive, Orlando, FL 32817','15639 Carriage Hill Court, Clermont, FL 34711','14901 White Magnolia Court, Orlando, FL 32824','2769 Monticello Way, Kissimmee, FL 34741','1326 Blackwater Pond Drive, Orlando, FL 32828','2835 Kemper Avenue, Orlando, FL 32814','10029 Cheshunt Drive, Orlando, FL 32817','14863 Royal Poinciana Drive, Orlando, FL 32828','1710 Billingshurst Court, Orlando, FL 32825','4964 Southfork Ranch Drive, Orlando, FL 32812','1721 Cheltenborough Drive, Orlando, FL 32835','732 Pond Pine Court, Orlando, FL 32825','512 Carey Way, Orlando, FL 32825','1898 Gregory Road, Orlando, FL 32825','812 Rosemere Circle, Orlando, FL 32835','2782 Roccella Court, Kissimmee, FL 34747','1562 Setting Sun Court, Clermont, FL 34711','13239 Canna Lily Drive, Orlando, FL 32824','307 South Oakland Avenue, Minneola, FL 34715','10418 Lago Louisa Court, Clermont, FL 34711','2788 Kissimmee Bay Circle, Kissimmee, FL 34744','137 Thornbury Drive, Kissimmee, FL 34744','1007 Shady Maple Circle, Ocoee, FL 34761','473 Turnstone Way, Orlando, FL 32828','3508 Vinning Court, Kissimmee, FL 34741','672 Carrigan Woods Trail, Oviedo, FL 32765','217 Magical Way, Kissimmee, FL 34744','16045 Magnolia Hill Street, Clermont, FL 34714','1222 Molona Street, Reunion, FL 34747','837 Clifton Hills Street, Orlando, FL 32828','6029 Silk Oak Drive, Orlando, FL 32819','4125 Scarlet Branch Road, Orlando, FL 32824','13406 Meadow Bay Loop, Orlando, FL 32824','2032 Kaylas Court, Orlando, FL 32817','1030 Elizabeth Ridge Court, Kissimmee, FL 34747','5043 Tempic Drive, Mount Dora, FL 32757','2705 Portchester Court, Kissimmee, FL 34744','34145 Fortunado Street, Sorrento, FL 32776','10283 Kristen Park Drive, Orlando, FL 32832','116 Milinda Lane, Oviedo, FL 32765','5204 Hammock Pointe Court, Saint Cloud, FL 34771','8966 Vickroy Terrace, Oviedo, FL 32765','12924 Lower River Boulevard, Orlando, FL 32828','3323 Shrike Hill Court, Clermont, FL 34714','4737 Blue Major Drive, Windermere, FL 34786','32807 Wolfs Trail, Sorrento, FL 32776','1213 Burning Tree Lane, Winter Park, FL 32792','7116 Calm Cove Court, Windermere, FL 34786','10637 Deergrass Lane, Orlando, FL 32821','921 La Terraza Lane, Kissimmee, FL 34744','34038 Fortunado Street, Sorrento, FL 32776','113 Red Bay Drive, Longwood, FL 32779','1011 Lascala Drive, Windermere, FL 34786','811 Brandy Oaks Loop, Winter Garden, FL 34787','1457 Resolute Street, Kissimmee, FL 34747','9605 Buoy Court, Orlando, FL 32817','14080 Bradbury Road, Orlando, FL 32828','6357 Sedona Leaf Court, Windermere, FL 34786','13817 Eagles Glen Court, Orlando, FL 32837','4719 Fort Knox Court, Orlando, FL 32822','321 Giovani Boulevard, Clermont, FL 34715','1772 Leather Fern Drive, Ocoee, FL 34761','4070 La Salle Avenue, Saint Cloud, FL 34772','532 Wekiva Cove Road, Longwood, FL 32779','7631 Redwood Country Road, Orlando, FL 32835','501 Rebecca Drive, Saint Cloud, FL 34769','13338 Phoenix Drive, Orlando, FL 32828','7050 Buckhorn Trail, Saint Cloud, FL 34771','1119 Lake Legro Court, Orlando, FL 32835','4747 Cains Wren Trail, Sanford, FL 32771','1014 FAIRCLOTH CT, Oviedo, FL 32765','3137 HOLLAND DR, Orlando, FL 32825','2521 TEAK COURT, Kissimmee, FL 34743','754 Woodside Road, Maitland, FL 32751','954 LONGWOOD CLUB PLACE, Longwood, FL 32750','1923 SEATONHAM STREET, Winter Garden, FL 34787','1040 ROYAL MARQUIS CIRCLE, Ocoee, FL 34761','3456 BUFFAM PLACE, Casselberry, FL 32707','1342 JECENIA BLOSSOM DRIVE, Apopka, FL 32712','559 HORSEMAN DRIVE, Oviedo, FL 32765','4671 CYPRESS RIVER ROAD, Saint Cloud, FL 34772','1825 MORGANS MILL CIRCLE, Orlando, FL 32825','23503 VALDERAMA LANE, Sorrento, FL 32776','13511 TETHERLINE TRL, Orlando, FL 32837','11845 SHELTERING PINE DRIVE, Orlando, FL 32836','2100 RICKOVER PLACE, Winter Garden, FL 34787','2306 TREYMORE DRIVE, Orlando, FL 32825','6559 LAKE PEMBROKE PLACE, Orlando, FL 32829','1937 DERBY GLEN DRIVE, Orlando, FL 32837','482 ORIONVISTA WAY, Oakland, FL 34787','13122 MOSS PARK RIDGE DRIVE, Orlando, FL 32832','1412 FOXFORREST CIRCLE, Apopka, FL 32712','550 LYNN STREET, Oviedo, FL 32765','10500 EASTPARK WOODS DRIVE, Orlando, FL 32832','1187 LAKE FRANCIS DRIVE, Apopka, FL 32712','1430 COLUSO DRIVE, Winter Garden, FL 34787','902 MOSSHART LANE, Orlando, FL 32825']
      # sts = ['100 CLOVERDALE CT, Hendersonville, Tn 37075','1005 Matheus Drive, Murfreesboro, Tn 37128','1006 Sky Valley Trail, Smyrna, Tn 37167','1007 Lassiter Drive, Goodlettsville, Tn 37072','1012 Valley Dr, Goodlettsville, Tn 37072','1013 ARCADIA COURT, ANTIOCH, Tn 37013','1017 Treasure Reef, Madison, Tn 37115','1018 Cornwall Court, Murfreesboro, Tn ','102 Grapevine Road, Hendersonville, Tn 37075','102 Marcus Circle, Murfreesboro, Tn 37130','102 Skylark Drive, Murfreesboro, Tn ','1024 Hammack Court, Nashville, Tn 37214','103 Lisa Circle, Lebanon, Tn 37087','1036 Blue Mountain Lane, Antioch, Tn 37013','104 Susan Drive, Hendersonville, Tn ','1048 Campbell Ave, Gallatin, Tn 37066','105 Cataract Drive, Murfreesboro, Tn 37129','105 Elizer Court, Hendersonville, Tn 37075','105 Guinevere Court, Murfreesboro, Tn ','106 Four Season Dr, Murfreesboro, Tn 37129','107 Timberview Circle, LaVergne, Tn 37086','108 Crystal Court, Nashville, Tn ','108 Schooner Ct, Hermitage, Tn ','108 Sunset, Mount Juliet, Tn 37122','108 Woodcraft Court, Nashville, Tn ','109 Hunters Chase Drive, Smyrna, Tn 37167','109 Painter Drive, Antioch, Tn 37013','1095 Gray Bill Drive, Gallatin, Tn 37066','1104 Brittany Park Lane, Antioch, Tn 37013','1106 Gardendale Drive, Murfreesboro, Tn 37130','1107 Jerico Road, Murfreesboro, Tn 37130','1108 Woburn Way, Antioch, Tn 37013','1109 Geneil Lane, LaVergne, Tn 37086','111 Bentree Drive, Hendersonville, Tn 37075','111 Deer Ridge Lane, Hendersonville, Tn ','111 Sandhill Rd, LaVergne, Tn 37086','1110 Mary Evelyn Court, Nashville, Tn ','1111 Alandee, Nashville, Tn 37214','111111 DELETED, LaVergne, Tn 37086','1113 Shallowbrook Trl S, Antioch, Tn 37013','1114 McDonough Circle, Thompsons Station, Tn 37179','1115 Gardendale Dr, Murfreesboro, Tn 37130','112 Haney Court, Murfreesboro, Tn 37128','113 Lindy Murff Court, Antioch, Tn ','114 Scotch Street, Hendersonville, Tn 37075','115 Bradford Circle, Hendersonville, Tn 37075','115 Cartwright Parkway, Goodlettsville, Tn ','115 Summerlake Place, Hendersonville, Tn 37075','1175 Bothwell Place, Gallatin, Tn 37066','118 Ivanhoe Court, Murfreesboro, Tn 37127','1193 sioux terrace, Madison, Tn 37115','1200 Timber Valley Drive, Nashville, Tn ','1204 Lasalle Court, Smyrna, Tn ','1205 Timber Valley Drive, Nashville, Tn 37214','121 Lakeside Park Drive, Hendersonville, Tn 37075','121 WILLIAMS VALLEY COURT, MADISON, Tn ','121 Williams Valley Ct, Madison, Tn 37115','122 Stillhouse Road, Hendersonville, Tn ','1220 Alandee Street, Nashville, Tn ','1222 Westridge Court, Antioch, Tn 37013','1229 Langwood Court, Gallatin, Tn 37066','1233 Shannon Ln, LaVergne, Tn 37086','124 Trace Drive, Goodlettsville, Tn 37072','125 Matheus Court, Murfreesboro, Tn ','1264 Dalmally Dr, Murfreesboro, Tn 37128','130 Agee Circle East, Hendersonville, Tn 37075','1303 Amberwood Circle, Murfreesboro, Tn ','1304 Springwood Ct, LaVergne, Tn 37086','1306 Saint hubbins drive, LAVERGNE, Tn 37086','131 Two Valley Road, Hendersonville, Tn 37075','1313 Rainglen Cove, Antioch, Tn 37013','1317 Bundy Drive, Smyrna, Tn 37167','1318 Georgetown, Old Hickory, Tn 37138','134 Ben Allen Rd, Nashville, Tn 37207','1340 Cutoff Rd, Murfreesboro, Tn 37129','1342 Nicole Ln, LaVergne, Tn 37086','1345 Shearron Court, Murfreesboro, Tn 37130','136 Dennis Rd, Hendersonville, Tn ','1401 Woodchimes Ct, Hermitage, Tn 37076','1402 Ardee, LaVergne, Tn 37086','143 Lane Drive, Mount Juliet, Tn 37122','1433 Middleborough Court, Murfreesboro, Tn 37130','1436 Pawnee Trail, Madison, Tn 37115','1438 Middleborough Court, Murfreesboro, Tn 37130','1438 Middleborough Ct, Murfreesboro, Tn 37130','1440 Middleborough Court, Murfreesboro, Tn 37130','146 Hickory Heights Drive, Hendersonville, Tn 37075','1505 Elm Run Court, Nashville, Tn ','1505 Heritage View Boulevard, Madison, Tn ','1507 CARDINAL LANE, Mount Juliet, Tn ','1512 Brookchase Court, Antioch, Tn 37013','1523 Journey Drive, Murfreesboro, Tn 37130','1533 Passage Drive, Murfreesboro, Tn 37130','1542 River Rock Boulevard, Murfreesboro, Tn ','1550 James Gafford Drive, LaVergne, Tn 37086','1570 Ridgemont Drive, LaVergne, Tn 37086','1615 Pacer Ct, Spring Hill, Tn 37174','1619 Allston Drive, Murfreesboro, Tn 37128','163 Lakeside Park Drive, Hendersonville, Tn 37075','1637 O Green Cv, Murfreesboro, Tn 37129','1701 Pine Crest Court, Mount Juliet, Tn 37122','1704 Carrington Court, Old Hickory, Tn 37138','1712 Jimmy Cove, LaVergne, Tn 37086','1728 Westerwald Drive, Murfreesboro, Tn 37127','1736 Londonview Place, Antioch, Tn 37013','1742 Freiburg Drive, Spring Hill, Tn 37174','1761 Heritage Glen Dr, Madison, Tn 37115','1772 Londonview Place, Antioch, Tn 37013','1805 Lakewood Village Drive, Antioch, Tn ','1821 Baslia Lane, Spring Hill, Tn 37174','1825 Annalee Drive, Antioch, Tn 37013','1834 Turnstone Court, Murfreesboro, Tn ','185 Center St, LaVergne, Tn 37086','1877 Brookmeadow Ln, Hermitage, Tn 37076','1880 Portway Rd, Spring Hill, Tn 37174','1903 Sean Cove, LaVergne, Tn ','1906 Venada Trail, Murfreesboro, Tn 37128','1913 Deep Woods Trail, Nashville, Tn 37214','1999 Waterford Dr, Old Hickory, Tn 37138','2003 Eagle Ct, Spring Hill, Tn 37174','2009 Remington Park Road, Hermitage, Tn 37076','202 Berrywood Drive, Smyrna, Tn 37167','202 Bluewater Court, LaVergne, Tn ','202 Fairfield Drive, Smyrna, Tn 37167','2022 Gardendale Drive, Murfreesboro, Tn 37130','203 Sierra Drive, Murfreesboro, Tn 37129','2031 Gardendale Drive, Murfreesboro, Tn ','204 IVY DRIVE, HENDERSONVILLE, Tn 37075','2041 Williams Valley Drive, Madison, Tn ','205 Mercury Drive, LaVergne, Tn 37086','205 Turtle Rock, Murfreesboro, Tn 37129','208 Orchard Valley Drive, Smyrna, Tn ','211 Deerlake Court, LaVergne, Tn 37086','2112 Caladonia Way, Smyrna, Tn 37167','212 Hedgeway, Gallatin, Tn 37066','2127 Ponty Pool Drive, Mount Juliet, Tn 37122','2157 Aberdeen, Murfreesboro, Tn 37130','216 Broadmoor Drive, Nashville, Tn 37207','2164 Spring Hill Circle, Spring Hill, Tn 37174','219 Cherry Hill Drive, Hendersonville, Tn 37075','2210 Burr Court, Murfreesboro, Tn 37129','223 Oak Point Ln, Mount Juliet, Tn ','2235 Brick Way, Murfreesboro, Tn 37130','225 Loxley Lane, Murfreesboro, Tn 37127','227 Shrewsbury Dr, Murfreesboro, Tn 37129','2283 Nonaville Rd, Mount Juliet, Tn 37122','229 LAKE FOREST DRIVE, LAVERGNE, Tn 37086','229 Slayton Drive, Madison, Tn 37115','2333 Cooper Terrace, Nashville, Tn 37216','2349 Hockett Drive, Nashville, Tn 37218','2420 S Bellah Court, Murfreesboro, Tn ','2427 Grover Street, Nashville, Tn 37207','246 Sunny Acre Drive, Mount Juliet, Tn ','2513 Gold Valley Drive, Murfreesboro, Tn 37130','2527 Lone Oak Circle, Murfreesboro, Tn 37128','2543 Medford Campbell Boulevard, Murfreesboro, Tn 37130','2550 Bethwood Drive, Nashville, Tn ','2600 Ravine Drive, Nashville, Tn 37217','2601 Mesa Dr, Nashville, Tn 37217','2623 Apostle Lane, Murfreesboro, Tn ','2688 Pine Ridge Rd, Nashville, Tn ','2704 Old Smith Springs Road, Nashville, Tn 37217','2711 Oxford Hall Ave, Murfreesboro, Tn 37128','2731 Roscommon Dr, Murfreesboro, Tn 37128','2740 Welshcrest Dr, Antioch, Tn 37013','2743 Mollys Court, Spring Hill, Tn ','2748 Windwalker Court, Murfreesboro, Tn 37128','2751 Oxford Hall Ave, Murfreesboro, Tn 37128','2808 Candlewicke Drive, Spring Hill, Tn ','2815 Creekbend Dr, Nashville, Tn 37207','2816 Penn Meade Drive, Nashville, Tn ','2818 Rachel Lane, Thompson Station, Tn 37179','2840 Cochran Trace Dr, Spring Hill, Tn 37174','2849 Paddle Wheel Drive, Nashville, Tn 37214','2864 Meadow Glen, Mount Juliet, Tn 37122','290 Indian Park Dr, Murfreesboro, Tn ','290 Old Nashville Hwy, La Vergne, Tn ','2903 WESTCHESTER COURT, Old Hickory, Tn 37138','2904 Torrence Trl, Spring Hill, Tn 37174','2907 Checkers Court, Spring Hill, Tn 37174','2914 Kedzie Drive, Murfreesboro, Tn 37130','2921 Katonka Ct, Antioch, Tn 37013','2941 Windemere Drive, Murfreesboro, Tn ','2982 Bridle Dr, Murfreesboro, Tn 37129','301 Canberra Ct, Hermitage, Tn 37076','3021 Holsted Drive, Murfreesboro, Tn 37128','3022 Barfield Road, Murfreesboro, Tn 37128','3022 Roellen Road, Murfreesboro, Tn ','3027 Cliffside Dr, Christiana, Tn 37037','3036 Ewngdale Drive, Nashville, Tn ','3036 PENN MEADE WAY, Nashville, Tn 37214','304 Sherry Cove, LaVergne, Tn ','3042 Weybridge Drive, Murfreesboro, Tn ','305 Clearlake Drive, LaVergne, Tn ','305 Country Village Dr, Smyrna, Tn 37167','305 Short Court, Smyrna, Tn 37167','3063 ACE WINTERMEYER DR., LaVergne, Tn 37086','307 Liberty Lane, LaVergne, Tn 37086','3073 Reelfoot Drive, Nashville, Tn 37214','308 Griffin Place, Hermitage, Tn 37076','308 Swanridge Court, Smyrna, Tn ','3105 Kennebeck Place, Antioch, Tn 37013','3107 Priest Woods Dr, Nashville, Tn ','3109 Windsail Trail, Antioch, Tn 37013','3121 Barksdale Harbor Drive, Nashville, Tn 37214','3142 E Lake Drive, Nashville, Tn 37214','317 Cedar Park Circle, LaVergne, Tn ','3213 Crowe Drive, Nashville, Tn ','323 Donna Drive, Hendersonville, Tn 37075','3255 Priest Woods Dr, Nashville, Tn 37214','327 Merlin Dr, Murfreesboro, Tn 37127','3273 Anderson Road, Antioch, Tn ','329 Stewart Springs Drive, Smyrna, Tn 37167','3300 Towne Village Road, Antioch, Tn 37013','3305 Penn Meade Way, Nashville, Tn ','3308 Towne Village Road, Antioch, Tn 37013','3340 Fletz Circle, Murfreesboro, Tn ','3348 Calais Circle, Antioch, Tn ','3440 White Pine Drive, Nashville, Tn 37214','3444 Daisy Trail, Antioch, Tn ','3505 Cobble St, Nashville, Tn 37211','3512 Roundwood Forest Drive, Antioch, Tn 37013','3524 Roundwood Forest Dr, Antioch, Tn 37013','355 Spring Valley Drive, Mount Juliet, Tn 37122','359 Clearlake Drive, La Vergne, Tn 37086','3605 Jerry Anderson Drive, Murfreesboro, Tn 37128','3717 Willow Creek Road, Nashville, Tn ','3874 Nestledown Drive, Murfreesboro, Tn 37129','400 Silver Springs Lane, Mount Juliet, Tn 37122','4007 Cadence Drive, Spring Hill, Tn ','4009 Affirmed Drive, Mount Juliet, Tn ','4009 Cadence Dr, Spring Hill, Tn 37174','401 Breckinridge Dr, Smyrna, Tn 37167','401 Sunset Dr, Mount Juliet, Tn 37122','4016 Deer Run Trace, Spring Hill, Tn 37174','402 Newbary Court, Franklin, Tn 37069','403 Indian Hills Ct, Gallatin, Tn 37066','403 SILVER SPRINGS LANE, Mount Juliet, Tn 37122','4037 Moss Rd, Antioch, Tn 37013','404 Woodsman Court, Nashville, Tn 37214','4044 LANCASHIRE DRIVE, Antioch, Tn ','405 Wooded Valley Ct, LaVergne, Tn 37086','4050 Lake Parkway, Hermitage, Tn 37076','407 Shadetree Ct, Nashville, Tn 37207','408 Lake Forest Drive, LaVergne, Tn ','411 SUNSET DRIVE, Mount Juliet, Tn 37122','421 Scotts Creek Trail, Hermitage, Tn 37076','4232 Chesney Glen Drive, Hermitage, Tn ','4233 Sandstone Drive, Antioch, Tn 37013','424 Dorchester Pl, Gallatin, Tn 37066','425 Whispering Drive, Spring Hill, Tn 37174','426 Tampa Drive, Nashville, Tn 37211','4273 Rachel Donelson Pass, Hermitage, Tn ','4300 Baton Rouge Drive, Hermitage, Tn 37076','4409 SANDPIPER LANE, ANTIOCH, Tn ','4424 Lavergne Couchville Pike, Antioch, Tn 37013','4425 Frost Street, Nashville, Tn 37214','444 Jessie Drive, Nashville, Tn ','4516 Hunters Green Cir, Antioch, Tn 37013','4517 Highland Ridge Drive, Antioch, Tn ','4549 Raccoon Trail, Hermitage, Tn ','4587 Artelia Drive, Antioch, Tn 37013','4620 Raccoon Trail, Hermitage, Tn 37076','468 Cedar Park Circle, LaVergne, Tn 37086','4717 Phoenix Dr, Hermitage, Tn ','4728 Greystone St, Antioch, Tn 37013','4728 Greystone Street, Antioch, Tn 37013','4729 Ashley Way, Hermitage, Tn 37076','480 Avery Valley Drive, Smyrna, Tn 37167','4816 Cape Hope Pass, Hermitage, Tn 37076','4824 Leesa Ann Lane, Hermitage, Tn 37076','4920 Oak Barrel Dr, Antioch, Tn 37013','4932 Alexis Dr, Antioch, Tn 37013','4951 Laura Jeanne Blvd, Murfreesboro, Tn 37129','4953 Laura Jeanne Bolvard, Murfreesboro, Tn 37129','501 Chinquapin Pl, Smyrna, Tn 37167','5026 Nina Marie Avenue, Murfreesboro, Tn 37129','505 Connie Ct, Smyrna, Tn ','505 Suzanne Court, Mount Juliet, Tn ','509 Dutchmans Drive, Hermitage, Tn ','5104 Patience Drive, Smyrna, Tn 37167','5104 Roxborough Drive, Hermitage, Tn 37076','5130 Jerickia Court, Murfreesboro, Tn 37129','5139 Hunters Point Ln, Hermitage, Tn 37076','5169 Preserve Blvd, Antioch, Tn 37013','520 RHODES CIRCLE, Gallatin, Tn 37066','521 HIGHLAND VIEW DR., Hermitage, Tn 37076','529 Jasmine Drive, Nashville, Tn 37211','603 N. Greenwood EXT, Lebanon, Tn 37087','604 Lake Terrace Drive, Nashville, Tn 37217','604 Whirlaway Drive, Antioch, Tn 37013','608 Blake Moore Drive, LaVergne, Tn 37086','608 Cayla Court, Smyrna, Tn ','608 Hidden Hill Dr, Hermitage, Tn 37076','609 CREEKFRONT DRIVE, Mount Juliet, Tn ','6121 Terry Drive, Nashville, Tn 37209','613 Denver Dr, Hermitage, Tn 37076','614 Federal Court, Murfreesboro, Tn 37129','6200 Tuckaleechee Lane, Antioch, Tn 37013','6204 Rocky Top Drive, Antioch, Tn 37013','628 Meadowview Dr, Mount Juliet, Tn ','632 Granwood Blvd, Old Hickory, Tn 37138','6441 Kari Drive, Murfreesboro, Tn ','6507 Forrest Lane, Murfreesboro, Tn ','6559 Kari Drive, Murfreesboro, Tn 37129','6736 Forrest Lane, Murfreesboro, Tn 37129','690 Holland Ridge Drive, LaVergne, Tn 37086','6980 Calderwood Drive, Antioch, Tn 37013','701 Clear Circle, Smyrna, Tn ','704 Alisa Cove, LaVergne, Tn ','705 Meade Drive, Spring Hill, Tn 37174','706 Judge Mason Way, LaVergne, Tn 37086','709 Pinwheel Court, Antioch, Tn ','7096 S Hampton Boulevard, Antioch, Tn 37013','7101 S Hampton Blvd, Antioch, Tn ','711 Kennington North, Nashville, Tn ','7152 Legacy Dr, Antioch, Tn 37013','716 William Howard Place, Nashville, Tn 37209','721 Woodcraft Drive, Nashville, Tn 37214','731 Hogan Drive, Murfreesboro, Tn 37128','7316 Ole Nottingham Drive, Antioch, Tn 37013','733 Woodcraft, Nashville, Tn 37214','738 Middleton Lane, Murfreesboro, Tn ','7481 Maggie Dr, Cane Ridge, Tn 37013','7545 W Winchester Drive, Antioch, Tn ','779 Curd Road, Mount Juliet, Tn 37122','801 Lake Terrace, Nashville, Tn ','805 Pebble Beach Circle, Mount Juliet, Tn 37122','814 General Forrest Drive, Smyrna, Tn 37167','817 Chilhowee Court, Smyrna, Tn 37167','8192 RAMSTONE WAY, ANTIOCH, Tn 37013','820 Netherlands Drive, Hermitage, Tn ','820 Stone Flower Ct, Antioch, Tn 37013','823 Buckhaven Drive, Smyrna, Tn 37167','823 Netherlands Drive, Hermitage, Tn ','825 Stoner Mill Lane, Hermitage, Tn 37076','8276 Ramstone Way, Antioch, Tn ','839 Netherlands Dr, Hermitage, Tn ','839 Netherlands Drive, Hermitage, Tn ','8604 Wayne St, Murfreesboro, Tn 37129','894 Lavergne Lane, LaVergne, Tn 37086','902 Bellwood Dr, Mount Juliet, Tn 37122','907 Lance King Court, LaVergne, Tn ','910 Emmett Ave, Nashville, Tn 37206','911 Tapoco Ct, Smyrna, Tn 37167','913 Tom Hailey Boulevard, LaVergne, Tn ','916 N 2ND Street, Nashville, Tn 37207','943 Strand Fleet Drive, Antioch, Tn 37013','969 Norwalk Dr, Nashville, Tn 37214','03 Cedar Bend Lane, LaVergne, Tn 37086','104 Camas Ct, Christiana, Tn 37037','109 Louann Lane, Hendersonville, Tn ','1090 Williamson Road, Goodlettsville, Tn 37072','1102 Williamson Road, Goodlettsville, Tn 37072','113 New Haven Court, Nashville, Tn 37214','127 Riverwood Dr, LaVergne, Tn 37086','134 Hillsdale Drive, Hendersonville, Tn 37075','138 Keenan Ln, Mount Juliet, Tn 37122','1411 Ashlawn Dr, Murfreesboro, Tn 37128','1412 Chutney Court, Antioch, Tn 37013','1517 Rochester Drive, Murfreesboro, Tn 37130','1547 John Galt Drive, Lebanon, Tn 37087','1605 Ogletree Court, La Vergne, Tn 37086','1724 Greenhard Court, ANTIOCH, Tn 37013','1740 Woodland Pointe Drive, Nashville, Tn 37214','1834 Turnstone Ct, Murfreesboro, Tn 37128','1996 Waterford Dr, Old Hickory, Tn 37138','202 Sycamore Drive, Smyrna, Tn ','207 Taylor Drive, LaVergne, Tn ','2105 Argo Place, Nashville, Tn 37211','217 S Valley Road, Hendersonville, Tn ','2204 Crescent Valley Lane, Hermitage, Tn 37076','225 Ash Drive, Franklin, Tn 37064','2331 Chandler Pl, Murfreesboro, Tn 37130','249 Walnut Grove Road, Christiana, Tn 37037','2600 Sailboat Court, Nashville, Tn 37217','2728 Aston Woods Lane, Thompsons Station, Tn ','30 Jay Street, Nashville, Tn 37210','3009 Sunnyview Drive, Nashville, Tn 37207','3046 High Rigger Drive, Nashville, Tn 37217','3152 Kinwood Dr, Antioch, Tn 37013','3269 Luann Drive, Antioch, Tn 37013','3308 E Lake Drive, Nashville, Tn 37214','3343 Oak Trees Court, Antioch, Tn ','3462 Clegg Drive, Spring Hill, Tn 37174','3540 Anderson Road, Nashville, Tn 37217','3657 Rutherford Drive, Spring Hill, Tn 37174','3858 Priest Lake Drive, Nashville, Tn 37217','4752 Reischa Drive, Antioch, Tn 37013','483 Belinda Parkway, Mount Juliet, Tn 37122','4865 Indian Summer Drive, Nashville, Tn 37207','509 Rockwood Drive, Hermitage, Tn 37076','512 Allen Drive, Franklin, Tn 37064','6044 Panama Drive, Hermitage, Tn 37076','6301 Watsonwood Court, Murfreesboro, Tn 37129','7117 Lone Eagle Court, Murfreesboro, Tn 37128','7434 Powells Chapel Road, Murfreesboro, Tn ','7500 W Winchester Drive, Antioch, Tn ','808 Clearspring Ct, Smyrna, Tn 37167','912 E Cynthia Trail, Goodlettsville, Tn 37072','912 Hammack Drive, Nashville, Tn 37214','923 Strand Fleet Drive, Antioch, Tn 37013','926 B N 6Th Street, Nashville, Tn 37207','7208 Autumn Crossing Way, Brentwood, Tn 37027','106 Sorrel Court, Hendersonville, Tn 37075','259 Keswick Grove Lane, FRANKLIN, Tn 37067','6391 Fischer Court, Brentwood, Tn 37027','105 Harkness Court, Smyrna, Tn 37167','1108 Poplar Hollow Rd., La Vergne, Tn 37086','128 Windward Drive, Mount Juliet, Tn 37122','421 Coles Ferry Road, Gallatin, Tn 37066','4873 Everest Drive, Old Hickory, Tn 37138','118 Cloverdale Court, Hendersonville, Tn 37075','1800 Cottage Grove Way, Antioch, Tn 37013','239 Indian Park Drive, Murfreesboro, Tn 37128','3107 Wellington Place, Murfreesboro, Tn 37128','391 Tapestry Place Unit 101, Gallatin, Tn 37066','5320 Skip Jack Drive, Antioch, Tn 37013','917 Crystal Bear Trail, Murfreesboro, Tn 37218','1009 Stonemark Trail, LaVergne, Tn 37086','110 Meadow Street, Old Hickory, Tn 37138','1171 Osborne Lane, Murfreesboro, Tn 37130','1408 Ardee Drive, LaVergne, Tn 37086','1413 Wakonda Court, Murfreesboro, Tn 37130','1434 Tuffnell Drive, LaVergne, Tn 37086','1621 Rice Hill Court, Antioch, Tn 37013','184 Center Street, LaVergne, Tn 37086','2005 Lassiter Drive, Goodlettsville, Tn 37072','2130 Spring Hill Circle, Spring Hill, Tn 37174','233 Lake Forest Drive, LaVergne, Tn 37086','2406 Hillmont Drive, Murfreesboro, Tn 37129','243 Lake Forest Drive, LaVergne, Tn 37086','3621 Arcadia Circle, Antioch, Tn 37013','4277 Valley Grove Drive, Hermitage, Tn 37076','428 Brownstone Street, Old Hickory, Tn 37138','431 Nyu Place, Murfreesboro, Tn 37128','432 Cedar Park Circle, LaVergne, Tn 37086','434 Cedar Park Circle, LaVergne, Tn 37086','4913 Alexis Drive, Cane Ridge, Tn 37013','510 Debbie Drive, Hermitage, Tn 37076','515 Cedar Park Circle, LaVergne, Tn 37086','637 Wildwood Drive, Smyrna, Tn 37167','7015 Westfork Drive, Smyrna, Tn 37167','7815 Rainey Drive, Antioch, Tn 37013','805 Park Court South, LaVergne, Tn 37086','912 Wildrose Court, Smyrna, Tn 37167','1015 Compton Road, Murfreesboro, Tn 37130','108 Butleigh Court, Goodlettsville, Tn 37072','1115 Fitzpatrick Road, Nashville, Tn 37214','113 Elissa Drive, Hendersonville, Tn 37075','121 Lee Court, Gallatin, Tn 37066','154 Dreville Drive, LaVergne, Tn 37086','167 Timberlake Drive, Hendersonville, Tn 37075','1894 Brookmeadow Lane, Hermitage, Tn 37076','2017 Abingdon Drive, LaVergne, Tn 37086','2283 Joann Drive, Spring Hill, Tn 37174','250 Lake Forest Drive, LaVergne, Tn 37086','2610 Foxfire Court, Murfreesboro, Tn 37130','3016 Towne Valley Road, Antioch, Tn ','3208 Country Meadow Road, Antioch, Tn 37013','415 Hill Road, Nashville, Tn 37220','4837 Terragon Trail, Antioch, Tn 37013','501 Aerie Landing, Antioch, Tn 37013','520 Bellmore Place, Nashville, Tn 37209','664 Holland Ridge Drive, LaVergne, Tn 37086','1540 Mount Mitchell Court, Antioch, Tn 37013','2809 PADDLE WHEEL DR., NASHVILLE, Tn 37214','8 CHARLESMEADE DR, JACKSON, Tn 38305','1849 Pallas Drive, Madison, Tn 37115','2920 Pipkin Hills Drive, Spring Hill, Tn 37174','2972 Kinwood Drive, Antioch, Tn 37013','337 Clearlake Drive, LaVergne, Tn ','110 Long Rifle Road, Smyrna, Tn 37167','118 SUNSET CIRCLE, Mount Juliet, Tn 37122','1217 Eastwoods Drive, Murfreesboro, Tn 37130','1311 Teakwood Court, Murfreesboro, Tn ','1424 Timber Valley Drive, Nashville, Tn 37214','1708 Realtree Way, Antioch, Tn 37013','3429 Towneship Road, Antioch, Tn 37013','604 Dusk Court, Nashville, Tn 37221','641 Belgium Drive, Hermitage, Tn 37076','1006 Seven Oaks Blvd, Smyrna, Tn 37167','1007 Cheryl Lane, LaVergne, Tn 37086','1017 Rolling Meadow Drive, Mount Juliet, Tn ','102 Wortham Court, LaVergne, Tn ','106 Cavalier Drive, Hendersonville, Tn 37075','1080 Blue Mountain Lane, Antioch, Tn 37013','1110 Mahogany Trail, Murfreesboro, Tn 37130','1112 Shawnee Trace, Madison, Tn ','1115 Gardendale Drive, Murfreesboro, Tn 37130','1135 Mahogany Trail, Murfreesboro, Tn ','1136 Eastwoods Drive, Murfreesboro, Tn ','1144 Brittany Park Ln, Antioch, Tn 37013','116 Firelight Court, Antioch, Tn 37013','117 Forest Lane, LaVergne, Tn 37086','1209 Elisha Court, LaVergne, Tn 37086','1232 Jacksons Hill Road, Hermitage, Tn ','1276 Saint Andrews Drive, Murfreesboro, Tn ','1283 Tiree Dr, Murfreesboro, Tn 37128','131 Agee Cir E, Hendersonville, Tn 37075','1329 Tonya Drive, LaVergne, Tn 37086','1445 Westview Drive, Murfreesboro, Tn 37128','1508 JAMES GAFFORD DRIVE, LAVERGNE, Tn 37086','1519 Mohawk Trail, Madison, Tn 37115','1520 Joe Pyron, Madison, Tn 37115','158 Homestead Place, Hendersonville, Tn 37075','1615 Bill Eller Drive, LaVergne, Tn 37086','1624 Saint Andrews Drive, Murfreesboro, Tn ','1645 Comanche Run, Madison, Tn 37115','1734 Lucille Lane, Murfreesboro, Tn 37129','1805 Rory Cove, LaVergne, Tn 37086','1816 Hickory Lawn Court, Antioch, Tn 37013','1844 Cottage Grove Way, Antioch, Tn 37013','1917 Portview Drive, Spring Hill, Tn ','224 Mary Joe Martin Drive, LaVergne, Tn 37086','227 Meigs Drive, Murfreesboro, Tn 37128','230 Quiet Lane, LaVergne, Tn 37086','2322 Briar Bend Dr, Murfreesboro, Tn 37128','2414 Hillmont Drive, Murfreesboro, Tn 37129','2472 Saddlewood Ct, Murfreesboro, Tn 37128','2507 Maybrook Court, Murfreesboro, Tn ','2607 Edge O Lake Drive, Nashville, Tn 37217','2624 Gold Valley Drive, Murfreesboro, Tn 37130','2709 Calais Ct, Murfreesboro, Tn 37127','2719 Rocking Horse Lane, Murfreesboro, Tn 37130','279 Indian Park Drive, Murfreesboro, Tn 37128','2804 Halifax Ct, Old Hickory, Tn 37138','2812 Pinnacle Court, Spring Hill, Tn ','2813 Hydes Ferry Rd, Nashville, Tn 37218','3006 Henderson Lane, Murfreesboro, Tn 37130','3022 Tuckers Place, LaVergne, Tn ','303 Hunters Chase Drive, Smyrna, Tn ','304 University Drive, Gallatin, Tn 37066','3041 Delta Queen Dr, Nashville, Tn 37214','322 Elizabeth Ct, Smyrna, Tn 37167','3408 Hamberton Circle, Murfreesboro, Tn 37128','3501 Seasons Drive, Antioch, Tn 37013','3512 Grace Falls Dr, Antioch, Tn 37013','3514 Whitebud Lane, Murfreesboro, Tn 37128','4009 Pepperwood Drive, Antioch, Tn ','401 Wildwood Dr, Smyrna, Tn 37167','402 Rodney Street, Gallatin, Tn 37066','4112 Pepperwood Drive, Antioch, Tn 37013','417 Belinda Parkway, Mount Juliet, Tn 37122','4530 Baton Rouge Drive, Hermitage, Tn ','504 Fairhaven Court, Nashville, Tn ','5067 Boyd Drive, Mufreesboro, Tn 37129','508 Summit Way, Mount Juliet, Tn 37122','5188 Preserve Boulevard, Antioch, Tn 37013','5201 Alyadar Drive, Hermitage, Tn ','529 Summit Way, Mount Juliet, Tn 37122','616 Aideen Place, Madison, Tn 37115','622 McKean Drive, Smyrna, Tn ','627 Walton Ferry Road, Hendersonville, Tn 37075','652 Rocky Mountain Parkway, Antioch, Tn 37013','701 Lake Terrace Drive, Nashville, Tn 37217','709 Winchester Pl, Antioch, Tn 37013','720 Stewart Valley Drive, Smyrna, Tn ','781 Woodcrest Court, LaVergne, Tn 37086','800 Southwoods Drive, Smyrna, Tn 37167','805 Troy Cove, LaVergne, Tn 37086','8328 TRADING POST COURT, NASHVILLE, Tn 37221','921 Birchmill Point, Antioch, Tn 37013','150 Elm St, LaVergne, Tn 37086','1604 Timber Run, Nashville, Tn 37214','204 Redondo Court North, Hendersonville, Tn 37075','2383 Paddock Drive, Murfreesboro, Tn 37128','2712 Sewanee Place, Murfreesboro, Tn 37128','5072 English Village Drive, Nashville, Tn 37211','2007 Golden Court, Spring Hill, Tn 37174','2815 Maple Circle, Thompsons Station, Tn 37179','102 Raspberry Lane, Smyrna, Tn ','103 Peartree Drive, Hendersonville, Tn 37075','104 Valley View Court, Hendersonville, Tn 37075','107 Happy Street, Lebanon, Tn ','108 Jesse Brown Drive, Goodlettsville, Tn 37072','109 Natchez Court S, LaVergne, Tn ','109 Smith Springs Court, Nashville, Tn 37217','110 Longview Drive, Goodlettsville, Tn 37072','1106 Pyburns Place, LaVergne, Tn ','1120 Richard Lee Circle, Hermitage, Tn 37076','1155 Flat Stone Drive, Gallatin, Tn 37066','1168 Cynthia Lane, 37174, Tn 37174','1175 Fitzpatrick Road, Nashville, Tn 37214','118 Camino Circle, Hendersonville, Tn 37075','1205 Red Oak Court, Murfreesboro, Tn 37130','126 Edgewood Drive, Hendersonville, Tn 37075','1276 Blairfield Drive, Antioch, Tn 37013','1311 Owasa Trail, Murfreesboro, Tn 37130','132 Branford Drive, Smyrna, Tn 37167','132 Two Valley Rd, Hendersonville, Tn 37075','1406 Pagosa Court, Murfreesboro, Tn 37130','1421 Suffolk Court, LaVergne, Tn 37086','163 East Drive, Hendersonville, Tn 37075','1702 Erin Trace, LaVergne, Tn 37086','1718 Luton Drive, LaVergne, Tn 37086','1737 Stone Hollow Court, Hermitage, Tn ','174 Evergreen Circle, Hendersonville, Tn 37075','1838 Pecan Ridge Drive, Murfreesboro, Tn 37128','1913 Wax Wing Court, Mount Juliet, Tn 37122','2025 Barretts Ridge Drive, Murfreesboro, Tn ','2114 Erin Lane, Mount Juliet, Tn 37122','214 Brentmeade Drive, Murfreesboro, Tn 37130','217 Manatee Court, Antioch, Tn ','2227 Dewey Drive, Spring Hill, Tn 37174','2231 Dewey Drive, Spring Hill, Tn 37174','2306 Centertree Drive, Murfreesboro, Tn 37128','2328 Franco Road, Madison, Tn 37115','2602 Matchstick Place, Spring Hill, Tn ','2619 Apostle Lane, Murfreesboro, Tn 37129','2636 Dakota Way, Murfreesboro, Tn 37130','2688 Pine Ridge Road, Nashville, Tn 37207','279 Indian Park Drive, Murfreesboro, Tn 37128','2826 Castlerea Drive, Murfreesboro, Tn 37128','2860 Paddle Wheel Drive, Nashville, Tn 37214','2900 Wills Court, Spring Hill, Tn 37174','294 Townes Drive, Nashville, Tn 37211','2941 Bridle Drive, Murfreesboro, Tn ','3002 Burnt Pine Drive, Smyrna, Tn 37167','307 Davids Way, LaVergne, Tn ','3102 Barretts Ridge Drive, Murfreesboro, Tn ','3117 Kerrybrook Drive, Murfreesboro, Tn ','3120 Justin Towne Court, Antioch, Tn 37013','3128 Roundwood Forest Lane, Antioch, Tn 37013','3137 E Lake Drive, Nashville, Tn ','314 Sierra Drive, Murfreesboro, Tn 37129','3304 Monoco Drive, Spring Hill, Tn 37174','3518 Florence Road, Murfreesboro, Tn 37129','3846 Florence Road, Murfreesboro, Tn ','3877 Manson Pike, Murfreesboro, Tn 37129','3924 Pepperwood Drive, Antioch, Tn 37013','4025 Apache Trail, Antioch, Tn 37013','406 Anthony Branch Drive, Mount Juliet, Tn 37122','414 Rachels Court West, Hermitage, Tn ','422 Bonnawood, Hermitage, Tn 37076','4256 Chesney Glen Drive, Hermitage, Tn 37076','507 Morris Dr, Murfreesboro, Tn ','510 Gammon Lane, LaVergne, Tn ','511 Hidden Hill Drive, Hermitage, Tn 37076','523 MILESDALE LANE, SMYRNA, Tn 37167','619 Creekfront Drive, Mount Juliet, Tn 37122','636 Frankfort Drive, Hermitage, Tn 37076','639 Belinda Pkwy, Mount Juliet, Tn ','689 Whispering Breeze, Mount Juliet, Tn ','808 Trailside Circle, Antioch, Tn 37013','810 Holder Court, Nashville, Tn ','8309 Ramstone Way, Antioch, Tn ','9018 Nathanael Drive, LaVergne, Tn ','9018 Nevada Avenue, Smyrna, Tn 37167','534 Joe B Jackson Parkway, Murfreesboro, Tn 37127','538 Sandy Dr, Mount Juliet, Tn 37122','538 Summit Way, Mount Juliet, Tn ','540 Scotts Creek Trail, Hermitage, Tn 37076','5432 Village Way, Nashville, Tn ','5500 Escalade Drive, Mount Juliet, Tn 37122','5501 Fairhaven Drive, Nashville, Tn ','552 Highpoint Drive, Smyrna, Tn 37167','565 Mill Station Drive, Nashville, Tn 37207','576 Noel Drive, Mount Juliet, Tn 37122','5804 Foxview Drive, Antioch, Tn 37013','5906 Colchester Drive, Hermitage, Tn 37076','5913 New Hope Court, Hermitage, Tn 37076','5986 Monroe Crossing, Antioch, Tn 37013','601 Jean Street, Gallatin, Tn 37066']
      # sts = ['108 Schooner Ct, Hermitage','2527 Lone Oak, Murfreesboro, Tn 37128']
      ag = ['5986 Monroe Crossing']
      bg = ['Antioch']
      
      i = 0
      ag.each do |a|
        atokens = a.split(' ')
        address = [atokens[0],atokens[1]].join(' ')
        city = bg[i]
        
        p = Property.where(["market_id = 7 and category = ? AND street_address like ? AND city like ?", 'single_family', "#{address}%", "#{city}%"]).order("id DESC").first
        
        begin
          puts "#{address}*#{city}*#{p.try(:id)}*#{p.try(:mls_number)}*#{p.try(:status)}*#{p.try(:sale_price)}*#{p.try(:closed_date)}*#{p.try(:dom)}"
        rescue Exception => ex
          puts "#{address}*#{city}*#{p.try(:id)}*#{p.try(:mls_number)}*#{p.try(:status)}*#{p.try(:sale_price)}*#{p.try(:closed_date)}*#{p.try(:dom)}"
        end
        i += 1
      end
      
      
      
    rescue Exception => ex
      puts " error #{ex.message}"
    end
  end
  
  desc "adh"
  task :adh => :environment do |t, args|
    begin
      # sts = [2398771,4724199,4855088,4714086,1887527,5186833,4605088,4649613,5177835,2428555,4085007,1804867,4637661,4773955,4828063,5087732,4859678,4605081,5190452,4833028,4699449,5121277,4689655,4654106,5191458,1692985,5224726,4857420,4859563,5190096,5175166,5124830,5093651,4670553,5190085,4656668,2494336,4643804,4648895,2398304,4848287,5116217,4803094,4670654,4633907,4851526,2398751,5194559,4637362,4663609,5186812,5182628,5186784,5091685,4779920,5091938,4637217,5176089,1806001,4643527,1725471,4779360,5213919,5179709,5145690,4699131,4855963,4681057,4851527,4732767,4864583,5090451,5193516,4845170,4740341,4084973,5213987,5088989,4650991,5210231,2235012,5116258,4690762,5087587,5180762,4106420,2453898,4656895,5244413,4709352,5181052,5102402,4820843,4779352,2377462,5126208,2212475,5125115,5093151,2420696,4778687,5180825,1770366,4726078,4784084,1982509,4645723,4682587,2059950,4749395,5186698,4637022,4652382,4860252,4646816,4646828,5179676,5192408,4723041,5188961,5090417,5248846,5211707]

      # sts = [2398771,4724199,2222300,1887527,1828218,4649613,5151291,2428555,4085007,1804867,4637661,4773955,4828063,4691520,4859678,1766189,5190452,4833028,4632533,4654106,5191458,1692985,2226424,4857420,4859563,5175166,5093651,2343861,4778282,2409862,2494336,4643804,2194083,2398304,1682064,1758709,2230593,1682914,2205316,2204787,2398751,4652712,2531263,4604087,4778817,4712023,5091938,2494340,1806001,4643527,1725471,4634515,2409806,2391857,4855963,1750689,1682552,4732767,2311113,4826303,4107227,4740341,4084973,4719633,2351557,4650991,2235012,4672814,1720876,5087587,2441296,4106420,2453898,1796701,2295789,4709352,2316057,2377462,2259351,2212475,2259760,4106942,2420696,2402406,2156654,1770366,4784084,1982509,2328201,2180103,2059950,2213002,4148062,2212994]
#       
      # sts = [46040345,46052501,46058606,46075000,46279726,46320780,47661230,47664001,47664477,47678233,47680968,47990045,53568294,53753434,54447361,58318708,58338731,62027687,66184901,66184957,66186675,66664765,68112946,69319076,71025542,71064513,71066037,71069031,71070355,80764029,81477559,81482363,82073001,82073040,82099418,82106731,82430776,84638424,89871761,99686186,99691705,99704857,99714626,99720134,99753682,103131114,103422032]

      # sts = [2398771,4724199,2222300,1887527,1828218,4649613,5151291,2428555,4085007,1804867,4637661,4773955,4828063,4691520,4859678,1766189,5190452,4833028,4632533,4654106,5191458,1692985,2226424,4857420,4859563,5175166,5093651,2343861,4778282,2409862,2494336,4643804,2194083,2398304,1682064,1758709,2230593,1682914,2205316,2204787,2398751,4652712,2531263,4604087,4778817,4712023,5091938,2494340,1806001,4643527,1725471,4634515,2409806,2391857,4855963,1750689,1682552,4732767,2311113,4826303,4107227,4740341,4084973,4719633,2351557,4650991,2235012,4672814,1720876,5087587,2441296,4106420,2453898,1796701,2295789,4709352,2316057,2377462,2259351,2212475,2259760,4106942,2420696,2402406,2156654,1770366,4784084,1982509,2328201,2180103,2059950,2213002,4148062,2212994,2001422,4644399,2271600,5248846,5192408,2328216]

      sts = [5471891,5470658,5473920,5435474,5111434,5347492,5471827,4696407,5475511,5473877,4852949,5450252,4649360,5461171,5345647,5472250,5475481,5421363,4858864,2413286,4862754,2413287,1747167,5469755,4858862,1747170,4858863,4858859,2413282,2230879,4858865,5386804,4760520,2413285,5450016,5445089,5121169,5471402,5350592,5381067,5471386,5339173,5471209,5468234,5406380,5471440,5473026,5335768,5472775,4664095,5471367,5476812,5474407,5475860,5469943,5474127,5474161,5474002,5473828,5474172,5473940,5369755,5473755,5473985,4734093,5471880,5392235,5426604,5475837,5475173,5474321,5471047,5440582,5473514,5471904,5468466,5475468,5100318,5476800,5287734,5463523,5472164,5470447,5472827,5452065,5471901,5457178,5462051,5475432,5468347,5333315,5450574,5307803,5166277,5115600,5288424,4659741,5468789,4861681,5342900,4850342,4861601,5470355,4643118,4862925,4869721,4858590,5468398,5246565,5472060,5477052,5440775,5462236,5315979,5277228,5476602,5472171,5340303,5294259,5378805,5460304,5425310,5424241,5413579,5475018,5241799,5467660,5443931,5456114,4851671,5301805,5385551,5469473,5467657,5448970,5472029,5270962,5475708,5472702,5471154,5474309,5475826,4677720,5096566,5473594,5469030,5327506,5467476,5363641,5332633,5470730,5476555,5470426,5201530,5467908,5473214,5396487,5464472,5451794,5372026,5475113,5277274,5459963,5471822,5469196,5463321,5471309,5261475,5401844,4767748,5092999,5469567,5432870,5388735,5419234,4723007,2328229,5475370,5452482,5091075,5474150,5360092,5472105,5472499,5469212,5202044,4718275,5470205,5198670,5476318,5422547,5331228,5470865,5314324,5476263,5464997,5442868,5469310,5452623,5462038,5474673,5475963,5472299,5476073,5389995,5473172,5469059,5473305,5459737,5473770,5368779,5475202,5469496,5387882,5476931,5476312,5471956,5468467,5472214,5384476,5468081,5475986,5322724,5471027,5406431,5464979,5209494,5472577,5474457,5475627,5474994,5446719,5426110,5474396,5469190,5346585,5474319,5469925,5181771,5474064,5387426,5352268,5436564,5445501,5473568,5476873,5431258,5473319,5475103,5476409,5476724,5465498,5369745,1984931,5470576,5471785,5475939,5470055,5476568,5473164,5388797,5338388,4865713,5253150,5472962,5436768,5302060,5409292,5472566,5372435,5454183,5431183,5320537,5138633,5439761,5472359,5186886,5462407,5364637,5471093,5425906,5425142,5197566,5467756,5375193,5471997,5474463,5468256,1939898,5425157,5476053,5358153,5209550,5381936,5474843,5471057,5471866,5209917,5446440,5200487,5473703,5468495,5474774,5088081,5471262,5449903,5472248,5460839,5476735,5198857,5474483,5473852,5468565,5473815,4848011,5338674,5138138,5474210,5466661,5471707,5470884,5473628,5471327,5389987,5473363,2003234,5475564,5474292,5465615,5471157,5407739,5427650,5277281,5476178,5456251,5291229,5343572,5400727,5476146,5475101,5457517,5469893,5461106,5456079,4777679,5450087,5097718,5475290,5283963,5475267,5120864,5451899,5468212,5464359,5381201,5467913,5469528,5468824,5471978,5468199,5471514,5411389,5472828,5452506,5340009,5360025,5449407,5462189,2446892,5470031,5459201,5252115,5466394,5471115,5476065,5445466,5452691,5467830,5446084,5469700,5472280,5332698,5471189,5273760,5467839,5476269,4723585,5470044,5385430,5088030,5428031,5107669,5471806,5471841,5460750,5476355,5223526,5468523,5472431,5124818,4848470,2391786,5470787,5154966,5475318,2507414,2327646,5387745,5472873,5474949,5470878,2045260,5475028,5470368,5471730,5449384,5444562,5474760,1717180,5419315,5474345,5476840,5471840,5475242,5372429,5471184,5474804,5475100,5472032,5399941,5350591,5363698,5476254,5473182,5393935,5151543,5454451,5279150,2355359,5471692,5466166,5451777,5468241,1756571,4850125,2225958,5472444,5177899,5366992,1934740,5449755,5452619,5420260,5111516,5476979,5446672,5469098,5468914,5344678,5466867,5474929,5435744,5476280,5468286,5444815,5475971,5451188,5269802,5461210,5465056,5476096,5468180,5424591,5279700,5449338,5475700,5469892,5469640,5322556,5471272,5463062,5467552,5315991,5326836,5470299,5472663,5475920,5469356,5281427,2431805,5453335,5407707,5448859,5468233,5467810,5304630,5452249,5350765,5459691,5396699,5472288,5472846,5472389,2156979,5473365,5473353,5464289,5470376,2454287,5349548,5395591,5473825,5470288,5151179,5472696,5470401,5447844,5379284,5332395,5446066,5424665,5154954,5463893,5461216,4769412,5098011,5451804,5187194,5281374,5428472,5477063,2386611,5473264,4740930,5377881,5474081,1679736,4788686,5470556,5155556,5461694,5405579,5476031,5475720,5224294,5476704,5440797,5242771,5468171,5455097,5447771,2413552,5474218,5473994,5474846,5319230,5476993,5345783,5360280,5453173,5172096,5469141,5395582,5323642,5460522,5440793,5474933,5468721,5464705,5470011,5472564,5475567,5472728,5470757,5471703,5475354,5466655,5476129,4778504,5468172,4713247,5463075,5269797,5477002,5475983,5297898,5198679,4661795,5181736,2372964,5356630,5468229,5357505,5446689,5456188,5462263,5473478,5473631,4776534,5468442,5273750,5472664,5468607,2521648,5471453,5189850,5471401,5265097,5476163,5138398,5350594,5464970,5472400,5469916,2002740,5424960,5471451,1743536,5452650,5428532,5468203,5126061,4711468,5470596,5475443,5470826,5475241,5455016,5039937,4638024,4839442,5384526,5419586,5212355,5371990,5165376,5427745,5301825,5446518,5391284,5466805,5473759,5314880,5352267,5291452,5471265,5300417,5474576,5454959,5429282,5466335,5469975,5413584,5466445,5406696,5474288,2377912,4787910,5406533,5381156,5393902,5474752,5381494,5166170,5471411,5464121,2048484,5331465,5476448,5242030,5455945,5226589,5417629,5466517,5464853,5450082,5407812,5420257,5375317,5165429,5467782,1705456,5462866,5411308,4646087,5466151,5470886,5475709,4862593,5346824,5459312,5282637,5434144,5087077,5145812,5468397,5228325,4846065,5468943,5087391,5466399,5111622,5475670,5188637,5476581,5460682,5459441,5474599,5410467,4734933,5333322,5475913,5093361,5464243,5402694,5475906,5237446,5469977,5474979,5458852,5476885,5468563,5430289,5471815,5476317,5261483,5457833,5302735,5399986,4743925,5446394,5473341,5464641,4635278,5473946,5470963,5470636,1835966,5470441,5468149,5451373,5397184,5475773,5475264,1770381,5247215,5471844,5470358,5215372,5449155,5292258,5476227,5203139,5410517,5263408,5172245,5182271,5468706,5098120,5477096,5469787,5406689,5240686,5476719,5455585,5462766,5331606,5409305,5314766,5475262,5344706,5374709,5476510,5097958,5440551,5471294,5145827,5470257,5354474,5450670,4782937,5473062,5476638,5472243,5197555,5359866,5470891,4728750,5328525,5447315,5470544,5473290,5476007,5468962,5227661,5335643,5431422,5468253,5460381,5468410,4118502,5477105,5472670,5468892,5464312,5229127,5475596,5431806,2226293,5460480,4853207,5155536,5474694,2188953,4774902,5474912,5471614,4864249,4848240,5471800,5458838,5451359,4106949,5344700,5344046,5233193,5406534,5316195,5415856,5097220,4784793,5469803,5412258,4778574,5475184,5470697,5476962,5385860,5463342,5332365,5468681,1806625,5251824,5412238,5211738,4643920,4604774,5470933,5124992,5331053,5446996,5475552,5471728,5400574,5455528,5307238,5470597,5453469,5444589,5362637,5091126,5184682,4865184,5115626,5461983,5473607,5471477,5416474,5359264,5469032,5287234,5472658,5356672,5414553,5469637,5092212,2161911,5397766,5436759,5315505,5450538,5366987,5468659,5322630,2348133,5096494,5248832,5251744,5247178,5423662,5449423,5210783,5227529,5424743,5406854,4633932,5454197,5401974,5445045,5469017,5473484,5472676,5471138,5275375,5469596,5477171,5461292,4726323,5468942,5406837,5363939,5452710,5475331,5471078,5476387,5279164,5332343,5469694,5476221,5314403,5476585,5472442,4867610,5476590,5177159,5475911,5155600,5469709,5475057,5471303,5468429,5471258,5476729,2447033,5476097,5470563,5391570,4856115,1767311,5420362,5301980,5407329,5471397,5269346,5473359,5334631,5464511,5472407,5472351,4853458,5472966,5388286,4749140,4604502,4694213,4748417,5469157,5188706,5469019,5095380,5471415,5092389,5465077,5475288,5472225,5471920,1752744,5466386,4778818,5475451,5172265,5438271,5476250,5471851,5145599,5469710,5420001,5473883,5469888,2020814,5222648,5467074,5470389,5466352,5468886,5477127,5458409,5476728,5472044,1739494,5240212,5456779,5387574,5460886,5460507,5451514,5343281,5400419,5461918,5344223,5150845,5460481,5474744,5404019,5037511,5451958,5475406,5165688,5407919,5095515,5463617,5469865,2191680,5468808,5475076,5252128,5463937,5439579,5474865,5473796,5398272,5466705,4866866,5423932,5181725,5322560,5438262,5096574,5476538,5223549,5466113,5126180,5363640,5476015,5476899,5384492,1727711,5475421,5472844,5472403,2162185,2327853,5473554,5470705,5451376,5468830,5372456,5468417,5474640,5474344,5476945,5466941,4745118,5375333,5352199,5088893,5435429,5418248,5313343,5453898,4854167,5470513,5474965,5475862,5451490,5475132,5474554,5469776,4845617,5268249,5264032,5087603,5463681,5446560,5466723,5461962,5468993,5469870,5391380,4779335,5313251,5195791,4688159,5432324,5440235,5229178,5471070,5476802,5475204,5472635,5470711,4732604,5335644,1735985,5468100,5472500,5475681,5212346,5472989,5211566,5411409,5473925,5473540,5286195,5474498,5413526,5098713,5406439,5477731,5227096,5396506,5431157,5455971,4659564,4677177,5472154,5466258,5470791,5469269,5432845,5474370,5473983,5455595,5381057,5473813,5473294,5465961,5455569,5471105,5477738,5466835,5471370,5476980,5476045,5468308,5468621,5473490,4107010,5145802,5338300,5432310,5291457,5359585,5475719,5474102,5472232,5460877,5458467,5436715,5205792,4858801,5470196,5411968,5151359,5428027,5447433,5294425,5454460,5475821,5472690,5460928,5474203,5362608,5476180,5106928,5470871,5468524,5315404,5469627,4850716,5472740,5470924,5472578,5353459,5472309,4758784,5185952,2351906,5460401,2340246,5465546,5300180,5445403,5155116,5466888,5367514,5264006,5476639,5472339,5473284,5450403,5461416,5189726,5395821,5283202,5474365,5455019,5472070,5476063,5350589,5261486,5279659,5460459,5374694,5224468,5464307,5155241,5374834,4774194,5456827,5475021,2020427,5447853,5476290,5455548,4675050,5451126,5471317,5475912,5343562,5468321,5469600,5473432,5455003,5468745,4632935,5476826,5233487,5423082,5475265,5389425,5473573,5465363,5427940,5449619,5456045,5092719,5473350,5424115,5353466,5382282,5475055,5384466,5349218,5474691,5475624,5471643,5267084,5475245,5452019,5471607,5470625,5455079,5470996,5475157,5475725,5476646,5471644,5471968,5467272,5328535,5474689,5476305,5470560,5476476,5419457,5388707,5417134,5474616,5470640,5472867,5475580,5425134,5185531,5464941,5471302,2476249,5422237,5278772,5474540,5468454,5402449,5469619,5452045,5475035,4775742,5473396,5177867,4634400,4779405,5465468,5304116,5456227,1799543,4648203,5469845,5424689,5254216,5438304,4643394,5476400,5473708,5471910,5419224,4669019,5471712,5468661,4733767,5395722,5475031,5468173,5469862,2413289,5474216,5469262,5427765,5473287,5474633,5467724,4869764,5442810,5465292,5470850,5327487,5474685,5305361,5472451,5470229,5476587,5477728,5467104,5250665,5472535,5455630,5472805,5422068,5468638,5476572,5449318,5472069,5165393,5477003,5468636,5473903,5346604,5471201,4868517,5468254,5475450,5470917,4159553,5209952,5391128,2157222,5451756,2296029,4852260,5468932,4640284,5252133,5469029,5286952,5456819,5276577,5470650,5471418,5272860,5211321,5471852,5442111,5473845,5456073,5471814,5477015,5470152,5468284,5474523,5459714,5475197,5093530,5470244,5467869,5279291,5469914,5290095,5459683,5419167,5209696,4745673,5465272,5468679,5473444,5452076,2222425,1714487,5111038,5324213,5241945,5469742,5280063,5450434,5107457,5474485,5475733,5478660,5475551,5471192,5389134,2251479,5477734,5470808,5467592,5267144,5463488,5472104,5386391,5476614,5474323,5470605,5469272,5468472,5151464,5471025,5093661,5471293,5472626,5471889,5472834,5473388,5347036,5473876,5475355,5450253,5476052,5469758,5392681,5474409,5458477,5263581,5369723,5455010,5322924,4857930,5098993,5172204]
      # 'AVM Rental,AVM Sale,
      # Back on Market,
      # Back on Market Date,
      # Bathrooms,
      # Beds,Calculated CAPEX,CDOM Sale,City,COMP GRADE,County,Distance,DOM,Elementary School,Garage Capacity,High School,Label,Latitude,Legal Subdivision,List Price,List Price/SF,
      # Longitude,Lot Size,Max CAP Offer,
      # Max CAPEX,Max LTV Offer,
      # Max Price,
      # Middle School,Min CAP Rate,Minimum CAPEX,MLS Number,
      # Offer CAP Rate,Offer to List Price,
      # Pool Private,Price per Sq Ft Sold,Price Reduction,Property Category,Rental CAP Rate,Rental Class,Rental Max Purch to LP Ratio,Rental Max Purchase Price,Rental Sold Count,Sale Price,Square Feet,Status,Street Address,Subdivision,Tax ID,Year Built,Zip Code,Listing Agent,Listing Agent Name,Listing Broker,Listing Broker Name,Selling Agent Name'
      sts.each do |d|
        Property.assign_user_id(21)
        p = Property.find(d)
        pclass = "invitation_proforma"
        sps = pclass.classify.constantize.where(["user_id = ? and property_id = ?", 21, p.id])
        
        
        pf = nil
        if sps  
          if sps.count == 0
            pf = pclass.classify.constantize.new(user_id: 21, property_id: p.id)
          else
            pf = sps.first
          end
        end
#         
        puts  "#{p.id}^ " \
              "#{p.street_address}^ " \
              "#{p.zillow_id}^ " \
              "#{p.comps_rental_stats["moderate_avm"]}^" \
              "#{p.comps_sale_stats["expanded_avm"]}^" \
              "#{p.back_on_market}^" \
              "#{p.is_back_on_market}^" \
              "#{p.bathrooms}^" \
              "#{p.beds}^"  \
              "#{pf.rental_output_renovation_calculated}^"  \
              "#{p.cdom}^"  \
              "#{p.city}^"  \
              "#{p.comp_grade}^"  \
              "#{p.county}^"  \
              "#{p.dom}^"  \
              "#{p.elementary_school}^"  \
              "#{p.garage_capacity}^"  \
              "#{p.high_school}^"  \
              "#{p.user_assigned_tag(21).try(:property_tag).try(:name)}^"  \
              "#{p.latitude}^"  \
              "#{p.legal_subdivision}^"  \
              "#{p.listing_date}^"  \
              "#{p.closed_date}^"  \
              "#{p.list_price}^"  \
              "#{p.list_price_psf}^"  \
              "#{p.longitude}^"  \
              "#{p.lot_square_feet}^"  \
              "#{pf.rental_output_max_purchase_amount}^"  \
              "#{pf.rental_output_max_renovation_amount}^"  \
              "#{pf.rental_output_max_ltv_offer}^"  \
              "#{pf.rental_output_max_price}^"  \
              "#{p.middle_school}^"  \
              "#{pf.rental_output_minimum_cap_rate}^"  \
              "#{pf.rental_output_minimum_capex}^"  \
              "#{p.mls_number}^"  \
              "#{pf.rental_output_cap_rate}^"  \
              "#{pf.rental_output_max_purchase_ratio}^"  \
              "#{p.pool_private}^"  \
              "#{p.sale_price_psf}^"  \
              "#{p.price_reduction}^"  \
              "#{p.category}^"  \
              "#{pf.rental_output_cap_rate}^"  \
              "#{p.rental_class}^"  \
              "#{pf.rental_output_max_purchase_ratio}^"  \
              "#{pf.rental_output_max_purchase_amount}^" \
              "#{p.comps_rental_stats["sold_count"]}^"  \
              "#{p.sale_price}^"  \
              "#{p.square_feet}^"  \
              "#{p.status}^"  \
              "#{p.street_address}^"  \
              "#{p.subdivision}^"  \
              "#{p.tax_id}^"  \
              "#{p.year_built}^"  \
              "#{p.zip_code}^"  \
              "#{p.listing_agent_id}^"  \
              "#{p.listing_agent_name}^"  \
              "#{p.listing_broker_id}^"  \
              "#{p.listing_broker_name}^"  \
              "#{p.selling_agent_name}"  




              
              
      end
      
    rescue Exception => ex
      puts " error #{ex.message}"
    end
  end

  desc "dallas_comp"
  task :dallas_comp => :environment do |t, args|
    begin


      mapping = {'Atlanta' => 5,
      'Chicago' => 4,
      'Dallas-Fort Worth' => 3,
      'Houston' => 1   }


      export_file = 'dallas3.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "comps"

      #-------------------------------------

      workbook = Roo::Excel.new('source.xls')

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      ary = []
      ((workbook.first_row + 1)..workbook.last_row).each do |row|
        hsh = {}
        
        acomps = workbook.row(row)[0].split(" ")
        street_number =  acomps.first
        street_direction = acomps[1..(acomps.length - 2)].join(" ")
        street_name = acomps.last

        square_feet    = workbook.row(row)[1]
        year_built     = workbook.row(row)[2]
        beds           = workbook.row(row)[3]
        bathrooms_full = workbook.row(row)[4]
        zip_code       = workbook.row(row)[5]
        state          = workbook.row(row)[6]
        county         = workbook.row(row)[7]
        city           = workbook.row(row)[8]

        puts "#{street_number} #{street_direction} #{street_name} #{square_feet} #{year_built} #{beds} #{bathrooms_full} #{zip_code} #{state} #{county}"

        p = Property.new(street_number: street_number, 
                         street_direction: street_direction , 
                         street_name: street_name, 
                         square_feet: square_feet,
                         year_built: year_built,
                         beds: beds,
                         bathrooms_full: bathrooms_full,
                         state: state, 
                         zip_code: zip_code,
                         county: county,
                         city: city,
                         category: 'single_family',
                         market_id: mapping[city])
        p.save(validate: false)

        puts "p.market_id = #{p.market_id}"
        case p.market_id 
        when 1
          
        when 3
          provider = "ntreis".camelcase.constantize.new(Market.find(p.market_id))
          provider.append_rets(p)
        when 4
          provider = "mred".camelcase.constantize.new(Market.find(p.market_id))
          provider.append_rets(p)
        when 5
          provider = "gamls".camelcase.constantize.new(Market.find(p.market_id))
          provider.append_rets(p)
        end

        if p.compute6(true)
          Delayed::Worker.logger.debug "computed successfully"          
        else
          Delayed::Worker.logger.debug "compute failed"          
        end

        p.save(validate: false)

        hsh["address"] = workbook.row(row)[0]
        hsh["square_feet"]    = workbook.row(row)[1]
        hsh["year_built"]      =  workbook.row(row)[2]  
        hsh["beds"]            = workbook.row(row)[3]
        hsh["bathrooms_full"] = workbook.row(row)[4]
        hsh["zip_code"]     = workbook.row(row)[5]  
        hsh["state"]          = workbook.row(row)[6]
        hsh["county"]         = workbook.row(row)[7]
        hsh["city"]           = workbook.row(row)[8]

        hsh["level4"]               = p.send(:comps_sale_stats)["level4"]
        hsh["level5"]               = p.send(:comps_sale_stats)["level5"]
        hsh["sold_count"]            = p.send(:comps_sale_stats)["sold_count"]
        hsh["zillow_sale_estimate"] = p.zillow_sale_estimate


        # hsh["street_number"] = p.street_number
        # hsh["street_name"] = p.street_name
        # hsh["city"] = p.city
        # hsh["state"] = p.state
        # hsh["zip_code"] = p.zip_code
        # hsh["zillow_sale_estimate"] = p.zillow_sale_estimate
        # hsh["zillow_rental_estimate"] = p.zillow_rental_estimate
        # hsh["elementary_school"] = p.elementary_school
        # hsh["elementary_school_rating"] = p.elementary_school_rating
        # hsh["middle_school"] = p.middle_school
        # hsh["middle_school_rating"] = p.middle_school_rating
        # hsh["high_school"] = p.high_school
        # hsh["high_school_rating"] = p.high_school_rating

        # ary << hsh

        columns = %w{address square_feet year_built beds bathrooms_full zip_code state county city level4 level5 sold_count zillow_sale_estimate}
        
        columns.each do |d|
          sheet1.row(i).push hsh[d] 
        end
        
        # break if i >= 20
        i += 1
      end

      book.write export_file 

      # market = Market.find(market_id)
      
      # ids = market.properties.where(["category = ? AND status IN (?) AND (id between ? and ?) AND (zillow_id is null)", category, status,  start_id, end_id]).pluck(:id) 
            
      # if ids && ids.length > 0
      #   market = Market.find(market_id)
      #   market.properties.where(id: ids).find_each do |p|
          
      #     p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
          
      #     if p.compute4(true)
      #       Delayed::Worker.logger.debug "computed successfully"
            
      #     else
      #       Delayed::Worker.logger.debug "compute failed"
            
      #     end
           
          
      #   end
      # end
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  desc "dummy"
  task :dummy => :environment do |t, args|
    begin
      Delayed::Worker.logger.debug " started"
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  desc "get zillow"
  task :get_zillow => :environment do |t, args|
    begin
      
      threads = []
      tn = []
      tn = ["one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen"]
      
      rows_per_chunk = 600
      
      export_file = 'file4.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "sheet1"

      #-------------------------------------

      workbook = Roo::Excel.new('nash.xls')

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      ary = []
      ((workbook.first_row + 1)..workbook.last_row).each do |row|
        hsh = {}
        street_address = workbook.row(row)[0].to_s
        a = street_address.split(" ")
        street_number = a.shift
        street_name = a.join(" ")
        
        street_name_components = street_name.match(/(.*?)\s*(\(*Lot\s*[\d#]+\)*|#\s*\d+|L\s*[-]*\d+|\*\s*\d+|\d+$)/i).captures rescue nil
        if ((street_name_components) && (street_name_components.length > 0))
          street_name = street_name_components[0]
        end
        puts "street_name = #{street_name}"
        
        city = workbook.row(row)[1]
        zip = workbook.row(row)[2].to_i.to_s
        state = workbook.row(row)[8]
        
        
        year_built  = workbook.row(row)[3]
        square_feet  = workbook.row(row)[4]
        bed  = workbook.row(row)[5]
        bath  = workbook.row(row)[6]
        county  = workbook.row(row)[7]
        subdivision  = workbook.row(row)[9]
        list_price  = workbook.row(row)[10]
        sale_price  = workbook.row(row)[11]
        list_date  = workbook.row(row)[12].to_s
        close_date  = workbook.row(row)[13].to_s
        dom  = workbook.row(row)[14]
        annual_taxes  = workbook.row(row)[15]
        annual_hoa  = workbook.row(row)[16]
        pool  = workbook.row(row)[17]
        listing_type  = workbook.row(row)[18]
        tax_number  = workbook.row(row)[19]               
       # street_number = workbook.row(row)[0].to_i.to_s
       # street_name = workbook.row(row)[1]
       # city = workbook.row(row)[2]
       #state = workbook.row(row)[3]
       # zip_code = workbook.row(row)[4].to_i.to_s
        puts "#{street_number} #{street_name}, #{city}, #{state} #{zip}"

        p = Property.new(street_number: street_number, street_name: street_name, city: city, state: state, zip_code: zip)

        if p.compute5(true)
          puts "computed successfully"          
        else
          puts "compute failed"          
        end
       
        
        hsh = {}
        set_hash(hsh,:street_address,street_address)
        set_hash(hsh,:city,city)
        set_hash(hsh,:zip,zip)
        set_hash(hsh,:year_built,year_built)
        set_hash(hsh,:square_feet,square_feet)
        set_hash(hsh,:bed,bed)
        set_hash(hsh,:bath,bath)
        set_hash(hsh,:county,county)
        set_hash(hsh,:state,state)
        set_hash(hsh,:subdivision,subdivision)
        set_hash(hsh,:list_price,list_price)
        set_hash(hsh,:sale_price,sale_price)
        set_hash(hsh,:list_date,list_date)
        set_hash(hsh,:close_date,close_date)
        set_hash(hsh,:dom,dom)
        set_hash(hsh,:annual_taxes,annual_taxes)
        set_hash(hsh,:annual_hoa,annual_hoa)
        set_hash(hsh,:pool,pool)
        set_hash(hsh,:listing_type,listing_type)
        set_hash(hsh,:tax_number,tax_number)
        set_hash(hsh,:zillow_sale_estimate,p.zillow_sale_estimate)
        set_hash(hsh,:zillow_rental_estimate,p.zillow_rental_estimate)
        
        cols = %w{street_address city zip year_built  square_feet  bed  bath  county  state  subdivision  list_price  sale_price  list_date  close_date  dom  annual_taxes  annual_hoa  pool  listing_type  tax_number zillow_sale_estimate zillow_rental_estimate}
        # puts "hash #{i}: #{hsh}"
        cols.each do |d|
          sheet1.row(i).push hsh[d] 
        end
        
        i += 1
      end

      book.write export_file 
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  
  
  
  desc "grab zillow"
  task :grab_zillow => :environment do |t, args|
    begin
      
      threads = []
      tn = []
      tn = ["one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen"]
      
      rows_per_chunk = 600
      
      

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "sheet1"

      #-------------------------------------
      
      input_file_name = "n1"
      workbook = Roo::Excel.new("#{input_file_name}.xls")
      
      export_file = "file#{input_file_name[1,1]}.xls"
      File.delete(export_file) if File.exist?(export_file)

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      ary = []
      ((workbook.first_row + 1)..workbook.last_row).each do |row|
        hsh = {}
        
       
        street_name = workbook.row(row)[1]
        street_name_components = street_name.match(/(.*?)\s*(\(*Lot\s*[\d#]+\)*|#\s*\d+|L\s*[-]*\d+|\*\s*\d+|\d+$)/i).captures rescue nil
        if ((street_name_components) && (street_name_components.length > 0))
          street_name = street_name_components[0]
        end
        
        street_number = workbook.row(row)[0]
        city = workbook.row(row)[2]
        
        state = workbook.row(row)[10]
        
        
        year_built  = workbook.row(row)[3]
        square_feet  = workbook.row(row)[4]
        bed  = workbook.row(row)[5]
        fb_tot = workbook.row(row)[6] 
        hb_tot = workbook.row(row)[7] 
        zip = workbook.row(row)[8].to_i.to_s
        county  = workbook.row(row)[9]
        
        subdivision = workbook.row(row)[11]
        
        list_price  = workbook.row(row)[12]
        sale_price  = workbook.row(row)[13]
        close_date  = workbook.row(row)[14].to_s
        sale_price  = workbook.row(row)[15]
        association_fee = workbook.row(row)[16]
        pool  = workbook.row(row)[17]
        listing_type  = workbook.row(row)[18]
        status  = workbook.row(row)[19]
        list_date = workbook.row(row)[20].to_s
        area = workbook.row(row)[21]
        tax_id = workbook.row(row)[22]
        dom = workbook.row(row)[23]
        list_agent = workbook.row(row)[24]
        
        
                   
       # street_number = workbook.row(row)[0].to_i.to_s
       # street_name = workbook.row(row)[1]
       # city = workbook.row(row)[2]
       #state = workbook.row(row)[3]
       # zip_code = workbook.row(row)[4].to_i.to_s
        puts "#{street_number} #{street_name}, #{city}, #{state} #{zip}"

        p = Property.new(street_number: street_number, street_name: street_name, city: city, state: state, zip_code: zip)

        if p.compute5(true)
          
          puts "computed successfully"          
        else
          puts "compute failed"          
        end
       
        
        hsh = {}
        set_hash(hsh,:street_number,street_number)
        set_hash(hsh,:street_name,street_name)
        set_hash(hsh,:city,city)
        set_hash(hsh,:year_built,year_built)
        set_hash(hsh,:square_feet,square_feet)
        set_hash(hsh,:bed,bed)
        set_hash(hsh,:fb_tot,fb_tot)
        set_hash(hsh,:hb_tot,hb_tot)
        set_hash(hsh,:zip,zip)
        set_hash(hsh,:county,county)
        set_hash(hsh,:state,state)
        set_hash(hsh,:subdivision,subdivision)
        set_hash(hsh,:list_price,list_price)
        set_hash(hsh,:sale_price,sale_price)
        set_hash(hsh,:close_date,close_date)
        set_hash(hsh,:sale_price,sale_price)
        set_hash(hsh,:association_fee,association_fee)
        set_hash(hsh,:pool,pool)
        set_hash(hsh,:listing_type,listing_type)
        set_hash(hsh,:status,status)
        set_hash(hsh,:list_date,list_date)
        set_hash(hsh,:area,area)
        set_hash(hsh,:tax_id,tax_id)
        set_hash(hsh,:dom,dom)
        set_hash(hsh,:list_agent,list_agent)
        set_hash(hsh,:zillow_sale_estimate,p.zillow_sale_estimate)
        set_hash(hsh,:zillow_rental_estimate,p.zillow_rental_estimate)
        
        cols = %w{street_number street_name city year_built square_feet bed fb_tot hb_tot zip county state subdivision list_price sale_price close_date sale_price association_fee pool listing_type status list_date area tax_id dom list_agent zillow_sale_estimate zillow_rental_estimate}
        # puts "hash #{i}: #{hsh}"
        cols.each do |d|
          # puts "d: #{d} :#{hsh[d]}"
          sheet1.row(i).push hsh[d] 
        end
        
        i += 1
      end

      book.write export_file 
      
    rescue Exception => ex
      
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  def set_hash(hsh,key,val)
    hsh[key.to_s] = val
    # puts "#{key.to_s}"
  end

  desc "fix tax_id orlando"
  task :fix_tax_id,[:market_id,:category,:status,:month_old] => :environment do |t, args|
    begin
      market_id = Integer(args.market_id)
      args.with_defaults(:category => nil)
      category = args.category
      args.with_defaults(:status => nil)
      status = args.status
      args.with_defaults(:month_old => nil)
      month_old = args.month_old
      
      lookback = month_old.try(:to_i)
      

      market = Market.find(market_id)
      # ids = {}
      
      # ids = market.properties.joins("INNER JOIN proformas ON properties.id = proformas.property_id and ifnull(proformas.user_id,0) = 0").where("not (ifnull(comparables_avm_rental,-1)  = -1) and ifnull(rental_output_cap_rate,-1) = -1 and category = ? and status = ? AND (properties.id between ? and ?)) ",category,status,start_id,end_id).pluck("properties.id")
      # ids = market.properties.where(["category = ? AND status IN (?) AND (id between ? and ?)  and (zillow_updated_at > '2014-08-08')", category, status,  start_id, end_id]).pluck(:id) 

      index = 0
      scond = nil

      if lookback
        raise "month_old supported only available to status 'sold'" if status.downcase != 'sold'
        scond = ["category = ? AND status = ? AND closed_date >= ?",category,status,lookback.months.ago]
      else
        scond = ["category = ? AND status = ?",category,status]
      end

      cnt = market.properties.where(scond).count
      market.properties.where(scond).find_each(batch_size: 2000) do |p|
        begin
          p.tax_id = p.detail._raw[:tax_id]
          p.save
          Delayed::Worker.logger.debug "#{category} #{status} seq #{index + 1}/#{cnt}: p.id| #{p.id} saved"
        rescue Exception => ex
          Delayed::Worker.logger.debug "loop error #{ex.message}"
        end
        index += 1
      end

    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  desc "afx"
  task :afx => :environment do |t, args|
    begin
      # sts = [2398771,4724199,4855088,4714086,1887527,5186833,4605088,4649613,5177835,2428555,4085007,1804867,4637661,4773955,4828063,5087732,4859678,4605081,5190452,4833028,4699449,5121277,4689655,4654106,5191458,1692985,5224726,4857420,4859563,5190096,5175166,5124830,5093651,4670553,5190085,4656668,2494336,4643804,4648895,2398304,4848287,5116217,4803094,4670654,4633907,4851526,2398751,5194559,4637362,4663609,5186812,5182628,5186784,5091685,4779920,5091938,4637217,5176089,1806001,4643527,1725471,4779360,5213919,5179709,5145690,4699131,4855963,4681057,4851527,4732767,4864583,5090451,5193516,4845170,4740341,4084973,5213987,5088989,4650991,5210231,2235012,5116258,4690762,5087587,5180762,4106420,2453898,4656895,5244413,4709352,5181052,5102402,4820843,4779352,2377462,5126208,2212475,5125115,5093151,2420696,4778687,5180825,1770366,4726078,4784084,1982509,4645723,4682587,2059950,4749395,5186698,4637022,4652382,4860252,4646816,4646828,5179676,5192408,4723041,5188961,5090417,5248846,5211707]

      # sts = [2398771,4724199,2222300,1887527,1828218,4649613,5151291,2428555,4085007,1804867,4637661,4773955,4828063,4691520,4859678,1766189,5190452,4833028,4632533,4654106,5191458,1692985,2226424,4857420,4859563,5175166,5093651,2343861,4778282,2409862,2494336,4643804,2194083,2398304,1682064,1758709,2230593,1682914,2205316,2204787,2398751,4652712,2531263,4604087,4778817,4712023,5091938,2494340,1806001,4643527,1725471,4634515,2409806,2391857,4855963,1750689,1682552,4732767,2311113,4826303,4107227,4740341,4084973,4719633,2351557,4650991,2235012,4672814,1720876,5087587,2441296,4106420,2453898,1796701,2295789,4709352,2316057,2377462,2259351,2212475,2259760,4106942,2420696,2402406,2156654,1770366,4784084,1982509,2328201,2180103,2059950,2213002,4148062,2212994]
#       
      # sts = [46040345,46052501,46058606,46075000,46279726,46320780,47661230,47664001,47664477,47678233,47680968,47990045,53568294,53753434,54447361,58318708,58338731,62027687,66184901,66184957,66186675,66664765,68112946,69319076,71025542,71064513,71066037,71069031,71070355,80764029,81477559,81482363,82073001,82073040,82099418,82106731,82430776,84638424,89871761,99686186,99691705,99704857,99714626,99720134,99753682,103131114,103422032]

      # sts = [2398771,4724199,2222300,1887527,1828218,4649613,5151291,2428555,4085007,1804867,4637661,4773955,4828063,4691520,4859678,1766189,5190452,4833028,4632533,4654106,5191458,1692985,2226424,4857420,4859563,5175166,5093651,2343861,4778282,2409862,2494336,4643804,2194083,2398304,1682064,1758709,2230593,1682914,2205316,2204787,2398751,4652712,2531263,4604087,4778817,4712023,5091938,2494340,1806001,4643527,1725471,4634515,2409806,2391857,4855963,1750689,1682552,4732767,2311113,4826303,4107227,4740341,4084973,4719633,2351557,4650991,2235012,4672814,1720876,5087587,2441296,4106420,2453898,1796701,2295789,4709352,2316057,2377462,2259351,2212475,2259760,4106942,2420696,2402406,2156654,1770366,4784084,1982509,2328201,2180103,2059950,2213002,4148062,2212994,2001422,4644399,2271600,5248846,5192408,2328216]

      sts = [4708560,2398771,4724199,2222300,1887527,1828218,4649613,5151291,2428555,4085007,1804867,4637661,4773955,4828063,4691520,4859678,5203362,4833028,4632533,5186082,4654106,5201842,1692985,2226424,4857420,4859563,5166148,5093651,2343861,4778282,5209931,2409862,2494336,4643804,2194083,2398304,1682064,1758709,2230593,1682914,2205316,2204787,2398751,4652712,2531263,4604087,4778817,4712023,5091938,5098456,2494340,1806001,4643527,1725471,4634515,2409806,2391857,4855963,1750689,1682552,4732767,2311113,4826303,4107227,4740341,4084973,4719633,2351557,4650991,2235012,4672814,1720876,5087587,2441296,4106420,2453898,1796701,2295789,4709352,2316057,2377462,2259351,2212475,2259760,4106942,2420696,2402406,2156654,1770366,4784084,1982509,2328201,2180103,2059950,2213002,4148062,2212994]
      # 'AVM Rental,AVM Sale,
      # Back on Market,
      # Back on Market Date,
      # Bathrooms,
      # Beds,Calculated CAPEX,CDOM Sale,City,COMP GRADE,County,Distance,DOM,Elementary School,Garage Capacity,High School,Label,Latitude,Legal Subdivision,List Price,List Price/SF,
      # Longitude,Lot Size,Max CAP Offer,
      # Max CAPEX,Max LTV Offer,
      # Max Price,
      # Middle School,Min CAP Rate,Minimum CAPEX,MLS Number,
      # Offer CAP Rate,Offer to List Price,
      # Pool Private,Price per Sq Ft Sold,Price Reduction,Property Category,Rental CAP Rate,Rental Class,Rental Max Purch to LP Ratio,Rental Max Purchase Price,Rental Sold Count,Sale Price,Square Feet,Status,Street Address,Subdivision,Tax ID,Year Built,Zip Code,Listing Agent,Listing Agent Name,Listing Broker,Listing Broker Name,Selling Agent Name'
      sts.each do |d|
        Property.assign_user_id(21)
        p = Property.find(d)
        pclass = "invitation_proforma"
        sps = pclass.classify.constantize.where(["user_id = ? and property_id = ?", 21, p.id])
        
        
        pf = nil
        if sps  
          if sps.count == 0
            pf = pclass.classify.constantize.new(user_id: 21, property_id: p.id)
          else
            pf = sps.first
          end
        end
#         
        puts  "#{p.id}^ " \
              "#{p.street_address}^ " \
              "#{pf.rental_output_max_purchase_ratio}"
                            
      end
      
    rescue Exception => ex
      puts " error #{ex.message}"
    end
  end

  desc "neighborhood"
  task :get_neighborhoods => :environment do |t, args|
    begin
      export_file = 'file3.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      # book = Spreadsheet::Workbook.new
      # sheet1 = book.create_worksheet :name => "staffs"
      
      #-------------------------------------

       
      

      workbook = Roo::Excel.new('nst_03.xls')

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      ary = []
      ((workbook.first_row + 1)..workbook.last_row).each do |row|

        proxy = Proxy.find_available

        if proxy

          ips = GoodIp.find_working_ips
          if !ips && (ips.count < 6)
            ips = [PROX_1,PROX_2,PROX_3,PROX_4,PROX_5,PROX_6]
          end

          phantomjs_options = ['--load-images=no', '--ignore-ssl-errors=yes', '--ssl-protocol=any']
          
          if !Rails.env.development?
            case (Time.now.to_f * 1000.0.to_i).to_s.last.to_i % 6
            when 0
              phantomjs_options << "--proxy=#{ips[0]}:80" 
              phantomjs_options << "--proxy-auth=glt:gltproxy"
            when 1
              phantomjs_options << "--proxy=#{ips[1]}:80" 
              phantomjs_options << "--proxy-auth=glt:gltproxy"
            when 2
              phantomjs_options << "--proxy=#{ips[2]}:80" 
              phantomjs_options << "--proxy-auth=glt:gltproxy"
            when 3
              phantomjs_options << "--proxy=#{ips[3]}:80" 
              phantomjs_options << "--proxy-auth=glt:gltproxy"
            when 4
              phantomjs_options << "--proxy=#{ips[4]}:80" 
              phantomjs_options << "--proxy-auth=glt:gltproxy"
            when 5
              phantomjs_options << "--proxy=#{ips[5]}:80" 
              phantomjs_options << "--proxy-auth=glt:gltproxy"
            end
          end


          Capybara.register_driver :poltergeist do |app|
            Capybara::Poltergeist::Driver.new(app, :js_errors => false, :timeout => 20, phantomjs_options: phantomjs_options)
          end
            
          Capybara.default_driver = :poltergeist
          Capybara.javascript_driver = :poltergeist
          Capybara.default_wait_time = 20
          Capybara.ignore_hidden_elements = false
          gsession = Capybara::Session.new(:poltergeist)
          hsh = {}
          census_geoid = workbook.row(row)[0].to_i.to_s
          neighborhood_name = workbook.row(row)[1]
          
          puts "#{census_geoid} #{neighborhood_name}"

          properties = Property.where(["market_id = ? and census_geoid = ?",1,census_geoid]).order("id desc") 
          if properties && properties.length > 0
            
            properties.each do |d|
              puts "Property found: #{d.address}"
              if nscout_code_local(d,gsession,neighborhood_name)
                break
              end
            end
            # nscout_code(property,gsession,neighborhood_name)
            # end

          else
            puts "No property matched"
          end

          gsession.driver.quit

        else
          puts "No proxy"
        
        end
        
        # break if i >= 20
        i += 1
      end

      # its = [5648462,4545137]
      # its.each do |it|
      #   property = Property.find(it)
      #   if !authorized_nscout_code(property,gsession)
      #     nscout_code(property,gsession)
      #   end
      # end



      
      
    rescue Exception => ex
      puts " error #{ex.message}"
    end
  end

  def nscout_code(prop,csession,cneighborhood_name=nil)
    ret = nil
    attrs = {}
    begin
      puts "1"
      csession.visit "http://www.neighborhoodscout.com/advanced-search/"
      puts "2"
      csession.find(".match-toggle").click
      puts "3"
      csession.fill_in "matching_search_address", :with => prop.address
      
      puts "4"
      csession.find("span#matching_search_stateSelectBoxItArrowContainer").click
      puts "5"
      csession.find("ul#matching_search_stateSelectBoxItOptions li[data-id='45']").click
      puts "6"
      sleep 5
      
      csession.find("span#matching_search_withinSelectBoxItArrowContainer").click
      csession.find("ul#matching_search_withinSelectBoxItOptions li[data-id='7']").click
      puts "7"
      csession.find("span#matching_search_citySelectBoxItArrowContainer").click      
      csession.find("ul#matching_search_citySelectBoxItOptions li[data-id='759']").click
      puts "8"
      csession.find("#matching-submit").click
      puts "9"
      sleep 6
      # return csession.current_url

      htm  = Nokogiri::HTML.parse(csession.html)
      hs = htm.xpath("//td[@class='name search-results']")

      data_mapping = ["ns_neigborhood", "ns_neigborhood_percent", "ns_comp1", "ns_comp1_percent", "ns_comp2", "ns_comp2_percent", "ns_comp3", "ns_comp3_percent", "ns_comp4", "ns_comp4_percent", "ns_comp5", "ns_comp5_percent", "ns_comp6", "ns_comp6_percent", "ns_comp7", "ns_comp7_percent", "ns_comp8", "ns_comp8_percent", "ns_comp9", "ns_comp9_percent"] 

      hs.each do |h|
        data_neighborhood_name = h.text.gsub(/\n/,"")
        data_neighborhood_percent = h.next_element.text
        popped_items = data_mapping.shift(2)
        
        attrs = attrs.merge({popped_items[0].to_sym =>  data_neighborhood_name, popped_items[1].to_sym =>  data_neighborhood_percent})

      end

      puts attrs 

      csession.find("td.search-results",:text => hs[0].text.gsub(/\n/,"")).click
      csession.click_on "See Neighborhood Information" 

      sleep 5

      dpage  = Nokogiri::HTML.parse(csession.html)
      
      dpage_match = dpage.xpath("//script[contains(.,'window.mapOptions')]").try(:text)
      
      raw =  dpage_match.gsub(/\r\n|\r|\n|\\/,'').gsub(/\"/, '\'')
      matches = raw.match(/'polygon_id':'(\d+)/)
      polygon_id = matches[1]

      puts "polygon_id #{polygon_id}"
      
      glt_neighborhood = cneighborhood_name
      addtl_attrs = {:market_id => prop.market_id, 
                       :address => prop.address,
                      :subdivision => prop.subdivision, 
                      :legal_subdivision => prop.legal_subdivision,
                      :mls_area_name => prop.mls_area_name,
                      :latitude => prop.latitude,
                      :longitude => prop.longitude,
                      :census_state => prop.census_state,
                      :census_county => prop.census_county,
                      :census_tract => prop.census_tract,
                      :census_block => prop.census_block,
                      :census_geoid => prop.census_geoid,
                      :glt_neighborhood => glt_neighborhood,
                      :polygon_id => polygon_id
                    }

      attrs = attrs.merge(addtl_attrs)
      neighborhood = Neighborhood.new(attrs)
      neighborhood.save

      create_html_source(glt_neighborhood,csession.html.html_safe)
      
      
      return true





     rescue Exception =>  e

      puts "Error: #{e.message}"
      
      ret = false
     end

     return ret
  end

  def nscout_code_local(prop,csession,cneighborhood_name=nil)
    ret = nil
    attrs = {}
    begin

      
      puts "1"
      csession.visit "http://www.neighborhoodscout.com/advanced-search/"
      
      dir = 'nscout_sources'
      filename = cneighborhood_name
      source = File.join(Rails.root, dir,filename)
      file = File.read(source)
      doc = Nokogiri::HTML(file)
      pos = 4

      nh_path = doc.at("tr[data-position='#{pos + 1}']").attribute("data-path").value

      csession.visit "http://www.neighborhoodscout.com#{nh_path}"
      # return csession.current_url

     
      sleep 4
      

      dpage  = Nokogiri::HTML.parse(csession.html)
      
      dpage_match = dpage.xpath("//script[contains(.,'window.mapOptions')]").try(:text)
      
      raw =  dpage_match.gsub(/\r\n|\r|\n|\\/,'').gsub(/\"/, '\'')
      matches = raw.match(/'polygon_id':'(\d+)/)
      polygon_id = matches[1]

      attr_pos = attrs["ns_comp#{pos}".to_sym]
      puts "HOU:#{cneighborhood_name}#ns_comp#{pos}:#{attr_pos}##{polygon_id}"

      ns_temp_two = NsTempTwo.new(:ns_comp => "ns_comp#{pos}", :census_geoid => polygon_id , :glt_neighborhood => cneighborhood_name   )
      ns_temp_two.save
      # glt_neighborhood = cneighborhood_name
      # addtl_attrs = {:market_id => prop.market_id, 
      #                :address => prop.address,
      #               :subdivision => prop.subdivision, 
      #               :legal_subdivision => prop.legal_subdivision,
      #               :mls_area_name => prop.mls_area_name,
      #               :latitude => prop.latitude,
      #               :longitude => prop.longitude,
      #               :census_state => prop.census_state,
      #               :census_county => prop.census_county,
      #               :census_tract => prop.census_tract,
      #               :census_block => prop.census_block,
      #               :census_geoid => prop.census_geoid,
      #               :glt_neighborhood => glt_neighborhood,
      #               :polygon_id => polygon_id
      #             }

      # attrs = attrs.merge(addtl_attrs)
      # neighborhood = Neighborhood.new(attrs)
      # neighborhood.save
      # create_html_source(glt_neighborhood,csession.html.html_safe)
      
      
      
      
      return true
      



     rescue Exception =>  e

      puts "Error: #{e.message}"
      
      return false
     end

     return ret
  end

  def authorized_nscout_code2(prop,csession,cneighborhood_name=nil)
    ret = nil
    attrs = {}
    begin

      
      puts "1"
      csession.visit "http://www.neighborhoodscout.com/advanced-search/"
      if nscout_login(csession)
        puts "2.5"
        csession.find(".match-toggle").click
        puts "3"
        csession.fill_in "matching_search_address", :with => prop.address
        
        puts "4"
        csession.find("span#matching_search_stateSelectBoxItArrowContainer").click
        puts "5"
        csession.find("ul#matching_search_stateSelectBoxItOptions li[data-id='45']").click
        puts "6"
        sleep 5
        
        csession.find("span#matching_search_withinSelectBoxItArrowContainer").click
        csession.find("ul#matching_search_withinSelectBoxItOptions li[data-id='14']").click
        puts "7"
          csession.find("span#matching_search_citySelectBoxItArrowContainer").click      
          csession.find("ul#matching_search_citySelectBoxItOptions li[data-id='759']").click
        puts "8"
        csession.find("#matching-submit").click
        puts "9"
        sleep 5
        # return csession.current_url

        htm  = Nokogiri::HTML.parse(csession.html)
        hs = htm.xpath("//td[@class='name search-results']")

        data_mapping = ["ns_neigborhood", "ns_neigborhood_percent", "ns_comp1", "ns_comp1_percent", "ns_comp2", "ns_comp2_percent", "ns_comp3", "ns_comp3_percent", "ns_comp4", "ns_comp4_percent", "ns_comp5", "ns_comp5_percent", "ns_comp6", "ns_comp6_percent", "ns_comp7", "ns_comp7_percent", "ns_comp8", "ns_comp8_percent", "ns_comp9", "ns_comp9_percent"] 

        hs.each do |h|
          data_neighborhood_name = h.text.gsub(/\n/,"")
          data_neighborhood_percent = h.next_element.text
          popped_items = data_mapping.shift(2)
          
          attrs = attrs.merge({popped_items[0].to_sym =>  data_neighborhood_name, popped_items[1].to_sym =>  data_neighborhood_percent})

        end

        


        


        puts "attrs: #{attrs}"

        # csession.find("td.search-results",:text => hs[1].text.gsub(/\n/,"")).click
        # csession.first("td.search-results").click
        pos = 9
        csession.find("tr[data-position='#{pos + 1}'] td.search-results").click
        puts "clicked #{csession.current_url}"
        # csession.click_on "See Neighborhood Information" 

        sleep 5
        

        dpage  = Nokogiri::HTML.parse(csession.html)
        
        dpage_match = dpage.xpath("//script[contains(.,'window.mapOptions')]").try(:text)
        
        raw =  dpage_match.gsub(/\r\n|\r|\n|\\/,'').gsub(/\"/, '\'')
        matches = raw.match(/'polygon_id':'(\d+)/)
        polygon_id = matches[1]

        attr_pos = attrs["ns_comp#{pos}".to_sym]
        puts "HOU:#{cneighborhood_name}#ns_comp#{pos}:#{attr_pos}##{polygon_id}"

        ns_temp_two = NsTempTwo.new(:ns_comp => "ns_comp#{pos}", :census_geoid => polygon_id , :glt_neighborhood => cneighborhood_name   )
        ns_temp_two.save
        # glt_neighborhood = cneighborhood_name
        # addtl_attrs = {:market_id => prop.market_id, 
        #                :address => prop.address,
        #               :subdivision => prop.subdivision, 
        #               :legal_subdivision => prop.legal_subdivision,
        #               :mls_area_name => prop.mls_area_name,
        #               :latitude => prop.latitude,
        #               :longitude => prop.longitude,
        #               :census_state => prop.census_state,
        #               :census_county => prop.census_county,
        #               :census_tract => prop.census_tract,
        #               :census_block => prop.census_block,
        #               :census_geoid => prop.census_geoid,
        #               :glt_neighborhood => glt_neighborhood,
        #               :polygon_id => polygon_id
        #             }

        # attrs = attrs.merge(addtl_attrs)
        # neighborhood = Neighborhood.new(attrs)
        # neighborhood.save
        # create_html_source(glt_neighborhood,csession.html.html_safe)
        
        nscout_logout(csession)
        sleep 3.5
        
        return true
      else
        raise Exception.new("Unable to log in")
      end



     rescue Exception =>  e

      puts "Error: #{e.message}"
      
      return false
     end

     return ret
  end


  def authorized_nscout_code(prop,csession,cneighborhood_name=nil)
    ret = nil
    attrs = {}
    begin

      
      puts "1"
      csession.visit "http://www.neighborhoodscout.com/advanced-search/"
      if nscout_login(csession)
        puts "2.5"
        csession.find(".match-toggle").click
        puts "3"
        csession.fill_in "matching_search_address", :with => prop.address
        
        puts "4"
        csession.find("span#matching_search_stateSelectBoxItArrowContainer").click
        puts "5"
        csession.find("ul#matching_search_stateSelectBoxItOptions li[data-id='45']").click
        puts "6"
        sleep 5
        
        csession.find("span#matching_search_withinSelectBoxItArrowContainer").click
        csession.find("ul#matching_search_withinSelectBoxItOptions li[data-id='14']").click
        puts "7"
          csession.find("span#matching_search_citySelectBoxItArrowContainer").click      
          csession.find("ul#matching_search_citySelectBoxItOptions li[data-id='759']").click
        puts "8"
        csession.find("#matching-submit").click
        puts "9"
        sleep 6
        # return csession.current_url

        htm  = Nokogiri::HTML.parse(csession.html)
        hs = htm.xpath("//td[@class='name search-results']")

        data_mapping = ["ns_neigborhood", "ns_neigborhood_percent", "ns_comp1", "ns_comp1_percent", "ns_comp2", "ns_comp2_percent", "ns_comp3", "ns_comp3_percent", "ns_comp4", "ns_comp4_percent", "ns_comp5", "ns_comp5_percent", "ns_comp6", "ns_comp6_percent", "ns_comp7", "ns_comp7_percent", "ns_comp8", "ns_comp8_percent", "ns_comp9", "ns_comp9_percent"] 

        hs.each do |h|
          data_neighborhood_name = h.text.gsub(/\n/,"")
          data_neighborhood_percent = h.next_element.text
          popped_items = data_mapping.shift(2)
          
          attrs = attrs.merge({popped_items[0].to_sym =>  data_neighborhood_name, popped_items[1].to_sym =>  data_neighborhood_percent})

        end

        


        


        puts attrs 

        # csession.find("td.search-results",:text => hs[0].text.gsub(/\n/,"")).click
        csession.first("td.search-results").click

        
        csession.click_on "See Neighborhood Information" 

        sleep 5
        

        dpage  = Nokogiri::HTML.parse(csession.html)
        
        dpage_match = dpage.xpath("//script[contains(.,'window.mapOptions')]").try(:text)
        
        raw =  dpage_match.gsub(/\r\n|\r|\n|\\/,'').gsub(/\"/, '\'')
        matches = raw.match(/'polygon_id':'(\d+)/)
        polygon_id = matches[1]

        puts "polygon_id #{polygon_id}"
        glt_neighborhood = cneighborhood_name
        addtl_attrs = {:market_id => prop.market_id, 
                       :address => prop.address,
                      :subdivision => prop.subdivision, 
                      :legal_subdivision => prop.legal_subdivision,
                      :mls_area_name => prop.mls_area_name,
                      :latitude => prop.latitude,
                      :longitude => prop.longitude,
                      :census_state => prop.census_state,
                      :census_county => prop.census_county,
                      :census_tract => prop.census_tract,
                      :census_block => prop.census_block,
                      :census_geoid => prop.census_geoid,
                      :glt_neighborhood => glt_neighborhood,
                      :polygon_id => polygon_id
                    }

        attrs = attrs.merge(addtl_attrs)
        neighborhood = Neighborhood.new(attrs)
        neighborhood.save
        create_html_source(glt_neighborhood,csession.html.html_safe)
        
        nscout_logout(csession)
        sleep 3.5
        
        return true
      else
        raise Exception.new("Unable to log in")
      end



     rescue Exception =>  e

      puts "Error: #{e.message}"
      
      return false
     end

     return ret
  end

  def nscout_login(csession)
    ret = nil
    begin
      if !csession.has_link?("My Account")
        csession.find("a[id='login']").click
        csession.first("input[id='user_email']").set "msamuels@yopmail.com"
        csession.first("input[id='user_password']").set "abc123"
        csession.find("input[id='login-button']").click
        sleep 3
        puts "2"
        

        if csession.find("ul#session-links a[href='/subscription']")
          return true
        else
          return false
        end
      else
        return true
      end
    rescue Exception => e
      puts "Exception: #{e.message}"
      ret = false
    end
    return ret
  end

  def nscout_logout(csession)
    begin
      csession.find("ul#session-links a[href='/users/sign_out']").click
    rescue Exception => e
      puts "unable to log out"
    end
  end

  def create_html_source(filename,contents)   
    begin
      dir = 'nscout_sources'

      Dir.mkdir(File.join(Rails.root, dir))  unless Dir.exist?(dir)
      File.delete(File.join(Rails.root, dir,filename)) if File.exist?(File.join(Rails.root, dir,filename))
      
      
      File.open(File.join(Rails.root, dir,filename), 'w') do |f|
        f.puts contents
      end
    rescue Exception => e
      puts "Error Message: #{e.mesasge}" 
    end
  end

  desc "urgent_ord"
  task :urgent_ord => :environment do |t, args|
    begin


      


      export_file = 'ord.xls'
      File.delete(export_file) if File.exist?(export_file)

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "comps"

      #-------------------------------------

      workbook = Roo::Excel.new('nw.xls')

      workbook.default_sheet = workbook.sheets[0]

      columns = %w{id zillow_id street_address mls_number status avm_rental avm_sale is_back_on_market back_on_market bathrooms beds calculated_capex cdom compgrade county dom garage_capacity elementary_school high_school middle_school Label legal_subdivision list_price lot_square_feet max_cap_offer max_capex max_ltv_offer max_price min_cap_rate min_capex offer_cap_rate offer_to_list_price pool_private sale_price sale_price_psf price_reduction category rental_cap_rate rental_class rental_max_purch_to_lp_ratio rental_max_purch_price rental_sold_count tax_id listing_broker_id listing_broker_name selling_agent_name}

      i = 0
      ary = []
      ((workbook.first_row + 1)..workbook.last_row).each do |row|
        hsh = {}
        
        j = 0

        property = Property.find(workbook.row(row)[0])
        pclass = "invitation_proforma"
        sps = pclass.classify.constantize.where(["user_id = ? and property_id = ?", 21, property.id])
        
        
        pf = nil
        if sps  
          if sps.count == 0
            pf = pclass.classify.constantize.new(user_id: 21, property_id: property.id)
          else
            pf = sps.first
          end
        end

        rental_avm = property.send("comps_rental_stats").format(:moderate_avm)
        sale_avm = property.send("comps_sale_stats").format(:expanded_avm)
        rental_sold_count =    property.send("comps_rental_stats").format(:sold_count)
        Property.set_user_id(21)
        label = property.user_assigned_tag.property_tag.name rescue nil


        puts "id: #{property.id} p: #{property.address} rental_avm: #{rental_avm} sale_avm: #{sale_avm}"
        columns.each do |w|
          case j
          when 5
            hsh[w] = rental_avm
          when 6
            hsh[w] = sale_avm
          when 11
            hsh[w] = pf.rental_output_renovation_calculated
          when 20
            hsh[w] = label
          when 24
            hsh[w] = pf.rental_output_max_purchase_amount 
          when 25
            hsh[w] = pf.rental_output_max_renovation_amount
          when 26
            hsh[w] = pf.rental_output_max_ltv_offer
          when 27
            hsh[w] = pf.rental_output_max_price
          when 28
            hsh[w] = pf.rental_output_minimum_cap_rate
          when 29
            hsh[w] = pf.rental_output_minimum_capex
          when 30
            hsh[w] = pf.rental_output_cap_rate
          when 31
            hsh[w] = pf.rental_output_max_purchase_ratio
          when 37
            hsh[w] = pf.definition.return_minimum_cap_rate
          when 39
            hsh[w] = pf.rental_output_max_purchase_ratio
          when 40
            hsh[w] = pf.rental_output_max_purchase_amount
          when 41
            hsh[w] = rental_sold_count 
          when 43
            hsh[w] = property.listing_broker_id
          when 44
            hsh[w] = property.listing_broker_name
          else
            hsh[w] = workbook.row(row)[j]
          end
          j += 1
        end
        
        columns.each do |d|
          sheet1.row(i).push hsh[d] 
        end
        
        # break if i >= 20
        i += 1
      end

      book.write export_file 
      
    rescue Exception => ex
      Delayed::Worker.logger.debug " error #{ex.message}"
    end
  end
  
  
  
end
