namespace :geocode do

  desc "test google"
  task :ord_rsold07 => :environment do |t, args|
    begin
      weight = 2450
      agent = Mechanize.new
      agent.user_agent = "ruby/net::http"
      agent.pluggable_parser.default = Mechanize::Page

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "sheet1"

      if t.name =~ /:/
        
        input_file_name = t.name.split(":")[1]
        
      else
        input_file_name = t.name
      end
      puts "file name #{input_file_name}"
      workbook = Roo::Excelx.new("#{input_file_name}.xlsx")

      export_file = "out#{input_file_name[1,1]}.xls"
      File.delete(export_file) if File.exist?(export_file)

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      j = 0
      ary = []
      count = 0
      arr = []
      
      st = 0
      3.times do |idx|
        arr[idx] = Thread.new {
          # sleep(rand(0)/10.0)
          Thread.current["mycount"] = count
          
          proxs = ['23.95.235.25','192.3.55.166','155.94.221.34']
          # prop_address = addresses[idx % 10]
          # coord = get_coordinate(prop_address,agent,'23.95.235.36',i)
          # puts "coord #{coord}"
          st = 2450 * idx
          (st..(st + 2449)).each do |row|
            hsh = {}
            prop_id = workbook.row(row)[0]
            prop_address = workbook.row(row)[1]
    
            
            coord = get_coordinate(prop_address,agent,proxs[idx],row)
            
            if !coord.try(:[],"lat").blank?
              prop = Property.find(prop_id)
              prop.latitude = coord.try(:[],"lat")
              prop.longitude = coord.try(:[],"lng")
              prop.send(:fetch_zestimate, true)
    
              prop.updateSchoolInfo(true)
              prop.save(validate: false)
              puts "idx: #{row} prop #{prop.id} saved"
            end
            
            puts "coord #{coord}"
    
            sleep 1.0/15.0
    
            # set_hash(hsh,:prop_id,prop_id)
            # set_hash(hsh,:prop_address,prop_address)
            # set_hash(hsh,:lat,coord.try(:[],"lat"))
            # set_hash(hsh,:lng,coord.try(:[],"lng"))
    
            # cols = %w{prop_id prop_address lat lng}
    
            # cols.each do |d|
              # puts "d: #{d} :#{hsh[d]}"
              # sheet1.row(i).push hsh[d]
            # end
    
            i += 1
            if (i % weight == 0)
              j = (i / weight)
            end
          end
          
          count += 1
        }
      end
      
      arr.each {|t| t.join }
      puts "finished"

      # book.write export_file

    rescue Exception => ex

      puts " error #{ex.message}"
    end
  end



  def get_coordinate(address,ag,proxy,idx)
    google_lat = nil
    google_lng = nil
    begin
      agent = ag

      a1 = Time.now
      agent.set_proxy(proxy,80,'glt','gltproxy')
      page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}")
      a2 = Time.now

      begin
        json = page.body
        dec = ActiveSupport::JSON.decode json
        google_lat =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat")
        google_lng =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng")
        status =  dec.try(:[],"status")
        puts "~~ geocoding #{idx} #{address}: lat: #{google_lat} lng: #{google_lng} using proxy: #{proxy} idx: #{idx} time elapsed: #{(a2 - a1).to_s} status: #{status}"
        {"lat" => google_lat, "lng" => google_lng}
      rescue Exception => ex
         puts  "geocode error #{ex.message}"
      end

    rescue Mechanize::ResponseCodeError => e

      case e.message
        when /403/ then
          puts "Error 403: #{e.message}"
        when /400/ then
          puts "Error 404: #{e.message}"
        else
          puts "Error elsewhere: #{e.message}"
      end

    rescue Exception =>  e

      puts "Error #{e.message}"

    end
    {"lat" => google_lat, "lng" => google_lng}
  end

  def set_hash(hsh,key,val)
    hsh[key.to_s] = val
    # puts "#{key.to_s}"
  end

end
