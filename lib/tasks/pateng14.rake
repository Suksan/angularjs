namespace :geocode do

  desc "test google"
  task :pateng26 => :environment do |t, args|
    begin
      weight = 2450
      agent = Mechanize.new
      agent.user_agent = "ruby/net::http"
      agent.pluggable_parser.default = Mechanize::Page

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "sheet1"

      if t.name =~ /:/
        
        input_file_name = t.name.split(":")[1]
        
      else
        input_file_name = t.name
      end
      puts "file name #{input_file_name}"
      workbook = Roo::Excelx.new("#{input_file_name}.xlsx")

      export_file = "out#{input_file_name[1,1]}.xls"
      File.delete(export_file) if File.exist?(export_file)

      workbook.default_sheet = workbook.sheets[0]
      proxs = ['23.95.238.118','69.12.80.125','172.245.240.221','23.95.235.137','23.94.188.72','107.183.229.234','155.94.251.153','69.12.80.191','23.94.188.208','23.95.235.18','23.94.188.224','155.94.221.15','69.12.79.252','107.183.229.222','69.12.79.3','107.183.229.211','23.95.238.49','155.94.251.16','155.94.251.208','172.245.240.236','23.95.238.2','69.12.80.133','23.95.235.24','23.95.235.158','23.95.238.245','23.95.235.34','23.94.188.242','198.55.112.37','155.94.221.34','107.181.78.88','23.95.235.22','107.183.229.137','107.183.229.225','107.181.78.220','69.12.80.176','107.183.229.235','107.181.78.85','155.94.221.62','23.95.238.252','155.94.251.143','23.94.188.74','69.12.80.157','23.95.235.16','69.12.80.193','107.181.78.5','23.95.235.25','192.3.55.166']

      i = 0
      j = 0
      st = 2       #2
      j = st
      ary = []
      ((workbook.first_row )..workbook.last_row).each do |row|
        hsh = {}
        prop_id = workbook.row(row)[1]
        prop_address = workbook.row(row)[2]

        puts "idx: #{row} preparing sending request #{proxs[j]} : address : #{prop_address}"
        coord = get_coordinate1(prop_address,agent,proxs[j],i)
        
        
        prop = Property.find(prop_id)
        if !coord.try(:[],"lat").blank?
          prop.latitude = coord.try(:[],"lat")
          prop.longitude = coord.try(:[],"lng")
        end
        
        prop.save(validate: false)
        puts "idx: #{row} prop #{prop.id} saved"
      
      
        puts "coord #{coord}"  if !coord

        sleep 1.0/15.0

        # set_hash(hsh,:prop_id,prop_id)
        # set_hash(hsh,:prop_address,prop_address)
        # set_hash(hsh,:lat,coord.try(:[],"lat"))
        # set_hash(hsh,:lng,coord.try(:[],"lng"))

        # cols = %w{prop_id prop_address lat lng}

        # cols.each do |d|
          # puts "d: #{d} :#{hsh[d]}"
          # sheet1.row(i).push hsh[d]
        # end

        i += 1
        if (i % weight == 0)
          j  = (i  / weight)   + st
        end
      end

      # book.write export_file

    rescue Exception => ex

      puts " error #{ex.message}"
    end
  end



  def get_coordinate1(address,ag,proxy,idx)
    google_lat = nil
    google_lng = nil
    begin
      agent = ag

      a1 = Time.now
      agent.set_proxy(proxy,80,'glt','gltproxy')
      page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}")
      a2 = Time.now

      begin
        json = page.body
        dec = ActiveSupport::JSON.decode json
        google_lat =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat")
        google_lng =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng")
        status =  dec.try(:[],"status")

        puts "~~ geocoding #{idx} #{address}: lat: #{google_lat} lng: #{google_lng} using proxy: #{proxy} idx: #{idx} time elapsed: #{(a2 - a1).to_s} status: #{status}"
        puts "status: #{status}"
        
        {"lat" => google_lat, "lng" => google_lng}
      rescue Exception => ex
         puts  "geocode error #{ex.message}"
      end

    rescue Mechanize::ResponseCodeError => e

      case e.message
        when /403/ then
          puts "Error 403: #{e.message}"
        when /400/ then
          puts "Error 404: #{e.message}"
        else
          puts "Error elsewhere: #{e.message}"
      end

    rescue Exception =>  e

      puts "Error #{e.message}"

    end
    {"lat" => google_lat, "lng" => google_lng}
  end

  def set_hash(hsh,key,val)
    hsh[key.to_s] = val
    # puts "#{key.to_s}"
  end

end