require 'csv'

namespace :data do
  desc "Import data CSV file"
  task :import => :environment do
    Market.all.each do |market|
      file_path = Rails.root.join('db').join("#{market.mls_code}_dump.csv")
      market.properties.to_csv(columns, file_path)
    end
  end

  desc "Export all market properties to CSV"
  task :export => :environment do
    columns = [:id] + Property.active_attributes(calc: false, delegates: PropertyDetail).select do |attr|
      attr.to_s.match(/^(zillow|pshark|comps|latitude|longitude)/).nil?
    end
    Market.all.each do |market|
      out_path = Rails.root.join('db').join("#{market.mls_code}_dump.csv")
      market.properties.includes(:detail).to_csv(columns, out_path)
    end
  end
end