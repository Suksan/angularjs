namespace :i18n do
  desc 'Update translation file with excel dataset'
  task :update do |t, args|
    locales_dir = Rails.root.join('config').join('locales')
    translations_path = locales_dir.join('en.yml')
    translations = YAML::load_file(translations_path)
    
    attrs = {}

    book = Spreadsheet.open(locales_dir.join('labels.xls'))
    for sheet in book.worksheets
      model_attrs = {}
      headers = sheet.row(0).to_a
      sheet.each_with_index do |row, index|
        next if index == 0
        values = Hash[headers.zip(row.to_a)]
        id = values.delete('id')
        model_attrs[id] = values
      end 
      attrs[sheet.name.underscore] = model_attrs
    end

    translations['en']['activerecord']['attributes'] = attrs

    File.open(translations_path, 'w') do |out|
      out.write translations.to_yaml
    end

  end
end