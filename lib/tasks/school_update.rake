task :update_school => :environment do
  p "converting start time --- #{Time.new.inspect}"    
  
  query = "UPDATE properties p
                  INNER JOIN property_details d
                       ON p.detail_id = d.id
          SET p.elementary_school = d.elementary_school,
              p.middle_school = d.middle_school,
             p.high_school = d.high_school"
   ActiveRecord::Base.connection.execute(query);
        
   p "converting end time --- #{Time.new.inspect}"
end