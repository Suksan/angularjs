task :listing_date_update => :environment do
  p "converting start time --- #{Time.new.inspect}"    
  
  # Property.where("id between :id1 and :id2", id1: 330000, id2: 351000).find_each(:batch_size => 1500) do |p|  
  Property.find_each(:batch_size => 1500) do |p|
    p.listing_date = p.detail._raw[:listing_date]
    p.closed_date = p.detail._raw[:closed_date]
    p.pending_date = p.detail._raw[:pending_date]
    p.termination_date = p.detail._raw[:termination_date]
    p.withdrawn_date = p.detail._raw[:withdrawn_date]
    p.expiration_date = p.detail._raw[:expiration_date]
    p.save
    puts "p.id #{p.id}"
   end
        
   p "converting end time --- #{Time.new.inspect}"
end