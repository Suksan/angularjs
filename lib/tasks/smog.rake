desc "smog"
task :smog => :environment do |t, args|
  begin
    
    threads = []
    tn = []
    tn = ["one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen"]
    market_id = 10
    volume = 0
    rows_per_chunk = 16
          
    workbook = Roo::Excelx.new('soom.xlsx')
    
    workbook.default_sheet = workbook.sheets[0]
    
    i = 0
    ary = []
    datas = []
    
    j = 0
    
    
    count = (workbook.last_row.to_d / rows_per_chunk.to_d).ceil
    
    if count > 0 
      selected = tn[0..(count-1)]
      selected.each do |number| 
        
         
        puts "number: #{number}"
        puts "loop: #{i}"
        idx = i + 1
        datas = []
        st = rows_per_chunk * (idx - 1) + 1
        if workbook.last_row > (rows_per_chunk * (idx - 1) + rows_per_chunk) 
          en = rows_per_chunk * (idx - 1) + rows_per_chunk
        else
          en = workbook.last_row
        end 
        
        ((st)..(en)).each do |row|
          data = workbook.row(row)[0].try(:to_s).try(:to_i)
          datas << data
        end
        
        token = datas.reject(&:blank?).join(" ")
        
        threads << Thread.new do 
          Rake.application.invoke_task("#{number}[#{market_id},'#{token}']")
        end
        
        i += 1
      end
    end
  

    threads.each { |thread| thread.join } 
    
    
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

def lookup(market_id,ids)

    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          Delayed::Worker.logger.debug "computed successfully"
          
        else
          Delayed::Worker.logger.debug "compute failed"
          
        end
         
        
      end
    end
end

task :one,[:market_id,:ids] => :environment do |t, args| 
  begin
    puts "task one"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end
 
task :two,[:market_id,:ids] => :environment do |t, args| 
 begin
    puts "task two"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end
 
task :three,[:market_id,:ids] => :environment do |t, args| 
 begin
    puts "task three"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end


task :four,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task four"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :five,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task five"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :six,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task six"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :seven,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task seven"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :eight,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task eight"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end


task :nine,[:market_id,:ids] => :environment do |t, args| 
  begin
    puts "task nine"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end


task :ten,[:market_id,:ids] => :environment do |t, args| 
  begin
    puts "task ten"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :eleven,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task eleven"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :twelve,[:market_id,:ids] => :environment do |t, args| 
  begin
    puts "task twelve"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :thirteen,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task thirteen"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end

task :fourteen,[:market_id,:ids] => :environment do |t, args| 
   begin
    puts "task fourteen"
    market_id = Integer(args.market_id)
    args.with_defaults(:ids => nil)
    ids = args.ids
      
    
    ary = ids.split(" ")
    
    market = Market.find(market_id)
    puts "aux invoked market #{market_id}"
    puts "token = #{ids}"
    
    
    if ary && ary.length > 0
      market = Market.find(market_id)
      market.properties.where(id: ary).order("id desc").find_each do |p|
        
        p "zZZZZzzzzzzz get #{p.id} computed zZZZZZZZZZZZZzzz"
        
        if p.compute2(true)
          puts  "computed successfully"
          
        else
          puts "compute failed"
          
        end
         
        
      end
    end
  rescue Exception => ex
    puts " error #{ex.message}"
  end
end