namespace :services do

  desc "restock google"
  task :restock_google => :environment do |t, args|
    begin
            

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "sheet1"

      if t.name =~ /:/
        
        input_file_name = t.name.split(":")[1]
        
      else
        input_file_name = t.name
      end
      puts "file name #{input_file_name}"
      workbook = Roo::Excelx.new("#{input_file_name}.xlsx")

      # export_file = "out#{input_file_name[1,1]}.xls"
      # File.delete(export_file) if File.exist?(export_file)

      workbook.default_sheet = workbook.sheets[0]

      Service.delete_all(code: 'google_geocoding')
      i = 0
      j = 0
      ary = []
      ((workbook.first_row )..workbook.last_row).each do |row|
        hsh = {}
        ip = workbook.row(row)[0]
        
        srv = Service.new(code: 'google_geocoding', login: "user#{row.to_s.rjust(2, "0")}", password: 'password',key: ip,
daily_limit: 2450, daily_calls: 0, too_many_attempts: 0, error_count: 0)
        
        srv.save
        puts "idx: #{row} ip: #{ip} "
      
      end

     
    rescue Exception => ex

      puts " error #{ex.message}"
    end
  end



  def get_coordinate1(address,ag,proxy,idx)
    google_lat = nil
    google_lng = nil
    begin
      agent = ag

      a1 = Time.now
      agent.set_proxy(proxy,80,'glt','gltproxy')
      page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}")
      a2 = Time.now

      begin
        json = page.body
        dec = ActiveSupport::JSON.decode json
        google_lat =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat")
        google_lng =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng")
        status =  dec.try(:[],"status")

        puts "~~ geocoding #{idx} #{address}: lat: #{google_lat} lng: #{google_lng} using proxy: #{proxy} idx: #{idx} time elapsed: #{(a2 - a1).to_s} status: #{status}"
        puts "status: #{status}"
        
        {"lat" => google_lat, "lng" => google_lng}
      rescue Exception => ex
         puts  "geocode error #{ex.message}"
      end

    rescue Mechanize::ResponseCodeError => e

      case e.message
        when /403/ then
          puts "Error 403: #{e.message}"
        when /400/ then
          puts "Error 404: #{e.message}"
        else
          puts "Error elsewhere: #{e.message}"
      end

    rescue Exception =>  e

      puts "Error #{e.message}"

    end
    {"lat" => google_lat, "lng" => google_lng}
  end

  def set_hash(hsh,key,val)
    hsh[key.to_s] = val
    # puts "#{key.to_s}"
  end

end