desc 'Generate a random password'
task :password, [:length] do |t, args|
  require 'securerandom'
  n = (args.length || '16').to_i / 2
  puts SecureRandom.hex(n)
end