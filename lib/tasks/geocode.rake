namespace :geocode do

  desc "test google"
  task :test_google => :environment do |t, args|
    begin
      weight = 2450
      agent = Mechanize.new
      agent.user_agent = "ruby/net::http"
      agent.pluggable_parser.default = Mechanize::Page

      Spreadsheet.client_encoding = 'UTF-8'

      book = Spreadsheet::Workbook.new
      sheet1 = book.create_worksheet :name => "sheet1"

      input_file_name = "d2"
      workbook = Roo::Excelx.new("#{input_file_name}.xlsx")

      export_file = "out#{input_file_name[1,1]}.xls"
      File.delete(export_file) if File.exist?(export_file)

      workbook.default_sheet = workbook.sheets[0]

      i = 0
      j = 0
      ary = []
      ((workbook.first_row )..workbook.last_row).each do |row|
        hsh = {}
        prop_id = workbook.row(row)[0]
        prop_address = workbook.row(row)[1]

        proxs = ['69.12.72.94','155.94.129.110','155.94.251.212','107.181.78.50','23.94.188.160','23.95.235.37','23.95.235.29','23.95.238.166','107.183.229.232','23.95.238.28','155.94.251.129','155.94.129.241','69.12.80.16','155.94.251.135','69.12.79.222','107.183.229.239','107.181.78.164','155.94.251.151','23.95.238.12','155.94.221.126','107.183.229.228','23.95.235.41','69.12.80.246','23.94.188.70','23.95.235.38','155.94.251.28','23.94.188.24','198.55.112.125','155.94.251.179','69.12.91.158','155.94.221.129','107.183.229.185','23.95.238.231','155.94.251.213','107.181.78.90','155.94.128.95','155.94.221.191','23.95.238.174','198.55.112.72','107.181.78.41','198.55.112.68','155.94.251.157','23.95.235.21','69.12.79.8','107.183.229.236','155.94.221.251','69.12.80.151','23.94.188.222','155.94.251.185','107.183.229.237','172.245.240.237','23.95.238.237','69.12.79.179','69.12.80.183','23.94.188.68','155.94.129.16','69.12.80.186','23.95.238.27','23.95.238.250','107.183.229.233','69.12.79.225','107.183.229.226','198.55.112.22','69.12.80.139','23.95.238.116','198.55.112.102','107.181.78.23','23.95.235.42','69.12.72.248','23.95.238.238','155.94.251.34','23.94.188.73','155.94.251.206','23.94.188.26','23.94.188.60','23.95.238.198','107.181.78.80','155.94.251.201','23.95.238.171','23.94.188.53','23.94.188.54','155.94.251.111','155.94.251.166','155.94.221.180','69.12.79.248','23.94.188.57','155.94.129.248','107.181.78.161','155.94.129.33','107.183.229.230','155.94.129.137','23.94.188.56','107.181.78.198','23.95.238.10','23.95.238.14','23.95.235.19','69.12.80.234','23.94.188.42','23.94.188.228','69.12.91.132','172.245.240.252','107.183.229.139','69.12.80.120','155.94.251.30','69.12.80.19','23.95.238.36','23.95.235.27','198.55.112.16','198.55.112.74','23.95.235.36','23.95.235.17','69.12.91.167','198.55.112.23','198.55.112.110','69.12.79.189','23.95.235.20','69.12.80.157','23.95.235.16','69.12.91.113','69.12.80.193','107.181.78.5','23.95.235.25','192.3.55.166','155.94.221.34','107.181.78.88','23.95.235.22','107.183.229.137','107.183.229.225','107.181.78.220','69.12.80.176','107.183.229.235','107.181.78.85','155.94.221.62','23.95.238.252','155.94.251.143','23.94.188.74','107.181.78.203','23.95.238.118','69.12.80.125','172.245.240.221','69.12.91.190','23.95.235.137','23.94.188.72','107.183.229.234','107.183.229.217','155.94.251.153','69.12.91.150','69.12.80.191','23.94.188.208','23.95.235.18','23.94.188.224','155.94.221.15','69.12.79.252','107.183.229.222','69.12.79.3','107.183.229.211','107.181.78.249','23.95.238.49','155.94.251.16','155.94.251.208','172.245.240.236','23.95.238.2','69.12.80.133','23.95.235.24','23.95.235.158','69.12.91.145','69.12.91.194','23.95.238.245','69.12.91.123','23.95.235.34','23.94.188.242','198.55.112.37']
        coord = get_coordinate(prop_address,agent,proxs[j],i)
        
        if !coord.try(:[],"lat").blank?
          prop = Property.find(prop_id)
          prop.latitude = coord.try(:[],"lat")
          prop.longitude = coord.try(:[],"lng")
          prop.send(:fetch_zestimate, true)

          prop.updateSchoolInfo(true)
          prop.save(validate: false)
          puts "idx: #{row} prop #{prop.id} saved"
        end
        
        puts "coord #{coord}"

        sleep 1.0/10.0

        # set_hash(hsh,:prop_id,prop_id)
        # set_hash(hsh,:prop_address,prop_address)
        # set_hash(hsh,:lat,coord.try(:[],"lat"))
        # set_hash(hsh,:lng,coord.try(:[],"lng"))

        # cols = %w{prop_id prop_address lat lng}

        # cols.each do |d|
          # puts "d: #{d} :#{hsh[d]}"
          # sheet1.row(i).push hsh[d]
        # end

        i += 1
        if (i % weight == 0)
          j = (i / weight)
        end
      end

      # book.write export_file

    rescue Exception => ex

      puts " error #{ex.message}"
    end
  end



  def get_coordinate(address,ag,proxy,idx)
    google_lat = nil
    google_lng = nil
    begin
      agent = ag

      a1 = Time.now
      agent.set_proxy(proxy,80,'glt','gltproxy')
      page = agent.get("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}")
      a2 = Time.now

      begin
        json = page.body
        dec = ActiveSupport::JSON.decode json
        google_lat =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lat")
        google_lng =  dec.try(:[],"results").try(:[],0).try(:[],"geometry").try(:[],"location").try(:[],"lng")

        puts "~~ geocoding #{idx} #{address}: lat: #{google_lat} lng: #{google_lng} using proxy: #{proxy} idx: #{idx} time elapsed: #{(a2 - a1).to_s}"
        {"lat" => google_lat, "lng" => google_lng}
      rescue Exception => ex
         puts  "geocode error #{ex.message}"
      end

    rescue Mechanize::ResponseCodeError => e

      case e.message
        when /403/ then
          puts "Error 403: #{e.message}"
        when /400/ then
          puts "Error 404: #{e.message}"
        else
          puts "Error elsewhere: #{e.message}"
      end

    rescue Exception =>  e

      puts "Error #{e.message}"

    end
    {"lat" => google_lat, "lng" => google_lng}
  end

  def set_hash(hsh,key,val)
    hsh[key.to_s] = val
    # puts "#{key.to_s}"
  end

end
