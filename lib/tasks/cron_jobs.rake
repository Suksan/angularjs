namespace :cron_jobs do
  desc "Initialize dynamic cron_job schedule"
  task :initialize => :environment do |t, args|
    Market.all.each do |m|
      MarketCronJob.create!([
        {market_id: m.id, name: 'Tue', frequency_type: 'every_day_of_week', day_of_week: 'Tue', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
        {market_id: m.id, name: 'Wed', frequency_type: 'every_day_of_week', day_of_week: 'Wed', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0},
        {market_id: m.id, name: 'Thu', frequency_type: 'every_day_of_week', day_of_week: 'Thu', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
        {market_id: m.id, name: 'Fri', frequency_type: 'every_day_of_week', day_of_week: 'Fri', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
        {market_id: m.id, name: 'Sat', frequency_type: 'every_day_of_week', day_of_week: 'Sat', start_hour: 12, start_min: 0, mer_indicator: 'am',duration_hour: 7, duration_min: 0 },
        {market_id: m.id, name: 'Sat', frequency_type: 'every_day_of_week', day_of_week: 'Sat', start_hour: 5, start_min: 0, mer_indicator: 'pm',duration_hour: 17, duration_min: 0 },
        {market_id: m.id, name: 'Sun', frequency_type: 'every_day_of_week', day_of_week: 'Sun', start_hour: 5, start_min: 0, mer_indicator: 'pm',duration_hour: 14, duration_min: 0 }
      ])
    end
    # days = Integer(args.days) rescue 1
    # args.with_defaults(:daily => false)
    # daily = args.daily
    # Market.all.each do |market|
      # MarketGrabJob.new(market_id: market.id, days: days, daily: daily).program(priority: -1)
      # MarketComputeJob.new(market_id: market.id).program()
      # MarketEmailSearchesJob.new(market_id: market.id, limit: 1000, weekly: true).program()
    # end
  end
  
  
end