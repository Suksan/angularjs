class RedisCli
  

  attr_accessor :host,:port,:db,:url,:queue
  
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end  

  class Configuration
    attr_accessor :host,:port,:db,:queue

    def initialize
      
    end
  end 
  
  def initialize(options={})
    options = redis_wrapper_options(options)
    @host = options[:host]
    @port = options[:port]
    @db = options[:db]
    @queue = options[:queue]
  end

  def url
    @url ||= [[@host,@port].reject(&:blank?).join(":"),@db].reject(&:blank?).join("/") if @url.blank?
    # @url = [@url,@db].reject(&:blank?).join("/") if !@db.blank?
    Delayed::Worker.logger.debug "url>> #{@url}"
    @url       
  end

  def lpush(klass,args)
    redis = Redis.new(:url => url)
    msg = { 'class' => klass,'args' =>  [args],'jid' => SecureRandom.hex(12), 'retry' => true, 'enqueued_at' => Time.now.to_f }
    redis.lpush(@queue,JSON.dump(msg))
  end
  
  def lpushq(klass,q,args)
    redis = Redis.new(:url => url)
    msg = { 'class' => klass,'args' =>  [args],'jid' => SecureRandom.hex(12), 'retry' => true, 'enqueued_at' => Time.now.to_f }
    redis.lpush(q,JSON.dump(msg))
  end
  
  def hmset(key, *attrs)
    redis = Redis.new(:url => url)
    redis.hmset(key, attrs)
  end
  
  def mapped_hmset(key, hsh)
    redis = Redis.new(:url => url)
    redis.mapped_hmset(key, hsh)
  end
  
  def hgetall(key)
    redis = Redis.new(:url => url)
    redis.hgetall(key)
  end
  
  def lrange(key,start,stop)
    redis = Redis.new(:url => url)
    redis.lrange(key,start,stop)
  end

  def redis_wrapper_options(overrides = {})      
    {
      :host  => RedisCli.configuration.host,
      :port => RedisCli.configuration.port,    
      :db => RedisCli.configuration.db,        
      :queue   => RedisCli.configuration.queue
      
    }.merge(overrides || {}).delete_if { |key, value| value.nil? }
  end
  
  

end