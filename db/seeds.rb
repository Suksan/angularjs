# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
admin = User.new(firstname: 'Admin', email: 'greenlight@greenletinv.com', password: 'test123', password_confirmation: 'test123')
admin.toggle(:admin)
admin.save(validate: false)

level2 = File.open(Rails.root.join('db', 'level2.xls'))

har = Market.new(name: 'Houston', state: 'TX', mls_code: 'har', mls_login: 'bigrob', mls_password: 'hou0804')
har.level2 = level2
har.save(validate: false)

sabor = Market.new(name: 'San Antonio', state: 'TX', mls_code: 'sabor', mls_login: '267407', mls_password: 'sana0804!@')
har.level2 = level2
sabor.save(validate: false)

har.users << admin
sabor.users << admin

if Rails.env.development?
  Service.create!([
    { code: 'property_shark', login: 'martin@mlkay.com', password: 'b02d26' },
    { code: 'google_geocoding', daily_limit: 2500 },
    { code: 'zillow', login: 'zillow18@greenletinv.com', password: 'zillow', key: 'X1-ZWz1dje0wjjrij_3qukz', daily_limit: 999 },
    { code: 'zillow', login: 'zillow19@greenletinv.com', password: 'zillow', key: 'X1-ZWz1djdwyhbnkb_3o1g1', daily_limit: 999 },
    { code: 'zillow', login: 'zillow20@greenletinv.com', password: 'zillow', key: 'X1-ZWz1bh4jzyb9xn_3pg0i', daily_limit: 999 }
  ])

else
  
  Service.create!([
    { code: 'property_shark', login: 'martin@mlkay.com', password: 'b02d26' },
    { code: 'zillow', login: 'martin@mlkay.com', password: 'res4927!', key: 'X1-ZWz1bsiar0dhcb_1oefy', daily_limit: 1000 },
    { code: 'google_geocoding', daily_limit: 2500 }
  ])

end

Job.delete_all

###### TEMPORARY
analyst = User.new(firstname: 'Analyst', role: 'analyst', email: 'analyst@greenletinv.com', password: 'test123', password_confirmation: 'test123')
analyst.save(validate: false)
assessor = User.new(firstname: 'Assessor', role: 'assessor', email: 'assessor@greenletinv.com', password: 'test123', password_confirmation: 'test123')
assessor.save(validate: false)
har.users << analyst
har.users << assessor

if Rails.env.development?
  property = Property.create!({category: 'single_family', mls_number: '98343004', status: 'active', square_feet: 2169, year_built: 1962, list_price: 319000, tax_amount: 5521, tax_value: 210000, market_id: 2,
    street_number: '5810', street_name: 'Willowbend', city: 'Houston', state: 'TX', zip_code: '77074', bathrooms_full: 2, beds: 3, cdom: 1, 
    comps_sale_stats:   {level1: 227874.1537, level2: 255051.2306, level3: 336582.4612, level4: 382065.3783, level5: 404806.8368, level6: 427548.2954, adj_cdom_median: 10.5, sold_count: 26, sold_square_feet_median: 2432, sold_square_feet_max: 2771},
    comps_rental_stats: {level1: 1577.454545, level2: 1577.454545, level3: 1577.454545, level4: 1577.454545, level5: 1577.454545, level6: 1577.454545, adj_cdom_median: 69},
    comps_build_stats:  {level1: 830000, level2: 830000, level3: 830000, level4: 830000, level5: 830000, level6: 830000, adj_cdom_median: 64, sold_count: 1, sold_square_feet_median: 4600, sold_square_feet_max: 4600}
  })
  property.compute
end