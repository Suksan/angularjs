# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140510161134) do

  create_table "a", :force => true do |t|
    t.datetime "timestamp",                     :null => false
    t.boolean  "active",     :default => false, :null => false
    t.boolean  "is_current", :default => false, :null => false
  end

  add_index "a", ["timestamp"], :name => "TIMESTAMP_INDEX", :unique => true

  create_table "deleted_cron_jobs", :force => true do |t|
    t.integer  "market_id"
    t.string   "name"
    t.integer  "start_hour"
    t.integer  "start_min"
    t.integer  "duration_hour"
    t.integer  "duration_min"
    t.string   "mer_indicator"
    t.string   "frequency_type"
    t.string   "day_of_week"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "deleted_cron_jobs", ["market_id"], :name => "index_deleted_cron_jobs_on_market_id"

  create_table "deleted_restart_cron_jobs", :force => true do |t|
    t.integer  "market_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "deleted_restart_cron_jobs", ["market_id"], :name => "index_deleted_restart_cron_jobs_on_market_id"

  create_table "jobs", :force => true do |t|
    t.integer  "market_id"
    t.integer  "user_id"
    t.integer  "priority"
    t.integer  "attempts"
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "started_at"
    t.datetime "completed_at"
    t.integer  "results_count"
    t.integer  "errors_count"
    t.string   "status"
    t.string   "name"
    t.text     "info"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "jobs", ["market_id", "user_id"], :name => "index_jobs_on_market_id_and_user_id"
  add_index "jobs", ["market_id"], :name => "index_jobs_on_market_id"
  add_index "jobs", ["priority", "run_at"], :name => "index_jobs_on_priority_and_run_at"

  create_table "market_assignments", :force => true do |t|
    t.integer  "market_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "market_assignments", ["market_id", "user_id"], :name => "index_market_assignments_on_market_id_and_user_id"
  add_index "market_assignments", ["market_id"], :name => "index_market_assignments_on_market_id"
  add_index "market_assignments", ["user_id"], :name => "index_market_assignments_on_user_id"

  create_table "market_build_costs", :force => true do |t|
    t.integer  "source_id"
    t.string   "source_type"
    t.integer  "min"
    t.integer  "max"
    t.integer  "weeks"
    t.decimal  "cost",        :precision => 30, :scale => 10
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  add_index "market_build_costs", ["source_id", "source_type"], :name => "index_market_build_costs_on_source_id_and_source_type"

  create_table "market_cron_jobs", :force => true do |t|
    t.integer  "market_id"
    t.string   "name"
    t.integer  "start_hour"
    t.integer  "start_min"
    t.integer  "duration_hour"
    t.integer  "duration_min"
    t.string   "mer_indicator"
    t.string   "frequency_type"
    t.string   "day_of_week"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "market_cron_jobs", ["market_id"], :name => "index_market_cron_jobs_on_market_id"

  create_table "market_definitions", :force => true do |t|
    t.integer  "source_id"
    t.string   "source_type"
    t.decimal  "purchase_price_discount",                     :precision => 30, :scale => 20
    t.decimal  "purchase_commission_earned",                  :precision => 30, :scale => 20
    t.decimal  "purchase_bank_short_discount",                :precision => 30, :scale => 20
    t.decimal  "purchase_homeowner_short_discount",           :precision => 30, :scale => 20
    t.string   "rental_loan"
    t.decimal  "rental_annual_rent_increase",                 :precision => 30, :scale => 20
    t.decimal  "rental_annual_vacancy_rate",                  :precision => 30, :scale => 20
    t.decimal  "rental_annual_maintenance_expense",           :precision => 30, :scale => 20
    t.decimal  "rental_management_expense",                   :precision => 30, :scale => 20
    t.decimal  "rental_annual_expense_increase",              :precision => 30, :scale => 20
    t.integer  "rental_years_to_sale"
    t.string   "seller_finance_loan"
    t.decimal  "seller_finance_sale_time_discount",           :precision => 30, :scale => 20
    t.decimal  "seller_finance_down_payment",                 :precision => 30, :scale => 20
    t.decimal  "seller_finance_interest_rate",                :precision => 30, :scale => 20
    t.integer  "seller_finance_term"
    t.decimal  "seller_finance_realtor_fee",                  :precision => 30, :scale => 20
    t.decimal  "seller_finance_management_expense",           :precision => 30, :scale => 20
    t.integer  "seller_finance_years_to_sale"
    t.string   "rental_resale_loan"
    t.decimal  "rental_resale_marketing_expense",             :precision => 30, :scale => 10
    t.decimal  "rental_resale_closing_expense",               :precision => 30, :scale => 20
    t.decimal  "rental_resale_warranty_expense",              :precision => 30, :scale => 10
    t.decimal  "rental_resale_investor_cap_rate",             :precision => 30, :scale => 20
    t.string   "sale_loan"
    t.decimal  "sale_realtor_fee_closing_costs",              :precision => 30, :scale => 20
    t.decimal  "sale_seller_contribution",                    :precision => 30, :scale => 20
    t.string   "build_loan"
    t.decimal  "build_realtor_fee_closing_costs",             :precision => 30, :scale => 20
    t.decimal  "build_seller_contribution",                   :precision => 30, :scale => 20
    t.integer  "listing_prep_days"
    t.decimal  "listing_sale_comp_discount",                  :precision => 30, :scale => 20
    t.decimal  "listing_commission_income",                   :precision => 30, :scale => 20
    t.decimal  "listing_marketing_expense",                   :precision => 30, :scale => 10
    t.decimal  "listing_agent_expense",                       :precision => 30, :scale => 20
    t.string   "listing_partnership_loan"
    t.decimal  "listing_partnership_commission_income",       :precision => 30, :scale => 20
    t.decimal  "listing_partnership_construction_markup",     :precision => 30, :scale => 20
    t.decimal  "listing_partnership_profits_percent",         :precision => 30, :scale => 20
    t.decimal  "listing_partnership_marketing_legal_expense", :precision => 30, :scale => 10
    t.decimal  "listing_partnership_seller_contribution",     :precision => 30, :scale => 20
    t.decimal  "assignment_sale_time_discount",               :precision => 30, :scale => 20
    t.decimal  "assignment_investor_arv_sale_ltv",            :precision => 30, :scale => 20
    t.decimal  "assignment_marketing_expense",                :precision => 30, :scale => 10
    t.decimal  "assignment_closing_expense",                  :precision => 30, :scale => 10
    t.decimal  "lending_bridge_purchase_down_payment",        :precision => 30, :scale => 20
    t.decimal  "lending_bridge_renovation_down_payment",      :precision => 30, :scale => 20
    t.decimal  "lending_bridge_max_ltv",                      :precision => 30, :scale => 20
    t.decimal  "lending_bridge_interest_rate",                :precision => 30, :scale => 20
    t.decimal  "lending_bridge_points",                       :precision => 30, :scale => 20
    t.integer  "lending_bridge_term"
    t.decimal  "lending_bridge_closing_costs",                :precision => 30, :scale => 20
    t.boolean  "lending_bridge_interest_only"
    t.integer  "lending_bridge_amortization"
    t.decimal  "lending_short_down_payment",                  :precision => 30, :scale => 20
    t.decimal  "lending_short_max_ltv",                       :precision => 30, :scale => 20
    t.decimal  "lending_short_interest_rate",                 :precision => 30, :scale => 20
    t.decimal  "lending_short_points",                        :precision => 30, :scale => 20
    t.integer  "lending_short_term"
    t.decimal  "lending_short_closing_costs",                 :precision => 30, :scale => 20
    t.boolean  "lending_short_interest_only"
    t.integer  "lending_short_amortization"
    t.decimal  "lending_short2_down_payment",                 :precision => 30, :scale => 20
    t.decimal  "lending_short2_max_ltv",                      :precision => 30, :scale => 20
    t.decimal  "lending_short2_interest_rate",                :precision => 30, :scale => 20
    t.decimal  "lending_short2_points",                       :precision => 30, :scale => 20
    t.integer  "lending_short2_term"
    t.decimal  "lending_short2_closing_costs",                :precision => 30, :scale => 20
    t.boolean  "lending_short2_interest_only"
    t.integer  "lending_short2_amortization"
    t.decimal  "lending_long_down_payment",                   :precision => 30, :scale => 20
    t.decimal  "lending_long_max_ltv",                        :precision => 30, :scale => 20
    t.decimal  "lending_long_interest_rate",                  :precision => 30, :scale => 20
    t.decimal  "lending_long_points",                         :precision => 30, :scale => 20
    t.integer  "lending_long_term"
    t.decimal  "lending_long_closing_costs",                  :precision => 30, :scale => 20
    t.boolean  "lending_long_interest_only"
    t.integer  "lending_long_amortization"
    t.decimal  "general_company_marginal_tax_rate",           :precision => 30, :scale => 20
    t.decimal  "general_investor_marginal_tax_rate",          :precision => 30, :scale => 20
    t.decimal  "general_capital_gains_rate",                  :precision => 30, :scale => 20
    t.decimal  "general_improvement_ratio",                   :precision => 30, :scale => 20
    t.decimal  "general_years_depreciation",                  :precision => 30, :scale => 15
    t.decimal  "general_annual_insurance_expense",            :precision => 30, :scale => 20
    t.decimal  "general_annual_utility_expense",              :precision => 30, :scale => 10
    t.integer  "general_days_to_close"
    t.decimal  "general_appreciation_rate",                   :precision => 30, :scale => 20
    t.decimal  "return_minimum_cap_rate",                     :precision => 30, :scale => 20
    t.decimal  "return_minimum_roi",                          :precision => 30, :scale => 20
    t.decimal  "return_minimum_return_on_total_project",      :precision => 30, :scale => 10
    t.integer  "return_rental_comp_level"
    t.integer  "return_rental_sale_comp_level"
    t.integer  "return_seller_finance_comp_level"
    t.integer  "return_rental_resale_comp_level"
    t.integer  "return_sale_comp_level"
    t.integer  "return_build_comp_level"
    t.integer  "return_assignment_comp_level"
    t.decimal  "renovation_unspecified_sale_rehab",           :precision => 30, :scale => 10
    t.decimal  "renovation_unspecified_rental_rehab",         :precision => 30, :scale => 10
    t.datetime "created_at",                                                                  :null => false
    t.datetime "updated_at",                                                                  :null => false
    t.decimal  "general_tax_rate",                            :precision => 30, :scale => 20
  end

  add_index "market_definitions", ["source_id", "source_type"], :name => "index_market_definitions_on_source_id_and_source_type"

  create_table "market_renovation_rehabs", :force => true do |t|
    t.integer  "source_id"
    t.string   "source_type"
    t.integer  "min"
    t.integer  "max"
    t.decimal  "sale",        :precision => 30, :scale => 10
    t.decimal  "rental",      :precision => 30, :scale => 10
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  add_index "market_renovation_rehabs", ["source_id", "source_type"], :name => "index_market_renovation_rehabs_on_source_id_and_source_type"

  create_table "market_renovation_timelines", :force => true do |t|
    t.integer  "source_id"
    t.string   "source_type"
    t.decimal  "min",         :precision => 30, :scale => 10
    t.decimal  "max",         :precision => 30, :scale => 10
    t.integer  "weeks"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
  end

  add_index "market_renovation_timelines", ["source_id", "source_type"], :name => "index_market_renovation_timelines_on_source_id_and_source_type"

  create_table "markets", :force => true do |t|
    t.string   "name"
    t.string   "state"
    t.string   "code"
    t.string   "mls_code"
    t.string   "mls_login"
    t.string   "mls_password"
    t.string   "level2_file_name"
    t.string   "level2_content_type"
    t.integer  "level2_file_size"
    t.datetime "level2_updated_at"
    t.string   "level3_file_name"
    t.string   "level3_content_type"
    t.integer  "level3_file_size"
    t.datetime "level3_updated_at"
    t.text     "sale_comp_query"
    t.text     "rental_comp_query"
    t.text     "build_comp_query"
    t.text     "search_stats"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "my_props", :force => true do |t|
    t.integer  "property_id"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "my_props", ["property_id"], :name => "index_my_props_on_property_id"
  add_index "my_props", ["user_id"], :name => "index_my_props_on_user_id"

  create_table "notes", :force => true do |t|
    t.integer  "property_id"
    t.string   "content"
    t.integer  "posted_by"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "notes", ["posted_by"], :name => "index_notes_on_posted_by"
  add_index "notes", ["property_id"], :name => "index_notes_on_property_id"

  create_table "people", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "telephone"
    t.string   "email"
    t.string   "birth_date"
    t.string   "education"
    t.string   "first_education_level"
    t.string   "first_education_institute"
    t.string   "second_education_level"
    t.string   "second_education_institute"
    t.string   "position_required"
    t.string   "expected_salary"
    t.string   "current_employer"
    t.string   "gender"
  end

  create_table "proformas", :force => true do |t|
    t.integer  "property_id"
    t.integer  "user_id"
    t.text     "_overrides"
    t.decimal  "purchase_price",                                               :precision => 30, :scale => 10
    t.date     "purchase_date"
    t.decimal  "purchase_commission",                                          :precision => 30, :scale => 20
    t.decimal  "purchase_annual_appreciation",                                 :precision => 30, :scale => 20
    t.decimal  "renovation_rental_rehab",                                      :precision => 30, :scale => 10
    t.decimal  "renovation_sale_rehab",                                        :precision => 30, :scale => 10
    t.decimal  "renovation_avg_rehab",                                         :precision => 30, :scale => 10
    t.decimal  "renovation_rental_hold_cost",                                  :precision => 30, :scale => 10
    t.integer  "renovation_rental_hold_weeks"
    t.decimal  "renovation_rental_resale_cost",                                :precision => 30, :scale => 10
    t.integer  "renovation_rental_resale_weeks"
    t.decimal  "renovation_seller_finance_cost",                               :precision => 30, :scale => 10
    t.integer  "renovation_seller_finance_weeks"
    t.decimal  "renovation_sale_listing_partnership_cost",                     :precision => 30, :scale => 10
    t.integer  "renovation_sale_listing_partnership_weeks"
    t.decimal  "renovation_build_cost",                                        :precision => 30, :scale => 10
    t.integer  "renovation_build_weeks"
    t.decimal  "renovation_assignment_cost",                                   :precision => 30, :scale => 10
    t.decimal  "comparables_avm_rental",                                       :precision => 30, :scale => 10
    t.integer  "comparables_cdom_rental"
    t.decimal  "comparables_avm_rental_sale",                                  :precision => 30, :scale => 10
    t.integer  "comparables_cdom_rental_sale"
    t.decimal  "comparables_avm_rental_resale",                                :precision => 30, :scale => 10
    t.integer  "comparables_cdom_rental_resale"
    t.decimal  "comparables_avm_seller_finance",                               :precision => 30, :scale => 10
    t.integer  "comparables_cdom_seller_finance"
    t.decimal  "comparables_avm_sale",                                         :precision => 30, :scale => 10
    t.integer  "comparables_cdom_sale"
    t.decimal  "comparables_avm_build",                                        :precision => 30, :scale => 10
    t.integer  "comparables_cdom_build"
    t.decimal  "comparables_avm_listing",                                      :precision => 30, :scale => 10
    t.integer  "comparables_cdom_listing"
    t.decimal  "comparables_avm_listing_partnership",                          :precision => 30, :scale => 10
    t.integer  "comparables_cdom_listing_partnership"
    t.decimal  "comparables_avm_wholesale",                                    :precision => 30, :scale => 10
    t.integer  "comparables_cdom_wholesale"
    t.decimal  "owner_equity_ratio",                                           :precision => 30, :scale => 20
    t.decimal  "owner_equity_amount",                                          :precision => 30, :scale => 10
    t.decimal  "carrying_expenses_property_taxes",                             :precision => 30, :scale => 10
    t.decimal  "carrying_expenses_insurance",                                  :precision => 30, :scale => 10
    t.decimal  "carrying_expenses_utilities",                                  :precision => 30, :scale => 10
    t.decimal  "carrying_expenses_annual",                                     :precision => 30, :scale => 10
    t.decimal  "tax_rate_company_marginal",                                    :precision => 30, :scale => 20
    t.decimal  "tax_rate_investor_marginal",                                   :precision => 30, :scale => 20
    t.decimal  "tax_rate_capital_gains_rate",                                  :precision => 30, :scale => 20
    t.decimal  "tax_rate_improvement_ratio",                                   :precision => 30, :scale => 20
    t.decimal  "tax_rate_years_depreciation",                                  :precision => 30, :scale => 15
    t.date     "rental_income_completion_date"
    t.date     "rental_income_lease_up_date"
    t.decimal  "rental_income_monthly_gross_rent",                             :precision => 30, :scale => 10
    t.decimal  "rental_income_annual_gross_rent",                              :precision => 30, :scale => 10
    t.decimal  "rental_income_other_income",                                   :precision => 30, :scale => 10
    t.decimal  "rental_income_annual_vacancy_rate",                            :precision => 30, :scale => 20
    t.decimal  "rental_income_annual_vacancy",                                 :precision => 30, :scale => 10
    t.decimal  "rental_income_total_annual_income",                            :precision => 30, :scale => 10
    t.decimal  "rental_income_annual_rent_increase",                           :precision => 30, :scale => 20
    t.decimal  "rental_income_years_to_sale",                                  :precision => 30, :scale => 15
    t.decimal  "rental_expenses_management_fee",                               :precision => 30, :scale => 10
    t.decimal  "rental_expenses_repairs_maintenance",                          :precision => 30, :scale => 10
    t.decimal  "rental_expenses_other",                                        :precision => 30, :scale => 10
    t.decimal  "rental_expenses_annual_expense_increase",                      :precision => 30, :scale => 20
    t.decimal  "rental_expenses_sale_realtor_closing_expense",                 :precision => 30, :scale => 20
    t.string   "rental_financing_loan"
    t.decimal  "rental_financing_purchase_renovation",                         :precision => 30, :scale => 10
    t.decimal  "rental_financing_down_payment_rate",                           :precision => 30, :scale => 20
    t.decimal  "rental_financing_down_payment",                                :precision => 30, :scale => 10
    t.decimal  "rental_financing_commission_credit_rate",                      :precision => 30, :scale => 20
    t.decimal  "rental_financing_commission_credit",                           :precision => 30, :scale => 10
    t.decimal  "rental_financing_available_rate",                              :precision => 30, :scale => 20
    t.decimal  "rental_financing_available",                                   :precision => 30, :scale => 10
    t.decimal  "rental_financing_loan_amount",                                 :precision => 30, :scale => 10
    t.decimal  "rental_financing_loan_points_rate",                            :precision => 30, :scale => 20
    t.decimal  "rental_financing_loan_points",                                 :precision => 30, :scale => 10
    t.decimal  "rental_financing_total_down_payment",                          :precision => 30, :scale => 10
    t.decimal  "rental_financing_interest_rate",                               :precision => 30, :scale => 20
    t.integer  "rental_financing_term"
    t.boolean  "rental_financing_interest_only"
    t.decimal  "rental_financing_closing_costs_rate",                          :precision => 30, :scale => 20
    t.decimal  "rental_financing_closing_costs",                               :precision => 30, :scale => 10
    t.decimal  "rental_financing_monthly_payment",                             :precision => 30, :scale => 10
    t.decimal  "rental_output_purchase_expense",                               :precision => 30, :scale => 10
    t.decimal  "rental_output_renovation_expense",                             :precision => 30, :scale => 10
    t.decimal  "rental_output_hold_weeks",                                     :precision => 30, :scale => 15
    t.decimal  "rental_output_carrying_expenses",                              :precision => 30, :scale => 10
    t.decimal  "rental_output_lending_expenses",                               :precision => 30, :scale => 10
    t.decimal  "rental_output_commission_credit",                              :precision => 30, :scale => 10
    t.decimal  "rental_output_total_pre_lease_up_expenses",                    :precision => 30, :scale => 10
    t.decimal  "rental_output_total_investment",                               :precision => 30, :scale => 10
    t.decimal  "rental_output_annual_income",                                  :precision => 30, :scale => 10
    t.decimal  "rental_output_annual_operating_expenses",                      :precision => 30, :scale => 10
    t.decimal  "rental_output_annual_principal_interest",                      :precision => 30, :scale => 10
    t.decimal  "rental_output_total_annual_expenses",                          :precision => 30, :scale => 10
    t.decimal  "rental_output_cash_yield_amount",                              :precision => 30, :scale => 10
    t.decimal  "rental_output_avg_cash_yield_amount",                          :precision => 30, :scale => 10
    t.decimal  "rental_output_cap_rate",                                       :precision => 30, :scale => 20
    t.decimal  "rental_output_current_avm",                                    :precision => 30, :scale => 10
    t.decimal  "rental_output_ltv",                                            :precision => 30, :scale => 20
    t.date     "rental_output_projected_sale_date"
    t.decimal  "rental_output_projected_sale_price",                           :precision => 30, :scale => 10
    t.decimal  "rental_output_loan_payoff",                                    :precision => 30, :scale => 10
    t.decimal  "rental_output_realtor_fees_closing_expense",                   :precision => 30, :scale => 10
    t.decimal  "rental_output_seller_contributions",                           :precision => 30, :scale => 10
    t.decimal  "rental_output_total_expenses",                                 :precision => 30, :scale => 10
    t.decimal  "rental_output_equity_contribution",                            :precision => 30, :scale => 10
    t.decimal  "rental_output_return_on_sale_amount",                          :precision => 30, :scale => 10
    t.decimal  "rental_output_return_on_sale_ratio",                           :precision => 30, :scale => 20
    t.decimal  "rental_output_pre_tax_roi_amount",                             :precision => 30, :scale => 10
    t.decimal  "rental_output_pre_tax_roi_ratio",                              :precision => 30, :scale => 20
    t.decimal  "rental_output_post_tax_roi_amount",                            :precision => 30, :scale => 10
    t.decimal  "rental_output_post_tax_roi_ratio",                             :precision => 30, :scale => 20
    t.decimal  "rental_output_pre_tax_roe",                                    :precision => 30, :scale => 20
    t.decimal  "rental_output_post_tax_roe",                                   :precision => 30, :scale => 20
    t.decimal  "rental_output_max_renovation_amount",                          :precision => 30, :scale => 10
    t.decimal  "rental_output_max_renovation_ratio",                           :precision => 30, :scale => 20
    t.decimal  "rental_output_max_purchase_amount",                            :precision => 30, :scale => 10
    t.decimal  "rental_output_max_purchase_ratio",                             :precision => 30, :scale => 20
    t.date     "seller_finance_income_completion_date"
    t.date     "seller_finance_income_sale_date"
    t.decimal  "seller_finance_income_sale_price",                             :precision => 30, :scale => 10
    t.decimal  "seller_finance_income_down_payment_rate",                      :precision => 30, :scale => 20
    t.decimal  "seller_finance_income_down_payment",                           :precision => 30, :scale => 10
    t.decimal  "seller_finance_income_loan_amount",                            :precision => 30, :scale => 10
    t.decimal  "seller_finance_income_interest_rate",                          :precision => 30, :scale => 20
    t.integer  "seller_finance_income_term"
    t.decimal  "seller_finance_income_monthly_payment",                        :precision => 30, :scale => 10
    t.decimal  "seller_finance_income_years_to_sale",                          :precision => 30, :scale => 15
    t.decimal  "seller_finance_expenses_realtor_closing_expense",              :precision => 30, :scale => 20
    t.decimal  "seller_finance_expenses_management_expense",                   :precision => 30, :scale => 10
    t.string   "seller_finance_financing_loan"
    t.decimal  "seller_finance_financing_purchase_renovation",                 :precision => 30, :scale => 10
    t.decimal  "seller_finance_financing_down_payment_purchase_rate",          :precision => 30, :scale => 20
    t.decimal  "seller_finance_financing_down_payment_purchase",               :precision => 30, :scale => 10
    t.decimal  "seller_finance_financing_commission_credit_rate",              :precision => 30, :scale => 20
    t.decimal  "seller_finance_financing_commission_credit",                   :precision => 30, :scale => 10
    t.decimal  "seller_finance_financing_total_down_payment",                  :precision => 30, :scale => 10
    t.decimal  "seller_finance_financing_loan_amount",                         :precision => 30, :scale => 10
    t.decimal  "seller_finance_financing_interest_rate",                       :precision => 30, :scale => 20
    t.decimal  "seller_finance_financing_loan_points_rate",                    :precision => 30, :scale => 20
    t.decimal  "seller_finance_financing_loan_points",                         :precision => 30, :scale => 10
    t.integer  "seller_finance_financing_term"
    t.boolean  "seller_finance_financing_interest_only"
    t.decimal  "seller_finance_financing_closing_costs_rate",                  :precision => 30, :scale => 20
    t.decimal  "seller_finance_financing_closing_costs",                       :precision => 30, :scale => 10
    t.decimal  "seller_finance_financing_monthly_payment",                     :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_sale_price",                             :precision => 30, :scale => 10
    t.date     "seller_finance_output_sale_date"
    t.decimal  "seller_finance_output_buyer_down_payment",                     :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_purchase_price",                         :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_renovation_expense",                     :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_hold_weeks",                             :precision => 30, :scale => 15
    t.decimal  "seller_finance_output_carrying_expenses",                      :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_lending_expenses",                       :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_realtor_closing_expense",                :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_total_pre_sale_expenses",                :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_equity_contribution",                    :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_annual_income",                          :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_annual_operating_expenses",              :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_annual_principal_interest",              :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_total_annual_expenses",                  :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_yearly_cash_yield_amount",               :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_cap_rate",                               :precision => 30, :scale => 20
    t.decimal  "seller_finance_output_loan_payoff",                            :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_return_on_sale",                         :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_pre_tax_roi_amount",                     :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_pre_tax_roi_ratio",                      :precision => 30, :scale => 20
    t.decimal  "seller_finance_output_post_tax_roi_amount",                    :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_post_tax_roi_ratio",                     :precision => 30, :scale => 20
    t.decimal  "seller_finance_output_pre_tax_roe_ratio",                      :precision => 30, :scale => 20
    t.decimal  "seller_finance_output_post_tax_roe_ratio",                     :precision => 30, :scale => 20
    t.decimal  "seller_finance_output_max_renovation_amount",                  :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_max_renovation_ratio",                   :precision => 30, :scale => 20
    t.decimal  "seller_finance_output_max_purchase_amount",                    :precision => 30, :scale => 10
    t.decimal  "seller_finance_output_max_purchase_ratio",                     :precision => 30, :scale => 20
    t.date     "rental_resale_income_completion_date"
    t.date     "rental_resale_income_sale_date"
    t.decimal  "rental_resale_income_sale_price",                              :precision => 30, :scale => 10
    t.decimal  "rental_resale_income_investor_cap_rate",                       :precision => 30, :scale => 20
    t.decimal  "rental_resale_expenses_marketing_expense",                     :precision => 30, :scale => 10
    t.decimal  "rental_resale_expenses_closing_expense",                       :precision => 30, :scale => 20
    t.decimal  "rental_resale_expenses_warranty_expenses",                     :precision => 30, :scale => 10
    t.string   "rental_resale_financing_loan"
    t.decimal  "rental_resale_financing_purchase_price",                       :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_renovation_price",                     :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_total_price",                          :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_down_payment_purchase_rate",           :precision => 30, :scale => 20
    t.decimal  "rental_resale_financing_down_payment_purchase",                :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_down_payment_renovation_rate",         :precision => 30, :scale => 20
    t.decimal  "rental_resale_financing_down_payment_renovation",              :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_commission_credit_rate",               :precision => 30, :scale => 20
    t.decimal  "rental_resale_financing_commission_credit",                    :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_loan_amount",                          :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_loan_points_rate",                     :precision => 30, :scale => 20
    t.decimal  "rental_resale_financing_loan_points",                          :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_total_down_payment",                   :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_total_available_rate",                 :precision => 30, :scale => 20
    t.decimal  "rental_resale_financing_total_available",                      :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_interest_rate",                        :precision => 30, :scale => 20
    t.integer  "rental_resale_financing_term"
    t.boolean  "rental_resale_financing_interest_only"
    t.decimal  "rental_resale_financing_closing_costs_rate",                   :precision => 30, :scale => 20
    t.decimal  "rental_resale_financing_closing_costs",                        :precision => 30, :scale => 10
    t.decimal  "rental_resale_financing_monthly_payment",                      :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_sale_price",                              :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_investor_cap_rate",                       :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_investor_ltv",                            :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_purchase_expense",                        :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_renovation_expense",                      :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_hold_weeks",                              :precision => 30, :scale => 15
    t.decimal  "rental_resale_output_carrying_expense",                        :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_lending_expense",                         :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_realtor_fees_closing_expense",            :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_seller_contributions",                    :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_total_expenses",                          :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_equity_contribution",                     :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_pre_tax_roi_amount",                      :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_pre_tax_roi_ratio",                       :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_post_tax_roi_amount",                     :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_post_tax_roi_ratio",                      :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_pre_tax_roe_ratio",                       :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_post_tax_roe_ratio",                      :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_max_renovation_amount",                   :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_max_renovation_ratio",                    :precision => 30, :scale => 20
    t.decimal  "rental_resale_output_max_purchase_amount",                     :precision => 30, :scale => 10
    t.decimal  "rental_resale_output_max_purchase_ratio",                      :precision => 30, :scale => 20
    t.date     "sale_income_completion_date"
    t.date     "sale_income_sale_date"
    t.decimal  "sale_income_sale_price",                                       :precision => 30, :scale => 10
    t.decimal  "sale_expenses_realtor_fees_closing_expense",                   :precision => 30, :scale => 20
    t.decimal  "sale_expenses_seller_contributions",                           :precision => 30, :scale => 10
    t.string   "sale_financing_loan"
    t.decimal  "sale_financing_purchase_price",                                :precision => 30, :scale => 10
    t.decimal  "sale_financing_renovation_price",                              :precision => 30, :scale => 10
    t.decimal  "sale_financing_total_price",                                   :precision => 30, :scale => 10
    t.decimal  "sale_financing_down_payment_purchase_rate",                    :precision => 30, :scale => 20
    t.decimal  "sale_financing_down_payment_purchase",                         :precision => 30, :scale => 10
    t.decimal  "sale_financing_down_payment_renovation_rate",                  :precision => 30, :scale => 20
    t.decimal  "sale_financing_down_payment_renovation",                       :precision => 30, :scale => 10
    t.decimal  "sale_financing_commission_credit_rate",                        :precision => 30, :scale => 20
    t.decimal  "sale_financing_commission_credit",                             :precision => 30, :scale => 10
    t.decimal  "sale_financing_total_available_rate",                          :precision => 30, :scale => 20
    t.decimal  "sale_financing_total_available",                               :precision => 30, :scale => 10
    t.decimal  "sale_financing_loan_amount",                                   :precision => 30, :scale => 10
    t.decimal  "sale_financing_interest_rate",                                 :precision => 30, :scale => 20
    t.decimal  "sale_financing_loan_points_rate",                              :precision => 30, :scale => 20
    t.decimal  "sale_financing_loan_points",                                   :precision => 30, :scale => 10
    t.decimal  "sale_financing_total_down_payment",                            :precision => 30, :scale => 10
    t.integer  "sale_financing_term"
    t.boolean  "sale_financing_interest_only"
    t.decimal  "sale_financing_closing_costs_rate",                            :precision => 30, :scale => 20
    t.decimal  "sale_financing_closing_costs",                                 :precision => 30, :scale => 10
    t.decimal  "sale_financing_monthly_payment",                               :precision => 30, :scale => 10
    t.decimal  "sale_output_sale_price",                                       :precision => 30, :scale => 10
    t.date     "sale_output_sale_date"
    t.decimal  "sale_output_purchase_expense",                                 :precision => 30, :scale => 10
    t.decimal  "sale_output_renovation_expense",                               :precision => 30, :scale => 10
    t.decimal  "sale_output_hold_weeks",                                       :precision => 30, :scale => 15
    t.decimal  "sale_output_carrying_expense",                                 :precision => 30, :scale => 10
    t.decimal  "sale_output_lending_expense",                                  :precision => 30, :scale => 10
    t.decimal  "sale_output_realtor_fees_closing_expense",                     :precision => 30, :scale => 10
    t.decimal  "sale_output_seller_contributions",                             :precision => 30, :scale => 10
    t.decimal  "sale_output_total_expenses",                                   :precision => 30, :scale => 10
    t.decimal  "sale_output_equity_contribution",                              :precision => 30, :scale => 10
    t.decimal  "sale_output_pre_tax_roi_amount",                               :precision => 30, :scale => 10
    t.decimal  "sale_output_pre_tax_roi_ratio",                                :precision => 30, :scale => 20
    t.decimal  "sale_output_post_tax_roi_amount",                              :precision => 30, :scale => 10
    t.decimal  "sale_output_post_tax_roi_ratio",                               :precision => 30, :scale => 20
    t.decimal  "sale_output_pre_tax_roe_ratio",                                :precision => 30, :scale => 20
    t.decimal  "sale_output_post_tax_roe_ratio",                               :precision => 30, :scale => 20
    t.decimal  "sale_output_max_renovation_amount",                            :precision => 30, :scale => 10
    t.decimal  "sale_output_max_renovation_ratio",                             :precision => 30, :scale => 20
    t.decimal  "sale_output_max_purchase_amount",                              :precision => 30, :scale => 10
    t.decimal  "sale_output_max_purchase_ratio",                               :precision => 30, :scale => 20
    t.date     "build_income_completion_date"
    t.date     "build_income_sale_date"
    t.decimal  "build_income_sale_price",                                      :precision => 30, :scale => 10
    t.decimal  "build_expenses_realtor_fees_closing_expense",                  :precision => 30, :scale => 20
    t.decimal  "build_expenses_seller_contributions",                          :precision => 30, :scale => 10
    t.string   "build_financing_loan"
    t.decimal  "build_financing_total_build_cost",                             :precision => 30, :scale => 10
    t.decimal  "build_financing_down_payment_rate",                            :precision => 30, :scale => 20
    t.decimal  "build_financing_down_payment",                                 :precision => 30, :scale => 10
    t.decimal  "build_financing_commission_credit_rate",                       :precision => 30, :scale => 20
    t.decimal  "build_financing_commission_credit",                            :precision => 30, :scale => 10
    t.decimal  "build_financing_total_down_payment",                           :precision => 30, :scale => 10
    t.decimal  "build_financing_total_available_rate",                         :precision => 30, :scale => 20
    t.decimal  "build_financing_total_available",                              :precision => 30, :scale => 10
    t.decimal  "build_financing_loan_amount",                                  :precision => 30, :scale => 10
    t.decimal  "build_financing_interest_rate",                                :precision => 30, :scale => 20
    t.decimal  "build_financing_loan_points_rate",                             :precision => 30, :scale => 20
    t.decimal  "build_financing_loan_points",                                  :precision => 30, :scale => 10
    t.integer  "build_financing_term"
    t.boolean  "build_financing_interest_only"
    t.decimal  "build_financing_closing_costs_rate",                           :precision => 30, :scale => 20
    t.decimal  "build_financing_closing_costs",                                :precision => 30, :scale => 10
    t.decimal  "build_financing_monthly_payment",                              :precision => 30, :scale => 10
    t.decimal  "build_output_sale_price",                                      :precision => 30, :scale => 10
    t.date     "build_output_sale_date"
    t.decimal  "build_output_total_build_expenses",                            :precision => 30, :scale => 10
    t.decimal  "build_output_hold_weeks",                                      :precision => 30, :scale => 15
    t.decimal  "build_output_carrying_expense",                                :precision => 30, :scale => 10
    t.decimal  "build_output_lending_expense",                                 :precision => 30, :scale => 10
    t.decimal  "build_output_realtor_fees_closing_expense",                    :precision => 30, :scale => 10
    t.decimal  "build_output_seller_contributions",                            :precision => 30, :scale => 10
    t.decimal  "build_output_total_expenses",                                  :precision => 30, :scale => 10
    t.decimal  "build_output_equity_contribution",                             :precision => 30, :scale => 10
    t.decimal  "build_output_pre_tax_roi_amount",                              :precision => 30, :scale => 10
    t.decimal  "build_output_pre_tax_roi_ratio",                               :precision => 30, :scale => 20
    t.decimal  "build_output_post_tax_roi_amount",                             :precision => 30, :scale => 10
    t.decimal  "build_output_post_tax_roi_ratio",                              :precision => 30, :scale => 20
    t.decimal  "build_output_pre_tax_roe_ratio",                               :precision => 30, :scale => 20
    t.decimal  "build_output_post_tax_roe_ratio",                              :precision => 30, :scale => 20
    t.decimal  "build_output_max_build_amount",                                :precision => 30, :scale => 10
    t.decimal  "build_output_max_build_ratio",                                 :precision => 30, :scale => 20
    t.decimal  "build_output_max_purchase_amount",                             :precision => 30, :scale => 10
    t.decimal  "build_output_max_purchase_ratio",                              :precision => 30, :scale => 20
    t.date     "listing_partnership_income_completion_date"
    t.date     "listing_partnership_income_sale_date"
    t.decimal  "listing_partnership_income_sale_price",                        :precision => 30, :scale => 10
    t.decimal  "listing_partnership_income_commission",                        :precision => 30, :scale => 20
    t.decimal  "listing_partnership_income_construction_markup",               :precision => 30, :scale => 20
    t.decimal  "listing_partnership_income_percent_of_profit",                 :precision => 30, :scale => 20
    t.decimal  "listing_partnership_expenses_marketing_legal",                 :precision => 30, :scale => 10
    t.decimal  "listing_partnership_expenses_seller_contributions",            :precision => 30, :scale => 10
    t.string   "listing_partnership_financing_loan"
    t.decimal  "listing_partnership_financing_renovation",                     :precision => 30, :scale => 10
    t.decimal  "listing_partnership_financing_down_payment_renovation_rate",   :precision => 30, :scale => 20
    t.decimal  "listing_partnership_financing_down_payment_renovation",        :precision => 30, :scale => 10
    t.decimal  "listing_partnership_financing_total_down_payment",             :precision => 30, :scale => 10
    t.decimal  "listing_partnership_financing_loan_amount",                    :precision => 30, :scale => 10
    t.decimal  "listing_partnership_financing_interest_rate",                  :precision => 30, :scale => 20
    t.decimal  "listing_partnership_financing_loan_points_rate",               :precision => 30, :scale => 20
    t.decimal  "listing_partnership_financing_loan_points",                    :precision => 30, :scale => 10
    t.integer  "listing_partnership_financing_term"
    t.boolean  "listing_partnership_financing_interest_only"
    t.decimal  "listing_partnership_financing_closing_costs_rate",             :precision => 30, :scale => 20
    t.decimal  "listing_partnership_financing_closing_costs",                  :precision => 30, :scale => 10
    t.decimal  "listing_partnership_financing_monthly_payment",                :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_sale_price",                        :precision => 30, :scale => 10
    t.date     "listing_partnership_output_sale_date"
    t.decimal  "listing_partnership_output_purchase_price",                    :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_renovation_expense",                :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_hold_weeks",                        :precision => 30, :scale => 15
    t.decimal  "listing_partnership_output_lending_expense",                   :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_marketing_legal_expense",           :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_seller_contributions",              :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_total_expenses",                    :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_equity_contribution",               :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_overall_return",                    :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_commission_income",                 :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_construction_reimbursement_income", :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_profit_participation",              :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_pre_tax_roi_amount",                :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_pre_tax_roi_ratio",                 :precision => 30, :scale => 20
    t.decimal  "listing_partnership_output_post_tax_roi_amount",               :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_post_tax_roi_ratio",                :precision => 30, :scale => 20
    t.decimal  "listing_partnership_output_pre_tax_roe_ratio",                 :precision => 30, :scale => 20
    t.decimal  "listing_partnership_output_post_tax_roe_ratio",                :precision => 30, :scale => 20
    t.decimal  "listing_partnership_output_max_renovation_amount",             :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_max_renovation_ratio",              :precision => 30, :scale => 20
    t.decimal  "listing_partnership_output_max_purchase_amount",               :precision => 30, :scale => 10
    t.decimal  "listing_partnership_output_max_purchase_ratio",                :precision => 30, :scale => 20
    t.date     "assignment_income_sale_date"
    t.decimal  "assignment_income_investor_arv_sale_ltv",                      :precision => 30, :scale => 20
    t.decimal  "assignment_income_sale_price",                                 :precision => 30, :scale => 10
    t.decimal  "assignment_expenses_marketing",                                :precision => 30, :scale => 10
    t.decimal  "assignment_expenses_closing",                                  :precision => 30, :scale => 10
    t.decimal  "assignment_output_sale_price",                                 :precision => 30, :scale => 10
    t.decimal  "assignment_output_acquisition_price",                          :precision => 30, :scale => 10
    t.decimal  "assignment_output_income",                                     :precision => 30, :scale => 10
    t.decimal  "assignment_output_marketing_expense",                          :precision => 30, :scale => 10
    t.decimal  "assignment_output_closing_expenses",                           :precision => 30, :scale => 10
    t.decimal  "assignment_output_total_expenses",                             :precision => 30, :scale => 10
    t.decimal  "assignment_output_pre_tax_roi_amount",                         :precision => 30, :scale => 10
    t.decimal  "assignment_output_post_tax_roi_amount",                        :precision => 30, :scale => 10
    t.decimal  "assignment_output_max_renovation_amount",                      :precision => 30, :scale => 10
    t.decimal  "assignment_output_max_renovation_ratio",                       :precision => 30, :scale => 20
    t.decimal  "assignment_output_max_purchase_amount",                        :precision => 30, :scale => 10
    t.decimal  "assignment_output_max_purchase_ratio",                         :precision => 30, :scale => 20
    t.date     "listing_income_listing_date"
    t.date     "listing_income_sale_date"
    t.decimal  "listing_income_sale_price",                                    :precision => 30, :scale => 10
    t.decimal  "listing_income_commission",                                    :precision => 30, :scale => 20
    t.decimal  "listing_expenses_marketing",                                   :precision => 30, :scale => 10
    t.decimal  "listing_expenses_agent",                                       :precision => 30, :scale => 20
    t.decimal  "listing_output_sale_price",                                    :precision => 30, :scale => 10
    t.decimal  "listing_output_income",                                        :precision => 30, :scale => 10
    t.decimal  "listing_output_marketing_expense",                             :precision => 30, :scale => 10
    t.decimal  "listing_output_agent_expense",                                 :precision => 30, :scale => 10
    t.decimal  "listing_output_total_expenses",                                :precision => 30, :scale => 10
    t.decimal  "listing_output_pre_tax_roi_amount",                            :precision => 30, :scale => 10
    t.decimal  "listing_output_post_tax_roi_amount",                           :precision => 30, :scale => 10
    t.datetime "created_at",                                                                                   :null => false
    t.datetime "updated_at",                                                                                   :null => false
    t.decimal  "carrying_expenses_maintenance",                                :precision => 30, :scale => 10
    t.decimal  "sale_expenses_total_price",                                    :precision => 30, :scale => 10
    t.decimal  "sale_output_total_price",                                      :precision => 30, :scale => 10
    t.decimal  "sale_output_commission_credit",                                :precision => 30, :scale => 10
    t.decimal  "rental_income_sale_price_current",                             :precision => 30, :scale => 10
    t.decimal  "rental_expenses_total_price",                                  :precision => 30, :scale => 10
    t.decimal  "rental_expenses_property_taxes",                               :precision => 30, :scale => 10
    t.decimal  "rental_expenses_insurance",                                    :precision => 30, :scale => 10
    t.decimal  "rental_expenses_seller_contributions",                         :precision => 30, :scale => 10
    t.decimal  "rental_financing_total_price",                                 :precision => 30, :scale => 10
    t.decimal  "rental_output_purchase",                                       :precision => 30, :scale => 10
    t.decimal  "rental_output_renovation",                                     :precision => 30, :scale => 10
    t.decimal  "rental_output_total_price",                                    :precision => 30, :scale => 10
    t.decimal  "rental_output_monthly_gross_rent",                             :precision => 30, :scale => 10
    t.decimal  "rental_sale_output_sale_price_current",                        :precision => 30, :scale => 10
    t.decimal  "rental_sale_output_total_sale_return",                         :precision => 30, :scale => 10
    t.decimal  "rental_output_noi",                                            :precision => 30, :scale => 10
    t.decimal  "rental_output_gross_yield",                                    :precision => 30, :scale => 10
    t.decimal  "rental_output_dscr",                                           :precision => 30, :scale => 10
    t.decimal  "build_expenses_purchase_price",                                :precision => 30, :scale => 10
    t.decimal  "build_recommended_size",                                       :precision => 30, :scale => 10
    t.decimal  "build_cost_sq_ft",                                             :precision => 30, :scale => 10
    t.decimal  "build_expenses_total_price",                                   :precision => 30, :scale => 10
    t.decimal  "build_financing_total_price",                                  :precision => 30, :scale => 10
    t.decimal  "build_output_purchase_expense",                                :precision => 30, :scale => 10
    t.decimal  "build_output_total_price",                                     :precision => 30, :scale => 10
    t.decimal  "build_output_commission_credit",                               :precision => 30, :scale => 10
    t.decimal  "sale_output_ltv",                                              :precision => 30, :scale => 20
    t.decimal  "rental_sale_output_total_pre_lease_up_expense",                :precision => 30, :scale => 10
    t.decimal  "sale_output_minimum_roi",                                      :precision => 30, :scale => 10
    t.decimal  "sale_output_minimum_return",                                   :precision => 30, :scale => 20
    t.decimal  "sale_output_max_renov_to_predict_ratio",                       :precision => 30, :scale => 10
    t.decimal  "sale_output_max_purchase_to_lp_ratio",                         :precision => 30, :scale => 10
    t.decimal  "build_output_minimum_roi",                                     :precision => 30, :scale => 10
    t.decimal  "build_output_minimum_cap_rate",                                :precision => 30, :scale => 20
    t.decimal  "build_output_max_construction_to_predict_ratio",               :precision => 30, :scale => 10
    t.decimal  "build_output_max_purchase_to_lp_ratio",                        :precision => 30, :scale => 10
    t.decimal  "rental_output_cash_yield_ratio",                               :precision => 30, :scale => 20
    t.decimal  "rental_output_avg_cash_yield_ratio",                           :precision => 30, :scale => 20
    t.decimal  "rental_output_minimum_cap_rate",                               :precision => 30, :scale => 10
    t.decimal  "rental_output_max_renov_to_predict_ratio",                     :precision => 30, :scale => 10
    t.decimal  "rental_output_max_purchase_to_lp_ratio",                       :precision => 30, :scale => 10
    t.decimal  "rental_output_annual_operating_expenses_percent",              :precision => 30, :scale => 10
    t.decimal  "rental_output_total_annual_expenses_percent",                  :precision => 30, :scale => 10
    t.decimal  "rental_output_noi_ratio",                                      :precision => 30, :scale => 10
  end

  add_index "proformas", ["property_id"], :name => "index_proformas_on_property_id"
  add_index "proformas", ["user_id"], :name => "index_proformas_on_user_id"

  create_table "properties", :force => true do |t|
    t.integer  "market_id"
    t.integer  "detail_id"
    t.text     "_overrides"
    t.integer  "beds"
    t.string   "category"
    t.integer  "cdom"
    t.string   "city"
    t.date     "closed_date"
    t.string   "county"
    t.date     "deed_date"
    t.string   "disclosures"
    t.integer  "dom"
    t.date     "expiration_date"
    t.string   "garage_capacity"
    t.string   "geo_market_area"
    t.string   "key_map_page"
    t.date     "last_sale_date"
    t.string   "legal_subdivision"
    t.decimal  "list_price",                   :precision => 30, :scale => 10
    t.string   "listing_agent_phone"
    t.string   "listing_agent_name"
    t.date     "listing_date"
    t.string   "lock_box_code"
    t.string   "location_key"
    t.string   "location_name"
    t.integer  "lot_square_feet"
    t.string   "mls_area_key"
    t.string   "mls_area_name"
    t.string   "mls_number"
    t.string   "mortgagee"
    t.decimal  "owner_estimated_loan_balance", :precision => 30, :scale => 10
    t.string   "owner_street_address"
    t.string   "owner_city"
    t.string   "owner_name"
    t.string   "owner_state"
    t.string   "owner_zip_code"
    t.string   "owner_zip_4"
    t.date     "pending_date"
    t.boolean  "pool_private"
    t.text     "realtor_remarks"
    t.string   "rental_class"
    t.string   "rental_type"
    t.decimal  "sale_price",                   :precision => 30, :scale => 10
    t.boolean  "selling_for_lot_value"
    t.string   "single_family_type"
    t.decimal  "sold_lease_price",             :precision => 30, :scale => 10
    t.integer  "square_feet"
    t.string   "street_direction"
    t.string   "street_name"
    t.integer  "street_number"
    t.string   "state"
    t.string   "status"
    t.string   "subdivision"
    t.string   "subdivision_section_number"
    t.decimal  "tax_amount",                   :precision => 30, :scale => 10
    t.string   "tax_id"
    t.decimal  "tax_market_value",             :precision => 30, :scale => 10
    t.decimal  "tax_rate",                     :precision => 30, :scale => 20
    t.decimal  "tax_value",                    :precision => 30, :scale => 10
    t.integer  "tax_year"
    t.date     "termination_date"
    t.integer  "year_built"
    t.string   "zip_4"
    t.string   "zip_code"
    t.date     "withdrawn_date"
    t.decimal  "sold_price",                   :precision => 30, :scale => 10
    t.decimal  "adj_sale_price",               :precision => 30, :scale => 10
    t.decimal  "adj_sale_price_psf",           :precision => 30, :scale => 10
    t.decimal  "list_price_psf",               :precision => 30, :scale => 10
    t.decimal  "sale_price_psf",               :precision => 30, :scale => 10
    t.decimal  "bathrooms",                    :precision => 30, :scale => 15
    t.string   "street_address"
    t.string   "zip"
    t.string   "city_state_zip"
    t.string   "address"
    t.string   "owner_zip"
    t.decimal  "latitude",                     :precision => 30, :scale => 15
    t.decimal  "longitude",                    :precision => 30, :scale => 15
    t.string   "zillow_id"
    t.decimal  "zillow_sale_estimate",         :precision => 30, :scale => 10
    t.decimal  "zillow_rental_estimate",       :precision => 30, :scale => 10
    t.datetime "zillow_updated_at"
    t.string   "pshark_id"
    t.datetime "pshark_updated_at"
    t.text     "comps_rental_stats"
    t.text     "comps_sale_stats"
    t.text     "comps_build_stats"
    t.datetime "comps_updated_at"
    t.string   "data_source"
    t.datetime "data_updated_at"
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
    t.string   "pshark_county"
    t.datetime "dom_updated_at"
    t.boolean  "auto_updatable"
    t.integer  "tag"
    t.string   "owner_phone"
    t.string   "owner_email"
    t.decimal  "offer_price",                  :precision => 30, :scale => 10
    t.string   "listing_agent_email"
    t.string   "rep"
    t.datetime "user_updated_at"
    t.string   "elementary_school"
    t.string   "middle_school"
    t.string   "high_school"
    t.integer  "elementary_school_rating"
    t.integer  "middle_school_rating"
    t.integer  "high_school_rating"
    t.datetime "school_updated_at"
    t.decimal  "avg_school_rating",            :precision => 30, :scale => 10
    t.integer  "walk_score"
    t.integer  "transit_score"
    t.decimal  "zillow_tax_amount",            :precision => 30, :scale => 10
    t.decimal  "appreciation_rate",            :precision => 30, :scale => 10
    t.string   "school_district"
    t.string   "exemption"
    t.string   "land_use_code"
    t.decimal  "last_ad_price",                :precision => 30, :scale => 20
    t.boolean  "owner_occupied"
    t.string   "map_facet"
    t.string   "land_use"
    t.string   "appraise_subdivision"
    t.datetime "mls_record_creation_date"
    t.datetime "mls_record_modified"
    t.string   "school_district_name"
    t.string   "mls_sub_area"
    t.string   "mls_sub_area_name"
    t.string   "mls_elementary_school"
    t.string   "mls_middle_school"
    t.string   "mls_high_school"
    t.decimal  "hoa_payment",                  :precision => 30, :scale => 10
    t.integer  "hoa_bill_freq"
    t.boolean  "zillow_sale_only"
    t.boolean  "zillow_rent_only"
    t.string   "flood"
    t.boolean  "near_commercial"
    t.date     "back_on_market"
    t.string   "basement_desc"
    t.string   "garage_attach"
    t.integer  "previous_list_price"
    t.integer  "rooms"
    t.string   "selling_agent_email"
    t.string   "selling_agent_name"
    t.string   "selling_agent_phone"
    t.string   "township"
    t.string   "waterfront"
  end

  add_index "properties", ["detail_id"], :name => "index_properties_on_detail_id"
  add_index "properties", ["legal_subdivision", "subdivision", "key_map_page"], :name => "legalsubdiv_subdiv_keymappage_idx"
  add_index "properties", ["market_id", "address"], :name => "index_properties_on_market_id_and_address"
  add_index "properties", ["market_id", "geo_market_area"], :name => "index_properties_on_market_id_and_geo_market_area"
  add_index "properties", ["market_id", "id", "tax_id", "address"], :name => "marketid_id_taxid_address_idx"
  add_index "properties", ["market_id", "mls_area_key"], :name => "index_properties_on_market_id_and_mls_area_key"
  add_index "properties", ["market_id", "mls_number"], :name => "index_properties_on_market_id_and_mls_number"
  add_index "properties", ["market_id", "status", "category"], :name => "marketid_status_category"
  add_index "properties", ["market_id", "status"], :name => "index_properties_on_market_id_and_status"
  add_index "properties", ["market_id", "tax_id"], :name => "index_properties_on_market_id_and_tax_id"
  add_index "properties", ["market_id"], :name => "index_properties_on_market_id"

  create_table "property_comparables", :force => true do |t|
    t.text     "_rental_comparables"
    t.text     "_sale_comparables"
    t.text     "_build_comparables"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "property_comparables", ["id"], :name => "index_property_comparables_on_id"

  create_table "property_details", :force => true do |t|
    t.text    "_raw"
    t.string  "access_lock_box"
    t.string  "access_to_complex"
    t.decimal "acreage",                            :precision => 30, :scale => 15
    t.decimal "annual_maintenance_fee",             :precision => 30, :scale => 10
    t.text    "appointment_phone_description"
    t.integer "bathrooms_full"
    t.integer "bathrooms_half"
    t.string  "builder_name"
    t.string  "building_name"
    t.decimal "building_stories",                   :precision => 30, :scale => 15
    t.string  "buyer_agent_bonus"
    t.string  "buyer_agent_compensation"
    t.date    "completion_date"
    t.string  "cooling_system_type"
    t.string  "coutertop_type"
    t.string  "defects"
    t.date    "estimated_close_date"
    t.string  "exterior_type"
    t.string  "financing_available"
    t.string  "floor_type"
    t.string  "foundation_type"
    t.string  "heating_system"
    t.decimal "hirise_fees",                        :precision => 30, :scale => 10
    t.text    "interior_description"
    t.string  "land_size"
    t.string  "legal_description"
    t.string  "level_of_unit"
    t.string  "listing_agent_fax"
    t.string  "listing_agent_id"
    t.string  "listing_broker_id"
    t.string  "listing_broker_name"
    t.integer "loan_amortized_years"
    t.decimal "loan_amount_interest_rate",          :precision => 30, :scale => 20
    t.decimal "loan_amount_new",                    :precision => 30, :scale => 10
    t.string  "loan_term"
    t.boolean "loss_mitigation"
    t.string  "lot"
    t.string  "lot_back_dimensions"
    t.string  "lot_description"
    t.string  "lot_front_dimension"
    t.string  "lot_improvements"
    t.string  "lot_left_dimension"
    t.string  "lot_number"
    t.string  "lot_right_dimension"
    t.string  "lot_use"
    t.decimal "maintenance_fee",                    :precision => 30, :scale => 10
    t.string  "mineral_rights"
    t.decimal "multi_owner_operating",              :precision => 30, :scale => 10
    t.decimal "muti_gross_income",                  :precision => 30, :scale => 10
    t.decimal "muti_gross_operating_income",        :precision => 30, :scale => 10
    t.decimal "mutli_income_net_operation",         :precision => 30, :scale => 10
    t.string  "neighborhood_number"
    t.boolean "new_construction"
    t.text    "new_construction_description"
    t.string  "night_phone"
    t.integer "number_of_photos"
    t.integer "number_of_units"
    t.string  "occupancy_rate"
    t.string  "office_phone"
    t.decimal "original_list_price",                :precision => 30, :scale => 10
    t.decimal "other_building_fees",                :precision => 30, :scale => 10
    t.decimal "other_fees",                         :precision => 30, :scale => 10
    t.boolean "pool_area"
    t.text    "public_remarks"
    t.string  "rental_sale_mls_number"
    t.string  "rental_terms"
    t.string  "road_surface"
    t.string  "roof_type"
    t.decimal "seller_paid_to_buyer_closing_costs", :precision => 30, :scale => 10
    t.decimal "seller_repair_credits",              :precision => 30, :scale => 10
    t.text    "showing_instructions"
    t.text    "siding_type"
    t.decimal "stories",                            :precision => 30, :scale => 15
    t.text    "street_surface_type"
    t.text    "style_of_building"
    t.string  "unit_number"
    t.integer "selling_agent_id"
    t.string  "equipment"
    t.string  "exterior_description"
    t.string  "fence_type"
    t.string  "fenced_yard"
    t.integer "fireplace_number"
    t.string  "hoa_membership"
    t.string  "mud_district"
  end

  add_index "property_details", ["id"], :name => "index_property_details_on_id"

  create_table "property_tags", :force => true do |t|
    t.string   "name"
    t.integer  "market_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "seq"
  end

  add_index "property_tags", ["market_id"], :name => "index_property_tags_on_market_id"

  create_table "proxies", :force => true do |t|
    t.string   "ip"
    t.integer  "port"
    t.string   "login"
    t.string   "password"
    t.integer  "daily_limit"
    t.integer  "daily_calls"
    t.date     "last_date"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.boolean  "too_many_attempts", :default => false
    t.integer  "error_count",       :default => 0
  end

  create_table "restart_cron_jobs", :force => true do |t|
    t.integer  "market_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "restart_cron_jobs", ["market_id"], :name => "index_restart_cron_jobs_on_market_id"

  create_table "school_districts", :force => true do |t|
    t.string   "school_district_name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "searches", :force => true do |t|
    t.integer  "market_id"
    t.integer  "user_id"
    t.text     "config"
    t.string   "name"
    t.string   "sort_by"
    t.boolean  "sort_ascending"
    t.date     "email_start_on"
    t.string   "email_frequency"
    t.boolean  "email_public"
    t.date     "email_last_on"
    t.boolean  "email_most_recent"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "searches", ["market_id", "user_id"], :name => "index_searches_on_market_id_and_user_id"

  create_table "services", :force => true do |t|
    t.string   "code"
    t.string   "login"
    t.string   "password"
    t.string   "key"
    t.integer  "daily_limit"
    t.integer  "daily_calls"
    t.date     "last_date"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "tbl_tmp", :force => true do |t|
    t.integer  "market_id"
    t.integer  "detail_id"
    t.text     "_overrides"
    t.integer  "beds"
    t.string   "category"
    t.integer  "cdom"
    t.string   "city"
    t.date     "closed_date"
    t.string   "county"
    t.date     "deed_date"
    t.string   "disclosures"
    t.integer  "dom"
    t.date     "expiration_date"
    t.string   "garage_capacity"
    t.string   "geo_market_area"
    t.string   "key_map_page"
    t.date     "last_sale_date"
    t.string   "legal_subdivision"
    t.decimal  "list_price",                   :precision => 30, :scale => 10
    t.string   "listing_agent_phone"
    t.string   "listing_agent_name"
    t.date     "listing_date"
    t.string   "lock_box_code"
    t.string   "location_key"
    t.string   "location_name"
    t.integer  "lot_square_feet"
    t.string   "mls_area_key"
    t.string   "mls_area_name"
    t.string   "mls_number"
    t.string   "mortgagee"
    t.decimal  "owner_estimated_loan_balance", :precision => 30, :scale => 10
    t.string   "owner_street_address"
    t.string   "owner_city"
    t.string   "owner_name"
    t.string   "owner_state"
    t.string   "owner_zip_code"
    t.string   "owner_zip_4"
    t.date     "pending_date"
    t.boolean  "pool_private"
    t.text     "realtor_remarks"
    t.string   "rental_class"
    t.string   "rental_type"
    t.decimal  "sale_price",                   :precision => 30, :scale => 10
    t.boolean  "selling_for_lot_value"
    t.string   "single_family_type"
    t.decimal  "sold_lease_price",             :precision => 30, :scale => 10
    t.integer  "square_feet"
    t.string   "street_direction"
    t.string   "street_name"
    t.integer  "street_number"
    t.string   "state"
    t.string   "status"
    t.string   "subdivision"
    t.string   "subdivision_section_number"
    t.decimal  "tax_amount",                   :precision => 30, :scale => 10
    t.string   "tax_id"
    t.decimal  "tax_market_value",             :precision => 30, :scale => 10
    t.decimal  "tax_rate",                     :precision => 30, :scale => 20
    t.decimal  "tax_value",                    :precision => 30, :scale => 10
    t.integer  "tax_year"
    t.date     "termination_date"
    t.integer  "year_built"
    t.string   "zip_4"
    t.string   "zip_code"
    t.date     "withdrawn_date"
    t.decimal  "sold_price",                   :precision => 30, :scale => 10
    t.decimal  "adj_sale_price",               :precision => 30, :scale => 10
    t.decimal  "adj_sale_price_psf",           :precision => 30, :scale => 10
    t.decimal  "list_price_psf",               :precision => 30, :scale => 10
    t.decimal  "sale_price_psf",               :precision => 30, :scale => 10
    t.decimal  "bathrooms",                    :precision => 30, :scale => 15
    t.string   "street_address"
    t.string   "zip"
    t.string   "city_state_zip"
    t.string   "address"
    t.string   "owner_zip"
    t.decimal  "latitude",                     :precision => 30, :scale => 15
    t.decimal  "longitude",                    :precision => 30, :scale => 15
    t.string   "zillow_id"
    t.decimal  "zillow_sale_estimate",         :precision => 30, :scale => 10
    t.decimal  "zillow_rental_estimate",       :precision => 30, :scale => 10
    t.datetime "zillow_updated_at"
    t.string   "pshark_id"
    t.datetime "pshark_updated_at"
    t.text     "comps_rental_stats"
    t.text     "comps_sale_stats"
    t.text     "comps_build_stats"
    t.datetime "comps_updated_at"
    t.string   "data_source"
    t.datetime "data_updated_at"
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
    t.string   "pshark_county"
    t.datetime "dom_updated_at"
    t.boolean  "auto_updatable"
    t.integer  "input_by"
    t.datetime "input_at"
    t.integer  "tag"
  end

  add_index "tbl_tmp", ["detail_id"], :name => "index_properties_on_detail_id"
  add_index "tbl_tmp", ["legal_subdivision", "subdivision", "key_map_page"], :name => "legalsubdiv_subdiv_keymappage_idx"
  add_index "tbl_tmp", ["market_id", "geo_market_area"], :name => "index_properties_on_market_id_and_geo_market_area"
  add_index "tbl_tmp", ["market_id", "id", "tax_id", "address"], :name => "marketid_id_taxid_address_idx"
  add_index "tbl_tmp", ["market_id", "mls_area_key"], :name => "index_properties_on_market_id_and_mls_area_key"
  add_index "tbl_tmp", ["market_id", "mls_number"], :name => "index_properties_on_market_id_and_mls_number"
  add_index "tbl_tmp", ["market_id", "status", "category"], :name => "marketid_status_category"
  add_index "tbl_tmp", ["market_id", "status"], :name => "index_properties_on_market_id_and_status"
  add_index "tbl_tmp", ["market_id"], :name => "index_properties_on_market_id"

  create_table "users", :force => true do |t|
    t.text     "preferences"
    t.boolean  "inactive"
    t.boolean  "admin"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "role"
    t.string   "phone"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "company"
    t.string   "fax"
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "xprop", :force => true do |t|
    t.integer  "market_id"
    t.integer  "detail_id"
    t.text     "_overrides"
    t.integer  "beds"
    t.string   "category"
    t.integer  "cdom"
    t.string   "city"
    t.date     "closed_date"
    t.string   "county"
    t.date     "deed_date"
    t.string   "disclosures"
    t.integer  "dom"
    t.date     "expiration_date"
    t.string   "garage_capacity"
    t.string   "geo_market_area"
    t.string   "key_map_page"
    t.date     "last_sale_date"
    t.string   "legal_subdivision"
    t.decimal  "list_price",                   :precision => 30, :scale => 10
    t.string   "listing_agent_phone"
    t.string   "listing_agent_name"
    t.date     "listing_date"
    t.string   "lock_box_code"
    t.string   "location_key"
    t.string   "location_name"
    t.integer  "lot_square_feet"
    t.string   "mls_area_key"
    t.string   "mls_area_name"
    t.string   "mls_number"
    t.string   "mortgagee"
    t.decimal  "owner_estimated_loan_balance", :precision => 30, :scale => 10
    t.string   "owner_street_address"
    t.string   "owner_city"
    t.string   "owner_name"
    t.string   "owner_state"
    t.string   "owner_zip_code"
    t.string   "owner_zip_4"
    t.date     "pending_date"
    t.boolean  "pool_private"
    t.text     "realtor_remarks"
    t.string   "rental_class"
    t.string   "rental_type"
    t.boolean  "selling_for_lot_value"
    t.string   "single_family_type"
    t.decimal  "sold_price",                   :precision => 30, :scale => 10
    t.decimal  "sold_lease_price",             :precision => 30, :scale => 10
    t.integer  "square_feet"
    t.string   "street_direction"
    t.string   "street_name"
    t.integer  "street_number"
    t.string   "state"
    t.string   "status"
    t.string   "subdivision"
    t.string   "subdivision_section_number"
    t.decimal  "tax_amount",                   :precision => 30, :scale => 10
    t.string   "tax_id"
    t.decimal  "tax_market_value",             :precision => 30, :scale => 10
    t.decimal  "tax_rate",                     :precision => 30, :scale => 20
    t.decimal  "tax_value",                    :precision => 30, :scale => 10
    t.integer  "tax_year"
    t.date     "termination_date"
    t.integer  "year_built"
    t.string   "zip_4"
    t.string   "zip_code"
    t.date     "withdrawn_date"
    t.decimal  "sale_price",                   :precision => 30, :scale => 10
    t.decimal  "adj_sale_price",               :precision => 30, :scale => 10
    t.decimal  "adj_sale_price_psf",           :precision => 30, :scale => 10
    t.decimal  "list_price_psf",               :precision => 30, :scale => 10
    t.decimal  "sale_price_psf",               :precision => 30, :scale => 10
    t.decimal  "bathrooms",                    :precision => 30, :scale => 15
    t.string   "street_address"
    t.string   "zip"
    t.string   "city_state_zip"
    t.string   "address"
    t.string   "owner_zip"
    t.decimal  "latitude",                     :precision => 30, :scale => 15
    t.decimal  "longitude",                    :precision => 30, :scale => 15
    t.string   "zillow_id"
    t.decimal  "zillow_sale_estimate",         :precision => 30, :scale => 10
    t.decimal  "zillow_rental_estimate",       :precision => 30, :scale => 10
    t.datetime "zillow_updated_at"
    t.string   "pshark_id"
    t.datetime "pshark_updated_at"
    t.string   "pshark_county"
    t.text     "comps_rental_stats"
    t.text     "comps_sale_stats"
    t.text     "comps_build_stats"
    t.datetime "comps_updated_at"
    t.string   "data_source"
    t.datetime "data_updated_at"
    t.datetime "dom_updated_at"
    t.datetime "created_at",                                                   :null => false
    t.datetime "updated_at",                                                   :null => false
    t.boolean  "auto_updatable"
  end

  add_index "xprop", ["market_id"], :name => "market_idx"

end
