class CreateDeletedCronJobs < ActiveRecord::Migration
  def change
    create_table :deleted_cron_jobs do |t|
     t.belongs_to :market
     DeletedCronJob.migrate(t)
     t.timestamps
    end
    
    add_index :deleted_cron_jobs, [:market_id]
    
  end
end
