class CreateMyProps < ActiveRecord::Migration
  def change
    create_table :my_props do |t|
      
      MyProp.migrate(t)
      t.timestamps
      
    end
    
    add_index :my_props, [:property_id]
    add_index :my_props, [:user_id]
  end
end
