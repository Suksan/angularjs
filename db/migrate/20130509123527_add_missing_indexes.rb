class AddMissingIndexes < ActiveRecord::Migration

  def change
    add_index :properties, [:market_id, :status]
    add_index :properties, [:market_id, :mls_area_key]
    add_index :properties, [:market_id, :geo_market_area]
  end

end
