class CreateMarketCronJobs < ActiveRecord::Migration
  def change
    create_table :market_cron_jobs do |t|
     t.belongs_to :market
     MarketCronJob.migrate(t)
     t.timestamps
    end
    
    add_index :market_cron_jobs, [:market_id]
    
  end
end
