class CreatePropertyTags < ActiveRecord::Migration

  def change
    create_table :property_tags do |t|
     
     PropertyTag.migrate(t)
     t.timestamps
         
    end
    
    add_index :property_tags, [:market_id]
    
  end

end
