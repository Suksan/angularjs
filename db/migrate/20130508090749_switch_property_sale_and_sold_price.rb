class SwitchPropertySaleAndSoldPrice < ActiveRecord::Migration
  def up
    swap
  end

  def down
    swap
  end

  private

    def swap
      ActiveRecord::Base.connection.execute("UPDATE properties SET sale_price=@tmp:=sale_price, sale_price=sold_price, sold_price=@tmp;")
      Property.reset_column_information
    end
end
