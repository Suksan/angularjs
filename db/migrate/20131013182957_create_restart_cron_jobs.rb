class CreateRestartCronJobs < ActiveRecord::Migration
  def change
    create_table :restart_cron_jobs do |t|
     t.belongs_to :market
     RestartCronJob.migrate(t)
     t.timestamps
    end
    
    add_index :restart_cron_jobs, [:market_id]
    
  end
end