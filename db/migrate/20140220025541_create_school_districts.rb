class CreateSchoolDistricts < ActiveRecord::Migration
  def change
    create_table :school_districts do |t|
      SchoolDistrict.migrate(t)
      t.timestamps
    end
  end
end
