class CreateProxies < ActiveRecord::Migration
  def change
  	create_table :proxies do |t|
      Proxy.migrate(t)
      t.timestamps
    end
  end
end
