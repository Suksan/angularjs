class CreateMarketsAndServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      Service.migrate(t)
      t.timestamps
    end

    create_table :markets do |t|
      Market.migrate(t)
      t.timestamps
    end

    create_table :market_assignments do |t|
      t.belongs_to :market
      t.belongs_to :user
      t.timestamps
    end
    add_index :market_assignments, :market_id
    add_index :market_assignments, :user_id
    add_index :market_assignments, [:market_id, :user_id]

    create_table :market_renovation_rehabs do |t|
      t.references :source, :polymorphic => true
      MarketRenovationRehab.migrate(t)
      t.timestamps
    end
    add_index :market_renovation_rehabs, [:source_id, :source_type]

    create_table :market_renovation_timelines do |t|
      t.references :source, :polymorphic => true
      MarketRenovationTimeline.migrate(t)
      t.timestamps
    end
    add_index :market_renovation_timelines, [:source_id, :source_type]

    create_table :market_build_costs do |t|
      t.references :source, :polymorphic => true
      MarketBuildCost.migrate(t)
      t.timestamps
    end
    add_index :market_build_costs, [:source_id, :source_type]

    create_table :market_definitions do |t|
      t.references :source, :polymorphic => true
      MarketDefinition.migrate(t)
      t.timestamps
    end
    add_index :market_definitions, [:source_id, :source_type]

    create_table :property_details do |t|
      PropertyDetail.migrate(t)
    end
    add_index :property_details, :id

    create_table :properties do |t|
      t.belongs_to :market
      t.belongs_to :detail
      Property.migrate(t)
      t.timestamps
    end
    add_index :properties, [:market_id, :mls_number]
    add_index :properties, :market_id
    add_index :properties, :detail_id
    
    create_table :proformas do |t|
      t.belongs_to :property
      t.belongs_to :user
      Proforma.migrate(t)
      t.timestamps
    end
    add_index :proformas, :property_id
    add_index :proformas, :user_id

    create_table :searches do |t|
      t.belongs_to :market
      t.belongs_to :user
      Search.migrate(t)
      t.timestamps
    end
    add_index :searches, [:market_id, :user_id]

    create_table :jobs do |t|
      t.belongs_to :market
      t.belongs_to :user
      Job.migrate(t)
      t.timestamps  
    end
    add_index :jobs, [:market_id]
    add_index :jobs, [:market_id, :user_id]
    add_index :jobs, [:priority, :run_at]
    
  end
end
