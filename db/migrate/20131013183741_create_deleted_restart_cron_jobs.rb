class CreateDeletedRestartCronJobs < ActiveRecord::Migration
  def change
    create_table :deleted_restart_cron_jobs do |t|
     t.belongs_to :market
     DeletedRestartCronJob.migrate(t)
     t.timestamps
    end

    add_index :deleted_restart_cron_jobs, [:market_id]

  end
end