class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
     
     Note.migrate(t)
     t.timestamps
         
    end
    
    add_index :notes, [:property_id]
    add_index :notes, [:posted_by]
  end
end