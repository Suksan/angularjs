require 'nokogiri'

# doc = Nokogiri::XML(File.open("metadata-lookup_type2.xml"))
doc = Nokogiri::XML(File.open("au02.xml"))

thing = doc.xpath("//METADATA-LOOKUP_TYPE[@Lookup='BasementDesc']/Lookup").each do |lookup|
  long_value = lookup.xpath("./LongValue").text
  value = lookup.xpath("./Value").text
  puts "\"#{value}\" => \"#{long_value}\","
  # puts "#{value}:#{long_value}"
end